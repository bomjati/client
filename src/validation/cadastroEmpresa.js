function validarCnpj(registration_number) {
  if (registration_number.length !== 18) {
    return { valido: false, texto: "CNPJ deve ter 18 digitos" };
  } else {
    return { valido: true, texto: "" };
  }
}

function validarCep(cep) {
  if (cep.length !== 8) {
    return { valido: false, texto: "CEP deve ter 8 digitos" };
  } else {
    return { valido: true, texto: "" };
  }
}

function validarPhone(phone1) {
  if (phone1.length !== 11) {
    return { valido: false, texto: "Telefone deve ter 9 digitos" };
  } else {
    return { valido: true, texto: "" };
  }
}

function validarEmail(email) {
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    return { valido: false, texto: "Formato de email invalido" };
  } else {
    return { valido: true, texto: "" };
  }
}

function validarPassword(password) {
  if (password.length < 6 || password.length > 72) {
    return { valido: false, texto: "Senha deve ter  o minimo de 6 digitos" };
  } else {
    return { valido: true, texto: "" };
  }
}

export { validarCnpj, validarCep, validarPhone, validarPassword, validarEmail };
