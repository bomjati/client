import React, { useState } from "react";
import { Outlet } from "react-router-dom";
import { makeStyles, AppBar, Toolbar } from "@material-ui/core";
import NavBar from "./NavBar";
import TopBar from "./TopBar";
import { useStyles } from "./useStyles";

const DashboardLayout = () => {
  const classes = useStyles();
  const [isMobileNavOpen, setMobileNavOpen] = useState(false);
  const [isNavOpen, setNavOpen] = useState(false);

  return (
    <div className={classes.root}>
      <TopBar
        onMobileNavOpen={() => setMobileNavOpen(true)}
        onNavOpen={() => setNavOpen(true)}
      />
      <NavBar
        onMobileClose={() => setMobileNavOpen(false)}
        onClose={() => setNavOpen(false)}
        openMobile={isMobileNavOpen}
        openNav={isNavOpen}
      />
      <div className={classes.wrapper}>
        <div className={classes.contentContainer}>
          <div className={classes.content}>
            <Outlet className={classes.content} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardLayout;
