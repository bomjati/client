import React, { useState, useEffect, MouseEvent, Fragment } from "react";
import { Link as RouterLink, useLocation } from "react-router-dom";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  AppBar,
  Badge,
  Box,
  Hidden,
  IconButton,
  Toolbar,
  makeStyles,
  Avatar,
  Typography,
  Menu,
  MenuItem,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemAvatar,
  Divider,
  Button,
} from "@material-ui/core";
import {
  Menu as MenuIcon,
  RateReview as RateReviewIcon,
  NotificationsOutlined as NotificationsIcon,
  Input as InputIcon,
  Radio as RadioIcon,
} from "@material-ui/icons";
import moment from "moment";
import api from "../../services/api";
import { useCookies } from "react-cookie";

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    width: 60,
    height: 60,
  },
}));

const user = {
  avatar: "/static/images/avatars/avatar_4.png",
};

const TopBar = ({ className, onNavOpen, onMobileNavOpen, ...rest }) => {
  const classes = useStyles();
  const location = useLocation();
  const [dateActived, setDateActived] = useState("");
  const [business, setBusiness] = useState(null);
  const [listNewChannels, setListNewChannels] = useState([]);
  const [channelControls, setChannelControls] = useState([]);
  const [isManager, setIsManager] = useCookies(["IsManager"]);
  const [isAdministrador, setIsAdministrador] = useState({});
  const [planControls, setPlanControls] = useCookies("PlanControls");
  const [vencimento, setVencimento] = useState(0);
  const [notifications, setNotifications] = useState([]);
  const [open, setOpen] = useState(null);
  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    backgroundColor: "white",
  };

  const styleIcon = {
    color: "#353b48",
  };

  useEffect(() => {
    onGetNotification();
    setInterval(() => {
      onGetNotification();
    }, 15000);
  }, []);

  useEffect(() => {
    onMe();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  useEffect(() => {
    if (isManager) {
      console.log(isManager.IsManager);
      setIsAdministrador(isManager.IsManager);

      if (business) {
        setInterval(() => {
          if (isManager.IsManager.status) {
            onGetClientsVerified();
          }
        }, 15000);
      }
    }
  }, [isManager, business]);

  useEffect(() => {
    if (planControls) {
      console.log(planControls);
    }
  }, [planControls]);

  const onGetClientsVerified = () => {
    api.get(`/business/client/${business.id}`).then((res) => {
      if (res.data) {
        setListNewChannels(res.data[0].Channels);
      }
    });
  };

  const onGetNotification = () => {
    api.get("/channel/simple").then((res) => {
      if (res) {
        let arryClientIds = [];

        for (let index = 0; index < res.data.length; index++) {
          const element = res.data[index];

          arryClientIds.push(element.clientId);
        }

        if (res.data.length > 0) {
          api
            .post("/supportrequest/notification", {
              clientIds: arryClientIds,
            })
            .then((response) => response.data)
            .then((resp) => {
              if (resp) {
                setNotifications(
                  resp.sort(function (a, b) {
                    return new Date(b.createdAt) - new Date(a.updatedAt);
                  })
                );
              }
            });
        }
      }
    });
  };

  const handleClose = (id) => {
    setOpen(null);
  };

  const handleClick = (event: MouseEvent<HTMLButtonElement>, id) => {
    setOpen(event.currentTarget);
  };

  const onMe = async () => {
    await api
      .get("/login/me")
      .then((response) => response.data)
      .then((res) => {
        console.log(res);
        if (res) {
          setBusiness(res.Business);
          setDateActived(res.Business.Plancontrols[0].updatedAt);
          setVencimento(res.Business.Plancontrols[0].Plan.days);
          setChannelControls(res.Channelcontrols);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onVerified = (id) => {
    api.put(`/client/edit/${id}`, { verified: "true" }).then((res) => {
      if (res.data) {
        if (res.data.message == "success") {
          onGetClientsVerified();
          setOpen(null);
        }
      }
    });
  };

  return (
    <AppBar className={clsx(classes.root, className)} elevation={0} {...rest}>
      {planControls.PlanControls ? (
        planControls.PlanControls.Plan ? (
          planControls.PlanControls.Plan.name == "Free" ? (
            <Box
              display="flex"
              justifyContent="center"
              style={{ backgroundColor: "red", height: 25 }}
            >
              <p style={{ color: "white", fontWeight: "bold" }}>
                Você está em Teste
              </p>
            </Box>
          ) : null
        ) : null
      ) : null}
      <Toolbar style={styleButtonIcon}>
        <Hidden lgUp>
          <IconButton color="inherit" onClick={onMobileNavOpen}>
            <MenuIcon style={styleIcon} />
          </IconButton>
        </Hidden>

        <h3 style={{ color: "#eb2f06" }} className="">
          Bomja
        </h3>

        <Box flexGrow={1} />
        <Hidden xsDown>
          <Typography
            className="mt-1"
            style={{ color: "#FD4B04" }}
            variant="inherit"
          >
            Restam{" "}
            {dateActived == ""
              ? ""
              : moment().to(
                  moment(dateActived, "YYYY-MM-DD HH:mm:ss").add(
                    vencimento,
                    "days"
                  ),
                  true
                )}
          </Typography>
        </Hidden>
        <div className="ml-2 mr-2">
          <IconButton
            onClick={(event) => {
              event.stopPropagation();
              handleClick(event);
            }}
            color="inherit"
          >
            <Badge
              badgeContent={notifications.length + listNewChannels.length}
              color="primary"
              max={999}
              //variant="dot"
            >
              <NotificationsIcon style={styleIcon} />
            </Badge>
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={open}
            keepMounted
            open={Boolean(open)}
            onClose={handleClose}
          >
            <List style={{ width: "36ch" }}>
              {(notifications.length == 0) & (listNewChannels.length == 0) ? (
                <div onClick={() => handleClose()}>
                  <Box display="flex" justifyContent="center" mt={2}>
                    <NotificationsIcon style={{ color: "silver" }} />
                  </Box>
                  <h6
                    style={{ color: "silver" }}
                    className="mt-2 mb-4 text-center"
                  >
                    Não há notificações no momento
                  </h6>
                </div>
              ) : (
                <div>
                  {listNewChannels.map((n) => (
                    <div>
                      <ListItem alignItems="flex-start">
                        <ListItemAvatar>
                          <RadioIcon />
                        </ListItemAvatar>
                        <ListItemText
                          primary="Nova Radio"
                          secondary={
                            <Fragment>
                              <div>
                                <Typography
                                  component="span"
                                  variant="body2"
                                  className={classes.inline}
                                  style={{
                                    fontWeight: "bold",
                                    marginTop: 20,
                                  }}
                                  color="textPrimary"
                                >
                                  {n.Client.name}
                                </Typography>
                              </div>
                              <div>
                                <Typography
                                  component="span"
                                  variant="body2"
                                  className={classes.inline}
                                  color="textSecondary"
                                >
                                  {n.Client.slogan}
                                </Typography>
                              </div>
                            </Fragment>
                          }
                        />
                      </ListItem>
                      <Box m={2} display="flex" justifyContent="flex-end">
                        <Button
                          onClick={() => {
                            onVerified(n.Client.id);
                          }}
                          variant="contained"
                          size="small"
                          style={{ backgroundColor: "red", color: "white" }}
                        >
                          OK
                        </Button>
                      </Box>
                      <Divider variant="inset" component="li" />
                    </div>
                  ))}
                  {notifications.map((n) => (
                    <div>
                      <Button
                        fullWidth
                        href={`/app/radiop?page=request&id=${n.id}`}
                        style={{ textTransform: "none" }}
                      >
                        <ListItem alignItems="flex-start">
                          <ListItemAvatar>
                            <RateReviewIcon />
                          </ListItemAvatar>
                          <ListItemText
                            primary="Novo Pedido"
                            secondary={
                              <Fragment>
                                <Typography
                                  component="span"
                                  variant="body2"
                                  className={classes.inline}
                                  color="textPrimary"
                                >
                                  {JSON.parse(n.informations)[0].client}
                                </Typography>
                                {`${
                                  n.informations[0].isTextFree == "true"
                                    ? `— ${String(
                                        n.informations[0].textFree
                                      ).substr(0, 47)}`
                                    : ""
                                }…`}
                              </Fragment>
                            }
                          />
                        </ListItem>
                      </Button>
                      <Divider variant="inset" component="li" />
                    </div>
                  ))}
                </div>
              )}
            </List>
          </Menu>
        </div>
        {/* <IconButton
          onClick={() => {
            window.location.href = "/home";
          }}
          color="inherit"
        >
          <InputIcon style={styleIcon} />
        </IconButton> */}

        <IconButton color="inherit" onClick={onNavOpen}>
          {/* <MenuIcon style={styleIcon} /> */}
          <Avatar src={user.avatar} />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

TopBar.propTypes = {
  className: PropTypes.string,
  onMobileNavOpen: PropTypes.func,
};

export default TopBar;
