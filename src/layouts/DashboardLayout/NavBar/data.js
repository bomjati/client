import {
  AlertCircle as AlertCircleIcon,
  BarChart as BarChartIcon,
  Archive as ArchiveIcon,
  Settings as SettingsIcon,
  Bookmark as BookmarkIcon,
  Briefcase as BriefcaseIcon,
  LogOut as LogOutIcon,
  User as UsersIcon,
  Users as UserssIcon,
  Folder as FolderIcon,
  FolderPlus as FolderPlusIcon,
  Speaker as SpeakerIcon,
  Music as MusicIcon,
  FilePlus as FilePlusIcon,
  Disc as DiscIcon,
  DollarSign as DollarSignIcon,
  Calendar as CalendarIcon,
  Mic as MicIcon,
  Activity as ActivityIcon,
  Book as BookIcon,
  Flag as FlagIcon,
  ShoppingBag as ShoppingBagIcon,
  Columns as ColumnsIcon,
  Globe as GlobeIcon,
  Home as HomeIcon,
  Key as KeyIcon,
  Edit3 as EditIcon,
  Clipboard as clipboardIcon,
  Grid as GridIcon,
} from "react-feather";

export const items = ({ cookie, isAdministrador, isLeft }) => {
  //const [planControls, setPlanControls] = useCookies("PlanControls");
  const Modulecontrols = cookie; //planControls.PlanControls.Plan.Modulecontrols;
  return [
    !isLeft
      ? null
      : {
          href:
            String(window.location.href).indexOf("vitrine") > -1
              ? "/app/vitrine/dashboard"
              : String(window.location.href).indexOf("radio") > -1
              ? "/app/radiod?page=dashboard"
              : String(window.location.href).indexOf("commerce") > -1
              ? "/app/commerce/dashboard"
              : "/app/dashboard",
          icon: BarChartIcon,
          title: "Dashboard",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "Rádio").length >
      0
    ) | isLeft
      ? null
      : {
          href: "/app/radioh?page=home",
          icon: MusicIcon,
          title: "Rádio",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "Rádio").length >
      0
    ) |
    !isLeft |
    !(String(window.location.href).indexOf("radio") > -1)
      ? null
      : {
          href: "/app/radioh?page=home",
          icon: HomeIcon,
          title: "Home",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "Rádio").length >
      0
    ) |
    !isLeft |
    !(String(window.location.href).indexOf("radio") > -1)
      ? null
      : {
          href: "/app/radioa?page=access",
          index: 1,
          icon: KeyIcon,
          title: "Acessos",
          isIndex: true,
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "Rádio").length >
      0
    ) |
    !isLeft |
    !(String(window.location.href).indexOf("radio") > -1) |
    !isAdministrador
      ? null
      : {
          href: "/app/radiop?page=request",
          index: 3,
          icon: EditIcon,
          title: "Pedidos",
          isIndex: true,
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "Rádio").length >
      0
    ) |
    !isLeft |
    !(String(window.location.href).indexOf("radio") > -1)
      ? null
      : {
          href: "/app/radioe?page=employee",
          index: 2,
          icon: UserssIcon,
          title: "Funcionarios",
          isIndex: true,
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "Vitrine Digital")
        .length > 0
    ) | !(!isLeft | (String(window.location.href).indexOf("vitrine") > -1))
      ? null
      : {
          href: "/app/vitrine",
          icon: ColumnsIcon,
          title: "Vitrine Promoções",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "E-commerce")
        .length > 0
    ) | !(!isLeft | (String(window.location.href).indexOf("commerce") > -1))
      ? null
      : {
          href: "/app/commerce",
          icon: GlobeIcon,
          title: "E-commerce",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "E-commerce")
        .length > 0
    ) | !(!isLeft | (String(window.location.href).indexOf("content") > -1))
      ? null
      : {
          href: "/app/content",
          icon: clipboardIcon,
          title: "Criador de Conteúdo",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "E-commerce")
        .length > 0
    ) | !(!isLeft | (String(window.location.href).indexOf("offer") > -1))
      ? null
      : {
          href: "/app/offer",
          icon: GridIcon,
          title: "Criador de Ofertas",
        },
    !(
      Modulecontrols.filter(
        (module) =>
          (module.Module.name == "E-commerce") |
          (module.Module.name == "Vitrine Digital")
      ).length > 0
    ) |
    !isLeft |
    !(
      (String(window.location.href).indexOf("vitrine") > -1) |
      (String(window.location.href).indexOf("produtos") > -1) |
      (String(window.location.href).indexOf("configuracoes") > -1) |
      (String(window.location.href).indexOf("commerce") > -1)
    )
      ? null
      : {
          href:
            String(window.location.href).indexOf("vitrine") > -1
              ? "/app/vitrine/produtos"
              : String(window.location.href).indexOf("commerce") > -1
              ? "/app/commerce/produtos"
              : "/app/produtos",
          icon: ShoppingBagIcon,
          title: "Produtos",
        },
    !(
      Modulecontrols.filter((module) => module.Module.name == "E-commerce")
        .length > 0
    ) |
    !isLeft |
    !(
      (String(window.location.href).indexOf("produtos") > -1) |
      (String(window.location.href).indexOf("configuracoes") > -1) |
      (String(window.location.href).indexOf("commerce") > -1)
    )
      ? null
      : {
          href:
            String(window.location.href).indexOf("commerce") > -1
              ? "/app/commerce/vendas"
              : "/app/vendas",
          icon: ShoppingBagIcon,
          title: "Vendas",
        },

    !(
      Modulecontrols.filter(
        (module) =>
          (module.Module.name == "E-commerce") |
          (module.Module.name == "Vitrine Digital")
      ).length > 0
    ) |
    !isLeft |
    !(
      (String(window.location.href).indexOf("vitrine") > -1) |
      (String(window.location.href).indexOf("produtos") > -1) |
      (String(window.location.href).indexOf("configuracoes") > -1) |
      (String(window.location.href).indexOf("commerce") > -1)
    )
      ? null
      : {
          href:
            String(window.location.href).indexOf("vitrine") > -1
              ? "/app/vitrine/configuracoes"
              : String(window.location.href).indexOf("commerce") > -1
              ? "/app/commerce/configuracoes"
              : "/app/configuracoes",
          icon: SettingsIcon,
          title: "Configurações",
        },
    !isAdministrador | isLeft
      ? null
      : {
          href: "/app/conta",
          icon: UsersIcon,
          title: "Conta",
        },
    {
      href: "/home",
      icon: LogOutIcon,
      title: "Logout",
    },
  ];
};

export const itemsADM = [
  {
    href: "/app-admin/dashboard",
    icon: BarChartIcon,
    title: "Dashboard",
    collapse: false,
  },
  {
    href: "/app-admin/segmento",
    icon: ArchiveIcon,
    title: "Segmentos",
    collapse: false,
  },
  {
    icon: FolderPlusIcon,
    title: "Tópicos",
    collapse: true,
    content: [
      {
        href: "/app-admin/topico-genero",
        icon: FlagIcon,
        title: "Gêneros",
        collapse: false,
      },
      {
        href: "/app-admin/topico-subgenero",
        icon: FlagIcon,
        title: "Sub-gêneros",
        collapse: false,
      },
    ],
  },
  /*{
    icon: FolderPlusIcon,
    title: "Conteúdos",
    collapse: true,
    content: [
      {
        href: "/app-admin/grupo-conteudo",
        icon: FolderIcon,
        title: "Grupos de Conteúdo",
        collapse: false,
      },
      {
        href: "/app-admin/tipo-conteudo",
        icon: BookmarkIcon,
        title: "Tipos de Conteúdos",
        collapse: false,
      },
      {
        href: "/app-admin/conteudo",
        icon: FilePlusIcon,
        title: "Lista de Conteúdos",
        collapse: false,
      },
    ],
  },
  {
    icon: BookIcon,
    title: "Conteúdo dos Canais",
    collapse: true,
    content: [
      {
        href: "/app-admin/comerciais",
        icon: ActivityIcon,
        title: "Comerciais",
        collapse: false,
      },
      {
        href: "/app-admin/vinhetas",
        icon: MicIcon,
        title: "Vinhetas",
        collapse: false,
      },
    ],
  },
  {
    href: "/app-admin/genero",
    icon: DiscIcon,
    title: "Gêneros",
    collapse: false,
  },
  {
    href: "/app-admin/sub-genero",
    icon: DiscIcon,
    title: "Sub Gêneros",
    collapse: false,
  },
  {
    href: "/app-admin/musica",
    icon: MusicIcon,
    title: "Musicas",
    collapse: false,
  }, */
  {
    href: "/app-admin/radio",
    icon: SpeakerIcon,
    title: "Rádio",
    collapse: false,
  },
  {
    href: "/app-admin/lojas",
    icon: BriefcaseIcon,
    title: "Empresas",
    collapse: false,
  },
  {
    href: "/app-admin/financeiro",
    icon: DollarSignIcon,
    title: "Financeiro",
    collapse: false,
  },
  {
    href: "/admin/login",
    icon: LogOutIcon,
    title: "Logout",
    collapse: false,
  },
];
