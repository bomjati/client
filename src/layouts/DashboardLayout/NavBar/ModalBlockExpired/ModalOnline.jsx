import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Divider,
  TextField,
  Slider,
  Grid,
  Typography,
  IconButton,
  Box,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  PlayArrow as PlayArrowIcon,
  Stop as StopIcon,
  Delete as DeleteIcon,
} from "@material-ui/icons";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "center",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white",
  },
}));

const Speech = ({ onCloseSpeech, open }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
            <Box
              p={10}
              maxWidth={400}
              style={{
                borderRadius: 15,
                backgroundColor: "#e67e22",
              }}
            >
              <h2
                style={{ marginBottom: 30 }}
                className="text-white text-center"
              >
                Expirou
              </h2>
              <h6 className="text-white text-center">
                Seu plano expirou e foi bloqueado, para mais detalhes entre em contato com o suporte.
              </h6>
            </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default Speech;
