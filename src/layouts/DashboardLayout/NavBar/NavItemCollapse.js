import React, { useState, useEffect } from "react";
import { NavLink as RouterLink } from "react-router-dom";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Button,
  List,
  ListItem,
  makeStyles,
  Collapse,
  Box,
} from "@material-ui/core";
import {
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
} from "@material-ui/icons";
import NavItem from "./NavItem";

const useStyles = makeStyles((theme) => ({
  item: {
    display: "flex",
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
    justifyContent: "flex-start",
    letterSpacing: 0,
    padding: "10px 8px",
    textTransform: "none",
    width: "100%",
  },
  icon: {
    marginRight: theme.spacing(1),
  },
  title: {
    marginRight: "auto",
    marginLeft: "20px",
    fontWeight: "bold"
  },
  active: {
    color: theme.palette.primary.main,
    "& $title": {
      fontWeight: theme.typography.fontWeightMedium,
    },
    "& $icon": {
      color: theme.palette.primary.main,
    },
  },
}));

const NavItemCollapse = ({
  className,
  content,
  key,
  icon: Icon,
  title,
  ...rest
}) => {
  const classes = useStyles();
  const [values, setValues] = useState();

  const checkCollapse = () => {
    setValues({ ...values, [title]: values ? !values[title] : true });
  };

  useEffect(() => {
    console.log(values);
  }, [values]);

  return (
    <div>
      <ListItem
        className={clsx(classes.item, className)}
        disableGutters
        {...rest}
      >
        <Button
          onClick={() => {
            checkCollapse();
          }}
          activeClassName={classes.active}
          className={classes.button}
        >
          {Icon && <Icon className={classes.icon} size="20" />}
          <span className={classes.title}>{title}</span>
          <Box display="flex" justifyContent="flex-end">
            {values ? (
              values[title] ? (
                <ExpandLessIcon />
              ) : (
                <ExpandMoreIcon />
              )
            ) : (
              <ExpandMoreIcon />
            )}
          </Box>
        </Button>
      </ListItem>
      <Collapse in={values && values[title]} timeout="auto" unmountOnExit>
        <div className="ml-4">
          <List component="div" disablePadding>
            {content.map((item) => (
              <NavItem
                href={item.href}
                key={item.title}
                title={item.title}
                icon={item.icon}
              />
            ))}
          </List>
        </div>
      </Collapse>
    </div>
  );
};

NavItem.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string,
};

export default NavItemCollapse;
