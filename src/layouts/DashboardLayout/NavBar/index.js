import React, { useEffect, useState } from "react";
import { Link as RouterLink, useLocation } from "react-router-dom";
import PropTypes from "prop-types";
import {
  Avatar,
  Box,
  Button,
  Divider,
  Drawer,
  Hidden,
  List,
  Typography,
  makeStyles,
  IconButton,
  Chip,
} from "@material-ui/core";
import NavItem from "./NavItem";
import NavItemCollapse from "./NavItemCollapse";
import { items, itemsADM } from "./data";
import ModalBlock from "./ModalBlock";
import ModalBlockExpired from "./ModalBlockExpired";
import api from "../../../services/api";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import { useCookies } from "react-cookie";
import moment from "moment";

const user = {
  avatar: "/static/images/avatars/avatar_4.png",
};

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 98,
    height: "calc(100% - 64px)",
  },
  avatar: {
    cursor: "pointer",
    width: 64,
    height: 64,
  },
  botaoPlay: {
    "&:focus": {
      outline: "none",
    },
  },
}));

const NavBar = ({ onMobileClose, openMobile, onClose, openNav }) => {
  const classes = useStyles();
  const location = useLocation();
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [typeAccess, setTypeAccess] = useState("");
  const [accessControls, setAccessControls] = useCookies(["AccessControls"]);
  const [planControls, setPlanControls] = useCookies("PlanControls");
  const [isManager, setIsManager] = useCookies(["IsManager"]);
  const [isAdministrador, setIsAdministrador] = useState({});
  const [showModalBlock, setShowModalBlock] = useState(false);
  const [showModalBlockExpired, setShowModalBlockExpired] = useState(false);
  const [dateActived, setDateActived] = useState("");
  const [vencimento, setVencimento] = useState(0);

  useEffect(() => {
    if (isManager) {
      console.log(isManager.IsManager);
      setIsAdministrador(isManager.IsManager);
    }
  }, [isManager]);

  useEffect(() => {
    console.log(planControls.PlanControls);
  }, [planControls]);

  useEffect(() => {
    console.log(window.location.href);
  }, []);

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }

    if (openNav && onClose) {
      onClose();
    }

    onMe();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  const onMe = async () => {
    await api
      .get("/login/me")
      .then((response) => response.data)
      .then((res) => {
        console.log(res);
        if (res) {
          setNome(res.name);
          setEmail(res.email);
          setTypeAccess(String(res.Typeuser.name).toLowerCase());
          setShowModalBlock(
            String(res.Business.Plancontrols[0].status).toLowerCase() == "ativo"
              ? false
              : true
          );
          setDateActived(res.Business.Plancontrols[0].updatedAt);
          setVencimento(res.Business.Plancontrols[0].Plan.days);

          if (
            moment(
              res.Business.Plancontrols[0].updatedAt,
              "YYYY-MM-DD HH:mm:ss"
            )
              .add(res.Business.Plancontrols[0].Plan.days, "days")
              .diff(moment(), "days") < 1
          ) {
            setShowModalBlockExpired(true);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const content = (
    <Box height="100%" display="flex" flexDirection="column">
      <Box alignItems="center" display="flex" flexDirection="column" p={2}>
        <Avatar className={classes.avatar} src={user.avatar} />
        <Typography className={classes.name} color="textPrimary" variant="h5">
          {nome}
        </Typography>
        <Typography color="textSecondary" variant="body2">
          {email}
        </Typography>
        {!isAdministrador.status ? null : (
          <Chip
            style={{
              backgroundColor: "#000133",
              color: "white",
              marginTop: 10,
            }}
            label="Admistrador"
            variant="outlined"
            size="small"
          />
        )}
        <Typography
          className="mt-1"
          style={{ color: "#FD4B04" }}
          variant="inherit"
        >
          Restam{" "}
          {dateActived == ""
            ? ""
            : moment().to(
                moment(dateActived, "YYYY-MM-DD HH:mm:ss").add(
                  vencimento,
                  "days"
                ),
                true
              )}
        </Typography>
      </Box>
      <Divider />
      <Box p={2}>
        <List>
          {typeAccess == ""
            ? null
            : typeAccess == "admin"
            ? itemsADM.map((item) =>
                !item.collapse ? (
                  <NavItem
                    href={item.href}
                    key={item.title}
                    title={item.title}
                    icon={item.icon}
                    isIndex={item.index ? item.index : false}
                  />
                ) : (
                  <NavItemCollapse
                    key={item.title}
                    title={item.title}
                    icon={item.icon}
                    content={item.content}
                  />
                )
              )
            : items({
                cookie:
                  planControls.PlanControls != "undefined"
                    ? planControls.PlanControls.Plan.Modulecontrols
                    : [{ Module: { name: "Rádio" } }],
                isAdministrador: isAdministrador.status,
                isLeft: true,
              }).map(
                (item) =>
                  item && (
                    <NavItem
                      href={item.href}
                      key={item.title}
                      title={item.title}
                      icon={item.icon}
                    />
                  )
              )}
        </List>
      </Box>
    </Box>
  );

  const contentRight = (
    <Box height="100%" display="flex" flexDirection="column">
      <Box alignItems="center" display="flex" p={2}>
        <Avatar
          style={{ marginRight: 20 }}
          component={RouterLink}
          src={user.avatar}
          to="/app/account"
        />
        <Typography
          style={{ fontWeight: "bold" }}
          className={classes.name}
          color="textPrimary"
          variant="body1"
        >
          {nome}
        </Typography>
      </Box>
      <Divider />
      <Box p={2}>
        <List>
          {typeAccess == ""
            ? null
            : typeAccess == "admin"
            ? itemsADM.map((item) =>
                !item.collapse ? (
                  <NavItem
                    href={item.href}
                    key={item.title}
                    title={item.title}
                    icon={item.icon}
                  />
                ) : (
                  <NavItemCollapse
                    key={item.title}
                    title={item.title}
                    icon={item.icon}
                    content={item.content}
                  />
                )
              )
            : items({
                cookie:
                  planControls.PlanControls != "undefined"
                    ? planControls.PlanControls.Plan.Modulecontrols
                    : [{ Module: { name: "Rádio" } }],
                isAdministrador: isAdministrador.status,
                isLeft: false,
              }).map(
                (item) =>
                  item && (
                    <NavItem
                      href={item.href}
                      key={item.title}
                      title={item.title}
                      icon={item.icon}
                    />
                  )
              )}
        </List>
      </Box>
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden smDown>
        <Drawer
          anchor="left"
          PaperProps={{
            style: {
              width: 256,
              top: planControls.PlanControls
                ? planControls.PlanControls.Plan
                  ? planControls.PlanControls.Plan.name == "Free"
                    ? 98
                    : 67
                  : 67
                : 67,
              height: "calc(100% - 64px)",
            },
          }}
          open
          variant="persistent"
        >
          {content}
        </Drawer>
      </Hidden>
      <Drawer
        anchor="right"
        classes={{ paper: classes.mobileDrawer }}
        onClose={onClose}
        open={openNav}
        variant="temporary"
      >
        {contentRight}
      </Drawer>
      <ModalBlock open={showModalBlock} />
      <ModalBlockExpired open={showModalBlockExpired} />
    </>
  );
};

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool,
};

NavBar.defaultProps = {
  onMobileClose: () => {},
  openMobile: false,
};

export default NavBar;
