import { BrowserRouter, Route, Switch, Navigate } from "react-router-dom";
import { isAuthenticated } from "../services/auth";
import React from "react";
import "../App.css";

import MainLayout from "../layouts/MainLayout";
import Home from "../components/Home";
import Dashboard from "../components/Dashboard/DashboardView";
import DashboardADM from "../components/Dashboard/DashboardView";
import Music from "../components/Radio";
import Login from "../components/Accounts/Login";
import LoginRepresentante from "../components/Accounts/Login/LoginRepresentante";
import Signup from "../components/Accounts/Signup";
import SignupRepresentante from "../components/Accounts/Signup/SignupRepresentante";
import Forgot from "../components/Accounts/Forgot";
import ForgotRepresentante from "../components/Accounts/Forgot/ForgotRepresentante";
import SignupChecked from "../components/InnerSection/SignupChecked";
import SignupCheckedRepresentante from "../components/InnerSection/SignupChecked/SignupCheckedRepresentante";
import ForgotChecked from "../components/InnerSection/ForgotChecked";
import ForgotCheckedRepresentante from "../components/InnerSection/ForgotChecked/ForgotCheckedRepresentante";
import ErrorPage from "../components/ErrorPage";
import CarouselProducts from "../components/CarouselProducts";
import CarouselProductsUtilitares from "../components/CarouselProductsUtilitarios";
import Commerce from "../components/Loja";

/*Imports Dashboard Cliente*/
import DashboardLayout from "../layouts/DashboardLayout";
import AccountView from "../components/Dashboard/views/account/AccountView";
import BusinessListView from "../components/Dashboard/views/business/BusinessListView";
import ChannelListView from "../components/Dashboard/views/channel/ChannelListView";
import AccessView from "../components/Dashboard/views/access/AccessListView";
import EmployeeView from "../components/Dashboard/views/employee/EmployeeListView";
import ProductListView from "../components/Dashboard/views/product/ProductListView";
import SalesListView from "../components/Dashboard/views/sales/SalesListView";
import RegisterView from "../components/Dashboard/views/auth/RegisterView";
import SettingsView from "../components/Dashboard/views/settings/SettingsView";
import VitrineView from "../components/Dashboard/views/vitrine/VitrineView";
import CommerceLink from "../components/Dashboard/views/commercelink/EcommerceListView";
import Content from "../components/Dashboard/views/content/ContentListView";
import Offer from "../components/Dashboard/views/offer/OfferListView";

/*Imports Dashboard Administração*/
import AccountViewADM from "../components/Dashboard/viewsADM/account/AccountView";
import BusinessListViewADM from "../components/Dashboard/viewsADM/business/BusinessListView";
import ContentGroupViewADM from "../components/Dashboard/viewsADM/contentgroup/ContentGroupListView";
import TypeContentViewADM from "../components/Dashboard/viewsADM/typecontent/TypeContentListView";
import GenreTopicListViewADM from "../components/Dashboard/viewsADM/genretopic/GenreTopicListView";
import GenresubTopicListViewADM from "../components/Dashboard/viewsADM/genresubtopic/GenresubTopicListView";
import ContentViewADM from "../components/Dashboard/viewsADM/content/ContentListView";
import SegmentViewADM from "../components/Dashboard/viewsADM/segment/SegmentListView";
import TypeVignetteViewADM from "../components/Dashboard/viewsADM/typevignette/TypeVignetteListView";
import GenreSubViewADM from "../components/Dashboard/viewsADM/genresub/GenreSubListView";
import LoginViewADM from "../components/Dashboard/viewsADM/auth/LoginView";
import SettingsViewADM from "../components/Dashboard/viewsADM/settings/SettingsView";
import GenreViewADM from "../components/Dashboard/viewsADM/genre/GenreListView";
import CommercialViewADM from "../components/Dashboard/viewsADM/commercial/CommercialListView";
import VignetteViewADM from "../components/Dashboard/viewsADM/vignette/VignetteListView";
import ChannelViewADM from "../components/Dashboard/viewsADM/channel/ChannelListView";
import MusicViewADM from "../components/Dashboard/viewsADM/music/MusicListView";

const routess = [
  {
    path: "/",
    element: <MainLayout />,
    children: [
      { path: "home", element: <Home /> },
      { path: "login", element: <Login /> },
      { path: "bomja-music", element: <Music /> },
      { path: "registro", element: <Signup /> },
      { path: "recuperar-senha", element: <Forgot /> },
      { path: "registro-concluido", element: <SignupChecked /> },
      { path: "recuperar-senha-concluido", element: <ForgotChecked /> },
      { path: "list-product", element: <CarouselProducts /> },
      {
        path: "list-product-utilitares",
        element: <CarouselProductsUtilitares />,
      },
      {
        path: "loja/:nameBusiness/:idBusiness/:idSegment/:ecommerceId",
        element: <Commerce />,
      },
      { path: "404", element: <ErrorPage /> },
      { path: "/", element: <Navigate to="/home" /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "app",
    element: <DashboardLayout />,
    children: [
      { path: "dashboard", element: <Dashboard /> },
      { path: "radio/dashboard", element: <Dashboard /> },
      { path: "vitrine/dashboard", element: <Dashboard /> },
      { path: "commerce/dashboard", element: <Dashboard /> },
      { path: "radioh", element: <ChannelListView /> },
      { path: "radiod", element: <ChannelListView /> },
      { path: "radioa", element: <ChannelListView /> },
      { path: "radioe", element: <ChannelListView /> },
      { path: "radiop", element: <ChannelListView /> },
      { path: "conta", element: <AccountView /> },
      { path: "vitrine/produtos", element: <ProductListView /> },
      { path: "commerce/produtos", element: <ProductListView /> },
      { path: "commerce/vendas", element: <SalesListView /> },
      { path: "vitrine/configuracoes", element: <SettingsView /> },
      { path: "commerce/configuracoes", element: <SettingsView /> },
      { path: "vitrine", element: <VitrineView /> },
      { path: "commerce", element: <CommerceLink /> },
      { path: "content", element: <Content /> },
      { path: "offer", element: <Offer /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "radio",
    element: <DashboardLayout />,
    children: [
      { path: "dashboard", element: <Dashboard /> },
      { path: "home", element: <ChannelListView /> },
      { path: "acesso", element: <AccessView /> },
      { path: "funcionario", element: <EmployeeView /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "admin",
    element: <MainLayout />,
    children: [
      { path: "login", element: <LoginViewADM /> },
      { path: "/", element: <Navigate to="/admin/login" /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  ,
  {
    path: "app-admin",
    element: <DashboardLayout />,
    children: [
      //Administração do Sistema
      { path: "dashboard", element: <DashboardADM /> },
      { path: "radio", element: <ChannelViewADM /> },
      { path: "lojas", element: <BusinessListViewADM /> },
      { path: "conta", element: <AccountViewADM /> },
      { path: "configuracoes", element: <SettingsViewADM /> },
      { path: "grupo-conteudo", element: <ContentGroupViewADM /> },
      { path: "tipo-conteudo", element: <TypeContentViewADM /> },
      { path: "topico-genero", element: <GenreTopicListViewADM /> },
      { path: "topico-subgenero", element: <GenresubTopicListViewADM /> },
      { path: "tipo-vinheta", element: <TypeVignetteViewADM /> },
      { path: "sub-genero", element: <GenreSubViewADM /> },
      { path: "segmento", element: <SegmentViewADM /> },
      { path: "conteudo", element: <ContentViewADM /> },
      { path: "comerciais", element: <CommercialViewADM /> },
      { path: "vinhetas", element: <VignetteViewADM /> },
      { path: "genero", element: <GenreViewADM /> },
      { path: "musica", element: <MusicViewADM /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
];

const routesRepesentante = [
  {
    path: "/",
    element: <MainLayout />,
    children: [
      { path: "login", element: <LoginRepresentante /> },
      { path: "bomja-music", element: <Music /> },
      { path: "registro", element: <SignupRepresentante /> },
      { path: "recuperar-senha", element: <ForgotRepresentante /> },
      { path: "registro-concluido", element: <SignupCheckedRepresentante /> },
      {
        path: "recuperar-senha-concluido",
        element: <ForgotCheckedRepresentante />,
      },
      { path: "list-product", element: <CarouselProducts /> },
      {
        path: "list-product-utilitares",
        element: <CarouselProductsUtilitares />,
      },
      { path: "loja", element: <Commerce /> },
      { path: "404", element: <ErrorPage /> },
      { path: "/", element: <Navigate to="/login" /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "app",
    element: <DashboardLayout />,
    children: [
      { path: "dashboard", element: <Dashboard /> },
      { path: "radio/dashboard", element: <Dashboard /> },
      { path: "vitrine/dashboard", element: <Dashboard /> },
      { path: "commerce/dashboard", element: <Dashboard /> },
      { path: "radioh", element: <ChannelListView /> },
      { path: "radioa", element: <ChannelListView /> },
      { path: "radioe", element: <ChannelListView /> },
      { path: "radiop", element: <ChannelListView /> },
      { path: "conta", element: <AccountView /> },
      { path: "vitrine/produtos", element: <ProductListView /> },
      { path: "commerce/produtos", element: <ProductListView /> },
      { path: "vitrine/configuracoes", element: <SettingsView /> },
      { path: "commerce/configuracoes", element: <SettingsView /> },
      { path: "vitrine", element: <VitrineView /> },
      { path: "commerce", element: <CommerceLink /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "radio",
    element: <DashboardLayout />,
    children: [
      { path: "dashboard", element: <Dashboard /> },
      { path: "home", element: <ChannelListView /> },
      { path: "acesso", element: <AccessView /> },
      { path: "funcionario", element: <EmployeeView /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "admin",
    element: <MainLayout />,
    children: [
      { path: "login", element: <LoginViewADM /> },
      { path: "/", element: <Navigate to="/admin/login" /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  ,
  {
    path: "app-admin",
    element: <DashboardLayout />,
    children: [
      //Administração do Sistema
      { path: "dashboard", element: <DashboardADM /> },
      { path: "radio", element: <ChannelViewADM /> },
      { path: "lojas", element: <BusinessListViewADM /> },
      { path: "conta", element: <AccountViewADM /> },
      { path: "configuracoes", element: <SettingsViewADM /> },
      { path: "grupo-conteudo", element: <ContentGroupViewADM /> },
      { path: "tipo-conteudo", element: <TypeContentViewADM /> },
      { path: "tipo-vinheta", element: <TypeVignetteViewADM /> },
      { path: "sub-genero", element: <GenreSubViewADM /> },
      { path: "segmento", element: <SegmentViewADM /> },
      { path: "conteudo", element: <ContentViewADM /> },
      { path: "comerciais", element: <CommercialViewADM /> },
      { path: "vinhetas", element: <VignetteViewADM /> },
      { path: "genero", element: <GenreViewADM /> },
      { path: "musica", element: <MusicViewADM /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
];

export default routess;
