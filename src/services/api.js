import axios from "axios";
import { getToken } from "./auth";

const cors = "https://cors.dc3erpcloud.com.br/";

const api = axios.create({

  baseURL: "https://servidor.bomja.com.br/api", //secure
  //baseURL: "http://bomja.vps-kinghost.net:9090/api", //servidor pure
  //baseURL: "http://localhost:8080/api", //local
  //baseURL: "http://192.168.0.106:8080/api", //homologação
});

export const apiReceitaWs = axios.create({
  baseURL: cors + "https://www.receitaws.com.br/v1/cnpj/",
});

export const apiViaCepWs = axios.create({
  baseURL: cors + "https://viacep.com.br/ws",
});

api.interceptors.request.use(async (config) => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;
