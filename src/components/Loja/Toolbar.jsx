import React, { useEffect, useState } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Hidden,
  Box,
  InputBase,
  Button,
  Tooltip,
  Avatar,
  Divider,
  Badge,
  Card,
  Typography,
} from "@material-ui/core";
import {
  LocationOn as LocationOnIcon,
  SearchRounded as SearchRoundedIcon,
  HomeOutlined as HomeOutlinedIcon,
  ConfirmationNumberOutlined as ConfirmationNumberOutlinedIcon,
  DateRangeRounded as DateRangeRoundedIcon,
  Loyalty as LoyaltyIcon,
  ShoppingCartOutlined as ShoppingCartIcon,
  ExpandMore as ExpandMoreIcon,
  AccountCircle as AccountCircleIcon,
} from "@material-ui/icons";
import { useCookies } from "react-cookie";

const MenuBar = ({
  SearchProduct,
  isVisibleToolbar,
  openCarSales,
  openLocation,
  onSignUp,
  onLogin,
  onMyCont,
  isActiveUser,
  selectAddress,
  nameBusiness,
}) => {
  const [itemsCarsSales, setItemsCarsSales] = useCookies(["ItemsCarsSales"]);
  const [qtdItems, setQtdItems] = useState(0);
  const [isMouseEnter, setIsMouseEnter] = useState({ name: "" });

  const titleize = (text) => {
    var words = text.toLowerCase().split(" ");
    for (var a = 0; a < words.length; a++) {
      var w = words[a];
      words[a] = w[0].toUpperCase() + w.slice(1);
    }
    return words.join(" ");
  };

  useEffect(() => {
    setQtdItems(
      itemsCarsSales.ItemsCarsSales ? itemsCarsSales.ItemsCarsSales.length : 0
    );
  }, [itemsCarsSales]);

  const styleButtunOption = {
    borderRadius: 10,
    textTransform: "none",
    marginRight: 10,
    fontWeight: "bold",
    height: 40,
  };

  const optionNamesButtom = [
    { name: "Inicio", icon: <HomeOutlinedIcon /> },
    { name: "Promoções", icon: <ConfirmationNumberOutlinedIcon /> },
  ];

  return (
    <div
      style={{ backgroundColor: "#fcfcfc", zIndex: 10000 }}
      className="w-100 mb-4"
    >
      <Hidden smUp>
        <div className={isVisibleToolbar ? `fixed-top` : {}}>
          <AppBar elevation={1} position="relative">
            {isVisibleToolbar ? (
              <div style={{ backgroundColor: "white" }} className="p-2">
                <Box
                  style={{
                    backgroundColor: "white",
                    borderRadius: 20,
                    padding: 8,
                    width: "100%",
                    marginTop: 15,
                    boxShadow: "1px 1px 4px rgba(0,0,0,0.2)",
                  }}
                  display="flex"
                  justifyContent="flex-start"
                  alignItems="center"
                >
                  <SearchRoundedIcon
                    style={{ color: "silver" }}
                    className="mr-2"
                  />
                  <InputBase
                    onChange={(event) => {
                      SearchProduct(event.target.value);
                    }}
                    placeholder="Buscar qualquer produto em oferta"
                    className="w-100"
                  />
                </Box>

                <Box
                  className="mt-2"
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Avatar
                    style={{
                      height: 70,
                      width: 70,
                    }}
                    src={
                      "https://img.sitemercado.com.br/redes/e6fc91884a715649e8a9c7698a831e57.png"
                    }
                    variant="square"
                  />
                  <Box
                    className="mt-2"
                    display="flex"
                    justifyContent="flex-start"
                    alignItems="center"
                  >
                    <div>
                      <h6 style={{ color: "silver" }} className="text-black">
                        Está Aberto •
                      </h6>
                      <h6 style={{ color: "silver" }} className="text-black">
                        Aproveite
                      </h6>
                    </div>
                    <DateRangeRoundedIcon
                      style={{ color: "silver" }}
                      className="ml-2"
                    />
                  </Box>
                </Box>
              </div>
            ) : (
              <Toolbar
                style={{ backgroundColor: "white" }}
                className="justify-content-between"
              >
                <h3 className="text-black">{titleize(nameBusiness)}</h3>
                <div className="d-flex align-items-center">
                <Button
                  variant="contained"
                  size="small"
                  style={{
                    color: "white",
                    backgroundColor: "#2ecc71",
                    borderRadius: 10,
                  }}
                  startIcon={<ShoppingCartIcon fontSize="small" />}
                  onClick={openCarSales}
                >
                  {qtdItems}
                </Button>

                  <Divider
                    className="ml-4 mr-4"
                    flexItem
                    orientation="vertical"
                    style={{ backgroundColor: "silver" }}
                  />

                {isActiveUser && (
                  <IconButton
                    onClick={onMyCont}
                  >
                    <AccountCircleIcon
                      style={{
                        width: 35,
                        height: 35,
                      }}
                    />
                  </IconButton>
                )}

                {!isActiveUser && (
                  <Button
                    onClick={() => {
                      onLogin();
                    }}
                    variant="contained"
                    size="small"
                    style={{
                      backgroundColor: "#ff3838",
                      color: "white",
                      fontWeight: "bold",
                      textTransform: "none",
                      borderRadius: 10,
                    }}
                  >
                    Entrar
                  </Button>
                )}
                </div>
              </Toolbar>
            )}
          </AppBar>
        </div>
      </Hidden>

      <Hidden xsDown>
        <div className={"fixed-top"}>
          <AppBar
            position="relative"
            elevation={0}
            style={{ height: isVisibleToolbar ? 140 : 70 }}
          >
            <Toolbar
              style={{ backgroundColor: "white", height: 70 }}
              className="justify-content-between"
            >
              <h3 className="text-black">{titleize(nameBusiness)}</h3>
              <div className="d-flex align-items-center">
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                  width={230}
                  mr={4}
                  style={{
                    cursor: "pointer",
                  }}
                >
                  <Box
                    display="flex"
                    alignItems="center"
                    style={{ cursor: "pointer" }}
                    onClick={openLocation}
                  >
                    <LocationOnIcon
                      style={{ color: "#ff3838", marginRight: 10 }}
                    />
                    <Typography
                      variant="body1"
                      style={{ fontWeight: "bolder", color: "black" }}
                    >
                      {selectAddress
                        ? `${selectAddress.publicPlace} ${selectAddress.number}`
                        : "Nome Rua, cidade .."}
                    </Typography>
                  </Box>

                  <ExpandMoreIcon
                    fontSize="small"
                    style={{ color: "silver" }}
                  />
                </Box>

                <Button
                  variant="contained"
                  size="small"
                  style={{
                    color: "white",
                    backgroundColor: "#2ecc71",
                    borderRadius: 10,
                  }}
                  startIcon={<ShoppingCartIcon fontSize="small" />}
                  onClick={openCarSales}
                >
                  {qtdItems}
                </Button>

                <Divider
                  className="ml-4 mr-4"
                  flexItem
                  orientation="vertical"
                  style={{ backgroundColor: "silver" }}
                />

                {!isActiveUser && (
                  <Button
                    onClick={() => {
                      onLogin();
                    }}
                    variant="contained"
                    size="small"
                    style={{
                      backgroundColor: "#ff3838",
                      color: "white",
                      fontWeight: "bold",
                      textTransform: "none",
                      borderRadius: 10,
                    }}
                  >
                    Entrar
                  </Button>
                )}

                {!isActiveUser && (
                  <Button
                    className="ml-2"
                    variant="contained"
                    size="small"
                    style={{
                      backgroundColor: "#2ecc71",
                      borderRadius: 10,
                    }}
                    onClick={() => {
                      onSignUp();
                    }}
                  >
                    <Typography
                      variant="button"
                      style={{
                        color: "white",
                        fontWeight: "bold",
                        textTransform: "none",
                        marginLeft: 5,
                        marginRight: 5,
                      }}
                    >
                      Inscrever-se
                    </Typography>
                  </Button>
                )}
                {isActiveUser && (
                  <IconButton
                    onClick={onMyCont}
                    style={{
                      marginLeft: 20,
                    }}
                  >
                    <AccountCircleIcon
                      style={{
                        width: 35,
                        height: 35,
                      }}
                    />
                  </IconButton>
                )}
              </div>
            </Toolbar>

            <Divider style={{ backgroundColor: "#dfe4ea" }} />

            {isVisibleToolbar && (
              <Toolbar
                style={{ backgroundColor: "white", height: 140 }}
                className="justify-content-between p-4 mt--2"
              >
                <Box
                  mt={4}
                  mb={4}
                  display="flex"
                  alignItems="center"
                  height={140}
                >
                  <Avatar
                    style={{
                      height: 70,
                      width: 70,
                    }}
                    src={
                      "https://img.sitemercado.com.br/redes/e6fc91884a715649e8a9c7698a831e57.png"
                    }
                    variant="square"
                  />
                  <Box display="flex" ml={4} height={20} alignItems="center">
                    {optionNamesButtom.map((options) => (
                      <Button
                        startIcon={options.icon}
                        onMouseEnter={() => {
                          setIsMouseEnter({ name: options.name });
                        }}
                        onMouseLeave={() => {
                          setIsMouseEnter({ name: "" });
                        }}
                        style={{
                          ...styleButtunOption,
                          backgroundColor:
                            isMouseEnter.name == options.name
                              ? "#3d3d3d"
                              : "white",
                          color:
                            isMouseEnter.name == options.name
                              ? "white"
                              : "#3d3d3d",
                        }}
                        variant="contained"
                      >
                        {options.name}
                      </Button>
                    ))}
                  </Box>
                </Box>

                <Box
                  style={{
                    backgroundColor: "white",
                    borderRadius: 20,
                    padding: 8,
                    width: "50%",
                    boxShadow: "1px 1px 4px rgba(0,0,0,0.2)",
                  }}
                  display="flex"
                  justifyContent="flex-start"
                  alignItems="center"
                >
                  <SearchRoundedIcon
                    style={{ color: "silver" }}
                    className="mr-2"
                  />
                  <InputBase
                    onChange={(event) => {
                      SearchProduct(event.target.value);
                    }}
                    placeholder="Buscar qualquer produto"
                    className="w-100"
                  />
                </Box>

                <Box
                  className=""
                  display="flex"
                  justifyContent="flex-start"
                  alignItems="center"
                >
                  <DateRangeRoundedIcon
                    style={{ color: "silver" }}
                    className="mr-2"
                  />
                  <div>
                    <h6 style={{ color: "silver" }} className="text-black">
                      Está Aberto •
                    </h6>
                    <h6 style={{ color: "silver" }} className="text-black">
                      Aproveite
                    </h6>
                  </div>
                </Box>
              </Toolbar>
            )}
          </AppBar>
        </div>

        <Box
          mb={4}
          display="flex"
          flexDirection="column"
          justifyContent="center"
        >
          <Card
            style={{
              marginLeft: "5%",
              marginRight: "5%",
              marginTop: "5%",
              borderBottomRightRadius: 20,
              borderBottomLeftRadius: 20,
              borderStyle: "solid",
              borderColor: "#ededed",
              borderWidth: 1,
            }}
          >
            <Box
              style={{ marginTop: 20, marginBottom: 50 }}
              className="h-100 w-100"
              display="flex"
              flexDirection="column"
              justifyContent="center"
            >
              <Box display="flex" justifyContent="center" alignItems="center">
                <img
                  height={150}
                  width={(150 * 3) / 4}
                  style={{ borderRadius: 10 }}
                  src="https://img.sitemercado.com.br/redes/e6fc91884a715649e8a9c7698a831e57.png"
                />
                <div className="ml-4">
                  <Box
                    className=""
                    display="flex"
                    justifyContent="flex-start"
                    alignItems="center"
                  >
                    <h3 className="text-black">{titleize(nameBusiness)}</h3>
                  </Box>
                  <Box
                    className="mt-2"
                    display="flex"
                    justifyContent="flex-start"
                    alignItems="center"
                  >
                    <DateRangeRoundedIcon
                      style={{ color: "black" }}
                      className="mr-2"
                    />
                    <h6 className="text-black">
                      Está Aberto • Aproveite as ofertas
                    </h6>
                  </Box>
                </div>
              </Box>
              <Box display="flex" justifyContent="center" alignItems="center">
                <Box
                  style={{
                    backgroundColor: "white",
                    borderRadius: 25,
                    padding: 12,
                    width: "50%",
                    marginTop: 15,
                    boxShadow: "1px 1px 4px rgba(0,0,0,0.3)",
                  }}
                  display="flex"
                  justifyContent="flex-start"
                  alignItems="center"
                >
                  <SearchRoundedIcon
                    style={{ color: "silver" }}
                    className="mr-2"
                  />
                  <InputBase
                    onChange={(event) => {
                      SearchProduct(event.target.value);
                    }}
                    placeholder="Buscar qualquer produto em oferta"
                    className="w-100"
                  />
                </Box>
              </Box>
            </Box>
          </Card>
        </Box>
      </Hidden>
    </div>
  );
};

export default MenuBar;
