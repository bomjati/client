import React, { useEffect, useState } from "react";
import {
  Modal,
  Backdrop,
  ClickAwayListener,
  Fade,
  Card,
  Box,
  IconButton,
  Grid,
  Avatar,
  InputBase,
  TextField,
  InputAdornment,
  Button,
  Divider,
  Typography
} from "@material-ui/core";
import {
  Close as CloseIcon,
  DeleteOutlineOutlined as DeleteIcon,
  Add as AddIcon,
  Remove as RemoveIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Favorite as FavoriteIcon,
  LocationOn as LocationOnIcon
} from "@material-ui/icons";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import api from "../../../services/api";
import { useCookies } from "react-cookie";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1)
      }
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3)
    },
    medium: {
      width: theme.spacing(6),
      height: theme.spacing(6)
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7)
    }
  })
);

const ModalLocation = ({ open, onCloseModalLocation }) => {
  const classes = useStyles();
  const [qtdItems, setQtdItems] = useState(0);
  const [valueItems, setvalueItems] = useState(0);

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)"
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={onCloseModalLocation}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            height="100%"
          >
            <Box display="flex" justifyContent="center" alignContent="center">
              <div
                style={{
                  width: 790,
                  backgroundColor: "white",
                  padding: 10,
                  borderRadius: 10
                }}
              >
                <Box display="flex" justifyContent="flex-end" mt={2} mr={2}>
                  <IconButton
                    onClick={onCloseModalLocation}
                    size="small"
                    style={{
                      backgroundColor: "#3d3d3d",
                      color: "white"
                    }}
                  >
                    <CloseIcon fontSize="small" />
                  </IconButton>
                </Box>
                <Typography
                  style={{
                    fontWeight: "bolder",
                    marginLeft: "5%",
                    color: "black"
                  }}
                  variant="h5"
                >
                  Onde você gostaria de receber seu pedido?
                </Typography>

                <Typography
                  style={{
                    marginLeft: "5%",
                    color: "black",
                    fontSize: 20,
                    marginTop: 35,
                    marginRight: "5%"
                  }}
                  variant="body1"
                >
                  Insira seu endereço para conferir os produtos oferecidos da sua área
                </Typography>

                <Box display="flex" justifyContent="space-between">
                  <div />
                  <TextField
                    variant="outlined"
                    fullWidth
                    margin="normal"
                    style={{
                      marginLeft: 35,
                      marginRight: 35,
                      marginTop: 25,
                      float: "right",
                      
                    }}
                    InputProps={{
                      style: {
                        borderRadius: 10,
                        padding: 0
                      },
                      startAdornment: (
                        <InputAdornment position="start">
                          <LocationOnIcon fontSize="small" style={{ color: "silver", marginLeft: 10, marginRight: 10 }} />
                        </InputAdornment>
                      )
                    }}
                  />
                  <div />
                </Box>

                <Box display="flex" justifyContent="space-between">
                  <div />
                  <Button
                    variant="contained"
                    size="large"
                    style={{
                      marginTop: 15,
                      marginBottom: 60,
                      borderRadius: 10,
                      textTransform: "none",
                      width: "50%",
                      color: "white",
                      fontWeight: "bold",
                      backgroundColor: "#2ecc71",
                    }}
                  >
                    Buscar
                  </Button>
                  <div />
                </Box>
              </div>
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalLocation;
