import React, { useEffect, useState } from "react";
import {
  Modal,
  Backdrop,
  ClickAwayListener,
  Fade,
  Card,
  Box,
  IconButton,
  Grid,
  Avatar,
  InputBase,
  TextField,
  InputAdornment,
  Button,
  Divider,
  Typography,
} from "@material-ui/core";
import {
  Close as CloseIcon,
  DeleteOutlineOutlined as DeleteIcon,
  Add as AddIcon,
  Remove as RemoveIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Favorite as FavoriteIcon,
  LocationOn as LocationOnIcon,
} from "@material-ui/icons";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import api from "../../../services/api";
import { useCookies } from "react-cookie";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    medium: {
      width: theme.spacing(6),
      height: theme.spacing(6),
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
  })
);

const ModalClonclued = ({ open, onCloseModalConclued }) => {
  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={onCloseModalConclued}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            height="100%"
          >
            <Box
              style={{
                backgroundColor: "white",
              }}
              display="flex"
              justifyContent="center"
              alignContent="center"
            >
              <div>
                <Typography
                  variant="h3"
                  style={{
                    fontWeight: "bold",
                    width: 350,
                    marginTop: 50,
                    marginBottom: 50,
                  }}
                >
                  Compra efetuada com sucesso!
                </Typography>
                <Box display="flex" justifyContent="space-between">
                  <div />
                  <Button
                    onClick={onCloseModalConclued}
                    fullWidth
                    style={{
                      marginBottom: 50,
                      backgroundColor: "#ff3838",
                      color: "white",
                      borderRadius: 20,
                    }}
                    variant="contained"
                    size="large"
                  >
                    Continuar comprando
                  </Button>
                  <div />
                </Box>
              </div>
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalClonclued;
