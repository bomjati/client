import React, { useEffect, useState } from "react";
import {
  Modal,
  Backdrop,
  ClickAwayListener,
  Fade,
  Card,
  Box,
  IconButton,
  Grid,
  Avatar,
  InputBase,
  TextField,
  InputAdornment,
  Button,
  Divider,
  Typography,
} from "@material-ui/core";
import {
  Close as CloseIcon,
  CheckCircle as CheckCircleIcon,
  Delete as DeleteIcon,
} from "@material-ui/icons";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import api from "../../../services/api";
import { showMessage } from "../utils/message";
import MessageConfirm from "../utils/MessageConfirm";
import { useCookies } from "react-cookie";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    medium: {
      width: theme.spacing(6),
      height: theme.spacing(6),
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
  })
);

const ModalCredCard = ({
  open,
  onCloseModalCredCard,
  onSelectCredCard,
  isActiveUser,
  userInfo,
}) => {
  const classes = useStyles();
  const [qtdItems, setQtdItems] = useState(0);
  const [valueItems, setvalueItems] = useState(0);
  const [indexSelect, setIndexSelect] = useState(0);
  const [listCredCard, setListCredCard] = useState();

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  const listTestLocation = [
    { end: "Rua Duque de Caxias - Centro", name: "Casa" },
    { end: "Av. Souza Naves - Centro", name: "Casa 2" },
  ];

  useEffect(() => {
    onGetCredCard();
  }, []);

  const onGetCredCard = async () => {
    if (!isActiveUser) {
      return;
    }

    const res = await api.get("/credcard").catch((error) => {
      showMessage(
        "Ops Falhou",
        "Complicações no servidor, tente mais tarde ",
        "danger"
      );
      console.log(error);
    });

    if (res.data) {
      console.log(res.data)
      setListCredCard(res.data);
    }
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={onCloseModalCredCard}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            height="100%"
          >
            <Box display="flex" justifyContent="center" alignContent="center">
              <div
                style={{
                  width: 400,
                  height: "100%",
                  backgroundColor: "white",
                  padding: 10,
                  borderRadius: 10,
                }}
              >
                <Box display="flex" justifyContent="flex-end" mt={2} mr={2}>
                  <IconButton
                    onClick={onCloseModalCredCard}
                    size="small"
                    style={{
                      backgroundColor: "#3d3d3d",
                      color: "white",
                    }}
                  >
                    <CloseIcon fontSize="small" />
                  </IconButton>
                </Box>
                <Typography
                  style={{
                    marginLeft: "5%",
                  }}
                  variant="body1"
                >
                  Olá, {userInfo && userInfo.name},
                </Typography>
                <Typography
                  style={{
                    marginLeft: "5%",
                  }}
                  variant="body1"
                >
                  Qual forma de pagamento vai utilizar hoje?
                </Typography>

                <Typography
                  style={{
                    marginTop: 25,
                    marginBottom: 10,
                    marginLeft: "5%",
                    color: "black",
                    fontWeight: "bold",
                  }}
                  variant="h6"
                >
                  Cartões salvos
                </Typography>
                <div style={{
                  height: 200
                }}>
                <PerfectScrollbar>
                  {listCredCard &&
                    listCredCard.map((testLocation) => (
                      <Card
                        onClick={() => {
                          setIndexSelect(testLocation.id);
                          onSelectCredCard(testLocation);
                        }}
                        className="p-3 ml-2 mt-2 w-90"
                        style={{
                          borderWidth: 1,
                          borderColor: "#e8e8e8",
                          borderStyle: "solid",
                          borderRadius: 15,
                        }}
                      >
                        <Box
                          display="flex"
                          justifyContent="space-between"
                          alignItems="center"
                        >
                          <Box display="flex" alignItems="center">
                            {!(indexSelect == testLocation.id) ? (
                              <div
                                style={{
                                  marginRight: 39,
                                }}
                              />
                            ) : (
                              <CheckCircleIcon
                                style={{
                                  color: "#2ecc71",
                                  marginRight: 15,
                                }}
                              />
                            )}
                            <div>
                              <Typography
                                variant="body2"
                                style={{ fontWeight: "bold", color: "black" }}
                              >
                                {String(testLocation.name).length > 30
                                  ? String(testLocation.name).substr(30) + "..."
                                  : testLocation.name}
                              </Typography>
                              <Box display="flex" alignItems="center">
                                <div
                                  style={{
                                    width: 5,
                                    height: 5,
                                    borderRadius: 5,
                                    marginRight: 10,
                                    backgroundColor: "silver",
                                  }}
                                />
                                <Typography variant="body2">
                                  **** **** ****{" "}
                                  {String(testLocation.number).substr(-4)}
                                </Typography>
                              </Box>
                            </div>
                          </Box>
                          <IconButton
                            size="small"
                            onClick={(event) => {
                              event.stopPropagation();
                            }}
                          >
                            <DeleteIcon
                              fontSize="small"
                              style={{
                                color: "red",
                              }}
                            />
                          </IconButton>
                        </Box>
                      </Card>
                    ))}
                </PerfectScrollbar>
                </div>

                <Box display="flex" justifyContent="space-between">
                  <div />
                  <Button
                    variant="contained"
                    size="large"
                    fullWidth
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      borderRadius: 10,
                      textTransform: "none",
                      color: "white",
                      fontWeight: "bold",
                      backgroundColor: "#2ecc71",
                    }}
                  >
                    Adicionar novo cartão
                  </Button>
                  <div />
                </Box>
                <Box display="flex" justifyContent="space-between">
                  <div />
                  <Button
                    variant="contained"
                    size="large"
                    onClick={onCloseModalCredCard}
                    fullWidth
                    style={{
                      marginBottom: 40,
                      borderRadius: 10,
                      textTransform: "none",
                      color: "black",
                      fontWeight: "bold",
                      backgroundColor: "white",
                    }}
                  >
                    Ok
                  </Button>
                  <div />
                </Box>
              </div>
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalCredCard;
