import React, { useEffect, useState } from "react";
import {
  Modal,
  Backdrop,
  ClickAwayListener,
  Fade,
  Card,
  Box,
  IconButton,
  Grid,
  Avatar,
  InputBase,
  Button,
  Divider,
} from "@material-ui/core";
import {
  Close as CloseIcon,
  DeleteOutlineOutlined as DeleteIcon,
  Add as AddIcon,
  Remove as RemoveIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Favorite as FavoriteIcon,
  SpeakerNotesOutlined as SpeakerNotesOutlinedIcon,
} from "@material-ui/icons";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import api from "../../../services/api";
import { useCookies } from "react-cookie";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    medium: {
      width: theme.spacing(6),
      height: theme.spacing(6),
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
  })
);

const ModalProduct = ({
  open,
  onCloseModalCarsSale,
  onCheckout,
  onOpenAddress,
  onOpenLogin,
  selectAddress,
  isActiveUser,
  nameBusiness,
}) => {
  const classes = useStyles();
  const [itemsCarsSales, setItemsCarsSales] = useCookies(["ItemsCarsSales"]);
  const [qtdItems, setQtdItems] = useState(0);
  const [valueItems, setvalueItems] = useState(0);

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  useEffect(() => {
    setQtdItems(
      itemsCarsSales.ItemsCarsSales ? itemsCarsSales.ItemsCarsSales.length : 0
    );
  }, [itemsCarsSales]);

  useEffect(() => {
    if (itemsCarsSales.ItemsCarsSales) {
      let vItems = 0;
      for (
        let index = 0;
        index < itemsCarsSales.ItemsCarsSales.length;
        index++
      ) {
        const element = itemsCarsSales.ItemsCarsSales[index];

        vItems =
          vItems +
          (element.promotion_price > 0
            ? element.promotion_price
            : element.price) *
            element.qtd;
      }

      setvalueItems(vItems);
    }
  }, [itemsCarsSales]);

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={onCloseModalCarsSale}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box display="flex" justifyContent="flex-end">
            <ClickAwayListener
              onClickAway={() => {
                onCloseModalCarsSale();
              }}
            >
              <div
                style={{
                  width: 500,
                  height: window.screen.height,
                  backgroundColor: "white",
                }}
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="space-between"
                  height={(window.screen.height * 92) / 100}
                >
                  <div style={{}}>
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                      ml={4}
                      mr={4}
                      mt={3}
                      mb={3}
                    >
                      <Box display="flex" alignItems="center">
                        <h6 className="mr-2" style={{ color: "#8395a7" }}>
                          Entregar para:
                        </h6>
                        <h5 style={{ color: "#2d3436" }}>
                          {selectAddress
                            ? `${selectAddress.publicPlace} ${selectAddress.number}`
                            : ""}
                        </h5>
                      </Box>
                      <IconButton
                        onClick={() => {
                          onCloseModalCarsSale();
                        }}
                        size="small"
                        style={{ backgroundColor: "black" }}
                      >
                        <CloseIcon
                          fontSize="small"
                          style={{ color: "white" }}
                        />
                      </IconButton>
                    </Box>
                    <Divider className="mt-1 mb-2" />
                    <Box
                      display="flex"
                      alignItems="center"
                      ml={4}
                      mr={4}
                      mt={2}
                      mb={2}
                    >
                      <Avatar
                        style={{ ...styleButtonIcon }}
                        className={classes.medium}
                        src="https://img.sitemercado.com.br/redes/e6fc91884a715649e8a9c7698a831e57.png"
                      />
                      <Box ml={2}>
                        <h6
                          className="mr-2"
                          style={{
                            fontSize: 18,
                            fontWeight: "bold",
                            color: "#2d3436",
                          }}
                        >
                          {nameBusiness}
                        </h6>
                        <p
                          onClick={() => {
                            onCloseModalCarsSale();
                          }}
                          style={{ color: "#2ecc71", cursor: "pointer" }}
                        >
                          Voltar para loja
                        </p>
                      </Box>
                    </Box>
                    <Divider className="mt-1 mb-2" />
                    {itemsCarsSales.ItemsCarsSales &&
                      itemsCarsSales.ItemsCarsSales.map((items) => {
                        return (
                          <Box
                            display="flex"
                            alignItems="center"
                            justifyContent="space-between"
                            ml={4}
                            mr={4}
                            mt={2}
                            mb={2}
                          >
                            <Box display="flex" alignItems="center">
                              <Avatar
                                variant="square"
                                className={classes.medium}
                                src={items.path_s3}
                              />
                              <Box ml={2}>
                                <h6
                                  className="mr-2"
                                  style={{
                                    color: "#2d3436",
                                  }}
                                >
                                  {String(items.name).substr(0, 26)}
                                  {String(items.name).length > 26 ? "..." : ""}
                                </h6>
                                <Box display="flex" alignItems="center">
                                  <p
                                    style={{
                                      color: "black",
                                      fontWeight: "500",
                                    }}
                                  >
                                    R${" "}
                                    {items.promotion_price > 0
                                      ? parseFloat(
                                          items.promotion_price
                                        ).toFixed(2)
                                      : parseFloat(items.price).toFixed(2)}
                                  </p>
                                  {items.promotion_price > 0 ? (
                                    <p
                                      className="ml-1"
                                      style={{
                                        color: "silver",
                                        textDecoration: "line-through",
                                      }}
                                    >
                                      R$ {parseFloat(items.price).toFixed(2)}
                                    </p>
                                  ) : null}
                                </Box>
                              </Box>
                            </Box>
                            <Box
                              display="flex"
                              justifyContent="space-between"
                              alignItems="center"
                              style={{
                                ...styleButtonIcon,
                                width: 100,
                                borderRadius: 10,
                              }}
                            >
                              {itemsCarsSales.ItemsCarsSales.filter((i) => {
                                return i.id == items.id;
                              })[0].qtd == 1 ? (
                                <IconButton
                                  onClick={(event) => {
                                    event.stopPropagation();
                                    let data =
                                      itemsCarsSales.ItemsCarsSales.filter(
                                        (i) => {
                                          return i.id != items.id;
                                        }
                                      );

                                    setItemsCarsSales("ItemsCarsSales", data, {
                                      path: "/",
                                    });
                                  }}
                                  size="small"
                                >
                                  <DeleteIcon
                                    style={{
                                      color: "#2d3436",
                                    }}
                                  />
                                </IconButton>
                              ) : (
                                <IconButton
                                  onClick={(event) => {
                                    event.stopPropagation();
                                    let data =
                                      itemsCarsSales.ItemsCarsSales.filter(
                                        (i) => {
                                          return i.id != items.id;
                                        }
                                      );

                                    let dataNow =
                                      itemsCarsSales.ItemsCarsSales.filter(
                                        (i) => {
                                          return i.id == items.id;
                                        }
                                      )[0];

                                    data.push({
                                      ...items,
                                      qtd: dataNow.qtd - 1,
                                    });

                                    setItemsCarsSales("ItemsCarsSales", data, {
                                      path: "/",
                                    });
                                  }}
                                  size="small"
                                >
                                  <RemoveIcon
                                    style={{
                                      color: "#2d3436",
                                    }}
                                  />
                                </IconButton>
                              )}

                              <h6 style={{ fontWeight: "bold" }}>
                                {
                                  itemsCarsSales.ItemsCarsSales.filter((i) => {
                                    return i.id == items.id;
                                  })[0].qtd
                                }{" "}
                                U
                              </h6>

                              <IconButton
                                onClick={(event) => {
                                  event.stopPropagation();
                                  let data =
                                    itemsCarsSales.ItemsCarsSales.filter(
                                      (i) => {
                                        return i.id != items.id;
                                      }
                                    );
                                  let dataNow =
                                    itemsCarsSales.ItemsCarsSales.filter(
                                      (i) => {
                                        return i.id == items.id;
                                      }
                                    )[0];

                                  data.push({
                                    ...items,
                                    qtd: dataNow.qtd + 1,
                                  });

                                  setItemsCarsSales("ItemsCarsSales", data, {
                                    path: "/",
                                  });
                                }}
                                size="small"
                              >
                                <AddIcon
                                  style={{
                                    color: "#2d3436",
                                  }}
                                />
                              </IconButton>
                            </Box>
                          </Box>
                        );
                      })}
                  </div>
                  <div>
                    <Divider className="mt-4 mb-2" />
                    <Box display="flex" pl={2} pr={2} mb={2}>
                      <Button
                        onClick={() => {
                          setItemsCarsSales("ItemsCarsSales", [], {
                            path: "/",
                          });
                          onCloseModalCarsSale();
                        }}
                        className="mr-1"
                        variant="contained"
                        fullWidth
                        style={{
                          backgroundColor: "white",
                          color: "#7f8c8d",
                          textTransform: "none",
                          fontWeight: "bold",
                          borderRadius: 10,
                          height: 50,
                        }}
                      >
                        Esvaziar Carrinho
                      </Button>
                      <div
                        onClick={() => {
                          selectAddress
                            ? valueItems > 0
                              ? !isActiveUser
                                ? onOpenLogin()
                                : onCheckout()
                              : alert("Selecione um item antes de proceguir")
                            : onOpenAddress();
                        }}
                        className="ml-1"
                        variant="contained"
                        style={{
                          backgroundColor: "#2ecc71",
                          textTransform: "none",
                          fontWeight: "bold",
                          height: 50,
                          borderRadius: 10,
                          width: "100%",
                          justifyContent: "center",
                          padding: 13,
                          cursor: "pointer",
                        }}
                      >
                        <Box
                          alignItems="center"
                          display="flex"
                          justifyContent="space-between"
                          style={{ width: "100%" }}
                        >
                          <div
                            style={{
                              backgroundColor: "black",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              width: 25,
                              height: 25,
                              borderRadius: 5,
                            }}
                          >
                            <h6 style={{ color: "white", fontWeight: "bold" }}>
                              {qtdItems}
                            </h6>
                          </div>
                          <h6
                            style={{
                              color: "white",
                              fontWeight: "bold",
                            }}
                          >
                            Pagar
                          </h6>
                          <p style={{ color: "white" }}>
                            R$ {parseFloat(valueItems).toFixed(2)}
                          </p>
                        </Box>
                      </div>
                    </Box>
                  </div>
                </Box>
              </div>
            </ClickAwayListener>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalProduct;
