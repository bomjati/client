import React, { useEffect, useState } from "react";
import {
  Modal,
  Backdrop,
  Fade,
  Card,
  Box,
  IconButton,
  Grid,
  Avatar,
  InputBase,
  Button,
} from "@material-ui/core";
import {
  Close as CloseIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Favorite as FavoriteIcon,
  SpeakerNotesOutlined as SpeakerNotesOutlinedIcon,
} from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import api from "../../../services/api";

const useStyles = makeStyles(({ breakpoints, spacing }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "flex-start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
    overflow: "scroll",
  },
  small: {
    width: spacing(4),
    height: spacing(4),
    marginRight: 10,
  },
  card: {
    [breakpoints.up("xs")]: {
      marginLeft: "15%",
      marginRight: "10%",
      marginTop: "10%",
      marginTop: "5%",
    },
    [breakpoints.up("lg")]: {
      marginLeft: "1%",
      marginRight: "1%",
      marginTop: "1%",
      marginBottom: "1%",
    },
  },
  img: {
    marginLeft: "20%",
  },
  viewImg: {
    height: 500,
    width: 400,
  },
}));

const ModalProduct = ({ productSelected, open, onCloseInfoProduct }) => {
  const classes = useStyles();
  const [favorited, setFavorited] = useState(false);
  const [countFavorited, setCountFavorited] = useState(0);
  const [countComment, setCountComment] = useState(0);
  const [infoComments, setInfoComments] = useState(null);
  const [comment, setComment] = useState("");

  useEffect(() => {
    console.log();
    // setCountFavorited(productSelected ? productSelected.like : 0)
    //  setCountComment(productSelected ? productSelected.comment : 0)
  }, [productSelected]);

  useEffect(() => {
    productSelected && getInfoDetail();
  }, [productSelected]);

  const getInfoDetail = () => {
    api
      .get(`/product/info/${productSelected.id}`)
      .then((resp) => {
        if (resp.data) {
          console.log(/*resp.data[0].comment*/);
          setInfoComments(/*resp.data[0].comment*/);
        }
      })
      .catch((error) => {});
  };

  const onFavorite = () => {
    setFavorited(!favorited);
    setCountFavorited(favorited ? countFavorited - 1 : countFavorited + 1);

    return;

    const dados = {
      like: comment,
    };

    api
      .post(`/like/${productSelected.product.id}`, dados)
      .then((resp) => {
        if (resp.data) {
          console.log(resp);
          getInfoDetail();
          setComment("");
        }
      })
      .catch((error) => {});
  };

  const onComment = () => {
    if (comment == "") {
      return;
    }

    const dados = {
      comment: comment,
    };

    api
      .post(`/comment/${productSelected.id}`, dados)
      .then((resp) => {
        if (resp.data) {
          console.log(resp);
          getInfoDetail();
          setComment("");
        }
      })
      .catch((error) => {});
  };

  return (
    <div>
      {productSelected && (
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={onCloseInfoProduct}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <div className={classes.card}>
              <Box
                display="flex"
                justifyContent="flex-end"
                className="w-100 mt-2 mb-2 mr-4"
                p={0}
              >
                <IconButton
                  style={{ backgroundColor: "white" }}
                  onClick={onCloseInfoProduct}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Card style={{ borderRadius: 20 }} className="p-4">
                <Grid container spacing={3}>
                  <Grid item lg={6} md={6} xs={12}>
                    <img
                      height={250}
                      width={(200 * 3) / 4}
                      src={
                        productSelected.path_s3
                          ? productSelected.path_s3
                          : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000"
                      }
                      className={classes.img}
                    />
                  </Grid>
                  <Grid item lg={6} md={6} xs={12}>
                    <h2
                      style={{
                        fontSize: 30,
                        color: "#1e272e",
                        paddingRight: 50,
                      }}
                      className="text-black"
                    >
                      {productSelected.name}
                    </h2>
                    <h5
                      style={{ color: "silver", fontWeight: "400" }}
                      className="text-black mt-2"
                    >
                      {productSelected.detail}
                    </h5>

                    <Box className="mt-3" display="flex" alignItems="flex-end">
                      <h2
                        style={{
                          fontWeight: "600",
                          fontSize: 25,
                          color: "#1e272e",
                        }}
                        className="text-black mr-2"
                      >
                        R${" "}
                        {parseFloat(productSelected.promotion_price)
                          .toFixed(2)
                          .replace(".", ",")}
                      </h2>
                      <h6
                        style={{
                          color: "#2f3640",
                          textDecoration: "line-through",
                          fontWeight: "600",
                          marginBottom: 5,
                        }}
                        className="text-black"
                      >
                        R${" "}
                        {parseFloat(productSelected.price)
                          .toFixed(2)
                          .replace(".", ",")}
                      </h6>
                    </Box>
                    <Box className="mt-1" display="flex" alignItems="center">
                      <div
                        style={{
                          backgroundColor: "#ff793f",
                          borderRadius: 20,
                          paddingLeft: 10,
                          paddingRight: 10,
                        }}
                      >
                        <p
                          style={{
                            fontWeight: "bold",
                            fontSize: 12,
                            color: "white",
                          }}
                        >
                          {(
                            ((productSelected.promotion_price -
                              productSelected.price) /
                              productSelected.price) *
                            100
                          ).toFixed(2)}
                          %
                        </p>
                      </div>

                      <h6
                        style={{
                          color: "#485460",
                          fontWeight: "600",
                          fontSize: 12,
                        }}
                        className="text-black ml-2"
                      >
                        Sua economia é de R${" "}
                        {parseFloat(
                          productSelected.price -
                            productSelected.promotion_price
                        )
                          .toFixed(2)
                          .replace(".", ",")}
                      </h6>
                    </Box>

                    <Card className="mt-3" style={{ borderRadius: 30 }}>
                      <Box display="flex" alignItems="center">
                        <IconButton
                          style={{ backgroundColor: "white" }}
                          onClick={onFavorite}
                        >
                          {favorited ? (
                            <FavoriteIcon style={{ color: "#e74c3c" }} />
                          ) : (
                            <FavoriteBorderIcon style={{ color: "black" }} />
                          )}
                        </IconButton>

                        <h6
                          style={{
                            color: "#485460",
                            fontWeight: "600",
                            fontSize: 12,
                          }}
                          className="text-black ml-1 mr-4"
                        >
                          {countFavorited}
                        </h6>

                        <SpeakerNotesOutlinedIcon />

                        <h6
                          style={{
                            color: "#485460",
                            fontWeight: "600",
                            fontSize: 12,
                          }}
                          className="text-black ml-4 mr-1"
                        >
                          {countComment}
                        </h6>
                      </Box>
                    </Card>

                    {(infoComments ? infoComments.length > 0 : false) && (
                      <Card className="mt-3 p-2" style={{ borderRadius: 15 }}>
                        {infoComments
                          .slice(
                            infoComments.length >= 3 ? -3 : -infoComments.length
                          )
                          .map((info) => (
                            <Box
                              className="mt-2"
                              display="flex"
                              alignItems="flex-start"
                            >
                              <Avatar className={classes.small} title="CM" />
                              <div
                                style={{
                                  backgroundColor: "#f5f6fa",
                                  borderRadius: 15,
                                }}
                                className="p-2"
                              >
                                <p
                                  style={{
                                    color: "#485460",
                                    fontWeight: "600",
                                    fontSize: 12,
                                  }}
                                  className="text-black"
                                >
                                  {"Fernando"}
                                </p>
                                <p
                                  style={{
                                    color: "#485460",
                                    fontSize: 12,
                                    marginTop: -10,
                                  }}
                                  className="text-black"
                                >
                                  {/*info.comment*/}
                                </p>
                              </div>
                            </Box>
                          ))}
                      </Card>
                    )}

                    <Box
                      className="mt-3"
                      display="flex"
                      alignItems="flex-start"
                    >
                      <Avatar className={classes.small} title="CM" />
                      <InputBase
                        style={{
                          backgroundColor: "#f5f6fa",
                          borderRadius: 15,
                          paddingRight: 10,
                          paddingLeft: 10,
                        }}
                        onChange={(event) => {
                          setComment(event.target.value);
                        }}
                        value={comment}
                        rowsMax={5}
                        multiline
                        className="w-100"
                        placeholder="Interaja com essa oferta. Comente Agora!"
                      />
                    </Box>
                    <Box
                      className="mt-2"
                      display="flex"
                      justifyContent="flex-end"
                    >
                      <div />
                      <Button
                        onClick={() => {
                          onComment();
                        }}
                        style={{
                          borderRadius: 20,
                          backgroundColor: "#ff3838",
                          color: "white",
                        }}
                        variant="contained"
                        size="small"
                      >
                        Comentar
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </Card>
            </div>
          </Fade>
        </Modal>
      )}
    </div>
  );
};

export default ModalProduct;
