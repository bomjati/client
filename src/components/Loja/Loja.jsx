import React, { useState, useEffect } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  Checkbox,
} from "@material-ui/core";
import {
  Close as CloseIcon,
  HomeOutlined as HomeOutlinedIcon,
  ConfirmationNumberOutlined as ConfirmationNumberOutlinedIcon,
  DeleteOutlineOutlined as DeleteIcon,
  Add as AddIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Remove as RemoveIcon,
  Favorite as FavoriteIcon,
  SpeakerNotesOutlined as SpeakerNotesOutlinedIcon,
} from "@material-ui/icons";
import Toolbar from "./Toolbar";
import ListProduct from "./Commerce";
import ModalProduct from "./Modal/ModalProduct";
import ModalCarsSales from "./Modal/ModalCarsSales";
import ModalLocation from "./Modal/ModalLocationList";
import ModalConclued from "./Modal/ModalConcluded";
import Signup from "./Register/SignUp";
import Login from "./Register/Login";
import MyCont from "./MyCont/MyCont";
import Checkout from "./Checkout/Checkout";
import api from "../../services/api";
import { getToken } from "../../services/auth";
import ScrollReact from "react-perfect-scrollbar";
import "react-perfect-scrollbar/dist/css/styles.css";
import { useParams } from "react-router-dom";

const CryptoJS = require("crypto-js");

function getParam(param) {
  return String(
    new URLSearchParams(window.location.search).get(param)
  ).replaceAll(" ", "+");
}

const Loja = () => {
  const { nameBusiness, idBusiness, idSegment, ecommerceId } = useParams();
  const [nameloja, setNameloja] = useState("a");
  const [nunPage, setNunPage] = useState(0);
  const [categorys, setCategorys] = useState([]);
  const [openInfoProduct, setOpenInfoProduct] = useState(false);
  const [openLocation, setOpenLocation] = useState(false);
  const [openConclued, setOpenConclued] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isMouseEnter, setIsMouseEnter] = useState({ name: "" });
  const [isVisibleToolbar, setIsVisibleToolbar] = useState(false);
  const [isModalCarsSale, setIsModalCarsSale] = useState(false);
  const [productSelected, setProductSelected] = useState(null);
  const [products, setProducts] = useState(null);
  const [productsBusca, setProductsBusca] = useState(null);
  const [scrollPosition, setSrollPosition] = useState(0);
  const [selectAddress, setSelectAddress] = useState();
  const [idCodificaded, setIdCodificaded] = useState(getParam("profile"));
  const isActiveUser = getToken() ? true : false;

  const titleize = (text) => {
    var words = text.toLowerCase().split(" ");
    for (var a = 0; a < words.length; a++) {
      var w = words[a];
      words[a] = w[0].toUpperCase() + w.slice(1);
    }
    return words.join(" ");
  };

  const styleButtunOption = {
    borderRadius: 10,
    textTransform: "none",
    marginRight: 10,
    fontWeight: "bold",
  };

  const optionNamesButtom = [
    { name: "Inicio", icon: <HomeOutlinedIcon /> },
    { name: "Promoções", icon: <ConfirmationNumberOutlinedIcon /> },
  ];

  useEffect(() => {
    setNameloja(titleize(String(nameBusiness).replaceAll("-", " ")));
    console.log(nameBusiness);
  }, [nameBusiness]);

  const getAllProduct = () => {
    api
      .get(`/product/${ecommerceId}`)
      .then((resp) => {
        if (resp.data) {
          console.log(resp.data);
          setProducts(resp.data);
          setProductsBusca(resp.data);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        console.log(`${window.location.origin}/404`);
        //window.location.assign(`${window.location.origin}/404`);
      });
  };

  const getAllGrupos = () => {
    setIsLoading(true);

    api
      .get(`/category`)
      .then((resp) => {
        if (resp.data) {
          console.log(resp.data);
          setCategorys(resp.data);
          //setCategorys(resp.data);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        console.log(`${window.location.origin}/404`);
        //window.location.assign(`${window.location.origin}/404`);
      });
  };

  const onViewProduct = (product) => {
    console.log(product);
    api
      .post(`/view/${product.id}`)
      .then((resp) => {
        if (resp.data) {
          console.log(resp);
          getAllProduct();
        }
      })
      .catch((error) => {});
  };

  const onOpenModalInfoProduct = () => {
    setOpenInfoProduct(true);
  };

  const onCloseModalInfoProduct = () => {
    setOpenInfoProduct(false);
  };

  const onSetProductSelected = (product) => {
    setProductSelected(product);
    onOpenModalInfoProduct();
  };

  const SearchProduct = (value) => {
    setProducts(
      productsBusca.filter(
        (p) =>
          String(p.name)
            .toLowerCase()
            .indexOf(String(value).toLowerCase()) > -1
      )
    );
  };

  const handleScroll = () => {
    const position = window.pageYOffset;
    setSrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    getAllGrupos();
    getAllProduct();
  }, []);

  useEffect(() => {
    scrollPosition > 70
      ? setIsVisibleToolbar(true)
      : setIsVisibleToolbar(false);
  }, [scrollPosition]);

  const onSelectAddress = (address) => {
    setSelectAddress(address);
  };

  const openCarSales = () => {
    setIsModalCarsSale(true);
  };

  const onCloseModalCarsSale = () => {
    setIsModalCarsSale(false);
  };

  const onCloseModalLocation = () => {
    setOpenLocation(false);
  };

  const onCloseModalConclued = () => {
    setOpenConclued(false);
  };

  return nunPage == 1 ? (
    <Signup
      onExit={() => {
        setNunPage(0);
      }}
      onOpenLogin={() => {
        setNunPage(2);
      }}
    />
  ) : nunPage == 2 ? (
    <Login
      onExit={() => {
        setNunPage(0);
      }}
      onSignUp={() => {
        setNunPage(1);
      }}
      onOpenHome={() => {
        setNunPage(0);
      }}
    />
  ) : nunPage == 3 ? (
    <MyCont
      onExit={() => {
        setNunPage(0);
      }}
      isActiveUser={isActiveUser}
    />
  ) : nunPage == 4 ? (
    <Checkout
      onExit={() => {
        setNunPage(0);
      }}
      nameBusiness={nameloja}
      onConclused={() => setOpenConclued(true)}
      ecommerceId={ecommerceId}
      isActiveUser={isActiveUser}
      onCloseModalCarsSale={onCloseModalCarsSale}
      selectAddress={selectAddress}
    />
  ) : nunPage == 0 ? (
    <div style={{ background: "#fcfcfc" }}>
      <Toolbar
        isActiveUser={isActiveUser}
        openCarSales={openCarSales}
        isVisibleToolbar={isVisibleToolbar}
        selectAddress={selectAddress}
        SearchProduct={SearchProduct}
        nameBusiness={nameloja}
        openLocation={() => {
          if (isActiveUser) {
            setOpenLocation(true);
          } else {
            setNunPage(2);
          }
        }}
        onSignUp={() => {
          setNunPage(1);
        }}
        onLogin={() => {
          setNunPage(2);
        }}
        onMyCont={() => {
          setNunPage(3);
        }}
      />

      {isVisibleToolbar ? null : (
        <Box display="flex" marginLeft={"5%"}>
          {optionNamesButtom.map((options) => (
            <Button
              startIcon={options.icon}
              onMouseEnter={() => {
                setIsMouseEnter({ name: options.name });
              }}
              onMouseLeave={() => {
                setIsMouseEnter({ name: "" });
              }}
              style={{
                ...styleButtunOption,
                backgroundColor:
                  isMouseEnter.name == options.name ? "#3d3d3d" : "white",
                color: isMouseEnter.name == options.name ? "white" : "#3d3d3d",
              }}
              variant="contained"
            >
              {options.name}
            </Button>
          ))}
        </Box>
      )}

      <ListProduct
        categorys={categorys}
        onOpenModalInfoProduct={onOpenModalInfoProduct}
        onSetProductSelected={onSetProductSelected}
        products={products}
        onViewProduct={onViewProduct}
        isLoading={isLoading}
      />

      <ModalProduct
        open={openInfoProduct}
        onCloseInfoProduct={onCloseModalInfoProduct}
        productSelected={productSelected}
      />

      <ModalCarsSales
        open={isModalCarsSale}
        nameBusiness={nameloja}
        onCloseModalCarsSale={onCloseModalCarsSale}
        isActiveUser={isActiveUser}
        onOpenLogin={() => {
          setNunPage(2);
        }}
        onOpenAddress={() => {
          if (isActiveUser) {
            setOpenLocation(true);
          } else {
            setNunPage(2);
          }
        }}
        onCheckout={() => {
          setNunPage(4);
        }}
        selectAddress={selectAddress}
      />

      <ModalLocation
        open={openLocation}
        onCloseModalLocation={onCloseModalLocation}
        onSelectAddress={onSelectAddress}
        isActiveUser={isActiveUser}
      />

      <ModalConclued
        open={openConclued}
        onCloseModalConclued={onCloseModalConclued}
      />
    </div>
  ) : (
    <div />
  );
};

export default Loja;
