export const data = [
    {
        "id": 1,
        "name": "Arroz",
        "price": "2.00",
        "promotion_price": "1.50",
        "detail": "teste",
        "date_start": "2020-10-19",
        "date_end": "2020-10-26",
        "time_start": "12:00:00",
        "time_end": "23:00:00",
        "user_id": 5,
        "product_category_id": 3,
        "created_at": "2020-10-22T18:00:33.000000Z",
        "updated_at": "2020-10-22T18:00:33.000000Z",
        "user": {
            "id": 5,
            "name": "Eduardo",
            "email": "dudumacen@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-10-22T11:55:02.000000Z",
            "updated_at": "2020-10-22T11:55:02.000000Z",
            "deleted_at": null
        },
        "product_img": {
            "id": 1,
            "nome_img": "221020180032.png",
            "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/produtos/Mercearia-em-geral/221020180032.png",
            "product_id": 1,
            "created_at": "2020-10-22T18:00:33.000000Z",
            "updated_at": "2020-10-22T18:00:33.000000Z"
        }
    },
    {
        "id": 3,
        "name": "Feijao Namorado S2.",
        "price": "50.00",
        "promotion_price": "30.00",
        "detail": "1kg",
        "date_start": "2020-10-23",
        "date_end": "2020-10-23",
        "time_start": "17:06:12",
        "time_end": "17:06:12",
        "user_id": 5,
        "product_category_id": 3,
        "created_at": "2020-10-22T20:29:09.000000Z",
        "updated_at": "2020-10-23T20:06:56.000000Z",
        "user": {
            "id": 5,
            "name": "Eduardo",
            "email": "dudumacen@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-10-22T11:55:02.000000Z",
            "updated_at": "2020-10-22T11:55:02.000000Z",
            "deleted_at": null
        },
        "product_img": {
            "id": 3,
            "nome_img": "231020185345.png",
            "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/produtos/Utilidades/231020185345.png",
            "product_id": 3,
            "created_at": "2020-10-22T20:29:09.000000Z",
            "updated_at": "2020-10-23T18:53:47.000000Z"
        }
    },
    {
        "id": 4,
        "name": "Guaraná Antártida",
        "price": "3.50",
        "promotion_price": "2.99",
        "detail": "300ml",
        "date_start": "2020-10-23",
        "date_end": "2020-10-23",
        "time_start": "17:23:45",
        "time_end": "17:23:45",
        "user_id": 5,
        "product_category_id": 5,
        "created_at": "2020-10-22T20:35:27.000000Z",
        "updated_at": "2020-10-23T20:24:30.000000Z",
        "user": {
            "id": 5,
            "name": "Eduardo",
            "email": "dudumacen@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-10-22T11:55:02.000000Z",
            "updated_at": "2020-10-22T11:55:02.000000Z",
            "deleted_at": null
        },
        "product_img": {
            "id": 4,
            "nome_img": "231020185541.png",
            "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/produtos/Utilidades/231020185541.png",
            "product_id": 4,
            "created_at": "2020-10-22T20:35:27.000000Z",
            "updated_at": "2020-10-23T18:55:43.000000Z"
        }
    },
    {
        "id": 5,
        "name": "Refrigerante Coca cola 2L",
        "price": "7.50",
        "promotion_price": "5.99",
        "detail": "2L",
        "date_start": "2020-10-23",
        "date_end": "2020-10-23",
        "time_start": "17:24:54",
        "time_end": "17:24:54",
        "user_id": 5,
        "product_category_id": 5,
        "created_at": "2020-10-22T20:50:32.000000Z",
        "updated_at": "2020-10-23T20:25:40.000000Z",
        "user": {
            "id": 5,
            "name": "Eduardo",
            "email": "dudumacen@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-10-22T11:55:02.000000Z",
            "updated_at": "2020-10-22T11:55:02.000000Z",
            "deleted_at": null
        },
        "product_img": {
            "id": 5,
            "nome_img": "231020185831.png",
            "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/produtos/Utilidades/231020185831.png",
            "product_id": 5,
            "created_at": "2020-10-22T20:50:32.000000Z",
            "updated_at": "2020-10-23T18:58:33.000000Z"
        }
    },
    {
        "id": 6,
        "name": "Fanta",
        "price": "6.50",
        "promotion_price": "4.99",
        "detail": "2L",
        "date_start": "2020-10-23",
        "date_end": "2020-10-23",
        "time_start": "17:28:07",
        "time_end": "17:28:07",
        "user_id": 5,
        "product_category_id": 5,
        "created_at": "2020-10-22T20:55:39.000000Z",
        "updated_at": "2020-10-23T20:28:52.000000Z",
        "user": {
            "id": 5,
            "name": "Eduardo",
            "email": "dudumacen@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-10-22T11:55:02.000000Z",
            "updated_at": "2020-10-22T11:55:02.000000Z",
            "deleted_at": null
        },
        "product_img": {
            "id": 6,
            "nome_img": "231020190022.png",
            "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/produtos/Utilidades/231020190022.png",
            "product_id": 6,
            "created_at": "2020-10-22T20:55:39.000000Z",
            "updated_at": "2020-10-23T19:00:24.000000Z"
        }
    },
    {
        "id": 7,
        "name": "Peps",
        "price": "6.52",
        "promotion_price": "4.99",
        "detail": "2L",
        "date_start": "2020-10-23",
        "date_end": "2020-10-23",
        "time_start": "17:35:40",
        "time_end": "17:35:40",
        "user_id": 5,
        "product_category_id": 1,
        "created_at": "2020-10-22T20:59:23.000000Z",
        "updated_at": "2020-10-23T20:36:29.000000Z",
        "user": {
            "id": 5,
            "name": "Eduardo",
            "email": "dudumacen@gmail.com",
            "email_verified_at": null,
            "created_at": "2020-10-22T11:55:02.000000Z",
            "updated_at": "2020-10-22T11:55:02.000000Z",
            "deleted_at": null
        },
        "product_img": {
            "id": 7,
            "nome_img": "231020185902.png",
            "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/produtos/Utilidades/231020185902.png",
            "product_id": 7,
            "created_at": "2020-10-22T20:59:23.000000Z",
            "updated_at": "2020-10-23T18:59:04.000000Z"
        }
    }
]