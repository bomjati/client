import React, { useState, useEffect } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
  Avatar
} from "@material-ui/core";
import {
  Settings as SettingsIcon,
  AccountBalanceWallet as AccountBalanceWalletIcon,
  AddLocation as AddLocationIcon,
  Delete as DeleteIcon,
  Add as AddIcon
} from "@material-ui/icons";
import api from "../../../services/api";
import { showMessage } from "../utils/message";
import MessageConfirm from "../utils/MessageConfirm";

export default function Cont({ isActiveUser }) {
  const [viewNewLocation, setViewNewLocation] = useState(false);
  const [isMessageConfirm, setIsMessageConfirm] = useState(false);
  const [addressId, setAddressId] = useState(0);
  const [listAddress, setListAddress] = useState();
  const [infoAddress, setInfoAddress] = useState({
    publicPlace: "",
    district: "",
    number: "",
    complement: "",
    name: "",
    cep: "",
    city: "",
    uf: ""
  });

  const listInputs = [
    {
      name: "Logradouro",
      field: "publicPlace",
      placeholder: "Rua, Avenida.. (ex: Rua Bandeirantes)"
    },
    { name: "Nº", field: "number", placeholder: "Numero (ex: 000)" },
    { name: "Bairro", field: "district", placeholder: "Bairro (ex: Centro)" },
    { name: "CEP", field: "cep", placeholder: "cep (ex: 00000-000 )" },
    { name: "Cidade", field: "city", placeholder: "Cidade (ex: São Paulo)" },
    { name: "UF", field: "uf", placeholder: "Estado (ex: SP)" }
  ];

  const testListLocation = [
    {
      name: "Casa",
      street: "Rua Julio Guerra",
      num: "946",
      district: "Jardim Imperial",
      city: "Ivaiporã",
      province: "PR"
    }
  ];

  useEffect(() => {
    onGetListAddress();
  }, []);

  const handleInputChange = e => {
    const { name, value } = e.target;

    setInfoAddress({
      ...infoAddress,
      [name]: value
    });
  };

  const onGetListAddress = async () => {
    if (!isActiveUser) {
      return;
    }
    
    const res = await api.get("/address").catch(error => {
      showMessage(
        "Ops Falhou",
        "Complicações no servidor, tente mais tarde ",
        "danger"
      );
      console.log(error);
    });

    if (res.data) {
      setListAddress(res.data);
    }
  };

  const onSaveLocation = async () => {
    const res = await api.post("/address/new", infoAddress);

    if (res.data) {
      if (res.data.message == "success") {
        showMessage(
          "Informaçõe Endereço",
          "Suas informações foram salvas com sucesso",
          "success"
        );
        setViewNewLocation(false);
        onGetListAddress();
      }
    }
  };

  const onDeleteLocation = async () => {
    const res = await api.delete(`/address/${addressId}`);

    if (res.data) {
      if (res.data.message == "success") {
        showMessage("Endereço", "Deletado com sucesso", "success");
        onGetListAddress();
        onMessageConfirmClose();
      }
    }
  };

  const onMessageConfirmOpen = id => {
    setAddressId(id);
    setIsMessageConfirm(true);
  };

  const onMessageConfirmClose = () => {
    setAddressId(0);
    setIsMessageConfirm(false);
  };

  return (
    <div>
      <Box alignItems="center" display="flex" flexWrap="wrap" mb={4}>
        {listAddress &&
          listAddress.map(listLocation => (
            <Card
              style={{
                borderWidth: 1,
                borderStyle: "solid",
                borderColor: "#e8e8e8",
                borderRadius: 15,
                width: 300,
                marginBottom: 10
              }}
              className="p-4"
            >
              <Box display="flex" justifyContent="space-between" mb={2}>
                <Typography style={{ color: "black", fontWeight: "bold" }}>
                  {listLocation.name}
                </Typography>
                <IconButton
                  onClick={() => {
                    onMessageConfirmOpen(listLocation.id);
                  }}
                  size="small"
                >
                  <DeleteIcon style={{ color: "red" }} fontSize="small" />
                </IconButton>
              </Box>

              <Typography variant="body2">{`${listLocation.publicPlace}, ${listLocation.number} - ${listLocation.district}`}</Typography>
              <Typography variant="body2">{`${listLocation.city} - ${listLocation.uf}`}</Typography>
            </Card>
          ))}
        {listAddress ? (
          listAddress.length > 0 ? (
            <IconButton
              onClick={() => {
                setViewNewLocation(true);
              }}
              style={{
                height: 50,
                width: 50,
                borderWidth: 1,
                borderStyle: "solid",
                borderColor: "#e8e8e8",
                boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                marginLeft: 25
              }}
            >
              <AddIcon
                style={{
                  height: 25,
                  width: 25
                }}
              />
            </IconButton>
          ) : (
            <Button
              onClick={() => {
                setViewNewLocation(true);
              }}
              style={{
                height: 50,
                backgroundColor: "#2ecc71",
                color: "white"
              }}
            >
              Adicionar Endereço
            </Button>
          )
        ) : (
          <Button
            onClick={() => {
              setViewNewLocation(true);
            }}
            style={{
              height: 50,
              backgroundColor: "#2ecc71",
              color: "white"
            }}
          >
            Adicionar Endereço
          </Button>
        )}
      </Box>
      {!viewNewLocation ? null : (
        <Card
          style={{
            borderWidth: 1,
            borderStyle: "solid",
            borderColor: "#e8e8e8",
            padding: 20
          }}
        >
          <Typography variant="body2">{"Nome do local"}</Typography>
          <InputBase
            fullWidth
            placeholder={"Local. (ex: casa, trabalho ...)"}
            name={"name"}
            value={infoAddress["name"]}
            onChange={handleInputChange}
            style={{
              marginBottom: 30
            }}
            inputProps={{
              style: {
                paddingTop: 15,
                paddingBottom: 15,
                paddingLeft: 8,
                marginTop: 8,
                marginBottom: 8,
                borderRadius: 5,
                backgroundColor: "white",
                boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)"
              }
            }}
          />
          <Grid container>
            {listInputs.map(input => (
              <Grid item sm={6}>
                <div
                  style={{
                    marginTop: 20,
                    marginRight: 20
                  }}
                >
                  <Typography variant="body2">{input.name}</Typography>
                  <InputBase
                    fullWidth
                    placeholder={input.placeholder}
                    name={input.field}
                    value={infoAddress[input.field]}
                    onChange={handleInputChange}
                    inputProps={{
                      style: {
                        marginTop: 8,
                        marginBottom: 8,
                        marginLeft: 8
                      }
                    }}
                  />
                  <Divider />
                </div>
              </Grid>
            ))}
          </Grid>
          <Box display="flex" justifyContent="flex-end" mt={4}>
            <Button
              onClick={() => {
                onSaveLocation();
              }}
              variant="contained"
              style={{
                backgroundColor: "#2ecc71",
                color: "white"
              }}
            >
              Adicionar
            </Button>
          </Box>
        </Card>
      )}
      <MessageConfirm
        title="Excluir"
        textInfo="Confirmando você estará excluindo esse endereço, certeza que deseja excluir?"
        open={isMessageConfirm}
        onAccept={onDeleteLocation}
        onClose={onMessageConfirmClose}
      />
    </div>
  );
}
