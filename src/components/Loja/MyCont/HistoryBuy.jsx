import React, { useState, useEffect } from "react";
import Cards from "react-credit-cards";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
  Avatar,
} from "@material-ui/core";
import {
  Delete as DeleteIcon,
  Add as AddIcon,
  ShoppingCart as ShoppingCartIcon,
} from "@material-ui/icons";
import api from "../../../services/api";
import "react-credit-cards/lib/styles.scss";
import moment from "moment";

export default function HistoryBuy() {
  const styleInputs = {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#e8e8e8",
    borderRadius: 5,
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 5,
  };

  const [viewNewCardCred, setViewNewCardCred] = useState(false);
  const [userInfo, setUserInfo] = useState(false);
  const [infoCard, setInfoCard] = useState({
    cvc: "",
    expiry: "",
    focus: "",
    name: "",
    number: "",
  });

  const [listBuys, setListaBuys] = useState();

  useEffect(() => {
    getUserInfo();
  }, []);

  useEffect(() => {
    if (userInfo) {
      getPurchases();
    }
  }, [userInfo]);

  const getUserInfo = async () => {
    const res = await api.get("/login/me/ecommerce");

    if (res.data) {
      console.log(res.data);
      setUserInfo(res.data);
    }
  };

  const getPurchases = async () => {
    var res = await api.get(`/purchase/${userInfo.id}`);
    console.log(res.data);
    setListaBuys(res.data);
  };

  const handleInputFocus = (e) => {
    setInfoCard({ ...infoCard, focus: e.target.name });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setInfoCard({ ...infoCard, [name]: value });
  };

  return (
    <div id="PaymentForm">
      <Box alignItems="center" display="flex" flexWrap="wrap" mb={4}>
        {listBuys &&
          listBuys.map((Buy) => (
            <Card
              style={{
                borderWidth: 1,
                marginBottom: 10,
                borderStyle: "solid",
                borderColor: "#e8e8e8",
                borderRadius: 15,
                width: "100%",
              }}
              className="p-4"
            >
              <Box
                display="flex"
                flexWrap="wrap"
                justifyContent="space-between"
              >
                <Box display="flex" alignItems="center">
                  <ShoppingCartIcon
                    className="mr-2"
                    style={{ color: "red" }}
                    fontSize="small"
                  />
                  <Typography style={{ color: "black" }}>
                    {String(
                      Buy.status == 1
                        ? "Pagamento Pendente"
                        : Buy.status == 2
                        ? "Pagamento Aprovado"
                        : Buy.status == 3
                        ? "Separando Produtos"
                        : Buy.status == 4
                        ? "Saiu para entrega"
                        : Buy.status == 5
                        ? "Finalizado"
                        : ""
                    ).toUpperCase()}
                  </Typography>
                </Box>

                <Typography>
                  {String(
                    `${Buy.Address.publicPlace}, ${Buy.Address.number}, ${Buy.Address.district} - ${Buy.Address.city}`
                  ).substr(0, 12)}
                  ...
                </Typography>

                <Typography style={{ fontWeight: "bold" }}>
                  R${parseFloat(Buy.total).toFixed(2)}
                </Typography>

                <Typography>
                  {moment(Buy.cratedAt).format("DD/MM/YYYY HH:mm")}
                </Typography>
              </Box>
            </Card>
          ))}
      </Box>
    </div>
  );
}
