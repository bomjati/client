import React, { useState, useEffect } from "react";
import Cards from "react-credit-cards";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
  Avatar
} from "@material-ui/core";
import { Delete as DeleteIcon, Add as AddIcon } from "@material-ui/icons";
import "react-credit-cards/lib/styles.scss";
import api from "../../../services/api";
import { showMessage } from "../utils/message";
import MessageConfirm from "../utils/MessageConfirm";

export default function Payment({ isActiveUser }) {
  const styleInputs = {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#e8e8e8",
    borderRadius: 5,
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 5
  };

  const [viewNewCardCred, setViewNewCardCred] = useState(false);
  const [isMessageConfirm, setIsMessageConfirm] = useState(false);
  const [credCardId, setCredCardId] = useState(0);
  const [listInfoCard, setListInfoCard] = useState();
  const [infoCard, setInfoCard] = useState({
    cvc: "",
    expiry: "",
    focus: "",
    name: "",
    number: ""
  });

  useEffect(() => {
    onGetListCredCard();
  }, []);

  /*############# Funções de format para inputs #############*/
  const formataCredCard = credicard => {
    credicard = credicard.replace(/\D/g, "");
    credicard = credicard.replace(/(\d{4})(\d)/, "$1 $2");
    credicard = credicard.replace(/(\d{4})(\d)/, "$1 $2");
    credicard = credicard.replace(/(\d{4})(\d)/, "$1 $2");
    credicard = credicard.replace(/(\d{4})$/, "$1");
    return credicard;
  };

  const formataCredCardDate = date => {
    date = date.replace(/\D/g, "");
    date = date.replace(/(\d{2})(\d)/, "$1/$2");
    date = date.replace(/(\d{2})$/, "$1");
    return date;
  };
  /* ####################################################### */

  const handleInputFocus = e => {
    setInfoCard({ ...infoCard, focus: e.target.name });
  };

  const handleInputChange = e => {
    const { name, value } = e.target;

    setInfoCard({
      ...infoCard,
      [name]:
        name == "expiry"
          ? formataCredCardDate(value)
          : name == "number"
          ? formataCredCard(value)
          : value
    });
  };

  const onGetListCredCard = async () => {
    if (!isActiveUser) {
      return;
    }
    
    const res = await api.get("/credcard").catch(error => {
      showMessage(
        "Ops Falhou",
        "Complicações no servidor, tente mais tarde ",
        "danger"
      );
      console.log(error);
    });

    if (res.data) {
      setListInfoCard(res.data);
    }
  };

  const onSaveCredCard = async () => {
    const res = await api.post("/credcard/new", {
      number: infoCard.number,
      expirationDate: infoCard.expiry,
      security: infoCard.cvc,
      name: infoCard.name
    });

    if (res.data) {
      if (res.data.message == "success") {
        showMessage(
          "Informaçõe cartão de crédito",
          "Suas informações foram salvas com sucesso",
          "success"
        );
        setViewNewCardCred(false);
        onGetListCredCard();
      }
    }
  };

  const onDeleteCredCard = async () => {
    const res = await api.delete(`/credcard/${credCardId}`);

    if (res.data) {
      if (res.data.message == "success") {
        showMessage("Cartão de crédito", "Deletado com sucesso", "success");
        onGetListCredCard();
        onMessageConfirmClose();
      }
    }
  };

  const onMessageConfirmOpen = id => {
    setCredCardId(id);
    setIsMessageConfirm(true);
  };

  const onMessageConfirmClose = () => {
    setCredCardId(0);
    setIsMessageConfirm(false);
  };

  return (
    <div id="HistoryBuyForm">
      <Box alignItems="center" display="flex" flexWrap="wrap" mb={4}>
        {listInfoCard &&
          listInfoCard.map(listCred => (
            <Card
              style={{
                borderWidth: 1,
                borderStyle: "solid",
                borderColor: "#e8e8e8",
                borderRadius: 15,
                marginBottom: 10,
                width: 300
              }}
              className="p-4"
            >
              <Box display="flex" justifyContent="space-between" mb={2}>
                <Typography style={{ color: "black", fontWeight: "bold" }}>
                  {listCred.name}
                </Typography>
                <IconButton
                  onClick={() => {
                    onMessageConfirmOpen(listCred.id);
                  }}
                  size="small"
                >
                  <DeleteIcon style={{ color: "red" }} fontSize="small" />
                </IconButton>
              </Box>

              <Typography variant="body2">
                **** **** **** {String(listCred.number).slice(-4)}
              </Typography>
              <Typography variant="body2">{`${listCred.expirationDate}`}</Typography>
            </Card>
          ))}
        {listInfoCard ? (
          listInfoCard.length > 0 ? (
            <IconButton
              onClick={() => {
                setViewNewCardCred(true);
              }}
              style={{
                height: 50,
                width: 50,
                borderWidth: 1,
                borderStyle: "solid",
                borderColor: "#e8e8e8",
                marginLeft: 25
              }}
            >
              <AddIcon
                style={{
                  height: 25,
                  width: 25
                }}
              />
            </IconButton>
          ) : (
            <Button
              onClick={() => {
                setViewNewCardCred(true);
              }}
              style={{
                height: 50,
                backgroundColor: "#2ecc71",
                color: "white"
              }}
            >
              Adicionar forma de pagamento
            </Button>
          )
        ) : (
          <Button
            onClick={() => {
              setViewNewCardCred(true);
            }}
            style={{
              height: 50,
              backgroundColor: "#2ecc71",
              color: "white"
            }}
          >
            Adicionar forma de pagamento
          </Button>
        )}
      </Box>
      {!viewNewCardCred ? null : (
        <Box display="flex" flexWrap="wrap">
          <Cards
            cvc={infoCard.cvc}
            expiry={infoCard.expiry}
            focused={infoCard.focus}
            name={infoCard.name}
            number={infoCard.number}
            locale={{ valid: "Validade" }}
            placeholders={{ name: "nome completo" }}
          />
          <form autoComplete="false">
            <div>
              <Typography
                style={{ marginLeft: 10, marginTop: 10 }}
                variant="body2"
              >
                Número cartão
              </Typography>
              <InputBase
                type="tel"
                inputProps={{
                  style: {
                    ...styleInputs
                  },
                  maxLength: 19
                }}
                name="number"
                placeholder="Número Cartão"
                fullWidth
                value={infoCard.number}
                onChange={handleInputChange}
                onFocus={handleInputFocus}
              />
            </div>

            <div>
              <Typography
                style={{ marginLeft: 10, marginTop: 10 }}
                variant="body2"
              >
                Nome completo
              </Typography>
              <InputBase
                inputProps={{
                  style: {
                    ...styleInputs
                  }
                }}
                name="name"
                placeholder="Nome Completo"
                fullWidth
                onChange={handleInputChange}
                onFocus={handleInputFocus}
              />
            </div>

            <Box display="flex">
              <div>
                <Typography
                  style={{ marginLeft: 10, marginTop: 10 }}
                  variant="body2"
                >
                  Data válidade
                </Typography>
                <InputBase
                  inputProps={{
                    style: {
                      ...styleInputs
                    },
                    maxLength: 5
                  }}
                  name="expiry"
                  value={infoCard.expiry}
                  placeholder="Data Validade"
                  fullWidth
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                />
              </div>

              <div>
                <Typography
                  style={{ marginLeft: 10, marginTop: 10 }}
                  variant="body2"
                >
                  CVC
                </Typography>
                <InputBase
                  inputProps={{
                    style: {
                      ...styleInputs
                    },
                    maxLength: 3
                  }}
                  name="cvc"
                  placeholder="CVC/CVV"
                  fullWidth
                  onChange={handleInputChange}
                  onFocus={handleInputFocus}
                />
              </div>
            </Box>
            <Box display="flex" justifyContent="flex-end" mr={1}>
              <Button
                onClick={() => {
                  onSaveCredCard();
                }}
                variant="contained"
                style={{
                  backgroundColor: "#2ecc71",
                  color: "white"
                }}
              >
                Salvar
              </Button>
            </Box>
          </form>
        </Box>
      )}
      <MessageConfirm
        title="Excluir"
        textInfo="Confirmando você estará excluindo essa forma de pagamento, certeza que deseja excluir?"
        open={isMessageConfirm}
        onAccept={onDeleteCredCard}
        onClose={onMessageConfirmClose}
      />
    </div>
  );
}
