import React, { useState, useEffect } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
  Hidden,
  Avatar,
} from "@material-ui/core";
import Toolbar from "../Register/Toolbar";
import Cont from "./Cont";
import Payment from "./Payment";
import Location from "./Location";
import HistoryBuy from "./HistoryBuy";
import {
  Settings as SettingsIcon,
  AccountBalanceWallet as AccountBalanceWalletIcon,
  AddLocation as AddLocationIcon,
  ShoppingCart as ShoppingCartIcon,
} from "@material-ui/icons";
import api from "../../../services/api";
import { logout } from "../../../services/auth";
import { showMessage } from "../utils/message";

export default function MyCont({ onExit, isActiveUser }) {
  const [enterMouse, setEnterMouse] = useState({ status: false, index: -1 });
  const [numPage, setNumPage] = useState(1);
  const [userInfo, setUserInfo] = useState({
    name: "",
  });

  const ListMenu = [
    {
      name: "Configurações da minha conta",
      onClick: () => {
        setNumPage(0);
      },
      icon: (style) => {
        return <SettingsIcon style={style} />;
      },
    },
    {
      name: "Formas de Pagamentos",
      onClick: () => {
        setNumPage(1);
      },
      icon: (style) => {
        return <AccountBalanceWalletIcon style={style} />;
      },
    },
    {
      name: "Endereços",
      onClick: () => {
        setNumPage(2);
      },
      icon: (style) => {
        return <AddLocationIcon style={style} />;
      },
    },
    {
      name: "Histórico de Compras",
      onClick: () => {
        setNumPage(3);
      },
      icon: (style) => {
        return <ShoppingCartIcon style={style} />;
      },
    },
    {
      name: "Sair",
      onClick: () => {
        onExit();
        logout();
      },
      icon: (style) => {
        return <ShoppingCartIcon style={style} />;
      },
    },
  ];

  useEffect(() => {
    getUserInfo();
  }, []);

  const getUserInfo = async () => {
    const res = await api.get("/login/me/ecommerce");

    if (res.data) {
      console.log(res.data);
      setUserInfo(res.data);
    }
  };

  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FAFAF8",
      }}
    >
      <Toolbar
        onExit={() => {
          onExit();
        }}
      />
      <Box
        style={{ marginLeft: "5%", marginRight: "5%", marginTop: "2%" }}
        display="flex"
      >
        <Hidden xsDown>
          <Card
            style={{
              width: 250,
              borderWidth: 1,
              borderStyle: "solid",
              borderColor: "#e8e8e8",
              borderRadius: 10,
            }}
          >
            <Box display="flex" m={2}>
              <Avatar
                src="/img/userProfile.gif"
                style={{
                  height: 60,
                  width: 60,
                  borderWidth: 2,
                  borderStyle: "solid",
                  borderColor: "#e8e8e8",
                }}
                className="mr-2"
              />
              <Box display="flex" flexDirection="column">
                <Typography variant="body2" color="textSecondary">
                  Meu Perfil
                </Typography>
                <Typography
                  variant="h6"
                  style={{ fontWeight: "bold", color: "#403122" }}
                >
                  {userInfo.name}
                </Typography>
              </Box>
            </Box>

            {ListMenu.map(({ icon, onClick, name }, index) => (
              <div>
                <Divider />
                <Box
                  onMouseEnter={() => {
                    setEnterMouse({ status: true, index });
                  }}
                  onMouseLeave={() => {
                    setEnterMouse({ status: false, index });
                  }}
                  onClick={() => onClick()}
                  style={{
                    cursor: "pointer",
                  }}
                  className="pt-4 pb-4"
                  display="flex"
                  alignItems="center"
                >
                  {icon({
                    color:
                      ((enterMouse.index == index) & enterMouse.status) |
                      (index == numPage)
                        ? "#2ecc71"
                        : "#8a8a8a",
                    marginLeft: 10,
                    marginRight: 5,
                  })}
                  <Typography
                    variant="body2"
                    style={{
                      color:
                        ((enterMouse.index == index) & enterMouse.status) |
                        (index == numPage)
                          ? "#2ecc71"
                          : "#8a8a8a",
                    }}
                  >
                    {name}
                  </Typography>
                </Box>
              </div>
            ))}
          </Card>
          <Card
            style={{
              borderWidth: 1,
              borderStyle: "solid",
              borderColor: "#e8e8e8",
              width: "80%",
              padding: 30,
              marginLeft: 20,
              borderRadius: 10,
            }}
          >
            <Typography
              className="mb-4"
              style={{ color: "#403122", fontWeight: "bold" }}
            >
              {numPage == 0
                ? "Informações da conta"
                : numPage == 1
                ? "Formas de pagamentos"
                : numPage == 2
                ? "Lista de Endereços"
                : "Historico de  Compras"}
            </Typography>
            {numPage == 0 ? (
              <Cont
                isActiveUser={isActiveUser}
                userInfo={userInfo}
                onRefreshUserInfo={getUserInfo}
              />
            ) : numPage == 1 ? (
              <Payment isActiveUser={isActiveUser} />
            ) : numPage == 2 ? (
              <Location isActiveUser={isActiveUser} />
            ) : (
              <HistoryBuy isActiveUser={isActiveUser} />
            )}
          </Card>
        </Hidden>
      </Box>

      <Hidden smUp>
        <Card
          style={{
            borderWidth: 1,
            borderStyle: "solid",
            borderColor: "#e8e8e8",
            borderRadius: 10,
          }}
        >
          <Box display="flex" m={2}>
            <Avatar
              src="/img/userProfile.gif"
              style={{
                height: 60,
                width: 60,
                borderWidth: 2,
                borderStyle: "solid",
                borderColor: "#e8e8e8",
              }}
              className="mr-2"
            />
            <Box display="flex" flexDirection="column">
              <Typography variant="body2" color="textSecondary">
                Meu Perfil
              </Typography>
              <Typography
                variant="h6"
                style={{ fontWeight: "bold", color: "#403122" }}
              >
                {userInfo.name}
              </Typography>
            </Box>
          </Box>

          {ListMenu.map(({ icon, onClick, name }, index) => (
            <div>
              <Divider />
              <Box
                onMouseEnter={() => {
                  setEnterMouse({ status: true, index });
                }}
                onMouseLeave={() => {
                  setEnterMouse({ status: false, index });
                }}
                onClick={() => onClick()}
                style={{
                  cursor: "pointer",
                }}
                className="pt-4 pb-4"
                display="flex"
                alignItems="center"
              >
                {icon({
                  color:
                    ((enterMouse.index == index) & enterMouse.status) |
                    (index == numPage)
                      ? "#2ecc71"
                      : "#8a8a8a",
                  marginLeft: 10,
                  marginRight: 5,
                })}
                <Typography
                  variant="body2"
                  style={{
                    color:
                      ((enterMouse.index == index) & enterMouse.status) |
                      (index == numPage)
                        ? "#2ecc71"
                        : "#8a8a8a",
                  }}
                >
                  {name}
                </Typography>
              </Box>
              {index == numPage && (
                <div className="p-4">
                  {numPage == 0 ? (
                    <Cont
                      isActiveUser={isActiveUser}
                      userInfo={userInfo}
                      onRefreshUserInfo={getUserInfo}
                    />
                  ) : numPage == 1 ? (
                    <Payment isActiveUser={isActiveUser} />
                  ) : numPage == 2 ? (
                    <Location isActiveUser={isActiveUser} />
                  ) : (
                    <HistoryBuy isActiveUser={isActiveUser} />
                  )}
                </div>
              )}
            </div>
          ))}
        </Card>
      </Hidden>
    </div>
  );
}
