import React, { useState, useEffect } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
  Avatar,
} from "@material-ui/core";
import api from "../../../services/api";
import { showMessage } from "../utils/message";

export default function Cont({ userInfo, onRefreshUserInfo }) {
  const [values, setValues] = useState({
    name: "",
    surname: "",
    email: "",
    cel: "",
    document: "",
    birthDate: "",
  });

  const listInputs = [
    { name: "Nome", field: "name" },
    { name: "Sobrenome", field: "surname" },
    { name: "Email", field: "email" },
    { name: "Celular", field: "cel" },
    { name: "CPF", field: "document" },
    { name: "Data de Nascimento", field: "birthDate" },
  ];

  const handleInput = (event) => {
    setValues({
      ...values,
      [event.target.name]:
        event.target.name == "birthDate"
          ? formataDate(event.target.value)
          : event.target.name == "cel"
          ? mphone(event.target.value)
          : event.target.name == "document"
          ? formataCPF(event.target.value)
          : event.target.value,
    });
  };

  useEffect(() => {
    setValues({
      ...values,
      name: String(userInfo.name).split(" ")[0],
      surname: String(userInfo.name).split(" ")[1],
      email: userInfo.email ? userInfo.email : "",
      cel: userInfo.cel ? userInfo.cel : "",
      document: userInfo.document ? userInfo.document : "",
    });
  }, [userInfo]);

  const onUpdateInfo = async () => {
    const res = await api.put(`/user/edit/${userInfo.id}`, {
      ...values,
      name: `${values.name} ${values.surname}`,
    });

    if (res.data) {
      if (res.data.message == "success") {
        showMessage(
          "Alteração",
          "Informações atualisadas com sucesso",
          "success"
        );
      } else {
        showMessage(
          "Alteração",
          "Ops, ocorreu um erro tente mais tarde",
          "danger"
        );
      }
    }
  };

  const formataCPF = (cpf) => {
    cpf = cpf.replace(/\D/g, "");
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return cpf;
  };
  
  const formataDate = (cpf) => {
    cpf = cpf.replace(/\D/g, "");
    cpf = cpf.replace(/(\d{2})(\d)/, "$1/$2");
    cpf = cpf.replace(/(\d{2})(\d)/, "$1/$2");
    cpf = cpf.replace(/(\d{4})$/, "$1");
    return cpf;
  };

  const mphone = (v) => {
    var r = v.replace(/\D/g, "");

    console.log(r);
    if (r.length == 0) {
      return "";
    } else {
      r = r.replace(/^0/, "");
      if (r.length > 10) {
        r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
      } else if (r.length > 5) {
        r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
      } else if (r.length > 2) {
        r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
      } else {
        r = r.replace(/^(\d*)/, "($1");
      }
      return r;
    }
  };

  return (
    <div>
      <Grid container>
        {listInputs.map((input) => (
          <Grid item sm={6}>
            <div
              style={{
                marginTop: 20,
                marginRight: 20,
              }}
            >
              <Typography variant="body2">{input.name}</Typography>
              <InputBase
                fullWidth
                value={values[String(input.field)]}
                name={input.field}
                onChange={handleInput}
                inputProps={{
                  maxLength:
                    input.field == "document"
                      ? 14
                      : input.field == "birthDate"
                      ? 10
                      : 255,
                  style: {
                    marginTop: 8,
                    marginBottom: 8,
                    marginLeft: 8,
                  },
                }}
              />
              <Divider />
            </div>
          </Grid>
        ))}
      </Grid>
      <Box display="flex" justifyContent="flex-end" mt={4}>
        <Button
          onClick={() => {
            onUpdateInfo();
          }}
          variant="contained"
          style={{
            backgroundColor: "#2ecc71",
            color: "white",
          }}
        >
          Atualizar
        </Button>
      </Box>
    </div>
  );
}
