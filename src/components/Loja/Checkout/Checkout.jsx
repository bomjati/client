import React, { useEffect, useState } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import Toolbar from "../Register/Toolbar";
import Sell from "./Sell";
import Coupon from "./Coupon";
import FormPayment from "./FormPayment";
import DetailProdut from "./DetailProdut";
import DetailAddress from "./DetailAddress";
import ModalCredCard from "../Modal/ModalCredCardList";
import MessageConfirm from "../utils/MessageConfirm";
import { showMessage } from "../utils/message";
import api from "../../../services/api";
import { useCookies } from "react-cookie";
import { tgdeveloper } from "./uses";

function detectar_mobile() {
  var check = false; //wrapper no check
  (function (a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}

export default function Checkout({
  onExit,
  onSignUp,
  selectAddress,
  nameBusiness,
  ecommerceId,
  isActiveUser,
  onConclused,
  onCloseModalCarsSale,
}) {
  const [showConfirm, setShowConfirm] = useState(false);
  const [itemsCarsSales, setItemsCarsSales] = useCookies(["ItemsCarsSales"]);
  const [qtdItems, setQtdItems] = useState(0);
  const [valueItems, setvalueItems] = useState(0);
  const [valueDiscont, setValueDiscont] = useState(0);
  const [selectCredCard, setSelectCredCard] = useState();
  const [isModalCredCard, setIsModalCredCard] = useState(false);
  const [userInfo, setUserInfo] = useState(false);
  const isMobile = detectar_mobile();

  useEffect(() => {
    console.log(selectCredCard);
  }, [selectCredCard]);

  useEffect(() => {
    getUserInfo();
  }, []);

  useEffect(() => {
    setQtdItems(
      itemsCarsSales.ItemsCarsSales ? itemsCarsSales.ItemsCarsSales.length : 0
    );
  }, [itemsCarsSales]);

  useEffect(() => {
    if (itemsCarsSales.ItemsCarsSales) {
      let vItems = 0;
      let vDiscont = 0;

      for (
        let index = 0;
        index < itemsCarsSales.ItemsCarsSales.length;
        index++
      ) {
        const element = itemsCarsSales.ItemsCarsSales[index];

        vItems =
          vItems +
          (element.promotion_price > 0
            ? element.promotion_price
            : element.price) *
            element.qtd;

        vDiscont =
          vDiscont +
          (element.promotion_price > 0
            ? element.price - element.promotion_price
            : 0) *
            element.qtd;
      }

      setvalueItems(vItems);
      setValueDiscont(vDiscont);
    }
  }, [itemsCarsSales]);

  const getUserInfo = async () => {
    if (!isActiveUser) return;

    const res = await api.get("/login/me/ecommerce");

    if (res.data) {
      console.log(res.data);
      setUserInfo(res.data);
    }
  };

  const onModalCredCard = () => {
    setIsModalCredCard(true);
  };

  const onCloseModalCredCard = () => {
    setIsModalCredCard(false);
  };

  const onSelectCredCard = (credcard) => {
    setSelectCredCard(credcard);
  };

  const onPayCard = async () => {
    let data = {
      MerchantId: "e28fe8c0-4216-4cb2-b238-0e68385d7c96",
      MerchantKey: "QJANYHGQTOHYMXYKDLESOHROKJYKBCHNZSZTGMQX",
      body: {
        MerchantOrderId: "2014111703",
        Customer: {
          Name: userInfo.name,
        },
        Payment: {
          Type: "CreditCard",
          Amount: String(valueItems).replace(/\D/g, ""),
          Installments: 1,
          SoftDescriptor: nameBusiness,
          CreditCard: {
            CardNumber: String(selectCredCard.number)
              .trim()
              .replaceAll(" ", ""),
            Holder: selectCredCard.name,
            ExpirationDate:
              selectCredCard.expirationDate.split("/")[0] +
              "/20" +
              selectCredCard.expirationDate.split("/")[1],
            SecurityCode: selectCredCard.security,
            Brand: tgdeveloper.getCardFlag(selectCredCard.number),
          },
          IsCryptoCurrencyNegotiation: false,
        },
      },
    };

    console.log(data);
    const res = await api
      .post("/paycard/cielo/payment", data)
      .catch((error) => {
        showMessage("Pagamento", "Algo deu errado, tente mais tarde", "danger");
      });

    console.log(res.data);

    switch (res.data.data.Payment.ReturnCode) {
      case "05":
        showMessage(
          "Pagamento",
          "Seu pagamento não foi autorizado, confirme os seus dados.",
          "danger"
        );
        break;
      case "57":
        showMessage(
          "Pagamento",
          "Seu pagamento não foi autorizado, cartão está expirado.",
          "danger"
        );
        break;
      case "78":
        showMessage(
          "Pagamento",
          "Seu pagamento não foi autorizado, cartão está bloqueado.",
          "danger"
        );
        break;
      case "99":
        if (
          res.data.data.Payment.ReturnMessage == "Operation Successful / Time Out"
        ) {
          onPayment();
        } else {
          showMessage(
            "Pagamento",
            "Seu pagamento não foi autorizado, instabilidade na rede.",
            "danger"
          );
        }
        break;
      case "77":
        showMessage(
          "Pagamento",
          "Seu pagamento não foi autorizado, cartão está cancelado.",
          "danger"
        );
        break;
      case "70":
        showMessage(
          "Pagamento",
          "Seu pagamento não foi autorizado, foram encontrado problemas no seu cartão consute no seu banco para mais informações.",
          "danger"
        );
        break;
      case "4":
      case "6":
        onPayment();
        break;
      default:
        showMessage("Pagamento", "Algo deu errado, tente mais tarde", "danger");
    }
  };

  const onPayment = async () => {
    if (itemsCarsSales.ItemsCarsSales.length > 0) {
      const datePayment = {
        discount: 0,
        total: valueItems,
        userId: userInfo.id,
        ecommerceId: ecommerceId,
        addressId: selectAddress.id,
        credcardId: selectCredCard ? selectCredCard.id : 0,
        isMoney: selectCredCard ? "false" : true,
        status: !selectCredCard ? 1 : 2,
      };

      const res = await api.post("/purchase/new", datePayment);

      if (res.data.message == "success") {
        var itemsPost = [];
        for (
          let index = 0;
          index < itemsCarsSales.ItemsCarsSales.length;
          index++
        ) {
          const element = itemsCarsSales.ItemsCarsSales[index];

          itemsPost = {
            qtd: element.qtd,
            productId: element.id,
            purchaseId: res.data.data.id,
          };

          console.log(itemsPost);
          const resI = await api.post("/purchasesitem/new", itemsPost);
        }

        onConclused();
        onCloseModalCarsSale();
        onExit();
      }
    } else {
      alert("nenhum produto selecionado");
    }
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onPayCard();
  };

  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FAFAF8",
      }}
    >
      <Toolbar
        onExit={() => {
          onExit();
        }}
      />

      <div
        style={{
          marginLeft: isMobile ? 10 : "10%",
          marginRight: isMobile ? 10 : "6%",
          marginTop: isMobile ? "8%" : "2%",
        }}
      >
        <Typography
          className="ml-3"
          variant="h4"
          style={{
            fontWeight: "bold",
          }}
        >
          Checkout
        </Typography>

        <Box display="flex" flexWrap="wrap">
          <Box display="flex" flexDirection="column">
            {/* Detalhes de Endereço */}
            <DetailAddress isMobile={isMobile} selectAddress={selectAddress} />

            {/* Detalhes Produtos */}
            <DetailProdut
              isMobile={isMobile}
              items={itemsCarsSales.ItemsCarsSales}
              qtdItems={qtdItems}
            />

            {/* Formas de Pagamentos  */}
            <Card
              style={{
                width: isMobile ? "100%" : 700,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: "#ededed",
                borderStyle: "solid",
              }}
              className="p-4 mt-4 mb-4"
            >
              <FormPayment
                isMobile={isMobile}
                onModalCredCard={onModalCredCard}
                selectCredCard={selectCredCard}
              />
              <Divider className="mt-4 mb-4" />
              <Coupon isMobile={isMobile} />
            </Card>
          </Box>

          {/* Deltalhes da Venda */}
          <Sell
            isMobile={isMobile}
            valueItems={valueItems}
            valueDiscont={valueDiscont}
            onPayment={() => setShowConfirm(true)}
          />
        </Box>
      </div>

      <ModalCredCard
        open={isModalCredCard}
        onCloseModalCredCard={onCloseModalCredCard}
        isActiveUser={isActiveUser}
        userInfo={userInfo}
        onSelectCredCard={onSelectCredCard}
      />

      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, seu pedido será enviado e finalizado? `}
        title="Deseja confirmar?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </div>
  );
}
