import React from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";

export default function Coupon() {
  return (
    <Box display="flex" justifyContent="space-between">
      <Typography
        variant="body1"
        style={{ fontWeight: "bold", color: "#363636" }}
      >
        Cupom
      </Typography>
      <Box display="flex" alignItems="center">
        <InputBase
          placeholder="Insira um cupom"
          style={{
            backgroundColor: "white",
            paddingLeft: 20,
            borderWidth: 1,
            borderRadius: 15,
            marginRight: 20,
            borderColor: "silver",
            borderStyle: "solid"
          }}
        />
        <Button
          variant="contained"
          size="small"
          style={{
            color: "white",
            borderRadius: 10,
            backgroundColor: "#2ecc71"
          }}
        >
          Validar
        </Button>
      </Box>
    </Box>
  );
}
