import React from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";

export default function FormPayment({ onModalCredCard, selectCredCard }) {
  return (
    <Box display="flex" justifyContent="space-between">
      <Typography
        variant="body1"
        style={{ fontWeight: "bold", color: "black" }}
      >
        Método de pagamentos
      </Typography>
      <Box display="flex" alignItems="center">
        <IconButton></IconButton>
        <Typography
          variant="body2"
          style={{
            color: "black",
            marginLeft: 20,
            marginRight: 20,
            fontWeight: "bold"
          }}
        >
          {selectCredCard ? `**** **** **** ${String(selectCredCard.number).substr(-4)}` : "Dinheiro"}
        </Typography>
        <Button
          onClick={() => {
            onModalCredCard();
          }}
          variant="contained"
          size="small"
          style={{
            color: "white",
            borderRadius: 10,
            backgroundColor: "#2ecc71"
          }}
        >
          Alterar
        </Button>
      </Box>
    </Box>
  );
}
