import React from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";

export default function Sell({ valueDiscont,valueItems, onPayment, isMobile }) {
  return (
    <Card
      className={`p-4 ${isMobile ? "m-1" : "m-4"}`}
      style={{
        flex: 1,
        height: 300,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: "#ededed",
        borderStyle: "solid"
      }}
    >
      <Box display="flex" justifyContent="space-between">
        <Typography variant="body2" color="textSecondary">
          Custo dos produtos
        </Typography>
        <Box display="flex">
          <Typography
            variant="body2"
            style={{
              color: "red",
              marginRight: 5,
              textDecoration: "line-through"
            }}
          >
            R$ {valueDiscont.toFixed(2)}
          </Typography>
          <Typography variant="body2" color="textPrimary">
            R$ {valueItems.toFixed(2)}
          </Typography>
        </Box>
      </Box>
      <Box mt={2} display="flex" justifyContent="space-between">
        <Typography variant="body2" color="textSecondary">
          Taxa de entrega
        </Typography>
        <Box display="flex">
          <Typography
            variant="body2"
            style={{
              color: "red",
              marginRight: 5,
              textDecoration: "line-through"
            }}
          >
            R$ 0,00
          </Typography>
          <Typography variant="body2" color="textPrimary">
            R$ 0,00
          </Typography>
        </Box>
      </Box>
      <Box mt={2} display="flex" justifyContent="space-between">
        <Typography variant="body2" color="textSecondary">
          Taxa de Serviço
        </Typography>
        <Typography variant="body2" color="textPrimary">
          R$ 0,00
        </Typography>
      </Box>

      <Divider className="mt-4 mb-4" />

      <Box mt={2} mb={4} display="flex" justifyContent="space-between">
        <Typography
          style={{
            fontWeight: "bold",
            color: "black"
          }}
          variant="body1"
        >
          Total
        </Typography>
        <Typography
          style={{
            fontWeight: "bold",
            color: "black"
          }}
          variant="body1"
        >
          R$ {valueItems.toFixed(2)}
        </Typography>
      </Box>
      <Button
        onClick={() => {
          onPayment();
        }}
        style={{
          color: "white",
          backgroundColor: "#2ecc71",
          height: 48,
          borderRadius: 10
        }}
        fullWidth
        variant="contained"
      >
        Fazer Pedido
      </Button>
    </Card>
  );
}
