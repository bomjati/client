import React from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";

export default function DetailAddress({ selectAddress, isMobile }) {
  return (
    selectAddress && (
      <Card
        style={{
          width: isMobile ? "100%" : 700,
          borderWidth: 1,
          borderColor: "#ededed",
          borderStyle: "solid",
          borderRadius: 10
        }}
        className="p-4 mt-4"
      >
        <Typography
          variant="body1"
          style={{ fontWeight: "bold", color: "#363636" }}
        >
          Endereços
        </Typography>
        <Typography
          className="mt-2"
          variant="h6"
          style={{ fontWeight: "bold", color: "#363636" }}
        >
          {`${selectAddress.publicPlace}, ${selectAddress.number}, ${selectAddress.district} | ${selectAddress.city} - ${selectAddress.uf}`}
        </Typography>

        <Box
          display="flex"
          flexDirection="column"
          style={{
            borderWidth: 1,
            borderColor: "silver",
            borderStyle: "solid",
            marginTop: 10,
            paddingTop: 10,
            paddingRight: 10,
            paddingLeft: 10,
            borderRadius: 12,
            width: "100%"
          }}
        >
          <Typography
            variant="button"
            style={{
              fontWeight: "bold",
              fontSize: 11,
              width: "100%",
              color: "#808080"
            }}
          >
            DETALHES DO SEU ENDEREÇO
          </Typography>
          <InputBase
            style={{
              marginTop: -5,
              padding: 0
            }}
            placeholder="descrição"
          />
        </Box>

        <Box mt={3} display="flex" justifyContent="space-between">
          <Typography
            variant="body1"
            style={{ fontWeight: "bold", color: "#363636" }}
          >
            Tempo de Entrega:
          </Typography>
        </Box>
      </Card>
    )
  );
}
