import React, { useEffect } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
  Avatar
} from "@material-ui/core";

export default function DetailProdut({ qtdItems, items, isMobile }) {
  useEffect(() => {
    console.log(items);
  }, [items]);
  return (
    <Card
      style={{
        width: isMobile ? "100%" : 700,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#ededed",
        borderStyle: "solid"
      }}
      className="p-4 mt-4"
    >
      <Box display="flex" justifyContent="space-between">
        <div>
          <Typography
            variant="body1"
            style={{ fontWeight: "bold", color: "#363636" }}
          >
            Seus Produtos
          </Typography>
          <Typography variant="body2" style={{ fontWeight: "bold" }}>
            {qtdItems} items
          </Typography>
        </div>
        <Box display="flex">
          {items &&
            items.slice(0,2).map(item => (
              <Avatar
                style={{ height: 45, width: 45, marginRight: 10 }}
                src={item.path_s3}
              />
            ))}

          {qtdItems > 2 && (
            <Box
              display="flex"
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
              style={{
                height: 45,
                width: 45,
                borderRadius: 45,
                backgroundImage: "linear-gradient(100deg, #ffbe76, red)"
              }}
            >
              <Typography
                style={{ color: "white", fontWeight: "bold" }}
                variant="body2"
              >
                + {qtdItems - 2}
              </Typography>
            </Box>
          )}
        </Box>
      </Box>
    </Card>
  );
}
