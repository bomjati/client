import React, { useEffect, useState } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import Toolbar from "./Toolbar";
import api from "../../../services/api";
import { showMessage } from "../utils/message";

export default function SignUp({ onExit, onOpenLogin }) {
  const styleBase = {
    borderStyle: "solid",
    borderWidth: 2,
    borderColor: "rgba(99, 205, 218, 0.6)",
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    backgroundColor: "white",
    height: 55,
  };

  const [acceptTerms, setAcceptTerms] = useState(false);
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");

  const onSignUp = async () => {
    if (validateInfo()) {
      const res = await api.post("/user/register", {
        name: `${name} ${lastName}`,
        email: email,
        password: password,
        accept_term: String(acceptTerms),
        typeuserId: 3,
        businessId: 0,
        document: "",
        cel: phone,
      });

      if (res.data) {
        if (res.data.message == "success") {
          onOpenLogin();
        }else{
          showMessage("Ops", "algo deu errado, tente mais tarte", "danger");
        }
      }
    }
  };

  const validateInfo = () => {
    if (name == "") {
      showMessage("Campo vázio", "preencha o campo Nome", "warning");
      return false;
    }
    if (lastName == "") {
      showMessage("Campo vázio", "preencha o campo Sobre Nome", "warning");
      return false;
    }
    if (email == "") {
      showMessage("Campo vázio", "preencha o campo Email", "warning");
      return false;
    }
    if (phone == "") {
      showMessage("Campo vázio", "preencha o campo celular", "warning");
      return false;
    }
    if (password == "") {
      showMessage("Campo vázio", "preencha o campo Senha", "warning");
      return false;
    }
    if (String(password).length < 8) {
      showMessage(
        "Campo inválido",
        "o campo senha necessita no minino de 8 caracteres",
        "warning"
      );
      return false;
    }
    if (!acceptTerms) {
      showMessage(
        "Assina-le",
        "assinale o campo aceitando os termos para proceguir",
        "warning"
      );
      return false;
    }

    return true;
  };

  const mphone = (v) => {
    var r = v.replace(/\D/g, "");

    console.log(r);
    if (r.length == 0) {
      return "";
    } else {
      r = r.replace(/^0/, "");
      if (r.length > 10) {
        r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
      } else if (r.length > 5) {
        r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
      } else if (r.length > 2) {
        r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
      } else {
        r = r.replace(/^(\d*)/, "($1");
      }
      return r;
    }
  };

  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FAFAF8",
      }}
    >
      <Toolbar
        onExit={() => {
          onExit();
        }}
      />
      <Box
        mr={4}
        ml={4}
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        style={{ backgroundColor: "#FAFAF8" }}
      >
        <div
          style={{
            paddingTop: 50,
          }}
        >
          <Typography variant="h3" style={{ fontWeight: "bold", width: 350 }}>
            Registra-se com seu número de telefone!
          </Typography>

          <Box mt={4} display="flex" justifyContent="space-between">
            <div style={{ ...styleBase, width: "48%" }}>
              <InputBase
                value={name}
                onChange={(event) => {
                  setName(event.target.value);
                }}
                className="ml-4"
                placeholder="Nome"
              />
            </div>
            <div style={{ ...styleBase, width: "48%" }}>
              <InputBase
                value={lastName}
                onChange={(event) => {
                  setLastName(event.target.value);
                }}
                className="ml-4"
                placeholder="Sobrenome"
              />
            </div>
          </Box>
          <div style={{ ...styleBase, marginTop: 20 }}>
            <InputBase
              value={email}
              onChange={(event) => {
                setEmail(event.target.value);
              }}
              className="ml-4"
              fullWidth
              placeholder="E-mail"
            />
          </div>
          <Box style={styleBase} mt={2} display="flex">
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <img
                height={32}
                width={32}
                style={{ marginLeft: 20 }}
                src={"/img/brasil.svg"}
              />
              <Typography
                variant="body1"
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginLeft: 20,
                  marginRight: 20,
                }}
              >
                +55
              </Typography>
            </Box>
            <Divider
              orientation="vertical"
              className="ml-3"
              style={{ width: 2, backgroundColor: "rgba(99, 205, 218, 0.6)" }}
            />
            <InputBase
              value={phone}
              onChange={(event) => {
                setPhone(mphone(event.target.value));
              }}
              className="ml-3"
              placeholder="Escreva seu número"
            />
          </Box>
          <div style={{ ...styleBase, marginTop: 20 }}>
            <InputBase
              value={password}
              onChange={(event) => {
                setPassword(event.target.value);
              }}
              className="ml-4"
              fullWidth
              type="password"
              placeholder="Senha"
            />
          </div>
          <Box mt={2}>
            <FormControlLabel
              control={
                <Checkbox
                  style={{
                    color: "#2ecc71",
                  }}
                  checked={acceptTerms}
                  onChange={() => {
                    setAcceptTerms(!acceptTerms);
                  }}
                  name="checkedA"
                />
              }
              label="Aceito os Termos e Condições e a Política de Privacidade"
            />
          </Box>
          <Box mt={2} mb={4} display="flex" justifyContent="center">
            <Button
              onClick={() => {
                onSignUp();
              }}
              fullWidth
              variant="contained"
              style={{
                color: "white",
                borderRadius: 5,
                backgroundColor: "#2ecc71",
              }}
            >
              Inscrever-se
            </Button>
          </Box>
        </div>
      </Box>
    </div>
  );
}
