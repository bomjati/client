import React from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Hidden,
  Box,
  InputBase,
  Button,
  Tooltip,
  Avatar,
  Divider,
  Badge,
  Typography,
} from "@material-ui/core";

import { ArrowBackIos as ArrowBackIosIcon } from "@material-ui/icons";

export default function Toolbars({ onExit }) {
  return (
    <AppBar className="fixed-top" position="relative">
      <Toolbar className="fixed-top" style={{ backgroundColor: "white", height: 35 }}>
        <Box
          style={{
            cursor: "pointer",
          }}
          display="flex"
          justifyItems="center"
          onClick={onExit}
        >
          <ArrowBackIosIcon
            fontSize="default"
            style={{
              color: "red",
            }}
          />
          <Typography style={{ fontWeight: "bold", color: "black" }}>
            Retornar
          </Typography>
        </Box>
      </Toolbar>
    </AppBar>
  );
}
