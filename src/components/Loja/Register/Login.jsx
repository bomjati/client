import React, { useEffect, useState } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
  InputBase,
  TextField,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import Toolbar from "./Toolbar";
import api from "../../../services/api";
import { login } from "../../../services/auth";
import { showMessage } from "../utils/message";

export default function Login({ onExit, onSignUp, onOpenHome }) {
  const styleBase = {
    borderStyle: "solid",
    borderWidth: 2,
    borderColor: "rgba(99, 205, 218, 0.6)",
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    backgroundColor: "white",
    height: 55,
  };

  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");

  const onLogin = async () => {
    if (validateInfo()) {
      const res = await api.post("/login/ecommerce", {
        password: password,
        cel: phone,
      });

      if (res.data) {
        if (res.data.auth) {
          login(res.data.token);
          onOpenHome();
        } else {
          showMessage(
            "Credenciais Incorretas",
            "Certifique, se suas informações estão corretas",
            "danger"
          );
        }
      } else {
        showMessage("Ops", "algo deu errado, tente mais tarte", "danger");
      }
    }
  };

  const validateInfo = () => {
    if (phone == "") {
      showMessage("Campo vázio", "preencha o campo celular", "warning");
      return false;
    }
    if (password == "") {
      showMessage("Campo vázio", "preencha o campo Senha", "warning");
      return false;
    }
    if (String(password).length < 8) {
      showMessage(
        "Campo inválido",
        "o campo senha necessita no minino de 8 caracteres",
        "warning"
      );
      return false;
    }

    return true;
  };

  const mphone = (v) => {
    var r = v.replace(/\D/g, "");

    if (r.length == 0) {
      return "";
    } else {
      r = r.replace(/^0/, "");
      if (r.length > 10) {
        r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
      } else if (r.length > 5) {
        r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
      } else if (r.length > 2) {
        r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
      } else {
        r = r.replace(/^(\d*)/, "($1");
      }
      return r;
    }
  };

  return (
    <div
      style={{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FAFAF8",
      }}
    >
      <Toolbar
        onExit={() => {
          onExit();
        }}
      />
      <Box
        mr={4}
        ml={4}
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        style={{ backgroundColor: "#FAFAF8" }}
      >
        <div
          style={{
            paddingTop: 50,
          }}
        >
          <Typography variant="h3" style={{ fontWeight: "bold", width: 350 }}>
            Iniciar sessão!
          </Typography>

          <Typography
            variant="body1"
            style={{ width: 350, marginTop: 30, marginBottom: 30 }}
          >
            Você ainda não se cadastrou?{" "}
            <a
              onClick={() => {
                onSignUp();
              }}
              style={{ color: "#2ecc71", cursor: "pointer" }}
            >
              Assine aqui
            </a>
          </Typography>

          <Box style={styleBase} mt={2} display="flex">
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <img
                height={32}
                width={32}
                style={{ marginLeft: 20 }}
                src="/img/brasil.svg"
              />
              <Typography
                variant="body1"
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginLeft: 20,
                  marginRight: 20,
                }}
              >
                +55
              </Typography>
            </Box>
            <Divider
              orientation="vertical"
              className="ml-3"
              style={{ width: 2, backgroundColor: "rgba(99, 205, 218, 0.6)" }}
            />
            <InputBase
              onChange={(event) => {
                setPhone(mphone(event.target.value));
              }}
              value={phone}
              className="ml-3"
              placeholder="Escreva seu número"
            />
          </Box>
          <div style={{ ...styleBase, marginTop: 20 }}>
            <InputBase
              value={password}
              onChange={(event) => {
                setPassword(event.target.value);
              }}
              className="ml-4"
              type="password"
              fullWidth
              placeholder="Senha"
            />
          </div>
          <Box mt={2} mb={4} display="flex" justifyContent="center">
            <Button
              onClick={() => {
                onLogin();
              }}
              fullWidth
              variant="contained"
              style={{
                color: "white",
                borderRadius: 5,
                backgroundColor: "#2ecc71",
              }}
            >
              Entrar
            </Button>
          </Box>
        </div>
      </Box>
    </div>
  );
}
