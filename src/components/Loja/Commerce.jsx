import React, { useEffect, useState, useRef } from "react";
import {
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Button,
  Typography,
  IconButton,
} from "@material-ui/core";
import {
  Close as CloseIcon,
  DeleteOutlineOutlined as DeleteIcon,
  Add as AddIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Remove as RemoveIcon,
  Favorite as FavoriteIcon,
  SpeakerNotesOutlined as SpeakerNotesOutlinedIcon,
} from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";
import { data } from "./data";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useCookies } from "react-cookie";

const useStyles = makeStyles(({ breakpoints }) => ({
  root: {
    [breakpoints.up("xs")]: {},
    [breakpoints.up("sm")]: {},
    marginLeft: "5%",
    marginRight: "5%",
    marginTop: "2%",
    marginBottom: "2%",
    borderRadius: 10,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#ededed",
  },
}));

const Commerce = ({
  onOpenModalInfoProduct,
  onSetProductSelected,
  products,
  isLoading,
  onViewProduct,
  categorys,
}) => {
  const classes = useStyles();
  const dataView = [1, 2, 3, 4];
  const [itemsCarsSales, setItemsCarsSales] = useCookies(["ItemsCarsSales"]);
  const [limit, setLimit] = useState(4);
  const [page, setPage] = useState(0);
  const listInnerRef = useRef();

  const onScroll = () => {
    if (listInnerRef.current) {
      const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
      if (scrollTop + clientHeight === scrollHeight) {
        // TO SOMETHING HERE
        console.log("Reached bottom");
        setLimit(limit + 4);
      }
    }
  };

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  useEffect(() => {
    console.log(itemsCarsSales.ItemsCarsSales);
  }, [itemsCarsSales]);

  return (
    <div onScroll style={{ background: "#fcfcfc" }} id="scrollProduct">
      {categorys
        .filter(
          (c) => products.filter((p) => p.categoryId == c.id).length > 0
        )
        .slice(page * limit, page * limit + limit)
        .map((category) => (
          <Card className={classes.root}>
            <Box mb={1} display="flex" justifyContent="space-between">
              <h3 style={{ color: "#2d3436" }} className="text-black m-4">
                {category.name}
              </h3>
            </Box>

            <PerfectScrollbar>
              <Box paddingLeft={2} display="flex">
                {isLoading &&
                  dataView.map((a) => (
                    <div
                      style={{
                        width: 190,
                        marginLeft: 25,
                        marginRight: 25,
                        marginTop: 20,
                      }}
                    >
                      <Box
                        className="h-100"
                        display="flex"
                        flexDirection="column"
                        justifyContent="space-between"
                      >
                        <div>
                          <Skeleton
                            animation="wave"
                            variant="rect"
                            height={(200 * 3) / 4}
                          />

                          <Skeleton animation="wave" className="mt-3" />
                          <Skeleton animation="wave" />
                        </div>
                        <div>
                          <Skeleton
                            style={{ borderRadius: 20 }}
                            className="mb-4 mt-4"
                            animation="wave"
                            variant="rect"
                            height={20}
                          />
                        </div>
                      </Box>
                    </div>
                  ))}

                {products &&
                  products
                    .filter((p) => p.categoryId == category.id)
                    .map((prod, index) => (
                      <div
                        style={{
                          width: 150,
                          marginLeft: 25,
                          marginRight: 25,
                        }}
                      >
                        <Box
                          onClick={() => {
                            onSetProductSelected(prod);
                            onOpenModalInfoProduct();
                          }}
                          style={{ cursor: "pointer" }}
                          className="h-100"
                          display="flex"
                          flexDirection="column"
                          justifyContent="space-between"
                        >
                          <div>
                            <div style={{ cursor: "pointer" }}>
                              {((prod.promotion_price - prod.price) /
                                prod.price) *
                                100 !=
                                0 && (
                                <div
                                  style={{
                                    backgroundColor: "#dfd6ff",
                                    borderRadius: 20,
                                    position: "absolute",
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    paddingTop: 5,
                                    paddingBottom: 5,
                                    marginTop: 20,
                                  }}
                                >
                                  <Typography
                                    variant="h2"
                                    style={{
                                      fontWeight: "bold",
                                      color: "#2e187d",
                                      fontSize: 12,
                                    }}
                                  >
                                    {(
                                      ((prod.promotion_price - prod.price) /
                                        prod.price) *
                                      100
                                    ).toFixed(2)}
                                    % dto.
                                  </Typography>
                                </div>
                              )}
                              <div
                                style={{
                                  width: 90,
                                  height: 140,
                                }}
                              >
                                <img
                                  width={90}
                                  height={(90 * 3) / 4}
                                  src={
                                    prod.Imageproducts
                                      ? prod.Imageproducts[0].path_s3
                                      : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000"
                                  }
                                  style={{
                                    marginLeft: 20,
                                    marginTop: 10,
                                  }}
                                />
                              </div>
                            </div>
                            <Box className="mt-3" display="flex">
                              <h5 className="text-black mr-2">
                                R${" "}
                                {parseFloat(prod.promotion_price)
                                  .toFixed(2)
                                  .replace(".", ",")}
                              </h5>
                              <h5
                                style={{
                                  color: "silver",
                                  textDecoration: "line-through",
                                }}
                                className="text-black"
                              >
                                R${" "}
                                {parseFloat(prod.price)
                                  .toFixed(2)
                                  .replace(".", ",")}
                              </h5>
                            </Box>
                            <Typography
                              variant="body1"
                              style={{ color: "black" }}
                              className="mt-3"
                            >
                              {prod.name}
                            </Typography>
                            <Typography
                              variant="body2"
                              color="textSecondary"
                              className="mt-2"
                            >
                              {prod.detail}
                            </Typography>
                          </div>
                          <div>
                            {itemsCarsSales.ItemsCarsSales &&
                            itemsCarsSales.ItemsCarsSales.filter(
                              (i) => i.id == prod.id
                            ).length > 0 ? (
                              <Box
                                className="mt-4"
                                display="flex"
                                justifyContent="space-between"
                                alignItems="center"
                                style={{
                                  ...styleButtonIcon,
                                  width: "100%",
                                  borderRadius: 10,
                                }}
                              >
                                {itemsCarsSales.ItemsCarsSales.filter((i) => {
                                  return i.id == prod.id;
                                })[0].qtd == 1 ? (
                                  <IconButton
                                    onClick={(event) => {
                                      event.stopPropagation();
                                      let data =
                                        itemsCarsSales.ItemsCarsSales.filter(
                                          (i) => {
                                            return i.id != prod.id;
                                          }
                                        );

                                      setItemsCarsSales(
                                        "ItemsCarsSales",
                                        data,
                                        {
                                          path: "/",
                                        }
                                      );
                                    }}
                                    size="small"
                                  >
                                    <DeleteIcon style={{ color: "#2d3436" }} />
                                  </IconButton>
                                ) : (
                                  <IconButton
                                    onClick={(event) => {
                                      event.stopPropagation();
                                      let data =
                                        itemsCarsSales.ItemsCarsSales.filter(
                                          (i) => {
                                            return i.id != prod.id;
                                          }
                                        );

                                      let dataNow =
                                        itemsCarsSales.ItemsCarsSales.filter(
                                          (i) => {
                                            return i.id == prod.id;
                                          }
                                        )[0];

                                      data.push({
                                        id: prod.id,
                                        name: prod.name,
                                        detail: prod.detail,
                                        price: prod.price,
                                        promotion_price: prod.promotion_price,
                                        path_s3: prod.Imageproducts[0].path_s3
                                          ? prod.Imageproducts[0].path_s3
                                          : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000",
                                        qtd: dataNow.qtd - 1,
                                      });

                                      setItemsCarsSales(
                                        "ItemsCarsSales",
                                        data,
                                        {
                                          path: "/",
                                        }
                                      );
                                    }}
                                    size="small"
                                  >
                                    <RemoveIcon style={{ color: "#2d3436" }} />
                                  </IconButton>
                                )}

                                <h5>
                                  {
                                    itemsCarsSales.ItemsCarsSales.filter(
                                      (i) => {
                                        return i.id == prod.id;
                                      }
                                    )[0].qtd
                                  }{" "}
                                  U
                                </h5>

                                <IconButton
                                  onClick={(event) => {
                                    event.stopPropagation();
                                    let data =
                                      itemsCarsSales.ItemsCarsSales.filter(
                                        (i) => {
                                          return i.id != prod.id;
                                        }
                                      );
                                    let dataNow =
                                      itemsCarsSales.ItemsCarsSales.filter(
                                        (i) => {
                                          return i.id == prod.id;
                                        }
                                      )[0];

                                    data.push({
                                      id: prod.id,
                                      name: prod.name,
                                      detail: prod.detail,
                                      price: prod.price,
                                      promotion_price: prod.promotion_price,
                                      path_s3: prod.Imageproducts[0].path_s3
                                        ? prod.Imageproducts[0].path_s3
                                        : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000",
                                      qtd: dataNow.qtd + 1,
                                    });

                                    setItemsCarsSales("ItemsCarsSales", data, {
                                      path: "/",
                                    });
                                  }}
                                  size="small"
                                >
                                  <AddIcon style={{ color: "#2d3436" }} />
                                </IconButton>
                              </Box>
                            ) : (
                              <Button
                                fullWidth
                                onClick={(event) => {
                                  event.stopPropagation();

                                  let dataItems = itemsCarsSales.ItemsCarsSales
                                    ? itemsCarsSales.ItemsCarsSales.filter(
                                        (i, index) => index > -1
                                      )
                                    : [];

                                  dataItems.push({
                                    id: prod.id,
                                    name: prod.name,
                                    detail: prod.detail,
                                    price: prod.price,
                                    promotion_price: prod.promotion_price,
                                    path_s3: prod.Imageproducts[0].path_s3
                                      ? prod.Imageproducts[0].path_s3
                                      : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000",
                                    qtd: 1,
                                  });
                                  setItemsCarsSales(
                                    "ItemsCarsSales",
                                    dataItems,
                                    {
                                      path: "/",
                                    }
                                  );
                                }}
                                style={{
                                  borderRadius: 10,
                                  backgroundColor: "#2ecc71",
                                  color: "white",
                                }}
                                size="small"
                                className="mt-4"
                                variant="contained"
                              >
                                Adicionar
                              </Button>
                            )}

                            <Box
                              className="mt-2 mb-2"
                              display="flex"
                              alignItems="center"
                              justifyContent="space-between"
                            >
                              <Box display="flex" alignItems="center">
                                <FavoriteIcon style={{ color: "#e74c3c" }} />

                                <h6
                                  style={{
                                    color: "#485460",
                                    fontWeight: "600",
                                    fontSize: 12,
                                  }}
                                  className="text-black ml-2"
                                >
                                  {/*prod.like*/}
                                </h6>
                              </Box>
                              <Box display="flex" alignItems="center">
                                <SpeakerNotesOutlinedIcon />

                                <h6
                                  style={{
                                    color: "#485460",
                                    fontWeight: "600",
                                    fontSize: 12,
                                  }}
                                  className="text-black ml-2"
                                >
                                  {/*prod.comment*/}
                                </h6>
                              </Box>
                            </Box>
                          </div>
                        </Box>
                      </div>
                    ))}
              </Box>
            </PerfectScrollbar>
          </Card>
        ))}
      <div style={{ height: 50 }} />
    </div>
  );
};

export default Commerce;
