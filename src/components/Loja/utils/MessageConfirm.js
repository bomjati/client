import React, { useState } from 'react'
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    useMediaQuery
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';

const MessageConfirm = ({ title, textInfo, open, onClose, onAccept }) => {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <div>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={onClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText>{textInfo}</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        autoFocus
                        onClick={onClose}
                        color="primary">Não</Button>
                    <Button
                        onClick={onAccept}
                        color="primary"
                        autoFocus>Sim</Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default MessageConfirm;