import { store } from 'react-notifications-component';
export const showMessage = (title, info, color) => {
    store.addNotification({
      title: title,
      message: info,
      type: color,
      insert: "top",
      container: "top-right",
      animationIn: ["animated", "bounceIn"],
      animationOut: ["animated", "bounceOut"],
      dismiss: {
        duration: color == "success" ? 1000 : 5000,
        onScreen: true
      }
    });
  } 