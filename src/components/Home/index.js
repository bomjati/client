import React, { Component } from "react";

import Header from "../HeaderSection/Header";
import HeroSection from "../HeroSection/HeroPrincipal";
import FeatureSection from "../Features/FeaturePrincipal";
import ContactSection from "../ContactSection/Contact";
import FooterSection from "../FooterSection/Footer";
import { Widget } from "react-jivosite";

class ThemeFive extends Component {
  render() {
    return (
      <div className="homepage-5">
        {/*====== Scroll To Top Area Start ======*/}
        <div id="scrollUp" title="Scroll To Top">
          <i className="fas fa-arrow-up" />
        </div>
        {/*====== Scroll To Top Area End ======*/}
        <div className="main">
          <Header imageData={"/img/logo.png"} />
          <HeroSection />
          <FeatureSection />
          <ContactSection />
          <FooterSection />
          <Widget id="5WZSPYPlo1" />
        </div>
      </div>
    );
  }
}

export default ThemeFive;
