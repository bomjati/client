import React from 'react';

function Header({imageData}) {
    return (
        <header className="navbar navbar-sticky navbar-expand-lg navbar-dark">
            <div className="container position-relative">
                <a className="navbar-brand" href="/">
                    <img className="navbar-brand-regular" src={imageData} alt="brand-logo" />
                    <img className="navbar-brand-sticky" src="/img/logo.png" alt="sticky brand-logo" />
                </a>
                <button className="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="navbar-inner">
                    {/*  Mobile Menu Toggler */}
                    <button className="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <nav>
                        <ul className="navbar-nav" id="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link" href="/home">Principal</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link scroll" href="#features">Funcionalidades</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link scroll" href="#contact">Contato</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    );
}

export default Header;