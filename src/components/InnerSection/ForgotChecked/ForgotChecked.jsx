import React, { Component } from 'react';
import Header from '../../HeaderSection/Header';

function SignupChecked() {
    const initData = {
        heading: "Enviado com sucesso!",
        content: "Agora é só ir em seu e-mail, e clicar no link de recuperação de senha.",
        btnText: "Pagina Principal"
    }

    return (
        <div className="inner inner-pages">
            <div className="main">
                {/* <Header imageData={"/img/logo.png"} /> */}
                <section id="home" className="section welcome-area inner-area bg-overlay h-100vh overflow-hidden">
                    <div className="container h-100">
                        <div className="row align-items-center h-100">
                            <div className="col-12 col-md-8">
                                <div className="welcome-intro">
                                    <h1 className="text-white">{initData.heading}</h1>
                                    <p className="text-white my-4">{initData.content}</p>
                                    <a href="/" className="btn sApp-btn text-uppercase">{initData.btnText}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}


export default SignupChecked;