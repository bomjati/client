import React from 'react';
import ContactForm from '../ContactForm';

function ContactSection() {
    const iconList = [
        {
            "id": 1,
            "iconClass": "fas fa-home",
            "text": "Rua Sertaneja, 135, Centro, Ivaiporã - PR"
        },
        {
            "id": 2,
            "iconClass": "fas fa-phone-alt",
            "text": "(43) 0000-0000"
        },
        {
            "id": 3,
            "iconClass": "fas fa-envelope",
            "text": "suporte@bonja.com"
        }
    ]

    return (
        <section id="contact" className="contact-area bg-gray ptb_100">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 col-md-10 col-lg-6">
                        {/* Section Heading */}
                        <div className="section-heading text-center">
                            <h2 className="text-capitalize">Contate-nos</h2>
                            <p className="d-none d-sm-block mt-4">Em caso de dúvidas, reclações ou elogios mande-nos sua mensagem ou entre em contato através dos informativos abaixo.</p>
                            <p className="d-block d-sm-none mt-4">Em caso de dúvidas, reclações ou elogios mande-nos sua mensagem.</p>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-between">
                    <div className="col-12 col-md-5">
                        {/* Contact Us */}
                        <div className="contact-us">
                            <p className="mb-3">Melhorando a qualidade, automaticamente você estará melhorando a produtividade. Entre em contato, e te mostraremos o caminho!</p>
                            <ul>
                                {iconList.map((item, idx) => {
                                    return (
                                        <li key={`ci_${idx}`} className="py-2">
                                            <a className="media" href="/#">
                                                <div className="social-icon mr-3">
                                                    <i className={item.iconClass} />
                                                </div>
                                                <span className="media-body align-self-center">{item.text}</span>
                                            </a>
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 pt-4 pt-md-0">
                        <ContactForm />
                    </div>
                </div>
            </div>
        </section>
    );
}


export default ContactSection;