import React, { useState } from "react";


function ContactForm() {
  const myForm = React.createRef()

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [subject, setSubject] = useState("");
  const [message, setMessage] = useState("");

  const submitHangler = (event) => {
    event.preventDefault();
    // console.log(this.state)
    myForm.current.reset()
    setName("")
    setEmail("")
    setSubject("")
    setMessage("")
    //console.log(this.state)
  }

  return (
    <div className="contact-box text-center">
      <form
        ref={myForm}
        onSubmit={submitHangler}
        className="contact-form"
        noValidate="novalidate"
      >
        <div className="row">
          <div className="col-12">
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                name="name"
                placeholder="Nome Completo"
                required="required"
                onChange={(event) => {
                  setName(event.target.value)
                }}
                value={name}
              />
            </div>
            <div className="form-group">
              <input
                type="email"
                className="form-control"
                name="email"
                placeholder="E-mail"
                required="required"
                onChange={(event) => {
                  setName(event.target.value)
                }}
                value={email}
              />
            </div>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                name="subject"
                placeholder="Assunto"
                required="required"
                onChange={(event) => {
                  setName(event.target.value)
                }}
                value={subject}
              />
            </div>
          </div>
          <div className="col-12">
            <div className="form-group">
              <textarea
                className="form-control"
                name="message"
                placeholder="Mensagem"
                required="required"
                onChange={(event) => {
                  setName(event.target.value)
                }}
                value={message}
              />
            </div>
          </div>
          <div className="col-12">
            <button
              type="submit"
              className="btn btn-lg btn-block mt-3"><span className="text-white pr-3"><i className="fas fa-paper-plane" /></span>
                    Enviar Mensagem
                </button>
          </div>
        </div>
      </form>
    </div>
  );
}


export default ContactForm;