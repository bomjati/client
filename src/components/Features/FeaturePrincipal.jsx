import React from 'react';

function FeatureSection() {
    const initData = {
        preHeading: "Fatores ",
        preHeadingspan: "Premium",
        heading: "Funcionalidades Oferecidas",
        headingText: "Leve tempo para tomar decisões, mas quando chegar a hora, pare de pensar e aja, veja como podemos te ajudara melhorar a dinâmica com seu cliente.",
        headingTexttwo: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati."
    }
    
    const data = [
        {
            image: "/img/featured_image_1.png",
            title: "Fully functional",
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis culpa expedita dignissimos."
        },
        {
            image: "/img/featured_image_2.png",
            title: "Live Chat",
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis culpa expedita dignissimos."
        },
        {
            image: "/img/featured_image_3.png",
            title: "Secure Data",
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis culpa expedita dignissimos."
        },
        {
            image: "/img/featured_image_4.png",
            title: "Easy to Customize",
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis culpa expedita dignissimos."
        },
        {
            image: "/img/featured_image_5.png",
            title: "Responsive Design",
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis culpa expedita dignissimos."
        },
        {
            image: "/img/featured_image_6.png",
            title: "Help Documentation",
            content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis culpa expedita dignissimos."
        }
    ]

    return (
        <section id="features" className="section features-area style-two overflow-hidden ptb_100">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-10 col-lg-6">
                        {/* Section Heading */}
                        <div className="section-heading">
                            <span className="d-inline-block rounded-pill shadow-sm fw-5 px-4 py-2 mb-3">
                                <i className="far fa-lightbulb text-primary mr-1" />
                                <span className="text-primary">{initData.preHeading}</span>
                                {initData.preHeadingspan}
                            </span>
                            <h2>{initData.heading}</h2>
                            <p className="d-none d-sm-block mt-4">{initData.headingText}</p>
                            <p className="d-block d-sm-none mt-4">{initData.headingText}</p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {data.map((item, idx) => {
                        return (
                            <div key={`ffd_${idx}`} className="col-12 col-md-6 col-lg-4 my-3 res-margin">
                                {/* Image Box */}
                                <div className="image-box text-center icon-1 p-5">
                                    {/* Featured Image */}
                                    <div className="featured-img mb-3">
                                        <img className="avatar-sm" src={item.image} alt="" />
                                    </div>
                                    {/* Icon Text */}
                                    <div className="icon-text">
                                        <h3 className="mb-2">{item.title}</h3>
                                        <p>{item.content}</p>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
}


export default FeatureSection;