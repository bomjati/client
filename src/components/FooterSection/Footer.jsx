import React from 'react';

function FooterSection() {
    const data = {
        image: "/img/logo.png",
        text: "Bomja sempre buscando o melhor para você."
    }

    const iconList = [
        {
            "id": 1,
            "link": "facebook",
            "iconClass": "fab fa-facebook-f"
        },
        {
            "id": 2,
            "link": "twitter",
            "iconClass": "fab fa-twitter"
        },
        {
            "id": 3,
            "link": "google-plus",
            "iconClass": "fab fa-google-plus-g"
        },
        {
            "id": 4,
            "link": "vine",
            "iconClass": "fab fa-vine"
        }
    ]

    return (
        <div>
            <div className=" d-none d-lg-block" />
            <footer className="footer-area ">
                {/* Footer Top */}
                <div className="footer-top ptb_100">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-sm-6 col-lg-3">
                                {/* Footer Items */}
                                <div className="footer-items">
                                    {/* Logo */}
                                    <a className="navbar-brand" href="/#">
                                        <img className="logo" src={data.image} alt="" />
                                    </a>
                                    <p className="mt-2 mb-3">{data.text}</p>
                                    {/* Social Icons */}
                                    <div className="social-icons d-flex">
                                        {iconList.map((item, idx) => {
                                            return (
                                                <a key={`fi_${idx}`} className={item.link} href="/#">
                                                    <i className={item.iconClass} />
                                                    <i className={item.iconClass} />
                                                </a>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Footer Bottom */}
                <div className="footer-bottom">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                {/* Copyright Area */}
                                <div className="copyright-area d-flex flex-wrap justify-content-center justify-content-sm-between text-center py-4">
                                    {/* Copyright Left */}
                                    <div className="copyright-left">© Copyrights {new Date().getFullYear()} reservado todos os direitos.</div>
                                    {/* Copyright Right */}
                                    <div className="copyright-right"><a href="/#">Bomja Tecnologia e Marketing</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default FooterSection;