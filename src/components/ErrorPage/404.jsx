import React from 'react';

function ErrorPage() {
    const classNameinitData = {
        heading: "404",
        text: "Erro inesperado foi encontrado. Talvez a página a qual deseja entrar nâo exista.",
        btnText: "Pagina Principal"
    }

    return (
        <div className="inner inner-pages">
            <div className="main">
                <section id="home" className="section welcome-area inner-area bg-overlay h-100vh overflow-hidden">
                    <div className="container h-100">
                        <div className="row align-items-center justify-content-center h-100">
                            <div className="col-12 col-md-7">
                                <div className="welcome-intro error-area text-center">
                                    <h1 className="text-white">{classNameinitData.heading}</h1>
                                    <p className="text-white my-4">{classNameinitData.text}</p>
                                    <a href="/" className="btn sApp-btn text-uppercase">{classNameinitData.btnText}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}


export default ErrorPage;