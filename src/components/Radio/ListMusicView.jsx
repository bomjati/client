import React, {
  useState,
  useEffect,
  useRef,
  MouseEvent,
  useCallback,
} from "react";
import {
  Grid,
  Container,
  Box,
  Avatar,
  Typography,
  Divider,
  Hidden,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Zoom,
  Slide,
} from "@material-ui/core";
import {
  PlayArrow as PlayArrowIcon,
  MoreVert as MoreVertIcon,
  Block as BlockIcon,
  Favorite as FavoriteIcon,
  Lock as LockIcon,
  AspectRatio as AspectRatioIcon,
} from "@material-ui/icons";
import "./App.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import { FullScreen, useFullScreenHandle } from "react-full-screen";

const ListMusicView = ({
  capa,
  listMusic,
  onPlayMusic,
  idMusicSelect,
  isPlayList,
  isPlayListSub,
  listBlocked,
  listFavorited,
  onLikeFavorite,
  onBlock,
  onDesLikeFavorite,
  onDesBlock,
  listPhrases,
  genres,
  client,
  listSegmentsCovers,
}) => {
  const [limitMusic, setLimitMusic] = useState(20);
  const [visibleOpenWindow, setVisibleOpenWindow] = useState(false);
  const [openFulscreen, setOpenFullscreen] = useState(false);
  const screen1 = useFullScreenHandle();
  const [pageMusic, setPageMusic] = useState(0);
  const [indexPhrase, setIndexPhrase] = useState(0);
  const [openMusicSelect, setOpenMusicSelect] = useState({
    id: 0,
    active: null,
  });
  const [visibleMenuMusicSelect, setVisibleMenuMusicSelect] = useState({
    id: 0,
    active: false,
  });
  const scrollListMusic = useRef(null);

  useEffect(() => {
    const id = setInterval(() => {
      //console.log(indexPhrase);
      setIndexPhrase(
        listPhrases.length == indexPhrase + 1 ? 0 : indexPhrase + 1
      );
    }, 15000);

    return () => {
      clearTimeout(id);
    };
  }, [indexPhrase]);

  const onAddMusicList = () => {
    setLimitMusic(listMusic.length > limitMusic ? limitMusic + 10 : limitMusic);
  };

  const handleClose = (id) => {
    setOpenMusicSelect({
      id: 0,
      active: null,
    });
  };

  const handleClick = (event: MouseEvent<HTMLButtonElement>, id) => {
    setOpenMusicSelect({
      id: id,
      active: event.currentTarget,
    });
  };

  const reportChange = useCallback(
    (state, handle) => {
      if (handle === screen1) {
        if (state) {
          setOpenFullscreen(true);
        } else {
          setOpenFullscreen(false);
        }
      }
    },
    [screen1]
  );

  return (
    <div
      style={{
        marginTop: 75,
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        background: "black",
        position: "absolute",
        zIndex: 10
      }}
    >
      <div style={{ background: "black" }}>
        <FullScreen handle={screen1} onChange={reportChange}>
          {openFulscreen ? (
            <div
              style={{
                width: "100%",
                height: "100%",
                backgroundColor: "white",
              }}
            >
              {String(listMusic[idMusicSelect].artist).indexOf("Conteúdo") >
              -1 ? (
                listSegmentsCovers &&
                listSegmentsCovers.map(
                  (l, index) =>
                    index == indexPhrase && (
                      <Slide
                        direction="left"
                        in={true}
                        timeout={500}
                        mountOnEnter
                        unmountOnExit
                      >
                        <img
                          onMouseEnter={() => setVisibleOpenWindow(true)}
                          onMouseLeave={() => setVisibleOpenWindow(false)}
                          style={{
                            display: "block",
                            objectFit: "cover",
                            width: "100%",
                            height: "100%",
                            zIndex: 1,
                            position: "relative",
                          }}
                          src={
                            listSegmentsCovers[
                              Math.floor(
                                Math.random() * listSegmentsCovers.length
                              )
                            ].path_s3
                          }
                        />
                      </Slide>
                    )
                )
              ) : (
                <div
                  style={{
                    width: "100%",
                    height: "100%",
                  }}
                >
                  {listPhrases.map(
                    (phrase, index) =>
                      index == indexPhrase && (
                        <Box
                          display="flex"
                          justifyContent="flex-end"
                          alignItems="center"
                          style={{
                            position: "absolute",
                            width: "100%",
                            height: "100%",
                          }}
                        >
                          <Slide
                            direction="left"
                            in={true}
                            timeout={500}
                            mountOnEnter
                            unmountOnExit
                          >
                            <Typography
                              variant="h1"
                              gutterBottom
                              style={{
                                fontFamily: "Anton",
                                color: client.Clientsettings[0].colorFont,
                                zIndex: 1,
                                position: "relative",
                                //textAlign: "center",
                                width: 750,
                              }}
                            >
                              {phrase.phrase}
                            </Typography>
                          </Slide>
                        </Box>
                      )
                  )}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    height="120%"
                    viewBox="0 0 1440 320"
                  >
                    <path
                      fill={client.Clientsettings[0].colorBackground}
                      fill-opacity="1"
                      d="M0,160L80,186.7C160,213,320,267,480,272C640,277,800,235,960,213.3C1120,192,1280,192,1360,192L1440,192L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z"
                    ></path>
                  </svg>
                </div>
              )}
            </div>
          ) : (
            <Container maxWidth="lg">
              <Grid container spacing={3}>
                <Grid item lg={6} md={6} xs={12}>
                  <Hidden smDown>
                    <Zoom in={true} style={{ transitionDuration: "800ms" }}>
                      <div>
                        {!visibleOpenWindow ? null : (
                          <div
                            onMouseEnter={() => setVisibleOpenWindow(true)}
                            onMouseLeave={() => setVisibleOpenWindow(true)}
                            style={{
                              zIndex: 10,
                              position: "absolute",
                              backgroundImage:
                                "linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(0,0,0,0) 100%)",
                              width: 600,
                              height: 100,
                            }}
                          >
                            <Box display="flex" justifyContent="flex-end">
                              <IconButton
                                style={{ marginTop: 20, marginRight: 20 }}
                                onClick={() => {
                                  setOpenFullscreen(true);
                                  screen1.enter();
                                }}
                              >
                                <AspectRatioIcon style={{ color: "white" }} />
                              </IconButton>
                            </Box>
                          </div>
                        )}
                        <img
                          onMouseEnter={() => setVisibleOpenWindow(true)}
                          onMouseLeave={() => setVisibleOpenWindow(false)}
                          style={{
                            borderRadius: 5,
                            marginRight: -20,
                          }}
                          src={
                            isPlayList
                              ? (listMusic[idMusicSelect].artist ==
                                  "Comercial") |
                                (listMusic[idMusicSelect].artist == "Vinheta") |
                                (String(
                                  listMusic[idMusicSelect].artist
                                ).indexOf("Conteúdo") >
                                  -1)
                                ? "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
                                : capa
                              : isPlayListSub
                              ? (listMusic[idMusicSelect].artist ==
                                  "Comercial") |
                                (listMusic[idMusicSelect].artist == "Vinheta") |
                                (String(
                                  listMusic[idMusicSelect].artist
                                ).indexOf("Conteúdo") >
                                  -1)
                                ? capa
                                : capa
                              : String(listMusic[idMusicSelect].artist).indexOf(
                                  "Conteúdo"
                                ) > -1
                              ? listMusic[idMusicSelect].cover
                                ? listMusic[idMusicSelect].cover
                                : listMusic[idMusicSelect].Genre
                                ? listMusic[idMusicSelect].Genre.Covers[0]
                                    .path_s3
                                : capa
                              : listMusic[idMusicSelect].Genre
                              ? listMusic[idMusicSelect].Genre.Covers[0].path_s3
                              : capa
                          }
                        />
                      </div>
                    </Zoom>
                  </Hidden>
                </Grid>
                <Grid item lg={6} md={6} xs={12}>
                  <div
                    style={{
                      top: 0,
                      bottom: 0,
                      height: "100%",
                    }}
                  >
                    <div style={{ height: "75vh" }}>
                      <h4 className="text-white ml-4">Lista de Musicas</h4>
                      <PerfectScrollbar
                        onYReachEnd={() => {
                          onAddMusicList();
                        }}
                        ref={scrollListMusic}
                      >
                        {listMusic
                          .slice(
                            pageMusic * limitMusic,
                            pageMusic * limitMusic + limitMusic
                          )
                          .map((music, index) => {
                            return (
                              <div
                                onClick={() => {
                                  onPlayMusic(index);
                                }}
                                onMouseEnter={() => {
                                  setVisibleMenuMusicSelect({
                                    id: music.id,
                                    active: true,
                                  });
                                }}
                                onMouseLeave={() => {
                                  setVisibleMenuMusicSelect({
                                    id: music.id,
                                    active: false,
                                  });
                                }}
                                key={index}
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  borderRadius: 10,
                                  borderWidth: 2,
                                  borderStyle: "solid",
                                  borderColor:
                                    music.artist == "Comercial"
                                      ? "#00a8ff"
                                      : music.artist == "Vinheta"
                                      ? "#FFC312"
                                      : String(music.artist).indexOf(
                                          "Conteúdo"
                                        ) > -1
                                      ? "#9b59b6"
                                      : "#26de81",
                                }}
                                className="w-100 mb-2 ct"
                              >
                                <div
                                  style={{
                                    height: 65,
                                    width: 10,
                                    marginLeft: -2,
                                    borderTopLeftRadius: 7,
                                    borderBottomLeftRadius: 7,
                                    backgroundColor:
                                      music.artist == "Comercial"
                                        ? "#00a8ff"
                                        : music.artist == "Vinheta"
                                        ? "#FFC312"
                                        : String(music.artist).indexOf(
                                            "Conteúdo"
                                          ) > -1
                                        ? "#9b59b6"
                                        : "#26de81",
                                  }}
                                />
                                <Grid item lg={12} md={12} xs={12}>
                                  <Box
                                    style={{
                                      backgroundColor:
                                        index == idMusicSelect
                                          ? "rgba(255,255,255,0.1)"
                                          : "transparent",
                                    }}
                                    className="p-1"
                                    display="flex"
                                    alignItems="center"
                                    justifyContent="space-between"
                                  >
                                    <Box
                                      className="p-1"
                                      display="flex"
                                      alignItems="center"
                                      justifyContent="flex-start"
                                    >
                                      <div
                                        onClick={() => {
                                          onPlayMusic(index);
                                        }}
                                      >
                                        <Avatar
                                          alt="Product"
                                          variant="square"
                                          src={
                                            isPlayList
                                              ? (music.artist == "Comercial") |
                                                (music.artist == "Vinheta") |
                                                (String(music.artist).indexOf(
                                                  "Conteúdo"
                                                ) >
                                                  -1)
                                                ? "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
                                                : capa
                                              : isPlayListSub
                                              ? (music.artist == "Comercial") |
                                                (music.artist == "Vinheta") |
                                                (String(music.artist).indexOf(
                                                  "Conteúdo"
                                                ) >
                                                  -1)
                                                ? music.cover
                                                  ? music.cover
                                                  : music.Genre
                                                  ? music.Genre.Covers[0]
                                                      .path_s3
                                                  : capa
                                                : music.Genre
                                                ? music.Genre.Covers[0].path_s3
                                                : capa
                                              : String(music.artist).indexOf(
                                                  "Conteúdo"
                                                ) > -1
                                              ? music.cover
                                                ? music.cover
                                                : music.Genre
                                                ? music.Genre.Covers[0].path_s3
                                                : capa
                                              : music.Genre
                                              ? music.Genre.Covers[0].path_s3
                                              : capa
                                          }
                                          className="av-radio mr-2"
                                        />
                                        <PlayArrowIcon className="av-play" />
                                      </div>
                                      <div>
                                        <div className="d-flex">
                                          <h5 className="text-white mr-4">
                                            {String(
                                              isPlayList
                                                ? music.title
                                                : music.title
                                            ).replace(".mp3", "")
                                            .replace(".m4a", "")}
                                          </h5>
                                          {listBlocked.filter((b) =>
                                            String(music.artist)
                                              .toLowerCase()
                                              .indexOf("vinheta") > -1
                                              ? b.vignetteId == music.id
                                              : String(music.artist)
                                                  .toLowerCase()
                                                  .indexOf("comercial") > -1
                                              ? b.commercialId == music.id
                                              : String(music.artist)
                                                  .toLowerCase()
                                                  .indexOf("conteúdo") > -1
                                              ? b.contentId == music.id
                                              : b.musicId == music.id
                                          ).length > 0 ? (
                                            <div
                                              style={{ alignItems: "center" }}
                                              className="d-flex"
                                            >
                                              <h5
                                                style={{ color: "#e67e22" }}
                                                className="mr-1"
                                              >
                                                (Bloqueado)
                                              </h5>
                                              <LockIcon
                                                style={{ color: "#e67e22" }}
                                                fontSize="small"
                                              />
                                            </div>
                                          ) : null}
                                        </div>
                                        <Typography
                                          style={{ color: "white" }}
                                          variant="body1"
                                        >
                                          {isPlayList
                                            ? music.artist
                                            : music.artist}
                                        </Typography>
                                      </div>
                                    </Box>
                                    {visibleMenuMusicSelect.id == music.id ? (
                                      <div>
                                        <IconButton
                                          onClick={(event) => {
                                            event.stopPropagation();
                                            handleClick(event, music.id);
                                          }}
                                        >
                                          <MoreVertIcon
                                            style={{
                                              color: "rgba(255,255,255, 0.5)",
                                            }}
                                          />
                                        </IconButton>
                                        <Menu
                                          id="simple-menu"
                                          anchorEl={
                                            openMusicSelect.id == music.id
                                              ? openMusicSelect.active
                                              : null
                                          }
                                          PaperProps={{
                                            style: {
                                              backgroundColor: "#303030",
                                            },
                                          }}
                                          keepMounted
                                          open={Boolean(
                                            openMusicSelect.id == music.id
                                              ? openMusicSelect.active
                                              : null
                                          )}
                                          onClose={handleClose}
                                        >
                                          <MenuItem
                                            onClick={() => {
                                              handleClose(music.id);
                                              const arrayBlock = listBlocked.filter(
                                                (b) =>
                                                  String(music.artist)
                                                    .toLowerCase()
                                                    .indexOf("vinheta") > -1
                                                    ? b.vignetteId == music.id
                                                    : String(music.artist)
                                                        .toLowerCase()
                                                        .indexOf("comercial") >
                                                      -1
                                                    ? b.commercialId == music.id
                                                    : String(music.artist)
                                                        .toLowerCase()
                                                        .indexOf("conteúdo") >
                                                      -1
                                                    ? b.contentId == music.id
                                                    : b.musicId == music.id
                                              );
                                              if (arrayBlock.length > 0) {
                                                onDesBlock(arrayBlock[0].id);
                                              } else {
                                                onBlock(music.id, music.artist);
                                              }
                                            }}
                                          >
                                            <ListItemIcon>
                                              <BlockIcon
                                                style={{
                                                  color:
                                                    "rgba(255,255,255,0.9)",
                                                }}
                                              />
                                            </ListItemIcon>
                                            <ListItemText
                                              style={{
                                                color: "rgba(255,255,255,0.9)",
                                              }}
                                            >
                                              {listBlocked.filter((b) =>
                                                String(music.artist)
                                                  .toLowerCase()
                                                  .indexOf("vinheta") > -1
                                                  ? b.vignetteId == music.id
                                                  : String(music.artist)
                                                      .toLowerCase()
                                                      .indexOf("comercial") > -1
                                                  ? b.commercialId == music.id
                                                  : String(music.artist)
                                                      .toLowerCase()
                                                      .indexOf("conteúdo") > -1
                                                  ? b.contentId == music.id
                                                  : b.musicId == music.id
                                              ).length > 0
                                                ? "Desbloquear"
                                                : "Bloquear"}
                                            </ListItemText>
                                          </MenuItem>
                                          <MenuItem
                                            onClick={() => {
                                              handleClose(music.id);
                                              const arrayFavorited = listFavorited.filter(
                                                (b) =>
                                                  String(music.artist)
                                                    .toLowerCase()
                                                    .indexOf("vinheta") > -1
                                                    ? b.vignetteId == music.id
                                                    : String(music.artist)
                                                        .toLowerCase()
                                                        .indexOf("comercial") >
                                                      -1
                                                    ? b.commercialId == music.id
                                                    : String(music.artist)
                                                        .toLowerCase()
                                                        .indexOf("conteúdo") >
                                                      -1
                                                    ? b.contentId == music.id
                                                    : b.musicId == music.id
                                              );
                                              if (arrayFavorited.length > 0) {
                                                onDesLikeFavorite(
                                                  arrayFavorited[0].id
                                                );
                                              } else {
                                                onLikeFavorite(
                                                  music.id,
                                                  music.artist
                                                );
                                              }
                                            }}
                                          >
                                            <ListItemIcon>
                                              <FavoriteIcon
                                                style={{
                                                  color:
                                                    "rgba(255,255,255,0.9)",
                                                }}
                                              />
                                            </ListItemIcon>
                                            <ListItemText
                                              style={{
                                                color: "rgba(255,255,255,0.9)",
                                              }}
                                            >
                                              {listFavorited.filter((b) =>
                                                String(music.artist)
                                                  .toLowerCase()
                                                  .indexOf("vinheta") > -1
                                                  ? b.vignetteId == music.id
                                                  : String(music.artist)
                                                      .toLowerCase()
                                                      .indexOf("comercial") > -1
                                                  ? b.commercialId == music.id
                                                  : String(music.artist)
                                                      .toLowerCase()
                                                      .indexOf("conteúdo") > -1
                                                  ? b.contentId == music.id
                                                  : b.musicId == music.id
                                              ).length
                                                ? "Desfavoritar"
                                                : "Favoritar"}
                                            </ListItemText>
                                          </MenuItem>
                                        </Menu>
                                      </div>
                                    ) : (
                                      <Typography
                                        style={{ color: "white" }}
                                        variant="body1"
                                      >
                                        {isPlayList
                                          ? music.totalTime
                                          : music.totalTime}
                                      </Typography>
                                    )}
                                  </Box>
                                </Grid>
                                {/* <div
                                  className="w-100"
                                  style={{
                                    background: "rgba(255,255,255,0.2)",
                                    height: 0.09,
                                  }}
                                /> */}
                              </div>
                            );
                          })}
                      </PerfectScrollbar>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Container>
          )}
        </FullScreen>
      </div>
    </div>
  );
};

export default ListMusicView;
