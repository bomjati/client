import React, { useState } from "react";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import {
  PlaylistAdd as PlaylistAddIcon,
  SettingsVoice as SettingsVoiceIcon,
  Assignment as AssignmentIcon,
  DynamicFeed as DynamicFeedIcon,
  PersonAdd as PersonAddIcon,
  List as ListIcon,
  Copyright as CopyrightIcon,
  ThumbUpAlt as ThumbUpAltIcon,
  Settings as SettingsIcon,
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
  Favorite as FavoriteIcon,
  Block as BlockIcon,
  Subscriptions as SubscriptionsIcon,
  Dvr as DvrIcon,
  PlaylistPlay as PlaylistPlayIcon,
  QueueMusic as QueueMusicIcon,
  ContactSupport as ContactSupportIcon,
  RotateLeft as RotateLeftIcon
} from "@material-ui/icons";

const Drawers = ({
  onNewPlaylist,
  onChamada,
  onConfigContent,
  onSuporteWhatsapp,
  onPlayList,
  onNewConteudo,
  onShowContent,
  isShowMenu,
  onClick,
  onKeyDown,
  onClose,
  onDireito,
  onLivre,
  onViewFavorited,
  onViewBlock,
  onShowSuportContact,
  onResetSelected
}) => {
  const [openAcervo, setOpenAcervo] = useState(false);
  const [openCollection, setOpenCollection] = useState(false);
  const [openPlaylist, setOpenPlaylist] = useState(false);
  const listMenu = [
    {
      onClick: () => setOpenAcervo(!openAcervo),
      name: "Acervos",
      icon: <SubscriptionsIcon />,
      list: true,
      listItem: [
        {
          onClick: () => onDireito(),
          name: "Com Direitos",
          icon: <CopyrightIcon />,
          list: false,
        },
        {
          onClick: () => onLivre(),
          name: "Creative Commons",
          icon: <ThumbUpAltIcon />,
          list: false,
        },
      ],
    },
    {
      onClick: () => setOpenCollection(!openCollection),
      name: "Coleções",
      icon: <ListIcon />,
      list: true,
      collection: true,
      listItem: [
        {
          onClick: () => onViewFavorited(),
          name: "Favoritas",
          icon: <FavoriteIcon />,
          list: false,
        },
        {
          onClick: () => onViewBlock(),
          name: "Bloqueadas",
          icon: <BlockIcon />,
          list: false,
        },
      ],
    },
    {
      onClick: () => setOpenPlaylist(!openPlaylist),
      name: "Playlist",
      icon: <QueueMusicIcon />,
      list: true,
      playlist: true,
      listItem: [
        {
          onClick: () => onNewPlaylist(),
          name: "Nova Playlist",
          icon: <PlaylistAddIcon />,
          list: false,
        },
        {
          onClick: () => onPlayList(),
          name: "Minhas Playlists",
          icon: <PlaylistPlayIcon />,
          list: false,
        },
      ],
    },

    /* {
      onClick: () => onNewEmployee(),
      name: "Cadastro de Funcionários",
      icon: <PersonAddIcon />,
    }, */
    {
      onClick: () => onChamada(),
      name: "Avisos",
      icon: <SettingsVoiceIcon />,
      list: false,
    },

    {
      onClick: () => onShowContent(),
      name: "Conteúdos",
      icon: <DynamicFeedIcon />,
      list: false,
    },

    {
      onClick: () => onShowSuportContact(),
      name: "Solicitação de Comerciais",
      icon: <DvrIcon />,
      list: false,
    },
    {
      onClick: () => onResetSelected(),
      name: "Resetar Seleção",
      icon: <RotateLeftIcon />,
      list: false,
    },
    {
      onClick: () => onConfigContent(),
      name: "Configurações",
      icon: <SettingsIcon />,
      list: false,
    },
    {
      onClick: () => onSuporteWhatsapp(),
      name: "Suporte",
      icon: <ContactSupportIcon />,
      list: false,
    },
  ];

  return (
    <Drawer
     variant="temporary"
      anchor={"left"}
      open={isShowMenu}
      onClose={() => {
        onClose();
      }}
    >
      <div
        style={{ width: 235 }}
        role="presentation"
        onClick={() => {
          onClick();
        }}
        onKeyDown={() => {
          onKeyDown();
        }}
      >
        <List>
          {listMenu.map((item) =>
            item.list ? (
              <div
                onClick={(event) => {
                  event.stopPropagation();
                  item.onClick();
                }}
              >
                <ListItem button key={item.name}>
                  <ListItemIcon style={{ color: "red" }}>
                    {item.icon}
                  </ListItemIcon>
                  <ListItemText style={{ fontSize: "9" }} primary={item.name} />

                  {(
                    item.playlist
                      ? openPlaylist
                      : item.collection
                      ? openCollection
                      : openAcervo
                  ) ? (
                    <ExpandLessIcon />
                  ) : (
                    <ExpandMoreIcon />
                  )}
                </ListItem>
                <div style={{ paddingLeft: 20 }}>
                  {(
                    item.playlist
                      ? !openPlaylist
                      : item.collection
                      ? !openCollection
                      : !openAcervo
                  )
                    ? null
                    : item.listItem.map((aitem) => (
                        <div
                          onClick={() => {
                            aitem.onClick();
                            onClick();
                          }}
                        >
                          <ListItem button key={aitem.name}>
                            <ListItemIcon style={{ color: "red" }}>
                              {aitem.icon}
                            </ListItemIcon>
                            <ListItemText
                              style={{ fontSize: "9" }}
                              primary={aitem.name}
                            />
                          </ListItem>
                        </div>
                      ))}
                </div>
              </div>
            ) : (
              <div
                onClick={() => {
                  item.onClick();
                }}
              >
                <ListItem button key={item.name}>
                  <ListItemIcon style={{ color: "red" }}>
                    {item.icon}
                  </ListItemIcon>
                  <ListItemText style={{ fontSize: "9" }} primary={item.name} />
                </ListItem>
              </div>
            )
          )}
        </List>
      </div>
    </Drawer>
  );
};

export default Drawers;
