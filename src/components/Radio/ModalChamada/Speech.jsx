import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Divider,
  TextField,
  Slider,
  Switch,
  Grid,
  Typography,
  IconButton,
  Box,
  Button,
  CircularProgress,
  FormControl,
  FormGroup,
  FormControlLabel,
  InputLabel,
  Select,
  MenuItem,
  InputAdornment
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  PlayArrow as PlayArrowIcon,
  Stop as StopIcon,
  Delete as DeleteIcon,
  Hearing as HearingIcon,
  Save as SaveIcon
} from "@material-ui/icons";
import PerfectScrollbar from "react-perfect-scrollbar";
import { frases, frases2, marcas, cores } from "./fraseData";
import api from "../../../services/api";
import { showMessage } from "../utils/message";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "center"
    },
    [breakpoints.up("sm")]: {
      alignItems: "center"
    },
    justifyContent: "center"
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "red" },
    backgroundColor: "red",
    color: "white"
  }
}));

const Speech = ({
  clientId,
  onCloseSpeech,
  onPlayingSpeech,
  open,
  isMobile
}) => {
  const classes = useStyles();
  const [audioTeste, setAudioTeste] = useState(new Audio(""));
  const [audioVeiculo, setAudioVeiculo] = useState(new Audio(""));
  const [audioInfoVeiculo, setAudioInfoVeiculo] = useState(new Audio(""));

  const [audio, setAudio] = useState(new Audio(""));
  const [playing, setPlaying] = useState(false);
  const [audioUrlFrase1, setAudioUrlFrase1] = useState("");

  const [audioPessoa, setAudioPessoa] = useState(new Audio(""));
  const [playingPessoa, setPlayingPessoa] = useState(false);
  const [audioUrlPessoaFem, setAudioUrlPessoaFem] = useState("");
  const [audioUrlPessoaMale, setAudioUrlPessoaMale] = useState("");

  const [audioFrase2, setAudioFrase2] = useState(new Audio(""));
  const [playingFrase2, setPlayingFrase2] = useState(false);
  const [audioUrlFrase2, setAudioUrlFrase2] = useState("");

  const [frase, setFrase] = useState("");
  const [frase1, setFrase1] = useState("");
  const [infoUrlVeiculo, setInfoUrlVeiculo] = useState("");
  const [fraseUrlVeiculo, setFraseUrlVeiculo] = useState("");
  const [nameEmployees, setNameEmployees] = useState("");
  const [nameEmployeesCustom, setNameEmployeesCustom] = useState("");
  const [nameMarca, setNameMarca] = useState("");
  const [nameCor, setNameCor] = useState("");
  const [numberPlaca, setNumberPlaca] = useState("");
  const [selectTypesChamadas, setSelectTypesChamadas] = useState("");
  const [velocidade, setVelocidade] = useState(1);
  const [employeeId, setEmployeeId] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [employees, setEmployees] = useState([]);
  const [voiceSelect, setVoiceSelect] = useState(false); //Fem & Male
  const [typesChamadas] = useState([
    { name: "Funcionário" },
    { name: "Veiculo" }
  ]);
  // const [fala] = useState(new SpeechSynthesisUtterance());

  const handleChangeVelocidade = (event, newVelocidade) => {
    setVelocidade(newVelocidade);
  };

  useEffect(() => {
    if (open) {
      onGetEmployee();
      setSelectTypesChamadas("");
      setFrase("");
      setFrase1("");
      setNameEmployees("");
      setNameEmployeesCustom("");
    }
  }, [open]);

  useEffect(() => {
    if (audioUrlFrase1 !== "") {
      console.log(voiceSelect ? audioUrlPessoaMale : audioUrlPessoaFem);
      onNewFile(audioUrlFrase1);
    }
  }, [audioUrlFrase1]);

  const onGetEmployee = () => {
    setIsLoading(true);
    api
      .get(`/employee/${clientId}`)
      .then(response => response.data)
      .then(resp => {
        console.log(resp);
        setIsLoading(false);
        setEmployees(resp);
      })
      .catch(error => {
        showMessage("Error", "Erro, consulte o suporte", "danger");
        setIsLoading(false);
      });
  };

  const onGetPhrasesEmployee = async (phrase1, phrase2) => {
    const pAudioUrlOne = (await api.get(`/ibm/phrase/${phrase1}`)).data;

    setAudioUrlFrase1("");
    setAudioUrlFrase1(
      voiceSelect ? pAudioUrlOne[0].path_s3_Male : pAudioUrlOne[0].path_s3_Fem
    );

    setAudioUrlFrase2("");
    const pAudioUrlTwo = (await api.get(`/ibm/phrase/${phrase2}`)).data;
    setAudioUrlFrase2(
      voiceSelect ? pAudioUrlTwo[0].path_s3_Male : pAudioUrlTwo[0].path_s3_Fem
    );
  };

  /* *************** Chama Teste - Testando nome  *********** */
  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      playTeste();
    }
  }, [audioTeste]);

  const onNewFileTeste = urlMusic => {
    audioTeste.pause();
    audioTeste.src = "";

    //setPlayingAuto(false);
    setAudioTeste(new Audio(urlMusic));
  };

  const playTeste = () => {
    setPlaying(true);
    onPlayingSpeech(true);
    //console.log("teste");
    //audio.autoplay();
    audioTeste.play();
    audioTeste.volume = 1;
    // console.log(voiceSelect ? audioUrlPessoaMale : audioUrlPessoaFem);
    audioTeste.onended = () => {
      stop();
    };
  };
  /* *************************************************************** */

  /* *************** Chama frase 1 - Chamada de Pessoa *********** */
  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      play();
    }
  }, [audio]);

  const onNewFile = urlMusic => {
    audio.pause();
    audio.src = "";

    //setPlayingAuto(false);
    setAudio(new Audio(urlMusic));
  };

  const play = () => {
    setPlaying(true);
    onPlayingSpeech(true);
    //console.log("teste");
    //audio.autoplay();
    audio.play();
    audio.volume = 1;
    // console.log(voiceSelect ? audioUrlPessoaMale : audioUrlPessoaFem);
    audio.onended = () => {
      onNewFilePessoa(voiceSelect ? audioUrlPessoaMale : audioUrlPessoaFem);
    };
  };
  /* *************************************************************** */

  /* *************** Chama frase de Veiculo - Chamada de Veiculo *********** */
  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      playVeiculo();
    }
  }, [audioVeiculo]);

  const onNewFileVeiculo = urlMusic => {
    audioVeiculo.pause();
    audioVeiculo.src = "";

    //setPlayingAuto(false);
    setAudioVeiculo(new Audio(urlMusic));
  };

  const playVeiculo = () => {
    setPlaying(true);
    onPlayingSpeech(true);
    //console.log("teste");
    //audio.autoplay();
    audioVeiculo.play();
    audioVeiculo.volume = 1;
    // console.log(voiceSelect ? audioUrlPessoaMale : audioUrlPessoaFem);
    audioVeiculo.onended = () => {
      onNewFileInfoVeiculo(infoUrlVeiculo);
    };
  };
  /* *************************************************************** */

  /* *************** Chama Informações do Veiculo - Chamada de Veiculo *********** */
  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      playInfoVeiculo();
    }
  }, [audioInfoVeiculo]);

  const onNewFileInfoVeiculo = urlMusic => {
    audioInfoVeiculo.pause();
    audioInfoVeiculo.src = "";

    //setPlayingAuto(false);
    setAudioInfoVeiculo(new Audio(urlMusic));
  };

  const playInfoVeiculo = () => {
    setPlaying(true);
    onPlayingSpeech(true);
    //console.log("teste");
    //audio.autoplay();
    audioInfoVeiculo.play();
    audioInfoVeiculo.volume = 1;
    // console.log(voiceSelect ? audioUrlPessoaMale : audioUrlPessoaFem);
    audioInfoVeiculo.onended = () => {
      stop();
    };
  };
  /* *************************************************************** */

  /* *************** Chama Nome Pessoa - Chamada de Pessoa *********** */
  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      playPessoa();
    }
  }, [audioPessoa]);

  const onNewFilePessoa = urlMusic => {
    audioPessoa.pause();
    audioPessoa.src = "";

    //setPlayingAuto(false);
    setAudioPessoa(new Audio(urlMusic));
  };

  const playPessoa = () => {
    setPlaying(true);
    onPlayingSpeech(true);
    // console.log("teste");
    //audio.autoplay();
    audioPessoa.play();
    audioPessoa.volume = 1;
    audioPessoa.onended = () => {
      onNewFileFrase2(audioUrlFrase2);
    };
  };
  /* *************************************************************** */

  /* *************** Chama frase 2 - Chamada de Pessoa *********** */
  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      playFrase2();
    }
  }, [audioFrase2]);

  const onNewFileFrase2 = urlMusic => {
    audioFrase2.pause();
    audioFrase2.src = "";

    //setPlayingAuto(false);
    setAudioFrase2(new Audio(urlMusic));
  };

  const playFrase2 = () => {
    setPlaying(true);
    onPlayingSpeech(true);
    console.log("teste");
    //audio.autoplay();
    audioFrase2.play();
    audioFrase2.volume = 1;
    audioFrase2.onended = () => {
      stop();
    };
  };
  /* *************************************************************** */

  const stop = () => {
    setPlaying(false);
    onPlayingSpeech(false);
    //speechSynthesis.cancel();
  };

  const onStartCalls = () => {
    if (nameEmployees == "Escrever Nome") {
      stop();
      
      api
        .post("/ibm/speech/azure/employee/new", {
          name: nameEmployeesCustom,
          text: String(nameEmployeesCustom)
            .split("-")[1]
            .trim()
            ? String(nameEmployeesCustom)
                .split("-")[1]
                .trim()
            : String(nameEmployeesCustom)
                .split("-")[0]
                .trim(),
          clientId: clientId
        })
        .then(response => response.data)
        .then(resp => {
          if (resp.message == "success") {
            setAudioUrlPessoaFem(resp.data.path_s3_Fem);
            setAudioUrlPessoaMale(resp.data.path_s3_Male);
            setNameEmployees("");
            setNameEmployeesCustom("");
            onGetEmployee();
            showMessage(
              "Novo Nome",
              `Cadastro realizado com sucesso.`,
              "success"
            );

            onGetPhrasesEmployee(frase1, frase);
          } else {
            showMessage("Novo Funcionário", `Falhou.`, "error");
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      console.log("aquis");
      onGetPhrasesEmployee(frase1, frase);
    }
  };

  const onGetPhrasesVeiculo = () => {
    setIsLoading(true);

    api
      .post(`/ibm/speech/azure/calls`, {
        voice: voiceSelect ? "male" : "fem",
        text: `Marca: ${nameMarca}. Cor: ${nameCor}. Da placa: ${String(
          onOrganizePlaca(numberPlaca)
        ).toUpperCase()}`
      })
      .then(response => response.data)
      .then(async resp => {
        const pInfoUrlOne = (
          await api.get(
            `/ibm/phrase/${"Por favor, compareça ao estacionamento dono do veículo"}`
          )
        ).data;

        setInfoUrlVeiculo(resp.data.path_s3);
        onNewFileVeiculo(
          voiceSelect ? pInfoUrlOne[0].path_s3_Male : pInfoUrlOne[0].path_s3_Fem
        );

        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
        showMessage("Error", "Erro, consulte o suporte", "danger");
        setIsLoading(false);
      });
  };

  const onTestNameAudio = () => {
    if (!String(nameEmployeesCustom).split("-")[1]) {
      showMessage("Atenção", "Preencha o campo antes de ouvir", "warning");
      return;
    }

    setIsLoading(true);

    api
      .post(`/ibm/speech/azure/calls`, {
        text: String(nameEmployeesCustom).split("-")[1]
      })
      .then(response => response.data)
      .then(resp => {
        console.log(resp);

        onNewFileTeste(resp.data.path_s3);

        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
        showMessage("Error", "Erro, consulte o suporte", "danger");
        setIsLoading(false);
      });
  };

  const saveNameEmployee = () => {
    setIsLoading(true);
    api
      .post("/ibm/speech/azure/employee/new", {
        name: nameEmployeesCustom,
        text: String(nameEmployeesCustom)
          .split("-")[1]
          .trim()
          ? String(nameEmployeesCustom)
              .split("-")[1]
              .trim()
          : String(nameEmployeesCustom)
              .split("-")[0]
              .trim(),
        clientId: clientId
      })
      .then(response => response.data)
      .then(resp => {
        if (resp.message == "success") {
          setAudioUrlPessoaFem(resp.data.path_s3_Fem);
          setAudioUrlPessoaMale(resp.data.path_s3_Male);
          setNameEmployees(resp.data.name);
          setNameEmployeesCustom("");
          onGetEmployee();
          showMessage(
            "Novo Nome",
            `Cadastro realizado com sucesso.`,
            "success"
          );
          setIsLoading(false);
        }
      });
  };

  const onDeleteEmployee = id => {
    api
      .delete(`/Employee/${id}`)
      .then(response => response.data)
      .then(resp => {
        if (resp.message == "success") {
          setNameEmployees("");
          setNameEmployeesCustom("");
          onGetEmployee();
          showMessage(
            "Nome Deletado",
            `Cadastro deletado com sucesso.`,
            "success"
          );
        } else {
          showMessage("Deleção Funcionário", `Falhou.`, "error");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onOrganizePlaca = placa => {
    let text = "";

    for (let index = 0; index < String(placa).length; index++) {
      const element = String(placa)[index];

      text =
        text +
        (isNumber(element)
          ? String(element) + " "
          : String(element).toUpperCase() + "...");
    }

    return text;
  };

  const isNumber = i => {
    return i >= "0" && i <= "9";
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={!isLoading && onCloseSpeech}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div>
              <Box
                display="flex"
                justifyContent="flex-end"
                className="mt-4 mb-2 mr-4"
                p={0}
              >
                <IconButton
                  disabled={isLoading}
                  style={{ backgroundColor: "white" }}
                  onClick={onCloseSpeech}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Card
                className="ml-4 mr-4 mb-4"
                style={{
                  borderRadius: 15,
                  width: isMobile == "true" ? "87%" : 720
                }}
              >
                <CardHeader
                  subheader="Frases prontas para chamadas"
                  title="Chamada"
                />

                <Divider />
                <CardContent className="p-4">
                  <Grid container>
                    <Grid item md={12} xs={12}>
                      <FormControl
                        className="mb-4"
                        fullWidth
                        variant="outlined"
                        required
                      >
                        <InputLabel htmlFor="age-native-required">
                          Tipo de Chamada
                        </InputLabel>

                        <Select
                          disabled={isLoading}
                          native
                          value={selectTypesChamadas}
                          name="uf"
                          onChange={event => {
                            setSelectTypesChamadas(`${event.target.value}`);

                            if (event.target.value == "Veiculo") {
                              setNameEmployees("");
                            }
                          }}
                        >
                          {/*Lista de UF*/}
                          <option aria-label="None" value="" />
                          {typesChamadas.map(i => {
                            return <option value={i.name}>{i.name}</option>;
                          })}
                        </Select>
                      </FormControl>
                    </Grid>

                    {selectTypesChamadas == "Funcionário" ? (
                      <FormControl
                        className="mb-4"
                        fullWidth
                        variant="outlined"
                        required
                      >
                        <InputLabel htmlFor="age-native-required">
                          Chamada
                        </InputLabel>

                        <Select
                          disabled={isLoading}
                          native
                          value={frase1}
                          name="uf"
                          onChange={event => {
                            setFrase1(`${event.target.value}`);
                          }}
                        >
                          {/*Lista de UF*/}
                          <option aria-label="None" value="" />
                          {frases2.map(i => {
                            return <option value={i.frases}>{i.frases}</option>;
                          })}
                        </Select>
                      </FormControl>
                    ) : null}

                    {selectTypesChamadas == "Funcionário" ? (
                      <Grid item md={12} xs={12}>
                        <FormControl
                          className="mb-4"
                          fullWidth
                          variant="outlined"
                          required
                        >
                          <InputLabel htmlFor="age-native-required">
                            Funcionários
                          </InputLabel>

                          <Select
                            disabled={isLoading}
                            //native
                            value={nameEmployees}
                            name="uf"
                            onChange={event => {
                              setNameEmployees(`${String(event.target.value)}`);

                              if (event.target.value != "Escrever Nome") {
                                api
                                  .get(
                                    `/employee/call/${
                                      String(event.target.value).split("|")[1]
                                    }`
                                  )
                                  .then(resp => resp.data)
                                  .then(resultEmployee => {
                                    if (
                                      ((resultEmployee[0].path_s3_Fem !== "") &
                                        (String(
                                          resultEmployee[0].path_s3_Fem
                                        ) !==
                                          "null")) |
                                      ((resultEmployee[0].path_s3_Male !== "") &
                                        (String(
                                          resultEmployee[0].path_s3_Male
                                        ) !==
                                          "null"))
                                    ) {
                                      console.log("aqio");
                                      setAudioUrlPessoaFem(
                                        resultEmployee[0].path_s3_Fem
                                      );
                                      setAudioUrlPessoaMale(
                                        resultEmployee[0].path_s3_Male
                                      );
                                    } else {
                                      api
                                        .post(
                                          `/ibm/speech/azure/employee/edit`,
                                          {
                                            id: String(
                                              event.target.value
                                            ).split("|")[1],
                                            text: String(
                                              String(event.target.value).split(
                                                "|"
                                              )[0]
                                            ).split("-")[0]
                                          }
                                        )
                                        .then(resp => resp.data)
                                        .then(resultEmployeeEdit => {
                                          console.log(
                                            `${resultEmployeeEdit.data.path_s3_Fem} --- ${resultEmployeeEdit.data.path_s3_Male}`
                                          );
                                          setAudioUrlPessoaFem(
                                            resultEmployeeEdit.data.path_s3_Fem
                                          );
                                          setAudioUrlPessoaMale(
                                            resultEmployeeEdit.data.path_s3_Male
                                          );
                                          onGetEmployee();
                                        });
                                    }
                                  });
                              }
                            }}
                          >
                            {/*Lista de UF*/}
                            <MenuItem aria-label="None" value="" />
                            <MenuItem
                              style={{ fontWeight: "bold" }}
                              value="Escrever Nome"
                            >
                              Escrever Nome
                            </MenuItem>

                            {employees.map(i => {
                              return (
                                <MenuItem
                                  key={i.name}
                                  value={`${i.name}|${i.id}`}
                                >
                                  <Box
                                    display="flex"
                                    alignItems="center"
                                    justifyContent="space-between"
                                    style={{ width: "100%" }}
                                  >
                                    <div>{String(i.name).split("-")[0]}</div>

                                    <IconButton
                                      onClick={event => {
                                        event.stopPropagation();
                                        onDeleteEmployee(i.id);
                                      }}
                                    >
                                      <DeleteIcon />
                                    </IconButton>
                                  </Box>
                                </MenuItem>
                              );
                            })}
                          </Select>
                        </FormControl>
                      </Grid>
                    ) : null}

                    {nameEmployees == "Escrever Nome" ? (
                      <TextField
                        className="mb-4"
                        fullWidth
                        helperText="Favor escrever o nome como esta no Documento."
                        label="Nome Original: "
                        disabled={isLoading}
                        name="Frase"
                        onChange={event => {
                          let textNow = String(nameEmployeesCustom).split("-");
                          setNameEmployeesCustom(
                            `${event.target.value}-${
                              textNow[1] ? textNow[1] : ""
                            }`
                          );
                        }}
                        required
                        value={String(nameEmployeesCustom).split("-")[0]}
                        variant="outlined"
                      />
                    ) : null}

                    {nameEmployees == "Escrever Nome" ? (
                      <TextField
                        className="mb-4"
                        fullWidth
                        helperText="Favor escrever o nome como se pronúncia exemplo: Schneider se pronuncia Xinaider. Por se tratar de um Inteligência Artificial, terá nomes que precisaram de acentos em determinada letra para que a mesma consiga interpretar da maneira correta."
                        label="Nome para IA: "
                        disabled={isLoading}
                        name="Frase"
                        onChange={event => {
                          let textNow = String(nameEmployeesCustom).split("-");
                          setNameEmployeesCustom(
                            `${textNow[0]}-${event.target.value}`
                          );
                        }}
                        required
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <Box display="flex" alignItems="center">
                                <Button
                                  className="mr-2"
                                  disabled={isLoading}
                                  variant="contained"
                                  onClick={() => {
                                    onTestNameAudio();
                                  }}
                                  style={{
                                    color: "white",
                                    backgroundColor: "#1e272e",
                                    textTransform: "none"
                                  }}
                                  startIcon={
                                    <HearingIcon style={{ color: "white" }} />
                                  }
                                >
                                  Ouvir
                                </Button>
                                <Button
                                  className="ml-2"
                                  disabled={isLoading}
                                  variant="contained"
                                  onClick={() => {
                                    saveNameEmployee();
                                  }}
                                  style={{
                                    color: "white",
                                    backgroundColor: "#2ecc71",
                                    textTransform: "none"
                                  }}
                                  startIcon={
                                    <SaveIcon style={{ color: "white" }} />
                                  }
                                >
                                  Salvar
                                </Button>
                              </Box>
                            </InputAdornment>
                          )
                        }}
                        value={String(nameEmployeesCustom).split("-")[1]}
                        variant="outlined"
                      />
                    ) : null}

                    {selectTypesChamadas == "Veiculo" ? (
                      <FormControl
                        className="mb-4"
                        fullWidth
                        variant="outlined"
                        required
                      >
                        <InputLabel htmlFor="age-native-required">
                          Marca
                        </InputLabel>

                        <Select
                          disabled={isLoading}
                          native
                          value={nameMarca}
                          name="uf"
                          onChange={event => {
                            setNameMarca(`${event.target.value}`);
                          }}
                        >
                          {/*Lista de marcas*/}
                          <option aria-label="None" value="" />
                          {marcas.map(i => {
                            return <option value={i.marca}>{i.marca}</option>;
                          })}
                        </Select>
                      </FormControl>
                    ) : null}

                    {selectTypesChamadas == "Veiculo" ? (
                      <FormControl
                        className="mb-4"
                        fullWidth
                        variant="outlined"
                        required
                      >
                        <InputLabel htmlFor="age-native-required">
                          Cor
                        </InputLabel>

                        <Select
                          disabled={isLoading}
                          native
                          value={nameCor}
                          name="uf"
                          onChange={event => {
                            setNameCor(`${event.target.value}`);
                          }}
                        >
                          {/*Lista de marcas*/}
                          <option aria-label="None" value="" />
                          {cores.map(i => {
                            return <option value={i.cor}>{i.cor}</option>;
                          })}
                        </Select>
                      </FormControl>
                    ) : null}

                    {selectTypesChamadas == "Veiculo" ? null : (
                      <Grid item md={12} xs={12}>
                        <FormControl
                          className="mb-4"
                          fullWidth
                          variant="outlined"
                          required
                        >
                          <InputLabel htmlFor="age-native-required">
                            Texto Pronto
                          </InputLabel>

                          <Select
                            disabled={isLoading}
                            native
                            value={frase}
                            name="uf"
                            onChange={event => {
                              setFrase(`${event.target.value}`);
                            }}
                          >
                            {/*Lista de UF*/}
                            <option aria-label="None" value="" />
                            {frases.map(i => {
                              return (selectTypesChamadas == "Veiculo") &
                                (String(i.frases).indexOf("placa") > -1) ? (
                                <option value={i.frases}>{i.frases}</option>
                              ) : (String(i.frases).indexOf("placa") == -1) &
                                (selectTypesChamadas ==
                                  "Veiculo") ? null : (selectTypesChamadas ==
                                  "Funcionário") &
                                (String(i.frases).indexOf("placa") > -1) &
                                (selectTypesChamadas != "Veiculo") ? null : (
                                <option value={i.frases}>{i.frases}</option>
                              );
                            })}
                          </Select>
                        </FormControl>
                      </Grid>
                    )}

                    {selectTypesChamadas == "Veiculo" ? (
                      <Grid item md={12} xs={12}>
                        <TextField
                          fullWidth
                          label="Digite sua PLACA: "
                          disabled={isLoading}
                          name="Frase"
                          onChange={event => {
                            setNumberPlaca(event.target.value);
                          }}
                          required
                          value={numberPlaca}
                          variant="outlined"
                        />
                      </Grid>
                    ) : null}

                    <Grid item md={12} xs={12}>
                      <Card className="p-2 mt-4 mb-4">
                        <p>Tipo de Voz</p>
                        <Box
                          display="flex"
                          flexDirection="row"
                          alignItems="center"
                        >
                          <Typography
                            className="mr-3"
                            style={{ color: "black" }}
                          >
                            Feminina
                          </Typography>
                          <FormControlLabel
                            control={
                              <Switch
                                checked={voiceSelect}
                                onChange={event =>
                                  setVoiceSelect(event.target.checked)
                                }
                                name="checkedB"
                                color="primary"
                              />
                            }
                            label="Masculino"
                          />
                        </Box>
                      </Card>
                    </Grid>
                  </Grid>
                </CardContent>
                <Box
                  display="flex"
                  justifyContent="center"
                  className="w-100 mb-4 mt-1"
                  p={0}
                >
                  {isLoading ? (
                    <CircularProgress />
                  ) : playing ? (
                    <IconButton
                      className={classes.buttom}
                      onClick={() => {
                        stop();
                      }}
                    >
                      <StopIcon fontSize={"large"} />
                    </IconButton>
                  ) : (
                    <IconButton
                      className={classes.buttom}
                      onClick={() => {
                        //onBuscAudio();
                        if (selectTypesChamadas == "Funcionário") {
                          onStartCalls();
                        } else {
                          onGetPhrasesVeiculo();
                        }
                      }}
                    >
                      <PlayArrowIcon fontSize={"large"} />
                    </IconButton>
                  )}
                </Box>
              </Card>
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default Speech;
