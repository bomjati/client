import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Divider,
  TextField,
  Slider,
  Grid,
  Typography,
  IconButton,
  Box,
  Button,
  CircularProgress
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  PlayArrow as PlayArrowIcon,
  Stop as StopIcon,
  Delete as DeleteIcon,
  WhatsApp as WhatsAppIcon
} from "@material-ui/icons";
//import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "center"
    },
    [breakpoints.up("sm")]: {
      alignItems: "center"
    },
    justifyContent: "center"
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white"
  }
}));

const ModalSuporteWhatsApp = ({ onClose, open }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <Box
            pt={5}
            pl={8}
            pr={8}
            maxWidth={400}
            style={{
              borderRadius: 15,
              backgroundColor: "white"
            }}
          >
            <h2
              style={{ marginBottom: 20, color: "#1B2127" }}
              className="text-center"
            >
              Suporte
            </h2>
            <h6 style={{ color: "#33383D" }} className="text-center">
              Entre em contato conosco através de nosso WhatsApp, para tirar
              suas duvidas.
            </h6>

            <Box display="flex" justifyContent="center" mt={24} mb={4}>
              <div
                onClick={() => {
                  const url =
                    "https://api.whatsapp.com/send?phone=5543998364576&text=Olá, estou entrando em contato pelo site da Bomja! Pode me ajudar?";
                  const win = window.open(url, "_blank");
                  win.focus();
                  onClose();
                }}
                style={{
                  height: 40,
                  paddingLeft: 20,
                  paddingRight: 20,
                  borderRadius: 5,
                  cursor: "pointer",
                  backgroundColor: "#128C7E",
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <h6
                  style={{ color: "white", marginRight: 20 }}
                  className="text-center"
                >
                  Entrar em contato
                </h6>
                <WhatsAppIcon style={{ color: "white" }} />
              </div>
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalSuporteWhatsApp;
