import React, { useState, useEffect, MouseEvent } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  TextField,
  Button,
  Paper,
  InputBase,
  makeStyles,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Box,
  ClickAwayListener,
  Grid,
  Typography,
  Tooltip
} from "@material-ui/core";
import {
  SettingsVoice as SettingsVoiceIcon,
  PlaylistAdd as PlaylistAddIcon,
  Menu as MenuIcon,
  Search as SearchIcon,
  ArrowBack as ArrowBackIcon,
  Close as CloseIcon,
  MoreVert as MoreVertIcon,
  MusicNote as MusicNoteIcon,
  Block as BlockIcon,
  Favorite as FavoriteIcon,
  PlayArrow as PlayArrowIcon,
  ListAlt as ListAltIcon
} from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";
import api from "../../services/api";
import "./App.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(theme => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    backgroundColor: "#303030",
    width: 550
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
    fontWeight: "bold",
    color: "white"
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  }
}));

const MenuBar = ({
  onOpenSpeech,
  onOpenNewPlayList,
  onOpenPreviewList,
  onOpenMenu,
  onMyPlayList,
  onGenre,
  onStart,
  onPlayListSugere,
  nameClient,
  groupSelect,
  typeGroupSelect,
  isMobile,
  selectNextMusic,
  listBlocked,
  listFavorited,
  onLikeFavorite,
  onBlock,
  onDesLikeFavorite,
  onDesBlock,
  scrollPosition,
  listMusicCompleteArray
}) => {
  const classes = useStyles();
  const [useHoverTop, setUseHoverTop] = useState({ name: "", active: false });
  const [limitMusic, setLimitMusic] = useState(6);
  const [pageMusic, setPageMusic] = useState(0);
  const [openMusicSelect, setOpenMusicSelect] = useState({
    id: 0,
    active: null
  });
  const [visibleMenuMusicSelect, setVisibleMenuMusicSelect] = useState({
    id: 0,
    active: false
  });
  const [isSearch, setIsSearch] = useState(false);
  const [isConnected, setIsConnected] = useState(true);
  const [listMusicComplete, setListMusicComplete] = useState([]);
  const [listMusicCompleteBusca, setListMusicCompleteBusca] = useState([]);
  const [search, setSearch] = useState("");
  const activeTrue = { color: "white" };
  const activeFalse = { color: "rgba(255,255,255,0.6)" };

  useEffect(() => {
    window.addEventListener("offline", function(e) {
      console.log("offline");
      setIsConnected(false);
    });

    window.addEventListener("online", function(e) {
      console.log("online");
      setIsConnected(true);
    });
  }, []);

  useEffect(() => {
    setListMusicComplete(listMusicCompleteArray);
    setListMusicCompleteBusca(listMusicCompleteArray);
  }, [listMusicCompleteArray]);

  const onAddMusicList = () => {
    setLimitMusic(
      listMusicComplete.length > limitMusic ? limitMusic + 6 : limitMusic
    );
  };

  const handleClose = id => {
    setOpenMusicSelect({
      id: 0,
      active: null
    });
  };

  const handleClick = (event: MouseEvent<HTMLButtonElement>, id) => {
    setOpenMusicSelect({
      id: id,
      active: event.currentTarget
    });
  };

  const seachMusic = value => {
    let data = listMusicCompleteBusca.filter(
      m =>
        String(m.artist)
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "")
          .toUpperCase()
          .indexOf(
            String(value)
              .normalize("NFD")
              .replace(/[\u0300-\u036f]/g, "")
              .toUpperCase()
          ) > -1
    );

    if (data.length == 0) {
      data = listMusicCompleteBusca.filter(
        m =>
          String(m.title)
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .toUpperCase()
            .indexOf(
              String(value)
                .normalize("NFD")
                .replace(/[\u0300-\u036f]/g, "")
                .toUpperCase()
            ) > -1
      );

      setListMusicComplete(data);
    } else {
      setListMusicComplete(data);
    }
  };

  return (
    <AppBar
      elevation={0}
      position="fixed"
      style={{
        backgroundColor: scrollPosition > 0 ? "#1A1A1A" : "transparent"
      }}
    >
      {!isConnected && (
        <div
          style={{
            height: "40px",
            backgroundColor: "#e74c3c",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Typography variant="body1" style={{ color: "white", textAlign: "center", marginTop: 10 }}>
            Sem conexão com internet. Não atualize esta página pois perderá o
            carregameno em cache.
          </Typography>
        </div>
      )}
      <Toolbar
        style={{
          backgroundColor: scrollPosition > 0 ? "#1A1A1A" : "transparent"
        }}
        className="justify-content-between"
      >
        <div className="d-flex align-items-center">
          <IconButton
            onClick={() => {
              onOpenMenu();
            }}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon />
          </IconButton>
          <h3 className="text-white ml-2">
            {nameClient
              ? isMobile == "true"
                ? String(nameClient).split(" ").length > 1
                  ? `${String(nameClient).split(" ")[0]}`
                  : nameClient
                : nameClient
              : "Music"}
          </h3>
        </div>

        {isMobile == "true" ? null : isSearch ? (
          <div style={{ alignItems: "center" }} className="d-flex">
            <ClickAwayListener
              onClickAway={() => {
                setSearch("");
                setIsSearch(false);
              }}
            >
              <div>
                <Paper component="form" className={classes.root}>
                  <IconButton
                    onClick={() => {
                      setIsSearch(false);
                    }}
                    className={classes.iconButton}
                    aria-label="menu"
                  >
                    <ArrowBackIcon style={{ color: "rgba(255,255,225,0.5)" }} />
                  </IconButton>
                  <InputBase
                    className={classes.input}
                    placeholder="Pesquisar artista, musica..."
                    value={search}
                    onChange={event => {
                      setSearch(event.target.value);
                      seachMusic(event.target.value);
                    }}
                    inputProps={{ "aria-label": "Pesquisar" }}
                  />
                  {search.length == 0 ? null : (
                    <IconButton
                      onClick={() => {
                        setSearch("");
                      }}
                      className={classes.iconButton}
                      aria-label="menu"
                    >
                      <CloseIcon style={{ color: "rgba(255,255,225,0.5)" }} />
                    </IconButton>
                  )}
                </Paper>
                {!(search.length > 0) ? null : (
                  <Paper
                    style={{ marginTop: 10, position: "absolute", height: 300 }}
                    component="form"
                    className={classes.root}
                  >
                    <div style={{ height: 300 }}>
                      <PerfectScrollbar
                        onYReachEnd={() => {
                          onAddMusicList();
                        }}
                      >
                        {listMusicComplete
                          .slice(
                            pageMusic * limitMusic,
                            pageMusic * limitMusic + limitMusic
                          )
                          .map((music, index) => (
                            <div
                              onMouseEnter={() => {
                                setVisibleMenuMusicSelect({
                                  id: music.id,
                                  active: true
                                });
                              }}
                              onMouseLeave={() => {
                                setVisibleMenuMusicSelect({
                                  id: music.id,
                                  active: false
                                });
                              }}
                              key={index}
                              className="w-100 ct"
                            >
                              <Grid item lg={12} md={12} xs={12}>
                                <Box
                                  style={{
                                    backgroundColor: "transparent"
                                  }}
                                  className="p-1"
                                  display="flex"
                                  alignItems="center"
                                  justifyContent="space-between"
                                >
                                  <Box
                                    className="p-1"
                                    display="flex"
                                    alignItems="center"
                                    justifyContent="flex-start"
                                  >
                                    <div>
                                      <div className="d-flex">
                                        <h5 className="text-white mr-4">
                                          {String(music.title).replace(
                                            ".mp3",
                                            ""
                                          )}
                                        </h5>
                                      </div>
                                      <Typography
                                        style={{ color: "white" }}
                                        variant="body1"
                                      >
                                        {music.artist}
                                      </Typography>
                                    </div>
                                  </Box>
                                  {visibleMenuMusicSelect.id == music.id ? (
                                    <div>
                                      <IconButton
                                        onClick={event => {
                                          event.stopPropagation();
                                          handleClick(event, music.id);
                                        }}
                                      >
                                        <MoreVertIcon
                                          style={{
                                            color: "rgba(255,255,255, 0.5)"
                                          }}
                                        />
                                      </IconButton>
                                      <Menu
                                        id="simple-menu"
                                        anchorEl={
                                          openMusicSelect.id == music.id
                                            ? openMusicSelect.active
                                            : null
                                        }
                                        PaperProps={{
                                          style: {
                                            backgroundColor: "#303030",
                                            borderColor:
                                              "rgba(255,255,255,0.2)",
                                            borderWidth: 0.1,
                                            borderStyle: "solid"
                                          }
                                        }}
                                        keepMounted
                                        open={Boolean(
                                          openMusicSelect.id == music.id
                                            ? openMusicSelect.active
                                            : null
                                        )}
                                        onClose={handleClose}
                                      >
                                        <MenuItem
                                          onClick={() => {
                                            handleClose(music.id);
                                            selectNextMusic(music, false);
                                            setSearch("");
                                            setIsSearch(false);
                                          }}
                                        >
                                          <ListItemIcon>
                                            <PlayArrowIcon
                                              style={{
                                                color: "rgba(255,255,255,0.9)"
                                              }}
                                            />
                                          </ListItemIcon>
                                          <ListItemText
                                            style={{
                                              color: "rgba(255,255,255,0.9)"
                                            }}
                                          >
                                            Tocar agora
                                          </ListItemText>
                                        </MenuItem>
                                        <MenuItem
                                          onClick={() => {
                                            handleClose(music.id);
                                            selectNextMusic(music, true);
                                            setSearch("");
                                            setIsSearch(false);
                                          }}
                                        >
                                          <ListItemIcon>
                                            <MusicNoteIcon
                                              style={{
                                                color: "rgba(255,255,255,0.9)"
                                              }}
                                            />
                                          </ListItemIcon>
                                          <ListItemText
                                            style={{
                                              color: "rgba(255,255,255,0.9)"
                                            }}
                                          >
                                            Tocar na próxima
                                          </ListItemText>
                                        </MenuItem>
                                        <MenuItem
                                          onClick={() => {
                                            handleClose(music.id);
                                            const arrayBlock = listBlocked.filter(
                                              b => b.musicId == music.id
                                            );
                                            if (arrayBlock.length > 0) {
                                              onDesBlock(arrayBlock[0].id);
                                            } else {
                                              onBlock(music.id, music.artist);
                                            }
                                            setSearch("");
                                            setIsSearch(false);
                                          }}
                                        >
                                          <ListItemIcon>
                                            <BlockIcon
                                              style={{
                                                color: "rgba(255,255,255,0.9)"
                                              }}
                                            />
                                          </ListItemIcon>
                                          <ListItemText
                                            style={{
                                              color: "rgba(255,255,255,0.9)"
                                            }}
                                          >
                                            {listBlocked.filter(
                                              b => b.musicId == music.id
                                            ).length > 0
                                              ? "Desbloquear"
                                              : "Bloquear"}
                                          </ListItemText>
                                        </MenuItem>
                                        <MenuItem
                                          onClick={() => {
                                            handleClose(music.id);
                                            const arrayFavorited = listFavorited.filter(
                                              b => b.musicId == music.id
                                            );
                                            if (arrayFavorited.length > 0) {
                                              onDesLikeFavorite(
                                                arrayFavorited[0].id
                                              );
                                            } else {
                                              onLikeFavorite(
                                                music.id,
                                                music.artist
                                              );
                                            }
                                            setSearch("");
                                            setIsSearch(false);
                                          }}
                                        >
                                          <ListItemIcon>
                                            <FavoriteIcon
                                              style={{
                                                color: "rgba(255,255,255,0.9)"
                                              }}
                                            />
                                          </ListItemIcon>
                                          <ListItemText
                                            style={{
                                              color: "rgba(255,255,255,0.9)"
                                            }}
                                          >
                                            {listFavorited.filter(
                                              b => b.musicId == music.id
                                            ).length
                                              ? "Desfavoritar"
                                              : "Favoritar"}
                                          </ListItemText>
                                        </MenuItem>
                                      </Menu>
                                    </div>
                                  ) : (
                                    <Typography
                                      style={{ color: "white" }}
                                      variant="body1"
                                    >
                                      {music.totalTime}
                                    </Typography>
                                  )}
                                </Box>
                              </Grid>
                              <div
                                className="w-100"
                                style={{
                                  background: "rgba(255,255,255,0.2)",
                                  height: 0.09
                                }}
                              />
                            </div>
                          ))}
                      </PerfectScrollbar>
                    </div>
                  </Paper>
                )}
              </div>
            </ClickAwayListener>
          </div>
        ) : (
          <div style={{ alignItems: "center" }} className="d-flex">
            {(groupSelect == "Minhas Playlist") |
            (groupSelect == "Favoritos") |
            (groupSelect == "Bloqueados") ? (
              <div style={{ alignItems: "center" }} className="d-flex">
                <Button
                  onClick={() => onStart()}
                  variant="text"
                  className="ml-4 mr-4"
                >
                  <h4
                    style={{
                      color: "white",
                      textTransform: "none",
                      fontWeight: "normal"
                    }}
                    className="text-white"
                  >
                    Início
                  </h4>
                </Button>
              </div>
            ) : (
              <div style={{ alignItems: "center" }} className="d-flex">
                <Button
                  onClick={() => onPlayListSugere()}
                  variant="text"
                  onMouseEnter={() =>
                    setUseHoverTop({ name: "sugeridas", active: true })
                  }
                  onMouseLeave={() =>
                    setUseHoverTop({ name: "", active: false })
                  }
                  className="ml-4 mr-4"
                >
                  <h4
                    style={{
                      ...(((useHoverTop.name == "sugeridas") &
                        (useHoverTop.active == true)) |
                      (typeGroupSelect == "sugeridas")
                        ? activeTrue
                        : activeFalse),
                      fontWeight: "normal",
                      textTransform: "none"
                    }}
                  >
                    Destaques
                  </h4>
                </Button>

                {groupSelect != "Creative Commons" && (
                  <Button
                    onClick={() => onGenre()}
                    variant="text"
                    onMouseEnter={() =>
                      setUseHoverTop({ name: "generos", active: true })
                    }
                    onMouseLeave={() =>
                      setUseHoverTop({ name: "", active: false })
                    }
                    className="ml-4 mr-4"
                  >
                    <h4
                      style={{
                        ...(((useHoverTop.name == "generos") &
                          (useHoverTop.active == true)) |
                        (typeGroupSelect == "genero")
                          ? activeTrue
                          : activeFalse),
                        fontWeight: "normal",
                        textTransform: "none"
                      }}
                    >
                      Gêneros
                    </h4>
                  </Button>
                )}
              </div>
            )}
            <Button
              startIcon={
                <SearchIcon
                  style={
                    (useHoverTop.name == "pesquisar") &
                    (useHoverTop.active == true)
                      ? activeTrue
                      : activeFalse
                  }
                />
              }
              variant="text"
              className="ml-4 mr-4"
              onMouseEnter={() =>
                setUseHoverTop({ name: "pesquisar", active: true })
              }
              onMouseLeave={() => setUseHoverTop({ name: "", active: false })}
              onClick={() => {
                setIsSearch(true);
              }}
            >
              <h4
                style={{
                  ...((useHoverTop.name == "pesquisar") &
                  (useHoverTop.active == true)
                    ? activeTrue
                    : activeFalse),
                  fontWeight: "normal",
                  textTransform: "none"
                }}
              >
                Pesquisar
              </h4>
            </Button>
          </div>
        )}

        <div style={{ alignItems: "center" }} className="d-flex">
          <Tooltip arrow title="Configurações de Blocos Musicais">
            <IconButton
              onClick={() => {
                onOpenPreviewList();
              }}
              color="inherit"
              aria-label="play-list"
            >
              <ListAltIcon />
            </IconButton>
          </Tooltip>
          <Tooltip arrow title="Avisos">
            <IconButton
              onClick={() => {
                onOpenSpeech();
              }}
              color="inherit"
              aria-label="voices"
            >
              <SettingsVoiceIcon />
            </IconButton>
          </Tooltip>
          {isMobile == "true" ? null : (
            <Button
              onClick={() => onMyPlayList()}
              variant="contained"
              style={{
                backgroundColor: "white",
                color: "black",
                fontWeight: "bold",
                height: 32,
                borderRadius: 2
              }}
            >
              Minhas Playlists
            </Button>
          )}
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default MenuBar;
