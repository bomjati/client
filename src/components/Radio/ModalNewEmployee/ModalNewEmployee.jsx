import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Grid,
  IconButton,
  Box,
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Checkbox,
  FormControlLabel,
  Avatar,
  TextField,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  ExpandMore as ExpandMoreIcon,
  PlayArrow as PlayArrowIcon,
  CheckCircle as CheckCircleIcon,
} from "@material-ui/icons";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import api from "../../../services/api";
import { showMessage } from "../utils/message";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white",
  },
  MuiAccordionroot: {
    "&.MuiAccordion-root:before": {
      backgroundColor: "transparent",
    },
  },
}));

const ModalNewEmployee = ({ open, onCloseNewEmployee }) => {
  const classes = useStyles();
  const [name, setName] = useState("");

  const onSaveEmployee = () => {
    if (name == "") {
      showMessage(
        "Novo Funcionário",
        "Preencha o nome de seu funcionário para prosseguir.",
        "warning"
      );
      return;
    }

    api
      .post("/employee/new", { name: name })
      .then((response) => response.data)
      .then((resp) => {
        console.log(resp);
        if (resp.message == "success") {
          showMessage(
            "Novo Funcionário",
            "Funcionário criado com sucesso.",
            "success"
          );
          onCloseNewEmployee();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseNewEmployee}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4 mt-2">
              <Box
                display="flex"
                justifyContent="flex-end"
                className="w-90 mt-2 mb-2 mr-4"
                p={0}
              >
                <IconButton
                  style={{ backgroundColor: "white" }}
                  onClick={onCloseNewEmployee}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Card className="ml-4 mr-4" style={{ borderRadius: 15 }}>
                <CardHeader
                  subheader="Cadastre seu novo funcionário"
                  title="Novo Funcionário"
                />

                <Divider />
                <CardContent className="p-4">
                  <Card style={{ borderRadius: 20 }} className="w-100 p-4 mb-4">
                    <h5 className="text-black mb-2">Nome Funcionário</h5>
                    <div className="d-flex">
                      <TextField
                        fullWidth
                        label=""
                        name="name"
                        onChange={(event) => {
                          setName(event.target.value);
                        }}
                        required
                        value={name}
                        variant="outlined"
                        inputProps={{
                          style: {
                            padding: 10,
                            borderRadius: 50,
                          },
                        }}
                      />

                      <Button
                        onClick={() => {
                          onSaveEmployee();
                        }}
                        style={{
                          backgroundColor: "#27ae60",
                          color: "white",
                          borderRadius: 20,
                        }}
                        className="ml-3"
                        variant="contained"
                      >
                        Salvar
                      </Button>
                    </div>
                  </Card>
                </CardContent>
              </Card>
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewEmployee;
