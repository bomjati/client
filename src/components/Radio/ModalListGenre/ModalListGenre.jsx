import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Divider,
  InputBase,
  TextField,
  Slider,
  Grid,
  Typography,
  IconButton,
  Paper,
  Box,
  Button,
  Avatar,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  PlayArrow as PlayArrowIcon,
  Stop as StopIcon,
  Delete as DeleteIcon,
  WhatsApp as WhatsAppIcon,
  Search as SearchIcon,
  Block as BlockIcon,
  Favorite as FavoriteIcon,
  FavoriteBorder as FavoriteBorderIcon,
} from "@material-ui/icons";
import api from "../../../services/api";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "center",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white",
  },
}));

const ModalListGenre = ({
  onClose,
  open,
  nameGenre,
  listMusic,
  onSelect,
  capa,
  qtdMusic,
  isSubGenre,
  audioQualityId,
  onSelectGenre,
  listBlocked,
  listFavorited,
  onLikeFavorite,
  onBlock,
  onDesLikeFavorite,
  onDesBlock,
}) => {
  const classes = useStyles();
  const [search, setSearch] = useState("");
  const [musics, setMusics] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isPlay, setIsPlay] = useState(0);
  const colorsIcon = "#949494";
  const sizesIcon = 20;
  const [audio, setAudio] = useState(document.createElement("audio"));

  useEffect(() => {
    returnMusic();
  }, []);

  const onPlayMusic = (url, musicId) => {
    audio.pause();
    audio.src = url;
    audio.onended = () => {
      setIsPlay(false);
    };
    setIsPlay(musicId);
    audio.play();
  };

  const onPauseMusic = () => {
    setIsPlay(0);
    audio.pause();
  };

  const returnMusic = async () => {
    let arrayMusic = [];

    const element = listMusic.id;
    setIsLoading(true);

    if (isSubGenre) {
      const musS = await api.get(
        `/genresubitem/genresub/${element}/${audioQualityId}`
      );

      for (let index = 0; index < musS.data.length; index++) {
        const elementM = musS.data[index];

        arrayMusic.push(elementM.Music);
      }
    } else {
      const mus = await api.get(`/music/genre/${element}/${audioQualityId}`);

      if (mus.data) {
        console.log(mus.data);
        for (let index = 0; index < mus.data.length; index++) {
          const elementM = mus.data[index];

          arrayMusic.push(elementM);
        }
      }
    }

    setIsLoading(false);
    console.log(arrayMusic);
    setMusics(arrayMusic);
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div
            style={{
              borderRadius: 15,
              height: "100%",
              width: "100%",
              backgroundColor: "white",
            }}
          >
            <Box
              display="flex"
              justifyContent="flex-end"
              className="mt--4 mb--4 mr-2"
              p={0}
            >
              <IconButton
                disabled={isLoading}
                style={{ backgroundColor: "white" }}
                onClick={onClose}
              >
                <CloseIcon
                  style={{
                    color: "red",
                  }}
                />
              </IconButton>
            </Box>
            <Box
              pt={5}
              pr={4}
              pl={20}
              style={{
                borderRadius: 15,
                height: "100%",
                width: "100%",
                backgroundColor: "white",
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  marginBottom: 20,
                }}
              >
                <Avatar
                  style={{
                    borderRadius: 5,
                    height: 190,
                    width: 190,
                  }}
                  src={
                    listMusic.Covers.length > 0
                      ? listMusic.Covers[0].path_s3
                      : ""
                  }
                />
                <Box
                  ml={5}
                  display="flex"
                  flexDirection="column"
                  justifyContent="space-between"
                >
                  <Box>
                    <h3
                      style={{ marginBottom: 10, color: "#1B2127" }}
                      className=""
                    >
                      {listMusic ? listMusic.name : ""}
                    </h3>
                    <h6 style={{ color: "#33383D" }} className="">
                      {listMusic ? listMusic.countMusic : 0} Faixas
                    </h6>
                  </Box>
                  <Button
                    onClick={() => {
                      onSelect(listMusic, true);
                    }}
                    style={{
                      color: "white",
                      backgroundColor: "red",
                      borderRadius: 20,

                      marginTop: 10,
                    }}
                  >
                    <PlayArrowIcon style={{ color: "white" }} />

                    <h6
                      style={{
                        color: "white",
                        fontWeight: "bold",
                        marginLeft: 20,
                        marginRight: 20,
                      }}
                    >
                      Selecionar
                    </h6>
                  </Button>
                </Box>
              </Box>

              <PerfectScrollbar>
                <Paper
                  style={{ marginTop: 20 }}
                  component="form"
                  className={classes.root}
                >
                  <IconButton className={classes.iconButton} aria-label="menu">
                    <SearchIcon style={{ color: "rgba(0,0,0,0.8)" }} />
                  </IconButton>
                  <InputBase
                    style={{
                      width: "90%",
                    }}
                    placeholder="Pesquisar musica..."
                    value={search}
                    onChange={(event) => {
                      setSearch(event.target.value);
                    }}
                    inputProps={{ "aria-label": "Pesquisar" }}
                  />
                  {search.length == 0 ? null : (
                    <IconButton
                      onClick={() => {
                        setSearch("");
                      }}
                      className={classes.iconButton}
                      aria-label="menu"
                    >
                      <CloseIcon style={{ color: "rgba(0,0,0,0.8)" }} />
                    </IconButton>
                  )}
                </Paper>
                <Typography variant="body2" style={{ marginTop: 30 }}>
                  MÚSICAS
                </Typography>
                {isLoading && (
                  <Box
                    flexDirection="row"
                    justifyContent="center"
                    display="flex"
                  >
                    <CircularProgress />
                  </Box>
                )}
                {musics &&
                  musics.map((music) => {
                    return (
                      <Card
                        style={{
                          borderWidth: 0.1,
                          borderColor: "#e8e8e8",
                          borderStyle: "solid",
                          borderRadius: 10,
                        }}
                        className=" mt-2 mr-2"
                      >
                        <Box
                          flexDirection="row"
                          display="flex"
                          justifyContent="space-between"
                          flexWrap="wrap"
                          p={2}
                        >
                          <Typography
                            variant="body2"
                            style={{ color: "black", fontWeight: "bold" }}
                          >
                            {String(music.title)
                              .replace(".mp3", "")
                              .replace(".m4a", "")}
                          </Typography>
                          <Typography variant="body2">
                            {music.artist}
                          </Typography>
                          <Box flexDirection="row" display="flex">
                            <Typography variant="body2">
                              {music.totalTime}
                            </Typography>
                            <BlockIcon
                              className="ml-4"
                              aria-label="NotLike"
                              onClick={() => {
                                const arrayBlock = listBlocked.filter((b) =>
                                  String(music.artist)
                                    .toLowerCase()
                                    .indexOf("vinheta") > -1
                                    ? b.vignetteId == music.id
                                    : String(music.artist)
                                        .toLowerCase()
                                        .indexOf("comercial") > -1
                                    ? b.commercialId == music.id
                                    : String(music.artist)
                                        .toLowerCase()
                                        .indexOf("conteúdo") > -1
                                    ? b.contentId == music.id
                                    : b.musicId == music.id
                                );
                                if (arrayBlock.length > 0) {
                                  onDesBlock(arrayBlock[0].id);
                                } else {
                                  onBlock(music.id, music.artist);
                                }
                              }}
                              style={{
                                color:
                                  listBlocked.filter((b) =>
                                    String(music.artist)
                                      .toLowerCase()
                                      .indexOf("vinheta") > -1
                                      ? b.vignetteId == music.id
                                      : String(music.artist)
                                          .toLowerCase()
                                          .indexOf("comercial") > -1
                                      ? b.commercialId == music.id
                                      : String(music.artist)
                                          .toLowerCase()
                                          .indexOf("conteúdo") > -1
                                      ? b.contentId == music.id
                                      : b.musicId == music.id
                                  ).length > 0
                                    ? "#e67e22"
                                    : colorsIcon,
                                fontSize: sizesIcon,
                                cursor: "pointer",
                              }}
                            />

                            {listFavorited.filter((f) =>
                              String(music.artist)
                                .toLowerCase()
                                .indexOf("vinheta") > -1
                                ? f.vignetteId == music.id
                                : String(music.artist)
                                    .toLowerCase()
                                    .indexOf("comercial") > -1
                                ? f.commercialId == music.id
                                : String(music.artist)
                                    .toLowerCase()
                                    .indexOf("conteúdo") > -1
                                ? f.contentId == music.id
                                : f.musicId == music.id
                            ).length > 0 ? (
                              <FavoriteIcon
                                className="ml-4"
                                aria-label="Like"
                                onClick={() => {
                                  const arrayFavorited = listFavorited.filter(
                                    (b) =>
                                      String(music.artist)
                                        .toLowerCase()
                                        .indexOf("vinheta") > -1
                                        ? b.vignetteId == music.id
                                        : String(music.artist)
                                            .toLowerCase()
                                            .indexOf("comercial") > -1
                                        ? b.commercialId == music.id
                                        : String(music.artist)
                                            .toLowerCase()
                                            .indexOf("conteúdo") > -1
                                        ? b.contentId == music.id
                                        : b.musicId == music.id
                                  );
                                  if (arrayFavorited.length > 0) {
                                    onDesLikeFavorite(arrayFavorited[0].id);
                                  } else {
                                    onLikeFavorite(music.id, music.artist);
                                  }
                                }}
                                style={{
                                  color: "#eb2f06",
                                  fontSize: sizesIcon,
                                  cursor: "pointer",
                                }}
                              />
                            ) : (
                              <FavoriteBorderIcon
                                className="ml-4"
                                aria-label="Like"
                                onClick={() => {
                                  const arrayFavorited = listFavorited.filter(
                                    (b) =>
                                      String(music.artist)
                                        .toLowerCase()
                                        .indexOf("vinheta") > -1
                                        ? b.vignetteId == music.id
                                        : String(music.artist)
                                            .toLowerCase()
                                            .indexOf("comercial") > -1
                                        ? b.commercialId == music.id
                                        : String(music.artist)
                                            .toLowerCase()
                                            .indexOf("conteúdo") > -1
                                        ? b.contentId == music.id
                                        : b.musicId == music.id
                                  );
                                  if (arrayFavorited.length > 0) {
                                    onDesLikeFavorite(arrayFavorited[0].id);
                                  } else {
                                    onLikeFavorite(music.id, music.artist);
                                  }
                                }}
                                style={{
                                  color: colorsIcon,
                                  fontSize: sizesIcon,
                                  cursor: "pointer",
                                }}
                              />
                            )}
                            {isPlay == music.id ? (
                              <StopIcon
                                className="ml-4"
                                aria-label="Stop"
                                onClick={() => {
                                  onPauseMusic();
                                }}
                                style={{
                                  color: "black",
                                  fontSize: sizesIcon,
                                  cursor: "pointer",
                                }}
                              />
                            ) : (
                              <PlayArrowIcon
                                className="ml-4"
                                aria-label="Play"
                                onClick={() => {
                                  //console.log(music.url_m4a_s3)
                                  onPlayMusic(music.url_m4a_s3, music.id);
                                }}
                                style={{
                                  color: "black",
                                  fontSize: sizesIcon,
                                  cursor: "pointer",
                                }}
                              />
                            )}
                          </Box>
                        </Box>
                      </Card>
                    );
                  })}
              </PerfectScrollbar>
            </Box>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalListGenre;
