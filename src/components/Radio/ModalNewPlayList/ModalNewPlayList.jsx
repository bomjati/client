import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Grid,
  IconButton,
  Box,
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Checkbox,
  FormControlLabel,
  Avatar,
  TextField,
  Button,
  Container,
  TablePagination,
  ClickAwayListener,
  InputAdornment,
  Paper,
  CircularProgress
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  ExpandMore as ExpandMoreIcon,
  ExpandLess as ExpandLessIcon,
  PlayArrow as PlayArrowIcon,
  CheckCircle as CheckCircleIcon,
  Search as SearchIcon
} from "@material-ui/icons";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import api from "../../../services/api";
import { showMessage } from "../utils/message";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

const useStyles = makeStyles(({ breakpoints }) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    backgroundColor: "#303030",
    width: 550
  },
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start"
    },
    [breakpoints.up("sm")]: {
      alignItems: "center"
    },
    justifyContent: "center"
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white"
  },
  MuiAccordionroot: {
    "&.MuiAccordion-root:before": {
      backgroundColor: "transparent"
    }
  }
}));

const ModalNewPlayList = ({
  open,
  onCloseNewPlayList,
  heapLists,
  onGetPlayList,
  clientId,
  isMobile,
  listMusicCompleteArray
}) => {
  const classes = useStyles();
  const [heapList, setHeapList] = useState([]);
  const [musicSelect, setMusicSelect] = useState([]);
  const [listMusicComplete, setListMusicComplete] = useState([]);
  const [listMusicCompleteBusca, setListMusicCompleteBusca] = useState([]);
  const [openSelect, setOpenSelect] = useState([]);
  const [genereSelectAll, setGenereSelectAll] = useState([]);
  const [genereSelectAllSub, setGenereSelectAllSub] = useState([]);
  const [genereSelect, setGenereSelect] = useState(null);
  const [namePlayList, setNamePlayList] = useState("");
  const [limitMusic, setLimitMusic] = useState(5);
  const [pageMusic, setPageMusic] = useState(0);
  const { height, width } = useWindowDimensions();
  const [searchs, setSearchs] = useState({});
  const [isSearchs, setIsSearchs] = useState({});
  const [isViewsList, setIsViewsList] = useState({});
  const [isBackDrop, setIsBackDrop] = useState(false);

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)"
  };

  useEffect(() => {
    console.log(heapLists);
    setHeapList(heapLists);
  }, [heapLists]);

  useEffect(() => {
    setListMusicComplete(listMusicCompleteArray);
    setListMusicCompleteBusca(listMusicCompleteArray);
  }, [listMusicCompleteArray]);

  const onAddMusicList = () => {
    setLimitMusic(
      listMusicComplete.length > limitMusic ? limitMusic + 6 : limitMusic
    );
  };

  const handleLimitChangeMusic = event => {
    setLimitMusic(event.target.value);
  };

  const handlePageChangeMusic = (event, newPage) => {
    setPageMusic(newPage);
  };

  const onSelectAll = genero => {
    setGenereSelect(genero);
    setGenereSelectAll({
      ...genereSelectAll,
      [genero.id]: {
        status: genereSelectAll[String(genero.id)]
          ? !genereSelectAll[String(genero.id)].status
          : true,
        id: genero.id
      }
    });
  };

  const onSelectAllSub = genero => {
    setGenereSelect(genero);
    setGenereSelectAllSub({
      ...genereSelectAllSub,
      [genero.id]: {
        status: genereSelectAllSub[String(genero.id)]
          ? !genereSelectAllSub[String(genero.id)].status
          : true,
        id: genero.id
      }
    });
  };

  const onSavePlayList = () => {
    let dadosMusic = [];
    if (namePlayList == "") {
      showMessage(
        "Nova Playlist",
        "Preencha o nome de sua Playlist para prosseguir.",
        "warning"
      );
      return;
    }

    if (JSON.stringify(musicSelect) == "[]") {
      showMessage(
        "Nova Playlist",
        "Selecione musicas para salvar sua Playlist.",
        "warning"
      );
      return;
    }

    for (var key in musicSelect) {
      if (musicSelect[key].status) {
        dadosMusic.push(musicSelect[key].id);
      }
    }

    const dados = {
      name: namePlayList,
      clientId: clientId
    };

    if (!(dadosMusic.length > 0)) {
      showMessage(
        "Nova Playlist",
        "Selecione musicas para salvar sua Playlist.",
        "warning"
      );
      return;
    }

    console.log(JSON.stringify(dados));
    setIsBackDrop(true);
    api
      .post("/playlist/new", dados)
      .then(response => response.data)
      .then(resp => {
        console.log(resp);
        if (resp.message == "success") {
          const dadosM = {
            music: dadosMusic
          };
          console.log(dadosM)

          api
            .post(`/itemplaylist/new/${resp.data.id}`, dadosM)
            .then(response => response.data)
            .then(respI => {
              console.log(respI);
              if (respI.message == "success") {
                showMessage(
                  "Nova Playlist",
                  "Playlist criada com sucesso.",
                  "success"
                );
                onGetPlayList();
                onCloseNewPlayList();
                setMusicSelect([]);

                setOpenSelect([]);
                setNamePlayList("");
                setLimitMusic(5);
                setPageMusic(0);
                setSearchs({});
                setIsSearchs({});
                setIsViewsList({});
                setIsBackDrop(false);
              }
            })
            .catch(error => {
              console.log(error);
            });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    // console.log(musicSelect)
  }, [musicSelect]);

  useEffect(() => {
    //console.log(genereSelectAll)
    let dadosMusics = musicSelect;

    if (genereSelect) {
      for (let index = 0; index < genereSelect.Musics.length; index++) {
        const music = genereSelect.Musics[index];

        dadosMusics = {
          ...dadosMusics,
          [music.id]: {
            status: genereSelectAll[String(genereSelect.id)].status,
            id: music.id
          }
        };
      }

      setMusicSelect(dadosMusics);
    }
  }, [genereSelectAll]);

  useEffect(() => {
    //console.log(genereSelectAll)
    let dadosMusics = musicSelect;

    if (genereSelect) {
      for (let index = 0; index < genereSelect.Musics.length; index++) {
        const music = genereSelect.Musics[index];

        dadosMusics = {
          ...dadosMusics,
          [music.musicId]: {
            status: genereSelectAllSub[String(genereSelect.id)].status,
            id: music.musicId
          }
        };
      }

      setMusicSelect(dadosMusics);
    }
  }, [genereSelectAllSub]);

  const onGetMusicGenre = (genero, isSelect) => {
    if (!genero.Musics) {
      setIsBackDrop(true);
      api
        .get(`/music/genre/${genero.id}`)
        .then(respM => {
          if (respM.data) {
            setHeapList(
              heapList.map(h => {
                return {
                  ...h,
                  Rights: h.Rights.map(r => {
                    return {
                      ...r,
                      Genres: r.Genres.map(g => {
                        if (isSelect) {
                          if (g.id == genero.id) {
                            onSelectAll({
                              ...g,
                              Musics: respM.data
                            });
                          }
                        }
                        return g.id == genero.id
                          ? {
                              ...g,
                              Musics: respM.data
                            }
                          : g;
                      })
                    };
                  })
                };
              })
            );

            setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const onGetMusicGenreSubs = (genero, isSelect) => {
    if (!genero.Musics) {
      setIsBackDrop(true);
      api
        .get(`/genresubitem/genresub/${genero.id}`)
        .then(respM => {
          if (respM.data) {
            setHeapList(
              heapList.map(h => {
                return {
                  ...h,
                  Rights: h.Rights.map(r => {
                    return {
                      ...r,
                      Genresubs: r.Genresubs.map(g => {
                        if (isSelect) {
                          if (g.id == genero.id) {
                            onSelectAllSub({
                              ...g,
                              Musics: respM.data
                            });
                          }
                        }
                        return g.id == genero.id
                          ? {
                              ...g,
                              Musics: respM.data
                            }
                          : g;
                      })
                    };
                  })
                };
              })
            );

            setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const seachMusic = (value, Genres, isGenreSub) => {
    let data = listMusicCompleteBusca.filter(
      m =>
        String(m.artist)
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "")
          .toUpperCase()
          .indexOf(
            String(value)
              .normalize("NFD")
              .replace(/[\u0300-\u036f]/g, "")
              .toUpperCase()
          ) > -1
    );

    if (data.length == 0) {
      data = listMusicCompleteBusca.filter(
        m =>
          String(m.title)
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .toUpperCase()
            .indexOf(
              String(value)
                .normalize("NFD")
                .replace(/[\u0300-\u036f]/g, "")
                .toUpperCase()
            ) > -1
      );

      setListMusicComplete(data);
    } else {
      setListMusicComplete(data);
    }
  };

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={onCloseNewPlayList}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Fade in={open}>
        <PerfectScrollbar>
          <Container style={{ marginTop: 20 }}>
            <Box
              display="flex"
              justifyContent="flex-end"
              className="w-90 mt-2 mb-2 mr-4"
              p={0}
            >
              <IconButton
                style={{ backgroundColor: "white" }}
                onClick={onCloseNewPlayList}
              >
                <CloseIcon />
              </IconButton>
            </Box>
            <Card
              className="ml-1 mr-4"
              style={{
                borderRadius: 15,
                width: isMobile == "true" ? "96%" : 720,
                height: isMobile == "true" ? (height * 85) / 100 : 600
              }}
            >
              <CardHeader
                subheader="Selecione suas música favoritas"
                title="Nova Playlist"
              />

              <Divider />
              <PerfectScrollbar>
                <CardContent style={{ marginBottom: 250 }} className="p-4">
                  <Card
                    style={{ ...styleButtonIcon, borderRadius: 15 }}
                    className="w-100 p-4 mb-4"
                  >
                    <h5 className="text-black mb-2">Nome Playlist</h5>
                    <TextField
                      fullWidth
                      label=""
                      name="name_playlist"
                      onChange={event => {
                        setNamePlayList(event.target.value);
                      }}
                      required
                      value={namePlayList}
                      variant="outlined"
                      inputProps={{
                        style: {
                          padding: 10,
                          borderRadius: 50
                        }
                      }}
                    />
                  </Card>

                  {heapList &&
                    heapList.length > 0 &&
                    heapList[0].Rights.map(Right => (
                      <div
                        className="mb-2 mt-4"
                        style={
                          isViewsList[String(Right.id)]
                            ? {}
                            : {
                                ...styleButtonIcon,
                                padding: 15,
                                borderRadius: 15
                              }
                        }
                      >
                        {!isSearchs[String(Right.id)] ? (
                          <Box
                            mb={isViewsList[String(Right.id)] ? 4 : 0}
                            display="flex"
                            justifyContent="space-between"
                            alignItems="center"
                          >
                            <Box
                              onClick={() => {
                                setIsViewsList({
                                  ...isViewsList,
                                  [String(Right.id)]: isViewsList[
                                    String(Right.id)
                                  ]
                                    ? !isViewsList[String(Right.id)]
                                    : true
                                });
                              }}
                              display="flex"
                              alignItems="center"
                              style={{ cursor: "pointer" }}
                            >
                              <h5 className="text-black mr-4">{Right.name}</h5>
                              {isViewsList[String(Right.id)] ? (
                                <ExpandLessIcon />
                              ) : (
                                <ExpandMoreIcon />
                              )}
                            </Box>

                            <Button
                              onClick={() => {
                                setIsSearchs({
                                  ...isSearchs,
                                  [String(Right.id)]: isSearchs[
                                    String(Right.id)
                                  ]
                                    ? !isSearchs[String(Right.id)]
                                    : true
                                });
                              }}
                              size="medium"
                              variant="contained"
                              style={{ backgroundColor: "white", color: "red" }}
                              startIcon={<SearchIcon />}
                            >
                              Buscar Musicas
                            </Button>
                          </Box>
                        ) : (
                          <div>
                            <ClickAwayListener
                              onClickAway={() => {
                                setSearchs({
                                  ...searchs,
                                  [String(Right.id)]: searchs[String(Right.id)]
                                    ? ""
                                    : ""
                                });
                                setIsSearchs({
                                  ...isSearchs,
                                  [String(Right.id)]: isSearchs[
                                    String(Right.id)
                                  ]
                                    ? !isSearchs[String(Right.id)]
                                    : false
                                });
                              }}
                            >
                              <div>
                                <TextField
                                  fullWidth
                                  placeholder={`Buscar musicas ${Right.name}`}
                                  name="search"
                                  onChange={event => {
                                    setSearchs({
                                      ...searchs,
                                      [String(Right.id)]: event.target.value
                                    });

                                    seachMusic(
                                      event.target.value,
                                      Right.Genres,
                                      false
                                    );
                                  }}
                                  required
                                  value={
                                    searchs[String(Right.id)]
                                      ? searchs[String(Right.id)]
                                      : ""
                                  }
                                  variant="outlined"
                                  InputProps={{
                                    style: {
                                      ...styleButtonIcon,
                                      borderRadius: 25,
                                      marginTop: 15,
                                      marginBottom: 15
                                    },
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <Box display="flex" alignItems="center">
                                          <h5 className="text-black ml-4">
                                            {Right.name}
                                          </h5>
                                          <SearchIcon
                                            style={{
                                              marginLeft: 20,
                                              marginRight: 20
                                            }}
                                          />
                                        </Box>
                                      </InputAdornment>
                                    )
                                  }}
                                />
                                {!(searchs[String(Right.id)]
                                  ? String(searchs[String(Right.id)]).length > 0
                                  : false) ? null : (
                                  <Paper
                                    style={{
                                      marginTop: 10,
                                      position: "absolute",
                                      height: 300,
                                      zIndex: 99999
                                    }}
                                    component="form"
                                    className={classes.root}
                                  >
                                    <div style={{ height: 300 }}>
                                      <PerfectScrollbar
                                        onYReachEnd={() => {
                                          onAddMusicList();
                                        }}
                                      >
                                        {listMusicComplete
                                          .slice(
                                            pageMusic * limitMusic,
                                            pageMusic * limitMusic + limitMusic
                                          )
                                          .map((music, index) => (
                                            <div
                                              key={index}
                                              className="w-100 ct"
                                            >
                                              <Grid
                                                item
                                                lg={12}
                                                md={12}
                                                xs={12}
                                              >
                                                <Box
                                                  onClick={() => {
                                                    setMusicSelect({
                                                      ...musicSelect,
                                                      [music.id]: {
                                                        status: musicSelect[
                                                          String(music.id)
                                                        ]
                                                          ? !musicSelect[
                                                              String(music.id)
                                                            ].status
                                                          : true,
                                                        id: music.id
                                                      }
                                                    });

                                                    showMessage(
                                                      musicSelect[
                                                        String(music.id)
                                                      ]
                                                        ? musicSelect[
                                                            String(music.id)
                                                          ].status
                                                          ? "Musica Deselecionada"
                                                          : "Musica Selecionada"
                                                        : "Musica Selecionada",
                                                      musicSelect[
                                                        String(music.id)
                                                      ]
                                                        ? musicSelect[
                                                            String(music.id)
                                                          ].status
                                                          ? "Musica deselecionada com sucesso"
                                                          : "Musica selecionada com sucesso"
                                                        : "Musica selecionada com sucesso",
                                                      "success"
                                                    );
                                                  }}
                                                  style={{
                                                    backgroundColor:
                                                      "transparent"
                                                  }}
                                                  className="p-1"
                                                  display="flex"
                                                  alignItems="center"
                                                  justifyContent="space-between"
                                                >
                                                  <Box
                                                    display="flex"
                                                    alignItems="center"
                                                  >
                                                    {!musicSelect[
                                                      String(music.id)
                                                    ] ? null : !musicSelect[
                                                        String(music.id)
                                                      ].status ? null : (
                                                      <CheckCircleIcon
                                                        style={{
                                                          color: "#0be881"
                                                        }}
                                                      />
                                                    )}
                                                    <Box
                                                      className="p-1"
                                                      display="flex"
                                                      alignItems="center"
                                                      justifyContent="flex-start"
                                                    >
                                                      <div>
                                                        <div className="d-flex">
                                                          <h5 className="text-white mr-4">
                                                            {String(
                                                              music.title
                                                            ).replace(
                                                              ".mp3",
                                                              ""
                                                            )}
                                                          </h5>
                                                        </div>
                                                        <Typography
                                                          style={{
                                                            color: "white"
                                                          }}
                                                          variant="body1"
                                                        >
                                                          {music.artist}
                                                        </Typography>
                                                      </div>
                                                    </Box>
                                                  </Box>

                                                  <Typography
                                                    style={{ color: "white" }}
                                                    variant="body1"
                                                  >
                                                    {music.totalTime}
                                                  </Typography>
                                                </Box>
                                              </Grid>
                                              <div
                                                className="w-100"
                                                style={{
                                                  background:
                                                    "rgba(255,255,255,0.2)",
                                                  height: 0.09
                                                }}
                                              />
                                            </div>
                                          ))}
                                      </PerfectScrollbar>
                                    </div>
                                  </Paper>
                                )}
                              </div>
                            </ClickAwayListener>
                          </div>
                        )}
                        {isViewsList[String(Right.id)] &&
                          Right.Genres &&
                          Right.Genres.sort((a, b) => {
                            return a.name < b.name
                              ? -1
                              : a.name > b.name
                              ? 1
                              : 0;
                          }).map(genero => (
                            <Accordion
                              classes={{
                                root: classes.MuiAccordionroot
                              }}
                              style={{
                                ...styleButtonIcon,
                                borderRadius: 15,
                                marginTop: 20
                              }}
                              onClick={() => {
                                let arrayIdSelect =
                                  openSelect.filter(is => is == genero.id)
                                    .length > 0
                                    ? openSelect.filter(is => is != genero.id)
                                    : openSelect.concat([genero.id]);

                                console.log(arrayIdSelect);
                                setOpenSelect(arrayIdSelect);

                                if (!genero.Musics) {
                                  onGetMusicGenre(genero, false);
                                }
                              }}
                            >
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-label="Expand"
                                aria-controls="additional-actions1-content"
                                id="additional-actions1-header"
                              >
                                <FormControlLabel
                                  aria-label="Acknowledge"
                                  onClick={event => {
                                    event.stopPropagation();
                                  }}
                                  onFocus={event => event.stopPropagation()}
                                  control={
                                    <Box display="flex" marginRight={2}>
                                      <Checkbox
                                        onChange={() => {
                                          if (!genero.Musics) {
                                            onGetMusicGenre(genero, true);
                                          } else {
                                            onSelectAll(genero);
                                          }
                                        }}
                                      />
                                      <Avatar src={genero.Covers[0].path_s3} />
                                    </Box>
                                  }
                                  label={genero.name}
                                />
                              </AccordionSummary>
                              {!genero.Musics ? null : (
                                <AccordionDetails>
                                  <div>
                                    <Grid container spacing={1}>
                                      {openSelect.filter(is => is == genero.id)
                                        .length > 0
                                        ? genero.Musics.slice(
                                            pageMusic * limitMusic,
                                            pageMusic * limitMusic + limitMusic
                                          ).map((music, index) => {
                                            return (
                                              <div
                                                onClick={event => {
                                                  event.stopPropagation();
                                                  setMusicSelect({
                                                    ...musicSelect,
                                                    [music.id]: {
                                                      status: musicSelect[
                                                        String(music.id)
                                                      ]
                                                        ? !musicSelect[
                                                            String(music.id)
                                                          ].status
                                                        : true,
                                                      id: music.id
                                                    }
                                                  });
                                                }}
                                                style={{
                                                  cursor: "pointer"
                                                }}
                                                className="w-100"
                                              >
                                                <Grid
                                                  item
                                                  lg={12}
                                                  md={12}
                                                  xs={12}
                                                >
                                                  {musicSelect[
                                                    String(music.id)
                                                  ] &&
                                                    (!musicSelect[
                                                      String(music.id)
                                                    ].status ? null : (
                                                      <div
                                                        style={{
                                                          backgroundColor:
                                                            "rgba(0,0,0,0.05)",
                                                          position: "absolute",
                                                          height: 60,
                                                          justifyContent:
                                                            "space-between",
                                                          alignItems: "center",
                                                          display: "flex",
                                                          borderRadius: 10
                                                        }}
                                                        className="w-100"
                                                      >
                                                        <div />
                                                        <CheckCircleIcon
                                                          style={{
                                                            color: "#0be881"
                                                          }}
                                                        />
                                                        <div
                                                          className="h-100"
                                                          style={{
                                                            backgroundColor:
                                                              "#f53b57",
                                                            width: 12,
                                                            borderTopRightRadius: 10,
                                                            borderBottomRightRadius: 10
                                                          }}
                                                        />
                                                      </div>
                                                    ))}

                                                  <div
                                                    style={{
                                                      backgroundColor:
                                                        index == -1
                                                          ? "rgba(0,0,0,0.1)"
                                                          : "transparent",
                                                      justifyContent:
                                                        "space-between",
                                                      alignItems: "center",
                                                      display: "flex"
                                                    }}
                                                    className="p-1 w-100"
                                                  >
                                                    <Box
                                                      className="p-1 w-100"
                                                      display="flex"
                                                      alignItems="center"
                                                      justifyContent="flex-start"
                                                    >
                                                      <div className="ct">
                                                        <Avatar
                                                          alt="Product"
                                                          variant="square"
                                                          src={
                                                            genero.Covers[0]
                                                              .path_s3
                                                          }
                                                          className="av-radio mr-2"
                                                        />
                                                        <PlayArrowIcon className="av-play" />
                                                      </div>
                                                      <div>
                                                        <h5 className="text-black">
                                                          {String(
                                                            music.title
                                                          ).replace(".mp3", "")}
                                                        </h5>
                                                        <Typography
                                                          style={{
                                                            color: "black"
                                                          }}
                                                          variant="body1"
                                                        >
                                                          {music.artist}
                                                        </Typography>
                                                      </div>
                                                    </Box>
                                                    <Typography
                                                      style={{
                                                        color: "black"
                                                      }}
                                                      variant="body1"
                                                    >
                                                      {music.totalTime}
                                                    </Typography>
                                                  </div>
                                                </Grid>
                                                <div
                                                  className="w-100"
                                                  style={{
                                                    background:
                                                      "rgba(0,0,0,0.2)",
                                                    height: 0.09
                                                  }}
                                                />
                                              </div>
                                            );
                                          })
                                        : null}
                                    </Grid>
                                    <TablePagination
                                      component="div"
                                      style={{
                                        marginTop: 20
                                      }}
                                      onClick={event => event.stopPropagation()}
                                      count={
                                        genero.Musics ? genero.Musics.length : 0
                                      }
                                      onChangePage={handlePageChangeMusic}
                                      onChangeRowsPerPage={
                                        handleLimitChangeMusic
                                      }
                                      labelRowsPerPage="Linhas por página:"
                                      page={pageMusic}
                                      rowsPerPage={limitMusic}
                                      rowsPerPageOptions={[2, 5, 10, 25]}
                                    />
                                  </div>
                                </AccordionDetails>
                              )}
                            </Accordion>
                          ))}
                      </div>
                    ))}

                  {heapList &&
                    heapList.length > 0 &&
                    heapList[0].Rights.map(
                      Right =>
                        Right.Genresubs.length > 0 && (
                          <div
                            className="mb-2 mt-4"
                            style={
                              isViewsList[`sub-${String(Right.id)}`]
                                ? {}
                                : {
                                    ...styleButtonIcon,
                                    padding: 15,
                                    borderRadius: 15
                                  }
                            }
                          >
                            {!isSearchs[`sub-${String(Right.id)}`] ? (
                              <Box
                                mb={
                                  isViewsList[`sub-${String(Right.id)}`] ? 4 : 0
                                }
                                display="flex"
                                justifyContent="space-between"
                                alignItems="center"
                              >
                                <Box
                                  onClick={() => {
                                    setIsViewsList({
                                      ...isViewsList,
                                      [`sub-${String(Right.id)}`]: isViewsList[
                                        `sub-${String(Right.id)}`
                                      ]
                                        ? !isViewsList[
                                            `sub-${String(Right.id)}`
                                          ]
                                        : true
                                    });
                                  }}
                                  display="flex"
                                  alignItems="center"
                                  style={{ cursor: "pointer" }}
                                >
                                  <h5 className="text-black mr-4">
                                    {`Sugeridas - ${Right.name}`}
                                  </h5>
                                  {isViewsList[`sub-${String(Right.id)}`] ? (
                                    <ExpandLessIcon />
                                  ) : (
                                    <ExpandMoreIcon />
                                  )}
                                </Box>

                                <Button
                                  onClick={() => {
                                    setIsSearchs({
                                      ...isSearchs,
                                      [`sub-${String(Right.id)}`]: isSearchs[
                                        `sub-${String(Right.id)}`
                                      ]
                                        ? !isSearchs[`sub-${String(Right.id)}`]
                                        : true
                                    });
                                  }}
                                  size="medium"
                                  variant="contained"
                                  style={{
                                    backgroundColor: "white",
                                    color: "red"
                                  }}
                                  startIcon={<SearchIcon />}
                                >
                                  Buscar Musicas
                                </Button>
                              </Box>
                            ) : (
                              <div>
                                <ClickAwayListener
                                  onClickAway={() => {
                                    setSearchs({
                                      ...searchs,
                                      [`sub-${String(Right.id)}`]: searchs[
                                        `sub-${String(Right.id)}`
                                      ]
                                        ? ""
                                        : ""
                                    });
                                    setIsSearchs({
                                      ...isSearchs,
                                      [`sub-${String(Right.id)}`]: isSearchs[
                                        `sub-${String(Right.id)}`
                                      ]
                                        ? !isSearchs[`sub-${String(Right.id)}`]
                                        : false
                                    });
                                  }}
                                >
                                  <div>
                                    <TextField
                                      fullWidth
                                      placeholder={`Buscar musicas Sugeridas - ${Right.name}`}
                                      name="search"
                                      onChange={event => {
                                        setSearchs({
                                          ...searchs,
                                          [`sub-${String(Right.id)}`]: event
                                            .target.value
                                        });

                                        seachMusic(
                                          event.target.value,
                                          Right.Genresubs,
                                          true
                                        );
                                      }}
                                      required
                                      value={
                                        searchs[`sub-${String(Right.id)}`]
                                          ? searchs[`sub-${String(Right.id)}`]
                                          : ""
                                      }
                                      variant="outlined"
                                      InputProps={{
                                        style: {
                                          ...styleButtonIcon,
                                          borderRadius: 25,
                                          marginTop: 15,
                                          marginBottom: 15
                                        },
                                        startAdornment: (
                                          <InputAdornment position="start">
                                            <Box
                                              display="flex"
                                              alignItems="center"
                                            >
                                              <h5 className="text-black ml-4">
                                                {Right.name}
                                              </h5>
                                              <SearchIcon
                                                style={{
                                                  marginLeft: 20,
                                                  marginRight: 20
                                                }}
                                              />
                                            </Box>
                                          </InputAdornment>
                                        )
                                      }}
                                    />
                                    {!(searchs[`sub-${String(Right.id)}`]
                                      ? String(
                                          searchs[`sub-${String(Right.id)}`]
                                        ).length > 0
                                      : false) ? null : (
                                      <Paper
                                        style={{
                                          marginTop: 10,
                                          position: "absolute",
                                          height: 300,
                                          zIndex: 99999
                                        }}
                                        component="form"
                                        className={classes.root}
                                      >
                                        <div style={{ height: 300 }}>
                                          <PerfectScrollbar
                                            onYReachEnd={() => {
                                              onAddMusicList();
                                            }}
                                          >
                                            {listMusicComplete
                                              .slice(
                                                pageMusic * limitMusic,
                                                pageMusic * limitMusic +
                                                  limitMusic
                                              )
                                              .map((music, index) => (
                                                <div
                                                  key={index}
                                                  className="w-100 ct"
                                                >
                                                  <Grid
                                                    item
                                                    lg={12}
                                                    md={12}
                                                    xs={12}
                                                  >
                                                    <Box
                                                      onClick={() => {
                                                        setMusicSelect({
                                                          ...musicSelect,
                                                          [music.id]: {
                                                            status: musicSelect[
                                                              String(music.id)
                                                            ]
                                                              ? !musicSelect[
                                                                  String(
                                                                    music.id
                                                                  )
                                                                ].status
                                                              : true,
                                                            id: music.id
                                                          }
                                                        });

                                                        showMessage(
                                                          musicSelect[
                                                            String(music.id)
                                                          ]
                                                            ? musicSelect[
                                                                String(music.id)
                                                              ].status
                                                              ? "Musica Deselecionada"
                                                              : "Musica Selecionada"
                                                            : "Musica Selecionada",
                                                          musicSelect[
                                                            String(music.id)
                                                          ]
                                                            ? musicSelect[
                                                                String(music.id)
                                                              ].status
                                                              ? "Musica deselecionada com sucesso"
                                                              : "Musica selecionada com sucesso"
                                                            : "Musica selecionada com sucesso",
                                                          "success"
                                                        );
                                                      }}
                                                      style={{
                                                        backgroundColor:
                                                          "transparent"
                                                      }}
                                                      className="p-1"
                                                      display="flex"
                                                      alignItems="center"
                                                      justifyContent="space-between"
                                                    >
                                                      <Box
                                                        className="p-1"
                                                        display="flex"
                                                        alignItems="center"
                                                        justifyContent="flex-start"
                                                      >
                                                        <div>
                                                          <div className="d-flex">
                                                            <h5 className="text-white mr-4">
                                                              {String(
                                                                music.title
                                                              ).replace(
                                                                ".mp3",
                                                                ""
                                                              )}
                                                            </h5>
                                                          </div>
                                                          <Typography
                                                            style={{
                                                              color: "white"
                                                            }}
                                                            variant="body1"
                                                          >
                                                            {music.artist}
                                                          </Typography>
                                                        </div>
                                                      </Box>

                                                      <Typography
                                                        style={{
                                                          color: "white"
                                                        }}
                                                        variant="body1"
                                                      >
                                                        {music.totalTime}
                                                      </Typography>
                                                    </Box>
                                                  </Grid>
                                                  <div
                                                    className="w-100"
                                                    style={{
                                                      background:
                                                        "rgba(255,255,255,0.2)",
                                                      height: 0.09
                                                    }}
                                                  />
                                                </div>
                                              ))}
                                          </PerfectScrollbar>
                                        </div>
                                      </Paper>
                                    )}
                                  </div>
                                </ClickAwayListener>
                              </div>
                            )}
                            {isViewsList[`sub-${String(Right.id)}`] &&
                              Right.Genresubs &&
                              Right.Genresubs.sort((a, b) => {
                                return a.name < b.name
                                  ? -1
                                  : a.name > b.name
                                  ? 1
                                  : 0;
                              }).map(genero => (
                                <Accordion
                                  classes={{
                                    root: classes.MuiAccordionroot
                                  }}
                                  style={{
                                    ...styleButtonIcon,
                                    borderRadius: 15,
                                    marginTop: 20
                                  }}
                                  onClick={() => {
                                    let arrayIdSelect =
                                      openSelect.filter(is => is == genero.id)
                                        .length > 0
                                        ? openSelect.filter(
                                            is => is != genero.id
                                          )
                                        : openSelect.concat([genero.id]);

                                    console.log(arrayIdSelect);
                                    setOpenSelect(arrayIdSelect);

                                    if (!genero.Musics) {
                                      onGetMusicGenreSubs(genero, false);
                                    }
                                  }}
                                >
                                  <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-label="Expand"
                                    aria-controls="additional-actions1-content"
                                    id="additional-actions1-header"
                                  >
                                    <FormControlLabel
                                      aria-label="Acknowledge"
                                      onClick={event => {
                                        event.stopPropagation();
                                      }}
                                      onFocus={event => event.stopPropagation()}
                                      control={
                                        <Box display="flex" marginRight={2}>
                                          <Checkbox
                                            onChange={() => {
                                              if (!genero.Musics) {
                                                onGetMusicGenreSubs(
                                                  genero,
                                                  true
                                                );
                                              } else {
                                                onSelectAllSub(genero);
                                              }
                                            }}
                                          />
                                          <Avatar
                                            src={genero.Covers[0].path_s3}
                                          />
                                        </Box>
                                      }
                                      label={genero.name}
                                    />
                                  </AccordionSummary>
                                  {!genero.Musics ? null : (
                                    <AccordionDetails>
                                      <div>
                                        <Grid container spacing={1}>
                                          {openSelect.filter(
                                            is => is == genero.id
                                          ).length > 0
                                            ? genero.Musics.slice(
                                                pageMusic * limitMusic,
                                                pageMusic * limitMusic +
                                                  limitMusic
                                              ).map((music, index) => {
                                                return (
                                                  <div
                                                    onClick={event => {
                                                      event.stopPropagation();
                                                      setMusicSelect({
                                                        ...musicSelect,
                                                        [music.musicId]: {
                                                          status: musicSelect[
                                                            String(
                                                              music.musicId
                                                            )
                                                          ]
                                                            ? !musicSelect[
                                                                String(
                                                                  music.musicId
                                                                )
                                                              ].status
                                                            : true,
                                                          id: music.musicId
                                                        }
                                                      });
                                                    }}
                                                    style={{
                                                      cursor: "pointer"
                                                    }}
                                                    className="w-100"
                                                  >
                                                    <Grid
                                                      item
                                                      lg={12}
                                                      md={12}
                                                      xs={12}
                                                    >
                                                      {musicSelect[
                                                        String(music.musicId)
                                                      ] &&
                                                        (!musicSelect[
                                                          String(music.musicId)
                                                        ].status ? null : (
                                                          <div
                                                            style={{
                                                              backgroundColor:
                                                                "rgba(0,0,0,0.05)",
                                                              position:
                                                                "absolute",
                                                              height: 60,
                                                              justifyContent:
                                                                "space-between",
                                                              alignItems:
                                                                "center",
                                                              display: "flex",
                                                              borderRadius: 10
                                                            }}
                                                            className="w-100"
                                                          >
                                                            <div />
                                                            <CheckCircleIcon
                                                              style={{
                                                                color: "#0be881"
                                                              }}
                                                            />
                                                            <div
                                                              className="h-100"
                                                              style={{
                                                                backgroundColor:
                                                                  "#f53b57",
                                                                width: 12,
                                                                borderTopRightRadius: 10,
                                                                borderBottomRightRadius: 10
                                                              }}
                                                            />
                                                          </div>
                                                        ))}

                                                      <div
                                                        style={{
                                                          backgroundColor:
                                                            index == -1
                                                              ? "rgba(0,0,0,0.1)"
                                                              : "transparent",
                                                          justifyContent:
                                                            "space-between",
                                                          alignItems: "center",
                                                          display: "flex"
                                                        }}
                                                        className="p-1 w-100"
                                                      >
                                                        <Box
                                                          className="p-1 w-100"
                                                          display="flex"
                                                          alignItems="center"
                                                          justifyContent="flex-start"
                                                        >
                                                          <div className="ct">
                                                            <Avatar
                                                              alt="Product"
                                                              variant="square"
                                                              src={
                                                                genero.Covers[0]
                                                                  .path_s3
                                                              }
                                                              className="av-radio mr-2"
                                                            />
                                                            <PlayArrowIcon className="av-play" />
                                                          </div>
                                                          <div>
                                                            <h5 className="text-black">
                                                              {String(
                                                                music.Music
                                                                  .title
                                                                  ? music.Music
                                                                      .title
                                                                  : ""
                                                              ).replace(
                                                                ".mp3",
                                                                ""
                                                              )}
                                                            </h5>
                                                            <Typography
                                                              style={{
                                                                color: "black"
                                                              }}
                                                              variant="body1"
                                                            >
                                                              {music.Music
                                                                .artist
                                                                ? music.Music
                                                                    .artist
                                                                : ""}
                                                            </Typography>
                                                          </div>
                                                        </Box>
                                                        <Typography
                                                          style={{
                                                            color: "black"
                                                          }}
                                                          variant="body1"
                                                        >
                                                          {music.Music.totalTime
                                                            ? music.Music
                                                                .totalTime
                                                            : ""}
                                                        </Typography>
                                                      </div>
                                                    </Grid>
                                                    <div
                                                      className="w-100"
                                                      style={{
                                                        background:
                                                          "rgba(0,0,0,0.2)",
                                                        height: 0.09
                                                      }}
                                                    />
                                                  </div>
                                                );
                                              })
                                            : null}
                                        </Grid>
                                        <TablePagination
                                          component="div"
                                          style={{
                                            marginTop: 20
                                          }}
                                          onClick={event =>
                                            event.stopPropagation()
                                          }
                                          count={
                                            genero.Musics
                                              ? genero.Musics.length
                                              : 0
                                          }
                                          onChangePage={handlePageChangeMusic}
                                          onChangeRowsPerPage={
                                            handleLimitChangeMusic
                                          }
                                          labelRowsPerPage="Linhas por página:"
                                          page={pageMusic}
                                          rowsPerPage={limitMusic}
                                          rowsPerPageOptions={[2, 5, 10, 25]}
                                        />
                                      </div>
                                    </AccordionDetails>
                                  )}
                                </Accordion>
                              ))}
                          </div>
                        )
                    )}
                </CardContent>
              </PerfectScrollbar>
              <div
                style={{
                  position: "absolute",
                  zIndex: 10,
                  marginTop: -180,
                  width: isMobile == "true" ? "87%" : 720
                }}
              >
                <Box
                  style={{
                    backgroundColor: "white",
                    paddingTop: 5,
                    paddingBottom: 5,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20
                  }}
                  display="flex"
                  justifyContent="center"
                >
                  <Button
                    onClick={() => {
                      onSavePlayList();
                    }}
                    style={{
                      backgroundColor: "#27ae60",
                      color: "white",
                      borderRadius: 20
                    }}
                    className="mt-4 mb-4"
                    variant="contained"
                  >
                    Salvar Playlist
                  </Button>
                </Box>
              </div>
            </Card>
            <Backdrop style={{ zIndex: 999999 }} open={isBackDrop}>
              <CircularProgress style={{ color: "red" }} />
            </Backdrop>
          </Container>
        </PerfectScrollbar>
      </Fade>
    </Modal>
  );
};

export default ModalNewPlayList;
