import React, { useState } from "react";
import {
    Box,
    IconButton,
    Menu,
    MenuItem,
    ListItemIcon,
    Divider,
    Typography
} from '@material-ui/core';
import {
    MoreVert as MoreVertIcon,
    LibraryMusic as LibraryMusicIcon,
    PlaylistPlay as PlaylistPlayIcon,
    Edit as EditIcon,
    Delete as DeleteIcon
} from '@material-ui/icons'
import '../App.scss'

const PlayListCard = ({ onEditPlayList, onPlaylistSelect, playlist, onDeletePlayList }) => {
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className='cts mb-4 ml-2 mr-2'>
            <div onClick={() => { onPlaylistSelect(playlist) }} className='ct'>
                <div className='gn-play-list'>
                    <div className='d-flex justify-content-center align-items-center'>
                        <h6 style={{ color: 'rgba(255,255,255,0.5)' }} className='text-black'>{playlist.countMusic}</h6>
                        <PlaylistPlayIcon style={{ fontSize: 55, color: 'rgba(255,255,255,0.6)' }} />
                    </div>
                </div>

                <div
                    style={{
                        cursor: "pointer",
                        borderRadius: 150,
                        backgroundColor: 'white',
                        height: 150,
                        width: 150,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                    <LibraryMusicIcon style={{
                        fontSize: 50,
                        color: 'black',
                        marginTop: '32%',
                        marginLeft: '32%'
                    }} />
                </div>
            </div>

            <h5 className="text-white text-center mt-2">{playlist.name}</h5>
            <h6 className="text-white text-center">{playlist.countMusic} musicas</h6>
            <div style={{
                position: 'absolute',
                marginLeft: '10%',
                marginTop: -50
            }} className='d-flex justify-content-between'>
                <div />

                <IconButton onClick={handleClick} color='white' className='av-opt-list'>
                    <MoreVertIcon style={{ color: 'white' }} />
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem onClick={() => {
                        onEditPlayList(playlist)
                        handleClose()
                    }}>
                        <ListItemIcon>
                            <EditIcon style={{ color: '#686de0' }} fontSize="small" />
                        </ListItemIcon>
                        <Typography variant="inherit">Editar</Typography>
                    </MenuItem>
                    <Divider />
                    <MenuItem onClick={() => {
                        onDeletePlayList(playlist.id)
                        handleClose()
                    }}>
                        <ListItemIcon>
                            <DeleteIcon style={{ color: '#ff4d4d' }} fontSize="small" />
                        </ListItemIcon>
                        <Typography variant="inherit">Excluir</Typography>
                    </MenuItem>
                </Menu>

            </div>
        </div>
    );
}

export default PlayListCard;
