import React, { useState } from "react";
import {
  Box,
  Grid,
  Typography,
  Avatar,
  Tooltip,
  CircularProgress,
  IconButton
} from "@material-ui/core";
import {
  PlayCircleFilled as PlayCircleFilledIcon,
  PlayCircleFilledWhite as PlayCircleFilledWhiteIcon,
  Check as CheckIcon,
  MoreVert as MoreVertIcon
} from "@material-ui/icons";
import "../App.scss";

const GeneroCard = ({
  generosList,
  onGeneroSelect,
  onGenreList,
  isMobile,
  genreIdsSelectEffect,
  genreSubIdsSelect
}) => {
  const [showSizeIcon, setShowSizeIcon] = useState(false);
  const [showCT, setShowCT] = useState(false);

  return (
    <div
      onClick={() => {
        onGeneroSelect(generosList);
      }}
      style={{ cursor: "pointer", marginBottom: 20 }}
      className="mb-4 mr-4"
      onMouseEnter={() => {
        setShowCT(true);
      }}
      onMouseLeave={() => {
        setShowCT(false);
      }}
    >
      <div>
        {!(genreSubIdsSelect.filter(g => g == generosList.id).length > 0) &
        !(
          genreIdsSelectEffect.filter(g => g.id == generosList.id).length > 0
        ) ? (
          <Box
            style={{
              position: "absolute",
              zIndex: 10,
              height: isMobile == "true" ? 130 : 190,
              width: isMobile == "true" ? 130 : 190,
              background: !showCT
                ? "transparent"
                : "linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.2))",
              transition: ".2s ease"
            }}
            display="flex"
            justifyContent="flex-end"
            flexDirection="column"
            alignItems="flex-end"
            pb={2}
            pr={2}
          >
            {!showCT ? (
              <div />
            ) : (
              <IconButton
                onClick={event => {
                  event.stopPropagation();
                  onGenreList(generosList, true);
                }}
                style={{
                  marginBottom: 60
                }}
              >
                <MoreVertIcon
                  style={{
                    transition: ".2s ease",
                    color: showSizeIcon
                      ? "rgba(255,255,255,1)"
                      : "rgba(255,255,255,0.8)",
                    fontSize: 30
                  }}
                  className=""
                />
              </IconButton>
            )}
            {!showCT ? null : (
              <PlayCircleFilledWhiteIcon
                onMouseEnter={() => {
                  setShowSizeIcon(true);
                }}
                onMouseLeave={() => {
                  setShowSizeIcon(false);
                }}
                style={{
                  transition: ".2s ease",
                  color: showSizeIcon
                    ? "rgba(255,255,255,1)"
                    : "rgba(255,255,255,0.8)",
                  fontSize: showSizeIcon ? 55 : 50
                }}
                className=""
              />
            )}
          </Box>
        ) : (
          <Box
            style={{
              position: "absolute",
              zIndex: 10,
              height: isMobile == "true" ? 130 : 190,
              width: isMobile == "true" ? 130 : 190,
              borderRadius: 5,
              backgroundColor: "rgba(0,0,0,0.7)"
            }}
            display="flex"
            justifyContent="flex-end"
            flexDirection="column"
            alignItems="flex-end"
            pb={2}
            pr={2}
          >
            {!showCT ? (
              <div />
            ) : (
              <IconButton
                onClick={event => {
                  event.stopPropagation();
                  onGenreList(generosList, true);
                }}
                style={{
                  marginBottom: 60
                }}
              >
                <MoreVertIcon
                  style={{
                    transition: ".2s ease",
                    color: showSizeIcon
                      ? "rgba(255,255,255,1)"
                      : "rgba(255,255,255,0.8)",
                    fontSize: 30
                  }}
                  className=""
                />
              </IconButton>
            )}

            {genreIdsSelectEffect.filter(g => g.id == generosList.id).length >
            0 ? (
              <CircularProgress style={{ color: "#26e07e" }} />
            ) : (
              <PlayCircleFilledWhiteIcon
                style={{ fontSize: 55, color: "#26e07e" }}
              />
            )}
          </Box>
        )}

        <Avatar
          style={{
            borderRadius: 5,
            height: isMobile == "true" ? 130 : 190,
            width: isMobile == "true" ? 130 : 190
          }}
          src={
            generosList.Covers.length > 0 ? generosList.Covers[0].path_s3 : ""
          }
        />
      </div>
      <Tooltip
        arrow
        title={
          String(generosList.name).length > 19
            ? `${String(generosList.name)}`
            : ""
        }
      >
        <h5
          style={{ width: isMobile == "true" ? 130 : 190 }}
          className="text-white mt-2"
        >
          {String(generosList.name).length > 19
            ? `${String(generosList.name).substr(0, 20)}...`
            : `${String(generosList.name)}`}
        </h5>
      </Tooltip>

      <h6 style={{ color: "#696969" }} className="mt-1">
        Álbum • {generosList.countMusic} Musicas
      </h6>
    </div>
  );
};

export default GeneroCard;
