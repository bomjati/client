import React, { useState, useEffect, useRef } from "react";
import moment from "moment";
import {
  Card,
  IconButton,
  Box,
  Slider,
  Grid,
  Avatar,
  Typography,
  Hidden,
  CircularProgress
} from "@material-ui/core";
import api from "../../services/api";
import axios from "axios";

import {
  SkipNext as SkipNextIcon,
  SkipPrevious as SkipPreviousIcon,
  PlayArrow as PlayArrowIcon,
  Pause as PauseIcon,
  ArrowDropDown as ArrowDropDownIcon,
  ArrowDropUp as ArrowDropUpIcon,
  VolumeDown as VolumeDownIcon,
  VolumeUp as VolumeUpIcon,
  ThumbDownAlt as ThumbDownAltIcon,
  ThumbUp as ThumbUpIcon,
  FavoriteBorder as FavoriteBorderIcon,
  Favorite as FavoriteIcon,
  Block as BlockIcon
} from "@material-ui/icons";

function getiOS() {
  var userAgent = window.navigator.userAgent,
      platform = window.navigator.platform,
      macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
      windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
      iosPlatforms = ['iPhone', 'iPad', 'iPod'],
      os = null;

  if (macosPlatforms.indexOf(platform) !== -1) {
    os = 'Mac OS';
  } else if (iosPlatforms.indexOf(platform) !== -1) {
    os = 'iOS';
  } else if (windowsPlatforms.indexOf(platform) !== -1) {
    os = 'Windows';
  } else if (/Android/.test(userAgent)) {
    os = 'Android';
  } else if (!os && /Linux/.test(platform)) {
    os = 'Linux';
  }

  return os == "iOS" ? true : false;
}

const Player = ({
  urlMusic,
  onNextMusic,
  onBackMusic,
  onShowListMusicView,
  onLikeFavorite,
  onBlock,
  onDesLikeFavorite,
  onDesBlock,
  onHoraCertaPhrase,
  showListMusic,
  nameMusic,
  nameArtista,
  capa,
  playingSpeech,
  isPauseAuto,
  onPlayingAuto,
  playingNow,
  clientId,
  numMusicList,
  numMusicListPassed,
  listBlocked,
  listFavorited,
  idMusic,
  genreSelect,
  genreName
}) => {
  const [valueProgressMusic, setValueProgressMusic] = useState(1);
  const [valueTimeTotal, setValueTimeTotal] = useState(0);
  const [contTentativas, setContTentativas] = useState(0);
  const [idExeculted, setIdExeculted] = useState(-1);
  const [valueDurationTotal, setValueDurationTotal] = useState("0:00");
  const [valueVolume, setValueVolume] = useState(0.5);
  const [getTextProgressTime, setGetTextProgressTime] = useState("0:00");
  const [audio, setAudio] = useState(document.createElement("audio")); //useState(new Audio());
  //const [audio, setAudio] = useState([new Audio()]);
  const [playingAuto, setPlayingAuto] = useState(false);
  const [showProgress, setShowProgress] = useState(false);

  const loadAudio = async url => {
    setShowProgress(true);
    setPlayingAuto(false);
    //console.log(url);
    audio.muted = true;
    audio.pause();
    audio.src = "";
    var blobUrl;

    var isSafari = window.safari !== undefined;
    if (isSafari || getiOS()) {
       blobUrl = url;
       //alert("Aberto no IOS ou Safari")
    } else {
      const resp = await fetch(url);
      const arrayBuffer = await resp.arrayBuffer();
      const blob = new Blob([arrayBuffer], { type: "audio/aac" });
       blobUrl = window.URL.createObjectURL(blob);
    }

    //console.log(blobUrl);
    setIdExeculted(numMusicList);

    api
      .post(`/exultedmusic/new`, {
        name: `Iniciado a Musica | ${nameMusic} | ${nameArtista} | ${genreName}`,
        clientId: clientId,
        positionList: numMusicList
      })
      .then(resp => {
        if (resp.message == "success") {
        }
      })
      .catch(error => {});

      audio.play();
      audio.src = blobUrl;
      audio.muted = false;
      audio.play();
      audio.volume = valueVolume;
    audio.ontimeupdate = () => {
      handleChangeTimeMusic([], audio.currentTime * 1000);
      timess();
    };
    audio.onended = () => {
      if(nameArtista == "Hora Certa"){
        onHoraCertaPhrase()
      }else{
        onNextMusic();
      }
    };
    setPlayingAuto(true);
    //setPlayingAuto(false);
    // setAudio(new Audio(blobUrl));
    setShowProgress(false);
  };

  useEffect(() => {
    if (idExeculted > -1) {
      if (idExeculted != numMusicList) {
        onAutoPlay();
      }
    }
  }, [idExeculted]);

  const useAudio = () => {
    const toggleAuto = () => play();
    const togglePause = () => pause();
    const newFile = () => onNewFile();
    const timess = () => getTimess();

    const getTimess = () => {
      const timeMusic = moment.duration(audio.duration * 1000);
      if (audio.duration) {
        setValueDurationTotal(
          String(
            timeMusic.minutes() +
              ":" +
              (timeMusic.seconds() < 10
                ? `0${timeMusic.seconds()}`
                : timeMusic.seconds())
          )
        );
        setValueTimeTotal(audio.duration * 1000);
        //console.log(timeMusic.minutes() + ':' + timeMusic.seconds())
      }
    };

    const play = () => {
      api
        .post(`/exultedmusic/new`, {
          name: `Iniciado a Musica | ${nameMusic} | ${nameArtista} | ${genreName}`,
          clientId: clientId,
          positionList: numMusicList
        })
        .then(resp => {
          if (resp.message == "success") {
          }
        })
        .catch(error => {});

      audio.play();
      audio.volume = valueVolume;
      audio.ontimeupdate = () => {
        handleChangeTimeMusic([], audio.currentTime * 1000);
        getTimess();
      };
      audio.onended = () => {
        onNextMusic();
      };
      setPlayingAuto(true);
    };

    const pause = () => {
      api
        .post(`/exultedmusic/new`, {
          name: `Pausado a Musica | ${nameMusic} | ${nameArtista} | ${genreName}`,
          clientId: clientId,
          positionList: numMusicList
        })
        .then(resp => {
          if (resp.message == "success") {
          }
        })
        .catch(error => {});

      audio.pause();
      setPlayingAuto(false);
    };

    const onNewFile = () => {
      loadAudio(urlMusic);
    };

    useEffect(() => {
      // play();
    }, [audio]);

    return [toggleAuto, togglePause, newFile, timess];
  };

  const [toggleAuto, togglePause, newFile, timess] = useAudio();

  const onAutoPlay = () => {
    newFile();
  };

  useEffect(() => {
    onPlayingAuto(playingAuto);
  }, [playingAuto]);

  useEffect(() => {
    if (playingAuto) {
      togglePause();
    } else {
      toggleAuto();
    }
  }, [playingNow]);

  useEffect(() => {
    audio.pause();
    audio.src = "";
    onAutoPlay();
  }, [urlMusic]);

  useEffect(() => {
    if (playingSpeech) {
     // console.log('chegou')
      togglePause();
    } else {
      toggleAuto();
    }
  }, [playingSpeech]);

  useEffect(() => {
    if (
      listBlocked.filter(b =>
        String(nameArtista)
          .toLowerCase()
          .indexOf("vinheta") > -1
          ? b.vignetteId == idMusic
          : String(nameArtista)
              .toLowerCase()
              .indexOf("comercial") > -1
          ? b.commercialId == idMusic
          : String(nameArtista)
              .toLowerCase()
              .indexOf("conteúdo") > -1
          ? b.contentId == idMusic
          : b.musicId == idMusic
      ).length > 0
    ) {
      onNextMusic();
    }
  }, [nameArtista]);

  const handleChangeTimeMusic = (event, newValue) => {
    setValueProgressMusic(newValue);
    getInfoProgress(newValue);
  };

  const handleChangeVolume = (event, newValue) => {
    setValueVolume(newValue);

    audio.volume = newValue;
  };

  const getInfoProgress = value => {
    const timeMusic = moment.duration(value);
    setGetTextProgressTime(
      String(
        timeMusic.minutes() +
          ":" +
          (timeMusic.seconds() < 10
            ? `0${timeMusic.seconds()}`
            : timeMusic.seconds())
      )
    );
  };

  const colorsIcon = "white";

  return (
    <div className="pt-3">
      <Slider
        style={{ position: "absolute", marginBottom: 10, color: "#eb2f06" }}
        className="fixed-top"
        value={valueProgressMusic}
        min={0}
        step={1000}
        max={valueTimeTotal}
        aria-labelledby="non-linear-slider"
      />
      <Card style={{ backgroundColor: "#1A1A1A" }}>
        <Box display="flex" alignItems="center" justifyContent="space-between">
          {/* Player */}
          <Box className="p-1" display="flex" alignItems="center">
            <IconButton
              className="ml-1"
              aria-label="Previous"
              onClick={() => {
                onBackMusic();
              }}
            >
              <SkipPreviousIcon style={{ color: colorsIcon }} />
            </IconButton>
            <IconButton
              aria-label="Play-Pause"
              onClickCapture={() => {
                playingAuto ? togglePause() : toggleAuto();
              }}
            >
              {showProgress ? (
                <CircularProgress style={{ color: "#ff4040" }} />
              ) : playingAuto ? (
                <PauseIcon style={{ fontSize: 40, color: "#ff4040" }} />
              ) : (
                <PlayArrowIcon style={{ fontSize: 40, color: "#ff4040" }} />
              )}
            </IconButton>
            <IconButton
              className="mr-1"
              aria-label="Next"
              onClick={() => {
                onNextMusic();
              }}
            >
              <SkipNextIcon style={{ color: colorsIcon }} />
            </IconButton>
            <Hidden smDown>
              <Box width={100} display="flex" alignItems="center">
                <p style={{ color: colorsIcon }}>{getTextProgressTime}</p>
                <p style={{ color: colorsIcon }} className="ml-1 mr-2">
                  /
                </p>
                <p style={{ color: colorsIcon }}>{valueDurationTotal}</p>
              </Box>
            </Hidden>
          </Box>
          {/* info musica */}
          <Hidden smDown>
            <Box
              className="p-1"
              display="flex"
              alignItems="center"
              justifyContent="center"
            >
              <Avatar
                alt="Product"
                variant="square"
                src={capa}
                className="mr-2"
              />
              <div className="mr-4">
                {/*  <Typography variant="h6">{nameMusic}</Typography> */}
                <h5 className="text-white">{nameMusic}</h5>
                <Typography style={{ color: colorsIcon }} variant="body1">
                  {nameArtista}
                </Typography>
              </div>
              <IconButton
                className="ml-4"
                aria-label="NotLike"
                onClick={() => {
                  const arrayBlock = listBlocked.filter(b =>
                    String(nameArtista)
                      .toLowerCase()
                      .indexOf("vinheta") > -1
                      ? b.vignetteId == idMusic
                      : String(nameArtista)
                          .toLowerCase()
                          .indexOf("comercial") > -1
                      ? b.commercialId == idMusic
                      : String(nameArtista)
                          .toLowerCase()
                          .indexOf("conteúdo") > -1
                      ? b.contentId == idMusic
                      : b.musicId == idMusic
                  );
                  if (arrayBlock.length > 0) {
                    onDesBlock(arrayBlock[0].id);
                  } else {
                    onBlock();
                  }
                }}
              >
                <BlockIcon
                  style={{
                    color:
                      listBlocked.filter(b =>
                        String(nameArtista)
                          .toLowerCase()
                          .indexOf("vinheta") > -1
                          ? b.vignetteId == idMusic
                          : String(nameArtista)
                              .toLowerCase()
                              .indexOf("comercial") > -1
                          ? b.commercialId == idMusic
                          : String(nameArtista)
                              .toLowerCase()
                              .indexOf("conteúdo") > -1
                          ? b.contentId == idMusic
                          : b.musicId == idMusic
                      ).length > 0
                        ? "#e67e22"
                        : colorsIcon
                  }}
                />
              </IconButton>
              <IconButton
                className="ml-1"
                aria-label="Like"
                onClick={() => {
                  const arrayFavorited = listFavorited.filter(b =>
                    String(nameArtista)
                      .toLowerCase()
                      .indexOf("vinheta") > -1
                      ? b.vignetteId == idMusic
                      : String(nameArtista)
                          .toLowerCase()
                          .indexOf("comercial") > -1
                      ? b.commercialId == idMusic
                      : String(nameArtista)
                          .toLowerCase()
                          .indexOf("conteúdo") > -1
                      ? b.contentId == idMusic
                      : b.musicId == idMusic
                  );
                  if (arrayFavorited.length > 0) {
                    onDesLikeFavorite(arrayFavorited[0].id);
                  } else {
                    onLikeFavorite();
                  }
                }}
              >
                {listFavorited.filter(f =>
                  String(nameArtista)
                    .toLowerCase()
                    .indexOf("vinheta") > -1
                    ? f.vignetteId == idMusic
                    : String(nameArtista)
                        .toLowerCase()
                        .indexOf("comercial") > -1
                    ? f.commercialId == idMusic
                    : String(nameArtista)
                        .toLowerCase()
                        .indexOf("conteúdo") > -1
                    ? f.contentId == idMusic
                    : f.musicId == idMusic
                ).length > 0 ? (
                  <FavoriteIcon style={{ color: "#eb2f06" }} />
                ) : (
                  <FavoriteBorderIcon style={{ color: colorsIcon }} />
                )}
              </IconButton>
            
            </Box>
          </Hidden>
          {/* funcionalidades */}
          <Box
            className="p-1"
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
          >
            {/* volume */}
            <Hidden only={["xs", "xl"]}>
              <Box
                className=""
                display="flex"
                alignItems="center"
                justifyContent="flex-end"
              >
                <VolumeDownIcon
                  style={{ color: colorsIcon }}
                  className="mr-2"
                />
                <Slider
                  min={0}
                  max={1}
                  step={0.01}
                  value={valueVolume}
                  onChange={handleChangeVolume}
                  style={{
                    width: 100,
                    color: colorsIcon
                  }}
                  aria-labelledby="continuous-slider"
                />
                <VolumeUpIcon style={{ color: colorsIcon }} className="ml-2" />
              </Box>
            </Hidden>

            <IconButton
              className="ml-2"
              aria-label="Previous"
              onClick={() => {
                onShowListMusicView();
              }}
            >
              {showListMusic ? (
                <ArrowDropDownIcon style={{ color: colorsIcon }} />
              ) : (
                <ArrowDropUpIcon style={{ color: colorsIcon }} />
              )}
            </IconButton>
          </Box>
        </Box>
      </Card>
    </div>
  );
};

export default Player;
