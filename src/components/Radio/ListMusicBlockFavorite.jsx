import React, { useState, useEffect, useRef, MouseEvent } from "react";
import {
  Grid,
  Container,
  Box,
  Avatar,
  Typography,
  Divider,
  Hidden,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
} from "@material-ui/core";
import {
  PlayArrow as PlayArrowIcon,
  MoreVert as MoreVertIcon,
  Block as BlockIcon,
  Favorite as FavoriteIcon,
  Lock as LockIcon,
} from "@material-ui/icons";
import "./App.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const ListMusicBlockFavorite = ({
  listBlocked,
  listFavorited,
  onDesLikeFavorite,
  onDesBlock,
  isBlocked,
}) => {
  const [listMusic, setListMusic] = useState(null);
  const [limitMusic, setLimitMusic] = useState(20);
  const [pageMusic, setPageMusic] = useState(0);
  const [openMusicSelect, setOpenMusicSelect] = useState({
    id: 0,
    active: null,
  });
  const [visibleMenuMusicSelect, setVisibleMenuMusicSelect] = useState({
    id: 0,
    active: false,
  });
  const scrollListMusic = useRef(null);

  useEffect(() => {
    console.log(listFavorited);
    setListMusic(isBlocked ? listBlocked : listFavorited);
  }, [listBlocked, listFavorited]);

  const onAddMusicList = () => {
    setLimitMusic(listMusic.length > limitMusic ? limitMusic + 10 : limitMusic);
  };

  const handleClose = (id) => {
    setOpenMusicSelect({
      id: 0,
      active: null,
    });
  };

  const handleClick = (event: MouseEvent<HTMLButtonElement>, id) => {
    setOpenMusicSelect({
      id: id,
      active: event.currentTarget,
    });
  };

  return (
    <div
      style={{
        marginTop: 45,
      }}
    >
      <div style={{ background: "transparent" }}>
        {!listMusic ? null : (
          <Container maxWidth="lg">
            <Grid container spacing={3}>
              <Grid item lg={12} md={12} xs={12}>
                <div
                  style={{
                    top: 0,
                    bottom: 0,
                    height: "100%",
                  }}
                >
                  <div style={{ height: "55vh" }}>
                    <h4 className="text-white ml-4"></h4>
                    <PerfectScrollbar
                      onYReachEnd={() => {
                        onAddMusicList();
                      }}
                      ref={scrollListMusic}
                    >
                      {listMusic
                        .slice(
                          pageMusic * limitMusic,
                          pageMusic * limitMusic + limitMusic
                        )
                        .map((music, index) => {
                          return (
                            <div
                              onMouseEnter={() => {
                                setVisibleMenuMusicSelect({
                                  id: music.id,
                                  active: true,
                                });
                              }}
                              onMouseLeave={() => {
                                setVisibleMenuMusicSelect({
                                  id: music.id,
                                  active: false,
                                });
                              }}
                              key={index}
                              className="w-100 ct"
                            >
                              <Grid item lg={12} md={12} xs={12}>
                                <Box
                                  style={{
                                    backgroundColor: "transparent",
                                  }}
                                  className="p-1"
                                  display="flex"
                                  alignItems="center"
                                  justifyContent="space-between"
                                >
                                  <Box
                                    className="p-1"
                                    display="flex"
                                    alignItems="center"
                                    justifyContent="flex-start"
                                  >
                                    <div>
                                      <Avatar
                                        alt="Product"
                                        variant="square"
                                        src={
                                          music.Commercial
                                            ? "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
                                            : music.Vignette
                                            ? "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
                                            : music.Content
                                            ? "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
                                            : music.Music
                                            ? music.Music.Genre.Covers[0]
                                                .path_s3
                                            : "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
                                        }
                                        className="av-radio mr-2"
                                      />
                                      <PlayArrowIcon className="av-play" />
                                    </div>
                                    <div>
                                      <div className="d-flex">
                                        <h5 className="text-white mr-4">
                                          {String(
                                            music.Commercial
                                              ? music.Commercial.name
                                              : music.Vignette
                                              ? music.Vignette.name
                                              : music.Content
                                              ? music.Content.name
                                              : music.Music ? music.Music.title: "Não definido"
                                          ).replace(".mp3", "")
                                          .replace(".m4a", "")}
                                        </h5>
                                      </div>
                                      <Typography
                                        style={{ color: "white" }}
                                        variant="body1"
                                      >
                                        {music.Commercial
                                          ? "Comercial"
                                          : music.Vignette
                                          ? "Vinheta"
                                          : music.Content
                                          ? "Conteúdo"
                                          : music.Music ? music.Music.artist : "Não definido"}
                                      </Typography>
                                    </div>
                                  </Box>
                                  {visibleMenuMusicSelect.id == music.id ? (
                                    <div>
                                      <IconButton
                                        onClick={(event) => {
                                          event.stopPropagation();
                                          handleClick(event, music.id);
                                        }}
                                      >
                                        <MoreVertIcon
                                          style={{
                                            color: "rgba(255,255,255, 0.5)",
                                          }}
                                        />
                                      </IconButton>
                                      <Menu
                                        id="simple-menu"
                                        anchorEl={
                                          openMusicSelect.id == music.id
                                            ? openMusicSelect.active
                                            : null
                                        }
                                        PaperProps={{
                                          style: {
                                            backgroundColor: "#303030",
                                          },
                                        }}
                                        keepMounted
                                        open={Boolean(
                                          openMusicSelect.id == music.id
                                            ? openMusicSelect.active
                                            : null
                                        )}
                                        onClose={handleClose}
                                      >
                                        {isBlocked ? (
                                          <MenuItem
                                            onClick={() => {
                                              handleClose(music.id);
                                              onDesBlock(music.id);
                                            }}
                                          >
                                            <ListItemIcon>
                                              <BlockIcon
                                                style={{
                                                  color:
                                                    "rgba(255,255,255,0.9)",
                                                }}
                                              />
                                            </ListItemIcon>
                                            <ListItemText
                                              style={{
                                                color: "rgba(255,255,255,0.9)",
                                              }}
                                            >
                                              {"Desbloquear"}
                                            </ListItemText>
                                          </MenuItem>
                                        ) : (
                                          <MenuItem
                                            onClick={() => {
                                              handleClose(music.id);
                                              onDesLikeFavorite(music.id);
                                            }}
                                          >
                                            <ListItemIcon>
                                              <FavoriteIcon
                                                style={{
                                                  color:
                                                    "rgba(255,255,255,0.9)",
                                                }}
                                              />
                                            </ListItemIcon>
                                            <ListItemText
                                              style={{
                                                color: "rgba(255,255,255,0.9)",
                                              }}
                                            >
                                              {"Desfavoritar"}
                                            </ListItemText>
                                          </MenuItem>
                                        )}
                                      </Menu>
                                    </div>
                                  ) : (
                                    <Typography
                                      style={{ color: "white" }}
                                      variant="body1"
                                    >
                                      {music.Commercial
                                        ? ""
                                        : music.Vignette
                                        ? ""
                                        : music.Content
                                        ? ""
                                        : music.Music ? music.Music.totalTime : ""}
                                    </Typography>
                                  )}
                                </Box>
                              </Grid>
                              <div
                                className="w-100"
                                style={{
                                  background: "rgba(255,255,255,0.2)",
                                  height: 0.09,
                                }}
                              />
                            </div>
                          );
                        })}
                    </PerfectScrollbar>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Container>
        )}
      </div>
    </div>
  );
};

export default ListMusicBlockFavorite;
