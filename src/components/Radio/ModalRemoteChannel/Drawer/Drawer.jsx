import React from "react";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Avatar,
  Box,
  Chip,
  Divider,
  makeStyles,
} from "@material-ui/core";
import {
  PlaylistAdd as PlaylistAddIcon,
  SettingsVoice as SettingsVoiceIcon,
  Assignment as AssignmentIcon,
  DynamicFeed as DynamicFeedIcon,
  PersonAdd as PersonAddIcon,
  Settings as SettingsIcon
} from "@material-ui/icons";
import moment from "moment";

const user = {
  avatar: "/static/images/avatars/avatar_4.png",
};

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 67,
    height: "calc(100% - 64px)",
  },
  avatar: {
    cursor: "pointer",
    width: 64,
    height: 64,
  },
  botaoPlay: {
    "&:focus": {
      outline: "none",
    },
  },
}));

const Drawers = ({
  onList,
  onChamada,
  onGenre,
  onShowContent,
  onShowConfig,
  isShowMenu,
  onClick,
  onKeyDown,
  onClose,
  clientName,
}) => {
  const classes = useStyles();
  const listMenu = [
    {
      onClick: () => onList(),
      name: "Lista de Execução",
      icon: <PlaylistAddIcon />,
    },
    {
      onClick: () => onGenre(),
      name: "Generos",
      icon: <PersonAddIcon />,
    },
    {
      onClick: () => onChamada(),
      name: "Avisos",
      icon: <SettingsVoiceIcon />,
    },
    {
      onClick: () => onShowContent(),
      name: "Conteúdos",
      icon: <DynamicFeedIcon />,
    },
    {
      onClick: () => onShowConfig(),
      name: "Configurações",
      icon: <SettingsIcon />,
    },
  ];

  return (
    <Drawer
      anchor={"left"}
      open={isShowMenu}
      onClose={() => {
        onClose();
      }}
    >
      <div
        style={{ width: 235 }}
        role="presentation"
        onClick={() => {
          onClick();
        }}
        onKeyDown={() => {
          onKeyDown();
        }}
      >
        <Box alignItems="center" display="flex" flexDirection="column" p={2}>
          <Avatar className={classes.avatar} src={user.avatar} />
          <Typography
            className="text-center"
            color="textPrimary"
            variant="body1"
            style={{ fontWeight: "bold", marginTop: 10 }}
          >
            {clientName}
          </Typography>
          <Typography color="textSecondary" variant="body2">
            Bomja Music
          </Typography>
          <Chip
            style={{
              borderColor: "#3B3B98",
              backgroundColor: "#3B3B98",
              color: "white",
              marginTop: 5,
            }}
            label="Free"
            variant="outlined"
            size="small"
          />
        </Box>
        <Divider />
        <List>
          {listMenu.map((item) => (
            <div
              onClick={() => {
                item.onClick();
              }}
            >
              <ListItem button key={item.name}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText style={{ fontSize: "9" }} primary={item.name} />
              </ListItem>
            </div>
          ))}
        </List>
      </div>
    </Drawer>
  );
};

export default Drawers;
