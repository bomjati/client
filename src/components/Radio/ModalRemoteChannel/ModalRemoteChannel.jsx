import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import NewChannel from "./FormRemoteChannel";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalNewBusiness = ({
  open,
  onCloseRemoteChannel,
  getAllChannel,
  getRemoteChannelId,
  listMusic,
  onNextMusic,
  onBackMusic,
  onPlayMusic,
  onRefreshGuia,
  onCloseGuia,
  onSelectGenre,
  onSelectSubGenre
}) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseRemoteChannel}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4">
              <NewChannel
                onCloseModal={onCloseRemoteChannel}
                getRemoteChannelId={getRemoteChannelId}
                listMusic={listMusic}
                onBackMusic={onBackMusic}
                onNextMusic={onNextMusic}
                onPlayMusic={onPlayMusic}
                getAllChannel={getAllChannel}
                onCloseGuia={onCloseGuia}
                onRefreshGuia={onRefreshGuia}
                onSelectGenre={onSelectGenre}
                onSelectSubGenre={onSelectSubGenre}
              />
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewBusiness;
