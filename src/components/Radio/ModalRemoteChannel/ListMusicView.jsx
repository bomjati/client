import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
import {
  Grid,
  Container,
  Box,
  Avatar,
  Typography,
  Divider,
  Hidden,
  IconButton,
  Menu,
  MenuItem,
  Card
} from "@material-ui/core";
import {
  MoreVert as MoreVertIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  Lock as LockIcon
} from "@material-ui/icons";
import "./App.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}
export function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

const ListMusicView = ({
  capa,
  listMusic,
  onPlayMusic,
  idMusicSelect,
  isPlayList,
  isPlayListSub,
  onModalMenuMusic,
  ThemeColorChannel,
  listFavorited,
  listBlocked
}) => {
  const [activeMenus, setActiveMenus] = useState({});
  const [limitMusic, setLimitMusic] = useState(20);
  const [pageMusic, setPageMusic] = useState(0);
  const { height, width } = useWindowDimensions();

  const scrollListMusic = useRef(null);

  const onAddMusicList = () => {
    setLimitMusic(listMusic.length > limitMusic ? limitMusic + 10 : limitMusic);
  };

  return (
    <div
      style={{
        backgroundColor: ThemeColorChannel == "white" ? "white" : "#0B0B0B",
        marginTop: 50,
        marginLeft: -20,
        marginRight: -20
      }}
    >
      <h4
        style={{ color: ThemeColorChannel == "white" ? "black" : "white" }}
        className="ml-4 mb-4"
      >
        Lista de Musicas
      </h4>
      <div style={{ height: (height * 65) / 100 }}>
        <PerfectScrollbar
          onYReachEnd={() => {
            onAddMusicList();
          }}
        >
          {listMusic
            .slice(pageMusic * limitMusic, pageMusic * limitMusic + limitMusic)
            .map((music, index) => {
              return (
                <div key={index} className="w-100">
                  <Box
                    style={{
                      backgroundColor:
                        index == idMusicSelect
                          ? "rgba(245, 59, 87, 0.2)"
                          : "transparent"
                    }}
                    className="pb-1 pt-1 pl-1"
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Box
                      className="p-1"
                      display="flex"
                      alignItems="center"
                      justifyContent="flex-start"
                    >
                      <div>
                        <div className="d-flex">
                          <p
                            style={{
                              fontWeight: "bold",
                              color:
                                ThemeColorChannel == "white"
                                  ? "black"
                                  : "white"
                            }}
                            className="mr-4"
                          >
                            {String(
                              isPlayList ? music.title : music.title
                            ).replace(".mp3", "")}
                          </p>
                          {listBlocked.filter(b =>
                            String(music.artist)
                              .toLowerCase()
                              .indexOf("vinheta") > -1
                              ? b.vignetteId == music.id
                              : String(music.artist)
                                  .toLowerCase()
                                  .indexOf("comercial") > -1
                              ? b.commercialId == music.id
                              : String(music.artist)
                                  .toLowerCase()
                                  .indexOf("conteúdo") > -1
                              ? b.contentId == music.id
                              : b.musicId == music.id
                          ).length > 0 ? (
                            <div
                              style={{ alignItems: "center" }}
                              className="d-flex"
                            >
                              <p
                                style={{
                                  fontWeight: "bold",
                                  color:
                                    ThemeColorChannel == "white"
                                      ? "#e67e22"
                                      : "white"
                                }}
                                className="mr-1"
                              >
                                (Bloqueado)
                              </p>
                              <LockIcon
                                style={{ color: "#e67e22" }}
                                fontSize="small"
                              />
                            </div>
                          ) : null}
                        </div>
                        <Typography
                          style={{
                            color:
                              ThemeColorChannel == "white" ? "black" : "white"
                          }}
                          variant="body1"
                        >
                          {isPlayList ? music.artist : music.artist}
                        </Typography>
                        <p
                          style={{
                            fontWeight: "bold",
                            color:
                              ThemeColorChannel == "white" ? "silver" : "white"
                          }}
                          p
                        >
                          {isPlayList ? music.totalTime : music.totalTime}
                        </p>
                      </div>
                    </Box>
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="flex-start"
                    >
                      <div>
                        <IconButton
                          onClick={() => {
                            onModalMenuMusic(music);
                          }}
                        >
                          <MoreVertIcon
                            style={{
                              color:
                                ThemeColorChannel == "white"
                                  ? "silver"
                                  : "white"
                            }}
                          />
                        </IconButton>
                      </div>
                    </Box>
                  </Box>
                  <div
                    className="w-100"
                    style={{
                      background: "rgba(255, 0, 0, 0.2)",
                      height: 0.09
                    }}
                  />
                </div>
              );
            })}
        </PerfectScrollbar>
      </div>
    </div>
  );
};

export default ListMusicView;
