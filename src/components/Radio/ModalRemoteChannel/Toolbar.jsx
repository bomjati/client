import React from "react";
import { AppBar, Toolbar, IconButton } from "@material-ui/core";
import {
  SettingsVoice as SettingsVoiceIcon,
  PlaylistAdd as PlaylistAddIcon,
  Menu as MenuIcon,
} from "@material-ui/icons";

const MenuBar = ({ onOpenMenu, nameClient }) => {
  return (
    <AppBar position="fixed">
      <Toolbar
        style={{ backgroundColor: "#1A1A1A" }}
        className="justify-content-between"
      >
        <div className="d-flex align-items-center">
          <IconButton
            onClick={() => {
              onOpenMenu();
            }}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon style={{ color: "red" }} />
          </IconButton>
          <h3 style={{ color: "white" }} className="text-black ml-2">
            {nameClient ? nameClient : "Bomja Music"}
          </h3>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default MenuBar;
