import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Modal,
  Fade,
  Slide,
  Backdrop,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {
  Close as CloseIcon,
  SkipNext as SkipNextIcon,
  SkipPrevious as SkipPreviousIcon,
  PlayArrow as PlayArrowIcon,
  Pause as PauseIcon,
  ArrowDropDown as ArrowDropDownIcon,
  ArrowDropUp as ArrowDropUpIcon,
  VolumeDown as VolumeDownIcon,
  VolumeUp as VolumeUpIcon,
  ThumbDownAlt as ThumbDownAltIcon,
  ThumbUp as ThumbUpIcon,
  PowerSettingsNew as PowerSettingsNewIcon,
  Block as BlockIcon,
  Favorite as FavoriteIcon,
  Favorite,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../utils/message";
import ListMusicView from "./ListMusicView";
import Drawer from "./Drawer";
import Toolbar from "./Toolbar";
import Speech from "./Speech";
import api from "../../../services/api";
import GeneroCard from "./Cards/GerneroCard";
import GeneroSubCard from "./Cards/GerneroSubCard";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const Formulario = ({
  getRemoteChannelId,
  clientName,
  listMusic,
  socket,
  listBlocked,
  listFavorited,
  onGetBlockeds,
  onGetFavoriteds,
  heapList,
  getSettingChannelModify,
}) => {
  let arrayListMusic = {};

  const [value, setValue] = useState(0);
  // const [heapList, setHeapList] = useState([]);
  const [typeGroupSelect, setTypeGroupSelect] = useState("genero");
  const [isPlayList, setIsPlayList] = useState(false);
  const [isPlayListSub, setIsPlayListSub] = useState(false);
  const [generoSelect, setGeneroSelect] = useState(null);
  const [numMusicList, setNumMusicList] = useState(0);
  const [genreIdsSelect, setGenreIdsSelect] = useState(null);
  const [playingAuto, setPlayingAuto] = useState(false);
  //const [listMusic, setListMusic] = useState({});
  const [isShowMenu, setIsShowMenu] = useState(false);
  const [socketConnected, setSocketConnected] = useState(false);
  const [showModalConfigMusic, setShowModalConfigMusic] = useState(false);
  const [musicModify, setMusicModify] = useState(null);

  const ThemeColorChannel = "white"; // dark
  const colorsIcon = "red";
  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  const typeSubListMobile = [
    { sub: "Nacional" },
    { sub: "Internacional" },
    { sub: "Mix" },
    { sub: "Forró" },
    { sub: "Sertanejo" },
    { sub: "Gospel" },
    { sub: "Gaúcha" },
    { sub: "Variedades" },
    { sub: "BPM" },
  ];

  const typeSugListMobile = [
    { sub: "Forró" },
    { sub: "Sertanejo" },
    { sub: "Variedades" },
  ];

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  useEffect(() => {
console.log(heapList)
  }, [heapList])

  useEffect(() => {
   // console.log(listMusic);
    if (JSON.stringify(listMusic) != "{}") {
      setNumMusicList(parseInt(listMusic.numMusicList));
      setGenreIdsSelect(JSON.parse(listMusic.genreIdsSelect));
      setGeneroSelect(JSON.parse(listMusic.music));
      setIsPlayListSub(JSON.parse(listMusic.isPlayListSub));
      setIsPlayList(JSON.parse(listMusic.isPlayList));
      setPlayingAuto(
        JSON.parse(listMusic.playingAuto == "" ? "true" : listMusic.playingAuto)
      );
    }
  }, [listMusic]);

  const onBackMusic = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "backMusic",
      exulted: false,
    });
  };

  const onNextMusic = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "nextMusic",
      exulted: false,
    });
  };

  const onGetBlock = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "getBlock",
      exulted: false,
    });
  };

  const onGetFavorited = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "getFavorited",
      exulted: false,
    });
  };

  const onPlayMusic = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "playMusic",
      exulted: false,
    });
  };

  const onRefreshGuia = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "refreshGuia",
      exulted: false,
    });
  };

  const onCloseGuia = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "closeGuia",
      exulted: false,
    });
  };

  const onSelectGenre = (genre) => {
    socket.emit("SendInfoSelectGenre", {
      clientId: getRemoteChannelId,
      genre: genre,
      exulted: false,
    });
  };

  const onSelectSubGenre = (subGenre) => {
    socket.emit("SendInfoSelectSubGenre", {
      clientId: getRemoteChannelId,
      genre: subGenre,
      exulted: false,
    });
  };

  const onModalMenuMusic = (music) => {
    setMusicModify(music);
    setShowModalConfigMusic(true);
  };

  const onBlock = (id, artist) => {
    const arrayBase = {
      musicId: 0,
      contentId: 0,
      commercialId: 0,
      vignetteId: 0,
      clientId: 0,
    };

    const data =
      String(artist).toLowerCase().indexOf("vinheta") > -1
        ? {
            ...arrayBase,
            vignetteId: id,
            clientId: getRemoteChannelId,
          }
        : String(artist).toLowerCase().indexOf("comercial") > -1
        ? {
            ...arrayBase,
            commercialId: id,
            clientId: getRemoteChannelId,
          }
        : String(artist).toLowerCase().indexOf("conteúdo") > -1
        ? {
            ...arrayBase,
            contentId: id,
            clientId: getRemoteChannelId,
          }
        : { ...arrayBase, musicId: id, clientId: getRemoteChannelId };

    api
      .post("/blocked/new", data)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Bloqueada",
            "Musica adicionada na sua lista de bloqueio.",
            "success"
          );
          onGetBlock();
          onGetBlockeds();
        } else {
          showMessage(
            "Musica Bloqueada",
            "Musica falhou ao bloquear.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onLikeFavorite = (id, artist) => {
    const arrayBase = {
      musicId: 0,
      contentId: 0,
      commercialId: 0,
      vignetteId: 0,
      clientId: 0,
    };

    const data =
      String(artist).toLowerCase().indexOf("vinheta") > -1
        ? {
            ...arrayBase,
            vignetteId: id,
            clientId: getRemoteChannelId,
          }
        : String(artist).toLowerCase().indexOf("comercial") > -1
        ? {
            ...arrayBase,
            commercialId: id,
            clientId: getRemoteChannelId,
          }
        : String(artist).toLowerCase().indexOf("conteúdo") > -1
        ? {
            ...arrayBase,
            contentId: id,
            clientId: getRemoteChannelId,
          }
        : { ...arrayBase, musicId: id, clientId: getRemoteChannelId };

    api
      .post("/favorited/new", data)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Favoritada",
            "Musica adicionada na sua lista de favoritas.",
            "success"
          );
          onGetFavoriteds();
          onGetFavorited();
        } else {
          showMessage(
            "Musica Favoritada",
            "Musica falhou ao favoritar.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onDesBlock = (id) => {
    api
      .delete(`/blocked/${id}`)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Desbloqueada",
            "Musica removida da sua lista de bloqueio.",
            "success"
          );
          onGetBlockeds();
          onGetBlock()
        } else {
          showMessage(
            "Musica Desbloqueada",
            "Musica falhou ao desbloquear.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onDesLikeFavorite = (id) => {
    api
      .delete(`/favorited/${id}`)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Desfavoritada",
            "Musica removida da sua lista de favoritas.",
            "success"
          );
          onGetFavoriteds();
          onGetFavorited()
        } else {
          showMessage(
            "Musica Desfavoritada",
            "Musica falhou ao desfavoritar.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  return (
    <div
      style={{
        backgroundColor: ThemeColorChannel == "white" ? "white" : "#0B0B0B",
      }}
    >
      <Toolbar
        nameClient={clientName}
        onOpenMenu={() => {
          setIsShowMenu(true);
        }}
      />

      <div
        style={{
          backgroundColor: ThemeColorChannel == "white" ? "white" : "#0B0B0B",
        }}
        className="mb-4"
      >
        {!generoSelect ? null : (
          <TabPanel value={value} index={0} dir={theme.direction}>
            {!generoSelect ? null : (
              <ListMusicView
                ThemeColorChannel={ThemeColorChannel}
                listMusic={generoSelect.Musics}
                idMusicSelect={numMusicList}
                isPlayList={isPlayList}
                isPlayListSub={isPlayListSub}
                onModalMenuMusic={onModalMenuMusic}
                listBlocked={listBlocked}
                listFavorited={listFavorited}
              />
            )}
          </TabPanel>
        )}
        <TabPanel value={value} index={1} dir={theme.direction}>
          <div
            style={{
              marginTop: 50,
              marginBottom: 20,
              backgroundColor: "#0B0B0B",
            }}
          >
            {heapList.length > 0 &&
              heapList[0].Rights.map((Right) => (
                <div
                  style={{
                    backgroundColor:
                      ThemeColorChannel == "white" ? "white" : "#0B0B0B",
                  }}
                >
                  <h6
                    style={{
                      color: ThemeColorChannel == "white" ? "black" : "white",
                    }}
                    className="text-black text-center mb-4"
                  >
                    Lista de musicas {String(Right.name).toLowerCase()}
                  </h6>

                  <div className="">
                    {!(Right.Genresubs.length > 0) ? null : (
                      <h5
                        style={{
                          color:
                            ThemeColorChannel == "white" ? "black" : "white",
                        }}
                        className="text-black mb-4"
                      >
                        Playlists Sugeridas
                      </h5>
                    )}
                    <PerfectScrollbar>
                      <Box display="flex">
                        {Right.Genresubs.map((generos) => (
                          <GeneroSubCard
                            ThemeColorChannel={ThemeColorChannel}
                            generosList={generos}
                            onGeneroSelect={onSelectSubGenre}
                          />
                        ))}
                      </Box>
                    </PerfectScrollbar>

                    {!(Right.Genres.length > 0) ? null : (
                      <h5
                        style={{
                          color:
                            ThemeColorChannel == "white" ? "black" : "white",
                        }}
                        className="text-black mb-4"
                      >
                        Generos
                      </h5>
                    )}
                    <PerfectScrollbar>
                      <Box display="flex">
                        {Right.Genres.map((generos) => (
                          <GeneroCard
                            ThemeColorChannel={ThemeColorChannel}
                            generosList={generos}
                            onGeneroSelect={onSelectGenre}
                          />
                        ))}
                      </Box>
                    </PerfectScrollbar>
                  </div>
                </div>
              ))}
          </div>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <Speech
            ThemeColorChannel={ThemeColorChannel}
            clientId={getRemoteChannelId}
            socket={socket}
          />
        </TabPanel>
        <TabPanel value={value} index={3} dir={theme.direction}></TabPanel>
      </div>

      <div
        style={{
          backgroundColor: "white",
          boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
          WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
          MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
        }}
        className="fixed-bottom w-100"
      >
        <Divider />

        {value == 0 ? (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <IconButton
              disabled={!generoSelect}
              style={styleButtonIcon}
              className="mr-4"
              aria-label="Previous"
              onClick={() => {
                onBackMusic();
              }}
            >
              <SkipPreviousIcon style={{ color: colorsIcon }} />
            </IconButton>
            <IconButton
              disabled={!generoSelect}
              className="mr-4"
              style={styleButtonIcon}
              aria-label="Play-Pause"
              onClick={() => {
                onPlayMusic();
              }}
            >
              {playingAuto ? (
                <PauseIcon style={{ fontSize: 40, color: colorsIcon }} />
              ) : (
                <PlayArrowIcon style={{ fontSize: 40, color: colorsIcon }} />
              )}
            </IconButton>
            <IconButton
              disabled={!generoSelect}
              style={styleButtonIcon}
              className="mr-1"
              aria-label="Next"
              onClick={() => {
                onNextMusic();
              }}
            >
              <SkipNextIcon style={{ color: colorsIcon }} />
            </IconButton>
          </Box>
        ) : value == 1 ? (
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={() => {
                onRefreshGuia();
              }}
              style={{ color: "white", backgroundColor: "red" }}
              variant="contained"
            >
              Atualisar Página
            </Button>
            <IconButton
              style={{ ...styleButtonIcon, backgroundColor: "white" }}
              aria-label="Previous"
              onClick={() => {
                onCloseGuia();
              }}
            >
              <PowerSettingsNewIcon style={{ color: "red" }} />
            </IconButton>
          </Box>
        ) : null}
      </div>

      <Drawer
        clientName={clientName}
        isShowMenu={isShowMenu}
        onClose={() => {
          setIsShowMenu(false);
        }}
        onClick={() => {
          setIsShowMenu(false);
        }}
        onList={() => {
          setValue(0);
        }}
        onGenre={() => {
          setValue(1);
        }}
        onChamada={() => {
          setValue(2);
        }}
        onShowContent={() => {
          setValue(1);
        }}
      />

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={showModalConfigMusic}
        onClose={() => {
          setShowModalConfigMusic(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-end",
        }}
      >
        <Slide
          direction="up"
          in={showModalConfigMusic}
          mountOnEnter
          unmountOnExit
        >
          <Card
            style={{
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              borderBottomLeftRadius: 0,
              borderBottomRightRadius: 0,
            }}
            className="w-100"
          >
            <p
              style={{ fontWeight: "bold", color: "black", marginBottom: 20 }}
              className="text-center mt-4 mr-4 ml-4 mb-2"
            >
              {musicModify && musicModify.title}
            </p>
            <Divider />
            <Button
              startIcon={
                <BlockIcon style={{ color: "#e67e22", marginLeft: 20 }} />
              }
              onClick={() => {
                const arrayBlock = listBlocked.filter((b) =>
                  String(musicModify.artist).toLowerCase().indexOf("vinheta") >
                  -1
                    ? b.vignetteId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("comercial") > -1
                    ? b.commercialId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("conteúdo") > -1
                    ? b.contentId == musicModify.id
                    : b.musicId == musicModify.id
                );
                if (arrayBlock.length > 0) {
                  onDesBlock(arrayBlock[0].id);
                } else {
                  onBlock(musicModify.id, musicModify.artist);
                }
              }}
              variant="text"
              fullWidth
              className="mt-4"
            >
              <Typography
                className="mt-2 mb-2"
                style={{
                  textAlign: "start",
                  width: "100%",
                  textTransform: "none",
                  color: "black",
                }}
              >
                {musicModify &&
                listBlocked.filter((b) =>
                  String(musicModify.artist).toLowerCase().indexOf("vinheta") >
                  -1
                    ? b.vignetteId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("comercial") > -1
                    ? b.commercialId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("conteúdo") > -1
                    ? b.contentId == musicModify.id
                    : b.musicId == musicModify.id
                ).length > 0
                  ? "Desbloquear"
                  : "Bloquear"}
              </Typography>
            </Button>
            <Button
              startIcon={
                <FavoriteIcon style={{ color: "#eb2f06", marginLeft: 20 }} />
              }
              onClick={() => {
                const arrayFavorited = listFavorited.filter((b) =>
                  String(musicModify.artist).toLowerCase().indexOf("vinheta") >
                  -1
                    ? b.vignetteId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("comercial") > -1
                    ? b.commercialId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("conteúdo") > -1
                    ? b.contentId == musicModify.id
                    : b.musicId == musicModify.id
                );
                if (arrayFavorited.length > 0) {
                  onDesLikeFavorite(arrayFavorited[0].id);
                } else {
                  onLikeFavorite(musicModify.id, musicModify.artist);
                }
              }}
              variant="text"
              fullWidth
              className="mb-4 mt-2"
            >
              <Typography
                className="mt-2 mb-2"
                style={{
                  textAlign: "start",
                  width: "100%",
                  textTransform: "none",
                  color: "black",
                }}
              >
                {musicModify &&
                listFavorited.filter((b) =>
                  String(musicModify.artist).toLowerCase().indexOf("vinheta") >
                  -1
                    ? b.vignetteId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("comercial") > -1
                    ? b.commercialId == musicModify.id
                    : String(musicModify.artist)
                        .toLowerCase()
                        .indexOf("conteúdo") > -1
                    ? b.contentId == musicModify.id
                    : b.musicId == musicModify.id
                ).length > 0
                  ? "Desfavoritar"
                  : "Favoritar"}
              </Typography>
            </Button>
          </Card>
        </Slide>
      </Modal>
    </div>
  );
};

export default Formulario;
