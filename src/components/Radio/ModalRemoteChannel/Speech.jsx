import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Divider,
  TextField,
  Slider,
  Grid,
  Typography,
  IconButton,
  Box,
  Button,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  InputAdornment
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  PlayArrow as PlayArrowIcon,
  Stop as StopIcon,
  Delete as DeleteIcon,
  Hearing as HearingIcon,
  Save as SaveIcon
} from "@material-ui/icons";
import PerfectScrollbar from "react-perfect-scrollbar";
import { frases, frases2, marcas, cores } from "../ModalChamada/fraseData";
import api from "../../../services/api";
import { showMessage } from "../utils/message";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "center"
    },
    [breakpoints.up("sm")]: {
      alignItems: "center"
    },
    justifyContent: "center"
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white"
  }
}));

const Speech = ({ clientId, socket }) => {
  const classes = useStyles();
  const [audio, setAudio] = useState(new Audio(""));
  const [frase, setFrase] = useState("");
  const [frase1, setFrase1] = useState("");
  const [nameEmployees, setNameEmployees] = useState("");
  const [nameEmployeesCustom, setNameEmployeesCustom] = useState("");
  const [numberPlaca, setNumberPlaca] = useState("");
  const [nameMarca, setNameMarca] = useState("");
  const [nameCor, setNameCor] = useState("");
  const [selectTypesChamadas, setSelectTypesChamadas] = useState("");
  const [velocidade, setVelocidade] = useState(1);
  const [employeeId, setEmployeeId] = useState(0);
  const [playing, setPlaying] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [employees, setEmployees] = useState([]);
  const [typesChamadas] = useState([
    { name: "Funcionário" },
    { name: "Veiculo" }
  ]);
  // const [fala] = useState(new SpeechSynthesisUtterance());

  const handleChangeVelocidade = (event, newVelocidade) => {
    setVelocidade(newVelocidade);
  };

  useEffect(() => {
    if (clientId) {
      onGetEmployee();
    }
  }, [clientId]);

  useEffect(() => {
    if (selectTypesChamadas.length > 0) {
      play();
    }
  }, [audio]);

  const onNewFile = urlMusic => {
    audio.pause();
    audio.src = "";

    //setPlayingAuto(false);
    setAudio(new Audio(urlMusic));
  };

  const play = () => {
    setIsLoading(true);
    audio.play();
    audio.volume = 1;
    audio.onended = () => {
      stop();
    };
  };

  const stop = () => {
    setIsLoading(false);
  };

  const onGetEmployee = () => {
    setIsLoading(true);
    api
      .get(`/employee/${clientId}`)
      .then(response => response.data)
      .then(resp => {
        console.log(resp);
        setIsLoading(false);
        setEmployees(resp);
      })
      .catch(error => {
        showMessage("Error", "Erro, consulte o suporte", "danger");
        setIsLoading(false);
      });
  };

  const onAviso = () => {
    socket.emit("SendInfoCallRealTime", {
      clientId: clientId,
      call:
        selectTypesChamadas == "Funcionário"
          ? `${frase1}. ${
              nameEmployees == "Escrever Nome"
                ? String(nameEmployeesCustom).split("-")[1]
                  ? String(nameEmployeesCustom).split("-")[1]
                  : String(nameEmployeesCustom).split("-")[0]
                : String(nameEmployees).split("-")[1]
                ? String(nameEmployees).split("-")[1]
                : String(nameEmployees).split("-")[0]
            }. ${frase}`
          : `Por favor, compareça ao estacionamento dono do veículo. Marca: ${nameMarca}. Cor: ${nameCor}. Da placa: ${String(
              onOrganizePlaca(numberPlaca)
            ).toUpperCase()}`,
      exulted: false
    });
  };

  const onTestNameAudio = () => {
    if (!String(nameEmployeesCustom).split("-")[1]) {
      showMessage("Atenção", "Preencha o campo antes de ouvir", "warning");
      return;
    }

    setIsLoading(true);

    api
      .post(`/ibm/speech`, {
        text: String(nameEmployeesCustom).split("-")[1]
      })
      .then(response => response.data)
      .then(resp => {
        console.log(resp);

        onNewFile(resp.data.path_s3);

        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
        showMessage("Error", "Erro, consulte o suporte", "danger");
        setIsLoading(false);
      });
  };

  const saveNameEmployee = () => {
    setIsLoading(true);
    api
      .post("/Employee/new", {
        name: nameEmployeesCustom,
        clientId: clientId
      })
      .then(response => response.data)
      .then(resp => {
        if (resp.message == "success") {
          setNameEmployees(nameEmployeesCustom);
          setNameEmployeesCustom("");
          onGetEmployee();
          showMessage(
            "Novo Nome",
            `Cadastro realizado com sucesso.`,
            "success"
          );
          setIsLoading(false);
        } else {
          showMessage("Novo Funcionário", `Falhou.`, "error");
          setIsLoading(false);
        }
      })
      .catch(error => {
        console.log(error);
        setIsLoading(false);
      });
  };

  const onDeleteEmployee = id => {
    api
      .delete(`/Employee/${id}`)
      .then(response => response.data)
      .then(resp => {
        if (resp.message == "success") {
          setNameEmployees("");
          setNameEmployeesCustom("");
          onGetEmployee();
          showMessage(
            "Nome Deletado",
            `Cadastro deletado com sucesso.`,
            "success"
          );
        } else {
          showMessage("Deleção Funcionário", `Falhou.`, "error");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onOrganizePlaca = placa => {
    let text = "";

    for (let index = 0; index < String(placa).length; index++) {
      const element = String(placa)[index];

      text =
        text +
        (isNumber(element) ? String(element) + " " : String(element) + "...");
    }

    return text;
  };

  const isNumber = i => {
    return i >= "0" && i <= "9";
  };

  return (
    <div style={{ marginTop: 50, backgroundColor: "white" }}>
      <PerfectScrollbar>
        <div>
          <FormControl className="mb-4" fullWidth variant="outlined" required>
            <InputLabel htmlFor="age-native-required">
              Tipo de Chamada
            </InputLabel>

            <Select
              disabled={isLoading}
              native
              value={selectTypesChamadas}
              name="uf"
              onChange={event => {
                setSelectTypesChamadas(`${event.target.value}`);

                if (event.target.value == "Veiculo") {
                  setNameEmployees("");
                }
              }}
            >
              {/*Lista de UF*/}
              <option aria-label="None" value="" />
              {typesChamadas.map(i => {
                return <option value={i.name}>{i.name}</option>;
              })}
            </Select>
          </FormControl>

          {selectTypesChamadas == "Funcionário" ? (
            <FormControl className="mb-4" fullWidth variant="outlined" required>
              <InputLabel htmlFor="age-native-required">Chamada</InputLabel>

              <Select
                disabled={isLoading}
                native
                value={frase1}
                name="uf"
                onChange={event => {
                  setFrase1(`${event.target.value}`);
                }}
              >
                {/*Lista de UF*/}
                <option aria-label="None" value="" />
                {frases2.map(i => {
                  return <option value={i.frases}>{i.frases}</option>;
                })}
              </Select>
            </FormControl>
          ) : null}

          {selectTypesChamadas == "Funcionário" ? (
            <FormControl className="mb-4" fullWidth variant="outlined" required>
              <InputLabel htmlFor="age-native-required">
                Funcionários
              </InputLabel>

              <Select
                disabled={isLoading}
                value={nameEmployees}
                name="uf"
                onChange={event => {
                  setNameEmployees(`${event.target.value}`);
                }}
              >
                {/*Lista de UF*/}
                <MenuItem aria-label="None" value="" />
                <MenuItem style={{ fontWeight: "bold" }} value="Escrever Nome">
                  Escrever Nome
                </MenuItem>

                {employees.map(i => {
                  return (
                    <MenuItem key={i.name} value={i.name}>
                      <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                        style={{ width: "100%" }}
                      >
                        <div>{String(i.name).split("-")[0]}</div>

                        <IconButton
                          onClick={event => {
                            event.stopPropagation();
                            onDeleteEmployee(i.id);
                          }}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </Box>
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          ) : null}

          {nameEmployees == "Escrever Nome" ? (
            <TextField
              className="mb-4"
              fullWidth
              helperText="Favor escrever o nome como esta no Documento."
              label="Nome Original: "
              disabled={isLoading}
              name="Frase"
              onChange={event => {
                let textNow = String(nameEmployeesCustom).split("-");
                setNameEmployeesCustom(
                  `${event.target.value}-${textNow[1] ? textNow[1] : ""}`
                );
              }}
              required
              value={String(nameEmployeesCustom).split("-")[0]}
              variant="outlined"
            />
          ) : null}

          {nameEmployees == "Escrever Nome" ? (
            <TextField
              className="mb-4"
              fullWidth
              helperText="Favor escrever o nome como se pronúncia exemplo: Schneider se pronuncia Xinaider. Por se tratar de um Inteligência Artificial, terá nomes que precisaram de acentos em determinada letra para que a mesma consiga interpretar da maneira correta."
              label="Nome para IA: "
              disabled={isLoading}
              name="Frase"
              onChange={event => {
                let textNow = String(nameEmployeesCustom).split("-");
                setNameEmployeesCustom(`${textNow[0]}-${event.target.value}`);
              }}
              required
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Box display="flex" alignItems="center">
                      <IconButton
                        className="mr-2"
                        size="small"
                        disabled={isLoading}
                        variant="contained"
                        onClick={() => {
                          onTestNameAudio();
                        }}
                        style={{
                          color: "white",
                          backgroundColor: "#1e272e",
                          textTransform: "none"
                        }}
                      >
                        <HearingIcon
                          fontSize="small"
                          style={{ color: "white" }}
                        />
                      </IconButton>
                      <IconButton
                        className="ml-2"
                        size="small"
                        disabled={isLoading}
                        variant="contained"
                        onClick={() => {
                          saveNameEmployee();
                        }}
                        style={{
                          color: "white",
                          backgroundColor: "#2ecc71",
                          textTransform: "none"
                        }}
                      >
                        <SaveIcon fontSize="small" style={{ color: "white" }} />
                      </IconButton>
                    </Box>
                  </InputAdornment>
                )
              }}
              value={String(nameEmployeesCustom).split("-")[1]}
              variant="outlined"
            />
          ) : null}

          {selectTypesChamadas == "Veiculo" ? (
            <FormControl className="mb-4" fullWidth variant="outlined" required>
              <InputLabel htmlFor="age-native-required">Marca</InputLabel>

              <Select
                disabled={isLoading}
                native
                value={nameMarca}
                name="uf"
                onChange={event => {
                  setNameMarca(`${event.target.value}`);
                }}
              >
                {/*Lista de marcas*/}
                <option aria-label="None" value="" />
                {marcas.map(i => {
                  return <option value={i.marca}>{i.marca}</option>;
                })}
              </Select>
            </FormControl>
          ) : null}

          {selectTypesChamadas == "Veiculo" ? (
            <FormControl className="mb-4" fullWidth variant="outlined" required>
              <InputLabel htmlFor="age-native-required">Cor</InputLabel>

              <Select
                disabled={isLoading}
                native
                value={nameCor}
                name="uf"
                onChange={event => {
                  setNameCor(`${event.target.value}`);
                }}
              >
                {/*Lista de marcas*/}
                <option aria-label="None" value="" />
                {cores.map(i => {
                  return <option value={i.cor}>{i.cor}</option>;
                })}
              </Select>
            </FormControl>
          ) : null}

          {selectTypesChamadas == "Veiculo" ? null : (
            <FormControl className="mb-4" fullWidth variant="outlined" required>
              <InputLabel htmlFor="age-native-required">Ação</InputLabel>

              <Select
                disabled={isLoading}
                native
                value={frase}
                name="uf"
                onChange={event => {
                  setFrase(`${event.target.value}`);
                }}
              >
                {/*Lista de UF*/}
                <option aria-label="None" value="" />
                {frases.map(i => {
                  return (selectTypesChamadas == "Veiculo") &
                    (String(i.frases).indexOf("placa") > -1) ? (
                    <option value={i.frases}>{i.frases}</option>
                  ) : (String(i.frases).indexOf("placa") == -1) &
                    (selectTypesChamadas ==
                      "Veiculo") ? null : (selectTypesChamadas ==
                      "Funcionário") &
                    (String(i.frases).indexOf("placa") > -1) &
                    (selectTypesChamadas != "Veiculo") ? null : (
                    <option value={i.frases}>{i.frases}</option>
                  );
                })}
              </Select>
            </FormControl>
          )}

          {selectTypesChamadas == "Veiculo" ? (
            <TextField
              className="mb-4"
              fullWidth
              label="Digite sua PLACA: "
              disabled={isLoading}
              name="Frase"
              onChange={event => {
                setNumberPlaca(event.target.value);
              }}
              required
              value={numberPlaca}
              variant="outlined"
            />
          ) : null}
        </div>
      </PerfectScrollbar>
      <div
        style={{
          backgroundColor: "white",
          boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
          WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
          MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)"
        }}
        className="fixed-bottom w-100"
      >
        <Divider />

        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          className="w-100"
          p={2}
        >
          <Button
            onClick={() => {
              onAviso();
            }}
            style={{ color: "white", backgroundColor: "red" }}
            variant="contained"
          >
            Executar aviso
          </Button>
        </Box>
      </div>
    </div>
  );
};

export default Speech;
