import React from "react";
import { Box, Grid, Typography, Avatar } from "@material-ui/core";
import { PlayCircleFilled as PlayCircleFilledIcon } from "@material-ui/icons";
import "../App.scss";

const GeneroCard = ({ generosList, onGeneroSelect, ThemeColorChannel }) => {
  return (
    <div
      onClick={() => {
        onGeneroSelect(generosList);
      }}
      style={{ cursor: "pointer" }}
      className="mb-4 mr-2"
    >
      <div className="ct">
      <Avatar
          style={{
            borderRadius: 5,
            height: 130,
            width: 130,
          }}
          src={generosList.Covers[0].path_s3}
        />
        <Box display="flex" justifyContent="flex-end">
          <PlayCircleFilledIcon style={{ fontSize: 50 }} className="gn-play" />
        </Box>
      </div>

      <h5
        style={{ color: ThemeColorChannel == "white" ? "black" : "white" }}
        className="text-black mt-2"
      >
        {generosList.name}
      </h5>
      <h6
        style={{ color: ThemeColorChannel == "white" ? "black" : "white" }}
        className="text-black"
      >
        Álbum • {}
      </h6>
    </div>
  );
};

export default GeneroCard;
