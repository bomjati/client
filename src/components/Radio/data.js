import { v4 as uuid } from 'uuid';
export const data = [
    {
        id: uuid(),
        capa: 'https://www.sertanejooficial.com.br/wp-content/uploads/2019/10/Capa-CD.jpg',
        genero: 'Sertanejo',
        type: 'Álbum •',
        list: [
            {
                link1: 'https://bomjafiles.s3-sa-east-1.amazonaws.com/sounds/acervo-01/reggaeton/201020145248-China++-+Anuel+aa+daddy+yankee+karol+g+ozuna+j+balvin+-+reggaeton+-.mp3',
                autor: 'Anuel aa daddy yankee karol g ozuna j balvin',
                musica: 'China',
                tempo: '3:57'
            },
            {
                link1: 'https://bomjafiles.s3-sa-east-1.amazonaws.com/sounds/acervo-01/Eletr%C3%B4nica/201020145254-More+tha+you+know+-+Axwell+l+ingrosso+-+Eletr%C3%B4nica+-.mp3',
                autor: 'Axwell l ingrosso',
                musica: 'More tha you know',
                tempo: '3:57'
            },
            {
                link1: 'https://bomjafiles.s3-sa-east-1.amazonaws.com/sounds/acervo-01/Eletr%C3%B4nica/201020145257-Secrets+-+Tiesto+kshmr+feat+vassy++-+Eletr%C3%B4nica+-.mp3',
                autor: 'Tiesto kshmr feat vassy',
                musica: 'Secrets',
                tempo: '3:57'
            }
        ]
    },
    {
        id: uuid(),
        capa: 'https://s.glbimg.com/jo/g1/f/original/blog/017f1e83-d80c-480e-9095-b5613c8688f4_detonautasroqueclubeVIcapa.png',
        genero: 'Pop Rock',
        type: 'Álbum •',
        list: [
            {
                link1: 'https://bomjafiles.s3-sa-east-1.amazonaws.com/sounds/acervo-01/Pop/201020145251-Mad+love++-+Sean+Puol+-+Pop+-.mp3',
                autor: 'Sean Puol',
                musica: 'Mad love',
                tempo: '3:57'
            },
        ]
    },
]