import React, { useState, useEffect } from "react";
import {
  Modal,
  Card,
  CardHeader,
  CardContent,
  Backdrop,
  Fade,
  Grid,
  IconButton,
  Box,
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Checkbox,
  FormControlLabel,
  Avatar,
  TextField,
  Button,
  TablePagination,
  CircularProgress,
  ClickAwayListener,
  Paper,
  InputAdornment
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Close as CloseIcon,
  Delete as DeleteIcon,
  ExpandMore as ExpandMoreIcon,
  PlayArrow as PlayArrowIcon,
  CheckCircle as CheckCircleIcon,
  AddCircle as AddCircleIcon,
  Search as SearchIcon,
  ExpandLess as ExpandLessIcon
} from "@material-ui/icons";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import api from "../../../services/api";
import { showMessage } from "../utils/message";
import MessageConfirm from "../utils/MessageConfirm";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start"
    },
    [breakpoints.up("sm")]: {
      alignItems: "center"
    },
    justifyContent: "center"
  },
  buttom: {
    "&:hover, &.Mui-focusVisible": { backgroundColor: "#6c5ce7" },
    backgroundColor: "#6c5ce7",
    color: "white"
  },
  MuiAccordionroot: {
    "&.MuiAccordion-root:before": {
      backgroundColor: "transparent"
    }
  },
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    backgroundColor: "#303030",
    width: 550
  }
}));

const ModalEditPlayList = ({
  open,
  onCloseEditPlayList,
  onGetPlayList,
  heapLists,
  editPlayLists,
  isMobile,
  listMusicCompleteArray
}) => {
  const classes = useStyles();
  const [musicSelect, setMusicSelect] = useState([]);
  const [editPlayList, setEditPlayList] = useState(null);
  const [heapList, setHeapList] = useState([]);
  const [openSelect, setOpenSelect] = useState([]);
  const [listMusicComplete, setListMusicComplete] = useState([]);
  const [listMusicCompleteBusca, setListMusicCompleteBusca] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [showListMusic, setShowListMusic] = useState(false);
  const [namePlayList, setNamePlayList] = useState("");
  const [musicPlayListId, setMusicPlayListId] = useState("");
  const [listViewGenere, setListViewGenere] = useState("");
  const [limitMusic, setLimitMusic] = useState(5);
  const [pageMusic, setPageMusic] = useState(0);
  const [limitMusicI, setLimitMusicI] = useState(5);
  const [pageMusicI, setPageMusicI] = useState(0);
  const { height, width } = useWindowDimensions();
  const [searchs, setSearchs] = useState({});
  const [isSearchs, setIsSearchs] = useState({});
  const [isViewsList, setIsViewsList] = useState({});
  const [isBackDrop, setIsBackDrop] = useState(false);

  const backgroundColor = "#ecf0f1";
  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)"
  };

  useEffect(() => {
    setHeapList(heapLists);
  }, [heapLists]);

  useEffect(() => {
    setListMusicComplete(listMusicCompleteArray);
    setListMusicCompleteBusca(listMusicCompleteArray);
  }, [listMusicCompleteArray]);

  const handleLimitChangeMusic = event => {
    setLimitMusic(event.target.value);
  };

  const handlePageChangeMusic = (event, newPage) => {
    setPageMusic(newPage);
  };

  const handleLimitChangeMusicI = event => {
    setLimitMusicI(event.target.value);
  };

  const handlePageChangeMusicI = (event, newPage) => {
    setPageMusicI(newPage);
  };



  useEffect(() => {
    editPlayLists && setNamePlayList(editPlayLists.name);

    if (editPlayLists) {
      setIsBackDrop(true);
      api
        .get(`/itemplaylist/playlist/${editPlayLists.id}`)
        .then(respP => {
          if (respP.data) {
            setIsBackDrop(false);
            if (respP.data.length > 0) {
              const value = { ...editPlayLists, Itemplaylists: respP.data };
              console.log(value);
              setEditPlayList(value);
              let dadosMusics = {};

              for (let index = 0; index < value.Itemplaylists.length; index++) {
                const music = value.Itemplaylists[index];

                dadosMusics = {
                  ...dadosMusics,
                  [music.musicId]: {
                    status: true,
                    id: music.musicId
                  }
                };
              }

              let genereVisible = {};

              for (let index = 0; index < heapList[0].Rights.length; index++) {
                const elementR = heapList[0].Rights[index];

                for (
                  let indexG = 0;
                  indexG < elementR.Genres.length;
                  indexG++
                ) {
                  const element = elementR.Genres[indexG];

                  genereVisible = {
                    ...genereVisible,
                    [element.id]: {
                      id: element.id,
                      status: true //onValidateGeneroMusic(element.Musics, dadosMusics),
                    }
                  };
                }
              }

              setListViewGenere(genereVisible);
              setMusicSelect(dadosMusics);
              setIsBackDrop(false);
            }
          }
        })
        .catch(error => {
          console.log(error);
        });
    }

    console.log(editPlayLists);
  }, [editPlayLists]);

  const onSaveName = () => {
    if (namePlayList == "") {
      showMessage(
        "Editar Playlist",
        "Preencha o nome de sua Playlist para prosseguir.",
        "warning"
      );
      return;
    }

    if (editPlayList.Itemplaylists.length == 0) {
      showMessage(
        "Editar Playlist",
        "Nenhuma musica selecionada, selecione para proceguir",
        "warning"
      );
      return;
    }

    const dados = {
      name: namePlayList
    };

    api
      .put(`/playlist/edit/${editPlayList.id}`, dados)
      .then(response => response.data)
      .then(resp => {
        if (resp.message == "success") {
          showMessage(
            "Alteração Playlist",
            "Nome da Playlist alterada com sucesso.",
            "success"
          );
          onGetPlayList();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onDeleteMusicList = () => {
    api
      .delete(`/itemplaylist/${musicPlayListId}`)
      .then(response => response.data)
      .then(resp => {
        console.log(resp);
        if (resp.message == "success") {
          showMessage(
            "Exclusão Musica",
            "Musica selecionada excluida com sucesso.",
            "success"
          );
          onGetPlayList();
          onCloseMessageConfirm();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onAddMusicListEdit = () => {
    setLimitMusic(
      listMusicComplete.length > limitMusic ? limitMusic + 6 : limitMusic
    );
  };

  const onAddMusicList = id => {
    const dados = {
      music: [id]
    };
    console.log(dados);

    api
      .post(`/itemplaylist/new/${editPlayLists.id}`, dados)
      .then(response => response.data)
      .then(resp => {
        console.log(resp);
        if (resp.message == "success") {
          showMessage("Musica", "Musica adicionada com sucesso.", "success");
          onGetPlayList();
          onCloseMessageConfirm();
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onGetMusicGenre = (genero, isSelect) => {
    if (!genero.Musics) {
      setIsBackDrop(true);
      api
        .get(`/music/genre/${genero.id}`)
        .then(respM => {
          if (respM.data) {
            setHeapList(
              heapList.map(h => {
                return {
                  ...h,
                  Rights: h.Rights.map(r => {
                    return {
                      ...r,
                      Genres: r.Genres.map(g => {
                        if (isSelect) {
                          if (g.id == genero.id) {
                            onSelectAll({
                              ...g,
                              Musics: respM.data
                            });
                          }
                        } else {
                          setIsBackDrop(false);
                        }
                        return g.id == genero.id
                          ? {
                              ...g,
                              Musics: respM.data
                            }
                          : g;
                      })
                    };
                  })
                };
              })
            );
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const onGetMusicGenreSubs = (genero, isSelect) => {
    if (!genero.Musics) {
      setIsBackDrop(true);
      api
        .get(`/genresubitem/genresub/${genero.id}`)
        .then(respM => {
          if (respM.data) {
            setHeapList(
              heapList.map(h => {
                return {
                  ...h,
                  Rights: h.Rights.map(r => {
                    return {
                      ...r,
                      Genresubs: r.Genresubs.map(g => {
                        if (isSelect) {
                          if (g.id == genero.id) {
                            onSelectAllSub({
                              ...g,
                              Musics: respM.data
                            });
                          }
                        }
                        return g.id == genero.id
                          ? {
                              ...g,
                              Musics: respM.data
                            }
                          : g;
                      })
                    };
                  })
                };
              })
            );

            setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const onSelectAll = async genero => {
    let ids = [];

    const item =
      numKeys(editPlayList) > 6
        ? editPlayList.Itemplaylists.filter(i => i.Music.genreId == genero.id)
        : [];

    if (item.length == genero.countMusic) {
      setIsBackDrop(true);
      let idsItem = [];

      for (let index = 0; index < item.length; index++) {
        const element = item[index];

        idsItem.push(element.id);
      }

      api
        .post(`/itemplaylist/delete/lote`, {data: idsItem})
        .then(response => response.data)
        .then(resp => {
          console.log(resp);
          if (resp.message == "success") {
            showMessage(
              "Gênero",
              "Gênero deselecionado com sucesso.",
              "success"
            );
            onGetPlayList();
            onCloseMessageConfirm();
            //setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      setIsBackDrop(true);
      for (let index = 0; index < genero.Musics.length; index++) {
        const element = genero.Musics[index];

        ids.push(element.id);
      }

      const dados = {
        music: ids
      };

      api
        .post(`/itemplaylist/new/${editPlayLists.id}`, dados)
        .then(response => response.data)
        .then(resp => {
          console.log(resp);
          if (resp.message == "success") {
            showMessage("Música", "Gênero selecionado com sucesso.", "success");
            onGetPlayList();
            onCloseMessageConfirm();
           // setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const onSelectAllSub = genero => {
    let ids = [];

    const item =
      numKeys(editPlayList) > 6
        ? editPlayList.Itemplaylists.filter(i => i.Music.genreId == genero.id)
        : [];

    if (item.length == genero.countMusic) {
      setIsBackDrop(true);
      let idsItem = [];

      for (let index = 0; index < item.length; index++) {
        const element = item[index];

        idsItem.push(element.id);
      }

      api
        .post(`/itemplaylist/delete/lote`, {data: idsItem})
        .then(response => response.data)
        .then(resp => {
          console.log(resp);
          if (resp.message == "success") {
            showMessage(
              "Gênero",
              "Gênero deselecionado com sucesso.",
              "success"
            );
            onGetPlayList();
            onCloseMessageConfirm();
            //setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      setIsBackDrop(true);
      for (let index = 0; index < genero.Musics.length; index++) {
        const element = genero.Musics[index];

        ids.push(element.id);
      }

      const dados = {
        music: ids
      };

      api
        .post(`/itemplaylist/new/${editPlayLists.id}`, dados)
        .then(response => response.data)
        .then(resp => {
          console.log(resp);
          if (resp.message == "success") {
            showMessage("Música", "Gênero selecionado com sucesso.", "success");
            onGetPlayList();
            onCloseMessageConfirm();
           // setIsBackDrop(false);
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    onDeleteMusicList();
  };

  const getCapaGeneros = id => {
    for (let index = 0; index < heapList[0].Rights.length; index++) {
      const elementR = heapList[0].Rights[index];

      for (let indexG = 0; indexG < elementR.Genres.length; indexG++) {
        const element = elementR.Genres[indexG];

        if (element.id == id) {
          return element.Covers[0].path_s3;
        }
      }
    }
  };

  const seachMusic = (value, Genres, isGenreSub) => {
    let data = listMusicCompleteBusca.filter(
      m =>
        String(m.artist)
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "")
          .toUpperCase()
          .indexOf(
            String(value)
              .normalize("NFD")
              .replace(/[\u0300-\u036f]/g, "")
              .toUpperCase()
          ) > -1
    );

    if (data.length == 0) {
      data = listMusicCompleteBusca.filter(
        m =>
          String(m.title)
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .toUpperCase()
            .indexOf(
              String(value)
                .normalize("NFD")
                .replace(/[\u0300-\u036f]/g, "")
                .toUpperCase()
            ) > -1
      );

      setListMusicComplete(data);
    } else {
      setListMusicComplete(data);
    }
  };

  const onValidateGeneroMusic = (musicList, dadosMusics) => {
    for (let index = 0; index < musicList.length; index++) {
      const element = musicList[index];

      if (!dadosMusics[String(element.id)]) {
        return true;
      }
    }

    return false;
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseEditPlayList}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4 mt-2">
              <Box
                display="flex"
                justifyContent="flex-end"
                className="w-90 mt-2 mb-2 mr-4"
                p={0}
              >
                <IconButton
                  style={{ backgroundColor: "white" }}
                  onClick={onCloseEditPlayList}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
              <Card
                className="ml-4 mr-4"
                style={{
                  borderRadius: 15,
                  width: isMobile == "true" ? "96%" : 720,
                  height: isMobile == "true" ? (height * 85) / 100 : 600
                }}
              >
                <CardHeader
                  subheader="Selecione suas música favoritas"
                  title="Modificando Playlist"
                />

                <Divider />
                <PerfectScrollbar>
                  <CardContent className="p-4">
                    <Card
                      style={{ borderRadius: 20 }}
                      className={`w-100 p-4 ${showListMusic ? "mb-4" : "mb-2"}`}
                    >
                      <h5 className="text-black mb-2">Nome Playlist</h5>
                      <div className="d-flex justify-content-center w-100">
                        <TextField
                          fullWidth
                          label=""
                          name="name_playlist"
                          onChange={event => {
                            setNamePlayList(event.target.value);
                          }}
                          required
                          value={namePlayList}
                          variant="outlined"
                          inputProps={{
                            style: {
                              padding: 10,
                              borderRadius: 50
                            }
                          }}
                        />
                        <Button
                          onClick={() => {
                            onSaveName();
                          }}
                          style={{
                            backgroundColor: "#27ae60",
                            color: "white",
                            borderRadius: 20
                          }}
                          className="ml-2"
                          variant="contained"
                        >
                          Salvar
                        </Button>
                      </div>
                    </Card>

                    {showListMusic ? null : (
                      <Card
                        style={{ borderRadius: 20 }}
                        className="pl-4 pr-4 pt-2 pb-2 mb-4 d-flex justify-content-between align-items-center"
                      >
                        <Button
                          onClick={() => {
                            setShowListMusic(!showListMusic);
                          }}
                          style={{
                            backgroundColor: "red",
                            color: "white",
                            borderRadius: 20
                          }}
                          className="ml-2"
                          variant="contained"
                        >
                          Mostrar
                        </Button>

                        <h6 className="text-black">Lista de Musicas</h6>
                      </Card>
                    )}

                    {!showListMusic ? null : (
                      <Card className="p-4" style={{ borderRadius: 20 }}>
                        <div className="mb-4 d-flex justify-content-between align-items-center">
                          <h6 className="text-black">Lista de Musicas</h6>

                          <Button
                            onClick={() => {
                              setShowListMusic(!showListMusic);
                            }}
                            style={{
                              backgroundColor: "red",
                              color: "white",
                              borderRadius: 20
                            }}
                            className="ml-2"
                            variant="contained"
                          >
                            Ocultar
                          </Button>
                        </div>

                        <div>
                          <Grid container spacing={1}>
                            {editPlayList &&
                              editPlayList.Itemplaylists.slice(
                                pageMusicI * limitMusicI,
                                pageMusicI * limitMusicI + limitMusicI
                              ).map((music, index) => {
                                return (
                                  <Grid item lg={12} md={12} xs={12}>
                                    <Card
                                      style={{
                                        backgroundColor: "white",
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                        display: "flex"
                                      }}
                                      className="p-1 w-100"
                                    >
                                      <Box
                                        className="p-1 w-100"
                                        display="flex"
                                        alignItems="center"
                                        justifyContent="flex-start"
                                      >
                                        <IconButton
                                          className="mr-1"
                                          onClick={() => {
                                            setMusicPlayListId(music.id);
                                            setShowConfirm(true);
                                          }}
                                        >
                                          <DeleteIcon
                                            style={{ color: "#e74c3c" }}
                                          />
                                        </IconButton>
                                        <div onClick={() => {}} className="ct">
                                          <Avatar
                                            alt="Product"
                                            variant="square"
                                            src={getCapaGeneros(
                                              music.Music && music.Music.genreId
                                            )}
                                            className="av-radio mr-2"
                                          />
                                        </div>
                                        <div>
                                          <h5 className="text-black">
                                            {String(music.Music.title).replace(
                                              ".mp3",
                                              ""
                                            )}
                                          </h5>
                                          <Typography
                                            style={{ color: "black" }}
                                            variant="body1"
                                          >
                                            {music.Music.artist}
                                          </Typography>
                                        </div>
                                      </Box>
                                      <Typography
                                        style={{ color: "black" }}
                                        variant="body1"
                                      >
                                        {music.Music.totalTime}
                                      </Typography>
                                    </Card>
                                  </Grid>
                                );
                              })}
                          </Grid>
                          <TablePagination
                            component="div"
                            style={{
                              marginTop: 20
                            }}
                            onClick={event => event.stopPropagation()}
                            count={
                              editPlayList && editPlayList.Itemplaylists
                                ? editPlayList.Itemplaylists.length
                                : 0
                            }
                            onChangePage={handlePageChangeMusicI}
                            onChangeRowsPerPage={handleLimitChangeMusicI}
                            labelRowsPerPage="Linhas por página:"
                            page={pageMusicI}
                            rowsPerPage={limitMusicI}
                            rowsPerPageOptions={[2, 5, 10, 25]}
                          />
                        </div>
                      </Card>
                    )}

                    {heapList.length > 0 &&
                      heapList[0].Rights.map(Right => (
                        <div
                          className="mb-2 mt-4"
                          style={
                            isViewsList[String(Right.id)]
                              ? {}
                              : {
                                  ...styleButtonIcon,
                                  padding: 15,
                                  borderRadius: 15
                                }
                          }
                        >
                          {!isSearchs[String(Right.id)] ? (
                            <Box
                              mb={isViewsList[String(Right.id)] ? 4 : 0}
                              display="flex"
                              justifyContent="space-between"
                              alignItems="center"
                            >
                              <Box
                                onClick={() => {
                                  setIsViewsList({
                                    ...isViewsList,
                                    [String(Right.id)]: isViewsList[
                                      String(Right.id)
                                    ]
                                      ? !isViewsList[String(Right.id)]
                                      : true
                                  });
                                }}
                                display="flex"
                                alignItems="center"
                                style={{ cursor: "pointer" }}
                              >
                                <h5 className="text-black mr-4">
                                  {Right.name}
                                </h5>
                                {isViewsList[String(Right.id)] ? (
                                  <ExpandLessIcon />
                                ) : (
                                  <ExpandMoreIcon />
                                )}
                              </Box>

                              <Button
                                onClick={() => {
                                  setIsSearchs({
                                    ...isSearchs,
                                    [String(Right.id)]: isSearchs[
                                      String(Right.id)
                                    ]
                                      ? !isSearchs[String(Right.id)]
                                      : true
                                  });
                                }}
                                size="medium"
                                variant="contained"
                                style={{
                                  backgroundColor: "white",
                                  color: "red"
                                }}
                                startIcon={<SearchIcon />}
                              >
                                Buscar Musicas
                              </Button>
                            </Box>
                          ) : (
                            <div>
                              <ClickAwayListener
                                onClickAway={() => {
                                  setSearchs({
                                    ...searchs,
                                    [String(Right.id)]: searchs[
                                      String(Right.id)
                                    ]
                                      ? ""
                                      : ""
                                  });
                                  setIsSearchs({
                                    ...isSearchs,
                                    [String(Right.id)]: isSearchs[
                                      String(Right.id)
                                    ]
                                      ? !isSearchs[String(Right.id)]
                                      : false
                                  });
                                }}
                              >
                                <div>
                                  <TextField
                                    fullWidth
                                    placeholder={`Buscar musicas ${Right.name}`}
                                    name="search"
                                    onChange={event => {
                                      setSearchs({
                                        ...searchs,
                                        [String(Right.id)]: event.target.value
                                      });

                                      seachMusic(
                                        event.target.value,
                                        Right.Genres,
                                        false
                                      );
                                    }}
                                    required
                                    value={
                                      searchs[String(Right.id)]
                                        ? searchs[String(Right.id)]
                                        : ""
                                    }
                                    variant="outlined"
                                    InputProps={{
                                      style: {
                                        ...styleButtonIcon,
                                        borderRadius: 25,
                                        marginTop: 15,
                                        marginBottom: 15
                                      },
                                      startAdornment: (
                                        <InputAdornment position="start">
                                          <Box
                                            display="flex"
                                            alignItems="center"
                                          >
                                            <h5 className="text-black ml-4">
                                              {Right.name}
                                            </h5>
                                            <SearchIcon
                                              style={{
                                                marginLeft: 20,
                                                marginRight: 20
                                              }}
                                            />
                                          </Box>
                                        </InputAdornment>
                                      )
                                    }}
                                  />
                                  {!(searchs[String(Right.id)]
                                    ? String(searchs[String(Right.id)]).length >
                                      0
                                    : false) ? null : (
                                    <Paper
                                      style={{
                                        marginTop: 10,
                                        position: "absolute",
                                        height: 300,
                                        zIndex: 9999999
                                      }}
                                      component="form"
                                      className={classes.root}
                                    >
                                      <div style={{ height: 300 }}>
                                        <PerfectScrollbar
                                          onYReachEnd={() => {
                                            onAddMusicListEdit();
                                          }}
                                        >
                                          {listMusicComplete
                                            .slice(
                                              pageMusic * limitMusic,
                                              pageMusic * limitMusic +
                                                limitMusic
                                            )
                                            .map((music, index) => (
                                              <div
                                                key={index}
                                                className="w-100 ct"
                                              >
                                                <Grid
                                                  item
                                                  lg={12}
                                                  md={12}
                                                  xs={12}
                                                >
                                                  <Box
                                                    onClick={() => {
                                                      if (
                                                        !musicSelect[
                                                          String(music.id)
                                                        ]
                                                      ) {
                                                        onAddMusicList(
                                                          music.id
                                                        );
                                                      } else if (
                                                        !musicSelect[
                                                          String(music.id)
                                                        ].status
                                                      ) {
                                                        onAddMusicList(
                                                          music.id
                                                        );
                                                      } else {
                                                        setShowConfirm(true);
                                                        setMusicPlayListId(
                                                          editPlayList.Itemplaylists.filter(
                                                            it =>
                                                              it.Music.id ==
                                                              music.id
                                                          )[0].id
                                                        );
                                                      }
                                                    }}
                                                    style={{
                                                      backgroundColor:
                                                        "transparent"
                                                    }}
                                                    className="p-1"
                                                    display="flex"
                                                    alignItems="center"
                                                    justifyContent="space-between"
                                                  >
                                                    <Box
                                                      display="flex"
                                                      alignItems="center"
                                                    >
                                                      {!musicSelect[
                                                        String(music.id)
                                                      ] ? null : !musicSelect[
                                                          String(music.id)
                                                        ].status ? null : (
                                                        <CheckCircleIcon
                                                          style={{
                                                            color: "#0be881"
                                                          }}
                                                        />
                                                      )}
                                                      <Box
                                                        className="p-1"
                                                        display="flex"
                                                        alignItems="center"
                                                        justifyContent="flex-start"
                                                      >
                                                        <div>
                                                          <div className="d-flex">
                                                            <h5 className="text-white mr-4">
                                                              {String(
                                                                music.title
                                                              ).replace(
                                                                ".mp3",
                                                                ""
                                                              )}
                                                            </h5>
                                                          </div>
                                                          <Typography
                                                            style={{
                                                              color: "white"
                                                            }}
                                                            variant="body1"
                                                          >
                                                            {music.artist}
                                                          </Typography>
                                                        </div>
                                                      </Box>
                                                    </Box>

                                                    <Typography
                                                      style={{ color: "white" }}
                                                      variant="body1"
                                                    >
                                                      {music.totalTime}
                                                    </Typography>
                                                  </Box>
                                                </Grid>
                                                <div
                                                  className="w-100"
                                                  style={{
                                                    background:
                                                      "rgba(255,255,255,0.2)",
                                                    height: 0.09
                                                  }}
                                                />
                                              </div>
                                            ))}
                                        </PerfectScrollbar>
                                      </div>
                                    </Paper>
                                  )}
                                </div>
                              </ClickAwayListener>
                            </div>
                          )}

                          {isViewsList[String(Right.id)] &&
                            Right.Genres.map(
                              genero =>
                                (listViewGenere[String(genero.id)]
                                  ? listViewGenere[String(genero.id)].status
                                  : true) && (
                                  <Accordion
                                    classes={{
                                      root: classes.MuiAccordionroot
                                    }}
                                    onClick={() => {
                                      let arrayIdSelect =
                                        openSelect.filter(is => is == genero.id)
                                          .length > 0
                                          ? openSelect.filter(
                                              is => is != genero.id
                                            )
                                          : openSelect.concat([genero.id]);

                                      console.log(arrayIdSelect);
                                      setOpenSelect(arrayIdSelect);

                                      if (!genero.Musics) {
                                        onGetMusicGenre(genero, false);
                                      }
                                    }}
                                    style={{ borderRadius: 20, marginTop: 10 }}
                                  >
                                    <AccordionSummary
                                      expandIcon={<ExpandMoreIcon />}
                                      aria-label="Expand"
                                      aria-controls="additional-actions1-content"
                                      id="additional-actions1-header"
                                    >
                                      <FormControlLabel
                                        aria-label="Acknowledge"
                                        onClick={event =>
                                          event.stopPropagation()
                                        }
                                        onFocus={event =>
                                          event.stopPropagation()
                                        }
                                        control={
                                          <Box display="flex" marginRight={2}>
                                            <Checkbox
                                              checked={
                                                editPlayList &&
                                                numKeys(editPlayList) > 6
                                                  ? editPlayList.Itemplaylists.filter(
                                                      i =>
                                                        i.Music.genreId ==
                                                        genero.id
                                                    ).length ==
                                                    genero.countMusic
                                                    ? true
                                                    : false
                                                  : false
                                              }
                                              onChange={() => {
                                                if (!genero.Musics) {
                                                  onGetMusicGenre(genero, true);
                                                } else {
                                                  onSelectAll(genero);
                                                }
                                              }}
                                            />
                                            <Avatar
                                              src={genero.Covers[0].path_s3}
                                            />
                                          </Box>
                                        }
                                        label={genero.name}
                                      />
                                    </AccordionSummary>
                                    <AccordionDetails className="w-100">
                                      <div>
                                        <Grid container spacing={1}>
                                          {openSelect.filter(
                                            is => is == genero.id
                                          ).length > 0
                                            ? genero.Musics &&
                                              genero.Musics.slice(
                                                pageMusic * limitMusic,
                                                pageMusic * limitMusic +
                                                  limitMusic
                                              ).map((music, index) => {
                                                return musicSelect[
                                                  String(music.id)
                                                ] ? null : (
                                                  <div
                                                    style={{
                                                      cursor: "pointer"
                                                    }}
                                                    className="w-100"
                                                  >
                                                    <Grid
                                                      item
                                                      lg={12}
                                                      md={12}
                                                      xs={12}
                                                    >
                                                      <div
                                                        style={{
                                                          backgroundColor:
                                                            index == -1
                                                              ? "rgba(0,0,0,0.1)"
                                                              : "transparent",
                                                          justifyContent:
                                                            "space-between",
                                                          alignItems: "center",
                                                          display: "flex"
                                                        }}
                                                        className="p-1 w-100"
                                                      >
                                                        <Box
                                                          className="p-1 w-100"
                                                          display="flex"
                                                          alignItems="center"
                                                          justifyContent="flex-start"
                                                        >
                                                          <IconButton
                                                            className="mr-1"
                                                            onClick={() => {
                                                              onAddMusicList(
                                                                music.id
                                                              );
                                                            }}
                                                          >
                                                            <AddCircleIcon
                                                              style={{
                                                                color: "#f1c40f"
                                                              }}
                                                            />
                                                          </IconButton>

                                                          <div
                                                            onClick={() => {}}
                                                            className="ct"
                                                          >
                                                            <Avatar
                                                              alt="Product"
                                                              variant="square"
                                                              src={
                                                                genero.Covers[0]
                                                                  .path_s3
                                                              }
                                                              className="av-radio mr-2"
                                                            />
                                                            <PlayArrowIcon className="av-play" />
                                                          </div>
                                                          <div>
                                                            <h5 className="text-black">
                                                              {String(
                                                                music.title
                                                              ).replace(
                                                                ".mp3",
                                                                ""
                                                              )}
                                                            </h5>
                                                            <Typography
                                                              style={{
                                                                color: "black"
                                                              }}
                                                              variant="body1"
                                                            >
                                                              {music.artist}
                                                            </Typography>
                                                          </div>
                                                        </Box>
                                                        <Typography
                                                          style={{
                                                            color: "black"
                                                          }}
                                                          variant="body1"
                                                        >
                                                          {music.totalTime}
                                                        </Typography>
                                                      </div>
                                                    </Grid>
                                                    <div
                                                      className="w-100"
                                                      style={{
                                                        background:
                                                          "rgba(0,0,0,0.2)",
                                                        height: 0.09
                                                      }}
                                                    />
                                                  </div>
                                                );
                                              })
                                            : null}
                                        </Grid>
                                        <TablePagination
                                          component="div"
                                          style={{
                                            marginTop: 20
                                          }}
                                          onClick={event =>
                                            event.stopPropagation()
                                          }
                                          count={
                                            genero.Musics
                                              ? genero.Musics.length
                                              : 0
                                          }
                                          onChangePage={handlePageChangeMusic}
                                          onChangeRowsPerPage={
                                            handleLimitChangeMusic
                                          }
                                          labelRowsPerPage="Linhas por página:"
                                          page={pageMusic}
                                          rowsPerPage={limitMusic}
                                          rowsPerPageOptions={[2, 5, 10, 25]}
                                        />
                                      </div>
                                    </AccordionDetails>
                                  </Accordion>
                                )
                            )}
                        </div>
                      ))}

                       
                  </CardContent>
                </PerfectScrollbar>
              </Card>

              <MessageConfirm
                open={showConfirm}
                textInfo={`Você confirmando essa operação, a musica selecionada será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
                title="Deseja excluir?"
                onClose={onCloseMessageConfirm}
                onAccept={onAcceptMessageConfirm}
              />
              <Backdrop style={{ zIndex: 999999 }} open={isBackDrop}>
                <CircularProgress style={{ color: "red" }} />
              </Backdrop>
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

function numKeys(obj) {
  var count = 0;
  for (var prop in obj) {
    count++;
  }
  return count;
}

export default ModalEditPlayList;
