import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import SreenSettingChannel from "../../Dashboard/views/channel/ModalSettingChannel/SreenSettingChannel";
//import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    flex: 1
  },
}));

const ModalSettingChannel = ({
  open,
  onCloseSettingChannel,
  getAllChannel,
  selectId,
  getSettingChannelModify,
  isMobile,
  getInfoClient,
  indexValuesPreviewList
}) => {
  const classes = useStyles();

  return (
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseSettingChannel}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
              <SreenSettingChannel
                onCloseModal={onCloseSettingChannel}
                getAllChannel={getAllChannel}
                idClient={selectId}
                getSettingChannelModify={getSettingChannelModify}
                isMobile={isMobile}
                getInfoClient={getInfoClient}
                isChannel={true}
                indexValuesPreviewList={indexValuesPreviewList}
              />
        </Fade>
      </Modal>
  );
};

export default ModalSettingChannel;
