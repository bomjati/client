import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Input,
  MenuItem,
  Chip,
  Typography,
  GridList,
  Avatar,
  Hidden
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../Dashboard/utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Cancel as CancelIcon,
  Delete as DeleteIcon,
  BusinessCenter as BusinessCenterIcon,
  Block as BlockIcon,
  Dns as DnsIcon,
  ListAlt as ListAltIcon,
  Assignment as AssignmentIcon,
  MarkunreadMailbox as MarkunreadMailboxIcon,
  AddCircle as AddCircleIcon,
  Today as TodayIcon,
  Event as EventIcon,
  ExpandMore as ExpandMoreIcon,
  ExpandLess as ExpandLessIcon,
  Send as SendIcon
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../../Dashboard/views/channel/help";
import { showMessage } from "../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../services/api";
import momentTimezone from "moment-timezone";
import moment from "moment";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: 2,
    marginTop: 10
  },
  selected: {
    backgroundColor: "rgba(235, 47, 6, 0.3)"
  },
  noLabel: {
    marginTop: theme.spacing(3)
  }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
    backgroundColor:
      personName.indexOf(name) === -1 ? "transparent" : "rgba(235, 47, 6, 0.3)"
  };
}

const Formulario = ({
  onCloseModal,
  getAllChannel,
  contentSelectChannel,
  idClient,
  isMobile,
  getInfoClient
}) => {
  let history = useNavigate();
  const classes = useStyles();
  const themes = useTheme();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listSegment, setListSegment] = useState(null);
  const [value, setValue] = useState(0);
  const [businessId, setBusinessId] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [personName, setPersonName] = useState([]);
  const [listTimeZones, setListTimeZones] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isViewListProduct, setIsViewListProduct] = useState(true);
  const [sendEmail, setSendEmail] = useState("");
  const [sendStartDate, setSendStartDate] = useState(
    moment().format("DD/MM/YYYY")
  );
  const [sendEndDate, setSendEndDate] = useState(moment().format("DD/MM/YYYY"));
  const [sendListProduct, setSendListProduct] = useState([
    { name: "", price: "R$ 0,00" }
  ]);
  const [sendObs, setSendObs] = useState("");
  const [sendTextFree, setSendTextFree] = useState("");

  const colorButtonContent = "red";
  const colorButtonMarginIcon = 20;
  const optionMenu = [
    {
      name: "Com Produtos",
      icon: (
        <MarkunreadMailboxIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(1);
      }
    },
    {
      name: "Sem Produtos",
      icon: (
        <AssignmentIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(2);
      }
    }
  ];

  useEffect(() => {
    console.log(getInfoClient);
  }, []);

  useEffect(() => {
    let timeZone = [];
    for (let index = 0; index < momentTimezone.tz.names().length; index++) {
      const element = momentTimezone.tz.names()[index];

      timeZone.push({
        local: String(element).replaceAll("_", " "),
        fuso: moment()
          .tz(element)
          .format("Z")
      });
    }

    setListTimeZones(timeZone);
  }, []);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566"
      }
    }
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  const formatRealBRL = v => {
    //console.log(v)
    v = parseFloat(parseInt(String(v).replace(/\D/g, "")) / 100).toFixed(2);
    v = String("R$ " + v).replace(".", ",");
    return v;
  };

  const onValidate = () => {
    if (sendEmail == "") {
      showMessage("Campo Vázio", "Campo email esta vázio", "warning");
      return;
    }

    if (value == 1) {
      if (sendListProduct[0].name == "") {
        showMessage(
          "Campo Vázio",
          "Campo nome do produto esta vázio",
          "warning"
        );
        return;
      }
    } else {
      if (sendTextFree == "") {
        showMessage("Campo Vázio", "Campo texto livre esta vázio", "warning");
        return;
      }
    }

    onSendEmailNow();
  };

  const onSendEmailNow = () => {
    setIsLoading(true);

    const dateS = moment(sendStartDate, "DD/MM/YYYY").format("DD/MM/YYYY");
    const dateE = moment(sendEndDate, "DD/MM/YYYY").format("DD/MM/YYYY");

    let arrayProduct = "";
    console.log(
      `<b>${getInfoClient.name} - ${
        getInfoClient.Channels[0].Business.business_name
      }</b> <br /><br /> <b>${
        value == 1 ? "Produtos" : "Texto Livre"
      }</b> <br /><br /> ${
        value == 1 ? arrayProduct : sendTextFree
      } <br /><br /> <b>Observação: </b> <br /><br /> ${sendObs} <br /><br /> Data de Vigência: <br /><br /> <b>Inicio: </b> ${dateS} <br /> <b>Fim: </b> ${dateE} <br />`
    );

    for (let index = 0; index < sendListProduct.length; index++) {
      const element = sendListProduct[index];

      arrayProduct =
        arrayProduct + `● ${element.name} - <b>${element.price}</b><br />`;
    }

    api
      .post(`/supportrequest/new`, {
        emails: sendEmail,
        informations: JSON.stringify([
          {
            client: getInfoClient.name,
            adm: getInfoClient.Channels[0].Business.business_name,
            isTextFree: value == 1 ? "false" : "true",
            product: sendListProduct,
            observation: sendObs,
            textFree: sendTextFree
          }
        ]),
        startDate: moment(sendStartDate, "DD/MM/YYYY")
          .add(1, "days")
          .format("YYYY-MM-DD"),
        endDate: moment(sendEndDate, "DD/MM/YYYY")
          .add(1, "days")
          .format("YYYY-MM-DD"),
        verified: "false",
        clientId: getInfoClient.id
      })
      .then(response => response.data)
      .then(resp => {
        if (resp.message == "success") {
          api
            .post(`/sendemail/send`, {
              adm: getInfoClient.Channels[0].Business.business_name,
              client: getInfoClient.name,
              reply: sendEmail,
              text: `<b>${getInfoClient.name} - ${
                getInfoClient.Channels[0].Business.business_name
              }</b> <br /><br /> <b>${
                value == 1 ? "Produtos" : "Texto Livre"
              }</b> <br /><br /> ${
                value == 1 ? arrayProduct : sendTextFree
              } <br /><br /> <b>Observação: </b> <br /><br /> ${sendObs} <br /><br /> Data de Vigência: <br /><br /> <b>Inicio: </b> ${dateS} <br /> <b>Fim: </b> ${dateE} <br />`
            })
            .then(response => response.data)
            .then(resp => {
              setIsLoading(false);
              if (resp.message == "success") {
                showMessage(
                  "Pedido Realizado",
                  "Pedido de conteúdo realizado com sucesso.",
                  "success"
                );

                onCloseModal();
              } else {
                showMessage(
                  "Pedido Falhou",
                  "Pedido de conteúdo falhou.",
                  "danger"
                );
              }
            })
            .catch(error => {
              console.log(error);
            });
        } else {
          setIsLoading(false);
          showMessage("Pedido Falhou", "Pedido de conteúdo falhou.", "danger");
        }
      })
      .catch(error => {
        setIsLoading(false);
        console.log(error);
      });
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="space-between"
          className="w-100 mt-2 mb-2 mr-2 ml-2"
          p={0}
        >
          {value == 0 ? (
            <div />
          ) : (
            <IconButton
              style={{ backgroundColor: "white" }}
              onClick={onCloseModal}
            >
              <CloseIcon />
            </IconButton>
          )}
          {value == 0 ? (
            <IconButton
              style={{ backgroundColor: "white" }}
              onClick={onCloseModal}
            >
              <CloseIcon />
            </IconButton>
          ) : (
            <Button
              onClick={() => {
                setValue(0);
              }}
              style={{
                backgroundColor: "red",
                color: "white",
                fontWeight: "bold"
              }}
              variant="contained"
            >
              Voltar menu de opções
            </Button>
          )}
        </Box>
        <Card style={{ borderRadius: 20, width: !isMobile ? 720 : "100%" }}>
          <CardHeader
            subheader={`${
              value == 0
                ? "Selecione a opção desejada"
                : "Informações sobre seus conteúdos"
            }`}
            title={
              value == 1
                ? "Com Produtos"
                : value == 2
                ? "Sem Produtos"
                : "Comerciais"
            }
          />

          {/* <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Conteúdos" {...a11yProps(1)} />
                <Tab label="Comerciais" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
          </div> */}
          <Divider />
          <CardContent>
            <div>
              <TabPanel value={value} index={0} dir={theme.direction}>
                <Box margin={-4} display="flex" flexDirection="column">
                  {optionMenu.map(oMenu => {
                    return (
                      <IconButton
                        style={{ borderRadius: 15 }}
                        onClick={() => oMenu.onClick()}
                      >
                        <Card
                          style={{
                            width: "100%",
                            borderRadius: 15,
                            borderWidth: 0.1,
                            borderStyle: "solid",
                            borderColor: "rgba(192, 192, 192, 0.4)"
                          }}
                        >
                          <Box
                            display="flex"
                            justifyContent="space-between"
                            alignItems="center"
                          >
                            <Box
                              alignItems="center"
                              display="flex"
                              flexDirection="row"
                            >
                              {oMenu.icon}
                              <Divider
                                style={{ height: 80 }}
                                orientation="vertical"
                              />
                            </Box>
                            <h5
                              style={{ color: "#3d3d3d" }}
                              className="text-black text-center ml-2 mr-2"
                            >
                              {oMenu.name}
                            </h5>
                            <div />
                          </Box>
                        </Card>
                      </IconButton>
                    );
                  })}
                </Box>
              </TabPanel>
              <TabPanel value={value} index={1} dir={theme.direction}>
                <div>
                  <TextField
                    fullWidth
                    label="Email de Retorno: "
                    helperText="Digite aqui o e-mail que deseja receber o retorno do pedido."
                    disabled={isLoading}
                    name="email"
                    onChange={event => {
                      setSendEmail(event.target.value);
                    }}
                    required
                    value={sendEmail}
                    variant="outlined"
                  />

                  <Box display="flex" flexWrap="wrap" mt={4}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        id="time-picker"
                        format="dd/MM/yyyy"
                        inputVariant="outlined"
                        disabled={isLoading}
                        label="Date picker inline"
                        name="startDate"
                        label="Inicio da Execução"
                        value={moment(sendStartDate, "DD/MM/YYYY")}
                        onChange={event => {
                          setSendStartDate(moment(event).format("DD/MM/YYYY"));
                        }}
                        fullWidth
                        style={{ marginRight: 10, marginTop: 10 }}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                        keyboardIcon={<TodayIcon />}
                      />
                    </MuiPickersUtilsProvider>

                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        id="time-picker"
                        format="dd/MM/yyyy"
                        inputVariant="outlined"
                        disabled={isLoading}
                        label="Time picker"
                        name="startHour"
                        label="Fim da Execução"
                        value={moment(sendEndDate, "DD/MM/YYYY")}
                        onChange={event => {
                          setSendEndDate(moment(event).format("DD/MM/YYYY"));
                        }}
                        fullWidth
                        style={{ marginRight: 10, marginTop: 10 }}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                        keyboardIcon={<TodayIcon />}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>

                  <Card
                    style={{
                      borderRadius: 15,
                      borderWidth: 0.1,
                      borderStyle: "solid",
                      borderColor: "rgba(192, 192, 192, 0.4)"
                    }}
                    className="p-4 mt-4 mb-4"
                  >
                    <Box
                      onClick={() => setIsViewListProduct(!isViewListProduct)}
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                      className="mb-4"
                    >
                      <h5>Lista de Produtos</h5>
                      {isViewListProduct ? (
                        <ExpandLessIcon style={{ color: "black" }} />
                      ) : (
                        <ExpandMoreIcon style={{ color: "black" }} />
                      )}
                    </Box>

                    {!isViewListProduct
                      ? null
                      : sendListProduct.map((product, index) => (
                          <Box display="flex" flexWrap="wrap" mb={2}>
                            <Hidden only={["sm", "xl", "xs"]}>
                              {!(sendListProduct.length > 1) ? null : (
                                <IconButton
                                  disabled={isLoading}
                                  style={{ marginBottom: 20, marginRight: 20 }}
                                  onClick={() => {
                                    let arrayData = sendListProduct.filter(
                                      (t, indexT) => indexT != index
                                    );

                                    setSendListProduct(arrayData);
                                  }}
                                >
                                  <DeleteIcon style={{ color: "#ff4d4d" }} />
                                </IconButton>
                              )}
                            </Hidden>

                            <TextField
                              label="Descrição Produto: "
                              helperText="Ex: Coca-cola 2L."
                              disabled={isLoading}
                              name="name"
                              onChange={event => {
                                let arrayData = sendListProduct.filter(
                                  (t, indexT) => indexT != index
                                );

                                arrayData.splice(index, 0, {
                                  name: event.target.value,
                                  price: sendListProduct.filter(
                                    (t, indexT) => indexT == index
                                  )[0].price
                                });

                                setSendListProduct(arrayData);
                              }}
                              required
                              style={{ marginTop: 10 }}
                              value={product.name}
                              variant="outlined"
                            />
                            <Box display="flex" flexWrap="wrap">
                              <TextField
                                label="Preço: "
                                disabled={isLoading}
                                name="price"
                                helperText=" "
                                onChange={event => {
                                  let arrayData = sendListProduct.filter(
                                    (t, indexT) => indexT != index
                                  );

                                  arrayData.splice(index, 0, {
                                    name: sendListProduct.filter(
                                      (t, indexT) => indexT == index
                                    )[0].name,
                                    price: formatRealBRL(event.target.value)
                                  });

                                  setSendListProduct(arrayData);
                                }}
                                required
                                style={{
                                  marginLeft: 10,
                                  marginTop: 10
                                }}
                                value={product.price}
                                variant="outlined"
                              />

                              <Hidden only={["sm", "xl", "xs"]}>
                                {!(
                                  index + 1 ==
                                  sendListProduct.length
                                ) ? null : (
                                  <IconButton
                                    disabled={isLoading}
                                    onClick={() => {
                                      if (sendListProduct.length == 10) {
                                        showMessage(
                                          "Produtos",
                                          "Limite de produtos alcançados",
                                          "warning"
                                        );

                                        return;
                                      }

                                      let arrayData = sendListProduct.filter(
                                        (t, indexT) => indexT > -1
                                      );

                                      arrayData.push({
                                        name: "",
                                        price: 0
                                      });

                                      setSendListProduct(arrayData);
                                    }}
                                    style={{ marginBottom: 20, marginLeft: 20 }}
                                  >
                                    <AddCircleIcon
                                      style={{ color: "#2ecc71" }}
                                    />
                                  </IconButton>
                                )}
                              </Hidden>
                            </Box>
                            <Hidden only={["md", "lg"]}>
                              {!(sendListProduct.length > 1) ? null : (
                                <Button
                                  fullWidth
                                  disabled={isLoading}
                                  variant="contained"
                                  style={{
                                    marginBottom: 20,
                                    backgroundColor: "#ff4d4d",
                                    color: "white"
                                  }}
                                  onClick={() => {
                                    let arrayData = sendListProduct.filter(
                                      (t, indexT) => indexT != index
                                    );

                                    setSendListProduct(arrayData);
                                  }}
                                  startIcon={
                                    <DeleteIcon style={{ color: "white" }} />
                                  }
                                >
                                  Excluir
                                </Button>
                              )}
                            </Hidden>
                            <Hidden only={["md", "lg"]}>
                              {!(index + 1 == sendListProduct.length) ? null : (
                                <Button
                                  fullWidth
                                  variant="contained"
                                  disabled={isLoading}
                                  onClick={() => {
                                    if (sendListProduct.length == 10) {
                                      showMessage(
                                        "Produtos",
                                        "Limite de produtos alcançados",
                                        "warning"
                                      );

                                      return;
                                    }

                                    let arrayData = sendListProduct.filter(
                                      (t, indexT) => indexT > -1
                                    );

                                    arrayData.push({
                                      name: "",
                                      price: 0
                                    });

                                    setSendListProduct(arrayData);
                                  }}
                                  style={{
                                    marginBottom: 20,
                                    backgroundColor: "#2ecc71",
                                    color: "white"
                                  }}
                                  startIcon={
                                    <AddCircleIcon style={{ color: "white" }} />
                                  }
                                >
                                  Mais Produtos
                                </Button>
                              )}
                            </Hidden>
                          </Box>
                        ))}
                  </Card>

                  <TextField
                    fullWidth
                    label="Observações: "
                    helperText="Campo livre para informações adicionais assim como frases desejadas, observações e exceções."
                    disabled={isLoading}
                    name="observacao"
                    onChange={event => {
                      setSendObs(event.target.value);
                    }}
                    multiline
                    rows={3}
                    required
                    value={sendObs}
                    variant="outlined"
                  />

                  <Box
                    mt={4}
                    display="flex"
                    justifyContent="flex-end"
                    alignItems="center"
                  >
                    {isLoading ? (
                      <CircularProgress
                        className="mr-4"
                        style={{ color: "red" }}
                      />
                    ) : null}
                    <Button
                      onClick={() => onValidate()}
                      disabled={isLoading}
                      style={{
                        borderRadius: 50,
                        backgroundColor: "red",
                        color: "white"
                      }}
                      variant="contained"
                      size="medium"
                      startIcon={<SendIcon style={{ color: "white" }} />}
                    >
                      Enviar
                    </Button>
                  </Box>
                </div>
              </TabPanel>
              <TabPanel value={value} index={2} dir={theme.direction}>
                <div>
                  <TextField
                    fullWidth
                    label="Email de Retorno: "
                    helperText="Digite aqui o e-mail que deseja receber o retorno do pedido."
                    disabled={isLoading}
                    name="email"
                    onChange={event => {
                      setSendEmail(event.target.value);
                    }}
                    required
                    value={sendEmail}
                    variant="outlined"
                  />

                  <Box display="flex" flexWrap="wrap" mt={4}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        id="time-picker"
                        format="dd/MM/yyyy"
                        inputVariant="outlined"
                        label="Date picker inline"
                        disabled={isLoading}
                        name="startDate"
                        label="Inicio da Execução"
                        value={moment(sendStartDate, "DD/MM/YYYY")}
                        onChange={event => {
                          setSendStartDate(moment(event).format("DD/MM/YYYY"));
                        }}
                        fullWidth
                        style={{ marginRight: 10, marginTop: 10 }}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                        keyboardIcon={<TodayIcon />}
                      />
                    </MuiPickersUtilsProvider>

                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        id="time-picker"
                        format="dd/MM/yyyy"
                        inputVariant="outlined"
                        disabled={isLoading}
                        label="Time picker"
                        name="startHour"
                        label="Fim da Execução"
                        value={moment(sendEndDate, "DD/MM/YYYY")}
                        onChange={event => {
                          setSendEndDate(moment(event).format("DD/MM/YYYY"));
                        }}
                        fullWidth
                        style={{ marginRight: 10, marginTop: 10 }}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                        keyboardIcon={<TodayIcon />}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>

                  <TextField
                    fullWidth
                    label="Texto Livre: "
                    helperText="Digite suas informações detalhadas."
                    disabled={isLoading}
                    name="textFree"
                    onChange={event => {
                      setSendTextFree(event.target.value);
                    }}
                    multiline
                    rows={3}
                    required
                    value={sendTextFree}
                    className="mt-4 mb-4"
                    variant="outlined"
                  />

                  <TextField
                    fullWidth
                    label="Observações: "
                    helperText="Campo livre para informações adicionais assim como frases desejadas, observações e exceções."
                    disabled={isLoading}
                    name="observacao"
                    onChange={event => {
                      setSendObs(event.target.value);
                    }}
                    multiline
                    rows={3}
                    required
                    value={sendObs}
                    variant="outlined"
                  />

                  <Box
                    mt={4}
                    display="flex"
                    justifyContent="flex-end"
                    alignItems="center"
                  >
                    {isLoading ? (
                      <CircularProgress
                        className="mr-4"
                        style={{ color: "red" }}
                      />
                    ) : null}
                    <Button
                      onClick={() => onValidate()}
                      disabled={isLoading}
                      style={{
                        borderRadius: 50,
                        backgroundColor: "red",
                        color: "white"
                      }}
                      variant="contained"
                      size="medium"
                      startIcon={<SendIcon style={{ color: "white" }} />}
                    >
                      Enviar
                    </Button>
                  </Box>
                </div>
              </TabPanel>
            </div>
          </CardContent>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
