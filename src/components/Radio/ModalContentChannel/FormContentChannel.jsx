import React, { useState, useContext, useEffect, MouseEvent } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Input,
  Chip,
  Typography,
  GridList,
  Avatar,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ContextValidacoesCadastro from "../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Cancel as CancelIcon,
  BusinessCenter as BusinessCenterIcon,
  Block as BlockIcon,
  Dns as DnsIcon,
  ListAlt as ListAltIcon,
  MoreVert as MoreVertIcon,
  Delete as DeleteIcon,
  Today as TodayIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../utils/message";
import MessageConfirm from "../utils/MessageConfirm";
import EditCommercial from "../../Dashboard/views/channel/ModalSettingChannel/commercial/ModalEditCommercial";
import NewCommercial from "../../Dashboard/views/channel/ModalCommercial";
import api, { apiReceitaWs, apiViaCepWs } from "../../../services/api";
import momentTimezone from "moment-timezone";
import moment from "moment";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import ToolbarCommercial from "./Toolbar";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
    marginTop: 10,
  },
  selected: {
    backgroundColor: "rgba(235, 47, 6, 0.3)",
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
    backgroundColor:
      personName.indexOf(name) === -1 ? "transparent" : "rgba(235, 47, 6, 0.3)",
  };
}

const Formulario = ({
  onCloseModal,
  getAllChannel,
  commercials,
  contentSelectChannel,
  idClient,
  listBlocked,
  onGetBlockeds,
  isMobile,
}) => {
  let history = useNavigate();
  const classes = useStyles();
  const themes = useTheme();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [listSegment, setListSegment] = useState(null);
  const [value, setValue] = useState(0);
  const [businessId, setBusinessId] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [personName, setPersonName] = useState([]);
  const [listTimeZones, setListTimeZones] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isOpenMenu, setIsOpenMenu] = useState({ id: 0, active: false });

  const [showConfirm, setShowConfirm] = useState(false);
  const [showConfirmLote, setShowConfirmLote] = useState(false);
  const [Commercialss, setCommercial] = useState([]);
  const [CommercialBusca, setCommercialBusca] = useState([]);
  const [showEditCommercial, setShowEditCommercial] = useState(false);
  const [idCommercialModify, setIdCommercialModify] = useState(0);
  const [getCommercialModify, setGetCommercialModify] = useState();
  const [showNewCommercial, setShowNewCommercial] = useState(false);

  const colorButtonContent = "red";
  const colorButtonMarginIcon = 20;
  const optionMenu = [
    {
      name: "Conteúdos",
      icon: (
        <DnsIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(1);
      },
    },
    {
      name: "Comerciais",
      icon: (
        <ListAltIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(2);
      },
    },
  ];

  useEffect(() => {
    console.log(contentSelectChannel);
  }, [contentSelectChannel]);

  useEffect(() => {
    let timeZone = [];
    for (let index = 0; index < momentTimezone.tz.names().length; index++) {
      const element = momentTimezone.tz.names()[index];

      timeZone.push({
        local: String(element).replaceAll("_", " "),
        fuso: moment().tz(element).format("Z"),
      });
    }

    setListTimeZones(timeZone);
  }, []);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handleClose = (id) => {
    setIsOpenMenu({ id: id, active: false });
  };

  const handleClick = (event: MouseEvent<HTMLButtonElement>, id) => {
    setIsOpenMenu({
      id: id,
      active: event.currentTarget,
    });
  };

  const onBlockContent = (id) => {
    api
      .post("/blocked/new", {
        clientId: idClient,
        musicId: 0,
        commercialId: 0,
        vignetteId: 0,
        contentId: id,
      })
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          showMessage("Bloqueio", "Bloqueio realizado com sucesso", "success");
          onGetBlockeds();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onBlockCommercial = (id) => {
    api
      .post("/blocked/new", {
        clientId: idClient,
        musicId: 0,
        commercialId: id,
        vignetteId: 0,
        contentId: 0,
      })
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          showMessage(
            "Desbloqueio",
            "Desbloqueio realizado com sucesso",
            "success"
          );
          onGetBlockeds();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onActive = (id) => {
    api
      .delete(`/blocked/${id}`)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          onGetBlockeds();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  /* --------------------------- Functions Commercial ---------------------------------- */

  useEffect(() => {
    if (idClient) {
      getCommercial(idClient);
    }
  }, [idClient]);

  const buscaCommercial = (value) => {
    const resultBusca = CommercialBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setCommercial(resultBusca);
  };

  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdCommercialModify(id);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteCommercial(idCommercialModify);
  };

  const onShowConfirmLote = () => {
    setShowConfirmLote(true);
  };

  const getCommercial = async (id) => {
    setIsLoading(true);
    const res = await api.get(`/Commercial/[${id}]`);

    if (res) {
      setCommercial(res.data);
      setCommercialBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeleteCommercial = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Commercial/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getCommercial(idClient);
        showMessage(
          "Comercial Deletado",
          "Comercial selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Comercial Falhou",
        "Houve um problema ao deletar seu comercial, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onCloseEditCommercial = () => {
    setShowEditCommercial(false);
  };

  const onShowEditCommercial = (data) => {
    setGetCommercialModify(data);
    setShowEditCommercial(true);
  };

  const onCloseNewCommercial = () => {
    setShowNewCommercial(false);
  };

  const onShowNewCommercial = () => {
    setShowNewCommercial(true);
  };

  /* ------------------------- End Functiom Commercial --------------------------------- */

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="space-between"
          className="w-100 mt-2 mb-2 mr-2 ml-2"
          p={0}
        >
          {value == 0 ? (
            <div />
          ) : (
            <IconButton
              style={{ backgroundColor: "white" }}
              onClick={onCloseModal}
            >
              <CloseIcon />
            </IconButton>
          )}

          {value == 0 ? (
            <IconButton
              style={{ backgroundColor: "white" }}
              onClick={onCloseModal}
            >
              <CloseIcon />
            </IconButton>
          ) : (
            <Button
              onClick={() => {
                setValue(0);
              }}
              style={{
                backgroundColor: "red",
                color: "white",
                fontWeight: "bold",
              }}
              variant="contained"
            >
              Voltar menu de opções
            </Button>
          )}
        </Box>
        <Card
          style={{
            borderRadius: 20,
            width: !isMobile ? 720 : "100%",
          }}
        >
          <CardHeader
            subheader={`${
              value == 1
                ? "Veja abaixo os conteúdos de cada segmento que selecionou"
                : "Informações sobre seus conteúdos"
            }`}
            title="Conteúdos"
          />

          {/* <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Conteúdos" {...a11yProps(1)} />
                <Tab label="Comerciais" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
          </div> */}
          <Divider />
          <CardContent>
            <div>
              <TabPanel value={value} index={0} dir={theme.direction}>
                <Box margin={-4} display="flex" flexDirection="column">
                  {optionMenu.map((oMenu) => {
                    return (
                      <IconButton
                        style={{ borderRadius: 15 }}
                        onClick={() => oMenu.onClick()}
                      >
                        <Card
                          style={{
                            width: "100%",
                            borderRadius: 15,
                            borderWidth: 0.1,
                            borderStyle: "solid",
                            borderColor: "rgba(192, 192, 192, 0.4)",
                          }}
                        >
                          <Box
                            display="flex"
                            justifyContent="space-between"
                            alignItems="center"
                          >
                            <Box
                              alignItems="center"
                              display="flex"
                              flexDirection="row"
                            >
                              {oMenu.icon}
                              <Divider
                                style={{ height: 80 }}
                                orientation="vertical"
                              />
                            </Box>
                            <h5
                              style={{ color: "#3d3d3d" }}
                              className="text-black text-center ml-2 mr-2"
                            >
                              {oMenu.name}
                            </h5>
                            <div />
                          </Box>
                        </Card>
                      </IconButton>
                    );
                  })}
                </Box>
              </TabPanel>
              <TabPanel value={value} index={1} dir={theme.direction}>
                <div>
                  {contentSelectChannel.map((seg, index) => (
                    <div>
                      <h5 className={`${index == 0 ? "mt-0" : "mt-4"}`}>
                        {seg.Segment.name}
                      </h5>
                      <PerfectScrollbar className="mb-4">
                        <Box display="flex" className="pl-3">
                          {seg.Segment.Contents.map((c, indexC) => {
                            return (
                              <div>
                                <Chip
                                  style={{
                                    position: "absolute",
                                    zIndex: 10,
                                    marginTop: 112,
                                    marginLeft: -10,
                                    backgroundColor:
                                      listBlocked.filter(
                                        (b) => b.contentId == c.id
                                      ).length > 0
                                        ? "#F4320C"
                                        : "#4B006E",
                                    color: "white",
                                    boxShadow:
                                      "2px 0px 8px -2px rgba(0,0,0,0.6)",
                                    WebkitBoxShadow:
                                      "2px 0px 8px -2px rgba(0,0,0,0.6)",
                                    MozBoxShadow:
                                      "2px 0px 8px -2px rgba(0,0,0,0.6)",
                                  }}
                                  size="small"
                                  icon={
                                    listBlocked.filter(
                                      (b) => b.contentId == c.id
                                    ).length > 0 ? (
                                      <BlockIcon style={{ color: "white" }} />
                                    ) : (
                                      <CheckIcon style={{ color: "white" }} />
                                    )
                                  }
                                  label={
                                    listBlocked.filter(
                                      (b) => b.contentId == c.id
                                    ).length > 0
                                      ? "Bloqueado"
                                      : "Ativado"
                                  }
                                />
                                <Card
                                  className="mt-4 m-1 mr-4 mb-4"
                                  style={{
                                    width: 210,
                                    borderRadius: 10,
                                    borderWidth: 0.1,
                                    borderStyle: "solid",
                                    borderColor: "rgba(192, 192, 192, 0.4)",
                                  }}
                                >
                                  <Avatar
                                    className="mb-2 mr-4"
                                    style={{
                                      height: 100,
                                      width: 210,
                                    }}
                                    variant="rounded"
                                    src={
                                      seg.Segment.Covers.length > 0
                                        ? seg.Segment.Covers[
                                            indexC < seg.Segment.Covers.length
                                              ? indexC
                                              : indexC -
                                                seg.Segment.Covers.length
                                          ].path_s3
                                        : ""
                                    }
                                  />
                                  <Box display="flex" justifyContent="center">
                                    <audio
                                      controls
                                      controlsList="nodownload"
                                      className="mb-2 mt-3 mb-3"
                                      style={{ width: 190 }}
                                      src={c.path_s3}
                                    />
                                  </Box>
                                  <Box display="flex" className="pl-4 pr-4">
                                    <Button
                                      onClick={() => {
                                        const getListBlock = listBlocked.filter(
                                          (b) => b.contentId == c.id
                                        );

                                        if (getListBlock.length > 0) {
                                          onActive(getListBlock[0].id);
                                        } else {
                                          onBlockContent(c.id);
                                        }
                                      }}
                                      size="small"
                                      variant="contained"
                                      className="mb-2"
                                      fullWidth
                                      style={{
                                        backgroundColor:
                                          listBlocked.filter(
                                            (b) => b.contentId == c.id
                                          ).length > 0
                                            ? "#3F012C"
                                            : "#000133",
                                        color: "white",
                                        borderRadius: 20,
                                      }}
                                      startIcon={
                                        listBlocked.filter(
                                          (b) => b.contentId == c.id
                                        ).length > 0 ? (
                                          <CheckIcon
                                            style={{ color: "white" }}
                                          />
                                        ) : (
                                          <BlockIcon
                                            style={{ color: "white" }}
                                          />
                                        )
                                      }
                                    >
                                      {listBlocked.filter(
                                        (b) => b.contentId == c.id
                                      ).length > 0
                                        ? "Desbloquear"
                                        : "Bloquear"}
                                    </Button>
                                  </Box>
                                </Card>
                              </div>
                            );
                          })}
                        </Box>
                      </PerfectScrollbar>

                      {seg.Segment.Contents.length == 0 ? (
                        <div>
                          <p
                            style={{
                              color: "rgba(0,0,0,0.5)",
                              fontWeight: "bold",
                            }}
                            className="text-black text-center text-bold"
                          >
                            Nenhum conteúdo
                          </p>
                          <p
                            style={{
                              color: "rgba(0,0,0,0.5)",
                              fontWeight: "bold",
                              marginTop: -4,
                            }}
                            className="text-black text-center text-bold"
                          >
                            disponível
                          </p>
                          <p
                            style={{ color: "rgba(0,0,0,0.5)" }}
                            className="text-black text-center"
                          >
                            Já estamos trabalhando nisso,
                          </p>
                          <p
                            style={{
                              marginTop: -8,
                              color: "rgba(0,0,0,0.5)",
                            }}
                            className="text-black text-center"
                          >
                            em breve o disponibilizaremos.
                          </p>
                        </div>
                      ) : null}
                    </div>
                  ))}
                </div>
              </TabPanel>
              <TabPanel value={value} index={2} dir={theme.direction}>
                <ToolbarCommercial
                  onShowNewCommercial={onShowNewCommercial}
                  SearchCommercial={buscaCommercial}
                />
                <Grid container spacing={1}>
                  <Grid item md={12} xs={12}>
                    <h5 className="mt-4">Meus Conteúdos</h5>
                    {Commercialss.length == 0 ? null : (
                      <div className="mb-1">
                        <PerfectScrollbar className="">
                          <Box marginBottom={2} display="flex">
                            {Commercialss.map((commercial, index) => (
                              <div>
                                <Card
                                  className="p-2 m-1 mt-3"
                                  style={{
                                    width: 210,
                                    borderRadius: 10,
                                    borderWidth: 0.1,
                                    borderStyle: "solid",
                                    borderColor: "rgba(192, 192, 192, 0.4)",
                                  }}
                                >
                                  <Box
                                    display="flex"
                                    justifyContent="space-between"
                                    alignItems="center"
                                  >
                                    <Chip
                                      style={{
                                        backgroundColor:
                                          listBlocked.filter(
                                            (b) =>
                                              b.commercialId == commercial.id
                                          ).length > 0
                                            ? "#F4320C"
                                            : "#4B006E",
                                        color: "white",
                                        fontWeight: "bold",
                                        boxShadow:
                                          "1px 0px 8px -3px rgba(0,0,0,0.6)",
                                        WebkitBoxShadow:
                                          "1px 0px 8px -3px rgba(0,0,0,0.6)",
                                        MozBoxShadow:
                                          "1px 0px 8px -3px rgba(0,0,0,0.6)",
                                      }}
                                      className="mt-1"
                                      size="small"
                                      icon={
                                        listBlocked.filter(
                                          (b) => b.commercialId == commercial.id
                                        ).length > 0 ? (
                                          <BlockIcon
                                            style={{ color: "white" }}
                                          />
                                        ) : (
                                          <CheckIcon
                                            style={{ color: "white" }}
                                          />
                                        )
                                      }
                                      label={
                                        listBlocked.filter(
                                          (b) => b.commercialId == commercial.id
                                        ).length > 0
                                          ? "Bloqueado"
                                          : "Ativado"
                                      }
                                    />
                                    <div>
                                      <IconButton
                                        onClick={(event) => {
                                          event.stopPropagation();
                                          handleClick(event, commercial.id);
                                        }}
                                      >
                                        <MoreVertIcon />
                                      </IconButton>

                                      <Menu
                                        id="simple-menu"
                                        anchorEl={
                                          isOpenMenu.id == commercial.id
                                            ? isOpenMenu.active
                                            : null
                                        }
                                        PaperProps={{
                                          style: {
                                            backgroundColor: "white",
                                          },
                                        }}
                                        keepMounted
                                        open={Boolean(
                                          isOpenMenu.id == commercial.id
                                            ? isOpenMenu.active
                                            : null
                                        )}
                                        onClose={handleClose}
                                      >
                                        <MenuItem
                                          onClick={() => {
                                            handleClose(commercial.id);
                                            onShowEditCommercial(commercial);
                                          }}
                                        >
                                          <ListItemIcon>
                                            <TodayIcon
                                              style={{ color: "#474787" }}
                                            />
                                          </ListItemIcon>
                                          <ListItemText
                                            style={{
                                              color: "rgba(0,0,0,0.9)",
                                            }}
                                          >
                                            {"Modificar"}
                                          </ListItemText>
                                        </MenuItem>
                                        <MenuItem
                                          onClick={() => {
                                            handleClose(commercial.id);
                                            onShowConfirm(commercial.id);
                                          }}
                                        >
                                          <ListItemIcon>
                                            <DeleteIcon
                                              style={{ color: "#ff4d4d" }}
                                            />
                                          </ListItemIcon>
                                          <ListItemText
                                            style={{
                                              color: "rgba(0,0,0,0.9)",
                                            }}
                                          >
                                            {"Deletar"}
                                          </ListItemText>
                                        </MenuItem>
                                      </Menu>
                                    </div>
                                  </Box>
                                  <p
                                    style={{ width: 210 }}
                                    className="mt-3 mb-2"
                                  >
                                    {String(commercial.name).substr(0, 40)}...
                                  </p>
                                  <Box display="flex" justifyContent="center">
                                    <audio
                                      controls
                                      controlsList="nodownload"
                                      style={{ width: 200 }}
                                      src={commercial.path_s3}
                                    />
                                  </Box>
                                  <Box display="flex" className="pl-4 pr-4">
                                    <Button
                                      onClick={() => {
                                        const getListBlock = listBlocked.filter(
                                          (b) => b.commercialId == commercial.id
                                        );

                                        if (getListBlock.length > 0) {
                                          onActive(getListBlock[0].id);
                                        } else {
                                          onBlockCommercial(commercial.id);
                                        }
                                      }}
                                      size="small"
                                      variant="contained"
                                      className="mt-3"
                                      fullWidth
                                      style={{
                                        backgroundColor:
                                          listBlocked.filter(
                                            (b) =>
                                              b.commercialId == commercial.id
                                          ).length > 0
                                            ? "#3F012C"
                                            : "#000133",
                                        color: "white",
                                        borderRadius: 20,
                                      }}
                                      startIcon={
                                        listBlocked.filter(
                                          (b) => b.commercialId == commercial.id
                                        ).length > 0 ? (
                                          <CheckIcon
                                            style={{ color: "white" }}
                                          />
                                        ) : (
                                          <BlockIcon
                                            style={{ color: "white" }}
                                          />
                                        )
                                      }
                                    >
                                      {listBlocked.filter(
                                        (b) => b.commercialId == commercial.id
                                      ).length > 0
                                        ? "Desbloquear"
                                        : "Bloquear"}
                                    </Button>
                                  </Box>
                                </Card>
                              </div>
                            ))}
                          </Box>
                        </PerfectScrollbar>
                      </div>
                    )}

                    {Commercialss.length == 0 ? (
                      <div
                        className="pt-3 pb-3 mt-3 mb-4"
                        style={{
                          borderRadius: 10,
                          boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                          WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                          MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
                        }}
                      >
                        <p
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontWeight: "bold",
                          }}
                          className="text-black text-center text-bold"
                        >
                          Nenhum conteúdo
                        </p>
                        <p
                          style={{
                            color: "rgba(0,0,0,0.5)",
                            fontWeight: "bold",
                            marginTop: -4,
                          }}
                          className="text-black text-center text-bold"
                        >
                          disponível
                        </p>
                        <p
                          style={{ color: "rgba(0,0,0,0.5)" }}
                          className="text-black text-center"
                        >
                          Adicione seus conteúdos e aproveite,
                        </p>
                        <p
                          style={{
                            marginTop: -8,
                            color: "rgba(0,0,0,0.5)",
                          }}
                          className="text-black text-center"
                        >
                          o máximo nossos serviços.
                        </p>
                      </div>
                    ) : null}
                  </Grid>
                </Grid>
              
              </TabPanel>
            </div>
          </CardContent>
        </Card>
      </Container>
      <EditCommercial
        open={showEditCommercial}
        selectId={[idClient]}
        getCommercialModify={getCommercialModify}
        getAllComerciais={() => getCommercial(idClient)}
        onCloseNewCommercial={onCloseEditCommercial}
      />

      <NewCommercial
        open={showNewCommercial}
        selectId={[idClient]}
        onCloseNewCommercial={onCloseNewCommercial}
        getAllComerciais={() => getCommercial(idClient)}
      />

      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o comercial será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </form>
  );
};

export default Formulario;
