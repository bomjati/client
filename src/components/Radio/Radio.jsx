import React, { useEffect, useRef } from "react";
import useState from "react-usestateref";
import {
  Grid,
  Box,
  Button,
  GridList,
  Card,
  CircularProgress,
  Avatar,
  Typography,
  Container,
  Grow,
  Backdrop,
  IconButton,
} from "@material-ui/core";
import { makeStyles, createMuiTheme } from "@material-ui/core/styles";
import {
  AspectRatio as AspectRatioIcon,
  PlaylistAdd as PlaylistAddIcon,
  LibraryMusic as LibraryMusicIcon,
  SettingsVoice as SettingsVoiceIcon,
  GroupSharp,
  ChevronLeft as ArrowBackIosIcon,
  ChevronRight as ArrowForwardIosIcon,
} from "@material-ui/icons";

import Toolbar from "./Toolbar.jsx";
import Player from "./Player";
import PlayListCard from "./Cards/PlayListCard";
import GeneroCard from "./Cards/GerneroCard";
import GeneroSubCard from "./Cards/GerneroSubCard";
import ListMusicView from "./ListMusicView";
import ListMusicBlockFavorite from "./ListMusicBlockFavorite";
import ModalSpeech from "./ModalChamada";
import ModalOnline from "./ModalOnline";
import ModalBlock from "./ModalBlock";
import ModalBlockExpired from "./ModalBlockExpired";
import ModalNewEmployee from "./ModalNewEmployee";
import ModalNewPlayList from "./ModalNewPlayList";
import ModalEditPlayList from "./ModalEditPlayList";
import ModalConfigConteudo from "./ModalConfigConteudo";
import ModalContentChannel from "./ModalContentChannel";
import ModalSuportContact from "./ModalSuportContact";
import ModalSuporteWhatsApp from "./ModalSuporteWhatsApp";
import ModalListGenre from "./ModalListGenre";
import ModalRemoteChannel from "./ModalRemoteChannel";
import { rightDataArray } from "./Arrays/right";
//import { genreDataArray } from "./Arrays/genre";
//import { genresubDataArray } from "./Arrays/subgenre";
import api from "../../services/api";
import socket from "../../services/socket";
import { showMessage } from "./utils/message";
import MessageConfirm from "./utils/MessageConfirm";
import Drawer from "./Drawer";
import moment from "moment";
import "./App.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import yesno from "yesno-dialog";

const theme = createMuiTheme({
  palette: {
    type: "dark",
  },
});

const useStyles = makeStyles(({ palette, breakpoints, spacing }) => ({
  generos: {
    paddingTop: 20,
    [breakpoints.up("sm")]: {
      paddingTop: spacing(10),
    },
    [breakpoints.up("md")]: {
      paddingTop: spacing(14),
    },
  },
  large: {
    width: spacing(20),
    height: spacing(20),
  },
  bottomCapa: {
    marginBottom: 85,
    marginRight: 40,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const getParam = (param) => {
  return new URLSearchParams(window.location.search).get(param);
};

function detectar_mobile() {
  var check = false; //wrapper no check
  (function (a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}

const Radio = () => {
  let arrayFunctionPlay = {};
  let arrayPreviousSelectGenre = {};
  let arrayPreviousSelectSubGenre = {};
  let arrayListMusicRemoto = {};
  let arrayCallPlayRemoto = {};
  let contConected = 0;
  let arrayMusic = [];

  var genreDataArray = [];
  var genresubDataArray = [];
  //var typeSugListMobile = [];

  const classes = useStyles();
  const pf = useRef();
  const [scrollEl, setScrollEl] = useState({});
  //const [genreDataArray, setGenreDataArray] = useState([]);
  //const [genresubDataArray, setGenresubDataArray] = useState([]);
  const [typeSugListMobile, setTypeSugListMobile, typeSugListMobileRef] = useState([]);
  const [scrollElGenre, setScrollElGenre] = useState({});
  const [commercialTimeExecut, setCommercialTimeExecut] = useState([]);
  const [timeExecuted, setTimeExecuted] = useState([]);
  const [timeCloseExecuted, setTimeCloseExecuted] = useState([]);
  const [generoList, setGeneroList] = useState([]);
  const [generoListInfo, setGeneroListInfo] = useState([]);
  const [isSubGenreList, setIsSubGenreList] = useState(false);
  const [heapList, setHeapList] = useState([]);
  const [heapListWS, setHeapListWS] = useState([]);
  const [genreIdsSelect, setGenreIdsSelect] = useState([]);
  const [genreIndexEffect, setGenreIndexEffect] = useState(-1);
  const [genreIdsSelectEffect, setGenreIdsSelectEffect] = useState([]);
  const [musicsGenresSelect, setMusicsGenresSelect] = useState([]);
  const [genrePendent, setGenrePendent, genrePendentRef] = useState([]);

  const [genreSubIdsSelect, setGenreSubIdsSelect] = useState([]);
  const [genreSubIndexEffect, setGenreSubIndexEffect] = useState(-1);
  const [genreSubIdsSelectEffect, setGenreSubIdsSelectEffect] = useState([]);
  const [musicsGenreSubSelect, setMusicsGenreSubSelect] = useState([]);
  const [genreSubPendent, setGenreSubPendent] = useState([]);
  const [listMusicComplete, setListMusicComplete] = useState([]);

  const [genreIdsSelectEffectOK, setGenreIdsSelectEffectOK] = useState([]);
  const [genreSubList, setGenreSubList] = useState([]);
  const [coverLists, setCoverLists] = useState([]);
  const [contentListArray, setContentListArray] = useState([]);
  const [favoritedsList, setFavoritedsList] = useState([]);
  const [blockedsList, setBlockedsList] = useState([]);
  const [playList, setPlatList] = useState([]);
  const [config, setConfig] = useState(null);
  const [showConfirm, setShowConfirm] = useState(false);
  const [isSearch, setIsSearch] = useState(false);
  const [contentSelectChannel, setContentSelectChannel] = useState([]);
  const [showContentChannel, setShowContentChannel] = useState(false);
  const [showModalNewPlayList, setShowModalNewPlayList] = useState(false);
  const [showModalNewEmployee, setShowModalNewEmployee] = useState(false);
  const [showModalEditPlayList, setShowModalEditPlayList] = useState(false);
  const [showListMusicView, setShowListMusicView] = useState(false);
  const [showPlayerView, setShowPlayerView] = useState(false);
  const [showModalSpeech, setShowModalSpeech] = useState(false);
  const [showModalOnline, setShowModalOnline] = useState(false);
  const [showModalConfigConteudo, setShowModalConfigConteudo] = useState(false);
  const [indexValuesPreviewList, setIndexValuesPreviewList] = useState(null);
  const [playingSpeech, setPlayingSpeech, playingSpeechRef] = useState(false);
  const [isShowMenu, setIsShowMenu] = useState(false);
  const [isPlayList, setIsPlayList] = useState(false);
  const [isPlayListSub, setIsPlayListSub] = useState(false);
  const [isPlayAuto, setIsPlayAuto] = useState(false);
  const [isPauseAuto, setIsPauseAuto] = useState(false);
  const [isUpdateListMusic, setIsUpdateListMusic] = useState(false);
  const [audioQualityId, setAudioQualityId, audioQualityIdRef] = useState(0);
  const [generoSelect, setGeneroSelect, generoSelectRef] = useState(null);
  const [generoSelectUpdates, setGeneroSelectUpdate, generoSelectRefUpdate] =
    useState(null);
  const [
    isGeneroSelectUpdates,
    setIsGeneroSelectUpdate,
    isGeneroSelectRefUpdate,
  ] = useState(false);
  const [generoSelectTemp, setGeneroSelectTemp, generoSelectTempRef] =
    useState(null);
  const [numMusicList, setNumMusicList, numMusicListRef] = useState(null);
  const [numMusicListPassed, setNumMusicListPassed] = useState(null);
  const [deletePlayListId, setDeletePlayListId] = useState(0);
  const [valueRandom, setValueRandom] = useState(
    Math.floor(Math.random() * (5 - 1) + 1)
  );
  const [listExecutId, setListExecutId] = useState(0);
  const [editPlayList, setEditPlayList] = useState(null);
  const keyaccess = getParam("profile");
  const [client, setClient] = useState("");
  const [commercials, setCommercials] = useState(null);
  const [timeClose, setTimeClose] = useState("");
  const [genreVozHoraCerta, setGenreVozHoraCerta] = useState("male");
  const [channelSetting, setChannelSetting] = useState(null);
  const [vignettes, setVignettes] = useState(null);
  const [institucional, setInstitucional] = useState(null);
  const [sazonal, setSazonal] = useState(null);
  const [content, setContent] = useState(null);
  const [groupSelect, setGroupSelect] = useState("Com Direitos");
  const [typeGroupSelect, setTypeGroupSelect] = useState("sugeridas");
  const [socketConnected, setSocketConnected] = useState(false);
  const [showModalBlock, setShowModalBlock] = useState(false);
  const [showModalBlockExpired, setShowModalBlockExpired] = useState(false);
  const [showModalListGenre, setShowModalListGenre] = useState(false);
  const [isOnline, setIsOnline] = useState(false);
  const [isContinue, setIsContinue] = useState(false);
  const [showSuportContact, setShowSuportContact] = useState(false);
  const [isLoadingGenre, setIsLoadingGenre] = useState(false);
  const [showSuporteWhatsapp, setShowSuporteWhatsapp] = useState(false);
  const [arrayChannelOnline, setArrayChannelOnline] = useState("");
  const [arrayChannelOnlineOld, setArrayChannelOnlineOld] = useState("");
  const [functionPlay, setFunctionPlay] = useState({});
  const [callPlay, setCallPlay] = useState({});
  const [previousSelectGenre, setPreviousSelectGenre] = useState({});
  const [previousSelectSubGenre, setPreviousSelectSubGenre] = useState({});
  const [playingAuto, setPlayingAuto] = useState("");
  const [playingNow, setPlayingNow] = useState("");
  const [listMusic, setListMusic] = useState({});
  const [listMusicRemoto, setListMusicRemoto] = useState({});
  const [listSegmentsCovers, setListSegmentsCovers] = useState([]);
  const [listSegments, setListSegments] = useState([]);
  const [listPhrases, setListPhrases] = useState([]);
  const [previewLists, setPreviewLists] = useState([]);
  const [previewListsUpdates, setPreviewListsUpdates] = useState([]);
  const [indexForContent, setIndexForContent] = useState(-1);
  const [isMobile, setIsMobile] = useState("null");
  const [audio, setAudio] = useState(new Audio(""));
  const [audioHoraCerta, setAudioHoraCerta] = useState(new Audio(""));
  const [isAudioHoraCerta, setIsAudioHoraCerta] = useState(false);
  const [typeAudioHoraCerta, setTypeAudioHoraCerta] = useState(-1);
  const [limitSubGenreList, setLimitSubGenreList] = useState(0);
  const [scrollPosition, setSrollPosition] = useState(0);
  const [countConnected, setCountConnected] = useState(0);
  const typeSubListMobile = [
    { sub: "Nacional" },
    { sub: "Internacional" },
    { sub: "Forró" },
    { sub: "Sertanejo" },
    { sub: "Gospel" },
    { sub: "Gaúcho" },
    { sub: "Variedades" },
    { sub: "BPM" },
    { sub: "Country" },
    { sub: "Dance & EDM" },
    { sub: "Reggae" },
    { sub: "Pop & Rock" },
    { sub: "Pagode" },
    { sub: "Jazz" },
    { sub: "Blues" },
    { sub: "MPB" },
    { sub: "Rock" },
    { sub: "Indie" },
    { sub: "Brasil" },
    { sub: "R&B" },
    { sub: "Instrumental" },
    { sub: "EQUAL" },
  ];

  

  const capaIsMobile = {
    copyright: "/static/images/channel/copyright.jpg",
    nocopyright: "/static/images/channel/nocopyright.jpg",
    playlist: "/static/images/channel/playlist.jpg",
  };

  //busca View Genre e GenreSub JSON
  useEffect(() => {
    getGenres();
  }, []);

  const getGenres = async () => {
    const arrayD = await api.get("/view/destaque");
    const arrayG = await api.get("/view/genre");
    const arrayGS = await api.get("/view/genresub");

    setTypeSugListMobile(arrayD.data);
    genresubDataArray = arrayGS.data;
    genreDataArray = arrayG.data;
  };

  useEffect(() => {
    if (detectar_mobile()) {
      setIsMobile("true");
    } else {
      setIsMobile("false");
    }
  }, []);

  useEffect(() => {
    console.log(genrePendent);
  }, [genrePendent]);

  useEffect(() => {
    setInterval(() => {
      if (timeClose != "") {
        for (let index = 0; index < JSON.parse(timeClose).length; index++) {
          const element = JSON.parse(timeClose)[index];

          if (element.time == moment().format("HH:mm")) {
            if (
              timeCloseExecuted.filter((t) => t == element.time).length == 0
            ) {
              let data = timeCloseExecuted;
              data.push(element.time);
              setTimeCloseExecuted(data);

              window.close();
            }
          }
        }
      }
    }, 1000);
  }, [timeClose, timeCloseExecuted]);

  useEffect(() => {
    let dia = moment().format("ddd");
    console.log(
      dia == "seg"
        ? "mon"
        : dia == "ter"
        ? "tue"
        : dia == "qua"
        ? "wed"
        : dia == "qui"
        ? "thu"
        : dia == "sex"
        ? "fri"
        : dia == "sáb"
        ? "sat"
        : dia == "dom"
        ? "sun"
        : dia
    );
  }, []);

  useEffect(() => {
    setInterval(() => {
      if (commercialTimeExecut.length > 0) {
        for (let index = 0; index < commercialTimeExecut.length; index++) {
          const element = commercialTimeExecut[index];
          let dia = moment().format("ddd");
          if (
            element.Weekdayrules[0][
              String(
                dia == "seg"
                  ? "mon"
                  : dia == "ter"
                  ? "tue"
                  : dia == "qua"
                  ? "wed"
                  : dia == "qui"
                  ? "thu"
                  : dia == "sex"
                  ? "fri"
                  : dia == "sáb"
                  ? "sat"
                  : dia == "dom"
                  ? "sun"
                  : dia
              )
            ] == "true"
          ) {
            for (let index = 0; index < element.Hours.length; index++) {
              const elementH = element.Hours[index];

              if (elementH.startHour == moment().format("HH:mm")) {
                if (
                  timeExecuted.filter((t) => t == elementH.startHour).length ==
                  0
                ) {
                  let data = timeExecuted;
                  data.push(elementH.startHour);
                  setTimeExecuted(data);

                  onSetPlayingSpeech(true);
                  audio.pause();
                  audio.src = "";
                  audio.volume = 0.5;
                  //

                  //setPlayingAuto(false);
                  setAudio(new Audio(element.path_s3));
                }
              }
            }
          }
        }
      }
    }, 1000);
  }, [commercialTimeExecut, timeExecuted]);

  useEffect(() => {
    if (countConnected == 0) {
      //alert(contConected)
      // contConected = contConected + 1;
      setCountConnected(1);
      socket.on("connect", () => setSocketConnected(true));

      socket.on("previousChannelOnline", (onlines) => {
        console.log(onlines);
        setArrayChannelOnline(onlines);
      });
    }
  }, []);

  useEffect(() => {
    /* if (contConected == 0) {
        alert(contConected)
        socket.on("connect", () => setSocketConnected(true));

         socket.on("previousChannelOnline", (onlines) => {
          console.log(onlines);
          setArrayChannelOnline(onlines);
        }); 

      } */

    console.log(isMobile);

    if ((isMobile == "true") & (client != "")) {
      /* socket.on("previousChannelOnline", (onlines) => {
          console.log(onlines);
          setArrayChannelOnline(onlines);
        }); */

      socket.on("previousListMusic", (list) => {
        // setListMusicRemoto(list);
      });

      socket.on("receivedListMusic", (onlines) => {
        onGetListExecut();
      });
    }

    if ((isMobile == "false") & (client != "")) {
      /* socket.on("previousChannelOnline", (onlines) => {
          console.log(onlines);
          setArrayChannelOnline(onlines);
        });  */

      socket.on("previousPlayMusic", (list) => {
        arrayFunctionPlay = list;
        setFunctionPlay(list);
      });

      socket.on("previousCallPlay", (list) => {
        arrayCallPlayRemoto = list;
        setCallPlay(list);
      });

      socket.on("previousListMusic", (list) => {
        console.log(list);
        if (list.musics) {
          // setListMusic(list);
        }
      });

      socket.on("receivedPlayMusic", (functionsPlays) => {
        //console.log(functionsPlays);
        if (
          !(
            JSON.stringify(arrayFunctionPlay) ==
            JSON.stringify({ ...arrayFunctionPlay, ...functionsPlays })
          )
        ) {
          arrayFunctionPlay = { ...arrayFunctionPlay, ...functionsPlays };
          //console.log(functionsPlays);

          setFunctionPlay(arrayFunctionPlay);
        }
      });

      socket.on("receivedCall", (callPlays) => {
        //console.log(arrayChannelOnline);
        if (
          !(
            JSON.stringify(arrayCallPlayRemoto) ==
            JSON.stringify({ ...arrayCallPlayRemoto, ...callPlays })
          )
        ) {
          arrayCallPlayRemoto = { ...arrayCallPlayRemoto, ...callPlays };
          //console.log(functionsPlays);

          setCallPlay(arrayCallPlayRemoto);
        }
      });

      socket.on("previousSelectGenre", (list) => {
        console.log(list);
        setPreviousSelectGenre(list);
      });

      socket.on("receivedSelectGenre", (list) => {
        console.log(list);
        if (
          !(
            JSON.stringify(arrayPreviousSelectGenre) ==
            JSON.stringify({ ...arrayPreviousSelectGenre, ...list })
          )
        ) {
          arrayPreviousSelectGenre = { ...arrayPreviousSelectGenre, ...list };
          //console.log(functionsPlays);

          setPreviousSelectGenre(arrayPreviousSelectGenre);
        }
      });

      socket.on("previousSelectSubGenre", (list) => {
        console.log(list);
        setPreviousSelectSubGenre(list);
      });

      socket.on("receivedSelectSubGenre", (list) => {
        console.log(list);
        if (
          !(
            JSON.stringify(arrayPreviousSelectSubGenre) ==
            JSON.stringify({ ...arrayPreviousSelectSubGenre, ...list })
          )
        ) {
          arrayPreviousSelectSubGenre = {
            ...arrayPreviousSelectSubGenre,
            ...list,
          };
          //console.log(functionsPlays);

          setPreviousSelectSubGenre(arrayPreviousSelectSubGenre);
        }
      });
    }
  }, [isMobile, client]);

  useEffect(() => {
    if (socketConnected) {
      if (client != "") {
        socket.emit("ChannelConnected", { clientId: client.id });
      }
    }
  }, [socketConnected]);

  useEffect(() => {
    if (
      Object.keys(previousSelectGenre).filter((key) => {
        return key == client.id;
      }).length > 0
    ) {
      if (!previousSelectGenre[String(client.id)].exulted) {
        if (heapList.length > 0) {
          let list = previousSelectGenre[String(client.id)].genre;
          console.log(list);
          if (genreIdsSelect.filter((g) => g == list.id).length > 0) {
            if (genreIdsSelect.length === 1) {
              showMessage(
                "Seleção de Generos",
                "Para você não ficar sem nenhuma musica em sua lista, favor selecione um genero antes de desmarcar este.",
                "warning"
              );
            } else {
              // setIsLoadingGenre(true);
              let dataSelect = genreIdsSelectEffect.filter(
                (gi, index) => index > -1
              );
              dataSelect.push(list);

              setGenreIdsSelectEffect(dataSelect);

              setGenreIdsSelect(genreIdsSelect.filter((g) => g != list.id));

              setGenrePendent(
                heapList[0].Rights.filter(
                  (r) => r.id == list.rightId
                )[0].Genres.filter((g) => g.id == genreIdsSelect[0])[0]
              );

              setMusicsGenresSelect(
                musicsGenresSelect.filter((m) => m.genreId != list.id)
              );
              //setIsLoadingGenre(false);
            }
          } else {
            let dataSelect = genreIdsSelectEffect.filter(
              (gi, index) => index > -1
            );
            dataSelect.push(list);
            setGenreIdsSelectEffect(dataSelect);
            // setGenreIndexEffect(genreIndexEffect + 1)
            // setIsLoadingGenre(true);
            let arrayGenreId = genreIdsSelect;
            arrayGenreId.push(list.id);
            setGenreIdsSelect(arrayGenreId);
            setGenrePendent(list);
            onSumMusic(list);
            //onGeneroSelect(list);
          }

          /* onGeneroSelect(
            GenreWS
          ); */

          socket.emit("SendInfoSelectGenre", {
            clientId: client.id,
            genre: previousSelectGenre[String(client.id)].genre,
            exulted: true,
          });
        }
      }
    }
  }, [previousSelectGenre]);

  useEffect(() => {
    if (
      Object.keys(previousSelectSubGenre).filter((key) => {
        return key == client.id;
      }).length > 0
    ) {
      if (!previousSelectSubGenre[String(client.id)].exulted) {
        onSubSelect(previousSelectSubGenre[String(client.id)].genre);

        socket.emit("SendInfoSelectSubGenre", {
          clientId: client.id,
          genre: previousSelectSubGenre[String(client.id)].genre,
          exulted: true,
        });
      }
    }
  }, [previousSelectSubGenre]);

  useEffect(() => {
    console.log([{ cliente: client, isContinue: isContinue }]);
    if (
      1 > 2
      /* (Object.keys(listMusic).filter((key) => {
        console.log(`${key} e ${client.id}`);
        return key == client.id;
      }).length >
        0) &
      isContinue */
    ) {
      const data = listMusic[String(client.id)];

      setGenreIdsSelect(data.genreIdsSelect);
      setGeneroSelect(data.musics);
      setIsPlayListSub(data.isPlayListSub);
      setIsPlayList(data.isPlayList);
      setNumMusicListPassed(numMusicList);
      setNumMusicList(data.numMusicList);
      //backMusic();
      // nextMusic();
    }
  }, [listMusic, client, isContinue]);

  useEffect(() => {
    if (client != "") {
      if (
        Object.keys(functionPlay).filter((key) => {
          return key == client.id;
        }).length > 0
      ) {
        const data =
          functionPlay[
            String(
              Object.keys(functionPlay).filter((key) => key == client.id)[0]
            )
          ];

        console.log(data);

        if ((data.function == "backMusic") & !data.exulted) {
          backMusic();
          socket.emit("SendInfoPlayRealTime", {
            clientId: client.id,
            function: "backMusic",
            exulted: true,
          });
        } else if ((data.function == "nextMusic") & !data.exulted) {
          nextMusic();
          socket.emit("SendInfoPlayRealTime", {
            clientId: client.id,
            function: "nextMusic",
            exulted: true,
          });
        } else if (data.function == "closeGuia") {
          if (!data.exulted) {
            socket.emit("SendInfoPlayRealTime", {
              clientId: client.id,
              function: "closeGuia",
              exulted: true,
            });

            window.close();
          }
        } else if (data.function == "refreshGuia") {
          if (!data.exulted) {
            socket.emit("SendInfoPlayRealTime", {
              clientId: client.id,
              function: "refreshGuia",
              exulted: true,
            });

            window.location.reload();
          }
        } else if (data.function == "playMusic") {
          if (generoSelect) {
            setPlayingNow(!playingNow);
          }

          socket.emit("SendInfoPlayRealTime", {
            clientId: client.id,
            function: "playMusic",
            exulted: true,
          });
        } else if (data.function == "getBlock") {
          onGetBlockeds();

          socket.emit("SendInfoPlayRealTime", {
            clientId: client.id,
            function: "getBlock",
            exulted: true,
          });
        } else if (data.function == "getFavorited") {
          onGetFavoriteds();

          socket.emit("SendInfoPlayRealTime", {
            clientId: client.id,
            function: "getFavorited",
            exulted: true,
          });
        }
      }
    }
  }, [functionPlay]);

  useEffect(() => {
    if (client != "") {
      if (
        Object.keys(callPlay).filter((key) => {
          console.log(`${key} e ${client.id}`);
          return key == client.id;
        }).length > 0
      ) {
        const data =
          callPlay[
            String(Object.keys(callPlay).filter((key) => key == client.id)[0])
          ];

        if (!data.exulted) {
          onCallManual(data.call);

          socket.emit("SendInfoCallRealTime", {
            clientId: client.id,
            call: data.call,
            exulted: true,
          });
        }
      }
    }
  }, [callPlay]);

  useEffect(() => {
    if (isOnline) {
      setShowModalOnline(true);
    }
  }, [isOnline]);

  useEffect(() => {
    if ((arrayChannelOnline != "") & (JSON.stringify(client).length > 4)) {
      const isTrue =
        Object.keys(arrayChannelOnline).filter((key) => {
          return key == client.id;
        }).length > 0
          ? arrayChannelOnline[
              String(
                Object.keys(arrayChannelOnline).filter((key) => {
                  return key == client.id;
                })[0]
              )
            ].status == "online"
            ? true
            : false
          : false;

      const isTrueOld =
        arrayChannelOnlineOld == ""
          ? true
          : Object.keys(arrayChannelOnlineOld).filter((key) => {
              return key == client.id;
            }).length > 0
          ? arrayChannelOnlineOld[
              String(
                Object.keys(arrayChannelOnlineOld).filter((key) => {
                  return key == client.id;
                })[0]
              )
            ].status == "online"
            ? true
            : true
          : false;

      setIsOnline(
        (isTrue == isTrueOld) & !(arrayChannelOnlineOld == "") ? false : isTrue
      );
      const isTrueVerified =
        (isTrue == isTrueOld) & !(arrayChannelOnlineOld == "") ? false : isTrue;

      if (isTrueVerified) {
        setIsContinue(false);
      } else {
        setArrayChannelOnlineOld(arrayChannelOnline);

        socket.emit("ChannelConnected", { clientId: client.id });

        setInterval(() => {
          socket.emit("ChannelConnected", { clientId: client.id });
        }, 600000);

        if ((isTrue != isTrueOld) & (arrayChannelOnlineOld != "")) {
        } else {
          setIsContinue(true);
        }
      }

      onGetBlockeds();
      onGetFavoriteds();
    }
  }, [client, arrayChannelOnline]);

  useEffect(() => {
    (async () => {
      console.log("1");
      let resC = await api.get("/cover");
      let resContent = await api.get("/content");

      let resR = rightDataArray; //await api.get("/right");

      let arrayGenreSubCover = [];

      for (let index = 0; index < genresubDataArray.length; index++) {
        const element = genresubDataArray[index];

        arrayGenreSubCover.push({
          ...element,
          Covers: resC.data.filter((c) => c.genresubId == element.id),
          Rights: resR.filter((r) => r.id == element.rightId),
        });
      }

      setGenreSubList(arrayGenreSubCover);

      let arrayGenreCover = [];

      for (let index = 0; index < genreDataArray.length; index++) {
        const element = genreDataArray[index];

        arrayGenreCover.push({
          ...element,
          Covers: resC.data.filter((c) => c.genreId == element.id),
          Rights: resR.filter((r) => r.id == element.rightId),
        });
      }

      setGeneroList(arrayGenreCover);

      setCoverLists(resC.data);
      setContentListArray(resContent.data);
      //getClient();
    })();
  }, []);

  useEffect(() => {
    if (contentListArray.length > 0) {
      getClient();
    }
  }, [contentListArray]);

  useEffect(() => {
    console.log(isContinue);
    if (client.id) {
      getPlayList();
    }
  }, [isContinue, client]);

  useEffect(() => {
    if (isSearch) {
      setIsSearch(false);
      return;
    }

    if (generoSelect) {
      if (generoSelectRef.current) {
        setIsGeneroSelectUpdate(false);
        setGeneroSelectUpdate(null);
        return;
      }

      api
        .get(`/exultedmusic/last/${client.id}`)
        .then((resp) => resp.data)
        .then((response) => {
          if (response.length > 0) {
            if (isPlayList) {
              if (
                String(
                  generoSelect.Itemplaylists[response[0].positionList].Music
                    .title
                )
                  .replaceAll(" ", "")
                  .replace(".mp3", "")
                  .replace(".m4a", "")
                  .toUpperCase() !=
                String(response[0].name)
                  .split("|")[1]
                  .replaceAll(" ", "")
                  .toUpperCase()
              ) {
                setNumMusicListPassed(numMusicList);
                //setShowListMusicView(!showListMusicView);
                setNumMusicList(0);
                setShowPlayerView(true);
                return;
              }
            } else {
              if (
                String(generoSelect.Musics[response[0].positionList].title)
                  .replaceAll(" ", "")
                  .replace(".mp3", "")
                  .replace(".m4a", "")
                  .toUpperCase() !=
                String(response[0].name)
                  .split("|")[1]
                  .replaceAll(" ", "")
                  .toUpperCase()
              ) {
                setNumMusicListPassed(numMusicList);
                setNumMusicList(0);
                //setShowListMusicView(!showListMusicView);
                setShowPlayerView(true);

                return;
              }
            }

            console.log(
              response[0].positionList + 1 >
                (isPlayList
                  ? generoSelect.Itemplaylists.length
                  : generoSelect.Musics.length) -
                  1
                ? 0
                : response[0].positionList + 1
            );

            setNumMusicListPassed(numMusicList);
            setNumMusicList(
              response[0].positionList + 1 >
                (isPlayList
                  ? generoSelect.Itemplaylists.length
                  : generoSelect.Musics.length) -
                  1
                ? 0
                : response[0].positionList + 1
            );
            // setShowListMusicView(!showListMusicView);
            setShowPlayerView(true);
          } else {
            setNumMusicListPassed(numMusicList);
            setNumMusicList(0);
            //setShowListMusicView(!showListMusicView);
            setShowPlayerView(true);
          }
        })
        .catch((error) => {
          setNumMusicList(0);
          setShowPlayerView(true);
        });
    }
  }, [generoSelect]);

  useEffect(() => {
    if (editPlayList) {
      playList.map((list) => {
        if (list.id == editPlayList.id) {
          setEditPlayList(list);
        }
      });
    }
  }, [playList]);

  useEffect(() => {
    if (client.id) {
      getHeap();
    }
  }, [isContinue, client]);

  useEffect(() => {
    if ((heapListWS.length > 0) & (listExecutId > 0)) {
      if (listExecutId > 0) {
        api
          .put(`/listexecut/edit/${listExecutId}`, {
            music: JSON.stringify(generoSelect),
            isPlayList: String(isPlayList),
            isPlayListSub: String(isPlayListSub),
            numMusicList: numMusicList,
            playingAuto: String(playingAuto),
            genreIdsSelect: JSON.stringify(genreIdsSelect),
            genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
          })
          .then((response) => response.data)
          .then((resp) => {
            if (resp.message == "success") {
              socket.emit("SendInfoListMusic", {
                clientId: client.id,
                music: [],
                isPlayList: isPlayList,
                isPlayListSub: isPlayListSub,
                numMusicList: numMusicList,
                playingAuto: playingAuto,
                genreIdsSelect: [],
              });
            }
          })
          .catch((error) => {
            console.log(error);
            //showMessage("Error", "Erro, consulte o suporte", "danger");
          });
      } else {
        api
          .post(`/listexecut/new`, {
            clientId: client.id,
            music: JSON.stringify(generoSelect),
            isPlayList: String(isPlayList),
            isPlayListSub: String(isPlayListSub),
            numMusicList: numMusicList,
            playingAuto: String(playingAuto),
            genreIdsSelect: JSON.stringify(genreIdsSelect),
            genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
          })
          .then((response) => response.data)
          .then((resp) => {
            if (resp.message == "success") {
              setListExecutId(resp.data.id);

              setIsPlayList(false);
              setIsPlayListSub(false);
              setNumMusicList(0);
              setGenreIdsSelect([]);
              setGenreSubIdsSelect([87]);

              onSubSelect(
                {
                  id: 87,
                  name: "Hot Lançamentos",
                  typeSegment: "todos",
                  rightId: 1,
                  createdAt: "2021-07-12 21:39:29",
                  updatedAt: "2021-07-12 21:39:29",
                  RightId: 1,
                  countMusic: 153,
                },
                null,
                true,
                true
              );
            }
          })
          .catch((error) => {
            console.log(error);
            //showMessage("Error", "Erro, consulte o suporte", "danger");
          });
      }
    }
  }, [heapListWS, listExecutId]);

  useEffect(() => {
    if ((client != "") & (listExecutId > 0)) {
      if (listExecutId > 0) {
        setTimeout(() => {
          api
            .put(`/listexecut/edit/${listExecutId}`, {
              music: JSON.stringify(generoSelect),
              isPlayList: String(isPlayList),
              isPlayListSub: String(isPlayListSub),
              numMusicList: numMusicList,
              playingAuto: String(playingAuto),
              genreIdsSelect: JSON.stringify(genreIdsSelect),
              genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
            })
            .then((response) => response.data)
            .then((resp) => {
              if (resp.message == "success") {
                socket.emit("SendInfoListMusic", {
                  clientId: client.id,
                  music: [],
                  isPlayList: isPlayList,
                  isPlayListSub: isPlayListSub,
                  numMusicList: numMusicList,
                  playingAuto: playingAuto,
                  genreIdsSelect: [],
                });
                //setShowPlayerView(true);
              }
            })
            .catch((error) => {
              console.log(error);
              // showMessage("Error", "Erro, consulte o suporte", "danger");
            });
        }, 200);
      } else {
        api
          .post(`/listexecut/new`, {
            clientId: client.id,
            music: JSON.stringify(generoSelect),
            isPlayList: String(isPlayList),
            isPlayListSub: String(isPlayListSub),
            numMusicList: numMusicList,
            playingAuto: String(playingAuto),
            genreIdsSelect: JSON.stringify(genreIdsSelect),
            genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
          })
          .then((response) => response.data)
          .then((resp) => {
            if (resp.message == "success") {
              setListExecutId(resp.data.id);

              setIsPlayList(false);
              setIsPlayListSub(false);
              setNumMusicList(0);
              setGenreIdsSelect([]);
              setGenreSubIdsSelect([87]);

              onSubSelect(
                {
                  id: 87,
                  name: "Hot Lançamentos",
                  typeSegment: "todos",
                  rightId: 1,
                  createdAt: "2021-07-12 21:39:29",
                  updatedAt: "2021-07-12 21:39:29",
                  RightId: 1,
                  countMusic: 153,
                },
                null,
                true,
                true
              );
            }
          })
          .catch((error) => {
            console.log(error);
            //showMessage("Error", "Erro, consulte o suporte", "danger");
          });
      }
    }
  }, [numMusicList, listExecutId]);

  useEffect(() => {
    if ((client != "") & (listExecutId > 0)) {
      if (listExecutId > 0) {
        api
          .put(`/listexecut/edit/${listExecutId}`, {
            music: JSON.stringify(generoSelect),
            isPlayList: String(isPlayList),
            isPlayListSub: String(isPlayListSub),
            numMusicList: numMusicList,
            playingAuto: String(playingAuto),
          })
          .then((response) => response.data)
          .then((resp) => {
            if (resp.message == "success") {
              socket.emit("SendInfoListMusic", {
                clientId: client.id,
                music: [],
                isPlayList: isPlayList,
                isPlayListSub: isPlayListSub,
                numMusicList: numMusicList,
                playingAuto: playingAuto,
                genreIdsSelect: [],
              });
            }
          })
          .catch((error) => {
            console.log(error);
            // showMessage("Error", "Erro, consulte o suporte", "danger");
          });
      } else {
        api
          .post(`/listexecut/new`, {
            clientId: client.id,
            music: JSON.stringify(generoSelect),
            isPlayList: String(isPlayList),
            isPlayListSub: String(isPlayListSub),
            numMusicList: numMusicList,
            playingAuto: String(playingAuto),
            genreIdsSelect: JSON.stringify(genreIdsSelect),
            genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
          })
          .then((response) => response.data)
          .then((resp) => {
            if (resp.message == "success") {
              setListExecutId(resp.data.id);

              setIsPlayList(false);
              setIsPlayListSub(false);
              setNumMusicList(0);
              setGenreIdsSelect([]);
              setGenreSubIdsSelect([87]);

              onSubSelect(
                {
                  id: 87,
                  name: "Hot Lançamentos",
                  typeSegment: "todos",
                  rightId: 1,
                  createdAt: "2021-07-12 21:39:29",
                  updatedAt: "2021-07-12 21:39:29",
                  RightId: 1,
                  countMusic: 153,
                },
                null,
                true,
                true
              );
            }
          })
          .catch((error) => {
            console.log(error);
            // showMessage("Error", "Erro, consulte o suporte", "danger");
          });
      }
    }
  }, [playingAuto, listExecutId]);

  /* aqui */
  useEffect(() => {
    if (client != "") {
      api
        .get(`/listexecut/${client.id}`)
        .then((response) => response.data)
        .then(async (resp) => {
          if (resp.length > 0) {
            console.log(resp);

            const getGenreSubTypeSegment = await api.get(
              `/genresub/typesegment`
            );

            let arrayGenreSub = getGenreSubTypeSegment.data.filter(
              (item) => String(item.typeSegment).toLowerCase() == "invisivel"
            );
            /* let arrayGenreSub = genresubDataArray.filter(
              (item) => String(item.typeSegment).toLowerCase() == "invisivel"
            ); */

            let musicDelete = [];

            for (let index = 0; index < arrayGenreSub.length; index++) {
              const element = arrayGenreSub[index];

              const getSubItem = await api.get(
                `/genresubitem/genresub/${element.id}`
              );

              musicDelete = [...musicDelete, ...getSubItem.data];
            }

            if (resp[0].music) {
              let selectMusicArray = JSON.parse(resp[0].music)
                ? JSON.parse(resp[0].music).Musics
                : [];
              setListMusicRemoto(resp[0]);
              setGenreIdsSelect(JSON.parse(resp[0].genreIdsSelect));
              setGenreSubIdsSelect(
                JSON.parse(resp[0].genreSubIdsSelect).filter(
                  (sub) =>
                    musicDelete.filter((d) => sub == d.genresubId).length == 0
                )
              );

              const selectMusicArrayFilter = selectMusicArray.filter(
                (m) =>
                  m &&
                  !(String(m.artist).indexOf("Comercial") > -1) &
                    !(String(m.artist).indexOf("Vinheta") > -1) &
                    !(String(m.artist).indexOf("Hora Certa") > -1) &
                    !(String(m.artist).indexOf("Conteúdo") > -1) &&
                  m
              );

              console.log(selectMusicArrayFilter);
              console.log(JSON.parse(resp[0].music));

              setMusicsGenresSelect(selectMusicArrayFilter);
              setMusicsGenreSubSelect(
                selectMusicArrayFilter.map((s) => {
                  return { Music: s };
                })
              );

              console.log(JSON.parse(resp[0].music));
              if (JSON.parse(resp[0].music)) {
                if (
                  Object.keys(JSON.parse(resp[0].music)).filter(
                    (key) => key == "Genresubitems"
                  ).length > 0
                ) {
                  const { Musics, ...G } = JSON.parse(resp[0].music);
                  onSubSelect(G, selectMusicArrayFilter, true);
                } else if (
                  Object.keys(JSON.parse(resp[0].music)).filter(
                    (key) => key == "Itemplaylists"
                  ).length > 0
                ) {
                  onPlaylistSelect(JSON.parse(resp[0].music));
                } else {
                  const { Musics, ...G } = JSON.parse(resp[0].music);
                  onGeneroSelect(G, selectMusicArrayFilter, true);
                }
              } else {
                const G = JSON.parse(resp[0].genreIdsSelect)
                  ? JSON.parse(resp[0].genreIdsSelect).length > 0
                    ? genreDataArray.filter(
                        (gen) => gen.id == JSON.parse(resp[0].genreIdsSelect)[0]
                      )[0]
                    : null
                  : null;

                if (G) {
                  onGeneroSelect(G);
                }

                const GS = JSON.parse(resp[0].genreSubIdsSelect)
                  ? JSON.parse(resp[0].genreSubIdsSelect).length > 0
                    ? genresubDataArray.filter(
                        (gen) =>
                          gen.id == JSON.parse(resp[0].genreSubIdsSelect)[0]
                      )[0]
                    : null
                  : null;

                if (GS) {
                  onSubSelect(GS);
                }
              }

              setIsPlayListSub(JSON.parse(resp[0].isPlayListSub));
              setIsPlayList(JSON.parse(resp[0].isPlayList));
              setListExecutId(resp[0].id);
              // backMusic();
            } else {
              setListExecutId(resp[0].id);
              const G = JSON.parse(resp[0].genreIdsSelect)
                ? JSON.parse(resp[0].genreIdsSelect).length > 0
                  ? genreDataArray.filter(
                      (gen) => gen.id == JSON.parse(resp[0].genreIdsSelect)[0]
                    )[0]
                  : null
                : null;

              if (G) {
                onGeneroSelect(G);
              } else {
                const GS = JSON.parse(resp[0].genreSubIdsSelect)
                  ? JSON.parse(resp[0].genreSubIdsSelect).length > 0
                    ? genresubDataArray.filter(
                        (gen) =>
                          gen.id == JSON.parse(resp[0].genreSubIdsSelect)[0]
                      )[0]
                    : null
                  : null;

                if (GS) {
                  onSubSelect(GS);
                }
              }
            }
          } else {
            api
              .post(`/listexecut/new`, {
                clientId: client.id,
                music: generoSelect,
                isPlayList: String(isPlayList),
                isPlayListSub: String(isPlayListSub),
                numMusicList: numMusicList,
                playingAuto: String(playingAuto),
                genreIdsSelect: JSON.stringify(genreIdsSelect),
                genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
              })
              .then((response) => response.data)
              .then((resp) => {
                if (resp.message == "success") {
                  setListExecutId(resp.data.id);

                  setIsPlayList(false);
                  setIsPlayListSub(false);
                  setNumMusicList(0);
                  setGenreIdsSelect([]);
                  setGenreSubIdsSelect([87]);

                  onSubSelect(
                    {
                      id: 87,
                      name: "Hot Lançamentos",
                      typeSegment: "todos",
                      rightId: 1,
                      createdAt: "2021-07-12 21:39:29",
                      updatedAt: "2021-07-12 21:39:29",
                      RightId: 1,
                      countMusic: 153,
                    },
                    null,
                    true,
                    true
                  );
                }
              })
              .catch((error) => {
                console.log(error);
                // showMessage("Error", "Erro, consulte o suporte", "danger");
              });
          }
        })
        .catch((error) => {
          console.log(error);
          //showMessage("Error", "Erro, consulte o suporte", "danger");
        });
    }
  }, [client]);

  useEffect(() => {
    if (listSegments.length > 0) {
      setIndexForContent(0);
    }
  }, [listSegments]);

  useEffect(() => {
    if (indexForContent > -1) {
      if (indexForContent < listSegments.length) {
        const element = listSegments[indexForContent];

        getContentSegment(element.segmentId);
      } else {
        setIndexForContent(-1);
      }
    }
  }, [indexForContent]);

  const onGetListExecut = () => {
    if (client != "") {
      api
        .get(`/listexecut/${client.id}`)
        .then((response) => response.data)
        .then((resp) => {
          if (resp.length > 0) {
            setListMusicRemoto(resp[0]);
          }
        })
        .catch((error) => {
          console.log(error);
          //showMessage("Error", "Erro, consulte o suporte", "danger");
        });
    }
  };

  const shuffle = (o) => {
    for (
      var j, x, i = o.length;
      i;
      j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x
    );
    return o;
  };

  useEffect(() => {
    if (audio) {
      play();
    }
  }, [audio]);

  const play = () => {
    audio.play();
    audio.volume = 1;
    audio.onended = () => {
      stop();
    };
  };

  const stop = () => {
    onSetPlayingSpeech(false);
  };

  const onCallManual = (call) => {
    console.log(call);

    const onNewFile = (urlMusic) => {
      onSetPlayingSpeech(true);
      audio.pause();
      audio.src = "";

      setPlayingAuto(false);
      setAudio(new Audio(urlMusic));
    };

    api
      .post(`/ibm/speech/azure/calls`, {
        text: call,
        voice: "fem",
      })
      .then((response) => response.data)
      .then((resp) => {
        console.log(resp);
        onNewFile(resp.data.path_s3);
      })
      .catch((error) => {
        console.log(error);
        showMessage("Error", "Erro, consulte o suporte", "danger");
      });
  };

  /* Hora Certa */
  var tentativas = 0;

  useEffect(() => {
    if (isAudioHoraCerta) {
      if (typeAudioHoraCerta == 1) {
        playHoraCerta(1);
      } else if (typeAudioHoraCerta == 2) {
        playHoraCerta(2);
      } else if (typeAudioHoraCerta == 3) {
        playHoraCerta(3);
      }
    }
  }, [audioHoraCerta, typeAudioHoraCerta, isAudioHoraCerta]);

  const playHoraCerta = (type) => {
    audioHoraCerta.play();
    audioHoraCerta.volume = 1;
    audioHoraCerta.onended = () => {
      if (type == 1) {
        setIsAudioHoraCerta(false);
        onHoraCertaHours();
      } else if (type == 2) {
        setIsAudioHoraCerta(false);
        onHoraCertaMinutes();
      } else if (type == 3) {
        setIsAudioHoraCerta(false);
        setTypeAudioHoraCerta(-1);
        stopHoraCerta();
        nextMusic();
      }
    };
  };

  const stopHoraCerta = () => {
    onSetPlayingSpeech(false);
  };

  const onHoraCertaPhrase = () => {
    const onNewFile = (urlMusic) => {
      onSetPlayingSpeech(true);
      audioHoraCerta.pause();
      audioHoraCerta.src = "";
      //

      //setPlayingAuto(false);
      setAudioHoraCerta(new Audio(urlMusic));
      setIsAudioHoraCerta(true);
    };

    api
      .get(`/times/phrase/${genreVozHoraCerta}`)
      .then((response) => response.data)
      .then((resp) => {
        setTypeAudioHoraCerta(1);
        onNewFile(resp.path_s3);
      })
      .catch((error) => {
        console.log(error);
        showMessage("Error", "Erro, consulte o suporte", "danger");
      });
  };

  const onHoraCertaHours = () => {
    const onNewFile = (urlMusic) => {
      onSetPlayingSpeech(true);
      audioHoraCerta.pause();
      audioHoraCerta.src = "";
      //

      //setPlayingAuto(false);
      setAudioHoraCerta(new Audio(urlMusic));
      setIsAudioHoraCerta(true);
    };

    api
      .get(
        `/times/horas/${
          moment().format("HH") == "00"
            ? "MEIANOITE"
            : String(moment().format("HH")).substr(0, 1) == "0"
            ? String(moment().format("HH")).substr(1, 1)
            : moment().format("HH")
        }/${genreVozHoraCerta}`
      )
      .then((response) => response.data)
      .then((resp) => {
        setTypeAudioHoraCerta(2);
        onNewFile(resp.path_s3);
      })
      .catch((error) => {
        console.log(error);
        showMessage("Error", "Erro, consulte o suporte", "danger");
      });
  };

  const onHoraCertaMinutes = () => {
    const onNewFile = (urlMusic) => {
      onSetPlayingSpeech(true);
      audioHoraCerta.pause();
      audioHoraCerta.src = "";
      //

      //setPlayingAuto(false);
      setAudioHoraCerta(new Audio(urlMusic));
      setIsAudioHoraCerta(true);
    };

    if (moment().format("mm") == "0") {
      stopHoraCerta();
      nextMusic();
    } else {
      api
        .get(
          `/times/minutos/${
            String(moment().format("mm")).substr(0, 1) == "0"
              ? String(moment().format("mm")).substr(1, 1)
              : moment().format("mm")
          }/${genreVozHoraCerta}`
        )
        .then((response) => response.data)
        .then((resp) => {
          setTypeAudioHoraCerta(3);
          onNewFile(resp.path_s3);
        })
        .catch((error) => {
          console.log(error);
          showMessage("Error", "Erro, consulte o suporte", "danger");
        });
    }
  };
  /* Fim Hora Certa */

  useEffect(() => {
    console.log(commercials);
  }, [commercials]);

  const getClient = () => {
    api
      .get(`/client/${keyaccess}`)
      .then(async (respCli) => {
        if (respCli.data) {
          if (respCli.data.length > 0) {
            const respB = await api.get(
              `/blocked/client/${respCli.data[0].id}`
            );
            const respP = await api.get(
              `/previewlist/client/${respCli.data[0].id}`
            );
            const respV = await api.get(
              `/vignette/client/${respCli.data[0].id}`
            );
            const respC = await api.get(
              `/commercial/client/${respCli.data[0].id}`
            );
            const respS = await api.get(
              `/segmentcontrol/client/${respCli.data[0].id}`
            );
            const respCS = await api.get(
              `/clientsetting/client/${respCli.data[0].id}`
            );
            const respCH = await api.get(
              `/channel/client/${respCli.data[0].id}`
            );

            setAudioQualityId(respCS.data[0].audioQualityId);

            const resp = {
              data: [
                {
                  ...respCli.data[0],
                  Previewlists: respP.data,
                  Vignettes: respV.data,
                  Commercials: respC.data,
                  Segmentcontrols: respS.data,
                  Clientsettings: respCS.data,
                  Blockeds: respB.data,
                  Channels: respCH.data,
                },
              ],
            };

            console.log(resp.data[0]);
            let segmentControlsArray = [];

            for (
              let indexS = 0;
              indexS < resp.data[0].Segmentcontrols.length;
              indexS++
            ) {
              const elementS = resp.data[0].Segmentcontrols[indexS];

              const resCovers = coverLists.filter(
                (c) => c.segmentId == elementS.segmentId
              ); /* await 
              api.get(
                `/cover/segment/${elementS.segmentId}`
              ); */

              const resContent = contentListArray.filter(
                (c) =>
                  (c.segmentId == elementS.segmentId) &
                  (c.audioQualityId == respCS.data[0].audioQualityId)
              ); /* await api.get(
                `/content/segment/${elementS.segmentId}`
              ); */

              console.log(
                resContent.filter((c) =>
                  moment().isBetween(
                    c.Days[0].startDate,
                    c.Days[0].dueDate,
                    undefined,
                    "[)"
                  )
                )
              );
              //dueDate
              //startDate

              segmentControlsArray.push({
                ...elementS,
                Segment: {
                  ...elementS.Segment,
                  Covers: resCovers,
                  Contents: resContent.filter((c) =>
                    moment().isBetween(
                      c.Days[0].startDate,
                      c.Days[0].dueDate,
                      undefined,
                      "[)"
                    )
                  ),
                },
              });
            }

            console.log(segmentControlsArray);
            if (resp.data[0].Segmentcontrols) {
              console.log(resp.data[0].Segmentcontrols);
              setListSegments(resp.data[0].Segmentcontrols);

              let arrayCoversSeg = [];

              for (
                let index = 0;
                index < resp.data[0].Segmentcontrols.length;
                index++
              ) {
                const element = resp.data[0].Segmentcontrols[index];

                const resCovers = coverLists.filter(
                  (c) => c.segmentId == element.Segment.id
                ); /*await  api.get(
                  `/cover/segment/${element.Segment.id}`
                ); */

                arrayCoversSeg = arrayCoversSeg.concat(shuffle(resCovers));
              }

              setListSegmentsCovers(arrayCoversSeg);

              api
                .get(`/segmentcontrol/phrase/${resp.data[0].id}`)
                .then((responsep) => responsep.data)
                .then((respp) => {
                  let phrases = [];

                  for (let index = 0; index < respp.length; index++) {
                    const element = respp[index];

                    if (element.Segment.Phrasesegments.length > 0) {
                      phrases.push(...element.Segment.Phrasesegments);
                    }
                  }

                  //console.log(phrases);
                  setListPhrases(shuffle(phrases));
                })
                .catch((error) => {
                  console.log(error);
                  showMessage("Error", "Erro, consulte o suporte", "danger");
                });
            }

            setContentSelectChannel(segmentControlsArray);
            setPreviewListsUpdates(
              JSON.parse(resp.data[0].Previewlists[0].preview).map((p) => {
                return { ...p, qtd: p.qtd == 0 ? -1 : 0, ended: false };
              })
            );

            setPreviewLists(JSON.parse(resp.data[0].Previewlists[0].preview));

            var dia = moment().format("ddd");

            setCommercials(
              resp.data[0].Commercials.filter(
                (Comercial) =>
                  (Comercial.Days.filter((day) =>
                    moment().isBetween(
                      `${day.startDate} 00:00:00`,
                      `${day.dueDate} 23:59:59`,
                      undefined,
                      "[]"
                    )
                  ).length >
                    0) &
                  (Comercial.Hours.length == 0) &
                  (Comercial.Weekdayrules[0][
                    String(
                      dia == "seg"
                        ? "mon"
                        : dia == "ter"
                        ? "tue"
                        : dia == "qua"
                        ? "wed"
                        : dia == "qui"
                        ? "thu"
                        : dia == "sex"
                        ? "fri"
                        : dia == "sáb"
                        ? "sat"
                        : dia == "dom"
                        ? "sun"
                        : dia
                    )
                  ] ==
                    "true")
              )
            );

            setCommercialTimeExecut(
              resp.data[0].Commercials.filter(
                (Comercial) =>
                  (Comercial.Days.filter((day) =>
                    moment().isBetween(
                      `${day.startDate} 00:00:00`,
                      `${day.dueDate} 23:59:59`,
                      undefined,
                      "[]"
                    )
                  ).length >
                    0) &
                  (Comercial.Hours.length > 0)
              )
            );

            setVignettes(resp.data[0].Vignettes);
            setChannelSetting(resp.data[0].Clientsettings[0]);

            if (resp.data[0].Clientsettings[0].timeClose) {
              setTimeClose(resp.data[0].Clientsettings[0].timeClose);
            }

            if (
              resp.data[0].Channels[0].Business.Plancontrols[0].status ==
              "ativo"
            ) {
              setClient(resp.data[0]);
            } else {
              setShowModalBlock(true);
            }

            console.log(
              moment(
                resp.data[0].Channels[0].Business.Plancontrols[0].updatedAt,
                "YYYY-MM-DD HH:mm:ss"
              )
                .add(
                  resp.data[0].Channels[0].Business.Plancontrols[0].Plan.days,
                  "days"
                )
                .format("YYYY-MM-DD HH:mm:ss")
            );

            if (
              moment(
                resp.data[0].Channels[0].Business.Plancontrols[0].updatedAt,
                "YYYY-MM-DD HH:mm:ss"
              )
                .add(
                  resp.data[0].Channels[0].Business.Plancontrols[0].Plan.days,
                  "days"
                )
                .diff(moment(), "days") < 1
            ) {
              setShowModalBlockExpired(true);
            }

            getMusicsList(respCS.data[0].audioQualityId);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getContentSegment = (id) => {
    let dataQ = content ? content : [];
    const dataC = contentListArray.filter((c) => c.segmentId == id);
    const dataCV = coverLists.filter((c) => c.segmentId == id);

    for (let index = 0; index < dataC.length; index++) {
      const element = {
        ...dataC[index],
        Covers: dataCV,
      };

      dataQ.push(element);
    }

    console.log(dataQ);
    setContent(shuffle(dataQ));
    setIndexForContent(indexForContent + 1);
  };

  const getHeap = () => {
    api
      .get("/heap")
      .then((resp) => {
        console.log(resp.data);
        if (resp.data) {
          const respR = { data: rightDataArray };
          if (respR.data) {
            console.log(respR.data);
            let arrayRight = [];
            let arrayHeap = [];

            let arraySubGenreWS = [];
            let arrayGenreWS = [];
            let arrayRightWS = [];
            let arrayHeapWS = [];

            console.log(genreSubList);
            console.log(generoList);

            /* Gera a lista de direitos para a radio */
            for (let index = 0; index < resp.data[0].Rights.length; index++) {
              const element = resp.data[0].Rights[index];

              arrayRight.push({
                ...element,
                Genres: generoList.filter((g) =>
                  client.Segmentcontrols.filter(
                    (sc) => String(sc.Segment.name).toLowerCase() == "academia"
                  ).length > 0
                    ? String(g.typeSegment).toLowerCase() == "academia"
                    : String(g.typeSegment).toLowerCase() == "todos" &&
                      g.rightId == element.id
                ),
                Genresubs: genreSubList.filter((g) =>
                  client.Segmentcontrols.filter(
                    (sc) => String(sc.Segment.name).toLowerCase() == "academia"
                  ).length > 0
                    ? String(g.typeSegment).toLowerCase() == "academia"
                    : String(g.typeSegment).toLowerCase() == "todos" &&
                      g.rightId == element.id
                ),
              });
            }

            /* Monta a lista para radio */
            for (let index = 0; index < resp.data.length; index++) {
              const element = resp.data[index];

              arrayHeap.push({
                ...element,
                Rights: arrayRight,
              });
            }

            /* Gera a lista de direitos para a Websocket */
            for (let index = 0; index < resp.data[0].Rights.length; index++) {
              const element = resp.data[0].Rights[index];

              //arrayGenreWS = respR.data[index].Genres;
              arrayGenreWS = generoList.filter((g) => g.rightId == element.id);

              console.log(arrayGenreWS);
              arraySubGenreWS = genreSubList.filter(
                ({ Genresubitems, ...g }) => g.rightId == element.id && g
              );

              arrayRightWS.push({
                ...element,
                Genres: arrayGenreWS,
                Genresubs: arraySubGenreWS,
              });

              console.log(arrayRightWS);
            }

            /* Monta a lista para Websocket */
            for (let index = 0; index < resp.data.length; index++) {
              const element = resp.data[index];

              arrayHeapWS.push({
                ...element,
                Rights: arrayRightWS,
              });
            }

            console.log(arrayHeapWS);
            console.log(arrayHeap);
            setHeapList(arrayHeap);
            setHeapListWS(arrayHeapWS);
          }
        }
      })
      .catch((error) => {});
  };

  const getPlayList = () => {
    console.log(client);
    api
      .get(`/playlist/${client.id}`)
      .then((resp) => {
        if (resp.data) {
          console.log(resp.data);
          setPlatList(resp.data);
          if (editPlayList) {
            setEditPlayList(
              resp.data.filter((p) => p.id == editPlayList.id)[0]
            );
          }
        }
      })
      .catch((error) => {});
  };

  const nextMusic = () => {
    const idM = generoSelect
      ? isPlayList
        ? generoSelect.Musics.length - 1 > numMusicList + 1
          ? generoSelect.Musics[numMusicList + 1].id
          : generoSelect.Musics[0].id
        : generoSelect.Musics.length - 1 > numMusicList + 1
        ? generoSelect.Musics[numMusicList + 1].id
        : generoSelect.Musics[0].id
      : 0;

    const artist = generoSelect
      ? isPlayList
        ? generoSelect.Musics.length - 1 > numMusicList + 1
          ? generoSelect.Musics[numMusicList + 1].artist
          : generoSelect.Musics[0].artist
        : generoSelect.Musics.length - 1 > numMusicList + 1
        ? generoSelect.Musics[numMusicList + 1].artist
        : generoSelect.Musics[0].artist
      : "";

    const arrayBlock = blockedsList.filter((b) =>
      String(artist).toLowerCase().indexOf("vinheta") > -1
        ? b.vignetteId == idM
        : String(artist).toLowerCase().indexOf("comercial") > -1
        ? b.commercialId == idM
        : String(artist).toLowerCase().indexOf("conteúdo") > -1
        ? b.contentId == idM
        : b.musicId == idM
    );

    if (isGeneroSelectRefUpdate.current) {
      setShowPlayerView(false);
      setShowListMusicView(false);
      setGeneroSelect(null);

      if (
        Object.keys(generoSelectRefUpdate.current).filter(
          (key) => key == "Genresubitems"
        ).length > 0
      ) {
        onSubSelect(
          generoSelectRefUpdate.current,
          generoSelectRefUpdate.current.Musics
        );
      } else {
        onGeneroSelect(
          generoSelectRefUpdate.current,
          generoSelectRefUpdate.current.Musics
        );
      }
    } else if (
      (String(generoSelectTempRef.current).length > 4) &
      ((genreSubIndexEffect < 0) & (genreIndexEffect < 0))
    ) {
      setNumMusicListPassed(numMusicList);
      setNumMusicList(0);
      setGeneroSelect(generoSelectTempRef.current);
      setGeneroSelectTemp(null);
      setShowPlayerView(true);
    } else if (!generoSelectTempRef.current) {
      setNumMusicListPassed(numMusicList);
      setNumMusicList(
        numMusicListRef.current + (arrayBlock.length > 0 ? 2 : 1) >
          (isPlayList
            ? generoSelectRef.current.Musics.length
            : generoSelectRef.current.Musics.length) -
            1
          ? 0
          : numMusicListRef.current + (arrayBlock.length > 0 ? 2 : 1)
      );
      setShowPlayerView(true);
    }
  };

  const backMusic = () => {
    const idM = generoSelect
      ? isPlayList
        ? generoSelect.Musics[numMusicList - 1 < 0 ? 0 : numMusicList - 1].id
        : generoSelect.Musics[numMusicList - 1 < 0 ? 0 : numMusicList - 1].id
      : 0;

    const artist = generoSelect
      ? isPlayList
        ? generoSelect.Musics[numMusicList - 1 < 0 ? 0 : numMusicList - 1]
            .artist
        : generoSelect.Musics[numMusicList - 1 < 0 ? 0 : numMusicList - 1]
            .artist
      : "";

    const arrayBlock = blockedsList.filter((b) =>
      String(artist).toLowerCase().indexOf("vinheta") > -1
        ? b.vignetteId == idM
        : String(artist).toLowerCase().indexOf("comercial") > -1
        ? b.commercialId == idM
        : String(artist).toLowerCase().indexOf("conteúdo") > -1
        ? b.contentId == idM
        : b.musicId == idM
    );

    if (isGeneroSelectRefUpdate.current) {
      setShowPlayerView(false);
      setShowListMusicView(false);
      setGeneroSelect(null);
      if (
        Object.keys(generoSelectRefUpdate.current).filter(
          (key) => key == "Genresubitems"
        ).length > 0
      ) {
        onSubSelect(
          generoSelectRefUpdate.current,
          generoSelectRefUpdate.current.Musics
        );
      } else {
        onGeneroSelect(
          generoSelectRefUpdate.current,
          generoSelectRefUpdate.current.Musics
        );
      }
    } else if (
      (String(generoSelectTempRef.current).length > 4) &
      ((genreSubIndexEffect < 0) & (genreIndexEffect < 0))
    ) {
      setNumMusicListPassed(numMusicList);
      setNumMusicList(0);
      setGeneroSelect(generoSelectTempRef.current);
      setGeneroSelectTemp(null);
      setShowPlayerView(true);
    } else if (!generoSelectTempRef.current) {
      setNumMusicListPassed(numMusicList);
      setNumMusicList(
        numMusicListRef.current - (arrayBlock.length > 0 ? 2 : 1) < 0
          ? 0
          : numMusicListRef.current - (arrayBlock.length > 0 ? 2 : 1)
      );
      setShowPlayerView(true);
    }
  };

  /* ---------- Inicio Select Mult SubGenre ---------- */

  /* useEffect(() => {
    console.log(musicsGenresSelect.length);

    if (musicsGenresSelect.length > 0) {
      console.log(genreSubIndexEffect);
      setGenreSubIndexEffect(genreSubIndexEffect + 1);
      // onGeneroSelect(genrePendent);
    }
  }, [musicsGenresSelect]); */

  const returnMusics = async () => {
    let arrayMusic = [];

    for (let index = 0; index < genreIdsSelect.length; index++) {
      const element = genreIdsSelect[index];

      const mus = await api.get(
        `/music/genre/${element}/${channelSetting.audioQualityId}`
      );

      if (mus.data) {
        console.log(mus.data);
        for (let index = 0; index < mus.data.length; index++) {
          const elementM = mus.data[index];

          arrayMusic.push(elementM);
        }
      }
    }

    for (let index = 0; index < genreSubIdsSelect.length; index++) {
      const element = genreSubIdsSelect[index];

      const musS = await api.get(
        `/genresubitem/genresub/${element}/${channelSetting.audioQualityId}`
      );

      for (let index = 0; index < musS.data.length; index++) {
        const elementM = musS.data[index];

        arrayMusic.push(elementM.Music);
      }
    }

    setIsGeneroSelectUpdate(true);
    setGeneroSelectUpdate({
      ...generoSelectRef.current,
      Musics: shuffle(arrayMusic),
    });
  };

  useEffect(() => {
    if ((genreIdsSelect.length > 0) | (genreSubIdsSelect.length > 0)) {
      //returnMusics();
    }
  }, [genreIdsSelect, genreSubIdsSelect]);

  useEffect(() => {
    console.log(genreSubIndexEffect);
    if (genreSubIndexEffect > -1) {
      console.log(genreSubIdsSelectEffect[0]);
      console.log({ aqui: genreSubPendent });
      if (genreSubIdsSelectEffect[0]) {
        if (
          genreSubIdsSelect.filter((g) => g == genreSubPendent.id).length == 0
        ) {
          onSubSelect(genreSubPendent);
        } else {
          onSubSelect(genreSubIdsSelectEffect[0]);
        }
      } else {
        setGenreSubIndexEffect(-1);
        setGenreSubIdsSelectEffect([]);
      }
    }
  }, [genreSubIndexEffect]);

  const onSumMusicSub = (listGen) => {
    api
      .get(
        `/genresubitem/genresub/${listGen.id}/${channelSetting.audioQualityId}`
      )
      .then((respM) => {
        if (respM.data) {
          let arrayMusicFullSub = [];

          for (let index = 0; index < respM.data.length; index++) {
            const element = respM.data[index];

            arrayMusicFullSub.push(element.Music);
          }

          setMusicsGenresSelect(
            shuffle(musicsGenresSelect.concat(arrayMusicFullSub))
          );
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  /* ---------- Fim Select Mult SubGenre ---------- */

  useEffect(() => {
    console.log(musicsGenresSelect.length);

    if (musicsGenresSelect.length > 0) {
      console.log(genreIndexEffect);
      setGenreIndexEffect(genreIndexEffect + 1);
      setGenreSubIndexEffect(genreSubIndexEffect + 1);
      // onGeneroSelect(genrePendent);
    }
  }, [musicsGenresSelect]);

  useEffect(() => {
    console.log(genreIndexEffect);
    if (genreIndexEffect > -1) {
      console.log(genreIdsSelectEffect[0]);
      if (genreIdsSelectEffect[0]) {
        if (genreIdsSelect.filter((g) => g == genrePendent.id).length == 0) {
          onGeneroSelect(genrePendent);
        } else {
          onGeneroSelect(genreIdsSelectEffect[0]);
        }
      } else {
        setGenreIndexEffect(-1);
        setGenreIdsSelectEffect([]);
      }
    }
  }, [genreIndexEffect]);

  const getMusicsList = (qualityId) => {
    api
      .get(`/music/${qualityId}`)
      .then((respM) => {
        if (respM.data) {
          setListMusicComplete(
            respM.data.filter((m) => m.audioQualityId == qualityId)
          );
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onSumMusic = (listGen) => {
    api
      .get(`/music/genre/${listGen.id}/${channelSetting.audioQualityId}`)
      .then((respM) => {
        if (respM.data) {
          setMusicsGenresSelect(shuffle(musicsGenresSelect.concat(respM.data)));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onGeneroSelect = (valueGenre, musicOld, isStart) => {
    // setIsLoadingGenre(true);
    api
      .get(`/music/genre/${valueGenre.id}/${channelSetting.audioQualityId}`)
      .then(async (respM) => {
        if (respM.data) {
          const value = { ...valueGenre, Musics: respM.data };
          console.log(value);
          console.log(musicsGenresSelect);
          console.log(musicOld);

          const getSubTypeSegment = await api.get(`/genresub/typesegment`);

          let arrayGenreSub = getSubTypeSegment.data.filter(
            (item) => String(item.typeSegment).toLowerCase() == "invisivel"
          );
          /* let arrayGenreSub = genresubDataArray.filter(
            (item) => String(item.typeSegment).toLowerCase() == "invisivel"
          ); */

          let musicDelete = [];

          for (let index = 0; index < arrayGenreSub.length; index++) {
            const element = arrayGenreSub[index];

            const getSubItem = await api.get(
              `/genresubitem/genresub/${element.id}/${channelSetting.audioQualityId}`
            );

            musicDelete = [...getSubItem.data, ...musicDelete];
          }

          console.log(musicDelete);

          if (
            !(value.Musics.length > 0) &
            (musicOld ? musicOld.length == 0 : true)
          ) {
            let arrayGenreId = genreIdsSelect.filter((g) => g != value.id);
            setGenreIdsSelect(arrayGenreId);
            showMessage(
              "Acervo de Musica",
              "Esse grupo de musicas está vázio.",
              "warning"
            );

            if (genreIndexEffect > -1) {
              let arrayEffect = genreIdsSelectEffect.filter(
                (g) => g.id != valueGenre.id
              );

              setGenreIdsSelectEffect(arrayEffect);
              setGenreIndexEffect(genreIndexEffect + 1);
            }
            return;
          }

          let musicComVin = [];
          let genreNow = [];

          let endCommercial = false;
          let endContent = false;

          let repetListMusic = false;

          const lastInformation = "";

          let musicsModify = musicOld
            ? shuffle(musicOld)
            : !musicsGenresSelect
            ? shuffle(value.Musics)
            : musicsGenresSelect.length > 0
            ? shuffle(musicsGenresSelect)
            : shuffle(value.Musics);

          musicsModify = musicsModify.filter(
            (m) => musicDelete.filter((d) => m && m.id == d.musicId).length == 0
          );

          let indexListUpdate = 0;

          let listObjectContent = previewListsUpdates.map((p) => {
            if (String(p.tipo).indexOf("Comercial") > -1) {
              return {
                ...p,
                data: commercials.filter(
                  (c) =>
                    String(c.manualType).toLowerCase() ==
                    String(p.tipo).split(" - ")[1].toLowerCase()
                ),
              };
            } else if (String(p.tipo).indexOf("Vinheta") > -1) {
              return { ...p, data: vignettes };
            } else if (String(p.tipo).indexOf("Hora Certa") > -1) {
              setGenreVozHoraCerta(
                p.genre ? (p.genre == "" ? "male" : p.genre) : "male"
              );
              return {
                ...p,
                urlFixed:
                  "https://bomjafilev.s3.amazonaws.com/times/campainha/userFile-25102021211638760.mp3",
              };
            } else {
              return {
                ...p,
                data: content
                  .filter((c) =>
                    moment().isBetween(
                      c.Days[0].startDate,
                      c.Days[0].dueDate,
                      undefined,
                      "[)"
                    )
                  )
                  .filter(
                    (c) =>
                      String(
                        c.Typecontent ? c.Typecontent.Contentgroup.name : ""
                      ).toLowerCase() == String(p.tipo).toLowerCase()
                  ),
              };
            }
          });

          console.log(listObjectContent);

          for (let index = 0; index < musicsModify.length; ) {
            let indexNew = index;
            const element = musicsModify[index];

            if (indexListUpdate < previewLists.length) {
              const elementL = previewLists[indexListUpdate];

              let listUpdates = listObjectContent.filter(
                (p) => p.id == elementL.id
              );

              if (listUpdates.length > 0) {
                if (listUpdates[0].qtd < elementL.qtd) {
                  listObjectContent = listObjectContent.map((p) => {
                    return p.id == elementL.id
                      ? { ...p, qtd: listUpdates[0].qtd + 1 }
                      : p;
                  });

                  if (listUpdates[0].qtd < elementL.qtd) {
                    if (elementL.qtd > 0) {
                      musicComVin.push(element);
                      indexNew++;
                    }
                  }

                  if (listUpdates[0].qtd + 1 == elementL.qtd) {
                    if (String(listUpdates[0].tipo).indexOf("Comercial") > -1) {
                      let seqCommercial = listUpdates[0].data;

                      const indexCommercial = Math.floor(
                        Math.random() * listUpdates[0].data.length
                      );

                      if (seqCommercial.length > 0) {
                        musicComVin.push({
                          artist: elementL.tipo,
                          id: seqCommercial[indexCommercial].id,
                          title: elementL.urlFixed
                            ? "Comercial Fixado"
                            : seqCommercial[indexCommercial].name,
                          totalTime: "",
                          url_m4a_s3: elementL.urlFixed
                            ? elementL.urlFixed
                            : seqCommercial[indexCommercial].path_s3,
                        });
                      }
                    } else if (
                      String(listUpdates[0].tipo).indexOf("Vinheta") > -1
                    ) {
                      const indexVignette = Math.floor(
                        Math.random() * listUpdates[0].data.length
                      );

                      if (listUpdates[0].data.length > 0) {
                        musicComVin.push({
                          artist: "Vinheta",
                          id: listUpdates[0].data[indexVignette].id,
                          title: elementL.urlFixed
                            ? "Vinheta Fixada"
                            : listUpdates[0].data[indexVignette].name,
                          totalTime: "",
                          url_m4a_s3: elementL.urlFixed
                            ? elementL.urlFixed
                            : listUpdates[0].data[indexVignette].path_s3,
                        });
                      }
                    } else if (
                      String(listUpdates[0].tipo).indexOf("Hora Certa") > -1
                    ) {
                      musicComVin.push({
                        artist: "Hora Certa",
                        id: 300,
                        title: "Informa",
                        totalTime: "",
                        url_m4a_s3: listObjectContent.filter(
                          (c) => c.tipo == "Hora Certa"
                        )[0].urlFixed,
                      });
                    } else {
                      let seqContent = listUpdates[0].data;

                      const indexContent = Math.floor(
                        Math.random() * seqContent.length
                      );

                      if (seqContent.length > 0) {
                        musicComVin.push({
                          artist: elementL.urlFixed
                            ? "Conteúdo"
                            : `Conteúdo ${
                                seqContent[indexContent].Typecontent
                                  ? seqContent[indexContent].Typecontent
                                      .Contentgroup.name
                                  : ""
                              }`,
                          id: seqContent[indexContent].id,
                          title: elementL.urlFixed
                            ? "Conteúdo Fixado"
                            : seqContent[indexContent].name,
                          totalTime: "",
                          url_m4a_s3: elementL.urlFixed
                            ? elementL.urlFixed
                            : seqContent[indexContent].path_s3,
                          cover:
                            seqContent[indexContent].Covers.length > 0
                              ? seqContent[indexContent].Covers[
                                  Math.floor(
                                    Math.random() *
                                      seqContent[indexContent].Covers.length
                                  )
                                ].path_s3
                              : null,
                        });
                      }
                    }
                  }
                } else {
                  listObjectContent = listObjectContent.map((p) => {
                    return p.id == elementL.id ? { ...p, ended: true } : p;
                  });
                }
              }

              if (listObjectContent.filter((p) => !p.ended).length == 0) {
                listObjectContent = listObjectContent.map((p, x) => {
                  return {
                    ...p,
                    ended: false,
                    qtd: previewLists[x].qtd == 0 ? -1 : 0,
                  };
                });

                endCommercial = true;
                endContent = true;

                indexListUpdate = 0;
              } else {
                if (listUpdates[0].qtd == elementL.qtd) {
                  indexListUpdate = indexListUpdate + 1;
                }
              }

              if (index + 1 == musicsModify.length) {
                if (!endCommercial | !endContent) {
                  index = 0;
                  repetListMusic = true;
                }
              }

              if (repetListMusic) {
                if (!endCommercial | !endContent) {
                  index = musicsModify.length;
                }
              }
            }

            index = indexNew;
          }

          genreNow = { ...value, Musics: musicComVin };
          console.log(genreNow);

          //setNumMusicList(0);
          setIsPlayList(false);
          setIsPlayListSub(false);
          //setGeneroSelect(genreNow);

          if (!generoSelect) {
            setGeneroSelect(genreNow);
            if (generoSelectRefUpdate.current) {
              setNumMusicList(1);
            } else {
              setNumMusicList(0);
            }
          } else {
            genreNow.Musics.filter((i, index) => index == 0);
            console.log(genreNow);
            setGeneroSelectTemp(genreNow);
          }

          if (isStart) {
            setGeneroSelect(genreNow);
            setNumMusicListPassed(0);
            setNumMusicList(0);
            setIsLoadingGenre(false);
            setShowPlayerView(true);
          }

          if (genreIndexEffect > -1) {
            let arrayEffect = genreIdsSelectEffect.filter(
              (g) => g.id != valueGenre.id
            );
            setGenreIdsSelectEffect(arrayEffect);
            setGenreIndexEffect(genreIndexEffect + 1);
          }
          // setIsLoadingGenre(false);
          // setShowListMusicView(!showListMusicView);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onSubSelect = (valueSub, musicOld, isStart, isReset) => {
    // setIsLoadingGenre(true);
    api
      .get(
        `/genresubitem/genresub/${valueSub.id}/${channelSetting.audioQualityId}`
      )
      .then(async (respI) => {
        console.log(valueSub);
        console.log(respI.data);

        if (respI.data) {
          const value = { ...valueSub, Genresubitems: respI.data };

          const getSubTypeSegment = await api.get(`/genresub/typesegment`);

          let arrayGenreSub = getSubTypeSegment.data.filter(
            (item) => String(item.typeSegment).toLowerCase() == "invisivel"
          );
          /* let arrayGenreSub = genresubDataArray.filter(
            (item) => String(item.typeSegment).toLowerCase() == "invisivel"
          ); */

          let musicDelete = [];

          for (let index = 0; index < arrayGenreSub.length; index++) {
            const element = arrayGenreSub[index];

            const getSubItem = await api.get(
              `/genresubitem/genresub/${element.id}/${channelSetting.audioQualityId}`
            );

            musicDelete = [...musicDelete, ...getSubItem.data];
          }

          console.log(musicDelete);

          if (!(value.Genresubitems.length > 0)) {
            let arrayGenreSubId = genreIdsSelect.filter((g) => g != value.id);
            setGenreSubIdsSelect(arrayGenreSubId);

            showMessage(
              "Acervo de Musica",
              "Esse grupo de musicas está vázio.",
              "warning"
            );

            if (genreSubIndexEffect > -1) {
              console.log("veio");
              let arrayEffect = genreSubIdsSelectEffect.filter(
                (g) => g.id != valueSub.id
              );
              setGenreSubIdsSelectEffect(arrayEffect);
              setGenreSubIndexEffect(genreSubIndexEffect + 1);
            }
            return;
          }

          /*  setMusicsGenresSelect([]);
          setGenreIdsSelect([]); */

          let musicComVin = [];
          let genreNow = [];

          let endCommercial = false;
          let endContent = false;

          let repetListMusic = false;

          const lastInformation = "";
          let indexListUpdate = 0;

          console.log(previewListsUpdates);

          let listObjectContent = previewListsUpdates.map((p) => {
            if (String(p.tipo).indexOf("Comercial") > -1) {
              return {
                ...p,
                data: commercials.filter(
                  (c) =>
                    String(c.manualType).toLowerCase() ==
                    String(p.tipo).split(" - ")[1].toLowerCase()
                ),
              };
            } else if (String(p.tipo).indexOf("Vinheta") > -1) {
              return { ...p, data: vignettes };
            } else if (String(p.tipo).indexOf("Hora Certa") > -1) {
              setGenreVozHoraCerta(
                p.genre ? (p.genre == "" ? "male" : p.genre) : "male"
              );
              return {
                ...p,
                urlFixed:
                  "https://bomjafilev.s3.amazonaws.com/times/campainha/userFile-25102021211638760.mp3",
              };
            } else {
              return {
                ...p,
                data: content
                  .filter((c) =>
                    moment().isBetween(
                      c.Days[0].startDate,
                      c.Days[0].dueDate,
                      undefined,
                      "[)"
                    )
                  )
                  .filter(
                    (c) =>
                      String(
                        c.Typecontent ? c.Typecontent.Contentgroup.name : ""
                      ).toLowerCase() == String(p.tipo).toLowerCase()
                  ),
              };
            }
          });

          console.log(listObjectContent);

          let arrayMusicFullSub = [];

          for (let index = 0; index < value.Genresubitems.length; index++) {
            const element = value.Genresubitems[index];

            if (element.Music) {
              arrayMusicFullSub.push(element.Music);
            }
          }
          console.log(arrayMusicFullSub);
          console.log(musicOld);

          let musicsModify = isReset
            ? arrayMusicFullSub
            : musicOld
            ? shuffle(musicOld)
            : !musicsGenresSelect
            ? shuffle(arrayMusicFullSub)
            : musicsGenresSelect.length > 0
            ? shuffle(musicsGenresSelect)
            : shuffle(arrayMusicFullSub); //shuffle(value.Genresubitems);

          console.log(musicsModify);

          musicsModify = musicsModify.filter(
            (m) => m && musicDelete.filter((d) => m.id == d.musicId).length == 0
          );

          //console.log(musicsModify);

          for (let index = 0; index < musicsModify.length; ) {
            let indexNew = index;
            const element = musicsModify[index];
            //console.log(indexNew);

            if (indexListUpdate < previewLists.length) {
              const elementL = previewLists[indexListUpdate];
              //console.log(elementL);

              let listUpdates = listObjectContent.filter(
                (p) => p.id == elementL.id
              );
              console.log(listUpdates);
              if (listUpdates.length > 0) {
                if (listUpdates[0].qtd < elementL.qtd) {
                  listObjectContent = listObjectContent.map((p) => {
                    return p.id == elementL.id
                      ? { ...p, qtd: listUpdates[0].qtd + 1 }
                      : p;
                  });

                  if (listUpdates[0].qtd < elementL.qtd) {
                    //console.log(elementL.qtd);
                    if (elementL.qtd > 0) {
                      musicComVin.push(element);
                      indexNew++; //(element.Music);
                    }
                  }

                  if (listUpdates[0].qtd + 1 == elementL.qtd) {
                    if (String(listUpdates[0].tipo).indexOf("Comercial") > -1) {
                      let seqCommercial = listUpdates[0].data;

                      const indexCommercial = Math.floor(
                        Math.random() * listUpdates[0].data.length
                      );

                      if (seqCommercial.length > 0) {
                        musicComVin.push({
                          artist: elementL.tipo,
                          id: seqCommercial[indexCommercial].id,
                          title: elementL.urlFixed
                            ? "Comercial Fixado"
                            : seqCommercial[indexCommercial].name,
                          totalTime: "",
                          url_m4a_s3: elementL.urlFixed
                            ? elementL.urlFixed
                            : seqCommercial[indexCommercial].path_s3,
                        });
                      }
                    } else if (
                      String(listUpdates[0].tipo).indexOf("Vinheta") > -1
                    ) {
                      const indexVignette = Math.floor(
                        Math.random() * listUpdates[0].data.length
                      );

                      if (listUpdates[0].data.length > 0) {
                        musicComVin.push({
                          artist: "Vinheta",
                          id: listUpdates[0].data[indexVignette].id,
                          title: elementL.urlFixed
                            ? "Vinheta Fixada"
                            : listUpdates[0].data[indexVignette].name,
                          totalTime: "",
                          url_m4a_s3: elementL.urlFixed
                            ? elementL.urlFixed
                            : listUpdates[0].data[indexVignette].path_s3,
                        });
                      }
                    } else if (
                      String(listUpdates[0].tipo).indexOf("Hora Certa") > -1
                    ) {
                      musicComVin.push({
                        artist: "Hora Certa",
                        id: 300,
                        title: "Informa",
                        totalTime: "",
                        url_m4a_s3: listObjectContent.filter(
                          (c) => c.tipo == "Hora Certa"
                        )[0].urlFixed,
                      });
                    } else {
                      let seqContent = listUpdates[0].data;

                      const indexContent = Math.floor(
                        Math.random() * seqContent.length
                      );

                      if (seqContent.length > 0) {
                        musicComVin.push({
                          artist: elementL.urlFixed
                            ? "Conteúdo"
                            : `Conteúdo ${
                                seqContent[indexContent].Typecontent
                                  ? seqContent[indexContent].Typecontent
                                      .Contentgroup.name
                                  : ""
                              }`,
                          id: seqContent[indexContent].id,
                          title: elementL.urlFixed
                            ? "Conteúdo Fixado"
                            : seqContent[indexContent].name,
                          totalTime: "",
                          url_m4a_s3: elementL.urlFixed
                            ? elementL.urlFixed
                            : seqContent[indexContent].path_s3,
                          cover:
                            seqContent[indexContent].Covers.length > 0
                              ? seqContent[indexContent].Covers[
                                  Math.floor(
                                    Math.random() *
                                      seqContent[indexContent].Covers.length
                                  )
                                ].path_s3
                              : null,
                        });
                      }
                    }
                  }
                } else {
                  listObjectContent = listObjectContent.map((p) => {
                    return p.id == elementL.id ? { ...p, ended: true } : p;
                  });
                }
              }

              if (listObjectContent.filter((p) => !p.ended).length == 0) {
                listObjectContent = listObjectContent.map((p, x) => {
                  return {
                    ...p,
                    ended: false,
                    qtd: previewLists[x].qtd == 0 ? -1 : 0,
                  };
                });

                endCommercial = true;
                endContent = true;

                indexListUpdate = 0;
              } else {
                if (listUpdates[0].qtd == elementL.qtd) {
                  indexListUpdate = indexListUpdate + 1;
                }
              }
            }

            if (index + 1 == musicsModify.length) {
              if (!endCommercial | !endContent) {
                index = 0;
                repetListMusic = true;
              }
            }

            if (repetListMusic) {
              if (!endCommercial | !endContent) {
                index = musicsModify.length;
              }
            }

            index = indexNew;

            //console.log("vinheta number " + String(qtdVignettePassed));
          }

          console.log(musicComVin);
          genreNow = { ...value, Musics: musicComVin };
          //console.log(genreNow);

          //setNumMusicList(0);
          setIsPlayList(false);
          setIsPlayListSub(true);

          console.log(generoSelect);

          if (!generoSelectRef.current) {
            setGeneroSelect(genreNow);

            if (generoSelectRefUpdate.current) {
              setNumMusicList(1);
            } else {
              setNumMusicList(0);
            }

            setShowPlayerView(true);
          } else {
            genreNow.Musics.filter((i, index) => index == 0);
            setGeneroSelectTemp(genreNow);
          }

          if (isStart) {
            setGeneroSelect(genreNow);
            setNumMusicListPassed(0);
            setNumMusicList(0);
            setIsLoadingGenre(false);
            setShowPlayerView(true);
          }

          if (genreSubIndexEffect > -1) {
            let arrayEffect = genreSubIdsSelectEffect.filter(
              (g) => g.id != valueSub.id
            );
            setGenreSubIdsSelectEffect(arrayEffect);
            setGenreSubIndexEffect(genreSubIndexEffect + 1);
          }

          //setIsLoadingGenre(false);
          // setShowListMusicView(!showListMusicView);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if ((listExecutId > 0) & (String(generoSelectTemp).length > 10)) {
      console.log();
      api
        .put(`/listexecut/edit/${listExecutId}`, {
          music: JSON.stringify(generoSelectTemp),
          isPlayList: String(isPlayList),
          isPlayListSub: String(isPlayListSub),
          genreIdsSelect: JSON.stringify(genreIdsSelect),
          genreSubIdsSelect: JSON.stringify(genreSubIdsSelect),
          numMusicList: numMusicList,
          playingAuto: String(playingAuto),
        })
        .then((response) => response.data)
        .then((resp) => {
          if (resp.message == "success") {
            socket.emit("SendInfoListMusic", {
              clientId: client.id,
              music: [],
              isPlayList: isPlayList,
              isPlayListSub: isPlayListSub,
              numMusicList: numMusicList,
              playingAuto: playingAuto,
              genreIdsSelect: [],
            });
          }
        })
        .catch((error) => {
          console.log(error);
          // showMessage("Error", "Erro, consulte o suporte", "danger");
        });
    }

    console.log(generoSelectTemp);
  }, [generoSelectTemp]);

  const onPlaylistSelect = (valuePlay) => {
    setIsLoadingGenre(true);
    api
      .get(`/itemplaylist/playlist/${valuePlay.id}`)
      .then((respP) => {
        if (respP.data) {
          if (respP.data.length > 0) {
            const value = { ...valuePlay, Itemplaylists: respP.data };

            if (!(value.Itemplaylists.length > 0)) {
              showMessage("Playlist", "Essa está vázio.", "warning");
              return;
            }

            setMusicsGenresSelect([]);
            setGenreIdsSelect([]);
            setGenreSubIdsSelect([]);

            let musicComVin = [];
            let genreNow = [];

            let endCommercial = false;
            let endContent = false;

            let repetListMusic = false;

            const lastInformation = "";

            let indexListUpdate = 0;

            let listObjectContent = previewListsUpdates.map((p) => {
              if (String(p.tipo).indexOf("Comercial") > -1) {
                return {
                  ...p,
                  data: commercials.filter(
                    (c) =>
                      String(c.manualType).toLowerCase() ==
                      String(p.tipo).split(" - ")[1].toLowerCase()
                  ),
                };
              } else if (String(p.tipo).indexOf("Vinheta") > -1) {
                return { ...p, data: vignettes };
              } else if (String(p.tipo).indexOf("Hora Certa") > -1) {
                setGenreVozHoraCerta(
                  p.genre ? (p.genre == "" ? "male" : p.genre) : "male"
                );
                return {
                  ...p,
                  urlFixed:
                    "https://bomjafilev.s3.amazonaws.com/times/campainha/userFile-25102021211638760.mp3",
                };
              } else {
                return {
                  ...p,
                  data: content
                    .filter((c) =>
                      moment().isBetween(
                        c.Days[0].startDate,
                        c.Days[0].dueDate,
                        undefined,
                        "[)"
                      )
                    )
                    .filter(
                      (c) =>
                        String(
                          c.Typecontent ? c.Typecontent.Contentgroup.name : ""
                        ).toLowerCase() == String(p.tipo).toLowerCase()
                    ),
                };
              }
            });

            const MusicEmbaralhado = shuffle(value.Itemplaylists);

            for (let index = 0; index < MusicEmbaralhado.length; ) {
              let indexNew = index;
              const element = MusicEmbaralhado[index];

              if (indexListUpdate < previewLists.length) {
                const elementL = previewLists[indexListUpdate];

                let listUpdates = listObjectContent.filter(
                  (p) => p.id == elementL.id
                );

                if (listUpdates.length > 0) {
                  if (listUpdates[0].qtd < elementL.qtd) {
                    listObjectContent = listObjectContent.map((p) => {
                      return p.id == elementL.id
                        ? { ...p, qtd: listUpdates[0].qtd + 1 }
                        : p;
                    });

                    if (listUpdates[0].qtd < elementL.qtd) {
                      if (elementL.qtd > 0) {
                        musicComVin.push(element.Music);
                        indexNew++;
                      }
                    }

                    if (listUpdates[0].qtd + 1 == elementL.qtd) {
                      if (
                        String(listUpdates[0].tipo).indexOf("Comercial") > -1
                      ) {
                        let seqCommercial = listUpdates[0].data;

                        const indexCommercial = Math.floor(
                          Math.random() * listUpdates[0].data.length
                        );

                        if (seqCommercial.length > 0) {
                          musicComVin.push({
                            artist: elementL.tipo,
                            id: seqCommercial[indexCommercial].id,
                            title: elementL.urlFixed
                              ? "Comercial Fixado"
                              : seqCommercial[indexCommercial].name,
                            totalTime: "",
                            url_m4a_s3: elementL.urlFixed
                              ? elementL.urlFixed
                              : seqCommercial[indexCommercial].path_s3,
                          });
                        }
                      } else if (
                        String(listUpdates[0].tipo).indexOf("Vinheta") > -1
                      ) {
                        const indexVignette = Math.floor(
                          Math.random() * listUpdates[0].data.length
                        );

                        if (listUpdates[0].data.length > 0) {
                          musicComVin.push({
                            artist: "Vinheta",
                            id: listUpdates[0].data[indexVignette].id,
                            title: elementL.urlFixed
                              ? "Vinheta Fixada"
                              : listUpdates[0].data[indexVignette].name,
                            totalTime: "",
                            url_m4a_s3: elementL.urlFixed
                              ? elementL.urlFixed
                              : listUpdates[0].data[indexVignette].path_s3,
                          });
                        }
                      } else if (
                        String(listUpdates[0].tipo).indexOf("Hora Certa") > -1
                      ) {
                        musicComVin.push({
                          artist: "Hora Certa",
                          id: 1,
                          title: "",
                          totalTime: "",
                          url_m4a_s3: listObjectContent.filter(
                            (c) => c.tipo == "Hora Certa"
                          )[0].urlFixed,
                        });
                      } else {
                        let seqContent = listUpdates[0].data;

                        const indexContent = Math.floor(
                          Math.random() * seqContent.length
                        );

                        if (seqContent.length > 0) {
                          musicComVin.push({
                            artist: elementL.urlFixed
                              ? "Conteúdo"
                              : `Conteúdo ${
                                  seqContent[indexContent].Typecontent
                                    ? seqContent[indexContent].Typecontent
                                        .Contentgroup.name
                                    : ""
                                }`,
                            id: seqContent[indexContent].id,
                            title: elementL.urlFixed
                              ? "Conteúdo Fixado"
                              : seqContent[indexContent].name,
                            totalTime: "",
                            url_m4a_s3: elementL.urlFixed
                              ? elementL.urlFixed
                              : seqContent[indexContent].path_s3,
                            cover:
                              seqContent[indexContent].Covers.length > 0
                                ? seqContent[indexContent].Covers[
                                    Math.floor(
                                      Math.random() *
                                        seqContent[indexContent].Covers.length
                                    )
                                  ].path_s3
                                : null,
                          });
                        }
                      }
                    }
                  } else {
                    listObjectContent = listObjectContent.map((p) => {
                      return p.id == elementL.id ? { ...p, ended: true } : p;
                    });
                  }
                }

                if (listObjectContent.filter((p) => !p.ended).length == 0) {
                  listObjectContent = listObjectContent.map((p, x) => {
                    return {
                      ...p,
                      ended: false,
                      qtd: previewLists[x].qtd == 0 ? -1 : 0,
                    };
                  });

                  endCommercial = true;
                  endContent = true;

                  indexListUpdate = 0;
                } else {
                  if (listUpdates[0].qtd == elementL.qtd) {
                    indexListUpdate = indexListUpdate + 1;
                  }
                }

                if (index + 1 == MusicEmbaralhado.length) {
                  if (!endCommercial | !endContent) {
                    index = 0;
                    repetListMusic = true;
                  }
                }

                if (repetListMusic) {
                  if (!endCommercial | !endContent) {
                    index = MusicEmbaralhado.length;
                  }
                }
              }

              index = indexNew;
            }

            genreNow = { ...value, Musics: musicComVin };
            console.log(genreNow);

            setIsPlayList(false);
            setIsPlayListSub(false);
            setGeneroSelect(genreNow);
            setNumMusicListPassed(numMusicList);
            setNumMusicList(0);
            setIsLoadingGenre(false);
            setShowPlayerView(true);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onPlayMusic = (index) => {
    setNumMusicListPassed(numMusicList);
    setNumMusicList(index);
  };

  const onGetBlockeds = () => {
    api
      .get(`/blocked/${client.id}`)
      .then((resp) => {
        if (resp.data) {
          setBlockedsList(resp.data);
          console.log(resp.data);
        }
      })
      .catch((error) => {});
  };

  const onGetFavoriteds = () => {
    api
      .get(`/favorited/${client.id}`)
      .then((resp) => {
        if (resp.data) {
          setFavoritedsList(resp.data);
          console.log(resp.data);
        }
      })
      .catch((error) => {});
  };

  const onBlock = (id, artist) => {
    const arrayBase = {
      musicId: 0,
      contentId: 0,
      commercialId: 0,
      vignetteId: 0,
      clientId: 0,
    };

    const data =
      String(artist).toLowerCase().indexOf("vinheta") > -1
        ? {
            ...arrayBase,
            vignetteId: id,
            clientId: client.id,
          }
        : String(artist).toLowerCase().indexOf("comercial") > -1
        ? {
            ...arrayBase,
            commercialId: id,
            clientId: client.id,
          }
        : String(artist).toLowerCase().indexOf("conteúdo") > -1
        ? {
            ...arrayBase,
            contentId: id,
            clientId: client.id,
          }
        : { ...arrayBase, musicId: id, clientId: client.id };

    api
      .post("/blocked/new", data)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Bloqueada",
            "Musica adicionada na sua lista de bloqueio.",
            "success"
          );
          onGetBlockeds();
        } else {
          showMessage(
            "Musica Bloqueada",
            "Musica falhou ao bloquear.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onLikeFavorite = (id, artist) => {
    console.log({ aqui: id });
    const arrayBase = {
      musicId: 0,
      contentId: 0,
      commercialId: 0,
      vignetteId: 0,
      clientId: 0,
    };

    const data =
      String(artist).toLowerCase().indexOf("vinheta") > -1
        ? {
            ...arrayBase,
            vignetteId: id,
            clientId: client.id,
          }
        : String(artist).toLowerCase().indexOf("comercial") > -1
        ? {
            ...arrayBase,
            commercialId: id,
            clientId: client.id,
          }
        : String(artist).toLowerCase().indexOf("conteúdo") > -1
        ? {
            ...arrayBase,
            contentId: id,
            clientId: client.id,
          }
        : { ...arrayBase, musicId: id, clientId: client.id };

    api
      .post("/favorited/new", data)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Favoritada",
            "Musica adicionada na sua lista de favoritas.",
            "success"
          );
          onGetFavoriteds();
        } else {
          showMessage(
            "Musica Favoritada",
            "Musica falhou ao favoritar.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onDesBlock = (id) => {
    api
      .delete(`/blocked/${id}`)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Desbloqueada",
            "Musica removida da sua lista de bloqueio.",
            "success"
          );
          onGetBlockeds();
        } else {
          showMessage(
            "Musica Desbloqueada",
            "Musica falhou ao desbloquear.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onDesLikeFavorite = (id) => {
    api
      .delete(`/favorited/${id}`)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage(
            "Musica Desfavoritada",
            "Musica removida da sua lista de favoritas.",
            "success"
          );
          onGetFavoriteds();
        } else {
          showMessage(
            "Musica Desfavoritada",
            "Musica falhou ao desfavoritar.",
            "warning"
          );
        }
      })
      .catch((error) => {});
  };

  const onOpenConfigConteudo = () => {
    setShowModalConfigConteudo(true);
  };

  const onCloseConfigConteudo = () => {
    setShowModalConfigConteudo(false);
  };

  const onCloseSuportContact = () => {
    setShowSuportContact(false);
  };

  const onOpenSpeech = () => {
    setShowModalSpeech(true);
  };

  const onCloseSpeech = () => {
    setShowModalSpeech(false);
  };

  const onSetPlayingSpeech = (value) => {
    console.log(value);
    setPlayingSpeech(value);
  };

  const onCloseNewPlayList = () => {
    setShowModalNewPlayList(false);
  };

  const onShowContentChannel = () => {
    setShowContentChannel(true);
  };

  const onCloseContentChannel = () => {
    setShowContentChannel(false);
  };

  const onCloseNewEmployee = () => {
    setShowModalNewEmployee(false);
  };

  const onCloseEditPlayList = () => {
    setShowModalEditPlayList(false);
  };

  const onEditPlayList = (playlist) => {
    setEditPlayList(playlist);
    setShowModalEditPlayList(true);
  };

  const onDeletePlayList = (id) => {
    setShowConfirm(true);
    setDeletePlayListId(id);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    api
      .delete(`/playlist/${deletePlayListId}`)
      .then((resp) => {
        if (resp.data.message == "success") {
          showMessage("PlayList", "Deleta com sucesso.", "success");
          setShowConfirm(false);
          getPlayList();
        }
      })
      .catch((error) => {});
  };

  const onPlayingAuto = (value) => {
    setPlayingAuto(value);
  };

  const onAddMusicList = () => {
    if (heapList.length > 0) {
      setLimitSubGenreList(
        typeSubListMobile.length > limitSubGenreList
          ? limitSubGenreList + 1
          : limitSubGenreList
      );
    }
  };

  const selectNextMusic = (music, valueIsSearch) => {
    console.log(valueIsSearch);
    setIsSearch(valueIsSearch);

    api
      .get(`/cover/genre/${music.genreId}`)
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          const defineMusic = {
            ...music,
            Genre: { ...music.Genre, Covers: resp },
          };

          let data = generoSelect.Musics.filter((g, index) => index > -1);

          data.splice(numMusicList + 1, 0, defineMusic);

          console.log(data);
          setGeneroSelect({ ...generoSelect, Musics: data });
        } else {
          setIsSearch(false);
        }
      })
      .catch((error) => {
        setIsSearch(false);
        console.log(error);
      });
  };

  const handleScroll = () => {
    const position = pf.current.scrollTop;
    //console.log(position);
    setSrollPosition(position);
  };

  const onOpenPreviewList = () => {
    setIndexValuesPreviewList(2);
    setShowModalConfigConteudo(true);
  };

  var scrollFixSugere = {};
  var scrollFixGenre = {};
  return !showModalOnline & (isMobile == "true") & !isContinue ? (
    <Box
      style={{
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        backgroundColor: "black",
        position: "absolute",
      }}
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      {client ? (
        <h4 style={{ color: "white" }} className="text-white mb-4">
          Bem Vindo
        </h4>
      ) : (
        <h1 className="text-white mb-4">Bomja Music</h1>
      )}
      {client ? <h1 className="text-white mb-4">{client.name}</h1> : null}
      <CircularProgress style={{ color: "red" }} />
    </Box>
  ) : (client ? client.id > 0 : false) & !isContinue & (isMobile == "true") ? (
    <ModalRemoteChannel
      socket={socket}
      listMusic={listMusicRemoto}
      clientName={client.name}
      getRemoteChannelId={client.id}
      getSettingChannelModify={channelSetting}
      listBlocked={blockedsList}
      listFavorited={favoritedsList}
      onGetBlockeds={onGetBlockeds}
      onGetFavoriteds={onGetFavoriteds}
      heapList={heapList}
    />
  ) : (
    <div
      id="body"
      style={{
        top: "0",
        bottom: "0",
        left: "0",
        right: "0",
        backgroundColor: "black",
        position: "absolute",
      }}
    >
      <Toolbar
        scrollPosition={scrollPosition}
        selectNextMusic={selectNextMusic}
        nameClient={client && client.name}
        groupSelect={groupSelect}
        typeGroupSelect={typeGroupSelect}
        isMobile={isMobile}
        onMyPlayList={() => {
          setGroupSelect("Minhas Playlist");
        }}
        onStart={() => {
          setGroupSelect("Com Direitos");
        }}
        onGenre={() => {
          setTypeGroupSelect("genero");
        }}
        onPlayListSugere={() => {
          setTypeGroupSelect("sugeridas");
        }}
        onOpenPreviewList={() => {
          onOpenPreviewList();
        }}
        onOpenMenu={() => {
          setIsShowMenu(true);
        }}
        onOpenSpeech={onOpenSpeech}
        onDesBlock={onDesBlock}
        onDesLikeFavorite={onDesLikeFavorite}
        onLikeFavorite={onLikeFavorite}
        onBlock={onBlock}
        listBlocked={blockedsList}
        listFavorited={favoritedsList}
        listMusicCompleteArray={listMusicComplete}
      />

      <PerfectScrollbar
        containerRef={(el) => (pf.current = el)}
        onScrollY={() => {
          handleScroll();
        }}
        onYReachEnd={() => {
          onAddMusicList();
        }}
      >
        <Box
          padding={isMobile == "true" ? 4 : 0}
          display="flex"
          justifyContent="center"
        >
          <div
            className={
              valueRandom == 1
                ? `imagemback1`
                : valueRandom == 2
                ? `imagemback2`
                : valueRandom == 3
                ? `imagemback3`
                : `imagemback4`
            }
          >
            <div
              style={{
                height: "100%",
                width: "100%",
                background: "linear-gradient(rgba(0,0,0,0.5), black)",
              }}
            />
          </div>

          <div
            style={{
              backgroundColor: "transparent",
              zIndex: 10,
              paddingTop: isMobile == "true" ? 40 : 200,
            }}
          >
            {isMobile == "true" ? (
              /* Para mobile */
              <Grid style={{ marginTop: 30, marginBottom: 80 }} container p={1}>
                <Grid md={6} sm={6} xs={6} xl={6} item>
                  <div
                    onClick={() => {
                      setTypeGroupSelect("sugeridas");
                      setGroupSelect("Com Direitos");
                    }}
                    style={{
                      backgroundColor: !(typeGroupSelect == "sugeridas")
                        ? "rgba(255,255,255,0.2)"
                        : "#ff3838",
                      width: "95%",
                      display: "flex",
                      borderRadius: 5,
                      marginLeft: -2,
                      justifyContent: "flex-start",
                      alignItems: "center",
                    }}
                  >
                    <div
                      style={{
                        width: 10,
                        height: 40,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        backgroundColor: "#2ecc71",
                      }}
                    />
                    <Typography
                      variant="caption"
                      display="block"
                      gutterBottom
                      style={{ fontWeight: "bold" }}
                      className={`ml-2 text-${
                        !(typeGroupSelect == "sugeridas") ? "white" : "white"
                      } `}
                    >
                      {String("Destaques")}
                    </Typography>
                  </div>
                </Grid>
                <Grid md={6} sm={6} xs={6} xl={6} item>
                  <div
                    onClick={() => {
                      setTypeGroupSelect("genero");
                      setGroupSelect("Com Direitos");
                    }}
                    style={{
                      backgroundColor: !(typeGroupSelect == "genero")
                        ? "rgba(255,255,255,0.2)"
                        : "#ff3838",
                      width: "95%",
                      display: "flex",
                      borderRadius: 5,
                      marginLeft: -2,
                      justifyContent: "flex-start",
                      alignItems: "center",
                    }}
                  >
                    <div
                      style={{
                        width: 10,
                        height: 40,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        backgroundColor: "#e67e22",
                      }}
                    />
                    <Typography
                      variant="caption"
                      display="block"
                      gutterBottom
                      style={{ fontWeight: "bold" }}
                      className={`ml-2 text-${
                        !(typeGroupSelect == "genero") ? "white" : "white"
                      } `}
                    >
                      {String("Gêneros")}
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            ) : (
              <div />
              /* Desktop */
            )}
            {!(groupSelect == "Favoritos") ? null : (
              <div style={{ maxWidth: 1200, marginBottom: 200 }}>
                <h5
                  style={{
                    color: "rgba(255,255,255,0.5)",
                    //marginTop: 200,
                    fontWeight: "normal",
                  }}
                  className="ml-4"
                >
                  SUA LISTA DE TODOS AUDIOS FAVORITOS
                </h5>
                <h2
                  style={{ color: "rgba(255,255,255,0.9)" }}
                  className="ml-4 mb-4"
                >
                  Favoritos
                </h2>

                <ListMusicBlockFavorite
                  onDesBlock={onDesBlock}
                  onDesLikeFavorite={onDesLikeFavorite}
                  listBlocked={blockedsList}
                  listFavorited={favoritedsList}
                  isBlocked={false}
                />
              </div>
            )}

            {!(groupSelect == "Bloqueados") ? null : (
              <div style={{ maxWidth: 1200 }}>
                <h5
                  style={{
                    color: "rgba(255,255,255,0.5)",
                    // marginTop: 200,
                    fontWeight: "normal",
                  }}
                  className="ml-4"
                >
                  SUA LISTA DE TODOS AUDIOS BLOQUEADOS
                </h5>
                <h2
                  style={{ color: "rgba(255,255,255,0.9)" }}
                  className="ml-4 mb-4"
                >
                  Bloqueados
                </h2>

                <ListMusicBlockFavorite
                  onDesBlock={onDesBlock}
                  onDesLikeFavorite={onDesLikeFavorite}
                  listBlocked={blockedsList}
                  listFavorited={favoritedsList}
                  isBlocked={true}
                />
              </div>
            )}

            {!(groupSelect == "Minhas Playlist") ? null : (
              <div style={{ maxWidth: 1200 }}>
                <h5
                  style={{
                    color: "rgba(255,255,255,0.5)",
                    //  marginTop: 200,
                    fontWeight: "normal",
                  }}
                  className="ml-4"
                >
                  PLAYLISTS CRIADAS POR VOCÊ
                </h5>
                <h2
                  style={{ color: "rgba(255,255,255,0.9)" }}
                  className="ml-4 mb-4"
                >
                  Suas Playlists
                </h2>
                <div style={{ marginTop: 45 }} className="w-100 mb-4 ml-4">
                  <Box display="flex" flexWrap="wrap" alignItems="center">
                    {playList.map((playlist) => (
                      <PlayListCard
                        playlist={playlist}
                        onDeletePlayList={onDeletePlayList}
                        onEditPlayList={onEditPlayList}
                        onPlaylistSelect={onPlaylistSelect}
                      />
                    ))}

                    <div
                      className="d-flex justify-content-center align-items-center"
                      style={{ width: 150, height: "80%" }}
                    >
                      <div>
                        <h5
                          style={{ color: "rgba(255,255,255,0.8)" }}
                          className="text-black text-center"
                        >
                          Cria uma Playlist
                        </h5>
                        <p
                          style={{ color: "rgba(255,255,255,0.8)" }}
                          className="text-black text-center"
                        >
                          adicione suas
                        </p>
                        <p
                          style={{
                            marginTop: -8,
                            color: "rgba(255,255,255,0.8)",
                          }}
                          className="text-black text-center"
                        >
                          musicas preferidas
                        </p>
                        <div className="d-flex justify-content-center">
                          <Button
                            onClick={() => {
                              setShowModalNewPlayList(true);
                            }}
                            className="mt-2"
                            size="small"
                            variant="contained"
                          >
                            Nova Playlist
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Box>
                </div>
              </div>
            )}

            {heapList.length == 0 ? (
              <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                style={{
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  position: "absolute",
                }}
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                >
                  <CircularProgress
                    style={{ color: "red", marginBottom: 30 }}
                  />
                  <h4 style={{ color: "white" }}>Buscando Musicas ...</h4>
                </Box>
              </Box>
            ) : (
              heapList[0].Rights.map((Right, index) =>
                !(groupSelect == Right.name) ? null : (
                  <div>
                    {isMobile == "true" ? (
                      <div
                        style={{
                          paddingBottom: 30,
                          marginLeft: isMobile == "true" ? 0 : 40,
                        }}
                        className="w-100 mb-4 mt-4"
                      >
                        {!showListMusicView & (typeGroupSelect == "genero")
                          ? typeSubListMobile
                              .sort((a, b) => {
                                return a.sub < b.sub
                                  ? -1
                                  : a.sub > b.sub
                                  ? 1
                                  : 0;
                              })
                              .map(({ sub }, index) => {
                                return Right.Genres.filter((g) =>
                                  sub == "Variedades"
                                    ? (String(g.name)
                                        .toLowerCase()
                                        .indexOf("nacional") ==
                                        -1) &
                                      (String(g.name)
                                        .toLowerCase()
                                        .indexOf("gospel") ==
                                        -1) &
                                      (String(g.name)
                                        .toLowerCase()
                                        .indexOf("sertanejo") ==
                                        -1) &
                                      (String(g.name)
                                        .toLowerCase()
                                        .indexOf("gaúcho") ==
                                        -1) &
                                      (String(g.name)
                                        .toLowerCase()
                                        .indexOf("bpm") ==
                                        -1)
                                    : String(g.name).indexOf(sub) > -1
                                ).length > 0 ? (
                                  <div>
                                    <h4
                                      style={{ marginTop: 15 }}
                                      className="text-white mb-3"
                                    >
                                      {sub}
                                    </h4>
                                    <div>
                                      <Box
                                        display="flex"
                                        justifyContent="space-between"
                                        position="absolute"
                                        zIndex={100}
                                        ml={-1.5}
                                        mr={0}
                                        mt={10}
                                        width={(window.screen.width * 98) / 100}
                                      >
                                        <IconButton
                                          onClick={() => {
                                            if (scrollElGenre[String(index)]) {
                                              scrollElGenre[
                                                String(index)
                                              ].scrollLeft =
                                                scrollElGenre[String(index)]
                                                  .scrollLeft - 200;
                                            }
                                          }}
                                          size="small"
                                          style={{ backgroundColor: "white" }}
                                        >
                                          <ArrowBackIosIcon />
                                        </IconButton>
                                        <IconButton
                                          onClick={() => {
                                            // console.log(scrollElGenre);
                                            if (scrollElGenre[String(index)]) {
                                              scrollElGenre[
                                                String(index)
                                              ].scrollLeft =
                                                scrollElGenre[String(index)]
                                                  .scrollLeft + 200;
                                            }
                                          }}
                                          size="small"
                                          style={{ backgroundColor: "white" }}
                                        >
                                          <ArrowForwardIosIcon />
                                        </IconButton>
                                      </Box>
                                      <PerfectScrollbar
                                        style={{
                                          overflowX: "hidden",
                                        }}
                                        containerRef={(ref) => {
                                          scrollFixGenre = {
                                            ...scrollFixGenre,
                                            [String(index)]: ref,
                                          };
                                          //console.log(scrollFixGenre);

                                          setScrollElGenre(scrollFixGenre);
                                        }}
                                      >
                                        <div
                                          style={{
                                            width:
                                              (window.screen.width * 90) / 100,
                                          }}
                                        >
                                          <Box
                                            display="flex"
                                            style={{
                                              width: "100%",
                                            }}
                                          >
                                            {Right.Genres.filter((g) =>
                                              sub == "Variedades"
                                                ? (String(g.name)
                                                    .toLowerCase()
                                                    .indexOf("nacional") ==
                                                    -1) &
                                                  (String(g.name)
                                                    .toLowerCase()
                                                    .indexOf("gospel") ==
                                                    -1) &
                                                  (String(g.name)
                                                    .toLowerCase()
                                                    .indexOf("sertanejo") ==
                                                    -1) &
                                                  (String(g.name)
                                                    .toLowerCase()
                                                    .indexOf("gaúcho") ==
                                                    -1) &
                                                  (String(g.name)
                                                    .toLowerCase()
                                                    .indexOf("mix") ==
                                                    -1) &
                                                  (String(g.name)
                                                    .toLowerCase()
                                                    .indexOf("bpm") ==
                                                    -1)
                                                : String(g.name).indexOf(sub) >
                                                  -1
                                            )
                                              .sort((a, b) => {
                                                return a.name < b.name
                                                  ? -1
                                                  : a.name > b.name
                                                  ? 1
                                                  : 0;
                                              })
                                              .map((generos, indexg) => (
                                                <Grow
                                                  mountOnEnter
                                                  unmountOnExit
                                                  in={true}
                                                  timeout={
                                                    indexg == 0
                                                      ? 0
                                                      : indexg * 500
                                                  }
                                                >
                                                  <div style={{}}>
                                                    <GeneroCard
                                                      generosList={generos}
                                                      onGenreList={(
                                                        info,
                                                        isSub
                                                      ) => {
                                                        setGeneroListInfo(info);
                                                        setIsSubGenreList(
                                                          isSub
                                                        );
                                                        setShowModalListGenre(
                                                          true
                                                        );
                                                      }}
                                                      genreIdsSelectEffect={
                                                        genreIdsSelectEffect
                                                      }
                                                      onGeneroSelect={(
                                                        list
                                                      ) => {
                                                        /* if (
                                                    genreIdsSelect.length === 0
                                                  ) {
                                                    setMusicsGenresSelect([]);
                                                    setMusicsGenreSubSelect([]);
                                                  } */
                                                        if (
                                                          genreIdsSelect.filter(
                                                            (g) => g == list.id
                                                          ).length > 0
                                                        ) {
                                                          if (
                                                            (genreSubIdsSelect.length ===
                                                              0) &
                                                            (genreIdsSelect.length ===
                                                              1)
                                                          ) {
                                                            showMessage(
                                                              "Seleção de Generos",
                                                              "Para você não ficar sem nenhuma musica em sua lista, favor selecione um genero antes de desmarcar este.",
                                                              "warning"
                                                            );
                                                          } else {
                                                            // setIsLoadingGenre(true);
                                                            /* setGenreSubIdsSelect([]);
                                                      setMusicsGenreSubSelect(
                                                        []
                                                      ); */
                                                            let dataSelect =
                                                              genreIdsSelectEffect.filter(
                                                                (gi, index) =>
                                                                  index > -1
                                                              );
                                                            dataSelect.push(
                                                              list
                                                            );

                                                            setGenreIdsSelectEffect(
                                                              dataSelect
                                                            );

                                                            setGenreIdsSelect(
                                                              genreIdsSelect.filter(
                                                                (g) =>
                                                                  g != list.id
                                                              )
                                                            );

                                                            setGenrePendent(
                                                              Right.Genres.filter(
                                                                (g) =>
                                                                  g.id ==
                                                                  genreIdsSelect[0]
                                                              )[0]
                                                            );

                                                            setMusicsGenresSelect(
                                                              musicsGenresSelect.filter(
                                                                (m) =>
                                                                  m
                                                                    ? m.genreId !=
                                                                      list.id
                                                                    : false
                                                              )
                                                            );
                                                            //setIsLoadingGenre(false);
                                                          }
                                                        } else {
                                                          /* setGenreSubIdsSelect([]);
                                                    setMusicsGenreSubSelect([]); */
                                                          let dataSelect =
                                                            genreIdsSelectEffect.filter(
                                                              (gi, index) =>
                                                                index > -1
                                                            );
                                                          dataSelect.push(list);
                                                          setGenreIdsSelectEffect(
                                                            dataSelect
                                                          );
                                                          // setGenreIndexEffect(genreIndexEffect + 1)
                                                          // setIsLoadingGenre(true);
                                                          let arrayGenreId =
                                                            genreIdsSelect;
                                                          arrayGenreId.push(
                                                            list.id
                                                          );
                                                          setGenreIdsSelect(
                                                            arrayGenreId
                                                          );
                                                          setGenrePendent(list);
                                                          onSumMusic(list);
                                                          //onGeneroSelect(list);
                                                        }
                                                      }}
                                                      genreIdsSelect={
                                                        genreIdsSelect
                                                      }
                                                    />
                                                  </div>
                                                </Grow>
                                              ))}
                                          </Box>
                                        </div>
                                      </PerfectScrollbar>
                                    </div>
                                  </div>
                                ) : null;
                              })
                          : null}

                        {!showListMusicView &
                          (typeGroupSelect == "sugeridas") &&
                          typeSugListMobile.current.map((sub, index) => {
                            return (
                              <div>
                                <h4
                                  style={{ marginTop: 15 }}
                                  className="text-white mb-3"
                                >
                                  {sub.name}
                                </h4>
                                <div>
                                  <Box
                                    display="flex"
                                    justifyContent="space-between"
                                    position="absolute"
                                    zIndex={100}
                                    ml={-1.5}
                                    mr={0}
                                    mt={10}
                                    width={(window.screen.width * 98) / 100}
                                  >
                                    <IconButton
                                      onClick={() => {
                                        console.log(scrollEl);
                                        if (scrollEl[String(index)]) {
                                          scrollEl[String(index)].scrollLeft =
                                            scrollEl[String(index)].scrollLeft -
                                            200;
                                        }
                                      }}
                                      size="small"
                                      style={{ backgroundColor: "white" }}
                                    >
                                      <ArrowBackIosIcon />
                                    </IconButton>
                                    <IconButton
                                      onClick={() => {
                                        console.log(scrollEl);
                                        if (scrollEl[String(index)]) {
                                          scrollEl[String(index)].scrollLeft =
                                            scrollEl[String(index)].scrollLeft +
                                            200;
                                        }
                                      }}
                                      size="small"
                                      style={{ backgroundColor: "white" }}
                                    >
                                      <ArrowForwardIosIcon />
                                    </IconButton>
                                  </Box>
                                  <PerfectScrollbar
                                    containerRef={(ref) => {
                                      scrollFixSugere = {
                                        ...scrollFixSugere,
                                        [String(index)]: ref,
                                      };

                                      setScrollEl(scrollFixSugere);
                                    }}
                                  >
                                    <div
                                      style={{
                                        width: (window.screen.width * 90) / 100,
                                      }}
                                    >
                                      <Box
                                        display="flex"
                                        style={{ width: "100%" }}
                                      >
                                        {Right.Genresubs.filter(
                                          (g) =>
                                            sub.Genresubcontroltopics.filter(
                                              (s) => s.genresubId == g.id
                                            ).length > 0
                                        ).map((generos) => (
                                          <GeneroSubCard
                                            generosList={generos}
                                            onGenreList={(info, isSub) => {
                                              setGeneroListInfo(info);
                                              setIsSubGenreList(isSub);
                                              setShowModalListGenre(true);
                                            }}
                                            genreIdsSelectEffect={
                                              genreSubIdsSelectEffect
                                            }
                                            onGeneroSelect={async (list) => {
                                              /* if (
                                                  genreSubIdsSelect.length === 0
                                                ) {
                                                  setMusicsGenreSubSelect([]);
                                                  setMusicsGenresSelect([]);
                                                } */
                                              if (
                                                genreSubIdsSelect.filter(
                                                  (g) => g == list.id
                                                ).length > 0
                                              ) {
                                                if (
                                                  (genreSubIdsSelect.length ===
                                                    1) &
                                                  (genreIdsSelect.length === 0)
                                                ) {
                                                  showMessage(
                                                    "Seleção de Generos",
                                                    "Para você não ficar sem nenhuma musica em sua lista, favor selecione um genero antes de desmarcar este.",
                                                    "warning"
                                                  );
                                                } else {
                                                  // setIsLoadingGenre(true);
                                                  let dataSelect =
                                                    genreSubIdsSelectEffect.filter(
                                                      (gi, index) => index > -1
                                                    );
                                                  dataSelect.push(list);

                                                  setGenreSubIdsSelectEffect(
                                                    dataSelect
                                                  );

                                                  setGenreSubIdsSelect(
                                                    genreSubIdsSelect.filter(
                                                      (g) => g != list.id
                                                    )
                                                  );

                                                  console.log(
                                                    "Chegou nesse aqui"
                                                  );
                                                  setGenreSubPendent(
                                                    heapList[0].Rights.Genresubs.filter(
                                                      (g) =>
                                                        g.id ==
                                                        genreSubIdsSelect.filter(
                                                          (g) => g == list.id
                                                        )[0]
                                                    )[0]
                                                  );

                                                  let resSubItem =
                                                    await api.get(
                                                      `/genresubitem/genresub/${list.id}/pure`
                                                    );

                                                  console.log(
                                                    resSubItem.data,
                                                    musicsGenreSubSelect
                                                  );

                                                  setMusicsGenresSelect(
                                                    musicsGenresSelect.filter(
                                                      (m) =>
                                                        m
                                                          ? resSubItem.data.filter(
                                                              (s) =>
                                                                s.musicId ==
                                                                m.id
                                                            ).length == 0
                                                          : false
                                                    )
                                                  );
                                                  //setIsLoadingGenre(false);
                                                }
                                              } else {
                                                let dataSelect =
                                                  genreSubIdsSelectEffect.filter(
                                                    (gi, index) => index > -1
                                                  );
                                                dataSelect.push(list);
                                                setGenreSubIdsSelectEffect(
                                                  dataSelect
                                                );
                                                // setGenreIndexEffect(genreIndexEffect + 1)
                                                // setIsLoadingGenre(true);
                                                let arrayGenreId =
                                                  genreSubIdsSelect;
                                                arrayGenreId.push(list.id);
                                                setGenreSubIdsSelect(
                                                  arrayGenreId
                                                );
                                                console.log(
                                                  "Chegou nesse aqui de baixo"
                                                );
                                                setGenreSubPendent(list);
                                                onSumMusicSub(list);
                                                //onGeneroSelect(list);
                                              }
                                            }}
                                            genreSubIdsSelect={
                                              genreSubIdsSelect
                                            }
                                          />
                                        ))}
                                      </Box>
                                    </div>
                                  </PerfectScrollbar>
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    ) : (
                      <div
                        style={{
                          paddingBottom: 30,
                        }}
                        className="mb-4  mt-4"
                      >
                        {!showListMusicView & (typeGroupSelect == "genero")
                          ? typeSubListMobile
                              .sort((a, b) => {
                                if (a.sub < b.sub) return -1;
                                if (a.sub > b.sub) return 1;
                                return 0;
                              })
                              .slice(
                                0 * limitSubGenreList,
                                0 * limitSubGenreList + limitSubGenreList
                              )
                              .map(({ sub }, index) => {
                                return Right.Genres.filter((g) =>
                                  sub == "Variedades"
                                    ? typeSubListMobile.filter(
                                        (typeSub) =>
                                          String(g.name)
                                            .toLowerCase()
                                            .indexOf(
                                              String(typeSub.sub).toLowerCase()
                                            ) == -1
                                      ).length == typeSubListMobile.length
                                    : String(g.name).indexOf(sub) > -1
                                ).length > 0 ? (
                                  <div
                                    style={{
                                      marginBottom: 70,
                                    }}
                                  >
                                    <h2
                                      style={{
                                        fontWeight: "600",
                                        color: "white",
                                      }}
                                      className="mb-3 ml-4"
                                    >
                                      {sub}
                                    </h2>
                                    {/* <PerfectScrollbar
                                      //style={{ maxWidth: 1300 }}
                                    > */}

                                    <Box
                                      style={{
                                        maxWidth:
                                          (window.screen.width * 94) / 100 >
                                          1300
                                            ? 1300
                                            : (window.screen.width * 94) / 100,
                                      }}
                                      display="flex"
                                      flexWrap="wrap"
                                      mt={5}
                                      ml={6}
                                      mr={4}
                                    >
                                      {Right.Genres.filter((g) =>
                                        sub == "Variedades"
                                          ? typeSubListMobile.filter(
                                              (typeSub) =>
                                                String(g.name)
                                                  .toLowerCase()
                                                  .indexOf(
                                                    String(
                                                      typeSub.sub
                                                    ).toLowerCase()
                                                  ) == -1
                                            ).length ==
                                            typeSubListMobile.length /* (String(g.name)
                                              .toLowerCase()
                                              .indexOf(
                                                "alternativo nacional"
                                              ) ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("bossa nova") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("mpb clássicas") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("nova mpb") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("axé") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("brega") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("chillout lounge") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("católica") ==
                                              -1) |
                                            (String(g.name)
                                              .toLowerCase()
                                              .indexOf("bpm") ==
                                              -1) */
                                          : String(g.name).indexOf(sub) > -1
                                      )
                                        .sort((a, b) => {
                                          return a.name < b.name
                                            ? -1
                                            : a.name > b.name
                                            ? 1
                                            : 0;
                                        })
                                        .map((generos, indexg) => (
                                          <Grow
                                            mountOnEnter
                                            unmountOnExit
                                            in={true}
                                            timeout={
                                              indexg == 0 ? 0 : indexg * 500
                                            }
                                          >
                                            <div style={{ marginBottom: 20 }}>
                                              <GeneroCard
                                                generosList={generos}
                                                onGenreList={(info, isSub) => {
                                                  setGeneroListInfo(info);
                                                  setIsSubGenreList(isSub);
                                                  setShowModalListGenre(true);
                                                }}
                                                genreIdsSelectEffect={
                                                  genreIdsSelectEffect
                                                }
                                                onGeneroSelect={async (
                                                  list
                                                ) => {
                                                  /* if (
                                                    genreIdsSelect.length === 0
                                                  ) {
                                                    setMusicsGenresSelect([]);
                                                    setMusicsGenreSubSelect([]);
                                                  } */
                                                  if (
                                                    genreIdsSelect.filter(
                                                      (g) => g == list.id
                                                    ).length > 0
                                                  ) {
                                                    if (
                                                      (genreSubIdsSelect.length ===
                                                        0) &
                                                      (genreIdsSelect.length ===
                                                        1)
                                                    ) {
                                                      showMessage(
                                                        "Seleção de Generos",
                                                        "Para você não ficar sem nenhuma musica em sua lista, favor selecione um genero antes de desmarcar este.",
                                                        "warning"
                                                      );
                                                    } else {
                                                      // setIsLoadingGenre(true);
                                                      /* setGenreSubIdsSelect([]);
                                                      setMusicsGenreSubSelect(
                                                        []
                                                      ); */

                                                      let dataSelect =
                                                        genreIdsSelectEffect.filter(
                                                          (gi, index) =>
                                                            index > -1
                                                        );
                                                      dataSelect.push(list);

                                                      setGenreIdsSelectEffect(
                                                        dataSelect
                                                      );

                                                      setGenreIdsSelect(
                                                        genreIdsSelect.filter(
                                                          (g) => g != list.id
                                                        )
                                                      );

                                                      setGenrePendent(
                                                        Right.Genres.filter(
                                                          (g) =>
                                                            g.id ==
                                                            genreIdsSelect[0]
                                                        )[0]
                                                      );

                                                      console.log(
                                                        genreIdsSelect[0]
                                                      );
                                                      console.log(Right.Genres);

                                                      setMusicsGenresSelect(
                                                        musicsGenresSelect.filter(
                                                          (m) =>
                                                            m
                                                              ? m.genreId !=
                                                                list.id
                                                              : false
                                                        )
                                                      );
                                                      //setIsLoadingGenre(false);
                                                    }
                                                  } else {
                                                    /* setGenreSubIdsSelect([]);
                                                    setMusicsGenreSubSelect([]); */
                                                    if (
                                                      list.pejorative == "1"
                                                    ) {
                                                      const yes = await yesno({
                                                        labelYes: "Sim",
                                                        labelNo: "Não",
                                                        bodyText:
                                                          "Nesse gênero contém termos pejorativos, deseja selecionar assim mesmo ?",
                                                      });

                                                      if (!yes) {
                                                        return;
                                                      }
                                                    }

                                                    let dataSelect =
                                                      genreIdsSelectEffect.filter(
                                                        (gi, index) =>
                                                          index > -1
                                                      );
                                                    dataSelect.push(list);
                                                    setGenreIdsSelectEffect(
                                                      dataSelect
                                                    );
                                                    // setGenreIndexEffect(genreIndexEffect + 1)
                                                    // setIsLoadingGenre(true);
                                                    let arrayGenreId =
                                                      genreIdsSelect;
                                                    arrayGenreId.push(list.id);
                                                    setGenreIdsSelect(
                                                      arrayGenreId
                                                    );
                                                    setGenrePendent(list);
                                                    onSumMusic(list);
                                                    //onGeneroSelect(list);
                                                  }
                                                }}
                                                genreIdsSelect={genreIdsSelect}
                                              />
                                            </div>
                                          </Grow>
                                        ))}
                                    </Box>
                                  </div>
                                ) : null;
                              })
                          : null}

                        {!showListMusicView &
                        (typeGroupSelect == "sugeridas")
                          ? typeSugListMobile
                               .slice(
                                0 * limitSubGenreList,
                                0 * limitSubGenreList + limitSubGenreList
                              ) 
                              .map((sub, index) => {
                                return Right.Genresubs.filter(
                                  (g) =>
                                    sub.Genresubcontroltopics.filter(
                                      (s) => s.genresubId == g.id
                                    ).length > 0
                                ).length > 0 ? (
                                  <div
                                    style={{
                                      marginBottom: 70,
                                    }}
                                  >
                                    <h2
                                      style={{
                                        fontWeight: "600",
                                        color: "white",
                                      }}
                                      className="mb-3 ml-4"
                                    >
                                      {sub.name}
                                    </h2>
                                    {/* <PerfectScrollbar
                                      style={{ maxWidth: 1300 }}
                                    > */}
                                    <Box
                                      display="flex"
                                      flexWrap="wrap"
                                      mt={5}
                                      style={{
                                        maxWidth:
                                          (window.screen.width * 94) / 100,
                                      }}
                                      display="flex"
                                      flexWrap="wrap"
                                      mt={5}
                                      ml={6}
                                      mr={4}
                                    >
                                      {Right.Genresubs.filter(
                                        (g) =>
                                          sub.Genresubcontroltopics.filter(
                                            (s) => s.genresubId == g.id
                                          ).length > 0
                                      ).map((generos, indexg) => (
                                        <Grow
                                          mountOnEnter
                                          unmountOnExit
                                          in={true}
                                          timeout={
                                            indexg == 0 ? 0 : indexg * 500
                                          }
                                        >
                                          <div>
                                            <GeneroSubCard
                                              generosList={generos}
                                              onGenreList={(info, isSub) => {
                                                setGeneroListInfo(info);
                                                setIsSubGenreList(isSub);
                                                setShowModalListGenre(true);
                                              }}
                                              genreIdsSelectEffect={
                                                genreSubIdsSelectEffect
                                              }
                                              onGeneroSelect={async (list) => {
                                                /* if (
                                                  genreSubIdsSelect.length === 0
                                                ) {
                                                  setMusicsGenreSubSelect([]);
                                                  setMusicsGenresSelect([]);
                                                } */
                                                if (
                                                  genreSubIdsSelect.filter(
                                                    (g) => g == list.id
                                                  ).length > 0
                                                ) {
                                                  if (
                                                    (genreSubIdsSelect.length ===
                                                      1) &
                                                    (genreIdsSelect.length ===
                                                      0)
                                                  ) {
                                                    showMessage(
                                                      "Seleção de Generos",
                                                      "Para você não ficar sem nenhuma musica em sua lista, favor selecione um genero antes de desmarcar este.",
                                                      "warning"
                                                    );
                                                  } else {
                                                    // setIsLoadingGenre(true);
                                                    let dataSelect =
                                                      genreSubIdsSelectEffect.filter(
                                                        (gi, index) =>
                                                          index > -1
                                                      );
                                                    dataSelect.push(list);

                                                    setGenreSubIdsSelectEffect(
                                                      dataSelect
                                                    );

                                                    setGenreSubIdsSelect(
                                                      genreSubIdsSelect.filter(
                                                        (g) => g != list.id
                                                      )
                                                    );

                                                    console.log(
                                                      "Chegou nesse aqui de baixo 3°"
                                                    );
                                                    console.log(
                                                      genreSubIdsSelect
                                                    );
                                                    console.log(
                                                      genreSubIdsSelect[0]
                                                    );
                                                    console.log(
                                                      Right.Genresubs
                                                    );

                                                    setGenreSubPendent(
                                                      Right.Genresubs.filter(
                                                        (g) =>
                                                          g.id ==
                                                          genreSubIdsSelect.filter(
                                                            (g) => g == list.id
                                                          )[0]
                                                      )[0]
                                                    );

                                                    let resSubItem =
                                                      await api.get(
                                                        `/genresubitem/genresub/${list.id}/pure`
                                                      );

                                                    console.log(
                                                      resSubItem.data,
                                                      musicsGenreSubSelect
                                                    );

                                                    setMusicsGenresSelect(
                                                      musicsGenresSelect.filter(
                                                        (m) =>
                                                          m
                                                            ? resSubItem.data.filter(
                                                                (s) =>
                                                                  s.musicId ==
                                                                  m.id
                                                              ).length == 0
                                                            : false
                                                      )
                                                    );
                                                    //setIsLoadingGenre(false);
                                                  }
                                                } else {
                                                  if (list.pejorative == "1") {
                                                    const yes = await yesno({
                                                      labelYes: "Sim",
                                                      labelNo: "Não",
                                                      bodyText:
                                                        "Nesse gênero contém termos pejorativos, deseja selecionar assim mesmo ?",
                                                    });

                                                    if (!yes) {
                                                      return;
                                                    }
                                                  }

                                                  let dataSelect =
                                                    genreSubIdsSelectEffect.filter(
                                                      (gi, index) => index > -1
                                                    );
                                                  dataSelect.push(list);
                                                  setGenreSubIdsSelectEffect(
                                                    dataSelect
                                                  );
                                                  // setGenreIndexEffect(genreIndexEffect + 1)
                                                  // setIsLoadingGenre(true);
                                                  let arrayGenreId =
                                                    genreSubIdsSelect;
                                                  arrayGenreId.push(list.id);
                                                  setGenreSubIdsSelect(
                                                    arrayGenreId
                                                  );
                                                  console.log(
                                                    "Chegou nesse aqui de baixo 4°"
                                                  );
                                                  setGenreSubPendent(list);
                                                  onSumMusicSub(list);
                                                  //onGeneroSelect(list);
                                                }
                                              }}
                                              genreSubIdsSelect={
                                                genreSubIdsSelect
                                              }
                                              isMobile={isMobile}
                                            />
                                          </div>
                                        </Grow>
                                      ))}
                                    </Box>
                                  </div>
                                ) : null;
                              })
                          : null}
                      </div>
                    )}
                  </div>
                )
              )
            )}
          </div>
        </Box>
      </PerfectScrollbar>

      {showListMusicView ? (
        <ListMusicView
          capa={
            !isPlayList
              ? generoSelect.Covers
                ? generoSelect.Covers[0].path_s3
                : "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
              : null
          }
          listMusic={!isPlayList ? generoSelect.Musics : generoSelect.Musics}
          idMusicSelect={numMusicList}
          onPlayMusic={onPlayMusic}
          isPlayList={isPlayList}
          isPlayListSub={isPlayListSub}
          onDesBlock={onDesBlock}
          onDesLikeFavorite={onDesLikeFavorite}
          onLikeFavorite={onLikeFavorite}
          onBlock={onBlock}
          listBlocked={blockedsList}
          listFavorited={favoritedsList}
          listPhrases={listPhrases}
          listSegmentsCovers={listSegmentsCovers}
          client={client}
        />
      ) : null}

      {!showListMusicView & (1 < 0) ? (
        generoSelect ? (
          <div className={"fixed-bottom"}>
            <Box
              className={classes.bottomCapa}
              display="flex"
              justifyContent="flex-end"
            >
              <div
                onClick={() => {
                  // setShowListMusicView(!showListMusicView);
                }}
                className="ct"
              >
                <img
                  alt="Remy Sharp"
                  src={
                    isPlayList
                      ? generoSelect.Itemplaylists[numMusicList].Music.Genre
                          .Covers[0].path_s3
                      : generoSelect.Covers[0].path_s3
                  }
                  className="av-bottom-capa"
                />
                <AspectRatioIcon className="av-aspect-ratio" />
              </div>
            </Box>
          </div>
        ) : null
      ) : null}

      {showPlayerView & isContinue & (String(generoSelectTemp).length > 0) ? (
        <div className="fixed-bottom">
          <Player
            nameMusic={String(
              isPlayList
                ? generoSelect.Musics[numMusicList].title
                  ? generoSelect.Musics[numMusicList].title
                  : ""
                : generoSelect.Musics[numMusicList].title
                ? generoSelect.Musics[numMusicList].title
                : ""
            )
              .replace(".mp3", "")
              .replace(".m4a", "")}
            nameArtista={
              isPlayList
                ? generoSelect.Musics[numMusicList].artist
                : generoSelect.Musics[numMusicList].artist
                ? generoSelect.Musics[numMusicList].artist
                : ""
            }
            capa={
              isPlayList
                ? generoSelect.Musics[numMusicList].Genre.Covers[0].path_s3
                : generoSelect.Musics[numMusicList].Genre
                ? generoSelect.Musics[numMusicList].Genre.Covers[0].path_s3
                : generoSelect.Covers
                ? generoSelect.Covers[0].path_s3
                : "https://bomjafiles.s3-sa-east-1.amazonaws.com/images/internobomja/sino.png"
            }
            urlMusic={
              isPlayList
                ? generoSelect.Musics[numMusicList].url_m4a_s3
                : generoSelect.Musics[numMusicList].url_m4a_s3
                ? generoSelect.Musics[numMusicList].url_m4a_s3
                : ""
            }
            showListMusic={showListMusicView}
            onShowListMusicView={() => {
              setShowListMusicView(!showListMusicView);
            }}
            numMusicList={numMusicList}
            numMusicListPassed={numMusicListPassed}
            genreSelect={generoSelect}
            genreName={
              generoSelect.Musics[numMusicList].Genre
                ? generoSelect.Musics[numMusicList].Genre.name
                : generoSelect.Musics[numMusicList].artist
            }
            clientId={client.id}
            onNextMusic={() => {
              console.log(generoSelectTemp);
              nextMusic();
            }}
            onBackMusic={backMusic}
            onHoraCertaPhrase={onHoraCertaPhrase}
            onDesBlock={onDesBlock}
            onDesLikeFavorite={onDesLikeFavorite}
            onLikeFavorite={() =>
              onLikeFavorite(
                isPlayList
                  ? generoSelect.Musics[numMusicList].id
                  : generoSelect.Musics[numMusicList].id,
                isPlayList
                  ? generoSelect.Musics[numMusicList].artist
                  : generoSelect.Musics[numMusicList].artist
              )
            }
            onBlock={() =>
              onBlock(
                isPlayList
                  ? generoSelect.Musics[numMusicList].id
                  : generoSelect.Musics[numMusicList].id,
                isPlayList
                  ? generoSelect.Musics[numMusicList].artist
                  : generoSelect.Musics[numMusicList].artist
              )
            }
            listBlocked={blockedsList}
            listFavorited={favoritedsList}
            idMusic={
              isPlayList
                ? generoSelect.Musics[numMusicList].id
                : generoSelect.Musics[numMusicList].id
            }
            playingSpeech={playingSpeechRef.current}
            isPauseAuto={isPauseAuto}
            playingNow={playingNow}
            onPlayingAuto={onPlayingAuto}
          />
        </div>
      ) : null}

      <ModalSpeech
        clientId={client.id}
        onCloseSpeech={onCloseSpeech}
        open={showModalSpeech}
        onPlayingSpeech={onSetPlayingSpeech}
        isMobile={isMobile}
      />

      <ModalOnline open={showModalOnline} />
      <ModalBlock open={showModalBlock} />
      <ModalBlockExpired open={showModalBlockExpired} />

      <ModalConfigConteudo
        selectId={client.id}
        getAllChannel={() => getClient()}
        isMobile={JSON.parse(isMobile)}
        getSettingChannelModify={channelSetting}
        onCloseSettingChannel={onCloseConfigConteudo}
        open={showModalConfigConteudo}
        getInfoClient={client}
        indexValuesPreviewList={indexValuesPreviewList}
      />

      <ModalSuporteWhatsApp
        open={showSuporteWhatsapp}
        onClose={() => {
          setShowSuporteWhatsapp(false);
        }}
      />

      {showModalListGenre ? (
        <ModalListGenre
          open={showModalListGenre}
          nameGenre="Album Bomja"
          qtdMusic={0}
          onDesBlock={onDesBlock}
          onDesLikeFavorite={onDesLikeFavorite}
          onLikeFavorite={onLikeFavorite}
          onBlock={onBlock}
          listBlocked={blockedsList}
          listFavorited={favoritedsList}
          isSubGenre={isSubGenreList}
          audioQualityId={channelSetting ? channelSetting.audioQualityId : 0}
          listMusic={generoListInfo}
          onSelect={async (list, isSub) => {
            setShowModalListGenre(false);
            if (genreSubIdsSelect.filter((g) => g == list.id).length > 0) {
              if (
                (genreSubIdsSelect.length === 1) &
                (genreIdsSelect.length === 0)
              ) {
                showMessage(
                  "Seleção de Generos",
                  "Para você não ficar sem nenhuma musica em sua lista, favor selecione um genero antes de desmarcar este.",
                  "warning"
                );
              } else {
                // setIsLoadingGenre(true);
                let dataSelect = genreSubIdsSelectEffect.filter(
                  (gi, index) => index > -1
                );
                dataSelect.push(list);

                setGenreSubIdsSelectEffect(dataSelect);

                setGenreSubIdsSelect(
                  genreSubIdsSelect.filter((g) => g != list.id)
                );

                setGenreSubPendent(
                  heapList[0].Rights[0].Genresubs.filter(
                    (g) =>
                      g.id == genreSubIdsSelect.filter((g) => g == list.id)[0]
                  )[0]
                );

                let resSubItem = await api.get(
                  `/genresubitem/genresub/${list.id}/pure`
                );

                console.log(resSubItem.data, musicsGenreSubSelect);

                setMusicsGenresSelect(
                  musicsGenresSelect.filter((m) =>
                    m
                      ? resSubItem.data.filter((s) => s.musicId == m.id)
                          .length == 0
                      : false
                  )
                );
              }
            } else {
              if (list.pejorative == "1") {
                const yes = await yesno({
                  labelYes: "Sim",
                  labelNo: "Não",
                  bodyText:
                    "Nesse gênero contém termos pejorativos, deseja selecionar assim mesmo ?",
                });

                if (!yes) {
                  return;
                }
              }

              let dataSelect = genreSubIdsSelectEffect.filter(
                (gi, index) => index > -1
              );
              dataSelect.push(list);
              setGenreSubIdsSelectEffect(dataSelect);
              // setGenreIndexEffect(genreIndexEffect + 1)
              // setIsLoadingGenre(true);
              let arrayGenreId = genreSubIdsSelect;
              arrayGenreId.push(list.id);
              setGenreSubIdsSelect(arrayGenreId);
              setGenreSubPendent(list);
              onSumMusicSub(list);

              //onGeneroSelect(list);
            }
          }}
          onClose={() => {
            setShowModalListGenre(false);
          }}
        />
      ) : null}

      <ModalSuportContact
        selectId={client.id}
        getAllChannel={() => getClient()}
        isMobile={JSON.parse(isMobile)}
        getSettingChannelModify={channelSetting}
        onCloseContentChannel={onCloseSuportContact}
        open={showSuportContact}
        getInfoClient={client}
      />

      <ModalNewEmployee
        onCloseNewEmployee={onCloseNewEmployee}
        open={showModalNewEmployee}
      />

      <ModalNewPlayList
        onCloseNewPlayList={onCloseNewPlayList}
        open={showModalNewPlayList}
        heapLists={heapList}
        clientId={client.id}
        onGetPlayList={getPlayList}
        isMobile={isMobile}
        listMusicCompleteArray={listMusicComplete}
      />

      <ModalEditPlayList
        onCloseEditPlayList={onCloseEditPlayList}
        open={showModalEditPlayList}
        heapLists={heapList}
        onGetPlayList={getPlayList}
        editPlayLists={editPlayList}
        isMobile={isMobile}
        listMusicCompleteArray={listMusicComplete}
      />

      <ModalContentChannel
        open={showContentChannel}
        onCloseContentChannel={onCloseContentChannel}
        contentSelectChannel={contentSelectChannel}
        commercials={commercials}
        idClient={client.id}
        isMobile={JSON.parse(isMobile)}
        listBlocked={blockedsList}
        onGetBlockeds={onGetBlockeds}
      />

      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a Playlist será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />

      <Backdrop
        //style={{ zIndex: 10000, color: "#fff" }}
        className={classes.backdrop}
        open={isLoadingGenre}
        transitionDuration={0}
        timeout={0}
      >
        <Box display="flex" justifyContent="center">
          <CircularProgress style={{ color: "red" }} />
        </Box>
      </Backdrop>

      <Drawer
        isShowMenu={isShowMenu}
        onClick={() => {
          setIsShowMenu(false);
        }}
        onKeyDown={() => {
          setIsShowMenu(false);
        }}
        onClose={() => {
          setIsShowMenu(false);
        }}
        onNewPlaylist={() => {
          setShowModalNewPlayList(true);
        }}
        onChamada={() => {
          onOpenSpeech();
        }}
        onConfigContent={() => {
          onOpenConfigConteudo();
        }}
        onShowContent={() => {
          onShowContentChannel();
        }}
        onDireito={() => {
          setGroupSelect("Com Direitos");
        }}
        onLivre={() => {
          setGroupSelect("Creative Commons");
          setTypeGroupSelect("sugeridas");
        }}
        onPlayList={() => {
          setGroupSelect("Minhas Playlist");
        }}
        onViewFavorited={() => {
          setGroupSelect("Favoritos");
        }}
        onViewBlock={() => {
          setGroupSelect("Bloqueados");
        }}
        onShowSuportContact={() => {
          setShowSuportContact(true);
        }}
        onSuporteWhatsapp={() => {
          setShowSuporteWhatsapp(true);
        }}
        onResetSelected={() => {
          setIsPlayList(false);
          setIsPlayListSub(false);
          setNumMusicList(0);
          setGenreIdsSelect([]);
          setGenreSubIdsSelect([87]);

          onSubSelect(
            {
              id: 87,
              name: "Hot Lançamentos",
              typeSegment: "todos",
              rightId: 1,
              createdAt: "2021-07-12 21:39:29",
              updatedAt: "2021-07-12 21:39:29",
              RightId: 1,
              countMusic: 153,
            },
            null,
            true,
            true
          );
        }}
      />
    </div>
  );
};

function numKeys(obj) {
  var count = 0;
  for (var prop in obj) {
    count++;
  }
  return count;
}

export default Radio;
