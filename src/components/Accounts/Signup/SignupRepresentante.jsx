import React, { useState } from "react";
import Header from "../../HeaderSection/Header";
import { login } from "../../../services/auth";
import { useNavigate } from "react-router-dom";
import api from "../../../services/api";
import Checkbox from "@material-ui/core/Checkbox";

function Signup() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password_confirmation, setPassword_confirmation] = useState("");
  const [checked_term, setChecked] = React.useState(false);
  const [error, setError] = useState("");
  let history = useNavigate();

  const initData = {
    heading: "Criar uma conta !",
    content:
      "Falta pouco, para que possamos lhe auxiliar e lhe mostrar um caminho diferenciado de comunicação para impressionar seus clientes.",
    formHeading: "Sign Up",
    formContent:
      "Preencha todos os campos para que possamos obter algumas informações sobre você. Nunca enviaremos spam para você",
    formText: "Ja tenho um cadastro?",
    btnText: "Cadastrar",
    btnText_2: "Login",
  };

  const handleRegistro = async () => {
    if (
      !name ||
      !email ||
      !password ||
      !password_confirmation ||
      !checked_term
    ) {
      setError("Preencha todos os dados para se cadastrar");
    } else {
      try {
        const res = await api.post("/user/register", {
          name,
          email,
          password,
          accept_term: true,
          typeuserId: 2,
        });
        if (res.data.message == "success") {
          const data = [
            { accessId: 1, level: 4, userId: res.data.data.id },
            { accessId: 2, level: 4, userId: res.data.data.id },
            { accessId: 3, level: 4, userId: res.data.data.id },
          ];

          api
            .post("/accesscontrol/new", { data: data })
            .then((response) => response.data)
            .then((resp) => {
              if (resp.message == "success") {
              }
            })
            .catch((error) => {
              console.log(error);
            });

          history("/registro-concluido");
        }
      } catch (err) {
        console.log(err);
        setError("Ocorreu um erro ao registrar sua conta. T.T");
      }
    }
  };

  const handleChangeCheck = (event) => {
    setChecked(event.target.checked);
  };

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        handleRegistro();
      }}
    >
      <div className="accounts inner-pages signup">
        <div className="main">
          {/* <Header imageData={"/img/logo.png"} /> */}
          <section
            id="home"
            className="section welcome-area h-100vh bg-overlay d-flex align-items-center"
          >
            <div className="container">
              <div className="row align-items-center justify-content-center">
                {/* Welcome Intro Start */}
                <div className="col-12 col-lg-7">
                  <div className="welcome-intro">
                    <h1 className="text-white">{initData.heading}</h1>
                    <p className="text-white my-4">{initData.content}</p>
                  </div>
                </div>
                <div className="col-12 col-md-8 col-lg-5">
                  {/* Contact Box */}
                  <div className="contact-box bg-white text-center rounded p-4 p-sm-5 mt-5 mt-lg-0 shadow-lg">
                    {/* Contact Form */}
                    <form id="contact-form">
                      <div className="contact-top">
                        <h3 className="contact-title">
                          {initData.formHeading}
                        </h3>
                        <h5 className="text-secondary fw-3 py-3">
                          {initData.formContent}
                        </h5>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="form-group">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-user-alt" />
                                </span>
                              </div>
                              <input
                                type="text"
                                className="form-control"
                                name="name"
                                placeholder="Nome Completo"
                                required="required"
                                value={name}
                                onChange={(event) => {
                                  setName(event.target.value);
                                }}
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-envelope-open" />
                                </span>
                              </div>
                              <input
                                type="email"
                                className="form-control"
                                name="email"
                                placeholder="E-mail"
                                required="required"
                                value={email}
                                onChange={(event) => {
                                  setEmail(event.target.value);
                                }}
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-unlock-alt" />
                                </span>
                              </div>
                              <input
                                type="password"
                                className="form-control"
                                name="password"
                                placeholder="Senha"
                                required="required"
                                value={password}
                                onChange={(event) => {
                                  setPassword(event.target.value);
                                }}
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-unlock-alt" />
                                </span>
                              </div>
                              <input
                                type="password"
                                className="form-control"
                                name="password_confirmation"
                                placeholder="Confirmar Senha"
                                required="required"
                                value={password_confirmation}
                                onChange={(event) => {
                                  setPassword_confirmation(event.target.value);
                                }}
                              />
                            </div>
                          </div>
                          <div className="form-group form-check d-flex align-center">
                            <Checkbox
                              checked={checked_term}
                              required={true}
                              onChange={handleChangeCheck}
                              className="mt-3"
                              inputProps={{ "aria-label": "primary checkbox" }}
                            />
                            <label
                              className="form-check-label contact-bottom"
                              htmlFor="exampleCheck1"
                            >
                              <span className="d-inline-block mt-3">
                                Aceito os <a href="/#">Termos</a> &amp;{" "}
                                <a href="/#">Política de Privacidade</a>
                              </span>
                            </label>
                          </div>
                        </div>
                        <div className="col-12">
                          <button
                            className="btn btn-bordered w-100 mt-3"
                            type="submit"
                          >
                            {initData.btnText}
                          </button>
                        </div>
                        <div className="col-12">
                          <span className="d-block pt-2 mt-4 border-top">
                            {initData.formText}{" "}
                            <a href="/Login">{initData.btnText_2}</a>
                          </span>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* Shape Bottom */}
            <div className="shape-bottom">
              <svg
                viewBox="0 0 1920 310"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                className="svg replaced-svg"
              >
                <title>Bomja Shape</title>
                <desc>Created with Sketch</desc>
                <defs />
                <g
                  id="sApp-Landing-Page"
                  stroke="none"
                  strokeWidth={1}
                  fill="none"
                  fillRule="evenodd"
                >
                  <g
                    id="sApp-v1.0"
                    transform="translate(0.000000, -554.000000)"
                    fill="#FFFFFF"
                  >
                    <path
                      d="M-3,551 C186.257589,757.321118 319.044414,856.322454 395.360475,848.004007 C509.834566,835.526337 561.525143,796.329212 637.731734,765.961549 C713.938325,735.593886 816.980646,681.910577 1035.72208,733.065469 C1254.46351,784.220361 1511.54925,678.92359 1539.40808,662.398665 C1567.2669,645.87374 1660.9143,591.478574 1773.19378,597.641868 C1848.04677,601.75073 1901.75645,588.357675 1934.32284,557.462704 L1934.32284,863.183395 L-3,863.183395"
                      id="sApp-v1.0"
                    />
                  </g>
                </g>
              </svg>
            </div>
          </section>
        </div>
      </div>
    </form>
  );
}

export default Signup;
