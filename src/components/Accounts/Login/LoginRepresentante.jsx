import React, { useState } from "react";
import Header from "../../HeaderSection/Header";
import api from "../../../services/api";
import { login } from "../../../services/auth";
import { useNavigate } from "react-router-dom";
import { store } from "react-notifications-component";
import { useCookies } from "react-cookie";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [accessControls, setAccessControls] = useCookies(["AccessControls"]);
  const [planControls, setPlanControls] = useCookies(["PlanControls"]);
  const [isManager, setIsManager] = useCookies(["IsManager"]);
  let history = useNavigate();

  const infoData = {
    heading: "Seja Bem Vindo!",
    content:
      "",
    formHeading: "Login",
    formContent:
      "Preencha todos os campos para que possamos obter algumas informações sobre você. Nunca enviaremos spam para você",
    formText: "Não tenho cadastro?",
    btnText: "Entrar",
    btnText_2: "Cadastrar-se",
    btnText_3: "Esqueceu sua senha?",
  };

  const handleLogin = async () => {
    if (!email || !password) {
      setError("Preencha e-mail e senha para continuar!");
    } else {
      try {
        const res = await api.post("/login", {
          email,
          password,
        });

        if (String(res.data.data.Typeuser.name).toLowerCase() == "painel") {
          login(res.data.token);

          console.log(res.data);

          setAccessControls(
            "AccessControls",
            JSON.stringify(res.data.data.Accesscontrols),
            { path: "/" }
          );
          setPlanControls(
            "PlanControls",
            JSON.stringify(
              res.data.data.Business
                ? res.data.data.Business.Plancontrols[0]
                : res.data.data.Plancontrols[0]
            ),
            { path: "/" }
          );

          setIsManager(
            "IsManager",
            res.data.data.Plancontrols.length > 0
              ? JSON.stringify({ status: true, id: res.data.data.id })
              : JSON.stringify({ status: false, id: res.data.data.id }),
            { path: "/" }
          );

          history("/app/radioh?page=home");
        } else {
          setError(
            "Houve um problema com o login, verifique suas credenciais. T.T"
          );
          store.addNotification({
            title: "Login Falhou",
            message:
              "Houve um problema com o login, verefique se suas credenciais estão corretas.",
            type: "danger",
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "bounceIn"],
            animationOut: ["animated", "bounceOut"],
            dismiss: {
              duration: 5000,
              onScreen: true,
            },
          });
        }
      } catch (err) {
        setError(
          "Houve um problema com o login, verifique suas credenciais. T.T"
        );
        store.addNotification({
          title: "Login Falhou",
          message:
            "Houve um problema com o login, verefique se suas credenciais estão corretas.",
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "bounceIn"],
          animationOut: ["animated", "bounceOut"],
          dismiss: {
            duration: 5000,
            onScreen: true,
          },
        });
      }
    }
  };

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        handleLogin();
      }}
    >
      <div className="homepage-5 accounts inner-pages">
        <div className="main">
          {/* <Header imageData={"/img/logo.png"} /> */}
          <section
            id="home"
            className="section welcome-area h-100vh bg-overlay d-flex align-items-center"
          >
            <div className="container">
              <div className="row align-items-center justify-content-center">
                {/* Welcome Intro Start */}
                <div className="col-12 col-lg-7">
                  <div className="welcome-intro">
                    <h1 className="text-white">{infoData.heading}</h1>
                    <p className="text-white my-4">{infoData.content}</p>
                  </div>
                </div>
                <div className="col-12 col-md-8 col-lg-5">
                  {/* Contact Box */}
                  <div className="contact-box bg-white text-center rounded p-4 p-sm-5 mt-5 mt-lg-0 shadow-lg">
                    {/* Contact Form */}
                    <form id="contact-form">
                      <div className="contact-top">
                        <h3 className="contact-title">
                          {infoData.formHeading}
                        </h3>
                        <h5 className="text-secondary fw-3 py-3">
                          {infoData.formContent}
                        </h5>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="form-group">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-envelope-open" />
                                </span>
                              </div>
                              <input
                                type="email"
                                className="form-control"
                                name="email"
                                placeholder="E-mail"
                                required="required"
                                value={email}
                                onChange={(event) => {
                                  setEmail(event.target.value);
                                }}
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="input-group-text">
                                  <i className="fas fa-unlock-alt" />
                                </span>
                              </div>
                              <input
                                type="password"
                                className="form-control"
                                name="password"
                                placeholder="Senha"
                                required="required"
                                value={password}
                                onChange={(event) => {
                                  setPassword(event.target.value);
                                }}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="col-12">
                          <button
                            className="btn btn-bordered w-100 mt-3 mt-sm-4"
                            type="submit"
                          >
                            {infoData.btnText}
                          </button>
                          <div className="contact-bottom">
                            <span className="d-inline-block mt-3">
                              Ao logar-se, você aceita nossos{" "}
                              <a href="/#">Termos</a> &amp;{" "}
                              <a href="/#">Política de Privacidade</a>
                            </span>
                          </div>
                        </div>
                        <div className="col-12">
                          <span className="d-block pt-2 mt-4 border-top">
                            <a href="/recuperar-senha">{infoData.btnText_3}</a>
                          </span>
                        </div>
                      </div>
                   
                    </form>
                 
                  </div>
                </div>
              </div>
            </div>
            {/* Shape Bottom */}
            <div className="shape-bottom">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 1000 100"
                preserveAspectRatio="none"
              >
                <path
                  className="shape-fill"
                  fill="#FFFFFF"
                  d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7  c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4  c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"
                />
              </svg>
            </div>
          </section>
        </div>
      </div>
    </form>
  );
}

export default Login;
