import React, { useState } from 'react';
import Header from '../../HeaderSection/Header';
import { login } from "../../../services/auth";
import { useNavigate } from "react-router-dom";

function Forgot() {
    const [email, setEmail] = useState("")
    const initData = {
        heading: "Esqueceu sua senha?",
        content: "Não se preocupe. Digite seu e-mail. Enviaremos a você um link de redefinição de senha para redefinir sua senha.",
        btnText: "Resetar Senha"
    }

    return (
        <div className="inner inner-pages">
            <div className="main">
                 <Header imageData={"/img/logo.png"} /> 
                <section className="section welcome-area bg-overlay subscribe-area h-100vh ptb_100">
                    <div className="container h-100">
                        <div className="row align-items-center justify-content-center h-100">
                            <div className="col-12 col-md-10 col-lg-8">
                                <div className="subscribe-content text-center">
                                    <h1 className="text-white">{initData.heading}</h1>
                                    <p className="text-white my-4">{initData.content}</p>
                                    {/* Subscribe Form */}
                                    <form className="subscribe-form">
                                        <div className="form-group">
                                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Insira seu e-mail" />
                                        </div>
                                        <button type="submit" className="btn btn-lg btn-block">{initData.btnText}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}

export default Forgot;