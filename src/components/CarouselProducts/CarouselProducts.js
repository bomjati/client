import React, { useState, useEffect } from 'react';
import cx from 'clsx';
import Typography from '@material-ui/core/Typography';
import ParallaxSlide from './ParallaxSlide';
import { useStyles } from './useStyles'
import api from "../../services/api";

const getParam = (param) => {
    return new URLSearchParams(window.location.search).get(param);
}



const data = [
    {
        id: 1,
        title: 'Arroz 5 Kg',
        subtitle: '10x de 29,90',
        image:
            // eslint-disable-next-line max-len
            'https://firebasestorage.googleapis.com/v0/b/mui-treasury.appspot.com/o/public%2Fshoes%2Fair-huarache-gripp.png?alt=media',
    },
    {
        id: 2,
        title: 'NB ',
        subtitle: '10x de 29,90',
        image:
            // eslint-disable-next-line max-len
            'https://firebasestorage.googleapis.com/v0/b/mui-treasury.appspot.com/o/public%2Fshoes%2Fair-max-270.png?alt=media',
    },
    {
        id: 3,
        title: 'Air Max',
        subtitle: '10x de 29,90',
        image:
            // eslint-disable-next-line max-len
            'https://firebasestorage.googleapis.com/v0/b/mui-treasury.appspot.com/o/public%2Fshoes%2Fair-max-deluxe.png?alt=media',
    }
];


const ParallaxCarousel = () => {
    const classes = useStyles();
    const [products, setProducts] = useState([]);
    const idCategory = (getParam('category'));
    const [config, setConfig] = useState(null);
    const [isLoading, setIsLoading] = useState(null);

    //const arrowStyles = useArrowDarkButtonStyles();
    const createStyle = (slideIndex, fineIndex) => {
        const diff = slideIndex - fineIndex;
        if (Math.abs(diff) > 1) return {};
        return {
            transform: `rotateY(${(-diff + 1) * 45}deg)`,
        };
    };
    // eslint-disable-next-line react/prop-types
    const renderElements = ({ index, onChangeIndex }) => (
        <>

        </>
    );

    const getProducts = () => {
        setIsLoading(true)
        api.get('/product/0')
            .then((resp) => {
               // console.log(resp.data)
                if (resp.data) {
                    const prod = idCategory == '*' ? resp.data : resp.data.filter((p) => p.product.product_category_id == idCategory)
                    setProducts(prod)
                }
                setIsLoading(false)
            })
            .catch((error) => {
    
            })
    }
    
    useEffect(() => {
        getProducts()
    }, [])
    
    useEffect(() => {
        api.get('/config/0')
            .then((resp) => {
                if (resp.data) {
                    setConfig(resp.data[0])
                    console.log(resp.data[0])
                }
            })
            .catch((error) => {
    
            })
    }, [])

    const renderChildren = ({ injectStyle, fineIndex }) => 
     products.map((prod, i) => (
            <div key={prod.product.id} className={classes.slide}>
                <Typography
                    noWrap
                    className={cx(classes.text, classes.title)}
                    style={{ ...injectStyle(i, 60), ...createStyle(i, fineIndex) }}
                >
                    {prod.product.name}
                </Typography>
                <Typography
                    noWrap
                    className={cx(classes.text, classes.subtitle)}
                    style={{ ...injectStyle(i, 40), ...createStyle(i, fineIndex) }}
                >
                    R${String(prod.product.promotion_price).replace('.',',')}
                </Typography>
                <div className={classes.imageContainer}>
                    <img className={classes.image} src={prod.product.product_img.path_s3} alt={'slide'} />
                </div>
            </div>
        ));

        return (
            <div className={classes.root}>
                <ParallaxSlide renderElements={renderElements}>
                    {renderChildren}
                </ParallaxSlide>
            </div>
        );
    
};

export default ParallaxCarousel;