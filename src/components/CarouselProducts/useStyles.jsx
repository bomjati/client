import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ palette, breakpoints, spacing }) => ({
    root: {
      // a must if you want to set arrows, indicator as absolute
      position: 'relative',
      width: '100%',
    },
    slide: {
      perspective: 1000, // create perspective
      overflow: 'hidden',
      // relative is a must if you want to create overlapping layers in children
      position: 'relative',
      paddingTop: spacing(8),
      [breakpoints.up('sm')]: {
        paddingTop: spacing(10),
      },
      [breakpoints.up('md')]: {
        paddingTop: spacing(14),
      },
    },
    imageContainer: {
      display: 'block',
      position: 'relative',
      zIndex: 2,
      paddingBottom: '42.25%',
    },
    image: {
      display: 'block',
      position: 'absolute',
      zIndex: 10,
      width: '100%',
      height: '100%',
      objectFit: 'cover',
      marginLeft: '12%',
      [breakpoints.up('sm')]: {
        marginLeft: '4%',
      },
    },
    arrow: {
      display: 'none',
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      [breakpoints.up('sm')]: {
        display: 'inline-flex',
      },
    },
    arrowLeft: {
      left: 0,
      [breakpoints.up('lg')]: {
        left: -64,
      },
    },
    arrowRight: {
      right: 0,
      [breakpoints.up('lg')]: {
        right: -64,
      },
    },
    text: {
      // shared style for text-top and text-bottom
      fontFamily: 'Poppins, san-serif',
      fontWeight: 'bold',
      position: 'absolute',
      color: palette.common.white,
      padding: '0 8px',
      transform: 'rotateY(45deg)',
      lineHeight: 1.2,
      [breakpoints.up('sm')]: {
        padding: '0 16px',
      },
      [breakpoints.up('md')]: {
        padding: '0 24px',
      },
    },
    title: {
      top: 20,
      marginTop: spacing(5),
      left: '20%',
      height: '40%',
      fontSize: 50,
      zIndex: 1,
      background: 'linear-gradient(0deg, rgba(255,255,255,0) 0%, #9b59b6 100%)',
      [breakpoints.up('sm')]: {
        top: 40,
        fontSize: 42,
      },
      [breakpoints.up('md')]: {
        top: 52,
        fontSize: 52,
      },
    },
    subtitle: {
      top: 60,
      marginTop: spacing(5),
      left: '5%',
      height: '52%',
      fontSize: 50,
      zIndex: 2,
      background: 'linear-gradient(0deg, rgba(255,255,255,0) 0%, #3498db 100%)',
      [breakpoints.up('sm')]: {
        top: 112,
        left: '6%',
        fontSize: 52,
      },
      [breakpoints.up('md')]: {
        top: 128,
        fontSize: 62,
      },
    },
    indicatorContainer: {
      textAlign: 'center',
    },
  }));
  