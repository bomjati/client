import React, { useState, useEffect } from 'react';
import {
    Box
} from '@material-ui/core'
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import Product from './Product'
import api from "../../services/api";
import { data } from './data'
//import './App.scss'


const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
const getParam = (param) => {
    return new URLSearchParams(window.location.search).get(param);
}

const CarouselProducts = () => {
    const [index, setIndex] = useState(0);
    const [fineIndex, setFineIndex] = useState(index);
    const [products, setProducts] = useState([]);
    const idCategory = (getParam('category'))
    const [config, setConfig] = useState(null)
    const [isLoading, setIsLoading] = useState(false);

    const onChangeIndex = i => {
        setIndex(i);
    };

    const getProducts = () => {
        setIsLoading(true)
        api.get('/product/0')
            .then((resp) => {
                if (resp.data) {
                    const prod = idCategory == '*' ? resp.data : resp.data.filter((p) => p.categoryId == idCategory)
                    setProducts(prod)
                    console.log(prod)
                }
                setIsLoading(false)
            })
            .catch((error) => {

            })
    }

    useEffect(() => {
        getProducts()
    }, [])

    useEffect(() => {
        api.get('/setting/0')
            .then((resp) => {
                if (resp.data) {
                    setConfig(resp.data[0])
                    console.log(resp.data[0])
                }
            })
            .catch((error) => {

            })
    }, [])

    return (
        <div style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute' }}>
            <div className='fixed-top'>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                    <path fill={config && config.primaryColor} fill-opacity="1" d="M0,288L30,261.3C60,235,120,181,180,165.3C240,149,300,171,360,149.3C420,128,480,64,540,37.3C600,11,660,21,720,32C780,43,840,53,900,90.7C960,128,1020,192,1080,213.3C1140,235,1200,213,1260,192C1320,171,1380,149,1410,138.7L1440,128L1440,0L1410,0C1380,0,1320,0,1260,0C1200,0,1140,0,1080,0C1020,0,960,0,900,0C840,0,780,0,720,0C660,0,600,0,540,0C480,0,420,0,360,0C300,0,240,0,180,0C120,0,60,0,30,0L0,0Z"></path>
                </svg>
            </div>

            <div style={{ marginBottom: 60, marginRight: 10 }} className='fixed-bottom'>
                <Box display='flex' justifyContent='flex-end'>
                    <img
                        height={100}
                        width={(100 * 3) / 4}
                        src={config && config.Imagesettings[0].path_s3} />
                </Box>
            </div>

            <div className='fixed-bottom'>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                    <path fill={config && config.secondaryColor} fill-opacity="1" d="M0,192L30,202.7C60,213,120,235,180,245.3C240,256,300,256,360,266.7C420,277,480,299,540,288C600,277,660,235,720,224C780,213,840,235,900,256C960,277,1020,299,1080,304C1140,309,1200,299,1260,293.3C1320,288,1380,288,1410,288L1440,288L1440,320L1410,320C1380,320,1320,320,1260,320C1200,320,1140,320,1080,320C1020,320,960,320,900,320C840,320,780,320,720,320C660,320,600,320,540,320C480,320,420,320,360,320C300,320,240,320,180,320C120,320,60,320,30,320L0,320Z"></path>
                </svg>
            </div>

            <AutoPlaySwipeableViews
                interval={5000}
                resistance
                springConfig={{
                    duration: '1.5s',
                    easeFunction: '',
                    delay: '0s',
                }}
                enableMouseEvents
                index={index}
                onChangeIndex={onChangeIndex}
                onSwitching={i => {
                    setFineIndex(i);
                    console.log(i)
                }}
            >
                {products.map((Products, vindex) => vindex == index ? <Product Product={Products} isRefreshEffect={index} /> : <div />)}
            </AutoPlaySwipeableViews>
        </div>
    )
}

export default CarouselProducts