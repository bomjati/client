import React, { useState, useEffect } from 'react'
import {
    Box,
    Slide,
    Zoom,
    Grow
} from '@material-ui/core';
//import './App.scss'

const Product = ({ Product, isRefreshEffect }) => {
    const TimeSlide = 1500
    const [refresh, setRefresh] = useState(true)
    const wait = (time) => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve();
            }, time);
        });
    }

    const onRefreshEffect = async () => {
        setRefresh(true)
        await wait(4000);
    }

    useEffect(() => {
        onRefreshEffect();
    }, [isRefreshEffect])

    return (
        <div className='center-screen'>
            <Box display='flex' alignItems='center' justifyContent='flex-start'>
                <div>
                    <Grow timeout={TimeSlide} in={refresh}>
                        <div>
                            <h2 style={{ width: 400 }} className='text-black'>{String(Product.name).toUpperCase()}</h2>
                            <p>{Product.detail}</p>
                        </div>
                    </Grow>
                    <Box display='flex' alignItems='flex-end' justifyContent='center'>
                        <Slide timeout={TimeSlide} direction="right" in={refresh} mountOnEnter unmountOnExit>
                            <h1 style={{ color: '#95a5a6', fontSize: 60, marginBottom: 32, position: 'relative' }} className=''>R$</h1>
                        </Slide>
                        <Zoom timeout={TimeSlide} in={refresh} >
                            <Box style={{ position: 'relative' }} display='flex' alignItems='flex-start' justifyContent='center'>
                                <h1 style={{ color: '#e74c3c', fontSize: 190 }} className=''>{Math.floor(Product.promotion_price)}</h1>
                                <h1 style={{ color: '#e74c3c', fontSize: 60, marginTop: 27 }} className=''>,{String(Product.promotion_price).substr(-2)}</h1>
                            </Box>
                        </Zoom>
                    </Box>
                </div>
                <Slide timeout={TimeSlide} direction="left" in={refresh} mountOnEnter unmountOnExit>
                    <img
                        style={{ position: 'relative' }}
                        height={450}
                        width={(450 * 3) / 4}
                        src={Product.Imageproducts[0].path_s3}
                    />
                </Slide>
            </Box>
        </div>
    )
}

export default Product;