export const data = [
    {
        name: 'Arroz Prato Fino 5KG',
        detail: 'Aqui terá uma descrição breve se desejar',
        img: 'https://static.clubeextra.com.br/img/uploads/1/2/574002.png?imtype=imgProductDetail&size=sm',
        price: 17.99
    },
    {
        name: 'Feijão 1KG',
        detail: 'Aqui terá uma descrição breve se desejar',
        img: 'https://hiperideal.vteximg.com.br/arquivos/ids/167858-1000-1000/149845.jpg?v=636615816808670000',
        price: 8.25
    },
    {
        name: 'Alcatra',
        detail: 'Aqui terá uma descrição breve se desejar',
        img: 'https://static1.conquistesuavida.com.br/ingredients/2/54/26/72/@/24712--ingredient_detail_ingredient-2.png',
        price: 8.25
    }
]