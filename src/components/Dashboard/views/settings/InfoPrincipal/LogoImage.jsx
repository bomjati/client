import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  FormGroup,
  FormControlLabel,
  makeStyles,
  Switch
} from '@material-ui/core';
import {
  ImageSearch as ImageSearchIcon
} from '@material-ui/icons';
const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 100,
    width: 100
  }
}));

const LogoImage = ({ onChangeImageProduct, isEditing, infoConfig, className, ...rest }) => {
  const classes = useStyles();
  const [retrato, setRetrato] = useState(true)
  const [paisagem, setPaisagem] = useState(false)
  const [selectedFile, setSelectedFile] = useState()
  const [preview, setPreview] = useState()

  // criar uma prévia como efeito colateral, sempre que o arquivo selecionado for alterado
  useEffect(() => {
    if (!selectedFile) {
      setPreview(undefined)
      return
    }

    const objectUrl = URL.createObjectURL(selectedFile)
    setPreview(objectUrl)

    // memória livre sempre que este componente for desmontado
    return () => URL.revokeObjectURL(objectUrl)
  }, [selectedFile])

  const onSelectFile = e => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined)
      return
    }

    // Eu mantive este exemplo simples usando a primeira imagem em vez de vários
    setSelectedFile(e.target.files[0])
    onChangeImageProduct(e.target.files[0])
  }

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Box
          alignItems="center"
          display="flex"
          flexDirection="column"
        >
          {/* imagem produto  */}
          {selectedFile | isEditing ? null : <ImageSearchIcon style={{ fontSize: 80 }} />}
          {(selectedFile && <img height={170} width={(170 * 3) /4} src={preview} /> || isEditing && (infoConfig.Imagesettings.length > 0 ? <img height={170} width={(170 * 3) /4} src={infoConfig.Imagesettings[0].path_s3} /> : <ImageSearchIcon style={{ fontSize: 80 }} />))}
        </Box>
      </CardContent>
      {/* Abri Diretorio */}
      <Divider />

      <CardActions>
        <Button
          color="primary"
          fullWidth
          variant="text"
          htmlFor="sampleFile"
          component="label"
        >
          Abrir Diretório
        </Button>
      </CardActions>

      <input
        onChange={onSelectFile}
        type="file"
        id="sampleFile"
        style={{ display: "none" }} />
    </Card>
  );
};

LogoImage.propTypes = {
  className: PropTypes.string
};

export default LogoImage;
