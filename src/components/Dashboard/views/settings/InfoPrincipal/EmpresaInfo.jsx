import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
} from "@material-ui/pickers";
import { AccessTime as AccessTimeIcon } from "@material-ui/icons";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";

const useStyles = makeStyles(() => ({
  root: {},
}));

const EmpresaInfo = ({
  getInfoImage,
  isEditing,
  infoConfig,
  className,
  ...rest
}) => {
  const classes = useStyles();
  const [isLoad, setIsLoad] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dateNow = moment();
  const [values, setValues] = useState({
    timeToOpen: dateNow,
    closeTime: dateNow,
  });

  const onNewImage = (id) => {
    const settingFormDataImg = new FormData();
    settingFormDataImg.append(
      "file",
      !getInfoImage ? "sem imagem" : getInfoImage
    );

    setIsLoad(true);
    api
      .post(`/imagesetting/new/${id}`, settingFormDataImg)
      .then((res) => {
        console.log(res.data);
        if (res.data.message == "success") {
          showMessage("Imagem Salva", "Imagem salva com sucesso", "success");
        }
        setIsLoad(false);
      })
      .catch((error) => {
        console.log(error);
        setIsLoad(false);
      });
  };

  const onEditImage = () => {
    const settingFormDataImg = new FormData();
    settingFormDataImg.append(
      "file",
      !getInfoImage ? "sem imagem" : getInfoImage
    );

    setIsLoad(true);
    api
      .post(
        `/imagesetting/edit/${infoConfig.Imagesettings[0].id}`,
        settingFormDataImg
      )
      .then((res) => {
        console.log(res.data);
        if (res.data.message == "success") {
          showMessage(
            "Imagem Salva",
            "Atualização realizada com sucesso",
            "success"
          );
        } else {
          showMessage(
            "Configurações Falhou",
            "Houve um problema ao atualizar sua configurações, entre em contato com o suporte.",
            "danger"
          );
        }
        setIsLoad(false);
      })
      .catch((error) => {
        console.log(error);
        setIsLoad(false);
      });
  };

  const onPostNewConfig = async () => {
    // console.log(values)
    const settingFormData = {
      timeToOpen: moment(values.timeToOpen).format("HH:mm:ss"),
      closeTime: moment(values.closeTime).format("HH:mm:ss"),
    };

    if (
      moment(dateNow).format("HH:mm") ==
      moment(values.timeToOpen).format("HH:mm")
    ) {
      showMessage(
        "Abertura do Estabelecimento",
        "Informe o horario que seu estabelecimento abrirá",
        "warning"
      );
      return;
    } else if (
      moment(dateNow).format("HH:mm") ==
      moment(values.closeTime).format("HH:mm")
    ) {
      showMessage(
        "Fechamento do Estabelecimento",
        "Informe o horario que seu estabelecimento fechara",
        "warning"
      );
      return;
    } else if (!isEditing & !getInfoImage) {
      showMessage(
        "Logo Estabelecimento",
        "Selecione a logo de sua loja",
        "warning"
      );
      return;
    } else {
      if (isEditing) {
        setIsLoad(true);
        await api
          .put(`/setting/edit/${infoConfig.id}`, settingFormData)
          .then((res) => {
            console.log(res.data);
            if (res.data.message == "success") {
              showMessage(
                "Configurações Salvas",
                "Atualização realizada com sucesso",
                "success"
              );

              if (getInfoImage) {
                onEditImage();
              }
            } else {
              showMessage(
                "Configurações Falhou",
                "Houve um problema ao atualizar sua configurações, entre em contato com o suporte.",
                "danger"
              );
            }
            setIsLoad(false);
          })
          .catch((error) => {
            console.log(error);
            setIsLoad(false);
          });
      } else {
        setIsLoad(true);
        await api
          .post("/setting/new", settingFormData)
          .then((res) => {
            console.log(res.data);
            if (res.data.message == "success") {
              showMessage(
                "Configurações Salvas",
                "Cadastro realizado com sucesso",
                "success"
              );

              onNewImage(res.data.data.id);
            }
            setIsLoad(false);
          })
          .catch((error) => {
            console.log(error);
            setIsLoad(false);
          });
      }
    }
  };

  useEffect(() => {
    infoConfig &&
      setValues({
        ...values,
        timeToOpen: moment(
          moment().format("DD/MM/YYYY") + " " + infoConfig.timeToOpen,
          "DD/MM/YYY HH:mm:ss"
        ),
        closeTime: moment(
          moment().format("DD/MM/YYYY") + " " + infoConfig.closeTime,
          "DD/MM/YYY HH:mm:ss"
        ),
      });
  }, [infoConfig]);

  return (
    <form
      autoComplete="off"
      onSubmit={(event) => {
        event.preventDefault();
        onPostNewConfig();
      }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20 }}>
        <CardHeader
          subheader="Preencha as informações"
          title={"Informações da Empresa"}
        />
        <Divider />
        <CardContent>
          <Grid container>
            <Grid item md={6} xs={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container spacing={2}>
                  <Grid item md={6} xs={12}>
                    <KeyboardTimePicker
                      id="time-picker"
                      ampm={false}
                      inputVariant="outlined"
                      label="Time picker"
                      name="time_start"
                      label="Hora Abertura"
                      value={values.timeToOpen}
                      onChange={(event) => {
                        setValues({
                          ...values,
                          timeToOpen: event,
                        });
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change time",
                      }}
                      keyboardIcon={<AccessTimeIcon />}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <KeyboardTimePicker
                      id="time-picker"
                      ampm={false}
                      inputVariant="outlined"
                      label="Time picker"
                      name="time_start"
                      label="Hora Fechamento"
                      value={values.closeTime}
                      onChange={(event) => {
                        setValues({
                          ...values,
                          closeTime: event,
                        });
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change time",
                      }}
                      keyboardIcon={<AccessTimeIcon />}
                    />
                  </Grid>
                </Grid>
              </MuiPickersUtilsProvider>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        {/*  linha direcionada flex-end */}
        <Box display="flex" justifyContent="flex-end" p={2}>
          {/* Botão para salvar as informações */}
          <Button
            color="primary"
            disabled={isLoad || isLoading}
            variant="contained"
            style={{ borderRadius: 50 }}
            type="submit"
          >
            Salvar
          </Button>
          {isLoad && <CircularProgress className="ml-2" />}
        </Box>
      </Card>
    </form>
  );
};

EmpresaInfo.propTypes = {
  className: PropTypes.string,
};

export default EmpresaInfo;
