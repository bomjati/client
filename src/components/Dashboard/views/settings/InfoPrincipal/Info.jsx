import React, { useEffect, useState } from 'react';
import {
  Container,
  Grid,
  makeStyles,
  Card,
  IconButton,
  Box
} from '@material-ui/core';
import {
  Close as CloseIcon
} from '@material-ui/icons';
import Page from '../../../components/Page';
import LogoImage from './LogoImage';
import EmpresaInfo from './EmpresaInfo';
import api from "../../../../../services/api";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));



const Product = ({ infoConfig, isEditing }) => {
  const classes = useStyles();
  const [infoImage, setInfoImage] = useState("")

  // requesição para retornar as informações de quem logou
  const onChangeImageProduct = (value) => {
    setInfoImage(value)
  }

  return (
    <Page
      className={classes.root}
      title="Configurações"
    >
      <Grid
        container
        spacing={3}
      >
        <Grid
          item
          lg={4}
          md={6}
          xs={12}
        >
          <LogoImage
            isEditing={isEditing}
            infoConfig={infoConfig}
            onChangeImageProduct={onChangeImageProduct} />
        </Grid>
        <Grid
          item
          lg={8}
          md={6}
          xs={12}
        >
          <EmpresaInfo
            getInfoImage={infoImage}
            infoConfig={infoConfig}
            isEditing={isEditing} />
        </Grid>
      </Grid>
    </Page>
  );
};

export default Product;
