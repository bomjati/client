import React, { useState, useEffect } from 'react';
import {
  Box,
  Container,
  makeStyles,
  Grid
} from '@material-ui/core';
import Page from '../../../components/Page';
import ColorPadrao from './ColorPadrao';
import PadraoViewVitrine from './PadraoViewVitrine';
import InfoEmpresa from '../InfoPrincipal'
import api from "../../../../../services/api";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const SettingsView = () => {
  const classes = useStyles();
  const [infoConfig, setInfoConfig] = useState([])
  const [isEditing, setIsEditing] = useState(false)

  useEffect(() => {
    api.get('/setting/0')
      .then(response => response.data)
      .then(resp => {
        if (resp) {
          setInfoConfig(resp[0])
          if (resp.length > 0) {
            setIsEditing(true)
          }
        }
      })
  }, [])

  return (
    <Page
      className={classes.root}
      title="Configurações"
    >
      <Container maxWidth="lg">
        <Grid container>
          <Grid
            item
            xs={12}
            md={12}
            lg={12}>
            <InfoEmpresa isEditing={isEditing} infoConfig={infoConfig} />
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
            lg={12}>
            <ColorPadrao isEditing={isEditing} infoConfig={infoConfig} />
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
            lg={12}>
            <PadraoViewVitrine isEditing={isEditing} infoConfig={infoConfig} />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default SettingsView;
