import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import NewAccess from "./FormNewAccess";
//import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalNewAccess = ({
  open,
  onCloseNewAccess,
  getAllAccess,
  idClient,
  idBusiness,
  channels,
  isMobile,
}) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseNewAccess}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4">
              <NewAccess
                onCloseModal={onCloseNewAccess}
                getAllAccess={getAllAccess}
                idClient={idClient}
                idBusiness={idBusiness}
                channels={channels}
                isMobile={isMobile}
              />
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewAccess;
