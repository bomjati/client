import React, { useEffect } from "react";
import {
  Backdrop,
  Modal,
  makeStyles,
  Fade,
  Container,
  Card,
  IconButton,
  CardHeader,
  Box,
  Divider,
} from "@material-ui/core";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";
import moment from "moment";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalNewAccess = ({
  open,
  onCloseEditAccess,
  getAllAccess,
  idClient,
  idBusiness,
  getEditAccess,
  channels,
  isMobile,
  info,
}) => {
  const classes = useStyles();

  useEffect(() => {
    if (getEditAccess) {
      if (getEditAccess.verified == "false") {
        api
          .put(`/supportrequest/edit/${getEditAccess.id}`, {
            verified: "true",
          })
          .then((response) => response.data)
          .then((resp) => {
            if (resp.message == "success") {
              getAllAccess()
            } else {
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    }
  }, [getEditAccess]);
  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseEditAccess}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4">
              <Container
                component="article"
                style={{ width: isMobile ? "100%" : 720 }}
                className="pb-2"
              >
                <Box
                  display="flex"
                  justifyContent="flex-end"
                  className="w-100 mt-2 mb-2 mr-2"
                  p={0}
                >
                  <IconButton
                    style={{ backgroundColor: "white" }}
                    onClick={onCloseEditAccess}
                  >
                    <CloseIcon />
                  </IconButton>
                </Box>
                <Card style={{ borderRadius: 20 }}>
                  <CardHeader
                    subheader="Informações do pedido recebido"
                    title="Novo Pedido"
                  />

                  <Divider />

                  <Card
                    className="p-4 m-4"
                    style={{
                      borderRadius: 20,
                      borderColor: "rgba(196,196,196,0.2)",
                      borderWidth: 0.1,
                      borderStyle: "solid",
                    }}
                  >
                    <h5 className="mb-2">Cliente</h5>
                    <p className="mb-4">{info.client}</p>

                    <Divider />

                    <Box mb={4} display="flex" justifyContent="space-between">
                      <div>
                        <h5 className="mt-4 mb-2">Iniciar</h5>
                        <p>
                          {moment(getEditAccess.startDate).format("DD/MM/YYYY")}
                        </p>
                      </div>
                      <div>
                        <h5 className="mt-4 mb-2">Finalizar</h5>
                        <p>
                          {moment(getEditAccess.endDate).format("DD/MM/YYYY")}
                        </p>
                      </div>
                    </Box>

                    <Divider />

                    {getEditAccess.isTextFree == "true" ? (
                      <h5 className="mt-4 mb-2">Texto Livre</h5>
                    ) : (
                      <h5 className="mt-4 mb-2">Lista Produtos</h5>
                    )}
                    {getEditAccess.isTextFree == "true" ? (
                      <p>{info.textFree}</p>
                    ) : info.product ? (
                      info.product.map((p, index) => (
                        <div
                          className={`${
                            index == 0 ? "mt-4" : ""
                          } d-flex align-items-center`}
                        >
                          <h5 className="ml-2 mr-2">●</h5>
                          <p>{String(p.name).toUpperCase()} - </p>
                          <p className="ml-2 mr-2">{p.price}</p>
                        </div>
                      ))
                    ) : null}

                    <Divider className="mt-4" />

                    <h5 className="mt-4 mb-2">Observação</h5>
                    <p>{info.observation}</p>
                  </Card>
                </Card>
              </Container>
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewAccess;
