import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
  Tooltip,
  Hidden,
} from "@material-ui/core";
import { Search as SearchIcon } from "react-feather";
import AddIcon from "@material-ui/icons/AddOutlined";
import { showMessage } from "../../../utils/message";

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({
  onShowNewAccess,
  SearchAccess,
  levelAccess,
  className,
  isAdministrador,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box m={-5} mt={-4}>
        <Card style={{ borderRadius: 20 }}>
          <CardContent>
            <Hidden smDown>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box width={"50%"}>
                  <TextField
                    fullWidth
                    onChange={(event) => {
                      SearchAccess(event.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SvgIcon fontSize="small" color="action">
                            <SearchIcon />
                          </SvgIcon>
                        </InputAdornment>
                      ),
                    }}
                    placeholder="Procurar Pedido"
                    variant="outlined"
                  />
                </Box>
              </Box>
            </Hidden>
            <Hidden mdUp>
              <TextField
                fullWidth
                onChange={(event) => {
                  SearchAccess(event.target.value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon fontSize="small" color="action">
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  ),
                }}
                placeholder="Procurar Pedido"
                variant="outlined"
              />
            </Hidden>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
