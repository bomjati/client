import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Tooltip,
} from "@material-ui/core";
import getInitials from "../../../utils/getInitials";
import { showMessage } from "../../../utils/message";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Settings as SettingsIcon,
  RateReview as RateReviewIcon,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  onDeleteAccess,
  onShowModalSettingAccess,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  levelAccess,
  isAdministrador,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  useEffect(() => {
    console.log(customers);
  }, []);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  const formatTel = (str) => {
    return String(str).replace(/\D/g, "").length == 11
      ? String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d)(\d{4})(\d{4})$/, "($1) $2 $3-$4")
      : String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d{4})(\d{4})$/, "($1) $2-$3");
  };

  return !(customers.length > 0) ? null : (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={300}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <TableRow
                      hover
                      key={customer.id}
                      selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={
                            selectedCustomerIds.indexOf(customer.id) !== -1
                          }
                          onChange={(event) =>
                            handleSelectOne(event, customer.id)
                          }
                          value="true"
                        />
                      </TableCell>
                      <TableCell align="left">
                        <Badge
                          color="secondary"
                          variant="dot"
                          invisible={customer.verified == "true" ? true : false}
                          className="p-1"
                        >
                          {JSON.parse(customer.informations)[0].client}
                        </Badge>
                      </TableCell>
                      <TableCell align="left">
                        {moment(customer.startDate).format("DD/MM/YYYY")}
                      </TableCell>
                      <TableCell align="left">
                        {moment(customer.endDate).format("DD/MM/YYYY")}
                      </TableCell>
                      <TableCell align="right">
                        <Tooltip title="Configurações de Acesso" arrow>
                          <IconButton
                            disabled={isLoading}
                            onClick={() => {
                              if ((levelAccess > 2) | isAdministrador.status) {
                                onShowModalSettingAccess(customer);
                              } else {
                                showMessage(
                                  "Acesso Negado",
                                  "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
                                  "danger"
                                );
                              }
                            }}
                            color="inherit"
                          >
                            <RateReviewIcon style={{ color: "#3d3d3d" }} />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
