import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../../../services/api";
import { showMessage } from "../../../../../utils/message";
import MessageConfirm from "../../../../../utils/MessageConfirm";
import ModalEditVignette from "../ModalVignette";
import MultSelect from "./multSelectVignetteCommercial";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = ({
  idClient,
  onShowNewVignette,
  activeGetAllVignette,
  disableGetAllVignette,
}) => {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(false);
  const [showConfirmLote, setShowConfirmLote] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [showEditVignette, setShowEditVignette] = useState(false);
  const [Vignette, setVignette] = useState([]);
  const [VignetteBusca, setVignetteBusca] = useState([]);
  const [idVignetteModify, setIdVignetteModify] = useState(0);
  const [idsVignette, setIdsVignette] = useState([1]);
  const [getVignetteModify, setGetVignetteModify] = useState();
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    if (idClient) {
      getVignette(idClient);
    }
  }, [idClient]);

  useEffect(() => {
    console.log(idsVignette);
  }, [idsVignette]);

  useEffect(() => {
    if (activeGetAllVignette) {
      getVignette(idClient);
      disableGetAllVignette();
    }
  }, [activeGetAllVignette]);

  const buscaVignette = (value) => {
    let resultBusca = VignetteBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setVignette(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdVignetteModify(id);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteVignette(idVignetteModify);
  };

  const onShowConfirmLote = () => {
    setShowConfirmLote(true);
  };

  const onCloseMessageConfirmLote = () => {
    setShowConfirmLote(false);
  };

  const onAcceptMessageConfirmLote = () => {
    setShowConfirm(false);
    onDeleteLote();
  };

  const getVignette = async (id) => {
    setIsLoading(true);
    const res = await api.get(`/Vignette/[${id}]`);

    if (res) {
      setVignette(res.data);
      setVignetteBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeleteVignette = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Vignette/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getVignette(idClient);
        showMessage(
          "Vinheta Deletado",
          "Vinheta selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Vinheta Falhou",
        "Houve um problema ao deletar seu vinheta, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onShowEditVignette = (data) => {
    setGetVignetteModify(data);
    setShowEditVignette(true);
  };

  const onCloseEditVignette = () => {
    setShowEditVignette(false);
  };

  const onIdsVignette = (ids) => {
    setIdsVignette(ids);
  };

  const onDeleteLote = () => {
    setIsLoading(true);
    for (let index = 0; index < idsVignette.length; index++) {
      const element = idsVignette[index];

      api
        .delete(`/Vignette/${element}`)
        .then((response) => response.data)
        .then((res) => {
          if (res.message === "success") {
            if (index + 1 == idsVignette.length) {
              getVignette(idClient);
              showMessage(
                "Vinheta Deletado",
                "Vinheta selecionado foi deletado com sucesso.",
                "success"
              );
              setIsLoading(false);
            }
          } else {
            showMessage(
              "Vinheta Falhou",
              "Houve um problema ao deletar seu vinheta, entre em contato com o suporte.",
              "danger"
            );
          }
        });
    }
  };

  return (
    <Page className={classes.root} title="Vinhetas">
      <Container maxWidth={false}>
        <Toolbar
          SearchVignette={buscaVignette}
          onShowNewVignette={onShowNewVignette}
        />
        <Box mt={7} ml={-6} mr={-6}>
          {isLoading && <LinearProgress />}
          <Results
            onDeleteVignette={onShowConfirm}
            onShowEditVignette={onShowEditVignette}
            onIdsVignette={onIdsVignette}
            customers={Vignette}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
        {idsVignette.length > 0 && (
          <MultSelect onDelete={() => onShowConfirmLote()} />
        )}
      </Container>

      <ModalEditVignette
        open={showEditVignette}
        selectId={[idClient]}
        getVignetteModify={getVignetteModify}
        onCloseNewVignette={onCloseEditVignette}
        getAllVinhetas={() => getVignette(idClient)}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a vinheta será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
      <MessageConfirm
        open={showConfirmLote}
        textInfo={`Você confirmando essa operação, as vinheta serão deletadas de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirmLote}
        onAccept={onAcceptMessageConfirmLote}
      />
    </Page>
  );
};

export default CompanyListView;
