import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import NewVignette from "./FormNewVignette";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalNewVignette = ({
  open,
  onCloseNewVignette,
  getAllVinhetas,
  getVignetteModify,
  selectId,
}) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);

  const isLoadingModal = (value) => {
    setIsLoading(value);
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={!isLoading && onCloseNewVignette}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className="mb-4">
            <NewVignette
              selectId={selectId}
              onCloseModal={onCloseNewVignette}
              getAllVinhetas={getAllVinhetas}
              getVignetteModify={getVignetteModify}
              isLoadingModal={isLoadingModal}
            />
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewVignette;
