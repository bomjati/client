import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  duration,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../../../utils/uf";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  AccessTime as AccessTimeIcon,
  Delete as DeleteIcon,
  AddCircleOutline as AddCircleIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../../../utils/message";
import api from "../../../../../../../services/api";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";

let test = "";
const Formulario = ({
  onCloseModal,
  getAllVinhetas,
  isLoadingModal,
  selectId,
  getVignetteModify,
}) => {
  let history = useNavigate();
  const fileToArrayBuffer = require("file-to-array-buffer");
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listTypeVignette, setListTypeVignette] = useState(null);
  const [value, setValue] = useState(0);
  const [listClient, setListClient] = useState("");
  const [listSegment, setListSegment] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState();
  const [listConcluded, setListConcluded] = useState([]);
  const [listDuration, setListDuration] = useState(null);
  const [dateSelect, setDateSelect] = useState({
    1: {
      dueDate: moment().format("YYYY-MM-DD"),
    },
  });

  const [values, setValues] = useState({
    typevignetteId: "",
    nameTypeVignette: "",
  });

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }

    setSelectedFile(e.target.files);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {

    if (moment(dateSelect["1"].dueDate, "YYYY-MM-DD").diff(moment()) <= 0) {
      showMessage("Atenção", `Selecione um dia maior que hoje`, "warning");

      return;
    }

    onSave();
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    isLoadingModal(true);
    setIsLoading(true);

    let arrayRefreshDays = {};

    for (const key in dateSelect) {
      if (dateSelect.hasOwnProperty(key)) {
        const elementH = dateSelect[key];

        arrayRefreshDays = {
          ...arrayRefreshDays,
          [key]: {
            ...elementH,
            vignetteId: getVignetteModify.id,
          },
        };
      }
    }

    api
      .post("/day/new", arrayRefreshDays)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          showMessage(
            "Vinhetas",
            `Modificação realizado com sucesso.`,
            "success"
          );
          setIsLoading(false);
          isLoadingModal(false);
          getAllVinhetas();
          onCloseModal();
        } else {
          showMessage("Novo Comercial", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (getVignetteModify) {
      let Days = getVignetteModify.Days[0];
      setDateSelect({
        1: {
          dueDate: Days.dueDate,
        },
      });
    }
  }, [getVignetteModify]);

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={!isLoading && onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu vinheta"
            title="Modificação de Vinheta"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações da Vinheta" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      {Object.keys(dateSelect).map((key, index) => (
                        <Box mt={2} display="flex" justifyContent="center">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              inputVariant="outlined"
                              format="dd/MM/yyyy"
                              id="date-picker-outlined"
                              label="Enceramento"
                              name="dueDate"
                              value={moment(
                                dateSelect[key].dueDate,
                                "YYYY-MM-DD"
                              )}
                              onChange={(event) => {
                                setDateSelect({
                                  ...dateSelect,
                                  [key]: {
                                    dueDate: moment(event).format("YYYY-MM-DD"),
                                  },
                                });
                              }}
                              fullWidth
                              KeyboardButtonProps={{
                                "aria-label": "change date",
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </Box>
                      ))}
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
