import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  Hidden,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Paper,
  Typography,
  Chip,
  MenuItem,
  Icon,
  Checkbox,
  Modal,
  Avatar,
  ClickAwayListener,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  PlayArrow as PlayArrowIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  Remove as RemoveIcon,
  Add as AddIcon,
  Edit as EditIcon,
  PermDataSetting as PermDataSettingIcon,
  ViewList as ViewListIcon,
  Extension as ExtensionIcon,
  RecordVoiceOver as RecordVoiceOverIcon,
  Cancel as CancelIcon,
  AccessTime as AccessTimeIcon,
  AddCircle as AddCircleIcon,
  Delete as DeleteIcon,
  Done as DoneIcon,
  List as ListIcon,
  Today as TodayIcon,
  Search as SearchIcon,
  MoreHoriz as MoreHorizIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../../../services/api";
import Vignette from "./vignette/VignetteListView";
import Commercial from "./commercial/CommercialListView";
import ModalNewVignette from "../ModalVignette";
import ModalNewCommercial from "../ModalCommercial";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import arrayMove from "array-move";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment, { RFC_2822 } from "moment";
import DateFnsUtils from "@date-io/date-fns";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";
import { ChromePicker } from "react-color";
import ListExecutedResult from "./ListExecuted/Results";
import "./app.css";

import { v4 } from "uuid";
import Results from "../ChannelListView/Results";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
    marginTop: 10,
  },
  selected: {
    backgroundColor: "rgba(235, 47, 6, 0.3)",
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
  modal: {
    display: "flex",
    [theme.breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [theme.breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
    backgroundColor:
      personName.indexOf(name) === -1 ? "transparent" : "rgba(235, 47, 6, 0.3)",
  };
}

const Formulario = ({
  onCloseModal,
  getAllChannel,
  idClient,
  getSettingChannelModify,
  getInfoClient,
  isMobile,
  isChannel,
  indexValuesPreviewList,
}) => {
  let history = useNavigate();
  const classes = useStyles();
  const themes = useTheme();
  const [showPreview, setShowPreview] = useState(false);
  const [showNewVignette, setShowNewVignette] = useState(false);
  const [showNewCommercial, setShowNewCommercial] = useState(false);
  const [activeGetAllCommertial, setActiveGetAllCommertial] = useState(false);
  const [activeGetAllVignette, setActiveGetAllVignette] = useState(false);
  const [isRuleVignetteDefault, setIsRuleVignetteDefault] = useState(true);
  const [isRuleVignetteSazonal, setIsRuleVignetteSazonal] = useState(false);
  const [openSelectTypeCommercial, setOpenSelectTypeCommercial] =
    useState(false);
  const [isRuleVignetteInstitucional, setIsRuleVignetteInstitucional] =
    useState(false);
  const [isRuleVignetteComercial, setIsRuleVignetteComercial] = useState(true);
  const [showListContent, setShowListContent] = useState({ "-1": false });
  const [sazonalSelect, setSazonalSelect] = useState();
  const [color, setColor] = useState("#2BA3DA");
  const [colorFont, setColorFont] = useState("white");
  const [institucionalSelect, setInstitucionalSelect] = useState();
  const [listVignette, setListVignette] = useState(null);
  const [value, setValue] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [listSegment, setListSegment] = useState(null);
  const [contentgroup, setContentGroup] = useState(null);
  const [personName, setPersonName] = useState([]);
  const [listTypeCommercial, setListTypeCommercial] = useState([]);
  const [listAudioQuality, setListAudioQuality] = useState([]);
  const [listCommercials, setListCommercials] = useState([]);
  const [listContents, setListContents] = useState([]);
  const [listExecuteds, setListExecuteds] = useState([]);
  const [dataDeListExecuteds, setDataDeListExecuteds] = useState(moment());
  const [dataParaListExecuteds, setDataParaListExecuteds] = useState(moment());
  const [nameChannel, setNameChannel] = useState("");
  const [items, setItems] = useState([]);
  const [countGetCli, setCountGetCli] = useState(0);
  const [values, setValues] = useState({
    qtdMusic: 0,
    qtdMusicCommertial: 0,
    isRuleVignetteDefault: "false",
    isRuleVignetteComercial: "false",
    isCloseChannelAuto: "false",
    timeClose: null,
    sazonalVignetteId: 0,
    institucionalVignetteId: 0,
    colorFont: "white",
    colorBackground: "#2BA3DA",
    audioQualityId: 0,
  });
  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  const listColor = [
    { tipo: "Outros", color: "#3498db" },
    { tipo: "Musica", color: "#26de81" },
    { tipo: "Institucional", color: "#9b59b6" },
    { tipo: "Sazonal", color: "#e67e22" },
    { tipo: "Flash", color: "#e74c3c" },
    { tipo: "Vinheta", color: "#FFC312" },
    { tipo: "Comercial", color: "#00a8ff" },
    { tipo: "Hora Certa", color: "black" },
  ];

  const colorButtonContent = "red";
  const colorButtonMarginIcon = 20;
  const optionMenu = [
    {
      name: "Editar Canal",
      icon: (
        <EditIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(5);
      },
    },
    {
      name: "Configurações",
      icon: (
        <PermDataSettingIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(1);
      },
    },
    {
      name: "Preview de Execução",
      icon: (
        <ViewListIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(2);
      },
    },
    {
      name: "Vinhetas",
      icon: (
        <RecordVoiceOverIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(4);
      },
    },
    {
      name: "Relatório Execução",
      icon: (
        <ListIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setValue(6);
      },
    },
  ];

  const onChangeComplete = (color, event) => {
    setValues({ ...values, colorBackground: color.hex });
  };

  const onChangeCompleteFont = (color, event) => {
    setValues({ ...values, colorFont: color.hex });
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeSelect = (event) => {
    if (personName.length <= 6) {
      setPersonName(event.target.value);
    } else {
      showMessage(
        "Limite Seleção",
        "Você atigiu o limite de 6 segmentos selecionados para um canal",
        "warning"
      );
    }
  };

  const SortableItem = SortableElement(({ value, qtd, teste, genre }) => {
    return (
      <div
        onClick={(event) => {
          event.stopPropagation();
        }}
      >
        <Box display="flex">
          <div>
            <IconButton
              size="small"
              onClick={() => {
                let data = items.filter((i, indexi) => {
                  return teste != indexi;
                });
                setItems(data);
              }}
              style={{
                ...styleButtonIcon,
                backgroundColor: "red",
                marginRight: 0,
                marginTop: 12,
              }}
            >
              <CloseIcon fontSize="small" style={{ color: "white" }} />
            </IconButton>
          </div>

          <div className="w-100">
            <div
              style={{
                boxShadow: "1px 1px 8px -4px rgba(0,0,0,0.75)",
                WebkitBoxShadow: "1px 1px 8px -4px rgba(0,0,0,0.75)",
                MozBoxShadow: "1px 1px 8px -4px rgba(0,0,0,0.75)",
                borderRadius: 15,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
                borderBottomLeftRadius: items.filter((i, indexi) => {
                  return teste == indexi;
                })[0].urlFixed
                  ? 0
                  : 15,
                borderBottomRightRadius: items.filter((i, indexi) => {
                  return teste == indexi;
                })[0].urlFixed
                  ? 0
                  : 15,
                borderColor: listColor.filter(
                  (c) =>
                    String(value)
                      .toUpperCase()
                      .split("-")[0]
                      .indexOf(String(c.tipo).toUpperCase()) > -1
                ).length > 0 ? listColor.filter(
                  (c) =>
                    String(value)
                      .toUpperCase()
                      .split("-")[0]
                      .indexOf(String(c.tipo).toUpperCase()) >
                    -1
                )[0].color : '#e84393',
                borderWidth: 0.5,
                borderStyle: "solid",
                backgroundColor: "white",
                cursor: "move",
                width: "100%",
              }}
              className="m-2"
            >
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box display={"flex"} mr={4}>
                  <Box
                    style={{
                      borderTopLeftRadius: 13,
                      borderBottomLeftRadius: items.filter((i, indexi) => {
                        return teste == indexi;
                      })[0].urlFixed
                        ? 0
                        : 13,
                      color: "white",
                      backgroundColor: listColor.filter(
                        (c) =>
                          String(value)
                            .toUpperCase()
                            .split("-")[0]
                            .indexOf(String(c.tipo).toUpperCase()) > -1
                      ).length > 0 ? listColor.filter(
                        (c) =>
                          String(value)
                            .toUpperCase()
                            .split("-")[0]
                            .indexOf(String(c.tipo).toUpperCase()) >
                          -1
                      )[0].color : '#e84393',
                    }}
                    pl={2}
                    pr={2}
                    pb={1}
                    pt={1}
                    display="flex"
                  >
                    <Box
                      display="flex"
                      flexDirection="column"
                      justifyContent="center"
                      alignItems="center"
                    >
                      <h4 className="text-white text-center mb-1">{qtd}</h4>
                      <p className="text-white text-center">Musicas</p>
                    </Box>
                  </Box>
                  <Box
                    display="flex"
                    flexDirection={"column"}
                    justifyContent={"space-between"}
                    ml={0.5}
                    mt={0.5}
                    mb={0.5}
                  >
                    {qtd > 0 ? null : <div style={{ width: 32 }} />}
                    {qtd == 0 ? null : (
                      <IconButton
                        size="small"
                        onClick={() => {
                          let data = items.filter((i, indexi) => {
                            return teste != indexi;
                          });

                          data.splice(teste, 0, {
                            id: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].id,
                            tipo: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].tipo,
                            genre: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].genre
                              ? items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].genre
                              : "",
                            qtd:
                              items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].qtd - 1,
                            urlFixed: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].urlFixed,
                          });

                          setItems(data);
                        }}
                        style={{
                          ...styleButtonIcon,
                          marginRight: 10,
                          borderColor: "silver",
                          borderWidth: 0.5,
                          borderStyle: "solid",
                          backgroundColor: "white",
                        }}
                      >
                        <RemoveIcon
                          fontSize="small"
                          style={{ color: "#d35400" }}
                        />
                      </IconButton>
                    )}

                    <IconButton
                      size="small"
                      onClick={() => {
                        let data = items.filter((i, indexi) => {
                          return teste != indexi;
                        });

                        data.splice(teste, 0, {
                          id: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].id,
                          tipo: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].tipo,
                          genre: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].genre
                            ? items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].genre
                            : "",
                          qtd:
                            items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].qtd + 1,
                          urlFixed: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].urlFixed,
                        });

                        setItems(data);
                      }}
                      style={{
                        ...styleButtonIcon,
                        marginRight: 10,
                        borderColor: "silver",
                        borderWidth: 0.5,
                        borderStyle: "solid",
                        backgroundColor: "white",
                      }}
                    >
                      <AddIcon
                        fontSize="small"
                        style={{
                          color: "#27ae60",
                        }}
                      />
                    </IconButton>
                  </Box>
                </Box>

                <Hidden only={["sm", "xl", "xs"]}>
                  <Box
                    display="flex"
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="center"
                  >
                    <div>
                      <p className="mr-4">{value}</p>
                    </div>
                    {value == "Hora Certa" && (
                      <Box
                        display="flex"
                        flexDirection="row"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <p className="mr-4">Feminina</p>
                        <Switch
                          onChange={(event) => {
                            let data = items.filter((i, indexi) => {
                              return teste != indexi;
                            });

                            if (event.target.checked) {
                              console.log("male");
                              data.splice(teste, 0, {
                                id: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].id,
                                tipo: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].tipo,
                                genre: "male",
                                qtd: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].qtd,
                                urlFixed: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].urlFixed,
                              });
                            } else {
                              console.log("fale");
                              data.splice(teste, 0, {
                                id: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].id,
                                tipo: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].tipo,
                                genre: "fale",
                                qtd: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].qtd,
                                urlFixed: items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].urlFixed,
                              });
                            }
                            setItems(data);
                          }}
                          checked={genre == "male" ? true : false}
                          defaultChecked
                        />
                        <p className="mr-4">Masculina</p>
                      </Box>
                    )}
                  </Box>
                </Hidden>

                <Hidden only={["sm", "xl", "xs"]}>
                  <div>
                    <Box
                      display="flex"
                      justifyContent="flex-end"
                      marginRight={1.4}
                    >
                      <IconButton
                        size="small"
                        onClick={() => {
                          setShowListContent({
                            [String(teste)]: !showListContent[String(teste)],
                          });
                        }}
                      >
                        <MoreHorizIcon
                          fontSize="small"
                          style={{ color: "#353b48" }}
                        />
                      </IconButton>
                    </Box>

                    <Box display="flex" marginTop={1}>
                      {qtd > 0 ? null : <div style={{ width: 32 }} />}
                      {qtd == 0 ? null : (
                        <IconButton
                          size="small"
                          onClick={() => {
                            let data = items.filter((i, indexi) => {
                              return teste != indexi;
                            });

                            data.splice(teste, 0, {
                              id: items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].id,
                              tipo: items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].tipo,
                              genre: items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].genre
                                ? items.filter((i, indexi) => {
                                    return teste == indexi;
                                  })[0].genre
                                : "",
                              qtd:
                                items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].qtd - 1,
                              urlFixed: items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].urlFixed,
                            });

                            setItems(data);
                          }}
                          style={{
                            ...styleButtonIcon,
                            marginRight: 10,
                            borderColor: "silver",
                            borderWidth: 0.5,
                            borderStyle: "solid",
                          }}
                        >
                          <RemoveIcon
                            fontSize="small"
                            style={{ color: "#d35400" }}
                          />
                        </IconButton>
                      )}

                      <IconButton
                        size="small"
                        onClick={() => {
                          let data = items.filter((i, indexi) => {
                            return teste != indexi;
                          });

                          data.splice(teste, 0, {
                            id: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].id,
                            tipo: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].tipo,
                            genre: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].genre
                              ? items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].genre
                              : "",
                            qtd:
                              items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].qtd + 1,
                            urlFixed: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].urlFixed,
                          });

                          setItems(data);
                        }}
                        style={{
                          ...styleButtonIcon,
                          marginRight: 10,
                          borderColor: "silver",
                          borderWidth: 0.5,
                          borderStyle: "solid",
                        }}
                      >
                        <AddIcon
                          fontSize="small"
                          style={{
                            color: "#27ae60",
                          }}
                        />
                      </IconButton>
                    </Box>
                  </div>
                </Hidden>

                <Hidden only={["md", "lg"]}>
                  <div>
                    <p style={{ fontWeight: "bold" }} className="text-center">
                      {value}
                    </p>

                    <Box display="flex" justifyContent="center" pl={2} pr={2}>
                      <IconButton
                        size="small"
                        onClick={() => {
                          let data = items.filter((i, indexi) => {
                            return teste != indexi;
                          });

                          data.splice(teste, 0, {
                            id: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].id,
                            tipo: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].tipo,
                            genre: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].genre
                              ? items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].genre
                              : "",
                            qtd:
                              items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].qtd - 1,
                            urlFixed: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].urlFixed,
                          });

                          setItems(data);
                        }}
                        style={{
                          ...styleButtonIcon,
                          marginRight: 10,
                        }}
                      >
                        <RemoveIcon
                          fontSize="small"
                          style={{ color: "#d35400" }}
                        />
                      </IconButton>

                      <IconButton
                        size="small"
                        onClick={() => {
                          let data = items.filter((i, indexi) => {
                            return teste != indexi;
                          });

                          data.splice(teste, 0, {
                            id: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].id,
                            tipo: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].tipo,
                            genre: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].genre
                              ? items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].genre
                              : "",
                            qtd:
                              items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].qtd + 1,
                            urlFixed: items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].urlFixed,
                          });

                          setItems(data);
                        }}
                        style={{
                          ...styleButtonIcon,
                          marginRight: 10,
                        }}
                      >
                        <AddIcon
                          fontSize="small"
                          style={{
                            color: "#27ae60",
                          }}
                        />
                      </IconButton>
                    </Box>
                  </div>
                </Hidden>
              </Box>
            </div>

            {items.filter((i, indexi) => {
              return teste == indexi;
            })[0].urlFixed ? (
              <Box
                className="w-100"
                display="flex"
                alignItems="center"
                style={{
                  borderBottomLeftRadius: 15,
                  borderBottomRightRadius: 15,
                  borderColor: listColor.filter(
                    (c) =>
                      String(value)
                        .toUpperCase()
                        .split("-")[0]
                        .indexOf(String(c.tipo).toUpperCase()) > -1
                  ).length > 0 ? listColor.filter(
                    (c) =>
                      String(value)
                        .toUpperCase()
                        .split("-")[0]
                        .indexOf(String(c.tipo).toUpperCase()) >
                      -1
                  )[0].color : '#e84393',
                  borderLeftWidth: 0.5,
                  borderTopWidth: 0,
                  borderRightWidth: 0.5,
                  borderBottomWidth: 0.5,
                  borderStyle: "solid",
                  marginLeft: 8,
                  marginTop: -9,
                  marginBottom: 15,
                }}
              >
                <audio
                  style={{ borderRadius: 20 }}
                  controls
                  controlsList="nodownload"
                  className="ml-1 mr-1 mb-3 mt-3"
                  src={
                    items.filter((i, indexi) => {
                      return teste == indexi;
                    })[0].urlFixed
                  }
                />
                <div>
                  <IconButton
                    size="small"
                    onClick={() => {
                      let data = items.filter((i, indexi) => {
                        return teste != indexi;
                      });

                      data.splice(teste, 0, {
                        id: items.filter((i, indexi) => {
                          return teste == indexi;
                        })[0].id,
                        tipo: items.filter((i, indexi) => {
                          return teste == indexi;
                        })[0].tipo,
                        genre: items.filter((i, indexi) => {
                          return teste == indexi;
                        })[0].genre
                          ? items.filter((i, indexi) => {
                              return teste == indexi;
                            })[0].genre
                          : "",
                        qtd: items.filter((i, indexi) => {
                          return teste == indexi;
                        })[0].qtd,
                        urlFixed: null,
                      });

                      setItems(data);
                    }}
                  >
                    <DeleteIcon fontSize="small" style={{ color: "#353b48" }} />
                  </IconButton>
                </div>
              </Box>
            ) : null}
          </div>
        </Box>

        <ClickAwayListener
          onClickAway={() => {
            setShowListContent({ [String(teste)]: false });
          }}
        >
          <div style={{ width: 380 }}>
            {showListContent[String(teste)] &&
              value == "Institucional" &&
              getInfoClient &&
              getInfoClient.Segmentcontrols.map((s) => {
                return (
                  <div>
                    <p style={{ fontWeight: "bold", marginTop: 15 }}>
                      {s.Segment.name}
                    </p>
                    {listContents
                      .filter(
                        (cf) =>
                          (cf.segmentId == s.Segment.id) &
                          (cf.Typecontent.Contentgroup.name == value)
                      )
                      .map((c) => {
                        return (
                          <Box
                            display="flex"
                            alignItems="center"
                            justifyContent="space-between"
                          >
                            <Checkbox
                              checked={
                                items.filter((i, indexi) => {
                                  return teste == indexi;
                                })[0].urlFixed == c.path_s3
                              }
                              onChange={(event) => {
                                let data = items.filter((i, indexi) => {
                                  return teste != indexi;
                                });

                                data.splice(teste, 0, {
                                  id: items.filter((i, indexi) => {
                                    return teste == indexi;
                                  })[0].id,
                                  tipo: items.filter((i, indexi) => {
                                    return teste == indexi;
                                  })[0].tipo,
                                  genre: items.filter((i, indexi) => {
                                    return teste == indexi;
                                  })[0].genre
                                    ? items.filter((i, indexi) => {
                                        return teste == indexi;
                                      })[0].genre
                                    : "",
                                  qtd: items.filter((i, indexi) => {
                                    return teste == indexi;
                                  })[0].qtd,
                                  urlFixed: items.filter((i, indexi) => {
                                    return teste == indexi;
                                  })[0].urlFixed
                                    ? items.filter((i, indexi) => {
                                        return teste == indexi;
                                      })[0].urlFixed == c.path_s3
                                      ? null
                                      : c.path_s3
                                    : c.path_s3,
                                });

                                setItems(data);
                                setShowListContent({ [String(teste)]: false });
                              }}
                              value="true"
                            />

                            <audio
                              style={{ borderRadius: 20 }}
                              controls
                              controlsList="nodownload"
                              className="mb-2 mt-3 mb-3"
                              src={c.path_s3}
                            />
                          </Box>
                        );
                      })}
                  </div>
                );
              })}

            {showListContent[String(teste)] &&
              value == "Vinheta" &&
              getInfoClient &&
              getInfoClient.Vignettes.map((v) => {
                return (
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Checkbox
                      checked={
                        items.filter((i, indexi) => {
                          return teste == indexi;
                        })[0].urlFixed == v.path_s3
                      }
                      onChange={(event) => {
                        let data = items.filter((i, indexi) => {
                          return teste != indexi;
                        });

                        data.splice(teste, 0, {
                          id: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].id,
                          tipo: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].tipo,
                          qtd: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].qtd,
                          genre: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].genre
                            ? items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].genre
                            : "",
                          urlFixed: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].urlFixed
                            ? items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].urlFixed == v.path_s3
                              ? null
                              : v.path_s3
                            : v.path_s3,
                        });

                        setItems(data);
                        setShowListContent({ [String(teste)]: false });
                      }}
                      value="true"
                    />

                    <audio
                      style={{ borderRadius: 20 }}
                      controls
                      controlsList="nodownload"
                      className="mb-2 mt-3 mb-3"
                      src={v.path_s3}
                    />
                  </Box>
                );
              })}

            {showListContent[String(teste)] &&
              String(value).indexOf("Comercial") > -1 &&
              getInfoClient &&
              getInfoClient.Commercials.filter(
                (cc) => cc.manualType == String(value).split("-")[1].trim()
              ).map((c) => {
                return (
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Checkbox
                      checked={
                        items.filter((i, indexi) => {
                          return teste == indexi;
                        })[0].urlFixed == c.path_s3
                      }
                      onChange={(event) => {
                        let data = items.filter((i, indexi) => {
                          return teste != indexi;
                        });

                        data.splice(teste, 0, {
                          id: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].id,
                          tipo: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].tipo,
                          qtd: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].qtd,
                          genre: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].genre
                            ? items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].genre
                            : "",
                          genre: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].genre
                            ? items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].genre
                            : "",
                          urlFixed: items.filter((i, indexi) => {
                            return teste == indexi;
                          })[0].urlFixed
                            ? items.filter((i, indexi) => {
                                return teste == indexi;
                              })[0].urlFixed == c.path_s3
                              ? null
                              : c.path_s3
                            : c.path_s3,
                        });

                        setItems(data);
                        setShowListContent({ [String(teste)]: false });
                      }}
                      value="true"
                    />

                    <audio
                      style={{ borderRadius: 20 }}
                      controls
                      controlsList="nodownload"
                      className="mb-2 mt-3 mb-3"
                      src={c.path_s3}
                    />
                  </Box>
                );
              })}
          </div>
        </ClickAwayListener>
      </div>
    );
  });

  const SortableList = SortableContainer(({ items }) => {
    return (
      <div
        onClick={(event) => {
          event.stopPropagation();
        }}
        style={{ display: "flex", flex: 1, flexDirection: "column" }}
      >
        {items.map((value, index) => {
          return (
            <SortableItem
              key={`item-${value.tipo}`}
              index={index}
              value={value.tipo}
              qtd={value.qtd}
              genre={value.genre}
              teste={index}
            />
          );
        })}
      </div>
    );
  });

  const SortableComponent = () => {
    return (
      <SortableList
        helperClass="sortableHelper"
        items={items}
        distance={1}
        onSortEnd={onSortEnd}
        onClick={(event) => {
          event.stopPropagation();
        }}
      />
    );
  };

  useEffect(() => {
    if (indexValuesPreviewList) {
      setValue(indexValuesPreviewList);
    }
  }, [indexValuesPreviewList]);

  const onSortEnd = ({ oldIndex, newIndex }) => {
    setItems(arrayMove(items, oldIndex, newIndex));
  };

  const onShowNewVignette = () => {
    setShowNewVignette(true);
  };

  const onCloseNewVignette = () => {
    setShowNewVignette(false);
  };

  const onShowNewCommercial = () => {
    setShowNewCommercial(true);
  };

  const onCloseNewCommercial = () => {
    setShowNewCommercial(false);
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    if (isRuleVignetteSazonal & (values.sazonalVignetteId < 1)) {
      showMessage(
        "Atenção",
        `Você ativou vinheta Sazonal e não selecionou um opção.`,
        "warning"
      );
      return;
    }

    if (isRuleVignetteInstitucional & (values.institucionalVignetteId < 1)) {
      showMessage(
        "Atenção",
        `Você ativou vinheta Institucional e não selecionou um opção.`,
        "warning"
      );
      return;
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(
        `/clientsetting/edit/${getInfoClient.Clientsettings[0].clientId}`,
        values
      )
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          if (nameChannel != "") {
            api
              .put(`/client/edit/${getInfoClient.Clientsettings[0].clientId}`, {
                name: nameChannel,
              })
              .then((response) => response.data)
              .then((resp) => {
                if (resp.message == "success") {
                  api
                    .delete(
                      `/segmentcontrol/clean/${getInfoClient.Clientsettings[0].clientId}`
                    )
                    .then((response) => response.data)
                    .then((resp) => {
                      if (resp.message == "success") {
                        for (
                          let index = 0;
                          index < personName.length;
                          index++
                        ) {
                          const element = personName[index];

                          api
                            .post("/segmentcontrol/new", {
                              clientId:
                                getInfoClient.Clientsettings[0].clientId,
                              segmentId: element,
                            })
                            .then((response) => response.data)
                            .then((respc) => {
                              if (respc.message == "success") {
                                if (getInfoClient.Previewlists.length == 0) {
                                  api
                                    .post("/previewlist/new", {
                                      clientId:
                                        getInfoClient.Clientsettings[0]
                                          .clientId,
                                      preview: JSON.stringify(items),
                                    })
                                    .then((response) => response.data)
                                    .then((respc) => {
                                      if (respc.message == "success") {
                                        if (index + 1 == personName.length) {
                                          showMessage(
                                            "Modificação de Canal",
                                            `realizada com sucesso.`,
                                            "success"
                                          );
                                          getAllChannel();
                                          setIsLoading(false);
                                          onCloseModal();
                                        }
                                      } else {
                                        showMessage(
                                          "Novo Canal",
                                          `Falhou.`,
                                          "error"
                                        );
                                      }
                                    })
                                    .catch((error) => {
                                      console.log(error);
                                    });
                                } else {
                                  api
                                    .put(
                                      `/previewlist/edit/${getInfoClient.Previewlists[0].id}`,
                                      {
                                        clientId:
                                          getInfoClient.Clientsettings[0]
                                            .clientId,
                                        preview: JSON.stringify(items),
                                      }
                                    )
                                    .then((response) => response.data)
                                    .then((respc) => {
                                      if (respc.message == "success") {
                                        if (index + 1 == personName.length) {
                                          showMessage(
                                            "Modificação de Canal",
                                            `realizada com sucesso.`,
                                            "success"
                                          );
                                          getAllChannel();
                                          setIsLoading(false);
                                          onCloseModal();
                                        }
                                      } else {
                                        showMessage(
                                          "Novo Canal",
                                          `Falhou.`,
                                          "error"
                                        );
                                      }
                                    })
                                    .catch((error) => {
                                      console.log(error);
                                    });
                                }
                              } else {
                                showMessage("Novo Canal", `Falhou.`, "error");
                              }
                            })
                            .catch((error) => {
                              console.log(error);
                            });
                        }
                      } else {
                        showMessage(
                          "Modificação de Channel",
                          `Falhou.`,
                          "error"
                        );
                      }
                    })
                    .catch((error) => {
                      console.log(error);
                    });
                } else {
                  showMessage("Modificação de Channel", `Falhou.`, "error");
                }
              })
              .catch((error) => {
                console.log(error);
              });
          } else {
            showMessage(
              "Modificação de Canal",
              `realizada com sucesso.`,
              "success"
            );
            getAllChannel();
            setIsLoading(false);
            onCloseModal();
          }
        } else {
          showMessage("Modificação de Channel", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllComerciais = () => {
    setActiveGetAllCommertial(true);
  };

  const disableGetAllCommercials = () => {
    setActiveGetAllCommertial(false);
  };

  const getAllVignette = () => {
    setActiveGetAllVignette(true);
  };

  const disableGetAllVignette = () => {
    setActiveGetAllVignette(false);
  };

  useEffect(() => {
    if (countGetCli == 0) {
      (async () => {
        console.log(getInfoClient);
        setCountGetCli(1);
        if (getInfoClient) {
          setNameChannel(getInfoClient.name);
          setListCommercials(getInfoClient.Commercials);

          const respContent = await api.get("/content");

          console.log(respContent.data);
          setListContents(respContent.data);

          let dataArraySeg = [];
          for (
            let index = 0;
            index < getInfoClient.Segmentcontrols.length;
            index++
          ) {
            const element = getInfoClient.Segmentcontrols[index];

            dataArraySeg.push(element.segmentId);
          }

          setPersonName(dataArraySeg);
          setItems(
            getInfoClient.Previewlists.length > 0
              ? JSON.parse(getInfoClient.Previewlists[0].preview)
              : []
          );
        }
      })();
    }
  }, [getInfoClient]);

  useEffect(() => {
    api
      .get("/segment/content")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListSegment(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    api
      .get(`/vignette/[${idClient}]`)
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListVignette(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });

    getContentGroup();
    getNameDistict();
    getListExecuted();
  }, []);

  useEffect(() => {
    getAudioQuality();
  }, []);

  useEffect(() => {
    console.log(getInfoClient.Clientsettings[0]);
    if (getInfoClient.Clientsettings[0]) {
      if (getInfoClient.Clientsettings[0].sazonalVignetteId > 0) {
        setIsRuleVignetteSazonal(true);
      }
      if (getInfoClient.Clientsettings[0].institucionalVignetteId > 0) {
        setIsRuleVignetteInstitucional(true);
      }

      setValues({
        qtdMusic: getInfoClient.Clientsettings[0].qtdMusic,
        qtdMusicCommertial: getInfoClient.Clientsettings[0].qtdMusicCommertial,
        isRuleVignetteDefault:
          getInfoClient.Clientsettings[0].isRuleVignetteDefault,
        isRuleVignetteComercial:
          getInfoClient.Clientsettings[0].isRuleVignetteComercial,
        sazonalVignetteId: getInfoClient.Clientsettings[0].sazonalVignetteId,
        institucionalVignetteId:
          getInfoClient.Clientsettings[0].institucionalVignetteId,
        isCloseChannelAuto: getInfoClient.Clientsettings[0].isCloseChannelAuto,
        timeClose:
          (getInfoClient.Clientsettings[0].timeClose == "") |
          !getInfoClient.Clientsettings[0].timeClose
            ? null
            : getInfoClient.Clientsettings[0].timeClose,
        colorFont: getInfoClient.Clientsettings[0].colorFont,
        colorBackground: getInfoClient.Clientsettings[0].colorBackground,
        audioQualityId: getInfoClient.Clientsettings[0].audioQualityId,
      });
    }
  }, [getInfoClient.Clientsettings[0]]);

  const getAudioQuality = () => {
    api
      .get("/audioquality")
      .then((resp) => resp.data)
      .then((response) => {
        setListAudioQuality(response);
      })
      .catch((error) => console.log(error));
  };

  const getListExecuted = () => {
    api
      .get(
        `/exultedmusic/periodo/${idClient}/${moment(dataDeListExecuteds).format(
          "YYYY-MM-DD"
        )}/${moment(dataParaListExecuteds).format("YYYY-MM-DD")}`
      )
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListExecuteds(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getNameDistict = async () => {
    const res = await api.get(`/commercial/distinct/[${idClient}]`);

    if (res) {
      console.log(res.data);

      setListTypeCommercial(res.data);
    }
  };

  const getContentGroup = async () => {
    const res = await api.get("/contentgroup/admin");

    if (res) {
      const data = res.data;

      const arrayC = data.concat([
        { name: "Comercial" },
        { name: "Hora Certa" },
        { name: "Vinheta" },
      ]);
      setContentGroup(arrayC);
    }
  };

  const onCloseSelectTypeCommercial = () => {
    setOpenSelectTypeCommercial(false);
  };

  const onOpenSelectTypeCommercial = () => {
    setOpenSelectTypeCommercial(true);
  };

  return (
    <form
      style={{
        height: "100%",
        width: "100%",
        backgroundColor: "white",
      }}
      autoComplete="off"
      noValidate
    >
      <PerfectScrollbar>
        <div
          style={{
            backgroundColor: "white",
          }}
          // component="article"
          // className="pb-2"
        >
          <Card
            style={{
              borderRadius: 0,
              height: "100%",
              width: "100%",
            }}
          >
            <Box
              display="flex"
              justifyContent={isMobile ? "space-between" : "flex-end"}
              position={isMobile ? "relative" : "absolute"}
              className="w-100 mt-3 mr-4"
              p={0}
            >
              {value == 0 ? (
                <div />
              ) : (
                <IconButton
                  style={{ backgroundColor: "white" }}
                  onClick={onCloseModal}
                >
                  <CloseIcon />
                </IconButton>
              )}

              {value == 0 ? (
                <IconButton
                  style={{ backgroundColor: "white" }}
                  onClick={onCloseModal}
                >
                  <CloseIcon />
                </IconButton>
              ) : (
                <Button
                  onClick={() => {
                    setValue(0);
                  }}
                  style={{
                    backgroundColor: "red",
                    color: "white",
                    fontWeight: "bold",
                    marginRight: 40,
                  }}
                  variant="contained"
                >
                  Voltar menu de opções
                </Button>
              )}
            </Box>
            <CardHeader
              subheader="Preencha as informações de seu canal"
              title={`${
                value == 1
                  ? "Configurações do Canal"
                  : value == 2
                  ? "Ordem de Execução"
                  : value == 4
                  ? "Vinhetas"
                  : value == 5
                  ? "Modificando Canal"
                  : "Selecione um Opção"
              }`}
            />
            <Divider />

            <CardContent
              style={{
                paddingLeft: !isMobile ? (value == 2 ? 0 : "10%") : 0,
                paddingRight: !isMobile ? (value == 2 ? 0 : "10%") : 0,
              }}
            >
              <div>
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Box margin={-4} display="flex" flexDirection="column">
                    {optionMenu.map((oMenu) => {
                      return isChannel &
                        (oMenu.name == "Meus Conteúdos") ? null : (
                        <IconButton
                          style={{ borderRadius: 15 }}
                          onClick={() => oMenu.onClick()}
                        >
                          <Card
                            style={{
                              width: "100%",
                              borderRadius: 15,
                              borderWidth: 0.1,
                              borderStyle: "solid",
                              borderColor: "rgba(192, 192, 192, 0.4)",
                            }}
                          >
                            <Box
                              display="flex"
                              justifyContent="space-between"
                              alignItems="center"
                            >
                              <Box display="flex" flexDirection="row">
                                {oMenu.icon}
                                <Divider
                                  style={{ height: 80 }}
                                  orientation="vertical"
                                />
                              </Box>
                              <h5
                                style={{ color: "#3d3d3d" }}
                                className="text-black text-center ml-2 mr-2"
                              >
                                {oMenu.name}
                              </h5>
                              <div />
                            </Box>
                          </Card>
                        </IconButton>
                      );
                    })}
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Box mt={-2} />
                  <Box>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={isRuleVignetteSazonal}
                          onChange={(event) => {
                            setIsRuleVignetteSazonal(event.target.checked);
                            setValues({
                              ...values,
                              sazonalVignetteId: !event.target.checked
                                ? 0
                                : getInfoClient.Clientsettings[0]
                                    .sazonalVignetteId,
                            });
                          }}
                          name="isRuleVignetteSazonal"
                          color="primary"
                        />
                      }
                      label="Selecionar vinheta para executar após conteúdo Sazonal."
                    />
                    {isRuleVignetteSazonal ? (
                      <Box mt={1} mb={4} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Vinheta
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="vignetteId"
                            id="vignetteId"
                            value={values.sazonalVignetteId}
                            onChange={(event) => {
                              setValues({
                                ...values,
                                sazonalVignetteId: event.target.value,
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listVignette &&
                              listVignette.map((Vignettes) => (
                                <option value={Vignettes.id}>{`${
                                  Vignettes.name
                                } | ${
                                  Vignettes.Typevignette
                                    ? Vignettes.Typevignette.name
                                    : ""
                                }`}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    ) : null}

                    <Divider className="mt-1 mb-1" />
                  </Box>
                  <Box>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={isRuleVignetteInstitucional}
                          onChange={(event) => {
                            setIsRuleVignetteInstitucional(
                              event.target.checked
                            );

                            setValues({
                              ...values,
                              institucionalVignetteId: !event.target.checked
                                ? 0
                                : getInfoClient.Clientsettings[0]
                                    .institucionalVignetteId,
                            });
                          }}
                          name="isRuleVignetteInstitucional"
                          color="primary"
                        />
                      }
                      label="Selecionar vinheta para executar após conteúdo Institucional."
                    />
                    {isRuleVignetteInstitucional ? (
                      <Box mt={1} mb={4} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Vinheta
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="vignetteId"
                            id="vignetteId"
                            value={values.institucionalVignetteId}
                            onChange={(event) => {
                              setValues({
                                ...values,
                                institucionalVignetteId: event.target.value,
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listVignette &&
                              listVignette.map((Vignettes) => (
                                <option value={Vignettes.id}>{`${
                                  Vignettes.name
                                } | ${
                                  Vignettes.Typevignette
                                    ? Vignettes.Typevignette.name
                                    : ""
                                }`}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    ) : null}
                    <Divider className="mt-1 mb-1" />
                  </Box>
                  <Box>
                    <FormControlLabel
                      style={{
                        marginLeft: 5,
                        marginTop: 20,
                        marginBottom: 5,
                      }}
                      control={<a />}
                      label="Selecionar qualidade do audio."
                    />
                    <FormControl
                      style={{
                        marginBottom: 20,
                      }}
                      requireds
                      variant="outlined"
                      fullWidth
                    >
                      <InputLabel htmlFor="age-native-required">
                        Qualidade
                      </InputLabel>
                      <Select
                        required
                        native
                        disabled={isLoading}
                        name="audioQualityId"
                        id="audioQualityId"
                        value={values.audioQualityId}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            audioQualityId: event.target.value,
                          });
                        }}
                      >
                        <option aria-label="None" value="" />
                        {listAudioQuality &&
                          listAudioQuality.map((AudioQuality) => (
                            <option value={AudioQuality.id}>
                              {AudioQuality.name}
                            </option>
                          ))}
                      </Select>
                    </FormControl>
                    <Divider className="mt-1 mb-1" />
                  </Box>

                  <div>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={JSON.parse(values.isCloseChannelAuto)}
                          onChange={(event) => {
                            setValues({
                              ...values,
                              isCloseChannelAuto: String(event.target.checked),
                              timeClose: event.target.checked
                                ? JSON.stringify([
                                    { time: moment().format("HH:mm") },
                                  ])
                                : null,
                            });
                          }}
                          name="isCloseChannelAuto"
                          color="primary"
                        />
                      }
                      label="Fechar canal automático"
                    />
                    {!JSON.parse(values.isCloseChannelAuto) ? null : (
                      <div>
                        {!values.timeClose
                          ? null
                          : JSON.parse(values.timeClose).map((time, index) => (
                              <Box marginTop={2} display="flex">
                                {
                                  <IconButton
                                    disabled={isLoading}
                                    style={{ marginRight: 5 }}
                                    onClick={() => {
                                      let arrayData = JSON.parse(
                                        values.timeClose
                                      ).filter((t, indexT) => indexT != index);

                                      setValues({
                                        ...values,
                                        timeClose: JSON.stringify(arrayData),
                                      });
                                    }}
                                    color="inherit"
                                  >
                                    <DeleteIcon style={{ color: "#ff4d4d" }} />
                                  </IconButton>
                                }
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                  <KeyboardTimePicker
                                    id="time-picker"
                                    ampm={false}
                                    inputVariant="outlined"
                                    label="Time picker"
                                    name="startHour"
                                    label="Hora Inicio"
                                    value={moment(
                                      `${moment().format("YYYY-MM-DD")} ${
                                        time.time
                                      }`,
                                      "YYYY-MM-DD HH:mm"
                                    )}
                                    onChange={(event) => {
                                      let arrayData = JSON.parse(
                                        values.timeClose
                                      ).filter((t, indexT) => indexT != index);

                                      arrayData.splice(index, 0, {
                                        time: moment(event).format("HH:mm"),
                                      });

                                      console.log(arrayData);

                                      setValues({
                                        ...values,
                                        timeClose: JSON.stringify(arrayData),
                                      });
                                    }}
                                    style={{
                                      width: 125,
                                      marginRight: 10,
                                    }}
                                    KeyboardButtonProps={{
                                      "aria-label": "change time",
                                    }}
                                    keyboardIcon={<AccessTimeIcon />}
                                  />
                                </MuiPickersUtilsProvider>
                                {!(
                                  JSON.parse(values.timeClose).length ==
                                  index + 1
                                ) ? null : (
                                  <IconButton
                                    disabled={isLoading}
                                    style={{ marginLeft: 5 }}
                                    onClick={() => {
                                      let arrayData = JSON.parse(
                                        values.timeClose
                                      );

                                      arrayData.push({
                                        time: moment().format("HH:mm"),
                                      });

                                      setValues({
                                        ...values,
                                        timeClose: JSON.stringify(arrayData),
                                      });
                                    }}
                                    color="inherit"
                                  >
                                    <AddCircleIcon
                                      style={{ color: "#2ecc71" }}
                                    />
                                  </IconButton>
                                )}
                              </Box>
                            ))}
                      </div>
                    )}
                  </div>
                  <Divider className="mt-4 mb-4" />
                  <Typography variant="h6" className="text-center mb-4">
                    Cores Frases Institucionais
                  </Typography>
                  <Box
                    display="flex"
                    flexWrap="wrap"
                    justifyContent="space-between"
                  >
                    <div>
                      {/* Color Frases */}
                      <Typography
                        style={{ fontWeight: "bold" }}
                        variant="body1"
                        className="text-center"
                      >
                        Cor do Fundo
                      </Typography>
                      <Card
                        style={{
                          ...styleButtonIcon,
                          backgroundColor: values.colorBackground,
                          height: 40,
                          width: 225,
                          marginBottom: 10,
                        }}
                      />
                      <ChromePicker
                        color={values.colorBackground}
                        onChange={onChangeComplete}
                      />
                    </div>
                    <div>
                      {/* Color Frases */}
                      <Typography
                        style={{ fontWeight: "bold" }}
                        variant="body1"
                        className="text-center"
                      >
                        Cor da Fonte
                      </Typography>
                      <Card
                        style={{
                          ...styleButtonIcon,
                          backgroundColor: values.colorFont,
                          height: 40,
                          width: 225,
                          marginBottom: 10,
                        }}
                      />
                      <ChromePicker
                        color={values.colorFont}
                        onChange={onChangeCompleteFont}
                      />
                    </div>
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                  <div>
                    {/* Tipos de Conteudo */}
                    <div>
                      <Typography
                        style={{ fontWeight: "bold" }}
                        variant="h6"
                        color="textPrimary"
                      >
                        Tipos de Conteúdos
                      </Typography>
                      <Typography
                        style={{ marginBottom: 20 }}
                        variant="body2"
                        color="textSecondary"
                      >
                        Selecione um tipo de conteúdo.
                      </Typography>
                      <Box
                        display="flex"
                        flex={1}
                        flexDirection="row"
                        justifyContent="flex-start"
                        flexWrap="wrap"
                        mb={10}
                      >
                        {contentgroup &&
                          contentgroup.map((c) => (
                            <Chip
                              className="m-2"
                              label={c.name}
                              style={{
                                ...styleButtonIcon,
                                color: "white",
                                backgroundColor: listColor.filter(
                                  (lc) =>
                                    String(c.name)
                                      .toUpperCase()
                                      .split("-")[0]
                                      .indexOf(String(lc.tipo).toUpperCase()) >
                                    -1
                                ).length > 0 ? listColor.filter(
                                  (lc) =>
                                    String(c.name)
                                      .toUpperCase()
                                      .split("-")[0]
                                      .indexOf(String(lc.tipo).toUpperCase()) >
                                    -1
                                )[0].color : '#e84393',
                                borderColor: listColor.filter(
                                  (lc) =>
                                    String(c.name)
                                      .toUpperCase()
                                      .split("-")[0]
                                      .indexOf(String(lc.tipo).toUpperCase()) >
                                    -1
                                ).length > 0 ? listColor.filter(
                                  (lc) =>
                                    String(c.name)
                                      .toUpperCase()
                                      .split("-")[0]
                                      .indexOf(String(lc.tipo).toUpperCase()) >
                                    -1
                                )[0].color : '#e84393',
                                fontSize: 15,
                                width: 150,
                              }}
                              onClick={() => {
                                if (c.name == "Comercial") {
                                  onOpenSelectTypeCommercial();
                                } else {
                                  var data = items.filter(
                                    (i, index) => index > -1
                                  );
                                  data.push({
                                    id: items.length + 1,
                                    tipo: c.name,
                                    qtd: 0,
                                    urlFixed: null,
                                  });

                                  setItems(data);
                                }
                              }}
                              onDelete={() => {}}
                              deleteIcon={
                                <AddIcon style={{ color: "white" }} />
                              }
                              variant="outlined"
                            />
                          ))}
                      </Box>
                    </div>
                    <Box
                      display="flex"
                      flexDirection={!isMobile ? "row" : "column"}
                      justifyContent="space-between"
                      mb={1}
                      style={{ height: "100%" }}
                    >
                      {/* <div
                      style={{
                        width: 1,
                        backgroundColor: "silver",
                        marginRight: 40,
                      }}
                    /> */}

                      <div
                        style={{
                          flex: 1,
                        }}
                      >
                        <Box display={"flex"}>
                          <div>
                            <Typography
                              style={{ fontWeight: "bold" }}
                              variant="h6"
                              color="textPrimary"
                            >
                              Configuração Bloco Musical
                            </Typography>
                            <Typography
                              style={{ width: 300, marginBottom: 25 }}
                              variant="body2"
                              color="textSecondary"
                            >
                              Configure seus conteúdos e quantidade de musicas
                              que cada bloco tocará.
                            </Typography>
                          </div>
                          <Chip
                            className="m-2"
                            label={
                              showPreview ? "Menos informação" : "Ver preview"
                            }
                            style={{
                              ...styleButtonIcon,
                              color: "black",
                              textAlign: "center",
                              fontSize: 15,
                              width: 150,
                            }}
                            onClick={() => {
                              setShowPreview(!showPreview);
                            }}
                            onDelete={() => {}}
                            deleteIcon={<AddIcon style={{ color: "black" }} />}
                            variant="outlined"
                          />
                        </Box>
                        <SortableComponent />
                      </div>

                      {!showPreview ? null : (
                        <div
                          style={{
                            width: 1,
                            backgroundColor: "silver",
                            marginRight: 40,
                            marginLeft: 40,
                          }}
                        />
                      )}

                      {!showPreview ? null : (
                        <div
                          style={{
                            flex: 1,
                          }}
                        >
                          <Typography
                            style={{ fontWeight: "bold" }}
                            variant="h6"
                            color="textPrimary"
                          >
                            Preview de Execução
                          </Typography>
                          <Typography
                            style={{ width: 300, marginBottom: 35 }}
                            variant="body2"
                            color="textSecondary"
                          >
                            Veja aqui o preview da sequencia que será execultado
                            em sua lista .
                          </Typography>
                          {items.length > 0 &&
                            items.map((value) => {
                              let musicData = [];
                              for (let index = 0; index < value.qtd; index++) {
                                musicData.push(index);
                              }
                              return (
                                <div className="w-100">
                                  {musicData.length > 0 &&
                                    musicData.map((j) => (
                                      <div
                                        style={{
                                          display: "flex",
                                          marginBottom: 10,
                                          borderRadius: 10,
                                          borderWidth: 1.5,
                                          borderStyle: "solid",
                                          borderColor: listColor.filter(
                                            (lc) =>
                                              String("Musica")
                                                .toUpperCase()
                                                .split("-")[0]
                                                .indexOf(
                                                  String(lc.tipo).toUpperCase()
                                                ) > -1
                                          )[0].color,
                                        }}
                                        className="w-100"
                                      >
                                        <div
                                          style={{
                                            height: 65,
                                            width: 10,
                                            marginLeft: -2,
                                            borderTopLeftRadius: 10,
                                            borderBottomLeftRadius: 10,
                                            backgroundColor: listColor.filter(
                                              (lc) =>
                                                String("Musica")
                                                  .toUpperCase()
                                                  .split("-")[0]
                                                  .indexOf(
                                                    String(
                                                      lc.tipo
                                                    ).toUpperCase()
                                                  ) > -1
                                            )[0].color,
                                          }}
                                        />
                                        <Box
                                          className="p-1 w-100"
                                          display="flex"
                                          alignItems="center"
                                          justifyContent="space-between"
                                        >
                                          <Box
                                            className="p-1"
                                            display="flex"
                                            alignItems="center"
                                            justifyContent="flex-start"
                                          >
                                            <div>
                                              <Avatar
                                                alt="Musica"
                                                variant="square"
                                                className="av-radio mr-2"
                                              >
                                                M
                                              </Avatar>
                                            </div>
                                            <div>
                                              <div className="d-flex">
                                                <h5 className="text-black mr-4">
                                                  Musica Teste
                                                </h5>
                                              </div>
                                              <Typography
                                                style={{ color: "black" }}
                                                variant="body1"
                                              >
                                                Artista
                                              </Typography>
                                            </div>
                                          </Box>
                                          <Typography
                                            style={{ color: "black" }}
                                            variant="body1"
                                          >
                                            0:00
                                          </Typography>
                                        </Box>
                                      </div>
                                    ))}

                                  <div
                                    style={{
                                      display: "flex",
                                      marginBottom: 10,
                                      borderRadius: 10,
                                      borderWidth: 1.5,
                                      borderStyle: "solid",
                                      borderColor: listColor.filter(
                                        (lc) =>
                                          String(value.tipo)
                                            .toUpperCase()
                                            .split("-")[0]
                                            .indexOf(
                                              String(lc.tipo).toUpperCase()
                                            ) > -1
                                      )[0].color,
                                    }}
                                    className="w-100"
                                  >
                                    <div
                                      style={{
                                        height: 70,
                                        width: 10,
                                        marginLeft: -2,
                                        marginTop: -2,
                                        borderTopLeftRadius: 10,
                                        borderBottomLeftRadius: 10,
                                        backgroundColor: listColor.filter(
                                          (lc) =>
                                            String(value.tipo)
                                              .toUpperCase()
                                              .split("-")[0]
                                              .indexOf(
                                                String(lc.tipo).toUpperCase()
                                              ) > -1
                                        )[0].color,
                                      }}
                                    />
                                    <Box
                                      className="p-1 w-100"
                                      display="flex"
                                      alignItems="center"
                                      justifyContent="space-between"
                                    >
                                      <Box
                                        className="p-1"
                                        display="flex"
                                        alignItems="center"
                                        justifyContent="flex-start"
                                      >
                                        <div>
                                          <Avatar
                                            alt="Conteudo"
                                            variant="square"
                                            className="av-radio mr-2"
                                          >
                                            {value.tipo[0]}
                                          </Avatar>
                                        </div>
                                        <div>
                                          <div className="d-flex">
                                            <h5 className="text-black mr-4">
                                              {value.tipo}
                                            </h5>
                                          </div>
                                          <Typography
                                            style={{ color: "black" }}
                                            variant="body1"
                                          >
                                            Detalhes
                                          </Typography>
                                        </div>
                                      </Box>
                                      <Typography
                                        style={{ color: "black" }}
                                        variant="body1"
                                      >
                                        0:00
                                      </Typography>
                                    </Box>

                                    {/* <div
                                  className="w-100"
                                  style={{
                                    background: "rgba(0,0,0,0.2)",
                                    height: 0.09,
                                  }}
                                /> */}
                                  </div>
                                </div>
                              );
                            })}
                        </div>
                      )}
                    </Box>
                    <Modal
                      aria-labelledby="transition-modal-title"
                      aria-describedby="transition-modal-description"
                      className={classes.modal}
                      open={openSelectTypeCommercial}
                      onClose={onCloseSelectTypeCommercial}
                      closeAfterTransition
                      BackdropComponent={Backdrop}
                      BackdropProps={{
                        timeout: 500,
                      }}
                    >
                      <Fade in={openSelectTypeCommercial}>
                        <PerfectScrollbar>
                          <div className="mb-4">
                            <Box
                              display="flex"
                              justifyContent="space-between"
                              className="w-100 mt-2 mb-2 mr-2 ml-2"
                              p={0}
                            >
                              <div />

                              <IconButton
                                style={{
                                  backgroundColor: "white",
                                  marginRight: 20,
                                }}
                                onClick={onCloseSelectTypeCommercial}
                              >
                                <CloseIcon />
                              </IconButton>
                            </Box>
                            <Card
                              style={{
                                borderRadius: 20,
                                width: !isMobile ? 400 : "100%",
                                padding: 40,
                              }}
                            >
                              <FormControl
                                className="mb-2"
                                requireds
                                variant="outlined"
                                fullWidth
                              >
                                <InputLabel htmlFor="age-native-required">
                                  Tipo de Conteúdo
                                </InputLabel>
                                <Select
                                  required
                                  disabled={isLoading}
                                  native
                                  name="contentgroupId"
                                  id="contentgroupId"
                                  onChange={(event) => {
                                    var data = items.filter(
                                      (i, index) => index > -1
                                    );
                                    data.push({
                                      id: items.length + 1,
                                      tipo: `Comercial - ${event.target.value}`,
                                      qtd: 0,
                                      urlFixed: null,
                                    });

                                    setItems(data);
                                    onCloseSelectTypeCommercial();
                                  }}
                                >
                                  <option aria-label="None" value="" />
                                  {listTypeCommercial &&
                                    listTypeCommercial.map(
                                      (Groups) =>
                                        Groups.manualType && (
                                          <option value={Groups.manualType}>
                                            {Groups.manualType}
                                          </option>
                                        )
                                    )}
                                </Select>
                              </FormControl>
                            </Card>
                          </div>
                        </PerfectScrollbar>
                      </Fade>
                    </Modal>
                  </div>
                </TabPanel>
                <TabPanel value={value} index={4} dir={theme.direction}>
                  <Vignette
                    onShowNewVignette={onShowNewVignette}
                    activeGetAllVignette={activeGetAllVignette}
                    disableGetAllVignette={disableGetAllVignette}
                    idClient={idClient}
                  />
                </TabPanel>
                <TabPanel value={value} index={5} dir={theme.direction}>
                  <TextField
                    value={nameChannel}
                    onChange={(event) => setNameChannel(event.target.value)}
                    label="Nome do Canal"
                    margin="normal"
                    id="name"
                    name="name"
                    variant="outlined"
                    className={""}
                    fullWidth
                    required
                  />
                  <Box display="flex" justifyContent="center">
                    <FormControl requireds variant="outlined" fullWidth>
                      <InputLabel>Segmento</InputLabel>
                      <Select
                        required
                        multiple
                        name="segmentId"
                        id="segmentId"
                        //input={<Input id="select-multiple-chip" />}
                        value={personName}
                        onChange={handleChangeSelect}
                        renderValue={(selected) => (
                          <div className={classes.chips}>
                            {selected.map((value) => (
                              <Chip
                                key={value}
                                label={
                                  listSegment &&
                                  listSegment.filter(
                                    (segment) => segment.id == value
                                  )[0].name
                                }
                                onMouseDown={(event) => {
                                  event.stopPropagation();
                                }}
                                className={classes.chip}
                                style={{
                                  backgroundColor: "rgba(235, 47, 6, 0.9)",
                                  color: "white",
                                }}
                                deleteIcon={
                                  <CancelIcon
                                    style={{
                                      color: "rgba(255, 255, 255, 0.7)",
                                    }}
                                  />
                                }
                                onDelete={() => {
                                  setPersonName(
                                    personName.filter((id) => id !== value)
                                  );
                                }}
                              />
                            ))}
                          </div>
                        )}
                        MenuProps={MenuProps}
                      >
                        {listSegment &&
                          listSegment.map((segment) => (
                            <MenuItem
                              key={segment.id}
                              value={segment.id}
                              classes={{
                                selected: {
                                  fontcolor: "rgba(235, 47, 6, 0.9)",
                                },
                              }}
                              style={{
                                ...getStyles(segment.id, personName, themes),
                              }}
                            >
                              {segment.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={6} dir={theme.direction}>
                  <Box
                    display="flex"
                    flexWrap="wrap"
                    mb={4}
                    alignItems="center"
                    justifyContent="center"
                  >
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        id="time-picker"
                        format="dd/MM/yyyy"
                        inputVariant="outlined"
                        disabled={isLoading}
                        label="Date picker inline"
                        name="startDate"
                        label="Inicio"
                        value={dataDeListExecuteds}
                        onChange={(event) => {
                          setDataDeListExecuteds(event);
                        }}
                        style={{ margin: 10, width: 200 }}
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                        keyboardIcon={<TodayIcon />}
                      />
                    </MuiPickersUtilsProvider>

                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        id="time-picker"
                        format="dd/MM/yyyy"
                        inputVariant="outlined"
                        disabled={isLoading}
                        label="Time picker"
                        name="startHour"
                        label="Até"
                        value={dataParaListExecuteds}
                        onChange={(event) => {
                          setDataParaListExecuteds(event);
                        }}
                        style={{ margin: 10, width: 200 }}
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                        keyboardIcon={<TodayIcon />}
                      />
                    </MuiPickersUtilsProvider>

                    <IconButton
                      disabled={isLoading}
                      onClick={() => {
                        getListExecuted();
                      }}
                      style={{ backgroundColor: "red" }}
                      color="inherit"
                    >
                      <SearchIcon style={{ color: "white" }} />
                    </IconButton>
                  </Box>
                  {listExecuteds.length > 0 ? (
                    <ListExecutedResult customers={listExecuteds} />
                  ) : null}
                </TabPanel>
              </div>
            </CardContent>
            <Divider />

            {(value > 0) & (value < 6) ? (
              <Box
                display="flex"
                justifyContent="flex-end"
                alignItems="center"
                className="w-100"
                p={2}
              >
                <Button
                  onClick={onUpdate}
                  disabled={isLoading}
                  style={{
                    borderRadius: 50,
                    backgroundColor: "#10ac84",
                  }}
                  color="primary"
                  variant="contained"
                  size="medium"
                  startIcon={<CheckIcon style={{ color: "white" }} />}
                >
                  Salvar
                </Button>
                {isLoading && <CircularProgress />}
              </Box>
            ) : null}
          </Card>
        </div>
      </PerfectScrollbar>

      <ModalNewVignette
        open={showNewVignette}
        selectId={[idClient]}
        onCloseNewVignette={onCloseNewVignette}
        getAllVinhetas={getAllVignette}
      />
      <ModalNewCommercial
        open={showNewCommercial}
        selectId={[idClient]}
        onCloseNewCommercial={onCloseNewCommercial}
        getAllComerciais={getAllComerciais}
      />
    </form>
  );
};

export default Formulario;
