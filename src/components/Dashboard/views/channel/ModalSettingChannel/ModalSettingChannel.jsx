import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import SreenSettingChannel from "./SreenSettingChannel";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
//import "react-perfect-scrollbar/dist/css/styles.css";


const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalSettingChannel = ({
  open,
  onCloseSettingChannel,
  getAllChannel,
  selectId,
  getSettingChannelModify,
  getInfoClient,
  isMobile
}) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseSettingChannel}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
              <SreenSettingChannel
                onCloseModal={onCloseSettingChannel}
                getAllChannel={getAllChannel}
                idClient={selectId}
                getSettingChannelModify={getSettingChannelModify}
                getInfoClient={getInfoClient}
                isMobile={isMobile}
              />
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalSettingChannel;
