import React from "react";
import { Box, Card, Button } from "@material-ui/core";
import { Delete as DeleteIcon } from "@material-ui/icons";

export default function multSelectVignetteCommercial({ onDelete }) {
  return (
    <div>
      <Card style={{ borderRadius: 20 }} className="p-4 mt-4">
        <Box display="flex" justifyContent="flex-end">
          <Button
            onClick={onDelete}
            startIcon={<DeleteIcon />}
            style={{ borderRadius: 20 }}
            className="mr-2"
            variant="contained"
          >
            Deletar
          </Button>
        </Box>
      </Card>
    </div>
  );
}
