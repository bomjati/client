import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
  Tooltip,
  Hidden,
  Grid,
} from "@material-ui/core";
import { Search as SearchIcon } from "react-feather";
import AddIcon from "@material-ui/icons/AddOutlined";

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({
  onShowNewCommercial,
  SearchCommercial,
  className,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box m={-6} mt={-2}>
        <Card style={{ borderRadius: 20 }}>
          <Hidden smDown>
            <CardContent>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box width="50%">
                  <TextField
                    fullWidth
                    onChange={(event) => {
                      SearchCommercial(event.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SvgIcon fontSize="small" color="action">
                            <SearchIcon />
                          </SvgIcon>
                        </InputAdornment>
                      ),
                    }}
                    placeholder="Procurar conteúdo"
                    variant="outlined"
                  />
                </Box>
                <Tooltip title="Novo Conteúdo" arrow>
                  <Button
                    onClick={() => {
                      onShowNewCommercial();
                    }}
                    color="primary"
                    variant="contained"
                    style={{
                      backgroundColor: "white",
                      color: "#eb2f06",
                    }}
                    size="large"
                    startIcon={<AddIcon />}
                  >
                    NOVO CONTEÚDO
                  </Button>
                </Tooltip>
              </Box>
            </CardContent>
          </Hidden>
          <Hidden mdUp>
            <Grid container spacing={2}>
              <Grid item md={4} xs={12}>
                <Box
                  display="flex"
                  justifyContent="flex-end"
                  alignItems="center"
                >
                  <Tooltip title="Novo Conteúdo" arrow>
                    <Button
                      onClick={() => {
                        onShowNewCommercial();
                      }}
                      color="primary"
                      variant="contained"
                      className="mt-2 mr-2"
                      style={{
                        backgroundColor: "white",
                        color: "#eb2f06",
                      }}
                      size="large"
                      startIcon={<AddIcon />}
                    >
                      NOVO CONTEÚDO
                    </Button>
                  </Tooltip>
                </Box>
              </Grid>
              <Grid item md={8} xs={12}>
                <TextField
                  className="ml-2 mb-2 mr-2"
                  fullWidth
                  onChange={(event) => {
                    SearchCommercial(event.target.value);
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SvgIcon fontSize="small" color="action">
                          <SearchIcon />
                        </SvgIcon>
                      </InputAdornment>
                    ),
                  }}
                  placeholder="Procurar CONTEÚDO"
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </Hidden>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
