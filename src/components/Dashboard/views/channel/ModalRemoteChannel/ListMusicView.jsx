import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
import {
  Grid,
  Container,
  Box,
  Avatar,
  Typography,
  Divider,
  Hidden,
} from "@material-ui/core";
import { PlayArrow as PlayArrowIcon } from "@material-ui/icons";
import "./App.scss";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const ListMusicView = ({
  capa,
  listMusic,
  onPlayMusic,
  idMusicSelect,
  isPlayList,
  isPlayListSub,
}) => {
  useEffect(() => {
    console.log(listMusic);
  }, [listMusic]);

  const scrollListMusic = useRef(null);

  return (
    <div
      style={{
        background: "white",
      }}
    >
      <div style={{ background: "white" }}>
        <Container maxWidth="lg">
          <div>
            <h4 style={{ color: "#1e272e" }} className="ml-4 mb-4">
              Lista de Musicas
            </h4>
            <div style={{ height: 250 }}>
              <PerfectScrollbar>
                {listMusic.map((music, index) => {
                  return (
                    <div key={index} className="w-100">
                      <Box
                        style={{
                          backgroundColor:
                            index == idMusicSelect
                              ? "rgba(245, 59, 87, 0.2)"
                              : "transparent",
                        }}
                        className="p-1"
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                      >
                        <Box
                          className="p-1"
                          display="flex"
                          alignItems="center"
                          justifyContent="flex-start"
                        >
                          {/*  <div
                                  onClick={() => {
                                    //onPlayMusic(index);
                                  }}
                                >
                                  <Avatar
                                    alt={music.title}
                                    variant="square"
                                    className="av-radio mr-2"
                                  />
                                </div> */}
                          <div>
                            <h5 className="text-black text-bold">
                              {String(
                                isPlayList ? music.title : music.title
                              ).replace(".mp3", "")}
                            </h5>
                            <Typography
                              style={{ color: "#1e272e" }}
                              variant="body1"
                            >
                              {isPlayList ? music.artist : music.artist}
                            </Typography>
                          </div>
                        </Box>
                        <Typography
                          style={{ color: "#1e272e" }}
                          variant="body1"
                        >
                          {isPlayList ? music.totalTime : music.totalTime}
                        </Typography>
                      </Box>

                      <div
                        className="w-100"
                        style={{
                          background: "silver",
                          height: 0.09,
                        }}
                      />
                    </div>
                  );
                })}
              </PerfectScrollbar>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
};

export default ListMusicView;
