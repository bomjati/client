import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  Close as CloseIcon,
  SkipNext as SkipNextIcon,
  SkipPrevious as SkipPreviousIcon,
  PlayArrow as PlayArrowIcon,
  Pause as PauseIcon,
  ArrowDropDown as ArrowDropDownIcon,
  ArrowDropUp as ArrowDropUpIcon,
  VolumeDown as VolumeDownIcon,
  VolumeUp as VolumeUpIcon,
  ThumbDownAlt as ThumbDownAltIcon,
  ThumbUp as ThumbUpIcon,
  PowerSettingsNew as PowerSettingsNewIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import ListMusicView from "./ListMusicView";
import socket from "../../../../../services/socket";
import GeneroCard from "./Cards/GerneroCard";
import GeneroSubCard from "./Cards/GerneroSubCard";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const Formulario = ({
  onCloseModal,
  getRemoteChannelId,
  listMusic,
  onBackMusic,
  onNextMusic,
  onPlayMusic,
  onRefreshGuia,
  onCloseGuia,
  onSelectGenre,
  onSelectSubGenre,
}) => {
  let arrayListMusic = {};

  const [value, setValue] = useState(0);
  const [heapList, setHeapList] = useState([]);
  const [typeGroupSelect, setTypeGroupSelect] = useState("genero");
  const [isPlayList, setIsPlayList] = useState(false);
  const [isPlayListSub, setIsPlayListSub] = useState(false);
  const [generoSelect, setGeneroSelect] = useState(null);
  const [numMusicList, setNumMusicList] = useState(null);
  const [playingAuto, setPlayingAuto] = useState(false);

  const colorsIcon = "black";
  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  useEffect(() => {
    if (
      Object.keys(listMusic).filter((key) => {
        console.log(`${key} e ${getRemoteChannelId}`);
        return key == getRemoteChannelId;
      }).length > 0
    ) {
      const data =
        listMusic[
          String(
            Object.keys(listMusic).filter((key) => key == getRemoteChannelId)[0]
          )
        ];

      console.log(data);

      setGeneroSelect(data.musics);
      setIsPlayListSub(data.isPlayListSub);
      setIsPlayList(data.isPlayList);
      setNumMusicList(data.numMusicList);
      setPlayingAuto(data.playingAuto);
      setHeapList(data.heapList);
    }
  }, [listMusic]);

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Informações rápida de seu canal"
            title="Canal Remoto"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                {!generoSelect ? null : (
                  <Tab label="Informações do Canal" {...a11yProps(0)} />
                )}
                <Tab label="Generos" {...a11yProps(!generoSelect ? 0 : 1)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent style={{ minWidth: 300, maxWidth: 700 }}>
            <div>
              {!generoSelect ? null : (
                <TabPanel value={value} index={0} dir={theme.direction}>
                  {!generoSelect ? null : (
                    <ListMusicView
                      listMusic={generoSelect.Musics}
                      idMusicSelect={numMusicList}
                      isPlayList={isPlayList}
                      isPlayListSub={isPlayListSub}
                    />
                  )}
                </TabPanel>
              )}
              <TabPanel
                value={value}
                index={!generoSelect ? 0 : 1}
                dir={theme.direction}
              >
                <div>
                  {heapList.length > 0 &&
                    heapList[0].Rights.slice(0)
                      .reverse()
                      .map((Right) => (
                        <div>
                          <h6 className="text-black text-center mb-4">
                            Lista de musicas {String(Right.name).toLowerCase()}
                          </h6>

                          <div className="">
                            {!(Right.Genresubs.length > 0) ? null : (
                              <h5 className="text-black mb-4">
                                Playlists Sugeridas
                              </h5>
                            )}
                            <PerfectScrollbar>
                              <Box display="flex">
                                {Right.Genresubs.map((generos) => (
                                  <GeneroSubCard
                                    generosList={generos}
                                    onGeneroSelect={onSelectSubGenre}
                                  />
                                ))}
                              </Box>
                            </PerfectScrollbar>

                            {!(Right.Genres.length > 0) ? null : (
                              <h5 className="text-black mb-4">Generos</h5>
                            )}
                            <PerfectScrollbar>
                              <Box display="flex">
                                {Right.Genres.map((generos) => (
                                  <GeneroCard
                                    generosList={generos}
                                    onGeneroSelect={onSelectGenre}
                                  />
                                ))}
                              </Box>
                            </PerfectScrollbar>
                          </div>
                        </div>
                      ))}
                </div>
              </TabPanel>
            </div>
          </CardContent>
          <Divider />

          {(!generoSelect ? 1 > 2 : value == 0) ? (
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              className="w-100"
              p={2}
            >
              <IconButton
                style={styleButtonIcon}
                className="mr-4"
                aria-label="Previous"
                onClick={() => {
                  onBackMusic();
                }}
              >
                <SkipPreviousIcon style={{ color: colorsIcon }} />
              </IconButton>
              <IconButton
                className="mr-4"
                style={styleButtonIcon}
                aria-label="Play-Pause"
                onClick={() => {
                  onPlayMusic();
                }}
              >
                {playingAuto ? (
                  <PauseIcon style={{ fontSize: 40, color: colorsIcon }} />
                ) : (
                  <PlayArrowIcon style={{ fontSize: 40, color: colorsIcon }} />
                )}
              </IconButton>
              <IconButton
                style={styleButtonIcon}
                className="mr-1"
                aria-label="Next"
                onClick={() => {
                  onNextMusic();
                }}
              >
                <SkipNextIcon style={{ color: colorsIcon }} />
              </IconButton>
            </Box>
          ) : (
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              className="w-100"
              p={2}
            >
              <Button
                onClick={() => {
                  onRefreshGuia();
                }}
                variant="contained"
              >
                Atualisar Página
              </Button>
              <IconButton
                style={{ ...styleButtonIcon, backgroundColor: "red" }}
                aria-label="Previous"
                onClick={() => {
                  onCloseGuia();
                }}
              >
                <PowerSettingsNewIcon style={{ color: "white" }} />
              </IconButton>
            </Box>
          )}
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
