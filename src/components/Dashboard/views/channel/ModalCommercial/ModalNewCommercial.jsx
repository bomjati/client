import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import NewCommercial from "./FormNewCommercial";
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalNewCommercial = ({
  open,
  onCloseNewCommercial,
  getAllComerciais,
  selectId,
}) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);

  const isLoadingModal = (value) => {
    setIsLoading(value);
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={!isLoading && onCloseNewCommercial}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
        <PerfectScrollbar>
          <div className="mb-4">
            <NewCommercial
              selectId={selectId}
              onCloseModal={onCloseNewCommercial}
              isLoadingModal={isLoadingModal}
              getAllComerciais={getAllComerciais}
            />
          </div>
        </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewCommercial;
