import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../../../services/api";

const Formulario = ({
  onCloseModal,
  getAllBusiness,
  onDeleteBusiness,
  onDisableFirstAccess,
  getAllChannel,
  clientId,
  onShowConfirmationInfo,
}) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listSegment, setListSegment] = useState(null);
  const [value, setValue] = useState(0);
  const [userId, setUserId] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    registration_number: "",
    business_name: "",
    fantasy_name: "",
    cep: "",
    public_place: "",
    number: "",
    complement: "",
    district: "",
    county: "",
    uf: "",
    email: "",
    phone1: "",
    phone2: "",
    terms_and_conditions: "",
    segmentId: "",
  });

  const colorButtonContent = "red";
  const colorButtonMarginIcon = 20;
  const optionMenu = [
    {
      name: "Pessoa Física",
      icon: (
        <AssignmentIndIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setTypeClient("fisica");
        setValue(value + 1);
      },
    },
    {
      name: "Pessoa Juridica",
      icon: (
        <BusinessCenterIcon
          fontSize="large"
          style={{ color: colorButtonContent, margin: colorButtonMarginIcon }}
        />
      ),
      onClick: () => {
        setTypeClient("juridica");
        setValue(value + 1);
      },
    },
  ];

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (key != "complement") {
        if (key != "phone2") {
          if (
            (values[key] == "") |
            (String(values[key]) == "null") |
            (String(values[key]) == "false")
          ) {
            let campoRecused =
              key == "registration_number"
                ? " CNPJ "
                : key == "business_name"
                ? " Rasão Social "
                : key == "fantasy_name"
                ? " Nome Fantasia "
                : key == "cep"
                ? " CEP "
                : key == "public_place"
                ? " Logradouro "
                : key == "number"
                ? " Número Endereço "
                : key == "district"
                ? " Bairro "
                : key == "county"
                ? " Cidade "
                : key == "uf"
                ? " UF "
                : key == "email"
                ? " E-mail "
                : key == "phone1"
                ? " Telefone 1 "
                : key == "segmentId"
                ? " Segmento "
                : key == "terms_and_conditions"
                ? " Termos e Condições "
                : "Indefinido";

            showMessage(
              "Atenção",
              `Campo ${campoRecused} ${
                key == "terms_and_conditions"
                  ? "não foi aceito"
                  : "não foi preenchido"
              }.`,
              "warning"
            );

            return key == "registration_number"
              ? setValue(0)
              : key == "business_name"
              ? setValue(0)
              : key == "fantasy_name"
              ? setValue(0)
              : key == "cep"
              ? setValue(1)
              : key == "public_place"
              ? setValue(1)
              : key == "number"
              ? setValue(1)
              : key == "district"
              ? setValue(1)
              : key == "county"
              ? setValue(1)
              : key == "uf"
              ? setValue(1)
              : key == "email"
              ? setValue(2)
              : key == "phone1"
              ? setValue(2)
              : key == "segmentId"
              ? setValue(3)
              : key == "terms_and_conditions"
              ? setValue(3)
              : null;
            // return;
          }
        }
      }
    }

    onSave();
  };

  const MascaraParaLabel = (value) => {
    value = String(value).replace(/\D/g, "");

    if (typeClient == "juridica") {
      value = String(value).replace(/^(\d{2})(\d)/, "$1.$2");
      value = String(value).replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
      value = String(value).replace(/\.(\d{3})(\d)/, ".$1/$2");
      value = String(value).replace(/(\d{4})(\d)/, "$1-$2");
    } else {
      value = String(value).replace(/^(\d{3})(\d)/, "$1.$2");
      value = String(value).replace(/^(\d{3})\.(\d{3})(\d)/, "$1.$2.$3");
      value = String(value).replace(/\.(\d{3})(\d)/, ".$1-$2");
    }

    return value;
  };

  const mTel = (value) => {
    value = String(value).replace(/\D/g, "");
    value = String(value).replace(/^(\d{2})(\d)/g, "($1) $2");
    value = String(value).replace(/(\d)(\d{4})$/, "$1-$2");
    return value;
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .post("/business/new", values)
      .then((response) => response.data)
      .then((respB) => {
        if (respB.message == "success") {
          api
            .put(`/user/edit/${userId}`, { businessId: respB.data.id })
            .then((response) => response.data)
            .then((resp) => {
              if (resp.message == "success") {
                api
                  .post(`/plancontrol/new/`, {
                    businessId: respB.data.id,
                    userId: userId,
                    planId: 1,
                    status: "ativo",
                  })
                  .then((response) => response.data)
                  .then((respP) => {
                    if (respP.message == "success") {
                      api
                        .post("/channel/new", {
                          ...{
                            name: values.business_name,
                            slogan: "Ouvido musica boa você fica feliz",
                            terms_and_conditions: "true",
                          },
                          businessId: respB.data.id,
                          segmentId: 0,
                        })
                        .then((response) => response.data)
                        .then((resp) => {
                          if (resp.message == "success") {
                            api
                              .post("/clientsetting/new", {
                                qtdMusic: 1,
                                qtdMusicCommertial: 1,
                                isRuleVignetteComercial: "false",
                                isRuleVignetteDefault: "true",
                                sazonalVignetteId: 0,
                                institucionalVignetteId: 0,
                                clientId: resp.client.id,
                                audioQualityId: 1,
                                colorBackground: "#000000",
                                colorFont: "#ffffff",
                              })
                              .then((response) => response.data)
                              .then((respc) => {
                                if (respc.message == "success") {
                                  api
                                    .post("/clientlocalitie/new", {
                                      ...{
                                        city: values.county,
                                        uf: values.uf,
                                        nameFuso: "America/Sao Paulo",
                                        fuso: "-03:00",
                                      },
                                      clientId: resp.client.id,
                                    })
                                    .then((response) => response.data)
                                    .then((respl) => {
                                      if (respl.message == "success") {
                                        api
                                          .post("/segmentcontrol/new", {
                                            clientId: resp.client.id,
                                            segmentId: values.segmentId,
                                          })
                                          .then((response) => response.data)
                                          .then((respc) => {
                                            if (respc.message == "success") {
                                              api
                                                .post("/previewlist/new", {
                                                  clientId: resp.client.id,
                                                  preview:
                                                    '[{"id":1,"tipo":"Institucional","qtd":1},{"id":2,"tipo":"Vinheta","qtd":0}]',
                                                })
                                                .then(
                                                  (response) => response.data
                                                )
                                                .then((respc) => {
                                                  if (
                                                    respc.message == "success"
                                                  ) {
                                                    showMessage(
                                                      "Novo Canal",
                                                      `Cadastro realizado com sucesso.`,
                                                      "success"
                                                    );
                                                    showMessage(
                                                      "Nova Loja",
                                                      `Cadastro realizado com sucesso.`,
                                                      "success"
                                                    );
                                                    onShowConfirmationInfo();
                                                    getAllBusiness();
                                                    onDisableFirstAccess();
                                                    setIsLoading(false);
                                                    onCloseModal();

                                                    getAllChannel();
                                                    setIsLoading(false);
                                                    onCloseModal();
                                                  } else {
                                                    showMessage(
                                                      "Novo Canal",
                                                      `Falhou.`,
                                                      "error"
                                                    );
                                                  }
                                                })
                                                .catch((error) => {
                                                  console.log(error);
                                                });
                                            } else {
                                              showMessage(
                                                "Novo Canal",
                                                `Falhou.`,
                                                "error"
                                              );
                                            }
                                          })
                                          .catch((error) => {
                                            console.log(error);
                                          });
                                      } else {
                                        showMessage(
                                          "Novo Canal",
                                          `Falhou.`,
                                          "error"
                                        );
                                      }
                                    })
                                    .catch((error) => {
                                      console.log(error);
                                    });
                                } else {
                                  showMessage("Novo Canal", `Falhou.`, "error");
                                }
                              })
                              .catch((error) => {
                                console.log(error);
                              });
                          } else {
                            showMessage("Novo Canal", `Falhou.`, "error");
                          }
                        })
                        .catch((error) => {
                          console.log(error);
                        });
                    } else {
                      showMessage("Nova Loja", `Falhou.`, "error");
                    }
                  })
                  .catch((error) => {
                    console.log(error);
                  });
              } else {
                showMessage("Nova Loja", `Falhou.`, "error");
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } else {
          showMessage("Nova Loja", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onReceitaWs = async () => {
    const res = await apiReceitaWs.get(
      "/" + String(values.registration_number),
      {}
    );

    if (res) {
      setValues({
        ...values,
        business_name: res.data.nome,
        fantasy_name: res.data.fantasia,
        cep: String(res.data.cep).replace(/\D/g, ""),
        public_place: res.data.logradouro,
        number: res.data.numero,
        complement: res.data.complemento,
        district: res.data.bairro,
        county: res.data.municipio,
        uf: res.data.uf,
        email: res.data.email,
        phone1: String(res.data.telefone)
          .replace(" ", "")
          .replace(/\D/g, "")
          .substr(0, 10),
        phone2: "",
      });
      console.log(res.data);
    }
  };

  const onViaCepWs = async () => {
    const res = await apiViaCepWs.get(`${String(values.cep)}/json`, {});

    if (res) {
      setValues({
        ...values,
        cep: String(res.data.cep).replace(/\D/g, ""),
        public_place: res.data.logradouro,
        complement: res.data.complemento,
        district: res.data.bairro,
        county: res.data.localidade,
        uf: res.data.uf,
      });
      console.log(res.data);
    }
  };

  useEffect(() => {
    api
      .get("/segment")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListSegment(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });

    api
      .get("/login/me")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          setUserId(resp.id);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de sua empresa para prosseguir"
            title="Complete seu Cadastro"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                //onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Tipo de Cadastro" {...a11yProps(0)} />
                <Tab
                  label={typeClient == "fisica" ? "Pessoal" : "Empresa"}
                  {...a11yProps(1)}
                />
                <Tab label="Endereço" {...a11yProps(2)} />
                <Tab label="Contato" {...a11yProps(3)} />
                <Tab label="Segmento" {...a11yProps(4)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  {/* <Grid className="mt-4 mb-4" container spacing={1}>
                    <Grid item md={6} xs={6}>
                      <Box display="flex" justifyContent="center">
                        <Button
                          onClick={() => {
                            setTypeClient("fisica");
                            setValue(value + 1);
                          }}
                          variant="contained"
                          color="default"
                          size="large"
                          style={{
                            backgroundColor: "#48dbfb",
                            color: "#303952",
                            height: 150,
                            borderRadius: 15,
                          }}
                          startIcon={<AssignmentIndIcon fontSize="large" />}
                        >
                          Pessoa Física
                        </Button>
                      </Box>
                    </Grid>

                    <Grid item md={6} xs={6}>
                      <Box display="flex" justifyContent="center">
                        <Button
                          onClick={() => {
                            setTypeClient("juridica");
                            setValue(value + 1);
                          }}
                          variant="contained"
                          color="default"
                          style={{
                            backgroundColor: "#48dbfb",
                            color: "#303952",
                            height: 150,
                            borderRadius: 15,
                          }}
                          size="large"
                          startIcon={<BusinessCenterIcon fontSize="large" />}
                        >
                          Pessoa Jurídica
                        </Button>
                      </Box>
                    </Grid>
                  </Grid> */}
                  <CardHeader
                    subheader="Selecione o tipo que se adéque a seu perfil."
                    title="Seleção"
                    style={{
                      marginTop: -20,
                      color: 'black'
                    }}
                  />
                  <Box margin={2} display="flex" flexDirection="column">
                    {optionMenu.map((oMenu) => (
                      <IconButton
                        style={{ borderRadius: 15 }}
                        onClick={() => oMenu.onClick()}
                      >
                        <Card
                          style={{
                            width: "100%",
                            borderRadius: 15,
                            borderWidth: 0.1,
                            borderStyle: "solid",
                            borderColor: "rgba(192, 192, 192, 0.6)",
                          }}
                        >
                          <Box
                            display="flex"
                            justifyContent="space-between"
                            alignItems="center"
                          >
                            <Box display="flex" flexDirection="row">
                              {oMenu.icon}
                              <Divider
                                style={{ height: 80 }}
                                orientation="vertical"
                              />
                            </Box>
                            <h5
                              style={{ color: "#3d3d3d" }}
                              className="text-black text-center ml-2 mr-2"
                            >
                              {oMenu.name}
                            </h5>
                            <div />
                          </Box>
                        </Card>
                      </IconButton>
                    ))}
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={4} xs={12}>
                      <TextField
                        value={MascaraParaLabel(values.registration_number)}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            registration_number: String(event.target.value)
                              .replace(" ", "")
                              .replace(/\D/g, ""),
                          });
                        }}
                        inputProps={{
                          maxLength: typeClient == "fisica" ? 14 : 18,
                        }}
                        onBlur={(event) => {
                          typeClient == "juridica" && onReceitaWs();
                        }}
                        label={
                          typeClient == "fisica" ? "CPF" : "Inscrição CNPJ "
                        }
                        margin="normal"
                        id="registration_number"
                        name="registration_number"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.fantasy_name}
                        onChange={(event) => {
                          typeClient == "fisica"
                            ? setValues({
                                ...values,
                                business_name: event.target.value,
                                fantasy_name: event.target.value,
                              })
                            : handleChange(event);
                        }}
                        label={
                          typeClient == "fisica"
                            ? "Nome Completo"
                            : "Nome Fantasia"
                        }
                        margin="normal"
                        id="fantasy_name"
                        name="fantasy_name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>

                    {typeClient == "fisica" ? null : (
                      <Grid item md={12} xs={12}>
                        <TextField
                          value={values.business_name}
                          onChange={handleChange}
                          label="Nome Empresarial"
                          margin="normal"
                          id="business_name"
                          name="business_name"
                          variant="outlined"
                          className={""}
                          fullWidth
                          required
                        />
                      </Grid>
                    )}
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={3} xs={12}>
                      <TextField
                        value={values.cep}
                        onChange={handleChange}
                        onBlur={() => onViaCepWs()}
                        error={!erros.cep.valido}
                        helperText={erros.cep.texto}
                        label="CEP"
                        margin="normal"
                        id="cep"
                        name="cep"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={7} xs={12}>
                      <TextField
                        value={values.county}
                        onChange={handleChange}
                        label="Município"
                        margin="normal"
                        id="county"
                        name="county"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={2} xs={12}>
                      <FormControl
                        fullWidth
                        variant="outlined"
                        required
                        className="mt-3"
                      >
                        <InputLabel htmlFor="age-native-required">
                          UF
                        </InputLabel>
                        <Select
                          native
                          value={values.uf}
                          name="uf"
                          onChange={handleChange}
                        >
                          {/*Lista de UF*/}
                          <option aria-label="None" value="" />
                          {UFs.map((i) => {
                            return <option value={i.value}>{i.value}</option>;
                          })}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item md={9} xs={12}>
                      <TextField
                        value={values.public_place}
                        onChange={handleChange}
                        label="Logradouro"
                        margin="normal"
                        id="public_place"
                        name="public_place"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <TextField
                        value={values.number}
                        onChange={handleChange}
                        label="Número"
                        margin="normal"
                        id="number"
                        name="number"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>

                    <Grid item md={6} xs={12}>
                      <TextField
                        value={values.district}
                        onChange={handleChange}
                        label="Bairro/Distrito"
                        margin="normal"
                        id="district"
                        name="district"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        value={values.complement}
                        onChange={handleChange}
                        label="Complemento"
                        margin="normal"
                        id="complement"
                        name="complement"
                        variant="outlined"
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={3} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.email}
                        onChange={handleChange}
                        label="E-mail"
                        margin="normal"
                        id="email"
                        name="email"
                        variant="outlined"
                        size="medium"
                        required
                        fullWidth
                      />
                    </Grid>

                    <Grid item md={6} xs={12}>
                      <TextField
                        value={mTel(values.phone1)}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            phone1: String(event.target.value)
                              .replace(" ", "")
                              .replace(/\D/g, ""),
                          });
                        }}
                        inputProps={{
                          maxLength: 16,
                        }}
                        label="Telefone 1"
                        margin="normal"
                        id="phone1"
                        name="phone1"
                        variant="outlined"
                        size="medium"
                        required
                        fullWidth
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        value={mTel(values.phone2)}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            phone2: String(event.target.value)
                              .replace(" ", "")
                              .replace(/\D/g, ""),
                          });
                        }}
                        label="Telefone 2"
                        margin="normal"
                        id="phone2"
                        name="phone2"
                        variant="outlined"
                        size="medium"
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={4} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl
                          requireds
                          variant="outlined"
                          style={{ width: 235 }}
                        >
                          <InputLabel htmlFor="age-native-required">
                            Segmentos
                          </InputLabel>
                          <Select
                            required
                            native
                            name="segmentId"
                            id="segmentId"
                            value={values.segmentId}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listSegment &&
                              listSegment.map((segments) => (
                                <option value={segments.id}>
                                  {segments.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                      <Box display="flex" justifyContent="center">
                        <FormControlLabel
                          label="Termo e Condições"
                          control={
                            <Switch
                              required
                              checked={values.terms_and_conditions}
                              onChange={(event) => {
                                setValues({
                                  ...values,
                                  terms_and_conditions: event.target.checked,
                                });
                              }}
                              name="novidade"
                              color="primary"
                            />
                          }
                        />
                      </Box>
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          {value == 0 ? null : (
            <Box
              display="flex"
              justifyContent="flex-end"
              alignItems="center"
              className="w-100"
              p={2}
            >
              {value < 1 ? (
                <Button
                  onClick={() => {
                    setValue(value + 1);
                  }}
                  disabled={isLoading}
                  style={{
                    borderRadius: 50,
                    backgroundColor: "#3498db",
                  }}
                  color="primary"
                  variant="contained"
                  size="medium"
                  startIcon={<ArrowForwardIcon style={{ color: "white" }} />}
                >
                  Proximo
                </Button>
              ) : value < 4 ? (
                <div className="d-flex">
                  <Button
                    onClick={() => {
                      setValue(value - 1);
                    }}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#f39c12",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    className="mr-2"
                    startIcon={<ArrowBackIcon style={{ color: "white" }} />}
                  >
                    Voltar
                  </Button>
                  <Button
                    onClick={() => {
                      setValue(value + 1);
                    }}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#3498db",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    startIcon={<ArrowForwardIcon style={{ color: "white" }} />}
                  >
                    Proximo
                  </Button>
                </div>
              ) : (
                <div className="d-flex">
                  <Button
                    onClick={() => {
                      setValue(value - 1);
                    }}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#f39c12",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    className="mr-2"
                    startIcon={<ArrowBackIcon style={{ color: "white" }} />}
                  >
                    Voltar
                  </Button>
                  <Button
                    onClick={onUpdate}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#10ac84",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    startIcon={<CheckIcon style={{ color: "white" }} />}
                  >
                    Salvar
                  </Button>
                </div>
              )}
              {isLoading && <CircularProgress />}
            </Box>
          )}
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
