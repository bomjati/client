import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Input,
  MenuItem,
  Chip,
  Typography,
  GridList,
  Avatar,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Cancel as CancelIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../../../services/api";
import momentTimezone from "moment-timezone";
import moment from "moment";
//import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
    marginTop: 10,
  },
  selected: {
    backgroundColor: "rgba(235, 47, 6, 0.3)",
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
    backgroundColor:
      personName.indexOf(name) === -1 ? "transparent" : "rgba(235, 47, 6, 0.3)",
  };
}

const Formulario = ({ onCloseModal, getAllChannel }) => {
  let history = useNavigate();
  const classes = useStyles();
  const themes = useTheme();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listSegment, setListSegment] = useState(null);
  const [personName, setPersonName] = useState([]);
  const [value, setValue] = useState(0);
  const [businessId, setBusinessId] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [listTimeZones, setListTimeZones] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    name: "",
    slogan: "",
    terms_and_conditions: "",
  });

  const [valuesLocal, setValuesLocal] = useState({
    city: "",
    uf: "",
    nameFuso: "America/Sao Paulo",
    fuso: "-03:00",
  });

  useEffect(() => {
    let timeZone = [];
    for (let index = 0; index < momentTimezone.tz.names().length; index++) {
      const element = momentTimezone.tz.names()[index];

      timeZone.push({
        local: String(element).replaceAll("_", " "),
        fuso: moment().tz(element).format("Z"),
      });
    }

    setListTimeZones(timeZone);
  }, []);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeSelect = (event) => {
    if (personName.length <= 4) {
      onSelectSegment(event.target.value);
    } else {
      showMessage(
        "Limite Seleção",
        "Você atigiu o limite de 5 segmentos selecionados para um canal",
        "warning"
      );
    }
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "name"
            ? " Nome do Canal "
            : key == "terms_and_conditions"
            ? " Termos e Política de Privacidade "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        setValue(0);
        return;
      }
    }

    for (var key in valuesLocal) {
      if (
        (valuesLocal[key] == "") |
        (String(valuesLocal[key]) == "null") |
        (String(valuesLocal[key]) == "false")
      ) {
        let campoRecused =
          key == "city"
            ? " Municipio "
            : key == "uf"
            ? " UF "
            : key == "nameFuso"
            ? " Fuso Horário "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        setValue(1);
        return;
      }
    }

    if (personName.length == 0) {
      showMessage("Atenção", `Selecione ao menos um segmento.`, "warning");
      return;
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .post("/channel/new", { ...values, businessId: businessId, segmentId: 0 })
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          api
            .post("/clientsetting/new", {
              qtdMusic: 1,
              qtdMusicCommertial: 1,
              isRuleVignetteComercial: "false",
              isRuleVignetteDefault: "true",
              sazonalVignetteId: 0,
              institucionalVignetteId: 0,
              clientId: resp.client.id,
              audioQualityId: 1,
              colorBackground: "#000000",
              colorFont: "#ffffff",
            })
            .then((response) => response.data)
            .then((respc) => {
              if (respc.message == "success") {
                api
                  .post("/clientlocalitie/new", {
                    ...valuesLocal,
                    clientId: resp.client.id,
                  })
                  .then((response) => response.data)
                  .then((respl) => {
                    if (respl.message == "success") {
                      for (let index = 0; index < personName.length; index++) {
                        const element = personName[index];

                        api
                          .post("/segmentcontrol/new", {
                            clientId: resp.client.id,
                            segmentId: element,
                          })
                          .then((response) => response.data)
                          .then((respc) => {
                            if (respc.message == "success") {
                              if (index + 1 == personName.length) {
                                api
                                  .post("/previewlist/new", {
                                    clientId: resp.client.id,
                                    preview:
                                      '[{"id":1,"tipo":"Institucional","qtd":1},{"id":2,"tipo":"Vinheta","qtd":0}]',
                                  })
                                  .then((response) => response.data)
                                  .then((respc) => {
                                    if (respc.message == "success") {
                                      showMessage(
                                        "Novo Canal",
                                        `Cadastro realizado com sucesso.`,
                                        "success"
                                      );

                                      getAllChannel();
                                      setIsLoading(false);
                                      onCloseModal();
                                    } else {
                                      showMessage(
                                        "Novo Canal",
                                        `Falhou.`,
                                        "error"
                                      );
                                    }
                                  })
                                  .catch((error) => {
                                    console.log(error);
                                  });
                              }
                            } else {
                              showMessage("Novo Canal", `Falhou.`, "error");
                            }
                          })
                          .catch((error) => {
                            console.log(error);
                          });
                      }
                    } else {
                      showMessage("Novo Canal", `Falhou.`, "error");
                    }
                  })
                  .catch((error) => {
                    console.log(error);
                  });
              } else {
                showMessage("Novo Canal", `Falhou.`, "error");
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } else {
          showMessage("Novo Canal", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    api
      .get("/segment/content")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListSegment(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });

    api
      .get("/login/me")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          setBusinessId(resp.Business.id);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const onSelectSegment = async (id) => {
    console.log(personName.length)
    let arraySegment = [];

    for (let index = 0; index < listSegment.length; index++) {
      const element = listSegment[index];

      if (element.id == id[personName.length]) {
        const resCovers = await api.get(`/cover/segment/${id[personName.length]}`);
        const resContent = await api.get(`/content/segment/${id[personName.length]}`);

        arraySegment.push({
          ...element,
          Covers: resCovers.data,
          Contents: resContent.data,
        });

        console.log({
          ...element,
          Covers: resCovers.data,
          Contents: resContent.data,
        })
      } else {
        arraySegment.push({ ...element });
      }
    }

    setListSegment(arraySegment);

    console.log(arraySegment);

    setPersonName(id);
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu canal"
            title="Novo Canal"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do Canal" {...a11yProps(0)} />
                <Tab label="Localidade do Canal" {...a11yProps(1)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent style={{ minWidth: 300, maxWidth: 700 }}>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.name}
                        onChange={handleChange}
                        label="Nome do Canal"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.slogan}
                        onChange={handleChange}
                        label="Slogan Empresarial"
                        margin="normal"
                        id="name"
                        name="slogan"
                        variant="outlined"
                        className={""}
                        style={{ marginTop: 0 }}
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel>Segmento</InputLabel>
                          <Select
                            required
                            multiple
                            name="segmentId"
                            id="segmentId"
                            //input={<Input id="select-multiple-chip" />}
                            value={personName}
                            onChange={handleChangeSelect}
                            renderValue={(selected) => (
                              <div className={classes.chips}>
                                {selected.map((value) => (
                                  <Chip
                                    key={value}
                                    label={
                                      listSegment.filter(
                                        (segment) => segment.id == value
                                      )[0].name
                                    }
                                    onMouseDown={(event) => {
                                      event.stopPropagation();
                                    }}
                                    className={classes.chip}
                                    style={{
                                      backgroundColor: "rgba(235, 47, 6, 0.9)",
                                      color: "white",
                                    }}
                                    deleteIcon={
                                      <CancelIcon
                                        style={{
                                          color: "rgba(255, 255, 255, 0.7)",
                                        }}
                                      />
                                    }
                                    onDelete={() => {
                                      setPersonName(
                                        personName.filter((id) => id !== value)
                                      );
                                    }}
                                  />
                                ))}
                              </div>
                            )}
                            MenuProps={MenuProps}
                          >
                            {listSegment &&
                              listSegment.map((segment) => (
                                <MenuItem
                                  key={segment.id}
                                  value={segment.id}
                                  classes={{
                                    selected: {
                                      fontcolor: "rgba(235, 47, 6, 0.9)",
                                    },
                                  }}
                                  style={{
                                    ...getStyles(
                                      segment.id,
                                      personName,
                                      themes
                                    ),
                                  }}
                                >
                                  {segment.name}
                                </MenuItem>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>

                      {personName.length > 0 ? (
                        <Card className="p-4 mt-4">
                          <Typography
                            className="text-center mb-4"
                            variant="body1"
                          >
                            Veja abaixo os conteúdos de cada segmento que
                            selecionou
                          </Typography>
                          {personName.map((seg) => (
                            <div>
                              <h5>
                                {
                                  listSegment.filter((ls) => ls.id == seg)[0]
                                    .name
                                }
                              </h5>
                              <PerfectScrollbar className="mb-4">
                                <Box display="flex">
                                  {listSegment
                                    .filter((ls) => ls.id == seg)[0]
                                    .Contents.map((c) => {
                                      return (
                                        c && (
                                          <div>
                                            <Avatar
                                              className="mb-2 mt-4 mr-4"
                                              style={{
                                                height: 100,
                                                width: 200,
                                              }}
                                              variant="rounded"
                                              src={
                                                listSegment.filter(
                                                  (ls) => ls.id == seg
                                                )[0].Covers[
                                                  Math.floor(
                                                    Math.random() *
                                                      listSegment.filter(
                                                        (ls) => ls.id == seg
                                                      )[0].Covers.length
                                                  )
                                                ].path_s3
                                              }
                                            />
                                            <audio
                                              controls
                                              controlsList="nodownload"
                                              className="mb-3"
                                              style={{ width: 200 }}
                                              src={c.path_s3}
                                            />
                                          </div>
                                        )
                                      );
                                    })}
                                </Box>
                              </PerfectScrollbar>

                              {listSegment.filter((ls) => ls.id == seg)[0]
                                .Contents.length == 0 ? (
                                <div>
                                  <p
                                    style={{
                                      color: "rgba(0,0,0,0.5)",
                                      fontWeight: "bold",
                                    }}
                                    className="text-black text-center text-bold"
                                  >
                                    Nenhum conteúdo
                                  </p>
                                  <p
                                    style={{
                                      color: "rgba(0,0,0,0.5)",
                                      fontWeight: "bold",
                                      marginTop: -4,
                                    }}
                                    className="text-black text-center text-bold"
                                  >
                                    disponível
                                  </p>
                                  <p
                                    style={{ color: "rgba(0,0,0,0.5)" }}
                                    className="text-black text-center"
                                  >
                                    Já estamos trabalhando nisso,
                                  </p>
                                  <p
                                    style={{
                                      marginTop: -8,
                                      color: "rgba(0,0,0,0.5)",
                                    }}
                                    className="text-black text-center"
                                  >
                                    em breve o disponibilizaremos.
                                  </p>
                                </div>
                              ) : null}
                            </div>
                          ))}
                        </Card>
                      ) : null}

                      <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        className="mt-4"
                      >
                        <Switch
                          required
                          checked={values.terms_and_conditions}
                          onChange={(event) => {
                            setValues({
                              ...values,
                              terms_and_conditions: event.target.checked,
                            });
                          }}
                          name="novidade"
                          color="primary"
                        />
                        <span className="d-inline-block">
                          Ao ativar, você aceita nossos{" "}
                          <a style={{ fontWeight: "bold" }} href="/#">
                            Termos de uso
                          </a>{" "}
                          &amp;{" "}
                          <a style={{ fontWeight: "bold" }} href="/#">
                            Política de Privacidade
                          </a>
                        </span>
                      </Box>
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={6} xs={12}>
                      <TextField
                        value={valuesLocal.city}
                        onChange={(event) => {
                          setValuesLocal({
                            ...valuesLocal,
                            city: event.target.value,
                          });
                        }}
                        label="Municipio"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Box display="flex" justifyContent="center" mt={2}>
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel>UF</InputLabel>
                          <Select
                            required
                            native
                            name="segmentId"
                            id="segmentId"
                            value={valuesLocal.uf}
                            onChange={(event) => {
                              setValuesLocal({
                                ...valuesLocal,
                                uf: event.target.value,
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {UFs &&
                              UFs.map((uf) => (
                                <option value={uf.value}>{uf.value}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel>Fuso Horário</InputLabel>
                          <Select
                            required
                            native
                            name="segmentId"
                            id="segmentId"
                            value={`${valuesLocal.nameFuso}|${valuesLocal.fuso}`}
                            onChange={(event) => {
                              setValuesLocal({
                                ...valuesLocal,
                                nameFuso: event.target.value.split("|")[0],
                                fuso: event.target.value.split("|")[1],
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listTimeZones &&
                              listTimeZones.map((timeZone) =>
                                (String(timeZone.local).indexOf(
                                  "America/Sao Paulo"
                                ) >
                                  -1) |
                                (String(timeZone.local).indexOf(
                                  "America/Noronha"
                                ) >
                                  -1) |
                                (String(timeZone.local).indexOf(
                                  "America/Manaus"
                                ) >
                                  -1) |
                                (String(timeZone.local).indexOf("Porto Acre") >
                                  -1) ? (
                                  <option
                                    value={`${timeZone.local}|${timeZone.fuso}`}
                                  >
                                    {`${timeZone.local} ${timeZone.fuso}`}
                                  </option>
                                ) : null
                              )}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            {value == 1 ? (
              <Button
                onClick={() => setValue(0)}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#f39c12",
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<ArrowBackIcon style={{ color: "white" }} />}
              >
                Voltar
              </Button>
            ) : null}
            {value == 0 ? (
              <Button
                onClick={() => setValue(1)}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#3498db",
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<ArrowForwardIcon style={{ color: "white" }} />}
              >
                Próximo
              </Button>
            ) : (
              <Button
                onClick={onUpdate}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#10ac84",
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<CheckIcon style={{ color: "white" }} />}
              >
                Salvar
              </Button>
            )}
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
