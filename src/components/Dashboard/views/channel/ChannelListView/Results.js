import React, { useState, useEffect, Fragment } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import { showMessage } from "../../../utils/message";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Tooltip,
  Backdrop,
} from "@material-ui/core";
import { SpeedDial, SpeedDialIcon, SpeedDialAction } from "@material-ui/lab";
import getInitials from "../../../utils/getInitials";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  PlayCircleOutline as PlayCircleOutlineIcon,
  Settings as SettingsIcon,
  FiberManualRecord as FiberManualRecordIcon,
  SettingsRemote as SettingsRemoteIcon,
  ArtTrack as ArtTrackIcon,
  FileCopy as FileCopyIcon,
  Save as SaveIcon,
  Print as PrintIcon,
  Share as ShareIcon,
  Favorite as FavoriteIcon,
  FilterNone as FilterNoneIcon,
  CloudOff as CloundOffIcon
} from "@material-ui/icons";
import { useCookies } from "react-cookie";
import { red } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const useStylesList = makeStyles((theme) => ({
  root: {
    transform: "translateZ(0px)",
    flexGrow: 1,
  },
  exampleWrapper: {
    position: "relative",
  },
  radioGroup: {
    margin: theme.spacing(1, 0),
  },
  speedDial: {
    position: "absolute",
    "&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft": {
      bottom: theme.spacing(-3.5),
      right: theme.spacing(2),
    },
    "&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight": {
      top: theme.spacing(-1.5),
      left: theme.spacing(-1),
    },
  },
}));

function copyTextToClipboard(text) {
  var textArea = document.createElement("textarea");

  //
  // *** This styling is an extra step which is likely not required. ***
  //
  // Why is it here? To ensure:
  // 1. the element is able to have focus and selection.
  // 2. if the element was to flash render it has minimal visual impact.
  // 3. less flakyness with selection and copying which **might** occur if
  //    the textarea element is not visible.
  //
  // The likelihood is the element won't even render, not even a
  // flash, so some of these are just precautions. However in
  // Internet Explorer the element is visible whilst the popup
  // box asking the user for permission for the web page to
  // copy to the clipboard.
  //

  // Place in the top-left corner of screen regardless of scroll position.
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;

  // Ensure it has a small width and height. Setting to 1px / 1em
  // doesn't work as this gives a negative w/h on some browsers.
  textArea.style.width = '2em';
  textArea.style.height = '2em';

  // We don't need padding, reducing the size if it does flash render.
  textArea.style.padding = 0;

  // Clean up any borders.
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';

  // Avoid flash of the white box if rendered for any reason.
  textArea.style.background = 'transparent';


  textArea.value = text;

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }

  document.body.removeChild(textArea);
}

const Results = ({
  onShowListContent,
  onDisconnectChannel,
  onDeleteChannel,
  onShowModalEditChannel,
  onShowModalSettingChannel,
  onShowRemoteChannel,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  onSelectId,
  levelAccess,
  channelOnline,
  channelControls,
  isAdministrador,
  ...rest
}) => {
  const classes = useStyles();
  const classesL = useStylesList();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [direction, setDirection] = useState("left");
  const [open, setOpen] = useState([]);
  const [hidden, setHidden] = useState(false);

  const actions = [
    {
      icon: <ArtTrackIcon style={{ color: "#1e3799" }} />,
      name: "Lista de Conteúdo",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 1) | isAdministrador.status) {
          onShowListContent(customer);
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
    {
      icon: <SettingsRemoteIcon style={{ color: "#3d3d3d" }} />,
      name: "Canal Remoto",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 1) | isAdministrador.status) {
          if (isOnline) {
            onShowRemoteChannel(customer.Client.id);
          } else {
            showMessage(
              "Canal Fechado",
              "Seu canal esta fechado, acesse seu canal para ter acesso a essa funcionalidade.",
              "warning"
            );
          }
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
    {
      icon: <SettingsIcon />,
      name: "Configurações",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 2) | isAdministrador.status) {
          onShowModalSettingChannel(
            customer.Client.id,
            //customer.Client.Clientsettings[0],
            customer.Client
          );
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
    {
      icon: <CloundOffIcon />,
      name: "Desconectar",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 1) | isAdministrador.status) {
          onDisconnectChannel(
            customer.Client.id,
          );
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
    {
      icon: <PlayCircleOutlineIcon style={{ color: "#20bf6b" }} />,
      name: "Play Canal",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 0) | isAdministrador.status) {
          if (!isOnline) {
            const url = `${window.location.origin}/bomja-music?profile=${customer.Client.keyaccess}`;
            const win = window.open(url, "_blank");
            win.focus();
          } else {
            showMessage(
              "Canal Aberto",
              "Esse canal já está aberto desconecte-o e tente novamente.",
              "danger"
            );
          }
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
    {
      icon: <FilterNoneIcon style={{ color: "#FC427B" }} />,
      name: "Link Canal",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 0) | isAdministrador.status) {
            const url = `${window.location.origin}/bomja-music?profile=${customer.Client.keyaccess}`;
            copyTextToClipboard(url)
          
            showMessage(
              "Link Copiado",
              "Link de seu canal foi copiado com sucesso agora só colar no local desejado.",
              "success"
            );
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
    {
      icon: <DeleteIcon style={{ color: "#ff4d4d" }} />,
      name: "Deletar Canal",
      onClick: (customer, isOnline) => {
        handleClose(customer.clientId);
        if ((levelAccess > 3) | isAdministrador.status) {
          onDeleteChannel(customer.id);
        } else {
          showMessage(
            "Acesso Negado",
            "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
            "danger"
          );
        }
      },
    },
  ];

  const handleClose = (id) => {
    setOpen(open.filter((o) => o != id));
  };

  const handleOpen = (id) => {
    let data = [];
    data.push(id);
    setOpen(data);
  };

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.clientId);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  const formatTel = (str) => {
    return String(str).replace(/\D/g, "").length == 11
      ? String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d)(\d{4})(\d{4})$/, "($1) $2 $3-$4")
      : String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d{4})(\d{4})$/, "($1) $2-$3");
  };

  useEffect(() => {
    console.log(selectedCustomerIds);
    onSelectId(selectedCustomerIds);
  }, [selectedCustomerIds]);

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={750}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>

                {!(selectedCustomerIds.length > 0) ? (
                  <Hidden lgUp>
                    <TableCell style={{ width: 50 }} align="right" />
                  </Hidden>
                ) : null}

                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                {!(selectedCustomerIds.length > 0) ? (
                  <Hidden smDown>
                    <TableCell align="right" />
                  </Hidden>
                ) : null}
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => {
                    let isOnline = channelOnline[String(customer.clientId)]
                      ? channelOnline[String(customer.clientId)].status ==
                        "online"
                        ? true
                        : false
                      : false;

                    return !(
                      channelControls.filter(
                        (c) => c.clientId == customer.clientId
                      ).length > 0
                    ) & !isAdministrador.status ? null : (
                      <Fragment>
                        <TableRow
                          hover
                          key={customer.clientId}
                          selected={
                            selectedCustomerIds.indexOf(customer.clientId) !==
                            -1
                          }
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              checked={
                                selectedCustomerIds.indexOf(
                                  customer.clientId
                                ) !== -1
                              }
                              onChange={(event) =>
                                handleSelectOne(event, customer.clientId)
                              }
                              value="true"
                            />
                          </TableCell>
                          <Hidden lgUp>
                            {!(selectedCustomerIds.length > 0) ? (
                              <TableCell align="left">
                                <div
                                  style={{
                                    backgroundColor: "white",
                                    height: 35,
                                  }}
                                  className={classesL.exampleWrapper}
                                >
                                  <SpeedDial
                                    style={{
                                      color: "red",
                                      backgroundColor:
                                        open.filter(
                                          (o) => o == customer.clientId
                                        ).length > 0
                                          ? "#F5F4F4"
                                          : "transparent",
                                    }}
                                    ariaLabel="SpeedDial example"
                                    className={classesL.speedDial}
                                    hidden={hidden}
                                    FabProps={{
                                      style: {
                                        backgroundColor: "#e74c3c",
                                        height: 40,
                                        width: 40,
                                      },
                                    }}
                                    icon={
                                      <SpeedDialIcon
                                        icon={
                                          <SettingsIcon
                                            style={{ color: "white" }}
                                          />
                                        }
                                        openIcon={
                                          <EditIcon
                                            style={{ color: "white" }}
                                          />
                                        }
                                      />
                                    }
                                    onClose={() => {
                                      handleClose(customer.clientId);
                                    }}
                                    onOpen={() => {
                                      handleOpen(customer.clientId);
                                    }}
                                    open={
                                      open.filter((o) => o == customer.clientId)
                                        .length > 0
                                    }
                                    direction={"right"}
                                  >
                                    {actions.map((action) => (
                                      <SpeedDialAction
                                        key={action.name}
                                        icon={action.icon}
                                        tooltipTitle={action.name}
                                        onClick={() => {
                                          action.onClick(customer, isOnline);
                                        }}
                                      />
                                    ))}
                                  </SpeedDial>
                                </div>
                              </TableCell>
                            ) : null}
                          </Hidden>
                          <TableCell align="left">
                            <Box alignItems="center" display="flex">
                              <Avatar
                                className={classes.avatar}
                                /* src={customer.avatarUrl} */
                                src={customer.avatarUrl}
                              >
                                {getInitials(customer.Client.name)}
                              </Avatar>

                              <FiberManualRecordIcon
                                fontSize="small"
                                style={{
                                  color: isOnline ? "#2ecc71" : "#b33939",
                                }}
                                className="mr-2"
                              />

                              <Typography color="textPrimary" variant="body1">
                                {formatStringName(customer.Client.name)}
                              </Typography>
                            </Box>
                          </TableCell>
                          <TableCell align="left">
                            <Tooltip
                              title={customer.Client.Segmentcontrols.map(
                                (Segmentcontrol, index) =>
                                  index + 1 ==
                                  customer.Client.Segmentcontrols.length
                                    ? `${Segmentcontrol.Segment.name}`
                                    : `${Segmentcontrol.Segment.name}, `
                              )}
                              arrow
                            >
                              <Typography color="textPrimary" variant="body1">
                                {customer.Client.Segmentcontrols.length > 0
                                  ? customer.Client.Segmentcontrols[0].Segment
                                      .name
                                  : ""}
                                {customer.Client.Segmentcontrols.length > 1
                                  ? "..."
                                  : ""}
                              </Typography>
                            </Tooltip>
                          </TableCell>
                          <TableCell align="left">
                            {customer.Business.business_name}
                          </TableCell>
                          {!(selectedCustomerIds.length > 0) ? (
                            <Hidden smDown>
                              <TableCell align="right">
                                <div
                                  style={{ backgroundColor: "white" }}
                                  className={classesL.exampleWrapper}
                                >
                                  <SpeedDial
                                    style={{
                                      color: "red",
                                      backgroundColor:
                                        open.filter(
                                          (o) => o == customer.clientId
                                        ).length > 0
                                          ? "#F5F4F4"
                                          : "transparent",
                                    }}
                                    ariaLabel="SpeedDial example"
                                    className={classesL.speedDial}
                                    hidden={hidden}
                                    FabProps={{
                                      style: {
                                        backgroundColor: "#e74c3c",
                                      },
                                    }}
                                    icon={
                                      <SpeedDialIcon
                                        icon={<SettingsIcon />}
                                        openIcon={<EditIcon />}
                                      />
                                    }
                                    onClose={() => {
                                      handleClose(customer.clientId);
                                    }}
                                    onOpen={() => {
                                      handleOpen(customer.clientId);
                                    }}
                                    open={
                                      open.filter((o) => o == customer.clientId)
                                        .length > 0
                                    }
                                    direction={direction}
                                  >
                                    {actions.map((action) => (
                                      <SpeedDialAction
                                        key={action.name}
                                        icon={action.icon}
                                        tooltipTitle={action.name}
                                        onClick={() => {
                                          action.onClick(customer, isOnline);
                                        }}
                                      />
                                    ))}
                                  </SpeedDial>
                                </div>
                              </TableCell>
                            </Hidden>
                          ) : null}
                        </TableRow>
                        {/*  <Hidden lgUp>
                          <TableRow style={{ height: 0 }}>
                            <TableCell style={{ height: "auto !important" }} />
                            {!(selectedCustomerIds.length > 0) ? (
                              <TableCell align="left">
                                <div
                                  style={{ backgroundColor: "white", height: 35 }}
                                  className={classesL.exampleWrapper}
                                >
                                  <SpeedDial
                                    style={{
                                      color: "red",
                                      backgroundColor:
                                        open.filter(
                                          (o) => o == customer.clientId
                                        ).length > 0
                                          ? "white"
                                          : "transparent",
                                    }}
                                    ariaLabel="SpeedDial example"
                                    className={classesL.speedDial}
                                    hidden={hidden}
                                    FabProps={{
                                      style: {
                                        backgroundColor: "#e74c3c",

                                      },
                                    }}
                                    icon={
                                      <SpeedDialIcon
                                        icon={<SettingsIcon />}
                                        openIcon={<EditIcon />}
                                      />
                                    }
                                    onClose={() => {
                                      handleClose(customer.clientId);
                                    }}
                                    onOpen={() => {
                                      handleOpen(customer.clientId);
                                    }}
                                    open={
                                      open.filter((o) => o == customer.clientId)
                                        .length > 0
                                    }
                                    direction={"right"}
                                  >
                                    {actions.map((action) => (
                                      <SpeedDialAction
                                        key={action.name}
                                        icon={action.icon}
                                        tooltipTitle={action.name}
                                        onClick={() => {
                                          action.onClick(customer, isOnline);
                                        }}
                                      />
                                    ))}
                                  </SpeedDial>
                                </div>
                              </TableCell>
                            ) : null}
                            <TableCell />
                            <TableCell />
                          </TableRow>
                        </Hidden> */}
                      </Fragment>
                    );
                  })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
