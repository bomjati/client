import React, { useState, useEffect } from "react";
import {
  Box,
  Container,
  makeStyles,
  LinearProgress,
  CircularProgress,
  Backdrop,
  Divider,
} from "@material-ui/core";
import { useLocation } from "react-router-dom";
import { createMuiTheme } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewChannel from "../ModalNewChannel";
import ModalContentChannel from "../ModalContentChannel";
import ModalRemoteChannel from "../ModalRemoteChannel";
import ModalEditChannel from "../ModalEdit";
import ModalNewVignette from "../ModalVignette";
import ModalNewCommercial from "../ModalCommercial";
import ModalNewBusiness from "../ModalNewBusiness";
import BottomMultSelect from "./multSelectVignetteCommercial";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";
import ModalSettingChannel from "../ModalSettingChannel";
import ModalConfirmationInfo from "../ModalConfirmationInfo";
import ModalEditSettingChannel from "../ModalEditSettingChannel";
import Employee from "../../employee/EmployeeListView";
import Access from "../../access/AccessListView";
import Dashboard from "../../../DashboardView";
import RequestContent from "../../requestContent/RequestContentListView";
import OptionChannel from "./OptionChannel";
import { TabPanel, a11yProps } from "../help";
import { useCookies } from "react-cookie";
import socket from "../../../../../services/socket";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

function getParam(param) {
  return String(
    new URLSearchParams(window.location.search).get(param)
  ).replaceAll(" ", "+");
}

function isMobileDevice() {
  return (
    typeof window.orientation !== "undefined" ||
    navigator.userAgent.indexOf("IEMobile") !== -1
  );
}

const CompanyListView = () => {
  const classes = useStyles();
  let arrayChannelOnline = {};
  let arrayListMusic = {};
  let arrayListHeap = {};

  const location = useLocation();
  const [showConfirmationInfo, setShowConfirmationInfo] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showNewChannel, setShowNewChannel] = useState(false);
  const [showContentChannel, setShowContentChannel] = useState(false);
  const [showRemoteChannel, setShowRemoteChannel] = useState(false);
  const [showNewVignette, setShowNewVignette] = useState(false);
  const [showNewCommercial, setShowNewCommercial] = useState(false);
  const [showSetting, setShowSetting] = useState(false);
  const [showEditChannel, setShowEditChannel] = useState(false);
  const [showEditSetting, setShowEditSetting] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [Channel, setChannel] = useState([]);
  const [ChannelBusca, setChannelBusca] = useState([]);
  const [getEditChannel, setGetEditChannel] = useState([]);
  const [selectId, setSelectId] = useState([]);
  const [idChannelModify, setIdChannelModify] = useState(0);
  const [pageIndex, setPageIndex] = useState(0);
  const [idClient, setIdClient] = useState(0);
  const [idBusiness, setIdBusiness] = useState(0);
  const [levelAccessChannel, setLevelAccessChannel] = useState(0);
  const [levelAccess, setLevelAccess] = useState(0);
  const [levelAccessEmployee, setLevelAccessEmployee] = useState(0);
  const [accessControls, setAccessControls] = useCookies(["AccessControls"]);
  const [isManager, setIsManager] = useCookies(["IsManager"]);
  const [isAdministrador, setIsAdministrador] = useState({});
  const [getSettingChannelModify, setGetSettingChannelModify] = useState(null);
  const [getInfoClient, setGetInfoClient] = useState(null);
  const [getRemoteChannelId, setGetRemoteChannelId] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);
  const [channelOnlines, setChannelOnlines] = useState({});
  const [channelOnlinesClient, setChannelOnlinesClient] = useState({});
  const [listMusic, setListMusic] = useState({});
  const [listHeap, setListHeap] = useState({});
  const [channelControls, setChannelControls] = useState([]);
  const [segmentControls, setSegmentControls] = useState([]);
  const [contentSelectChannel, setContentSelectChannel] = useState(null);
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const [isMobile, setIsMobile] = useState(false);
  let count = 0;

  useEffect(() => {
    if (isMobileDevice()) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  }, []);

  useEffect(() => {
    setPageIndex(
      getParam("page") == "home"
        ? 0
        : getParam("page") == "access"
          ? 1
          : getParam("page") == "employee"
            ? 2
            : getParam("page") == "request"
              ? 3
              : getParam("page") == "dashboard"
                ? 4
                : 0
    );
  }, [location.pathname]);

  useEffect(() => {
    //alert()
    if (count == 0) {
    count = count + 1;
       socket.on("connect", () => console.log("Bem Vindo"));

    socket.on("previousChannelOnline", (onlines) => {
      setChannelOnlines(onlines);
      arrayChannelOnline = { ...onlines }
      console.log(onlines);
    });

    /* socket.on("previouslistheap", (list) => {
      setListHeap(list);
      console.log(list);
    }); */

    socket.on("previousListMusic", (list) => {
      setListMusic(list);
      console.log(list);
    });

    /* socket.on("receivedheap", (onlines) => {
      //console.log(arrayChannelOnline);
      for (const key in onlines) {
        if (Object.hasOwnProperty.call(onlines, key)) {
          const element = onlines[key];

          arrayListMusic = { ...arrayListMusic, [key]: element };
        }
      }

      setListMusic(arrayListMusic);
      console.log(arrayListMusic);
    }); */

    /* socket.on("receivedListMusic", (onlines) => {
      //console.log(arrayChannelOnline);
      for (const key in onlines) {
        if (Object.hasOwnProperty.call(onlines, key)) {
          const element = onlines[key];

          arrayListMusic = { ...arrayListMusic, [key]: element };
        }
      }

      //setListMusic(arrayListMusic);
      console.log(arrayListMusic);
    }); */

    socket.on("receivedChannelOnline", (onlines) => {
      if (
        !(
          JSON.stringify(arrayChannelOnline) ==
          JSON.stringify({ ...arrayChannelOnline, ...onlines })
        )
      ) {
        arrayChannelOnline = { ...arrayChannelOnline, ...onlines };
        //console.log(arrayChannelOnline);

        setChannelOnlines(arrayChannelOnline);
      }
    });
    }
  }, []);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  useEffect(() => { }, [channelOnlines]);

  useEffect(() => {
    if (isManager) {
      setIsAdministrador(isManager.IsManager);
    }
  }, [isManager]);

  useEffect(() => {
    if (pageIndex == 0) {
      getMe();
      getSegmentControl();
      getBusiness();
    }
  }, []);

  useEffect(() => {
    if (segmentControls.length > 0) {
      getChannel();
    }
  }, [segmentControls]);

  useEffect(() => {
    if (accessControls) {
      console.log(accessControls.AccessControls);

      setLevelAccessChannel(
        accessControls.AccessControls.filter(
          (a) => a.Access.name == "Cadastro de Canal"
        )[0].level
      );

      setLevelAccess(
        accessControls.AccessControls.filter(
          (a) => a.Access.name == "Cadastro de Acessos"
        ).length > 0
          ? accessControls.AccessControls.filter(
            (a) => a.Access.name == "Cadastro de Acessos"
          )[0].level
          : 0
      );

      setLevelAccessEmployee(
        accessControls.AccessControls.filter(
          (a) => a.Access.name == "Cadastro de Funcionarios"
        ).length > 0
          ? accessControls.AccessControls.filter(
            (a) => a.Access.name == "Cadastro de Funcionarios"
          )[0].level
          : 0
      );
    }
  }, [accessControls]);

  useEffect(() => {
    if (firstAccess) {
      setShowNewBusiness(true);
    } else {
      setShowNewBusiness(false);
    }
  }, [firstAccess]);

  const getMe = () => {
    setIsLoading(true);
    api
      .get("/login/me")
      .then((response) => response.data)
      .then((resp) => {
        console.log("teste");
        console.log(resp);
        setIdClient(resp.id)
        setIdBusiness(resp.Business && resp.Business.id);
        setChannelControls(resp.Channelcontrols);
      });
  };

  const buscaChannel = (value) => {
    const resultBusca = ChannelBusca.filter((i) => {
      return (
        String(i.Client["name"])
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    setChannel(resultBusca);
  };

  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdChannelModify(id);
  };

  const onShowConfirmationInfo = () => {
    setShowConfirmationInfo(true);
  };

  const onShowEditSetting = (id, data) => {
    setShowEditSetting(true);
    setGetSettingChannelModify(data);
    setIdClient(id);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onCloseEditSetting = () => {
    setShowEditSetting(false);
  };

  const onShowNewChannel = () => {
    setShowNewChannel(true);
  };

  const onCloseNewChannel = () => {
    setShowNewChannel(false);
  };

  const onShowContentChannel = async (values) => {
    let segmentControlsArray = [];
    setOpenBackDrop(true);

    for (
      let indexS = 0;
      indexS < values.Client.Segmentcontrols.length;
      indexS++
    ) {
      const elementS = values.Client.Segmentcontrols[indexS];

      const resCovers = await api.get(`/cover/segment/${elementS.segmentId}`);
      const resContent = await api.get(
        `/content/segment/${elementS.segmentId}`
      );

      segmentControlsArray.push({
        ...elementS,
        Segment: {
          ...elementS.Segment,
          Covers: resCovers.data,
          Contents: resContent.data,
        },
      });
    }

    let chennalOne = {
      ...values,
      Client: {
        ...values.Client,
        Segmentcontrols: segmentControlsArray,
      },
    };

    setContentSelectChannel(chennalOne);
    setOpenBackDrop(false);
    setShowContentChannel(true);
  };

  const onCloseContentChannel = () => {
    setShowContentChannel(false);
  };

  const onShowRemoteChannel = (value) => {
    setShowRemoteChannel(true);
    setGetRemoteChannelId(value);
  };

  const onCloseRemoteChannel = () => {
    setShowRemoteChannel(false);
  };

  const onShowNewVignette = () => {
    setShowNewVignette(true);
  };

  const onCloseNewVignette = () => {
    setShowNewVignette(false);
  };

  const onShowNewCommercial = () => {
    setShowNewCommercial(true);
  };

  const onCloseNewCommercial = () => {
    setShowNewCommercial(false);
  };

  const onShowEditChannel = (value) => {
    setShowEditChannel(true);
    setGetEditChannel(value);
  };

  const onCloseEditChannel = () => {
    setShowEditChannel(false);
  };

  const onShowModalSettingChannel = async (id, client) => {

    const previewLists = await api.get(`/previewlist/client/${id}`);
    console.log("get previewLists");

    const clientLocalities = await api.get(`/clientlocalitie/client/${id}`);
    console.log("get clientLocalities");

    const clientSettings = await api.get(`/clientsetting/client/${id}`);
    console.log("get clientSettings");

    const commercials = await api.get(`/commercial/client/${id}`);
    console.log("get commercial");

    const vignettes = await api.get(`/vignette/client/${id}`);
    console.log("get vignettes");

    //setIsLoadingSettings(false);

    const infoData = {
      ...client,
      Previewlists: previewLists.data,
      Clientlocalities: clientLocalities.data,
      Clientsettings: clientSettings.data,
      Commercials: commercials.data,
      Vignettes: vignettes.data,
    };

    console.log(infoData);
    setGetSettingChannelModify(clientSettings.data[0]);
    setGetInfoClient(infoData);
    setShowSetting(true);
    setIdClient(id);
  };

  const onCloseSettingChannel = () => {
    setShowSetting(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteChannel(idChannelModify);
  };

  const getSegmentControl = async () => {
    setIsLoading(true);
    const res = await api.get("/segmentcontrol");

    if (res) {
      // console.log(res.data);
      setSegmentControls(res.data);
    }
  };

  const getChannel = async () => {
    setIsLoading(true);
    const res = await api.get("/channel");

    if (res) {
      let arrayChannelSegmentControl = [];

      for (let index = 0; index < res.data.length; index++) {
        const element = res.data[index];

        const listSegmentControl = segmentControls.filter(
          (s) => s.clientId == element.clientId
        );

        let segmentControlsArray = [];

        for (let indexS = 0; indexS < listSegmentControl.length; indexS++) {
          const elementS = listSegmentControl[indexS];

          // const resCovers = await api.get(`/cover/segment/${elementS.segmentId}`);
          // const resContent = await api.get(`/content/segment/${elementS.segmentId}`);

          segmentControlsArray.push({
            ...elementS,
            Segment: {
              ...elementS.Segment,
              // Covers: resCovers.data,
              //Contents: resContent.data
            },
          });
        }

        const arrayClientChannel = {
          ...element.Client,
          Segmentcontrols: segmentControlsArray,
        };

        arrayChannelSegmentControl.push({
          ...element,
          Client: arrayClientChannel,
        });
      }

      setChannel(arrayChannelSegmentControl);
      setChannelBusca(arrayChannelSegmentControl);

      //console.log(arrayChannelSegmentControl);
    }

    setIsLoading(false);
  };

  const getBusiness = () => {
    setIsLoading(true);
    api
      .get("/business")
      .then((response) => response.data)
      .then((res) => {
        if (res.length == 0) {
          setFirstAccess(true);
        }
      });
  };

  const onUpdateChannel = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/Channel/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Radio Atualisada",
          "Radio selecionada foi atualisada com sucesso.",
          "success"
        );
        getChannel();
        onCloseEditChannel();
      }
    } else {
      showMessage(
        "Radio Falhou",
        "Houve um problema ao atualizar sua radio, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteChannel = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Channel/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getChannel();
        onCloseEditChannel();
        showMessage(
          "Radio Deletada",
          "Radio selecionada foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Loja Falhou",
        "Houve um problema ao deletar sua radio, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onSelectId = (value) => {
    setSelectId(value);
  };

  const handleChangeIndex = (index) => {
    setPageIndex(index);
  };

  const onPage = (value) => {
    handleChangeIndex(value);
  };

  const onBackMusic = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "backMusic",
      exulted: false,
    });
  };

  const onNextMusic = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "nextMusic",
      exulted: false,
    });
  };

  const onPlayMusic = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "playMusic",
      exulted: false,
    });
  };

  const onRefreshGuia = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "refreshGuia",
      exulted: false,
    });
  };

  const onCloseGuia = () => {
    socket.emit("SendInfoPlayRealTime", {
      clientId: getRemoteChannelId,
      function: "closeGuia",
      exulted: false,
    });

    onCloseRemoteChannel();
  };

  const onSelectGenre = (genre) => {
    socket.emit("SendInfoSelectGenre", {
      clientId: getRemoteChannelId,
      genre: genre,
      exulted: false,
    });
  };

  const onSelectSubGenre = (subGenre) => {
    socket.emit("SendInfoSelectSubGenre", {
      clientId: getRemoteChannelId,
      genre: subGenre,
      exulted: false,
    });
  };

  const onDisconnectChannel = (id) => {
     api.get(`/disconnect/channel/${id}`)
     .then(response => response.data)
     .then(resp => {
       if(resp.message == "success"){
        showMessage(
          "Radio Desconectada",
          "Radio selecionada foi desconectada com sucesso.",
          "success"
        );
       }
     })
  }

  return (
    <Page className={classes.root} title="Canais de Rádios">
      <Container maxWidth={false}>
        {/* <OptionChannel
          isViewAccess={levelAccess > 0 ? true : false}
          isViewEmployee={levelAccessEmployee > 0 ? true : false}
          onPage={onPage}
          indexPage={pageIndex}
        /> 
        <Divider style={{ marginTop: 25 }} />
*/}

        <TabPanel value={pageIndex} index={0} dir={theme.direction}>
          <div className="h-100">
            <div>
              {selectId.length > 0 && (
                <BottomMultSelect
                  onNewCommercial={onShowNewCommercial}
                  onNewVignette={onShowNewVignette}
                  onEditSetting={onShowEditSetting}
                />
              )}
            </div>

            <Toolbar
              SearchChannel={buscaChannel}
              onShowNewChannel={onShowNewChannel}
              levelAccess={levelAccessChannel}
              isAdministrador={isAdministrador}
            />
            <Box m={-2} mt={3}>
              {isLoading && <LinearProgress />}
              <Results
                onShowModalEditChannel={onShowEditChannel}
                onShowModalSettingChannel={onShowModalSettingChannel}
                onShowRemoteChannel={onShowRemoteChannel}
                onDeleteChannel={onShowConfirm}
                customers={Channel}
                isLoading={isLoading}
                titlesTopGrid={titlesTopGrid}
                onSelectId={onSelectId}
                levelAccess={levelAccessChannel}
                channelOnline={channelOnlines}
                channelControls={channelControls}
                isAdministrador={isAdministrador}
                onShowListContent={onShowContentChannel}
                onDisconnectChannel={onDisconnectChannel}
              />
            </Box>
          </div>
        </TabPanel>
        <TabPanel value={pageIndex} index={1} dir={theme.direction}>
          <Access
            isAdministrador={isAdministrador}
            channels={Channel}
            levelAccess={levelAccess}
            idBusiness={idBusiness}
          />
        </TabPanel>
        <TabPanel value={pageIndex} index={2} dir={theme.direction}>
          <Employee
            channelControls={channelControls}
            levelAccess={levelAccessEmployee}
            isAdministrador={isAdministrador}
          />
        </TabPanel>
        <TabPanel value={pageIndex} index={3} dir={theme.direction}>
          <RequestContent
            channelControls={channelControls}
            levelAccess={levelAccessEmployee}
            isAdministrador={isAdministrador}
          />
        </TabPanel>
        <TabPanel value={pageIndex} index={4} dir={theme.direction}>
          <Dashboard isModule={true} chOnlines={channelOnlines} />
        </TabPanel>
      </Container>

      <ModalRemoteChannel
        open={showRemoteChannel}
        listMusic={listMusic}
        onBackMusic={onBackMusic}
        onNextMusic={onNextMusic}
        onPlayMusic={onPlayMusic}
        onCloseGuia={onCloseGuia}
        onRefreshGuia={onRefreshGuia}
        onSelectGenre={onSelectGenre}
        onSelectSubGenre={onSelectSubGenre}
        getRemoteChannelId={getRemoteChannelId}
        onCloseRemoteChannel={onCloseRemoteChannel}
      />
      <ModalNewChannel
        open={showNewChannel}
        onCloseNewChannel={onCloseNewChannel}
        getAllChannel={getSegmentControl}
      />
      <ModalContentChannel
        open={showContentChannel}
        onCloseContentChannel={onCloseContentChannel}
        contentSelectChannel={contentSelectChannel}
        isMobile={isMobile}
      />
      <ModalNewVignette
        open={showNewVignette}
        selectId={selectId}
        onCloseNewVignette={onCloseNewVignette}
      />
      <ModalNewCommercial
        open={showNewCommercial}
        selectId={selectId}
        onCloseNewCommercial={onCloseNewCommercial}
      />
      <ModalNewBusiness
        open={showNewBusiness}
        firstAccess={firstAccess}
        onCloseNewBusiness={onCloseNewBusiness}
        onDisableFirstAccess={onDisableFirstAccess}
        getAllBusiness={getBusiness}
        getAllChannel={getSegmentControl}
        onShowConfirmationInfo={onShowConfirmationInfo}
        clientId={idClient}
      />
      <ModalSettingChannel
        open={showSetting}
        selectId={idClient}
        getAllChannel={() => getChannel()}
        getSettingChannelModify={getSettingChannelModify}
        getInfoClient={getInfoClient}
        onCloseSettingChannel={onCloseSettingChannel}
        isMobile={isMobile}
      />
      <ModalEditSettingChannel
        open={showEditSetting}
        selectId={selectId}
        getAllChannel={() => getChannel()}
        onCloseEditSettingChannel={onCloseEditSetting}
      />
      <ModalConfirmationInfo
        open={showConfirmationInfo}
        onCloseEditSettingChannel={onCloseEditSetting}
      />
      <ModalEditChannel
        getEditChannel={getEditChannel}
        open={showEditChannel}
        isLoading={isLoading}
        onCloseEditChannel={onCloseEditChannel}
        onDeleteChannel={onDeleteChannel}
        onUpdateChannel={onUpdateChannel}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a Loja será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />

      <Backdrop style={{ zIndex: 9999 }} open={openBackDrop}>
        <CircularProgress style={{ color: "red" }} />
      </Backdrop>
    </Page>
  );
};

export default CompanyListView;
