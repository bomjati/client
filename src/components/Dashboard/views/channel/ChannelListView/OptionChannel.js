import React, { useState, useRef, useEffect } from "react";
import { Box, Card, Button } from "@material-ui/core";
import {
  DeveloperBoard as DeveloperBoardIcon,
  Home as HomeIcon,
  MusicNote as MusicNoteIcon,
  RecordVoiceOver as RecordVoiceOverIcon,
  ArtTrack as ArtTrackIcon,
  Album as AlbumIcon,
  FolderOpen as FolderOpenIcon,
  BookmarkOutlined as BookmarkIcon,
  PostAdd as PostAddIcon,
  Receipt as ReceiptIcon,
  QueueMusic as QueueMusicIcon,
  AssignmentInd as AssignmentIndIcon,
  GroupAdd as GroupAddId,
} from "@material-ui/icons";

export default function OptionChannel({
  onPage,
  indexPage,
  isViewAccess,
  isViewEmployee,
}) {
  function useOutsideAlerter(ref) {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        selectPage(-1, "home");
      }
    }

    useEffect(() => {
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  const [activeMusic, setActiveMusic] = useState(false);
  const [activeContent, setActiveContent] = useState(false);
  const [activeVignette, setActiveVignette] = useState(false);
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  const styleSubMenu = { position: "absolute", zIndex: 1, marginTop: 10 };
  const styleButtonMenu = {
    borderRadius: 20,
    backgroundColor: "#00022E",
    color: "white",
  };
  const styleButtonSubMenu = {
    borderRadius: 20,
  };

  const selectPage = (value, type) => {
    if (value > -1) {
      onPage(value);
    }

    switch (type) {
      case "home":
        setActiveMusic(false);
        setActiveContent(false);
        setActiveVignette(false);

        break;
      case "employee":
        setActiveMusic(!activeMusic);
        setActiveContent(false);
        setActiveVignette(false);

        break;
      case "access":
        setActiveContent(!activeContent);
        setActiveMusic(false);
        setActiveVignette(false);

        break;
      case "vignette":
        setActiveVignette(!activeVignette);
        setActiveMusic(false);
        setActiveContent(false);

        break;
      default:
        break;
    }
  };

  return (
    <Box ref={wrapperRef} style={{}} display="flex" justifyContent="center">
      <div>
        <Button
          onClick={() => selectPage(0, "home")}
          startIcon={<HomeIcon />}
          style={{
            ...styleButtonMenu,
            color: indexPage == 0 ? "#f1c40f" : "white",
          }}
          className="mr-2"
          variant="contained"
        >
          Principal
        </Button>
      </div>
      {!isViewAccess ? null : (
        <div>
          <Button
            onClick={() => selectPage(1, "access")}
            startIcon={<GroupAddId />}
            style={{
              ...styleButtonMenu,
              color: indexPage == 1 ? "#f1c40f" : "white",
            }} //backgroundColor: "#FAFAFA"
            className="mr-2"
            variant="contained"
          >
            Acessos
          </Button>
        </div>
      )}
      {!isViewEmployee ? null : (
        <div>
          <Button
            onClick={() => selectPage(2, "employee")}
            startIcon={<AssignmentIndIcon />}
            style={{
              ...styleButtonMenu,
              color: indexPage == 2 ? "#f1c40f" : "white",
            }} //backgroundColor: "#FAFAFA"
            className="mr-2"
            variant="contained"
          >
            Funcionários
          </Button>
        </div>
      )}
    </Box>
  );
}
