import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import FormConfirmationInfo from "./FormConfirmationInfo";
//import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    flex: 1
  },
}));

const ModalConfirmationInfo= ({ open, onCloseConfirmation, getAllChannel }) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={onCloseConfirmation}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
              <FormConfirmationInfo
                onCloseModal={onCloseConfirmation}
              />
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalConfirmationInfo;
