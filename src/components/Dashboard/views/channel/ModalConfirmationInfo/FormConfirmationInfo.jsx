import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Input,
  MenuItem,
  Chip,
  Typography,
  GridList,
  Avatar,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Cancel as CancelIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  DriveEtaOutlined,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../../../services/api";
import momentTimezone from "moment-timezone";
import moment from "moment";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
    marginTop: 10,
  },
  selected: {
    backgroundColor: "rgba(235, 47, 6, 0.3)",
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
    backgroundColor:
      personName.indexOf(name) === -1 ? "transparent" : "rgba(235, 47, 6, 0.3)",
  };
}

const Formulario = ({ onCloseModal, getAllChannel,  }) => {
  let history = useNavigate();

  return (
    <Box
      style={{
        backgroundColor: "white",
        width: "100%",
        height: "100%",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          paddingTop: "15%",
        }}
      >
        <div>
          <Typography
            variant="h3"
            style={{
              fontWeight: "bold",
            }}
          >
            Está Quase lá!
          </Typography>
          <Typography style={{
            marginTop: 40
          }} variant="h6">Para que o sistema funcione</Typography>
          <Typography variant="h6">corretamente será necessário</Typography>
          <Typography variant="h6">fazer o login novamente.</Typography>
          <Button
          onClick={() => {
            history("/home");
          }}
            variant="contained"
            style={{
              width: "100%",
              marginTop: 30,
              backgroundColor: "red",
              color: 'white'
            }}
          >
            OK
          </Button>
        </div>
      </div>
    </Box>
  );
};

export default Formulario;
