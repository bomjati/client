import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Typography,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  Edit as EditIcon,
  DeleteOutline as DeleteOutlineIcon,
  Check as CheckIcon,
  Close as CloseIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";
import api from "../../../../../services/api";

const Formulario = ({
  onCloseEditBusiness,
  getEditBusiness,
  onUpdateBusiness,
  onDeleteBusiness,
  isLoading,
}) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [showConfirm, setShowConfirm] = useState(false);
  const [listSegment, setListSegment] = useState(null);
  const [values, setValues] = useState({
    registration_number: "",
    business_name: "",
    fantasy_name: "",
    cep: "",
    public_place: "",
    number: "",
    complement: "",
    district: "",
    county: "",
    uf: "",
    email: "",
    phone1: "",
    phone2: "",
    segmentId: "",
  });
  const [value, setValue] = useState(0);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const styleDetele = createMuiTheme({
    palette: {
      primary: {
        main: "#e53935",
      },
    },
  });

  const styleSuccess = createMuiTheme({
    palette: {
      primary: {
        main: "#5352ed",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const getInfoEditBusiness = () => {
    if (getEditBusiness) {
      setValues({
        ...values,
        registration_number: getEditBusiness.registration_number,
        business_name: getEditBusiness.business_name,
        fantasy_name: getEditBusiness.fantasy_name,
        cep: getEditBusiness.cep,
        public_place: getEditBusiness.public_place,
        number: getEditBusiness.number,
        complement: getEditBusiness.complement,
        district: getEditBusiness.district,
        county: getEditBusiness.county,
        uf: getEditBusiness.uf,
        email: getEditBusiness.email,
        phone1: getEditBusiness.phone1,
        phone2: getEditBusiness.phone2,
        segmentId: getEditBusiness.segmentId,
      });
    }
    // console.log('passou aqui')
    // console.log(getEditBusiness)
  };

  const validateCampos = () => {
    for (var key in values) {
      if (key != "complement") {
        if (key != "phone2") {
          if ((values[key] == "") | (String(values[key]) == "null")) {
            let campoRecused =
              key == "registration_number"
                ? " CNPJ "
                : key == "business_name"
                ? " Rasão Social "
                : key == "fantasy_name"
                ? " Nome Fantasia "
                : key == "cep"
                ? " CEP "
                : key == "public_place"
                ? " Logradouro "
                : key == "number"
                ? " Número Endereço "
                : key == "district"
                ? " Bairro "
                : key == "county"
                ? " Cidade "
                : key == "uf"
                ? " UF "
                : key == "email"
                ? " E-mail "
                : key == "phone1"
                ? " Telefone 1 "
                : key == "segmentId"
                ? " Segmento "
                : "Indefinido";
                
            showMessage(
              "Atenção",
              `Campo ${campoRecused} não foi preenchido.`,
              "warning"
            );
            return;
          }
        }
      }
    }

    onUpdateBusiness(values, getEditBusiness.id);
  };

  const MascaraParaLabel = (value) => {
    value = String(value).replace(/\D/g, "");
    value = String(value).replace(/^(\d{2})(\d)/, "$1.$2");
    value = String(value).replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    value = String(value).replace(/\.(\d{3})(\d)/, ".$1/$2");
    value = String(value).replace(/(\d{4})(\d)/, "$1-$2");

    return value;
  };

  const mTel = (value) => {
    value = String(value).replace(/\D/g, "");
    value = String(value).replace(/^(\d{2})(\d)/g, "($1) $2");
    value = String(value).replace(/(\d)(\d{4})$/, "$1-$2");
    return value;
  };

  const handleChange = (event) => {
    console.log(event);
    console.log(event);

    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onDelete = () => {
    onDeleteBusiness(getEditBusiness.id);
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteBusiness(getEditBusiness.id);
  };

  useEffect(() => {
    getInfoEditBusiness();
  }, []);

  useEffect(() => {
    api
      .get("/segment")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListSegment(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      <CssBaseline />
      <Toolbar />

      <ThemeProvider theme={theme}>
        <Container component="article" maxWidth="md" className="pb-2">
          <Box
            display="flex"
            justifyContent="flex-end"
            className="w-100 mt-2 mb-2 mr-2"
            p={0}
          >
            <IconButton
              style={{ backgroundColor: "white" }}
              onClick={onCloseEditBusiness}
            >
              <CloseIcon />
            </IconButton>
          </Box>
          <Card style={{ borderRadius: 20 }}>
            <CardHeader
              subheader="Preencha as informações de sua empresa para prosseguir"
              title="Editando Loja"
            />

            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Empresa" {...a11yProps(0)} />
                <Tab label="Endereço" {...a11yProps(1)} />
                <Tab label="Contato" {...a11yProps(2)} />
                <Tab label="Segmento" {...a11yProps(3)} />
              </Tabs>
            </AppBar>

            <CardContent>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={4} xs={12}>
                      <TextField
                        value={MascaraParaLabel(values.registration_number)}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            registration_number: String(event.target.value)
                              .replace(" ", "")
                              .replace(/\D/g, ""),
                          });
                        }}
                        inputProps={{
                          maxLength: 18,
                        }}
                        onBlur={(event) => {
                          validarCampo(event);
                        }}
                        error={!erros.registration_number.valido}
                        helperText={erros.registration_number.texto}
                        label="Inscrição CNPJ "
                        margin="normal"
                        id="registration_number"
                        name="registration_number"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.fantasy_name}
                        onChange={handleChange}
                        label="Nome Fantasia"
                        margin="normal"
                        id="fantasy_name"
                        name="fantasy_name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>

                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.business_name}
                        onChange={handleChange}
                        label="Nome Empresarial"
                        margin="normal"
                        id="business_name"
                        name="business_name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={3} xs={12}>
                      <TextField
                        value={values.cep}
                        onChange={handleChange}
                        onBlur={validarCampo}
                        error={!erros.cep.valido}
                        helperText={erros.cep.texto}
                        label="CEP"
                        margin="normal"
                        id="cep"
                        name="cep"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={7} xs={12}>
                      <TextField
                        value={values.county}
                        onChange={handleChange}
                        label="Município"
                        margin="normal"
                        id="county"
                        name="county"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={2} xs={12}>
                      <FormControl
                        fullWidth
                        variant="outlined"
                        required
                        className="mt-3"
                      >
                        <InputLabel htmlFor="age-native-required">
                          UF
                        </InputLabel>
                        <Select
                          native
                          value={values.uf}
                          name="uf"
                          onChange={handleChange}
                        >
                          {/*Lista de UF*/}
                          <option aria-label="None" value="" />
                          {UFs.map((i) => {
                            return <option value={i.value}>{i.value}</option>;
                          })}
                        </Select>
                      </FormControl>
                    </Grid>
                    <Grid item md={9} xs={12}>
                      <TextField
                        value={values.public_place}
                        onChange={handleChange}
                        label="Logradouro"
                        margin="normal"
                        id="public_place"
                        name="public_place"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={3} xs={12}>
                      <TextField
                        value={values.number}
                        onChange={handleChange}
                        label="Número"
                        margin="normal"
                        id="number"
                        name="number"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>

                    <Grid item md={6} xs={12}>
                      <TextField
                        value={values.district}
                        onChange={handleChange}
                        label="Bairro/Distrito"
                        margin="normal"
                        id="district"
                        name="district"
                        variant="outlined"
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        value={values.complement}
                        onChange={handleChange}
                        label="Complemento"
                        margin="normal"
                        id="complement"
                        name="complement"
                        variant="outlined"
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.email}
                        onChange={handleChange}
                        onBlur={validarCampo}
                        error={!erros.email.valido}
                        helperText={erros.email.texto}
                        label="E-mail"
                        margin="normal"
                        id="email"
                        name="email"
                        variant="outlined"
                        size="medium"
                        required
                        fullWidth
                      />
                    </Grid>

                    <Grid item md={6} xs={12}>
                      <TextField
                        value={mTel(values.phone1)}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            phone1: String(event.target.value)
                              .replace(" ", "")
                              .replace(/\D/g, ""),
                          });
                        }}
                        onBlur={validarCampo}
                        error={!erros.phone1.valido}
                        helperText={erros.phone1.texto}
                        inputProps={{
                          maxLength: 13,
                        }}
                        label="Telefone 1"
                        margin="normal"
                        id="phone1"
                        name="phone1"
                        variant="outlined"
                        size="medium"
                        required
                        fullWidth
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        value={mTel(values.phone2)}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            phone2: String(event.target.value)
                              .replace(" ", "")
                              .replace(/\D/g, ""),
                          });
                        }}
                        label="Telefone 2"
                        margin="normal"
                        id="phone2"
                        name="phone2"
                        variant="outlined"
                        size="medium"
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={3} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined">
                          <InputLabel htmlFor="age-native-required">
                            Segmentos
                          </InputLabel>
                          <Select
                            required
                            native
                            name="segmentId"
                            id="segmentId"
                            value={values.segmentId}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listSegment &&
                              listSegment.map((segments) => (
                                <option value={segments.id}>
                                  {segments.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </CardContent>
            <Divider />

            <Box
              display="flex"
              justifyContent="flex-end"
              className="w-100"
              p={2}
            >
              <Grid container spacing={1} className="w-100">
                <Grid item md={3} xs={12}>
                  <ThemeProvider theme={styleDetele}>
                    <Button
                      onClick={() => {
                        setShowConfirm(true);
                      }}
                      fullWidth
                      disabled={isLoading}
                      style={{ borderRadius: 50 }}
                      color="primary"
                      variant="contained"
                      size="medium"
                      startIcon={<DeleteOutlineIcon />}
                    >
                      Excluir
                    </Button>
                  </ThemeProvider>
                </Grid>
                <Grid item md={3} xs={12}>
                  <ThemeProvider theme={styleSuccess}>
                    <Button
                      onClick={onUpdate}
                      fullWidth
                      disabled={isLoading}
                      style={{ borderRadius: 50 }}
                      color="primary"
                      variant="contained"
                      size="medium"
                      startIcon={<CheckIcon />}
                    >
                      Salvar
                    </Button>
                  </ThemeProvider>
                </Grid>
                {isLoading && (
                  <Grid item md={3} xs={12}>
                    <CircularProgress />
                  </Grid>
                )}
              </Grid>
            </Box>
          </Card>
        </Container>
      </ThemeProvider>
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a Loja de nome: ${
          values.fantasy_name
        } e CNPJ de número: ${MascaraParaLabel(
          values.registration_number
        )} será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </form>
  );
};

export default Formulario;
