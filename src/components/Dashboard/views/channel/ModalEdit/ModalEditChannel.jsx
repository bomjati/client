import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CadastroBusiness from './FormEditChannel'
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import {
    validarCep,
    validarCnpj,
    validarEmail,
    validarPassword,
    validarPhone,
  } from "../../../../../validation/cadastroEmpresa";

const useStyles = makeStyles(({ palette, breakpoints, spacing }) => ({
    modal: {
        display: 'flex',
        justifyContent: 'center',
        [breakpoints.up('xs')]: {
            alignItems: 'start',
        },
        [breakpoints.up('sm')]: {
            alignItems: 'center',
        },
        overflow: 'scroll'
    }
}));

const ModalNewBusiness = ({ open, onCloseEditBusiness, getEditBusiness, onUpdateBusiness, isLoading, onDeleteBusiness }) => {
    const classes = useStyles();

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={onCloseEditBusiness}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className='mb-4'>
                        <ContextValidacoesCadastro.Provider
                            value={{
                                registration_number: validarCnpj,
                                cep: validarCep,
                                phone1: validarPhone,
                                password: validarPassword,
                                email: validarEmail,
                            }}
                        >
                            <CadastroBusiness 
                            getEditBusiness={getEditBusiness} 
                            isLoading={isLoading}
                            onCloseEditBusiness={onCloseEditBusiness}
                            onUpdateBusiness={onUpdateBusiness}
                            onDeleteBusiness={onDeleteBusiness} />
                        </ContextValidacoesCadastro.Provider>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

export default ModalNewBusiness;