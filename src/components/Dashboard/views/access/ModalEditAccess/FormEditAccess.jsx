import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  InputAdornment,
  Switch,
  Collapse,
  Checkbox,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import { useCookies } from "react-cookie";
import Results from "./Results";
import { titlesGrid } from "./data";

const Formulario = ({
  onCloseModal,
  getAllAccess,
  idClient,
  idBusiness,
  getEditAccess,
  channels,
  isMobile,
}) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listAccess, setListAccess] = useState(null);
  const [value, setValue] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [isViewConfirm, setIsViewConfirm] = useState(false);
  const [isView, setIsView] = useState(false);
  const [planControls, setPlanControls] = useCookies(["PlanControls"]);
  const [modules, setModules] = useState([]);
  const [showCollapse, setShowCollapse] = useState({});
  const [checkAccess, setCheckAccess] = useState({});
  const [selectId, setSelectId] = useState([]);
  const [confirmPassword, setConfirmPassword] = useState("");
  const [titlesTopGrid] = useState(titlesGrid);
  const [values, setValues] = useState({
    name: "",
    email: "",
    password: "",
  });
  const styleCard = { borderRadius: 20 };

  useEffect(() => {
    console.log(channels);
  }, [channels]);

  useEffect(() => {
    setModules(planControls.PlanControls.Plan.Modulecontrols);

    planControls.PlanControls.Plan.Modulecontrols.map((module) => {
      setShowCollapse({
        ...showCollapse,
        [module.Module.id]: !showCollapse[String(module.Module.id)]
          ? true
          : false,
      });
    });
  }, planControls);

  useEffect(() => {
    console.log(getEditAccess);
    setValues({
      ...values,
      name: getEditAccess.name,
      email: getEditAccess.email,
      password: getEditAccess.password,
    });

    let checkAccessArray = {};
    for (let index = 0; index < getEditAccess.Accesscontrols.length; index++) {
      const element = getEditAccess.Accesscontrols[index];

      checkAccessArray = {
        ...checkAccessArray,
        [element.accessId]: element.level,
      };
    }

    setCheckAccess(checkAccessArray);
  }, [getEditAccess]);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "name"
            ? " Nome do Usuário "
            : key == "email"
            ? " E-mail do Usuário "
            : key == "password"
            ? " Senha "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    if (!(values["password"] == confirmPassword)) {
      showMessage(
        "Atenção",
        `Campo Senha e Confirme sua senha não coecidem.`,
        "warning"
      );

      return;
    }

    if (Object.keys(checkAccess).length > 0) {
      let countValue = 0;

      for (const key in checkAccess) {
        if (checkAccess.hasOwnProperty.call(checkAccess, key)) {
          const element = checkAccess[key];

          countValue = countValue + element;
        }
      }

      console.log(countValue);

      if (!(countValue > 0)) {
        showMessage(
          "Atenção",
          `Selecione ao menos uma permissão de acesso para esse usuário.`,
          "warning"
        );

        setValue(1);

        return;
      }
    } else {
      showMessage(
        "Atenção",
        `Selecione ao menos uma permissão de acesso para esse usuário.`,
        "warning"
      );

      setValue(1);

      return;
    }

    if (selectId.length == 0) {
      showMessage(
        "Atenção",
        `Selecione ao menos um canal para esse usuário.`,
        "warning"
      );

      setValue(2);

      return;
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/user/edit/${getEditAccess.id}`, values)
      .then((response) => response.data)
      .then((respU) => {
        if (respU.message == "success") {
          for (let index = 0; index < selectId.length; index++) {
            const elementid = selectId[index];

            api
              .post("/channelcontrol/new", {
                clientId: elementid,
                userId: respU.data.id,
              })
              .then((response) => response.data)
              .then((resps) => {
                if (resps.message == "success") {
                  if (index + 1 == selectId.length) {
                    let countIndex = 0;
                    let dataArrayAccess = [];

                    for (const key in checkAccess) {
                      if (checkAccess.hasOwnProperty.call(checkAccess, key)) {
                        const element = checkAccess[key];

                        dataArrayAccess.push({
                          accessId: key,
                          userId: respU.data.id,
                          level: element,
                        });
                      }
                    }

                    api
                      .post("/Accesscontrol/new", { data: dataArrayAccess })
                      .then((response) => response.data)
                      .then((resps) => {
                        if (resps.message == "success") {
                          showMessage(
                            "Acesso Editado",
                            `Edição realizada com sucesso.`,
                            "success"
                          );
                          getAllAccess();
                          setIsLoading(false);
                          onCloseModal();
                        } else {
                          showMessage("Editar Acesso", `Falhou.`, "error");
                        }
                      })
                      .catch((error) => {
                        console.log(error);
                      });
                  }
                } else {
                  showMessage("Editar Acesso", `Falhou.`, "error");
                }
              })
              .catch((error) => {
                console.log(error);
              });
          }
        } else {
          showMessage("Editar Acesso", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onSelectId = (value) => {
    setSelectId(value);
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu acesso"
            title="Editar Acesso"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do Acesso" {...a11yProps(0)} />
                <Tab label="Controle de Acesso" {...a11yProps(1)} />
                <Tab label="Acesso aos Canais" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.name}
                        onChange={handleChange}
                        label="Nome"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                      <TextField
                        value={values.email}
                        onChange={handleChange}
                        label="E-mail"
                        margin="normal"
                        id="name"
                        name="email"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                      <TextField
                        type={`${!isView ? "password" : "text"}`}
                        value={values.password}
                        onChange={handleChange}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() => {
                                  setIsView(!isView);
                                }}
                              >
                                {isView ? (
                                  <VisibilityIcon />
                                ) : (
                                  <VisibilityOffIcon />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        label="Senha"
                        margin="normal"
                        id="name"
                        name="password"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                      <TextField
                        type={`${!isViewConfirm ? "password" : "text"}`}
                        value={confirmPassword}
                        onChange={(event) => {
                          setConfirmPassword(event.target.value);
                        }}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() => {
                                  setIsViewConfirm(!isViewConfirm);
                                }}
                              >
                                {isViewConfirm ? (
                                  <VisibilityIcon />
                                ) : (
                                  <VisibilityOffIcon />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        label="Confirma Senha"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <div>
                    {modules.map((module) => (
                      <Card style={styleCard}>
                        <Box
                          onClick={() => {
                            setShowCollapse({
                              ...showCollapse,
                              [module.Module.id]: !showCollapse[
                                String(module.Module.id)
                              ]
                                ? true
                                : false,
                            });
                          }}
                          style={{ cursor: "pointer" }}
                          p={2}
                          display="flex"
                          justifyContent="space-between"
                        >
                          <h4 className="text-black">{module.Module.name}</h4>
                          {showCollapse[String(module.Module.id)] ? (
                            <KeyboardArrowUpIcon />
                          ) : (
                            <KeyboardArrowDownIcon />
                          )}
                        </Box>
                        <Collapse in={showCollapse[String(module.Module.id)]}>
                          {module.Module.Accesses.map((access) => (
                            <div className="pl-4 pr-4 pb-4">
                              <Card style={styleCard} className="p-4">
                                <h6
                                  style={{
                                    fontWeight: "bold",
                                    color: "#1e272e",
                                  }}
                                  className=""
                                >
                                  {access.name}
                                </h6>
                                <Box
                                  display="flex"
                                  flexDirection={isMobile ? "column" : "row"}
                                >
                                  <FormControlLabel
                                    control={
                                      <Checkbox
                                        checked={
                                          checkAccess[String(access.id)] > 0
                                        }
                                        onChange={() => {
                                          setCheckAccess({
                                            ...checkAccess,
                                            [access.id]: checkAccess[
                                              String(access.id)
                                            ]
                                              ? checkAccess[String(access.id)] >
                                                0
                                                ? 0
                                                : 1
                                              : 1,
                                          });
                                        }}
                                        name="checkedB"
                                        color="primary"
                                      />
                                    }
                                    label="Buscar"
                                  />
                                  <FormControlLabel
                                    control={
                                      <Checkbox
                                        checked={
                                          checkAccess[String(access.id)] > 1
                                        }
                                        onChange={() => {
                                          setCheckAccess({
                                            ...checkAccess,
                                            [access.id]: checkAccess[
                                              String(access.id)
                                            ]
                                              ? checkAccess[String(access.id)] >
                                                1
                                                ? 1
                                                : 2
                                              : 2,
                                          });
                                        }}
                                        name="checkedB"
                                        color="primary"
                                      />
                                    }
                                    label="Adicionar"
                                  />
                                  <FormControlLabel
                                    control={
                                      <Checkbox
                                        checked={
                                          checkAccess[String(access.id)] > 2
                                        }
                                        onChange={() => {
                                          setCheckAccess({
                                            ...checkAccess,
                                            [access.id]: checkAccess[
                                              String(access.id)
                                            ]
                                              ? checkAccess[String(access.id)] >
                                                2
                                                ? 2
                                                : 3
                                              : 3,
                                          });
                                        }}
                                        name="checkedB"
                                        color="primary"
                                      />
                                    }
                                    label="Alterar"
                                  />
                                  <FormControlLabel
                                    control={
                                      <Checkbox
                                        checked={
                                          checkAccess[String(access.id)] > 3
                                        }
                                        onChange={() => {
                                          setCheckAccess({
                                            ...checkAccess,
                                            [access.id]: checkAccess[
                                              String(access.id)
                                            ]
                                              ? checkAccess[String(access.id)] >
                                                3
                                                ? 3
                                                : 4
                                              : 4,
                                          });
                                        }}
                                        name="checkedB"
                                        color="primary"
                                      />
                                    }
                                    label="Deletar"
                                  />
                                </Box>
                              </Card>
                            </div>
                          ))}
                        </Collapse>
                      </Card>
                    ))}
                  </div>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                  <Results
                    getEditAccess={getEditAccess}
                    onSelectId={onSelectId}
                    titlesTopGrid={titlesTopGrid}
                    customers={channels}
                  />
                </TabPanel>
              
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
