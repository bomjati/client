import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  FormGroup,
  FormControlLabel,
  makeStyles,
  Switch,
} from "@material-ui/core";
import { ImageSearch as ImageSearchIcon } from "@material-ui/icons";
const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 100,
    width: 100,
  },
}));

const ProductImage = ({
  onModeImage,
  onChangeImageProduct,
  isEditing,
  productData,
  className,
  ...rest
}) => {
  const classes = useStyles();
  const [retrato, setRetrato] = useState(true);
  const [paisagem, setPaisagem] = useState(false);
  const [selectedFile, setSelectedFile] = useState();
  const [preview, setPreview] = useState();

  // criar uma prévia como efeito colateral, sempre que o arquivo selecionado for alterado
  useEffect(() => {
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // memória livre sempre que este componente for desmontado
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    // Eu mantive este exemplo simples usando a primeira imagem em vez de vários
    setSelectedFile(e.target.files[0]);
    onChangeImageProduct(e.target.files[0]);
  };

  const handleChangeRetrato = () => {
    setRetrato(!retrato);
    setPaisagem(false);
    onModeImage("retrato");
  };
  const handleChangePaisagem = () => {
    setRetrato(false);
    setPaisagem(!paisagem);
    onModeImage("paisagem");
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Box alignItems="center" display="flex" flexDirection="column">
          {/* imagem produto  */}
          {!selectedFile & !isEditing ? (
            <ImageSearchIcon style={{ fontSize: 80 }} />
          ) : null}
          {(selectedFile && <img src={preview} />) ||
            (isEditing && (
              <img
                src={
                  productData.Imageproducts.length
                    ? productData.Imageproducts[0].path_s3
                    : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000"
                }
              />
            ))}
        </Box>
      </CardContent>
      {/* Tipo de dimenções  */}
      <Divider />
      <FormGroup row className="justify-content-between ml-2 mr-2">
        <FormControlLabel
          control={
            <Switch
              checked={retrato}
              onChange={() => {
                handleChangeRetrato();
              }}
              name="retrato"
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
          }
          label="Retrato"
          labelPlacement="start"
        />

        <FormControlLabel
          control={
            <Switch
              checked={paisagem}
              onChange={() => {
                handleChangePaisagem();
              }}
              color="primary"
              name="paisagem"
              inputProps={{ "aria-label": "primary checkbox" }}
            />
          }
          label="Paisagem"
          labelPlacement="end"
        />
      </FormGroup>
      {/* Abri Diretorio */}
      <Divider />

      <CardActions>
        <Button
          color="primary"
          fullWidth
          variant="text"
          htmlFor="sampleFile"
          component="label"
        >
          Abrir Diretório
        </Button>
      </CardActions>

      <input
        onChange={onSelectFile}
        type="file"
        id="sampleFile"
        style={{ display: "none" }}
      />
    </Card>
  );
};

ProductImage.propTypes = {
  className: PropTypes.string,
};

export default ProductImage;
