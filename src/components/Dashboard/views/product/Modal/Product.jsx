import React, { useEffect, useState } from "react";
import {
  Container,
  Grid,
  makeStyles,
  Card,
  IconButton,
  Box
} from "@material-ui/core";
import { Close as CloseIcon } from "@material-ui/icons";
import Page from "../../../components/Page";
import ProductImage from "./ProductImage";
import ProductDetails from "./ProductDetails";
import api from "../../../../../services/api";

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Product = ({
  onCloseModal,
  getAllProduct,
  isLoading,
  allCategory,
  isEditing,
  productData,
  onEditProduct,
  idEcommerce
}) => {
  const classes = useStyles();
  const [userMe, setUserMe] = useState("");
  const [infoImage, setInfoImage] = useState("");
  const [modeImage, setModeImage] = useState("");

  // requesição para retornar as informações de quem logou
  const onChangeImageProduct = value => {
    setInfoImage(value);
  };

  const onModeImage = value => {
    setModeImage(value);
  };

  return (
    <Page className={classes.root} title="Novo Produto">
      <Container maxWidth="lg">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }} className="p-4">
          <Grid container spacing={3}>
            <Grid item lg={4} md={6} xs={12}>
              <ProductImage
                isEditing={isEditing}
                productData={productData}
                onChangeImageProduct={onChangeImageProduct}
                onModeImage={onModeImage}
              />
            </Grid>
            <Grid item lg={8} md={6} xs={12}>
              <ProductDetails
                getInfoImage={infoImage}
                onCloseModal={onCloseModal}
                onEditProduct={onEditProduct}
                getAllProduct={getAllProduct}
                allCategory={allCategory}
                productData={productData}
                isLoading={isLoading}
                isEditing={isEditing}
                modeImage={modeImage}
                idEcommerce={idEcommerce}
              />
            </Grid>
          </Grid>
        </Card>
      </Container>
    </Page>
  );
};

export default Product;
