import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  FormControl,
  Select,
  InputLabel,
  CircularProgress,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import { AccessTime as AccessTimeIcon } from "@material-ui/icons";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProductDetails = ({
  modeImage,
  getInfoImage,
  onCloseModal,
  getAllProduct,
  isLoading,
  allCategory,
  productData,
  isEditing,
  onEditProduct,
  idEcommerce,
  className,
  ...rest
}) => {
  const classes = useStyles();
  const [isLoad, setIsLoad] = useState(false);
  const dateNow = moment();
  const [values, setValues] = useState({
    name: "",
    categoryId: "",
    detail: "",
    price: "0",
    promotion_price: "0",
    date_time_start: dateNow,
    date_time_end: dateNow,
  });

  useEffect(() => {
    if (isEditing) {
      getInfoProductEdit();
    }
  }, []);

  const getInfoProductEdit = () => {
    console.log(productData);
    setValues({
      name: productData.name,
      categoryId: productData.categoryId,
      detail: productData.detail,
      price: productData.price,
      promotion_price: productData.promotion_price,
      date_time_start: moment(
        productData.date_time_start,
        "YYYY-MM-DD HH:mm:ss"
      ),
      date_time_end: moment(productData.date_time_end, "YYYY-MM-DD HH:mm:ss"),
    });
  };

  const PostNewProduct = async () => {
    // console.log(values)
    if (values.price < 0.01) {
      showMessage(
        "Preço do Produto",
        "Informe o preço do produto s/ desconto",
        "warning"
      );
      return;
    } else if (values.promotion_price < 0.01) {
      showMessage(
        "Preço do Produto",
        "Informe o preço do produto c/ desconto",
        "warning"
      );
      return;
    } else if (!getInfoImage & !isEditing) {
      showMessage(
        "Imagem Produto",
        "Selecione uma imagem para seu produto",
        "warning"
      );
      return;
    } else if ((dateNow == values.date_start) & (dateNow == values.date_end)) {
      showMessage(
        "Periodo de Oferta",
        "Informe o periodo a qual deseja que sua oferta esteja visivel",
        "warning"
      );
      return;
    } else if ((dateNow == values.time_start) & (dateNow == values.time_end)) {
      showMessage(
        "Periodo de Oferta",
        "Informe o periodo a qual deseja que sua oferta esteja visivel",
        "warning"
      );
      return;
    } else {
      //console.log(values)
      //console.log(getInfoImage)

      const productFormData = {
        name: values.name,
        price: values.price,
        promotion_price: values.promotion_price,
        detail: values.detail,
        ecommerceId: idEcommerce,
        categoryId: values.categoryId,
        date_time_start: moment(values.date_time_start).format(
          "YYYY-MM-DD HH:mm:ss"
        ),
        date_time_end: moment(values.date_time_end).format(
          "YYYY-MM-DD HH:mm:ss"
        ),
      };

      const productFormDataImg = new FormData();
      productFormDataImg.append(
        "file",
        !getInfoImage ? "sem imagem" : getInfoImage
      );

      if (isEditing) {
        if (!getInfoImage) {
          onEditProduct(productFormData, productData.id);
        } else {
          setIsLoad(true);
          await api
            .post(
              `/imageproduct/edit/${productData.Imageproducts[0].id}`,
              productFormDataImg
            )
            .then((res) => {
              console.log(res.data);
              if (res.data.message == "success") {
                onEditProduct(productFormData, productData.id);
              }
              setIsLoad(false);
            })
            .catch((error) => {
              console.log(error);
              setIsLoad(false);
            });
        }
      } else {
        setIsLoad(true);
        await api
          .post("/product/new", productFormData)
          .then(async (res) => {
            console.log(res.data);
            if (res.data.message == "success") {
              await api
            .post(`/imageproduct/new/${res.data.data.id}`,productFormDataImg)
            .then((resi) => {
              console.log(res.data);
              if (resi.data.message == "success") {
                showMessage(
                  "Novo Produto",
                  "Cadastro realizado com sucesso",
                  "success"
                );
                getAllProduct();
                onCloseModal();
              }
              setIsLoad(false);
            })
            .catch((error) => {
              console.log(error);
              setIsLoad(false);
            });
            }
          })
          .catch((error) => {
            console.log(error);
            setIsLoad(false);
          });
      }
    }
  };

  // onChange dos campos do form
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const formatRealBRL = (v) => {
    //console.log(v)
    v = parseFloat(parseInt(String(v).replace(/\D/g, "")) / 100).toFixed(2);
    v = String("R$ " + v).replace(".", ",");
    return v;
  };

  const insert = (main_string, ins_string, pos) => {
    if (typeof pos == "undefined") {
      pos = 0;
    }
    if (typeof ins_string == "undefined") {
      ins_string = "";
    }
    return main_string.slice(0, pos) + ins_string + main_string.slice(pos);
  };

  return (
    <form
      autoComplete="off"
      onSubmit={(event) => {
        event.preventDefault();
        PostNewProduct();
      }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20 }}>
        <CardHeader
          subheader="Preencha as informações do seu produto"
          title={!isEditing ? "Novo Produto" : "Editando Produto"}
        />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            {/* campo responsivo | nome produto */}
            <Grid item md={8} xs={12}>
              <TextField
                fullWidth
                label="Nome"
                name="name"
                onChange={handleChange}
                required
                value={String(values.name)
                  .toLowerCase()
                  .replace(/(?:^|\s)\S/g, function (a) {
                    return a.toUpperCase();
                  })}
                variant="outlined"
              />
            </Grid>
            {/* campo resposivo | categoria */}
            <Grid item md={4} xs={12}>
              <FormControl
                requireds
                fullWidth
                variant="outlined"
                className={classes.formControl_segment}
              >
                <InputLabel fullWidth htmlFor="age-native-required">
                  Categoria
                </InputLabel>
                <Select
                  fullWidth
                  required
                  native
                  name="categoryId"
                  id="segment"
                  value={values.categoryId}
                  onChange={handleChange}
                >
                  <option aria-label="None" value="" />
                  {allCategory.map((i) => (
                    <option value={i.id}>{i.name}</option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            {/* campo responsivo | detalhe */}
            <Grid item md={12} xs={12}>
              <TextField
                fullWidth
                label="Detalhe"
                multiline
                rowsMax={4}
                rows={4}
                name="detail"
                onChange={handleChange}
                required
                value={values.detail}
                variant="outlined"
              />
            </Grid>
            {/* campo resposivo | preço */}
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Preço"
                name="price"
                onChange={(event) => {
                  setValues({
                    ...values,
                    price: insert(
                      String(event.target.value).replace(/\D/g, ""),
                      ".",
                      -2
                    ),
                  });
                }}
                required
                value={formatRealBRL(values.price)}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | preço oferta  */}
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                label="Preço C/ Desconto"
                name="promotion_price"
                onChange={(event) => {
                  setValues({
                    ...values,
                    promotion_price: insert(
                      String(event.target.value).replace(/\D/g, ""),
                      ".",
                      -2
                    ),
                  });
                }}
                required
                value={formatRealBRL(values.promotion_price)}
                variant="outlined"
              />
            </Grid>
            {/* campo resposivo | Inicio promoção */}
            <Grid item md={6} xs={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container spacing={2}>
                  <Grid item md={6} xs={12}>
                    <KeyboardDatePicker
                      disableToolbar
                      inputVariant="outlined"
                      helperText="Campo designado para o inicio da promoção."
                      format="dd/MM/yyyy"
                      id="date-picker-outlined"
                      label="Dia Inicio"
                      name="date_start"
                      value={values.date_start}
                      onChange={(event) => {
                        setValues({
                          ...values,
                          date_start: event,
                        });
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <KeyboardTimePicker
                      id="time-picker"
                      ampm={false}
                      inputVariant="outlined"
                      label="Time picker"
                      name="date_time_start"
                      label="Hora Inicio"
                      value={values.date_time_start}
                      onChange={(event) => {
                        setValues({
                          ...values,
                          date_time_start: event,
                        });
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change time",
                      }}
                      keyboardIcon={<AccessTimeIcon />}
                    />
                  </Grid>
                </Grid>
              </MuiPickersUtilsProvider>
            </Grid>
            {/* campo resposivo | término promoção */}
            <Grid item md={6} xs={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container spacing={2}>
                  <Grid item md={6} xs={12}>
                    <KeyboardDatePicker
                      disableToolbar
                      inputVariant="outlined"
                      helperText="Campo designados para o final da promoção."
                      format="dd/MM/yyyy"
                      id="date-picker-outlined"
                      label="Dia Fim"
                      name="date_time_end"
                      value={values.date_time_end}
                      onChange={(event) => {
                        setValues({
                          ...values,
                          date_time_end: event,
                        });
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <KeyboardTimePicker
                      id="time-picker"
                      ampm={false}
                      inputVariant="outlined"
                      label="Time picker"
                      name="date_time_end"
                      label="Hora Fim"
                      value={values.date_time_end}
                      onChange={(event) => {
                        setValues({
                          ...values,
                          date_time_end: event,
                        });
                      }}
                      KeyboardButtonProps={{
                        "aria-label": "change time",
                      }}
                      keyboardIcon={<AccessTimeIcon />}
                    />
                  </Grid>
                </Grid>
              </MuiPickersUtilsProvider>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        {/*  linha direcionada flex-end */}
        <Box display="flex" justifyContent="flex-end" p={2}>
          {/* Botão para salvar as informações */}
          <Button
            color="primary"
            disabled={isLoad || isLoading}
            variant="contained"
            style={{ borderRadius: 50 }}
            type="submit"
          >
            Salvar produto
          </Button>
          {isLoad && <CircularProgress className="ml-2" />}
          {isLoading && <CircularProgress className="ml-2" />}
        </Box>
      </Card>
    </form>
  );
};

ProductDetails.propTypes = {
  className: PropTypes.string,
};

export default ProductDetails;
