import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Divider,
  Grid,
  Typography,
  makeStyles,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
} from "@material-ui/core";
import {
  MoreVert as MoreVertIcon,
  VisibilityOff as VisibilityOffIcon,
  Edit as EditIcon,
  Visibility as VisibilityIcon,
  AccessTime as AccessTimeIcon,
  Delete as DeleteIcon,
} from "@material-ui/icons";
import moment from "moment";
import "moment/locale/pt-br";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
  statsItem: {
    alignItems: "center",
    display: "flex",
  },
  statsIcon: {
    marginRight: theme.spacing(1),
  },
}));

const ProductCard = ({
  className,
  product,
  onDelete,
  onShowEditProduct,
  ...rest
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Box display="flex" justifyContent="flex-end" p={0}>
        <IconButton
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem
            onClick={() => {
              onShowEditProduct(product);
              handleClose();
            }}
          >
            <ListItemIcon>
              <EditIcon style={{ color: "#686de0" }} fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Editar</Typography>
          </MenuItem>
          <Divider />
          <MenuItem style={{ alignItems: "center" }} onClick={handleClose}>
            {moment().isBefore(
              moment(String(product.date_time_end), "YYYY-MM-DD HH:mm:ss")
            ) ? (
              <div>
                <ListItemIcon>
                  <VisibilityOffIcon
                    style={{ color: "#e67e22" }}
                    fontSize="small"
                  />
                </ListItemIcon>
                <Typography variant="inherit">Desabilitar</Typography>
              </div>
            ) : (
              <div>
                <ListItemIcon>
                  <VisibilityIcon
                    style={{ color: "#2ecc71" }}
                    fontSize="small"
                  />
                </ListItemIcon>
                <Typography variant="inherit">Habilitar</Typography>
              </div>
            )}
          </MenuItem>
          <Divider />
          <MenuItem
            onClick={() => {
              onDelete(product.id);
              handleClose();
            }}
          >
            <ListItemIcon>
              <DeleteIcon style={{ color: "#ff4d4d" }} fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Excluir</Typography>
          </MenuItem>
        </Menu>
      </Box>
      <Divider />
      <CardContent>
        <Box display="flex" justifyContent="center" mb={3}>
          <img
            alt="Product"
            height={150}
            width={(150 * 3) / 4}
            src={
              product.Imageproducts.length > 0
                ? product.Imageproducts[0].path_s3
                : "https://passarelacalcados.vteximg.com.br/arquivos/ids/263443-1000-1000/image-c0c2acb820874abc9642496ace149c06.jpg?v=637407166345700000"
            }
          />
        </Box>
        <Typography
          align="center"
          color="textPrimary"
          gutterBottom
          variant="h6"
        >
          {product.name}
        </Typography>
        <Typography align="center" variant="body2">
          {product.detail}
        </Typography>
      </CardContent>
      <Box flexGrow={1} />
      <Divider />
      <Box p={2}>
        <Grid container justify="space-between" spacing={2}>
          <Grid className={classes.statsItem} item>
            <AccessTimeIcon className={classes.statsIcon} color="action" />
            <Typography color="textSecondary" display="inline" variant="body2">
              {moment().isBefore(
                moment(String(product.date_time_end), "YYYY-MM-DD HH:mm:ss")
              )
                ? `Expira em ${moment(
                    String(product.date_time_start),
                    "YYYY-MM-DD HH:mm:ss"
                  ).from(
                    moment(
                      String(product.date_time_end),
                      "YYYY-MM-DD HH:mm:ss"
                    ),
                    true
                  )}`
                : "Promoção expirou"}
            </Typography>
          </Grid>
          <Grid className={classes.statsItem} item>
            <VisibilityIcon className={classes.statsIcon} color="action" />
            <Typography color="textSecondary" display="inline" variant="body2">
              {0} Views
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
};

ProductCard.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object.isRequired,
};

export default ProductCard;
