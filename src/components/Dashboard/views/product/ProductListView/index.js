import React, { useState, useEffect } from "react";
import {
  Box,
  Container,
  Grid,
  makeStyles,
  Card,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import Page from "../../../components/Page";
import Toolbar from "./Toolbar";
import Result from "./Results";
import ProductCard from "./ProductCard";
import ModalNewProduct from "../Modal";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  productCard: {
    height: "100%"
  }
}));

const ProductList = () => {
  const classes = useStyles();
  const [category, setCategory] = useState([]);
  const [products, setProduct] = useState([]);
  const [ecommerces, setEcommerces] = useState([]);
  const [ecommerceBusca, setEcommerceBusca] = useState([]);
  const [productData, setProductData] = useState([]);
  const [gerationProductList, setGerationProductList] = useState([]);
  const [gerationProductListBusca, setGerationProductListBusca] = useState([]);
  const [showConfirm, setShowConfirm] = useState(false);
  const [showNewProduct, setShowNewProduct] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [idProductModify, setIdProductModify] = useState(0);
  const [idEcommerce, setIdEcommerce] = useState(0);
  const [idBusiness, setIdBusiness] = useState(0);
  const [isProductGeted, setIsProductGeted] = useState(false);
  const [isCategoryGeted, setIsCategoryGeted] = useState(false);

  useEffect(() => {
    if (isProductGeted & isCategoryGeted) {
      const prod = category.map(ct => {
        return {
          ...ct,
          product: products.filter(i => {
            return String(i["categoryId"]) == String(ct.id);
          })
        };
      });

      console.log(prod);
      setGerationProductList(prod);
      setGerationProductListBusca(prod);
      setIsCategoryGeted(false);
      setIsProductGeted(false);
      setIsEditing(false);
    }
  }, [isProductGeted, isCategoryGeted]);

  const getAllCategory = async () => {
    setIsLoading(true);
    const res = await api.get("/category");

    if (res) {
      setCategory(res.data);
      setIsCategoryGeted(true);
    }
  };

  const getAllProduct = async idEcommerce => {
    setIsLoading(true);
    const res = await api.get(`/product/${idEcommerce}`);

    if (res) {
      console.log(res.data);
      setProduct(res.data);
      setIsProductGeted(true);

      setIsCategoryGeted(true);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (idEcommerce > 0) {
      getAllProduct(idEcommerce);
    }
  }, [idEcommerce]);

  useEffect(() => {
    getInfoBusiness();
  }, []);

  const getInfoBusiness = async () => {
    setIsLoading(true);
    const me = await api.get("/login/me");
    const eco = await api.get(`/ecommerce/${me.data.Business.id}`);

    setIdBusiness(me.data.Business.id);
    if (eco.data.length > 0) {
      setIdEcommerce(eco.data[0].id);
    }
    setEcommerces(eco.data);
    setEcommerceBusca(eco.data);
    setIsLoading(false);
  };

  const onSearchProduct = value => {
    let productUpdate = [];
    let resultBusca = gerationProductListBusca.filter(i => {
      return (
        String(i["name"])
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    if (resultBusca.length == 0) {
      let resultBusca2 = gerationProductListBusca.map(i => {
        const productsSearch =
          i["product"].length > 0
            ? i["product"].filter(p => {
                return (
                  String(p.name)
                    .toUpperCase()
                    .indexOf(String(value).toUpperCase()) > -1
                );
              })
            : [];

        if (productsSearch.length > 0) {
          productUpdate.push({ ...i, product: productsSearch });
        }

        //  console.log(i['product'])
        return productsSearch.length > 0 && { ...i, product: productsSearch };
      });
    }

    //console.log(productUpdate)
    if (resultBusca.length == 0) {
      setGerationProductList(productUpdate);
    } else {
      setGerationProductList(resultBusca);
    }
  };

  const onDeleteProduct = async id => {
    setIsEditing(true);
    setIsLoading(true);
    const res = await api.delete(`/product/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getAllProduct();
        onCloseNewProduct();
        showMessage(
          "Produto Deletado",
          "Produto selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Produto Falhou",
        "Houve um problema ao deletar seu produto, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onUpdateProduct = async (value, id) => {
    setIsLoading(true);
    await api
      .put(`/product/edit/${id}`, value)
      .then(res => {
        console.log(res.data);
        if (res.data.message == "success") {
          showMessage(
            "Atualisando Produto",
            "Atualização realizada com sucesso",
            "success"
          );
          getAllProduct();
          onCloseNewProduct();
        } else {
          showMessage(
            "Produto Falhou",
            "Houve um problema ao atualizar seu produto, entre em contato com o suporte.",
            "danger"
          );
        }
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
        setIsLoading(false);
      });
  };

  const onShowConfirm = id => {
    console.log(id);
    setShowConfirm(true);
    setIdProductModify(id);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteProduct(idProductModify);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onShowNewProduct = () => {
    setIsEditing(false);
    setShowNewProduct(true);
  };

  const onShowEditProduct = product => {
    setProductData(product);
    setIsEditing(true);
    setShowNewProduct(true);
  };

  const onCloseNewProduct = () => {
    setShowNewProduct(false);
  };

  useEffect(() => {
    getAllCategory();
  }, []);

  return (
    <Page className={classes.root} title="Produtos">
      <Container maxWidth={false}>
        <Toolbar
          SearchProduct={onSearchProduct}
          onShowNewProduct={onShowNewProduct}
        />
        <Box ml={0} mr={0} mt={4}>
          <FormControl fullWidth variant="outlined">
            <InputLabel id="demo-simple-select-outlined-label">
              E-commerce
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={idEcommerce}
              defaultValue={""}
              onChange={event => {
                setIdEcommerce(event.target.value);
              }}
              label="Selecione o Canal"
            >
              {ecommerces.map(c => (
                <MenuItem key={c.id} value={c.id}>
                  {c.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
        {isLoading && <LinearProgress className="ml-3 mr-3" />}
        <Box mt={3}>
          {gerationProductList.map(
            categorys =>
              categorys.product.length > 0 && (
                <Card style={{ borderRadius: 20 }} className="p-4 mb-4">
                  <h2 className="text-black m-4">
                    {String(categorys.name)
                      .toLowerCase()
                      .replace(/(?:^|\s)\S/g, function(a) {
                        return a.toUpperCase();
                      })}
                  </h2>
                  <Grid container spacing={3}>
                    {categorys.product && (
                      <Result
                        onDeleteProduct={onShowConfirm}
                        onShowModalEditProduct={onShowEditProduct}
                        customers={categorys.product}
                      />
                    )}
                    {/*categorys.product.map(
                      (product) =>
                        product && (
                          <Grid item key={product.id} lg={2} md={6} xs={12}>
                            <ProductCard
                              className={classes.productCard}
                              product={product}
                              onDelete={onShowConfirm}
                              onShowEditProduct={onShowEditProduct}
                            />
                          </Grid>
                        )
                        )*/}
                  </Grid>
                </Card>
              )
          )}
        </Box>
        <Box mt={3} display="flex" justifyContent="center">
          <Pagination color="primary" count={3} size="small" />
        </Box>
      </Container>
      <ModalNewProduct
        open={showNewProduct}
        onCloseNewProduct={onCloseNewProduct}
        onEditProduct={onUpdateProduct}
        getAllProduct={getAllProduct}
        allCategory={category}
        productData={productData}
        idEcommerce={idEcommerce}
        isEditing={isEditing}
        isLoading={isLoading}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação,o Produto será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default ProductList;
