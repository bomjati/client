import React, { useState, useEffect } from "react";
import {
  Box,
  Container,
  makeStyles,
  LinearProgress,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewEmployee from "../ModalNewEmployee";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = ({ levelAccess, channelControls, isAdministrador }) => {
  const classes = useStyles();

  const [showNewEmployee, setShowNewEmployee] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditEmployee, setShowEditEmployee] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [channel, setChannel] = useState([]);
  const [Employee, setEmployee] = useState([]);
  const [EmployeeBusca, setEmployeeBusca] = useState([]);
  const [getEditEmployee, setGetEditEmployee] = useState([]);
  const [idEmployeeModify, setIdEmployeeModify] = useState(0);
  const [idClient, setIdClient] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getChannel();
  }, []);

  useEffect(() => {
    getEmployee();
  }, [idClient]);

  const handleChange = (event) => {
    setIdClient(event.target.value);
  };

  const buscaEmployee = (value) => {
    const resultBusca = EmployeeBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setEmployee(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdEmployeeModify(id);
  };
  const onShowNewEmployee = () => {
    setShowNewEmployee(true);
  };

  const onCloseNewEmployee = () => {
    setShowNewEmployee(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditEmployee = (value) => {
    setShowEditEmployee(true);
    setGetEditEmployee(value);
  };

  const onCloseEditEmployee = () => {
    setShowEditEmployee(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteEmployee(idEmployeeModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getEmployee = async () => {
    setEmployee([]);
    setEmployeeBusca([]);

    setIsLoading(true);
    const res = await api.get(`/Employee/${idClient}`);

    if (res) {
      console.log("chegou");
      console.log(idClient);
      setEmployee(res.data);
      setEmployeeBusca(res.data);
    }

    setIsLoading(false);
  };

  const getChannel = async () => {
    setIsLoading(true);

    api
      .get("/channel")
      .then((response) => response.data)
      .then((res) => {
        if (res) {
          let dataC = [];

          if (isAdministrador.status) {
            dataC = res;
          } else {
            for (let index = 0; index < channelControls.length; index++) {
              const element = channelControls[index];

              dataC.push(res.filter((c) => c.clientId == element.clientId)[0]);
            }
          }

          setChannel(dataC);

          if (dataC.length > 0) {
            setIdClient(dataC[0].id);
          }
        }

        setIsLoading(false);
      });
  };

  const onDeleteEmployee = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Employee/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getEmployee();
        onCloseEditEmployee();
        showMessage(
          "Funcionario Deletado",
          "Funcionario selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Funcionario Falhou",
        "Houve um problema ao deletar seu Funcionario, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Funcionário">
      <Container maxWidth={false}>
        <Toolbar
          levelAccess={levelAccess}
          SearchEmployee={buscaEmployee}
          onShowNewEmployee={onShowNewEmployee}
          isAdministrador={isAdministrador}
        />
        <Box ml={-5} mr={-5} mt={8}>
          <FormControl fullWidth variant="outlined">
            <InputLabel id="demo-simple-select-outlined-label">
              Canal
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={idClient}
              defaultValue={""}
              onChange={handleChange}
              label="Selecione o Canal"
            >
              {channel.map((c) => (
                <MenuItem  key={c.id} value={c.id}>
                  {c.Client.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
        <Box ml={-5} mr={-5} mt={2}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditEmployee={onShowEditEmployee}
            levelAccess={levelAccess}
            isAdministrador={isAdministrador}
            onDeleteEmployee={onShowConfirm}
            customers={Employee}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewEmployee
        open={showNewEmployee}
        onCloseNewEmployee={onCloseNewEmployee}
        getAllEmployee={getEmployee}
        idClient={idClient}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Funcionario será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
