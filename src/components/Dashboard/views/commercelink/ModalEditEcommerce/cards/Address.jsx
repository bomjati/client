import React from "react";
import { Card, Divider, Box, TextField } from "@material-ui/core";

const Info = () => {
  return (
    <Card
      className="mt-2"
      style={{
        borderWidth: 0.5,
        borderStyle: "solid",
        borderColor: "#E8E8E8",
      }}
    >
      <p className="m-4">Informações Gerais</p>
      <Box>
        <TextField
          className="mt-2 mb-2 ml-4 mr-4"
          placeholder="Nome E-commerce"
          variant="outlined"
          style={{
            width: "93%",
          }}
        />

        <TextField
          className="mt-2 mb-4 ml-4 mr-4"
          placeholder="CNPJ"
          variant="outlined"
          style={{
            width: "93%",
          }}
        />
      </Box>
    </Card>
  );
};

export default Info;
