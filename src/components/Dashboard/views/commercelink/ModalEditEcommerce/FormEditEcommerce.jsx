import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  InputAdornment,
  Switch,
  Collapse,
  Checkbox,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import { useCookies } from "react-cookie";
import Results from "./Results";
import { titlesGrid } from "./data";
import Cielo from "./cards/Cielo";
import Info from "./cards/Info";
import Seels from "./cards/Seels";
import Colors from "./cards/Colors";
import Hours from "./cards/Hours";

const Formulario = ({
  onCloseModal,
  getAllEcommerce,
  idClient,
  idBusiness,
  getEditEcommerce,
  channels,
  isMobile,
}) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listEcommerce, setListEcommerce] = useState(null);
  const [value, setValue] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [isViewConfirm, setIsViewConfirm] = useState(false);
  const [isView, setIsView] = useState(false);
  const [planControls, setPlanControls] = useCookies(["PlanControls"]);
  const [modules, setModules] = useState([]);
  const [showCollapse, setShowCollapse] = useState({});
  const [checkEcommerce, setCheckEcommerce] = useState({});
  const [selectId, setSelectId] = useState([]);
  const [confirmPassword, setConfirmPassword] = useState("");
  const [titlesTopGrid] = useState(titlesGrid);
  const [values, setValues] = useState({
    userId: 0,
    name: "",
    cnpj: "",
    addressId: 0,
    deliveryFee: "",
    otherFees: "",
    colorPrimary: "",
    colorSecondary: "",
    hoursOpen: "",
    hoursExit: "",
    paymentintegrationId: 0,
    businessId: idBusiness ? idBusiness : 0,
  });
  const [valuesCielo, setValuesCielo] = useState({
    ecommerceId: 0,
    cieloMerchantId: "",
    cieloMerchantKey: "",
  });
  const styleCard = { borderRadius: 20 };

  useEffect(() => {
    console.log(channels);
  }, [channels]);

  useEffect(() => {
    setModules(planControls.PlanControls.Plan.Modulecontrols);

    planControls.PlanControls.Plan.Modulecontrols.map((module) => {
      setShowCollapse({
        ...showCollapse,
        [module.Module.id]: !showCollapse[String(module.Module.id)]
          ? true
          : false,
      });
    });
  }, planControls);

  useEffect(() => {
    console.log(getEditEcommerce);
    setValues({
      ...values,
      userId: getEditEcommerce.userId,
      name: getEditEcommerce.name,
      cnpj: getEditEcommerce.cnpj,
      addressId: 0,
      deliveryFee: `R$ ${getEditEcommerce.deliveryFee.toFixed(2)}`,
      otherFees: `R$ ${getEditEcommerce.otherFees.toFixed(2)}`,
      colorPrimary: getEditEcommerce.colorPrimary,
      colorSecondary: getEditEcommerce.colorSecondary,
      hoursOpen: getEditEcommerce.hoursOpen,
      hoursExit: getEditEcommerce.hoursExit,
      paymentintegrationId: 0,
      businessId: getEditEcommerce.businessId,
    });

    setValuesCielo({
      ...valuesCielo,
      ecommerceId: getEditEcommerce.id,
      cieloMerchantId: getEditEcommerce.Paymentintegrations[0].cieloMerchantId,
      cieloMerchantKey: getEditEcommerce.Paymentintegrations[0].cieloMerchantKey,
    });
  }, [getEditEcommerce]);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    if (values["name"] == "") {
      showMessage("Atenção", `Campo Nome não preenchido.`, "warning");

      return;
    }

    if (values["cnpj"].length < 18) {
      showMessage(
        "Atenção",
        `Campo CNPJ não preenchido corretamente.`,
        "warning"
      );

      return;
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleChangeCielo = (event) => {
    setValuesCielo({
      ...valuesCielo,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/ecommerce/edit/${getEditEcommerce.id}`, {
        ...values,
        deliveryFee: String(values.deliveryFee)
          .replace("R$", "")
          .replace(".", "")
          .replace(",", ".")
          .trim(),
        otherFees: String(values.otherFees)
          .replace("R$", "")
          .replace(".", "")
          .replace(",", ".")
          .trim(),
      })
      .then((response) => response.data)
      .then((respU) => {
        if (respU.message == "success") {
          api
            .put(
              `/paymentintegration/edit/${getEditEcommerce.Paymentintegrations[0].id}`,
              {
                ...valuesCielo,
              }
            )
            .then((resp) => resp.data)
            .then((res) => {
              if (res.message == "success") {
                showMessage(
                  "E-commerce Editado",
                  `Edição realizada com sucesso.`,
                  "success"
                );
                getAllEcommerce();
                setIsLoading(false);
                onCloseModal();
              }
            });
        } else {
          showMessage("Editar Acesso", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onSelectId = (value) => {
    setSelectId(value);
  };

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20, width: 720 }}>
          <CardHeader
            subheader="Preencha as informações de seu e-commerce"
            title="Editando E-commerce"
          />
          <Divider />
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChangeTab}
              textColor="white"
              variant="fullWidth"
              aria-label="full width tabs example"
            >
              <Tab label="Informações do E-commerce" {...a11yProps(0)} />
              <Tab label="Métodos de Pagamentos" {...a11yProps(1)} />
            </Tabs>
          </AppBar>

          <TabPanel value={value} index={0} dir={theme.direction}>
            <CardContent>
              <Info handleChange={handleChange} values={values} />
              <Seels handleChange={handleChange} values={values} />
              <Hours handleChange={handleChange} values={values} />
              <Colors handleChange={handleChange} values={values} />
            </CardContent>
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <CardContent>
              <Cielo
                handleChangeCielo={handleChangeCielo}
                values={valuesCielo}
              />
            </CardContent>
          </TabPanel>

          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
