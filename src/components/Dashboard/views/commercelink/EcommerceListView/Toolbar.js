import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
  Tooltip,
  Hidden,
  Grid
} from "@material-ui/core";
import { Search as SearchIcon } from "react-feather";
import AddIcon from "@material-ui/icons/AddOutlined";
import { showMessage } from "../../../utils/message";

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({
  onShowNewEcommerce,
  SearchEcommerce,
  levelAccess,
  className,
  isAdministrador,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box mt={3}>
        <Card style={{ borderRadius: 20 }}>
          <CardContent>
            <Hidden smDown>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box width={"50%"}>
                  <TextField
                    fullWidth
                    onChange={(event) => {
                      SearchEcommerce(event.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SvgIcon fontSize="small" color="action">
                            <SearchIcon />
                          </SvgIcon>
                        </InputAdornment>
                      ),
                    }}
                    placeholder="Procurar E-commerce"
                    variant="outlined"
                  />
                </Box>
                <Tooltip title="Novo Ecommerce">
                  <Button
                    onClick={() => {
                      if ((levelAccess > 1) | isAdministrador.status) {
                        onShowNewEcommerce();
                      } else {
                        showMessage(
                          "Acesso Negado",
                          "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
                          "danger"
                        );
                      }
                    }}
                    color="primary"
                    variant="contained"
                    style={{
                      backgroundColor: "white",
                      color: "#eb2f06",
                    }}
                    size="large"
                    startIcon={<AddIcon />}
                  >
                    NOVO E-COMMERCE
                  </Button>
                </Tooltip>
              </Box>
            </Hidden>
            <Hidden mdUp>
              <Grid container spacing={2}>
                <Grid item md={4} xs={12}>
                  <Box
                    display="flex"
                    justifyContent="flex-end"
                    alignItems="center"
                  >
                    <Tooltip title="Novo Ecommerce" arrow>
                      <Button
                        onClick={() => {
                          if ((levelAccess > 1) | isAdministrador.status) {
                            onShowNewEcommerce();
                          } else {
                            showMessage(
                              "Acesso Negado",
                              "Você não tem acesso a essa funcionalidade, entre em contato com seu superior.",
                              "danger"
                            );
                          }
                        }}
                        color="primary"
                        variant="contained"
                        style={{
                          backgroundColor: "white",
                          color: "#eb2f06",
                        }}
                        size="large"
                        startIcon={<AddIcon />}
                      >
                        NOVO E-COMMERCE
                      </Button>
                    </Tooltip>
                  </Box>
                </Grid>
                <Grid item md={8} xs={12}>
                  <TextField
                    fullWidth
                    onChange={(event) => {
                      SearchEcommerce(event.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SvgIcon fontSize="small" color="action">
                            <SearchIcon />
                          </SvgIcon>
                        </InputAdornment>
                      ),
                    }}
                    placeholder="Procurar Acesso"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Hidden>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
