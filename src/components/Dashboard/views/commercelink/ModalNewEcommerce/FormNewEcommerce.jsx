import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  IconButton,
  CircularProgress,
  Tab,
  Tabs,
  AppBar,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import { useCookies } from "react-cookie";
import { titlesGrid } from "./data";
import Cielo from "./cards/Cielo";
import Info from "./cards/Info";
import Seels from "./cards/Seels";
import Colors from "./cards/Colors";
import Hours from "./cards/Hours";

const Formulario = ({
  onCloseModal,
  getAllEcommerce,
  idClient,
  idBusiness,
  channels,
  isMobile,
}) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listEcommerce, setListEcommerce] = useState(null);
  const [value, setValue] = useState(0);
  const [selectId, setSelectId] = useState([]);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [isViewConfirm, setIsViewConfirm] = useState(false);
  const [isView, setIsView] = useState(false);
  const [planControls, setPlanControls] = useCookies(["PlanControls"]);
  const [titlesTopGrid] = useState(titlesGrid);
  const [modules, setModules] = useState([]);
  const [showCollapse, setShowCollapse] = useState({});
  const [checkEcommerce, setCheckEcommerce] = useState({});
  const [confirmPassword, setConfirmPassword] = useState("");
  const [values, setValues] = useState({
    userId: 0,
    name: "",
    cnpj: "",
    addressId: 0,
    deliveryFee: "",
    otherFees: "",
    colorPrimary: "",
    colorSecondary: "",
    hoursOpen: "",
    hoursExit: "",
    paymentintegrationId: 0,
    businessId: idBusiness ? idBusiness : 0,
  });
  const [valuesCielo, setValuesCielo] = useState({
    ecommerceId: 0,
    cieloMerchantId: "",
    cieloMerchantKey: "",
  });

  const styleCard = { borderRadius: 20 };

  useEffect(() => {
    setModules(planControls.PlanControls.Plan.Modulecontrols);

    planControls.PlanControls.Plan.Modulecontrols.map((module) => {
      setShowCollapse({
        ...showCollapse,
        [module.Module.id]: !showCollapse[String(module.Module.id)]
          ? true
          : false,
      });
    });
  }, planControls);

  useEffect(() => {
    console.log(checkEcommerce);
  }, [checkEcommerce]);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "cnpj"
            ? " CNPJ "
            : key == "nome"
            ? " Nome Fantasia "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    if (!(values["password"] == confirmPassword)) {
      showMessage(
        "Atenção",
        `Campo Senha e Confirme sua senha não coecidem.`,
        "warning"
      );

      return;
    }

    if (Object.keys(checkEcommerce).length > 0) {
      let countValue = 0;

      for (const key in checkEcommerce) {
        if (checkEcommerce.hasOwnProperty.call(checkEcommerce, key)) {
          const element = checkEcommerce[key];

          countValue = countValue + element;
        }
      }

      console.log(countValue);

      if (!(countValue > 0)) {
        showMessage(
          "Atenção",
          `Selecione ao menos uma permissão de acesso para esse usuário.`,
          "warning"
        );

        setValue(1);

        return;
      }
    } else {
      showMessage(
        "Atenção",
        `Selecione ao menos uma permissão de acesso para esse usuário.`,
        "warning"
      );

      setValue(1);

      return;
    }

    if (selectId.length == 0) {
      showMessage(
        "Atenção",
        `Selecione ao menos um canal para esse usuário.`,
        "warning"
      );

      setValue(2);

      return;
    }

    onSave();
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const handleChangeCielo = (event) => {
    setValuesCielo({
      ...valuesCielo,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    //validateCampos();
    onSave();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .post("/ecommerce/new", {
        ...values,
        deliveryFee: String(values.deliveryFee)
          .replace("R$", "")
          .replace(".", "")
          .replace(",", ".")
          .trim(),
        otherFees: String(values.otherFees)
          .replace("R$", "")
          .replace(".", "")
          .replace(",", ".")
          .trim(),
      })
      .then((response) => response.data)
      .then((respU) => {
        if (respU.message == "success") {
          api
            .post(`/paymentintegration/new`, {
              ...valuesCielo,
              ecommerceId: respU.data.id,
            })
            .then((resp) => resp.data)
            .then((res) => {
              setIsLoading(false);

              showMessage(
                "Novo E-commerce salvo com sucesso",
                `E-commerce.`,
                "success"
              );
              getAllEcommerce();
              onCloseModal();
            });
        } else {
          showMessage("Novo E-commerce", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20, width: 720 }}>
          <CardHeader
            subheader="Preencha as informações de seu e-commerce"
            title="Novo E-commerce"
          />
          <Divider />
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChangeTab}
              textColor="white"
              variant="fullWidth"
              aria-label="full width tabs example"
            >
              <Tab label="Informações do E-commerce" {...a11yProps(0)} />
              <Tab label="Métodos de Pagamentos" {...a11yProps(1)} />
            </Tabs>
          </AppBar>

          <TabPanel value={value} index={0} dir={theme.direction}>
            <CardContent>
              <Info handleChange={handleChange} values={values} />
              <Seels handleChange={handleChange} values={values} />
              <Hours handleChange={handleChange} values={values} />
              <Colors handleChange={handleChange} values={values} />
            </CardContent>
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <CardContent>
              <Info
                handleChangeCielo={handleChangeCielo}
                values={valuesCielo}
              />
            </CardContent>
          </TabPanel>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
