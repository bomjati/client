import React from "react";
import { Card, Divider, Box, TextField } from "@material-ui/core";

const Info = ({ handleChange, values }) => {
  const formatCnpj = (value) => {
    value = String(value).replace(/\D/g, "");
    value = String(value).replace(/^(\d{2})(\d)/, "$1.$2");
    value = String(value).replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    value = String(value).replace(/\.(\d{3})(\d)/, ".$1/$2");
    value = String(value).replace(/(\d{4})(\d)/, "$1-$2");

    return value;
  };

  return (
    <Card className="mt-2" style={{
      borderWidth: 0.5,
      borderStyle: "solid",
      borderColor: "#E8E8E8",
    }}>
      <p className="m-4">Informações Gerais</p>
      <Box>
        <TextField
          className="mt-2 mb-2 ml-4 mr-4"
          placeholder="Nome E-commerce"
          variant="outlined"
          value={values.name}
          name="name"
          onChange={handleChange}
          style={{
            width: "93%",
          }}
        />

        <TextField
          className="mt-2 mb-4 ml-4 mr-4"
          placeholder="CNPJ"
          variant="outlined"
          value={values.cnpj}
          name="cnpj"
          inputProps={{
            maxLength: 18
          }}
          onChange={(event) => {
            handleChange({
              target: {
                name: event.target.name,
                value: formatCnpj(event.target.value),
              },
            });
          }}
          style={{
            width: "93%",
          }}
        />
      </Box>
    </Card>
  );
};

export default Info;
