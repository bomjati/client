import React from "react";
import { Card, Divider, Box, TextField } from "@material-ui/core";

const Cielo = ({ handleChangeCielo, values }) => {
  const formatCnpj = (value) => {
    value = String(value).replace(/\D/g, "");
    value = String(value).replace(/^(\d{2})(\d)/, "$1.$2");
    value = String(value).replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    value = String(value).replace(/\.(\d{3})(\d)/, ".$1/$2");
    value = String(value).replace(/(\d{4})(\d)/, "$1-$2");

    return value;
  };

  return (
    <Card className="mt-2" style={{
      borderWidth: 0.5,
      borderStyle: "solid",
      borderColor: "#E8E8E8",
    }}>
      <p className="m-4">Informações Cielo</p>
      <Box>
        <TextField
          className="mt-2 mb-2 ml-4 mr-4"
          placeholder="MerchantId"
          variant="outlined"
          value={values.cieloMerchantId}
          name="cieloMerchantId"
          onChange={handleChangeCielo}
          style={{
            width: "93%",
          }}
        />

        <TextField
          className="mt-2 mb-4 ml-4 mr-4"
          placeholder="MerchantKey"
          variant="outlined"
          value={values.cieloMerchantKey}
          name="cieloMerchantKey"
          inputProps={{
            maxLength: 18
          }}
          onChange={handleChangeCielo}
          style={{
            width: "93%",
          }}
        />
      </Box>
    </Card>
  );
};

export default Cielo;
