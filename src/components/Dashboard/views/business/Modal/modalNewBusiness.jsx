import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import NewBusiness from './FormNewBusiness'

const useStyles = makeStyles(({ breakpoints }) => ({
    modal: {
        display: 'flex',
        [breakpoints.up('xs')]: {
            alignItems: 'start',
        },
        [breakpoints.up('sm')]: {
            alignItems: 'center',
        },
        justifyContent: 'center',
        overflow: 'scroll'
    }
}));

const ModalNewBusiness = ({ open, onCloseNewBusiness, getAllBusiness, firstAccess, onDisableFirstAccess }) => {
    const classes = useStyles();

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={!firstAccess && onCloseNewBusiness}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className='mb-4'>
                        <NewBusiness
                            onCloseModal={!firstAccess && onCloseNewBusiness}
                            getAllBusiness={getAllBusiness}
                            onDisableFirstAccess={onDisableFirstAccess} />
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

export default ModalNewBusiness;