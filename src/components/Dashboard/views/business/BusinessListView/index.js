import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewBusiness from "../Modal";
import ModalEditBusiness from "../ModalEdit";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditBusiness, setShowEditBusiness] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [business, setBusiness] = useState([]);
  const [businessBusca, setBusinessBusca] = useState([]);
  const [getEditBusiness, setGetEditBusiness] = useState([]);
  const [idBusinessModify, setIdBusinessModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getBusiness();
  }, []);

  useEffect(() => {
    if (firstAccess) {
      setShowNewBusiness(true);
    } else {
      setShowNewBusiness(false);
    }
  }, [firstAccess]);

  const buscaBusiness = (value) => {
    const resultBusca = businessBusca.filter((i) => {
      return (
        String(i["business_name"])
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    setBusiness(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdBusinessModify(id);
  };
  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditBusiness = (value) => {
    setShowEditBusiness(true);
    setGetEditBusiness(value);
  };

  const onCloseEditBusiness = () => {
    setShowEditBusiness(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteBusiness(idBusinessModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getBusiness = async () => {
    setIsLoading(true);
    const res = await api.get("/business");

    if (res) {
      setBusiness(res.data);
      setBusinessBusca(res.data);

      if (res.data.length == 0) {
        setFirstAccess(true);
      }
    }
    setIsLoading(false);
  };

  const onUpdateBusiness = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/business/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Loja Atualisada",
          "Loja selecionada foi atualisada com sucesso.",
          "success"
        );
        getBusiness();
        onCloseEditBusiness();
      }
    } else {
      showMessage(
        "Loja Falhou",
        "Houve um problema ao atualizar sua loja, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteBusiness = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/business/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getBusiness();
        onCloseEditBusiness();
        showMessage(
          "Loja Deletada",
          "Loja selecionada foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Loja Falhou",
        "Houve um problema ao deletar sua loja, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Lojas">
      <Container maxWidth={false}>
        <Toolbar
          SearchBusiness={buscaBusiness}
          onShowNewBusiness={onShowNewBusiness}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditBusiness={onShowEditBusiness}
            onDeleteBusiness={onShowConfirm}
            customers={business}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewBusiness
        open={showNewBusiness}
        firstAccess={firstAccess}
        onCloseNewBusiness={onCloseNewBusiness}
        onDisableFirstAccess={onDisableFirstAccess}
        getAllBusiness={getBusiness}
      />
      <ModalEditBusiness
        getEditBusiness={getEditBusiness}
        open={showEditBusiness}
        isLoading={isLoading}
        onCloseEditBusiness={onCloseEditBusiness}
        onDeleteBusiness={onDeleteBusiness}
        onUpdateBusiness={onUpdateBusiness}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a Loja será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
