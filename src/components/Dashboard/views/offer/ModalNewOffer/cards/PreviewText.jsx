import React from "react";
import {
  Card,
  Divider,
  Box,
  TextField,
  Typography,
  InputBase,
  List,
} from "@material-ui/core";

const PreviewText = ({ textComplet, onChange }) => {
  return (
    <>
      <InputBase
        classes={{
          input: { textAlign: "center" }
        }}
        multiline
        fullWidth
        value={textComplet}
        onChange={onChange}
      />
    </>
  );
};

export default PreviewText;
