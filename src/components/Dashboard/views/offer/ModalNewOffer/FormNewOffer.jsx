import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  IconButton,
  CircularProgress,
  Tab,
  Tabs,
  AppBar,
  Typography,
  TextField,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
  Add as AddIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import { useCookies } from "react-cookie";
import { titlesGrid } from "./data";
import PreviewText from "./cards/PreviewText";
import { values } from "./data";
const Formulario = ({
  onCloseModal,
  getAllEcommerce,
  idClient,
  idBusiness,
  channels,
  isMobile,
}) => {
  String.prototype.capitalize = function () {
    return this.toLowerCase().replace(/(?:^|\s)\S/g, function (a) {
      return a.toUpperCase();
    });
  };

  let history = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [value, setValue] = useState(0);
  const [intro_first, setIntro_first] = useState("");
  const [nome_cliente, setNome_cliente] = useState("");
  const [intro_price_1, setIntro_price_1] = useState("");
  const [intro_price_next, setIntro_price_next] = useState("");
  const [slogan, setSlogan] = useState("");
  const [isWeek, setIsWeek] = useState(false);
  const [weekSelect, setWeekSelect] = useState(false);
  const [isEnfase, setIsEnfase] = useState(false);
  const [nameWeek, setNameWeek] = useState("");
  const [textComplet, setTextComplet] = useState("");
  const [audioUrl, setAudioUrl] = useState("");
  const [products, setProducts] = useState([
    {
      name: "",
      type: "",
      price: "",
      enfase: false,
    },
  ]);

  const weekList = [
    "Segunda",
    "Terça",
    "Quarta",
    "Quinta",
    "Sexta",
    "Sábado",
    "Domingo",
  ];

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  const styleCard = {
    borderWidth: 0.1,
    borderStyle: "solid",
    borderColor: "#f2f0f0",
    borderRadius: 10,
  };

  const styleCardInput = {
    borderRadius: 5,
  };

  useEffect(() => {
    if ((value > 0) & (products[0].name == "")) {
      showMessage(
        "Atenção",
        "Preencha ao menos um produto antes de avançar",
        "warning"
      );
      setValue(0);
    }
  }, [value]);

  const getRandomArbitrary = (min, max) => {
    return Math.random() * (max - min) + min;
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const onUpdate = () => {};

  const formatCurrencyBRL = (value) => {
    var str = value.replace(/\D/g, "");

    str = str / 100;
    str = str.toLocaleString("pt-br", { style: "currency", currency: "BRL" });
    return str;
  };

  const gerarAudio = () => {
    setAudioUrl("");
    setIsLoading(true);
    api
      .post("ibm/speech/azure/calls/offer", {
        voice: "fem",
        nTrilha: "1",
        text: String(textComplet).replaceAll(".", ". ").replaceAll("\n", " "),
      })
      .then((resp) => resp.data)
      .then((res) => {
        setIsLoading(false);
        if (res.message == "success") {
          setAudioUrl(res.data.path_s3);
        }
      });
  };

  const onGeraText = () => {
    var ListProd = "";

    if (products[0].name == "") return "";

    const criaListProducts = () => {
      for (let index = 0; index < products.length; index++) {
        const p = products[index];

        let IP1 = `${
          index == 0
            ? values.intro_price_1[
                parseInt(getRandomArbitrary(0, values.intro_price_1.length))
              ] + ".\n\n\n"
            : ""
        }`;
        let NE = `${p.name} ${
          p.enfase
            ? values.enfase[
                parseInt(getRandomArbitrary(0, values.enfase.length))
              ]
            : ""
        } ${p.price}.\n`;

        let NextProduct = `${
          (index !== 0) & ((index % 2 !== 0) & (products.length - 1 !== index))
            ? values.habilita[
                parseInt(getRandomArbitrary(0, values.habilita.length))
              ]
              ? `.\n\n${
                  values.habilita[
                    parseInt(getRandomArbitrary(0, values.habilita.length))
                  ]
                    ? nome_cliente + ".\n"
                    : ""
                }${
                  values.intro_price_next[
                    parseInt(
                      getRandomArbitrary(0, values.intro_price_next.length)
                    )
                  ]
                }.\n\n`
              : `\n\n${
                  values.intro_price_next[
                    parseInt(
                      getRandomArbitrary(0, values.intro_price_next.length)
                    )
                  ]
                }.\n
          ${
            values.habilita[
              parseInt(getRandomArbitrary(0, values.habilita.length))
            ]
              ? nome_cliente + ".\n\n"
              : ""
          }`
            : ""
        }`;

        ListProd = ListProd + IP1 + NE + NextProduct;
      }

      return ListProd;
    };

    const textComplets = `${
      values.habilita[parseInt(getRandomArbitrary(0, values.habilita.length))]
        ? values.nome_cliente +
          ".\n\n" +
          values.intro_first[
            parseInt(getRandomArbitrary(0, values.intro_first.length))
          ] +
          ".\n\n"
        : values.intro_first[
            parseInt(getRandomArbitrary(0, values.intro_first.length))
          ] +
          ".\n\n" +
          values.nome_cliente +
          ".\n\n"
    }${
      isWeek
        ? `${
            weekSelect[weekSelect.length - 1] == "a" ? "Na" : "No"
          } ${weekSelect} ${nameWeek}.\n\n`
        : ""
    }${criaListProducts()}\n\n${values.nome_cliente}.\n\n${values.slogan}.\n\n
  `;

    setTextComplet(textComplets);
  };

  const headleTextPreview = (event) => {
    setTextComplet(event.target.value);
  };

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyOffer="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20, width: 720 }}>
          <CardHeader
            subheader="Preencha as informações"
            title="Crie seu Modelo"
          />
          <Divider />
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChangeTab}
              textColor="white"
              variant="fullWidth"
              aria-label="full width tabs example"
            >
              <Tab label="Informações da Oferta" {...a11yProps(0)} />
              <Tab label="Preview" {...a11yProps(1)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0} dir={theme.direction}>
            <CardContent>
              <Card style={styleCard} className="p-4">
                <Box>
                  <Typography
                    className="ml-3"
                    variant="h5"
                    style={{ fontWeight: "bold" }}
                  >
                    Produtos
                  </Typography>
                  <Typography className="ml-3">
                    Adicione seus produtos em sua oferta
                  </Typography>
                </Box>
                <CardContent>
                  {products.map((product, index) => {
                    return (
                      <Box display="flex" mt={2}>
                        <Box display="flex">
                          <TextField
                            onChange={(event) => {
                              let p = products.filter((pr, i) => i > -1);

                              p[index].name = String(
                                event.target.value
                              ).capitalize();
                              setProducts(p);
                            }}
                            value={products[index].name}
                            style={{ ...styleCardInput, width: "75%" }}
                            className="mr-2"
                            variant="outlined"
                            label="Produto"
                            placeholder="Arroz"
                          />
                          <TextField
                            onChange={(event) => {
                              let p = products.filter((pr, i) => i > -1);

                              p[index].price = formatCurrencyBRL(
                                event.target.value
                              );
                              setProducts(p);
                            }}
                            value={products[index].price}
                            style={{ ...styleCardInput, width: "23%" }}
                            className="mr-2"
                            variant="outlined"
                            label="Preço"
                            placeholder="R$ 23,00"
                          />
                        </Box>
                        {index == products.length - 1 ? (
                          <IconButton
                            style={{ backgroundColor: "white" }}
                            onClick={() => {
                              let p = products.filter((pr, i) => i > -1);

                              p.push({
                                name: "",
                                type: "",
                                price: "",
                                enfase: false,
                              });

                              setProducts(p);
                            }}
                          >
                            <AddIcon
                              style={{
                                color: "#2ecc71",
                              }}
                            />
                          </IconButton>
                        ) : (
                          <IconButton
                            disabled={true}
                            style={{ backgroundColor: "white" }}
                          >
                            <AddIcon
                              style={{
                                color: "white",
                              }}
                            />
                          </IconButton>
                        )}
                      </Box>
                    );
                  })}
                </CardContent>
              </Card>
            </CardContent>
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <CardContent>
              <Box>
                {!isWeek ? (
                  <Box>
                    <Typography className="mb-2 ">
                      Adicione nome expecifico para oferta do dia da semana.
                      <Typography>Ex: "Segunda abaixa estoque"</Typography>
                    </Typography>

                    <Button
                      fullWidth
                      onClick={() => {
                        setIsWeek(true);
                      }}
                      style={{
                        backgroundColor: "#8e44ad",
                        color: "white",
                        borderRadius: 20,
                        marginBottom: 10,
                      }}
                      variant="contained"
                      size="small"
                    >
                      Dia da semana
                    </Button>
                  </Box>
                ) : (
                  <Box
                    style={{
                      margin: 10,
                    }}
                  >
                    <Box
                      display="flex"
                      flexDirection="row"
                      justifyOffer="flex-end"
                    >
                      <IconButton
                        onClick={() => {
                          setIsWeek(false);
                          setWeekSelect("");
                        }}
                        size="small"
                        style={{
                          ...styleButtonIcon,
                          backgroundColor: "white",
                          marginBottom: -14,
                          marginRight: -5,
                        }}
                      >
                        <CloseIcon
                          fontSize="small"
                          style={{
                            color: "red",
                          }}
                        />
                      </IconButton>
                    </Box>
                    <Box
                      style={{
                        ...styleButtonIcon,
                        paddingBottom: 30,
                        borderRadius: 20,
                      }}
                    >
                      <Button
                        onClick={() => {
                          setIsWeek(true);
                        }}
                        fullWidth
                        style={{
                          backgroundColor: "#8e44ad",
                          color: "white",
                          borderRadius: 20,
                          marginBottom: 10,
                        }}
                        variant="contained"
                        size="small"
                      >
                        Dia da semana
                      </Button>
                      <Box
                        display="flex"
                        flexDirection="row"
                        flexWrap="wrap"
                        pl={2}
                        pr={2}
                      >
                        {weekList.map((week) => {
                          return (
                            <Button
                              onClick={() => {
                                setWeekSelect(week);
                              }}
                              style={{
                                borderRadius: 20,
                                marginLeft: 10,
                                marginTop: 10,
                                backgroundColor:
                                  weekSelect == week ? "#e74c3c" : "",
                                color: weekSelect == week ? "white" : "",
                              }}
                              variant="contained"
                              size="small"
                            >
                              {week}
                            </Button>
                          );
                        })}
                      </Box>

                      {weekSelect && (
                        <TextField
                          style={{
                            width: "90%",
                          }}
                          onChange={(e) => {
                            setNameWeek(e.target.value);
                          }}
                          value={nameWeek}
                          className="mt-4 ml-4"
                          fullWidth
                          variant="outlined"
                          label="Nome da oferta"
                        />
                      )}
                    </Box>
                  </Box>
                )}

                {!isEnfase ? (
                  <Box>
                    <Typography className="mb-2 mt-4">
                      Adicione enfase em um produto especifico.
                    </Typography>
                    <Button
                      fullWidth
                      onClick={() => {
                        let enf = products.map((p, index) =>
                          index > -1 ? { ...p, enfase: true } : null
                        );

                        setProducts(enf);
                        setIsEnfase(true);
                      }}
                      style={{
                        backgroundColor: "#e67e22",
                        color: "white",
                        borderRadius: 20,
                      }}
                      variant="contained"
                      size="small"
                    >
                      Enfaze no Produto
                    </Button>
                  </Box>
                ) : (
                  <Box
                    style={{
                      margin: 10,
                    }}
                  >
                    <Box
                      display="flex"
                      flexDirection="row"
                      justifyOffer="flex-end"
                    >
                      <IconButton
                        onClick={() => {
                          let enf = products.map((p, index) =>
                            index > -1 ? { ...p, enfase: false } : null
                          );

                          setProducts(enf);
                          setIsEnfase(false);
                        }}
                        size="small"
                        style={{
                          ...styleButtonIcon,
                          backgroundColor: "white",
                          marginBottom: -14,
                          marginRight: -5,
                        }}
                      >
                        <CloseIcon
                          fontSize="small"
                          style={{
                            color: "red",
                          }}
                        />
                      </IconButton>
                    </Box>
                    <Box
                      style={{
                        ...styleButtonIcon,
                        paddingBottom: 10,
                        borderRadius: 5,
                        paddingLeft: 10,
                        paddingRight: 10,
                      }}
                    >
                      <Button
                        fullWidth
                        style={{
                          backgroundColor: "#e67e22",
                          color: "white",
                          borderRadius: 20,
                          marginTop: 20,
                          marginBottom: 10,
                        }}
                        variant="contained"
                        size="small"
                      >
                        Enfaze no Produto
                      </Button>
                      <Box>
                        {products.map((product, index) => {
                          return (
                            <Box display="flex" flexDirection="row">
                              <FormControlLabel
                                control={
                                  <Checkbox
                                    checked={product.enfase}
                                    onChange={(event) => {
                                      let p = products.filter(
                                        (pr, i) => i > -1
                                      );

                                      p[index].enfase = event.target.checked;
                                      setProducts(p);
                                    }}
                                    name="checkedB"
                                    color="primary"
                                  />
                                }
                                label={product.name}
                              />
                            </Box>
                          );
                        })}
                      </Box>
                    </Box>
                  </Box>
                )}
              </Box>
            </CardContent>
          </TabPanel>
          <TabPanel value={value} index={2} dir={theme.direction}>
            <CardContent>
              {textComplet == "" ? (
                <Box display="flex" flexDirection="row" justifyOffer="center">
                  <Button
                    onClick={() => onGeraText()}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#10ac84",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    startIcon={<CheckIcon style={{ color: "white" }} />}
                  >
                    Gerar Modelo
                  </Button>
                </Box>
              ) : (
                <Box>
                  <PreviewText
                    textComplet={textComplet}
                    onChange={headleTextPreview}
                  />
                </Box>
              )}
              {audioUrl !== "" && <audio controls src={audioUrl} />}
            </CardContent>
          </TabPanel>
          <Divider />

          <Box
            display="flex"
            justifyOffer="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            {value > 0 && (
              <Button
                onClick={() => setValue(value - 1)}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "white",
                  color: "black",
                  borderWidth: 1,
                  borderColor: "silver",
                  borderStyle: "solid",
                  marginRight: 10,
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<CheckIcon style={{ color: "black" }} />}
              >
                Voltar
              </Button>
            )}

            {(textComplet !== "") & (value > 0) ? (
              <Button
                onClick={() => onGeraText()}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#3D7AFD",
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<CheckIcon style={{ color: "white" }} />}
              >
                Atualizar Modelo
              </Button>
            ) : null}
            {(textComplet !== "") & (value > 0) ? (
              <Button
                onClick={() => gerarAudio()}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#1F0954",
                  marginLeft: 10,
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<CheckIcon style={{ color: "white" }} />}
              >
                Gravar Audio
              </Button>
            ) : null}
            {value < 2 && (
              <Button
                onClick={() => setValue(value + 1)}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#3498db",
                  marginLeft: 10,
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<CheckIcon style={{ color: "white" }} />}
              >
                Avançar
              </Button>
            )}
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
