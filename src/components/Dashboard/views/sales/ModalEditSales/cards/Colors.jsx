import React from "react";
import { Card, Divider, Box, TextField, Typography } from "@material-ui/core";
import { ChromePicker } from "react-color";

const Colors = ({ handleChange, values }) => {
  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };
  return (
    <Card className="mt-2" style={{
      borderWidth: 0.5,
      borderStyle: "solid",
      borderColor: "#E8E8E8",
    }}>
      <p className="m-4">Informações de Cores</p>
      <Box display="flex" flexDirection="row">
        <div
          style={{
            margin: 20,
          }}
        >
          {/* Color Frases */}
          <Typography
            style={{ fontWeight: "bold" }}
            variant="body1"
            className="text-center"
          >
            Cor principal
          </Typography>
          <Card
            style={{
              ...styleButtonIcon,
              backgroundColor: values.colorPrimary,
              height: 40,
              width: 225,
              marginBottom: 10,
            }}
          />
          
          <ChromePicker
            color={values.colorPrimary}
            onChange={(color, event) => {
              handleChange({
                target: {
                  name: "colorPrimary",
                  value: color.hex,
                },
              });
            }}
          />
        </div>

        <div
          style={{
            margin: 20,
          }}
        >
          {/* Color Frases */}
          <Typography
            style={{ fontWeight: "bold" }}
            variant="body1"
            className="text-center"
          >
            Cor secundaria
          </Typography>
          <Card
            style={{
              ...styleButtonIcon,
              backgroundColor: values.colorSecondary,
              height: 40,
              width: 225,
              marginBottom: 10,
            }}
          />

          <ChromePicker
            color={values.colorSecondary}
            onChange={(color, event) => {
              handleChange({
                target: {
                  name: "colorSecondary",
                  value: color.hex,
                },
              });
            }}
          />
        </div>
      </Box>
    </Card>
  );
};

export default Colors;
