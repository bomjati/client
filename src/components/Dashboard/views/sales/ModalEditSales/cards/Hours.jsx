import React from "react";
import { Card, Divider, Box, TextField } from "@material-ui/core";
import moment from "moment";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { AccessTime as AccessTimeIcon } from "@material-ui/icons";

const Hours = ({ handleChange, values }) => {
  return (
    <Card
      className="mt-2"
      style={{
        borderWidth: 0.5,
        borderStyle: "solid",
        borderColor: "#E8E8E8",
      }}
    >
      <p className="m-4">Informações de Horários</p>
      <Box display="flex" flexDirection="row" flexWrap marginBottom={4}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardTimePicker
            id="time-picker"
            ampm={false}
            inputVariant="outlined"
            label="Time picker"
            name="startHour"
            label="Hora Inicio"
            value={moment(
              `${moment().format("YYYY-MM-DD")} ${values.hoursOpen}`,
              "YYYY-MM-DD HH:mm"
            )}
            onChange={(event) => {
              handleChange({
                target: {
                  name: "hoursOpen",
                  value: moment(event).format("HH:mm"),
                },
              });
            }}
            style={{ width: 125, marginRight: 40, marginLeft: 20 }}
            KeyboardButtonProps={{
              "aria-label": "change time",
            }}
            keyboardIcon={<AccessTimeIcon />}
          />
        </MuiPickersUtilsProvider>

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardTimePicker
            id="time-picker"
            ampm={false}
            inputVariant="outlined"
            label="Time picker"
            name="startHour"
            label="Hora Inicio"
            value={moment(
              `${moment().format("YYYY-MM-DD")} ${values.hoursExit}`,
              "YYYY-MM-DD HH:mm"
            )}
            onChange={(event) => {
              handleChange({
                target: {
                  name: "hoursExit",
                  value: moment(event).format("HH:mm"),
                },
              });
            }}
            style={{ width: 125, marginRight: 10 }}
            KeyboardButtonProps={{
              "aria-label": "change time",
            }}
            keyboardIcon={<AccessTimeIcon />}
          />
        </MuiPickersUtilsProvider>
      </Box>
    </Card>
  );
};

export default Hours;
