import React from "react";
import { Card, Divider, Box, TextField } from "@material-ui/core";

const Seels = ({ handleChange, values }) => {
  const formatValuesCurrency = (value) => {
    var number = String(value).replace(/\D/g, "");

    number = number > 0 ? number / 100 : 0.0;
    return number.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
  };

  return (
    <Card className="mt-2" style={{
      borderWidth: 0.5,
      borderStyle: "solid",
      borderColor: "#E8E8E8",
    }}>
      <p className="m-4">Informações de Taxas</p>
      <Box display="flex" flexDirection="row">
        <TextField
          className="mt-2 mb-2 ml-4 mr-4"
          placeholder="Taxa de Entrega"
          variant="outlined"
          name="deliveryFee"
          value={values.deliveryFee}
          onChange={(event) => {
            handleChange({
              target: {
                name: event.target.name,
                value: formatValuesCurrency(event.target.value),
              },
            });
          }}
          style={{
            width: "45%",
          }}
        />

        <TextField
          className="mt-2 mb-4 ml-4 mr-4"
          placeholder="Outras Taxas"
          variant="outlined"
          name="otherFees"
          value={values.otherFees}
          onChange={(event) => {
            handleChange({
              target: {
                name: event.target.name,
                value: formatValuesCurrency(event.target.value),
              },
            });
          }}
          style={{
            width: "45%",
          }}
        />
      </Box>
    </Card>
  );
};

export default Seels;
