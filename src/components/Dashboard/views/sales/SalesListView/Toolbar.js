import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
  Tooltip,
  Hidden,
  Grid,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
import { Search as SearchIcon } from "react-feather";
import AddIcon from "@material-ui/icons/AddOutlined";
import { showMessage } from "../../../utils/message";

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({
  onShowNewEcommerce,
  SearchEcommerce,
  levelAccess,
  className,
  isAdministrador,
  filter,
  handleChangeFilter,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box mt={3}>
        <Card style={{ borderRadius: 20 }}>
          <CardContent>
            <Hidden smDown>
              <Box>
                <Box mb={2} width={"50%"}>
                  <TextField
                    fullWidth
                    onChange={(event) => {
                      SearchEcommerce(event.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SvgIcon fontSize="small" color="action">
                            <SearchIcon />
                          </SvgIcon>
                        </InputAdornment>
                      ),
                    }}
                    placeholder="Procurar Pedido"
                    variant="outlined"
                  />
                </Box>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Filtrar</FormLabel>
                  <RadioGroup
                    row
                    aria-label="gender"
                    name="gender1"
                    value={filter}
                    onChange={handleChangeFilter}
                  >
                    <FormControlLabel
                      value={0}
                      control={<Radio />}
                      label="Todos"
                    />
                    <FormControlLabel
                      value={1}
                      control={<Radio />}
                      label="Pagamento pendente"
                    />
                    <FormControlLabel
                      value={2}
                      control={<Radio />}
                      label="Pagamento aprovado"
                    />
                    <FormControlLabel
                      value={3}
                      control={<Radio />}
                      label="Separando produtos"
                    />
                    <FormControlLabel
                      value={4}
                      control={<Radio />}
                      label="Saiu para entrega"
                    />
                    <FormControlLabel
                      value={5}
                      control={<Radio />}
                      label="Finalizado"
                    />
                  </RadioGroup>
                </FormControl>
              </Box>
            </Hidden>
            <Hidden mdUp>
              <Grid container spacing={2}>
                <Grid item md={4} xs={12}>
                  <FormControl component="fieldset">
                    <FormLabel component="legend">Filtrar</FormLabel>
                    <RadioGroup
                      row
                      aria-label="gender"
                      name="gender1"
                      value={filter}
                      onChange={handleChangeFilter}
                    >
                      <FormControlLabel
                        value={0}
                        control={<Radio />}
                        label="Todos"
                      />
                      <FormControlLabel
                        value={1}
                        control={<Radio />}
                        label="Pagamento pendente"
                      />
                      <FormControlLabel
                        value={2}
                        control={<Radio />}
                        label="Pagamento aprovado"
                      />
                      <FormControlLabel
                        value={3}
                        control={<Radio />}
                        label="Separando produtos"
                      />
                      <FormControlLabel
                        value={4}
                        control={<Radio />}
                        label="Saiu para entrega"
                      />
                      <FormControlLabel
                        value={5}
                        control={<Radio />}
                        label="Finalizado"
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                <Grid item md={8} xs={12}>
                  <TextField
                    fullWidth
                    onChange={(event) => {
                      SearchEcommerce(event.target.value);
                    }}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SvgIcon fontSize="small" color="action">
                            <SearchIcon />
                          </SvgIcon>
                        </InputAdornment>
                      ),
                    }}
                    placeholder="Procurar Pedido"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Hidden>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
