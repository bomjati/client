import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Tooltip,
} from "@material-ui/core";
import getInitials from "../../../utils/getInitials";
import { showMessage } from "../../../utils/message";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Settings as SettingsIcon,
  FilterNone as FilterNoneIcon,
  AddShoppingCart as AddShoppingCartIcon,
  LocalShipping as LocalShippingIcon,
  Beenhere as BeenhereIcon,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

function copyTextToClipboard(text) {
  var textArea = document.createElement("textarea");

  //
  // *** This styling is an extra step which is likely not required. ***
  //
  // Why is it here? To ensure:
  // 1. the element is able to have focus and selection.
  // 2. if the element was to flash render it has minimal visual impact.
  // 3. less flakyness with selection and copying which **might** occur if
  //    the textarea element is not visible.
  //
  // The likelihood is the element won't even render, not even a
  // flash, so some of these are just precautions. However in
  // Internet Explorer the element is visible whilst the popup
  // box asking the user for permission for the web page to
  // copy to the clipboard.
  //

  // Place in the top-left corner of screen regardless of scroll position.
  textArea.style.position = "fixed";
  textArea.style.top = 0;
  textArea.style.left = 0;

  // Ensure it has a small width and height. Setting to 1px / 1em
  // doesn't work as this gives a negative w/h on some browsers.
  textArea.style.width = "2em";
  textArea.style.height = "2em";

  // We don't need padding, reducing the size if it does flash render.
  textArea.style.padding = 0;

  // Clean up any borders.
  textArea.style.border = "none";
  textArea.style.outline = "none";
  textArea.style.boxShadow = "none";

  // Avoid flash of the white box if rendered for any reason.
  textArea.style.background = "transparent";

  textArea.value = text;

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand("copy");
    var msg = successful ? "successful" : "unsuccessful";
    console.log("Copying text command was " + msg);
  } catch (err) {
    console.log("Oops, unable to copy");
  }

  document.body.removeChild(textArea);
}

const Results = ({
  onDeleteEcommerce,
  onShowModalSettingEcommerce,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  levelAccess,
  isAdministrador,
  onShipping,
  onFinish,
  filter,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  useEffect(() => {
    console.log(customers);
  }, [customers]);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  const formatTel = (str) => {
    return String(str).replace(/\D/g, "").length == 11
      ? String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d)(\d{4})(\d{4})$/, "($1) $2 $3-$4")
      : String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d{4})(\d{4})$/, "($1) $2-$3");
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={750}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell />
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {customers &&
                customers.length > 0 &&
                customers
                  .filter((customer) =>
                    filter == 0
                      ? customer
                      : customer.status == filter
                      ? customer
                      : null
                  )
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <TableRow
                      hover
                      key={customer.id}
                      selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={
                            selectedCustomerIds.indexOf(customer.id) !== -1
                          }
                          onChange={(event) =>
                            handleSelectOne(event, customer.id)
                          }
                          value="true"
                        />
                      </TableCell>
                      <TableCell align="left">
                        <Tooltip
                          title={
                            customer.status == 1
                              ? "Pagamento Pendente"
                              : customer.status == 2
                              ? "Pagamento Aprovado"
                              : customer.status == 3
                              ? "Produtos sendo Separado"
                              : customer.status == 4
                              ? "Saiu Para Entrega"
                              : customer.status == 5
                              ? "Finalizado"
                              : "Indefinido"
                          }
                          arrow
                        >
                          <Avatar
                            style={{
                              backgroundColor:
                                customer.status == 1
                                  ? "#d35400"
                                  : customer.status == 2
                                  ? "#f1c40f"
                                  : customer.status == 3
                                  ? "#3498db"
                                  : customer.status == 4
                                  ? "#8e44ad"
                                  : customer.status == 5
                                  ? "#2ecc71"
                                  : "I",
                            }}
                          >
                            {customer.status == 1
                              ? "PP"
                              : customer.status == 2
                              ? "PA"
                              : customer.status == 3
                              ? "PSS"
                              : customer.status == 4
                              ? "SPE"
                              : customer.status == 5
                              ? "F"
                              : "I"}
                          </Avatar>
                        </Tooltip>
                      </TableCell>
                      <TableCell align="left">{customer.User.name}</TableCell>
                      <TableCell align="left">R$ {customer.total}</TableCell>
                      <TableCell align="left">
                        {customer.Address.publicPlace},{" "}
                        {customer.Address.district}, {customer.Address.number} -{" "}
                        {customer.Address.city} - {customer.Address.uf}
                      </TableCell>
                      <TableCell align="right">
                        {customer.status == 4 && (
                          <Tooltip title="Finalizar" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onFinish(customer.id);
                              }}
                              color="inherit"
                            >
                              <BeenhereIcon style={{ color: "#2ecc71" }} />
                            </IconButton>
                          </Tooltip>
                        )}
                        {customer.status == 3 && (
                          <Tooltip title="Entregar Compra" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onShipping(customer.id);
                              }}
                              color="inherit"
                            >
                              <LocalShippingIcon style={{ color: "#8e44ad" }} />
                            </IconButton>
                          </Tooltip>
                        )}
                        {customer.status < 4 && (
                          <Tooltip title="Separar Pedido" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onShowModalSettingEcommerce(customer);
                              }}
                              color="inherit"
                            >
                              <AddShoppingCartIcon
                                style={{ color: "#00a8ff" }}
                              />
                            </IconButton>
                          </Tooltip>
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
