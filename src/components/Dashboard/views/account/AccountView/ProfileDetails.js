import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";
import { UFs } from "../../../utils/uf";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProfileDetails = ({ getAllUserMe, userMe, className, ...rest }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    registration_number: "",
    business_name: "",
    fantasy_name: "",
  });

  const onGetUserMe = () => {
    setValues({
      ...values,
      ["registration_number"]: userMe.registration_number,
      ["business_name"]: userMe.business_name,
      ["fantasy_name"]: userMe.fantasy_name,
    });
  };

  // pega inforamações do usuario que vem do arquivo index.js
  useEffect(() => {
    if (!(userMe == "")) {
      onGetUserMe();
    }
  }, [userMe]);

  const validateCampos = () => {
    for (var key in values) {
      if (key != "complement") {
        if (key != "phone2") {
          if ((values[key] == "") | (String(values[key]) == "null")) {
            let campoRecused =
              key == "registration_number"
                ? " CNPJ "
                : key == "business_name"
                ? " Rasão Social "
                : key == "fantasy_name"
                ? " Nome Fantasia "
                : "Indefinido";

            showMessage(
              "Atenção",
              `Campo ${campoRecused} não foi preenchido.`,
              "warning"
            );
            return;
          }
        }
      }
    }

    onUpdateBusiness(values, userMe.id);
  };

  const onUpdateBusiness = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/business/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Informações Atualisadas",
          "Informações selecionada foi atualisada com sucesso.",
          "success"
        );
        getAllUserMe();
      }
    } else {
      showMessage(
        "Informações Falhou",
        "Houve um problema ao atualizar suas Informações, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  // onChange dos campos do form
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20 }}>
        <CardHeader subheader="Suas informações para edição" title="Básico" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            {/* campo responsivo | nome usuario */}
            <Grid item md={6} xs={12}>
              <TextField
                value={values.registration_number}
                disabled={true}
                fullWidth
                label="CNPJ | CPF"
                name="registration_number"
                onChange={handleChange}
                required
                value={values.registration_number}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | fantasy_name */}
            <Grid item md={12} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Nome Fantasia | Nome"
                name="fantasy_name"
                onChange={handleChange}
                required
                value={values.fantasy_name}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | business_name */}
            {!(values.registration_number.length > 11) ? null : (
              <Grid item md={12} xs={12}>
                <TextField
                  fullWidth
                  disabled={isLoading}
                  label="Rasão Social"
                  name="business_name"
                  onChange={handleChange}
                  required
                  value={values.business_name}
                  variant="outlined"
                />
              </Grid>
            )}
          </Grid>
        </CardContent>
        <Divider />
        {/*  linha direcionada flex-end */}
        <Box display="flex" justifyContent="flex-end" p={2}>
          {/* Botão para salvar as informações */}
          <Button
            disabled={isLoading}
            color="primary"
            variant="contained"
            style={{ borderRadius: 50 }}
            onClick={() => {
              validateCampos();
            }}
          >
            Salvar informações
          </Button>
          {isLoading && <CircularProgress />}
        </Box>
      </Card>
    </form>
  );
};

ProfileDetails.propTypes = {
  className: PropTypes.string,
};

export default ProfileDetails;
