import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import moment from "moment";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography,
  makeStyles,
} from "@material-ui/core";

const user = {
  avatar: "/static/images/avatars/avatar_4.png",
  city: "",
  country: "USA",
  jobTitle: "Senior Developer",
  name: "Katarina Smith",
  timezone: "GTM-7",
};

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 100,
    width: 100,
  },
}));

const Profile = ({ userMe, className, ...rest }) => {
  const classes = useStyles();

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Box alignItems="center" display="flex" flexDirection="column">
          {/* imagem usuario  */}
          <Avatar className={classes.avatar} src="" />
          {/* data do cadastro do usuario */}
          <Typography
            style={{ marginTop: 10 }}
            color="textSecondary"
            variant="body1"
          >
            {`cadastrado ${moment(
              String(userMe.createdAt).substr(0, 10),
              "YYYY-MM-DD"
            ).format("DD/MM/YYYY")}`}
          </Typography>
          {/* hora do update */}
          <Typography
            className={classes.dateText}
            color="textSecondary"
            variant="body1"
          >
            {`atualizado em ${moment(
              String(userMe.updatedAt),
              "YYYY-MM-DD HH:mm:ss"
            ).format("DD/MM/YYYY HH:mm")}`}
          </Typography>
        </Box>
      </CardContent>
      <Divider />
      <CardActions>
        <Button color="primary" fullWidth variant="text">
          Editar Imagem
        </Button>
      </CardActions>
    </Card>
  );
};

Profile.propTypes = {
  className: PropTypes.string,
};

export default Profile;
