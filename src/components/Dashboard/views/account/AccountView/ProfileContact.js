import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";
import { UFs } from "../../../utils/uf";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProfileContact = ({ getAllUserMe, userMe, className, ...rest }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    email: "",
    phone1: "",
    phone2: "",
  });

  const onGetUserMe = () => {
    setValues({
      ...values,
      ["phone1"]: userMe.phone1,
      ["email"]: userMe.email,
      ["phone2"]: userMe.phone2,
    });
  };

  // pega inforamações do usuario que vem do arquivo index.js
  useEffect(() => {
    if (!(userMe == "")) {
      onGetUserMe();
    }
  }, [userMe]);

  const validateCampos = () => {
    for (var key in values) {
      if (key != "complement") {
        if (key != "phone2") {
          if ((values[key] == "") | (String(values[key]) == "null")) {
            let campoRecused =
              key == "email"
                ? " E-mail "
                : key == "phone1"
                ? " Telefone 1 "
                : "Indefinido";

            showMessage(
              "Atenção",
              `Campo ${campoRecused} não foi preenchido.`,
              "warning"
            );
            return;
          }
        }
      }
    }

    onUpdateBusiness(values, userMe.id);
  };

  const onUpdateBusiness = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/business/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Informações Atualisadas",
          "Informações selecionada foi atualisada com sucesso.",
          "success"
        );
        getAllUserMe();
      }
    } else {
      showMessage(
        "Informações Falhou",
        "Houve um problema ao atualizar suas Informações, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  // onChange dos campos do form
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20, marginTop: 20 }}>
        <CardHeader subheader="Suas informações para edição" title="Contatos" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            {/* campo responsivo | nome usuario */}
            <Grid item md={12} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="E-mail"
                name="email"
                onChange={handleChange}
                required
                value={values.email}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | phone1 */}
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Telefone 1"
                name="phone1"
                onChange={handleChange}
                required
                value={values.phone1}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | phone2 */}
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Telefone 2"
                name="phone2"
                onChange={handleChange}
                value={values.phone2}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        {/*  linha direcionada flex-end */}
        <Box display="flex" justifyContent="flex-end" p={2}>
          {/* Botão para salvar as informações */}
          <Button
            color="primary"
            disabled={isLoading}
            variant="contained"
            style={{ borderRadius: 50 }}
            onClick={() => {
              validateCampos();
            }}
          >
            Salvar informações
          </Button>
          {isLoading && <CircularProgress />}
        </Box>
      </Card>
    </form>
  );
};

ProfileContact.propTypes = {
  className: PropTypes.string,
};

export default ProfileContact;
