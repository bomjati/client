import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
} from "@material-ui/core";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";
import { UFs } from "../../../utils/uf";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProfileAddress = ({ getAllUserMe, userMe, className, ...rest }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    cep: "",
    public_place: "",
    number: "",
    complement: "",
    district: "",
    county: "",
    uf: "",
  });

  const onGetUserMe = () => {
    setValues({
      ...values,
      ["cep"]: userMe.cep,
      ["public_place"]: userMe.public_place,
      ["number"]: userMe.number,
      ["complement"]: userMe.complement,
      ["district"]: userMe.district,
      ["county"]: userMe.county,
      ["uf"]: userMe.uf,
    });
  };

  // pega inforamações do usuario que vem do arquivo index.js
  useEffect(() => {
    if (!(userMe == "")) {
      onGetUserMe();
    }
  }, [userMe]);

  const validateCampos = () => {
    for (var key in values) {
      if (key != "complement") {
        if (key != "phone2") {
          if ((values[key] == "") | (String(values[key]) == "null")) {
            let campoRecused =
              key == "cep"
                ? " CEP "
                : key == "public_place"
                ? " Logradouro "
                : key == "number"
                ? " Número Endereço "
                : key == "district"
                ? " Bairro "
                : key == "county"
                ? " Cidade "
                : key == "uf"
                ? " UF "
                : "Indefinido";

            showMessage(
              "Atenção",
              `Campo ${campoRecused} não foi preenchido.`,
              "warning"
            );
            return;
          }
        }
      }
    }

    onUpdateBusiness(values, userMe.id);
  };

  const onUpdateBusiness = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/business/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Informações Atualisadas",
          "Informações selecionada foi atualisada com sucesso.",
          "success"
        );
        getAllUserMe();
      }
    } else {
      showMessage(
        "Informações Falhou",
        "Houve um problema ao atualizar suas Informações, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  // onChange dos campos do form
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20, marginTop: 20 }}>
        <CardHeader subheader="Suas informações para edição" title="Endereço" />
        <Divider />
        <CardContent>
          <Grid container spacing={3}>
            {/* campo responsivo | nome usuario */}
            <Grid item md={3} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="CEP"
                name="cep"
                onChange={handleChange}
                required
                value={values.cep}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | county */}
            <Grid item md={7} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Municipio"
                name="county"
                onChange={handleChange}
                required
                value={values.county}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | uf */}
            <Grid item md={2} xs={12}>
              <FormControl fullWidth variant="outlined" required>
                <InputLabel htmlFor="age-native-required">UF</InputLabel>
                <Select
                  disabled={isLoading}
                  native
                  value={values.uf}
                  name="uf"
                  onChange={handleChange}
                >
                  {/*Lista de UF*/}
                  <option aria-label="None" value="" />
                  {UFs.map((i) => {
                    return <option value={i.value}>{i.value}</option>;
                  })}
                </Select>
              </FormControl>
            </Grid>
            {/* campo responsivo | public_place */}
            <Grid item md={10} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Logradouro"
                name="public_place"
                onChange={handleChange}
                required
                value={values.public_place}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | number */}
            <Grid item md={2} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="N°"
                name="number"
                onChange={handleChange}
                required
                value={values.number}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | district */}
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Bairro / Distrito"
                name="district"
                onChange={handleChange}
                required
                value={values.district}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | complement */}
            <Grid item md={6} xs={12}>
              <TextField
                fullWidth
                disabled={isLoading}
                label="Complemento"
                name="complement"
                onChange={handleChange}
                value={values.complement}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        {/*  linha direcionada flex-end */}
        <Box display="flex" justifyContent="flex-end" p={2}>
          {/* Botão para salvar as informações */}
          <Button
            color="primary"
            variant="contained"
            disabled={isLoading}
            style={{ borderRadius: 50 }}
            onClick={() => {
              validateCampos();
            }}
          >
            Salvar informações
          </Button>
          {isLoading && <CircularProgress />}
        </Box>
      </Card>
    </form>
  );
};

ProfileAddress.propTypes = {
  className: PropTypes.string,
};

export default ProfileAddress;
