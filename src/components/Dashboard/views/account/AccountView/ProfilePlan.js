import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  CircularProgress,
  GridList,
  Hidden
} from "@material-ui/core";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";

const useStyles = makeStyles(() => ({
  root: {},
}));

const ProfilePlan = ({ plans, getAllUserMe, userMe, className, ...rest }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [planId, setPlanId] = useState(0);

  const onGetUserMe = () => { };

  // pega inforamações do usuario que vem do arquivo index.js
  useEffect(() => {
    if (!(userMe == "")) {
      onGetUserMe();
      setPlanId(userMe.Plancontrols[0].planId);
    }
  }, [userMe]);

  useEffect(() => {
    console.log(plans);
  }, [plans]);

  return (
    <Grid container spacing={2}>
      {plans &&
        plans.map((plan) => (
          <Grid item lg={4} md={4} xl={4} xs={12}>
            <Card style={{ borderRadius: 20, marginRight: 20, marginLeft: 20, marginTop: 10 }}>
              <div
                style={{ height: 200, backgroundColor: "#2d3436" }}
              >
                <Box
                  height="100%"
                  alignItems="center"
                  justifyContent="center"
                  display="flex"
                  flexDirection="column"
                >
                  <Box display="flex">
                    <h3 style={{ marginTop: 10 }} className="text-white">
                      R$
                    </h3>
                    <Box alignItems="flex-end" display="flex">
                      <h1 style={{ marginBottom: -15 }} className="text-white">
                        {parseFloat(plan.price).toFixed(2).split(".")[0]}
                      </h1>
                      <h3 className="text-white">,</h3>
                      <h3 className="text-white">
                        {parseFloat(plan.price).toFixed(2).split(".")[1]}
                      </h3>
                    </Box>
                  </Box>
                  <h5 className="text-white mt-4">Plano {plan.name}</h5>
                  <p className="text-white">{plan.days} Dias</p>
              </Box>
              </div>
              <div
                style={{
                  height: 250,
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between"
                }}>
                <div>
                  <h5 className="ml-4 mr-4 mt-4 mb-2 text-black text-bold">Modulos</h5>
                  <Divider className="ml-4 mr-4 mb-4" />

                  {plan.Modulecontrols.map((m) => (<h6 className="ml-4 mr-4 mb-2 text-black text-bold">{m.Module.name}</h6>))}
                </div>

                <Box
                  mt={5}
                  mb={2}
                  display="flex"
                  justifyContent="center">
                  <Button
                    fullWidth
                    style={{
                      backgroundColor: planId == plan.id ? "#20bf6b" : "#4b4b4b",
                      color: "white",
                      marginLeft: 20,
                      marginRight: 20,
                    }}
                    variant="contained">{planId == plan.id ? "Ativo" : "Upgrate"}</Button>
                </Box>

              </div>
            </Card>
          </Grid>
        ))}
    </Grid>
  );
};

ProfilePlan.propTypes = {
  className: PropTypes.string,
};

export default ProfilePlan;
