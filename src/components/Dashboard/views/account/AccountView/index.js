import React, { useEffect, useState } from "react";
import {
  Container,
  Grid,
  makeStyles,
  Tab,
  Tabs,
  AppBar,
} from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import Page from "../../../components/Page";
import Profile from "./Profile";
import ProfileDetails from "./ProfileDetails";
import ProfileAddress from "./ProfileAddress";
import ProfileContact from "./ProfileContact";
import ProfilePlan from "./ProfilePlan";
import Password from "./Password";
import api from "../../../../../services/api";
import { TabPanel, a11yProps } from "../help";
import SwipeableViews from "react-swipeable-views";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const Account = () => {
  const classes = useStyles();
  const [userMe, setUserMe] = useState("");
  const [plans, setPlans] = useState(null);
  const [value, setValue] = useState(1);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  useEffect(() => {
    onMe();
    onPlans();
  }, []);

  const onPlans = async () => {
    const res = await api.get("/plan");

    if (res) {
      console.log(res.data);
      setPlans(res.data);
    }
  };

  // requesição para retornar as informações de quem logou
  const onMe = async () => {
    const res = await api.get("/login/me");

    if (res) {
      console.log(res.data);
      setUserMe(res.data.Business);
    }
  };

  const getAllUserMe = () => {
    onMe();
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <Page className={classes.root} title="Contas">
      <Container maxWidth="lg">
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChangeTab}
            textColor="white"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Informações" {...a11yProps(0)} />
            <Tab label="Plano" {...a11yProps(1)} />
          </Tabs>
        </AppBar>

        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <Grid className="mt-4" container spacing={3}>
              <Grid item lg={4} md={6} xs={12}>
                <Profile userMe={userMe} />
              </Grid>
              <Grid item lg={8} md={6} xs={12}>
                <ProfileDetails getAllUserMe={getAllUserMe} userMe={userMe} />
                <ProfileAddress getAllUserMe={getAllUserMe} userMe={userMe} />
                <ProfileContact getAllUserMe={getAllUserMe} userMe={userMe} />
                <Password userMe={userMe} />
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <ProfilePlan
              plans={plans}
              getAllUserMe={getAllUserMe}
              userMe={userMe}
            />
          </TabPanel>
        </SwipeableViews>
      </Container>
    </Page>
  );
};

export default Account;
