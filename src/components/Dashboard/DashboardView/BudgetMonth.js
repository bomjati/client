import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import MoneyIcon from '@material-ui/icons/Money';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: "#f1c40f",
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.red[900]
  },
  differenceValue: {
    color: colors.red[900],
    marginRight: theme.spacing(1)
  }
}));

const Budget = ({ className, value, ...rest }) => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="h6"
            >
              VENDAS MÊS
            </Typography>
            <Typography
              color="textPrimary"
              variant="h3"
            >
              {value ? value : 0}
            </Typography>
          </Grid>
        </Grid>
        <Avatar style={{ marginTop: 25 }} className={classes.avatar}>
          <MoneyIcon />
        </Avatar>
        <Box
          mt={2}
          display="flex"
          alignItems="center"
        >
          {/* <ArrowDownwardIcon className={classes.differenceIcon} /> */}
          <Typography
            color="textSecondary"
            variant="caption"
          >
            Real Time
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
};

Budget.propTypes = {
  className: PropTypes.string
};

export default Budget;
