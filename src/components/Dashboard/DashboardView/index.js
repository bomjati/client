import React, { useEffect, useState } from "react";
import { Container, Grid, makeStyles } from "@material-ui/core";
import api from "../../../services/api";
import socket from "../../../services/socket";
import Page from "../components/Page";
import Budget from "./Budget";
import BudgetMonth from "./BudgetMonth";
import BudgetNow from "./BudgetNow";
import BudgetWeek from "./BudgetWeek";
import LatestOrders from "./LatestOrders";
import LatestProducts from "./LatestProducts";
import Sales from "./Sales";
import TasksProgress from "./TasksProgress";
import TotalCustomers from "./TotalCustomers";
import TotalProfit from "./TotalProfit";
import TrafficByDevice from "./TrafficByDevice";
import { useLocation } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const Dashboard = ({ isModule, chOnlines }) => {
  const classes = useStyles();
  let arrayChannelOnline = {};
  const location = useLocation();
  const [isLoading, setIsLoading] = useState(false);
  const [channel, setChannel] = useState([]);
  const [channelOnlines, setChannelOnlines] = useState([]);
  const [seelMonth, setSeelMonth] = useState([]);
  const [seelWeek, setSeelWeek] = useState([]);
  const [seelNow, setSeelNow] = useState([]);
  const [idsEcommerces, setIdsEcommerces] = useState([]);
  const [valuesOnline, setValuesOnline] = useState(0);
  const [idBusiness, setIdBusiness] = useState(0);
  const [totalSeel, setTotalSeel] = useState(0);

  useEffect(() => {
    // socket.on("connect", () => console.log("Bem Vindo"));

    if (!isModule) {
      socket.on("previousChannelOnline", (onlines) => {
        console.log(onlines);
        arrayChannelOnline = { ...onlines };
        setChannelOnlines(onlines);
      });

      socket.on("receivedChannelOnline", (onlines) => {
        //console.log(arrayChannelOnline);
        if (
          !(
            JSON.stringify(arrayChannelOnline) ==
            JSON.stringify({ ...arrayChannelOnline, ...onlines })
          )
        ) {
          arrayChannelOnline = { ...arrayChannelOnline, ...onlines };
          //console.log(arrayChannelOnline);

          setChannelOnlines(arrayChannelOnline);
        }
      });
    }
  }, []);

  useEffect(() => {
    if (chOnlines) {
      setChannelOnlines(chOnlines);
    }
  }, [chOnlines]);

  useEffect(() => {
    getMe();
  }, []);
  useEffect(() => {
    if (idBusiness > 0) {
      api
        .get(`/ecommerce/${idBusiness}`)
        .then((resp) => resp.data)
        .then((res) => {
          let ids = [];

          for (let index = 0; index < res.length; index++) {
            const element = res[index];

            ids.push(element.id);
          }

          console.log(ids);
          setIdsEcommerces(ids);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [idBusiness]);

  useEffect(() => {
    if (idsEcommerces.length > 0) {
      console.log("veio");
      api
        .post(`/purchase/sales/month/`, {
          ids: idsEcommerces,
        })
        .then((resp) => resp.data)
        .then((res) => {
          let v = 0;

          for (let index = 0; index < res.length; index++) {
            const element = res[index];

            v = v + element.total;
          }
          setTotalSeel(v);
          setSeelMonth(res);
        })
        .catch((error) => {
          console.log(error);
        });

      api
        .post(`/purchase/sales/week/`, {
          ids: idsEcommerces,
        })
        .then((resp) => resp.data)
        .then((res) => {
          setSeelWeek(res);
        })
        .catch((error) => {
          console.log(error);
        });

      api
        .post(`/purchase/sales/now/`, {
          ids: idsEcommerces,
        })
        .then((resp) => resp.data)
        .then((res) => {
          setSeelNow(res);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [idsEcommerces]);

  useEffect(() => {
    console.log(channelOnlines);
    if (channel.length > 0) {
      setValuesOnline(
        channel.length > 0
          ? channel.filter((c) =>
              channelOnlines[String(c.clientId)]
                ? channelOnlines[String(c.clientId)].status == "online"
                : 1 > 2
            ).length
          : 0
      );
    }
  }, [channelOnlines, channel]);

  useEffect(() => {
    getChannel();
  }, []);

  const getChannel = async () => {
    setIsLoading(true);
    const res = await api.get("/channel");

    if (res) {
      setChannel(res.data);
    }

    setIsLoading(false);
  };

  const getMe = () => {
    setIsLoading(true);
    api
      .get("/login/me")
      .then((response) => response.data)
      .then((resp) => {
        setIdBusiness(resp.Business && resp.Business.id);
      });
  };

  return (
    <Page className={classes.root} title="Dashboard" contentPage="">
      <Container maxWidth={false}>
        <Grid container spacing={3}>
          {String(window.location.href).indexOf("commerce") == -1 && (
            <Grid item lg={3} sm={6} xl={3} xs={12}>
              <Budget value={valuesOnline} />
            </Grid>
          )}
          {String(window.location.href).indexOf("commerce") == -1 && (
            <Grid item lg={3} sm={6} xl={3} xs={12}>
              <TotalCustomers value={channel.length} />
            </Grid>
          )}
          <Grid item lg={3} sm={6} xl={3} xs={12}>
            <BudgetNow value={seelNow.length} />
          </Grid>

          <Grid item lg={3} sm={6} xl={3} xs={12}>
            <BudgetWeek value={seelWeek.length} />
          </Grid>

          <Grid item lg={3} sm={6} xl={3} xs={12}>
            <BudgetMonth value={seelMonth.length} />
          </Grid>

          <Grid item lg={3} sm={6} xl={3} xs={12}>
            <TotalProfit value={totalSeel} />
          </Grid>
          {/*  <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <TasksProgress />
          </Grid>
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}
          >
            <TotalProfit />
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}
          >
            <Sales />
          </Grid>
          <Grid
            item
            lg={4}
            md={6}
            xl={3}
            xs={12}
          >
            <TrafficByDevice />
          </Grid>
          <Grid
            item
            lg={4}
            md={6}
            xl={3}
            xs={12}
          >
            <LatestProducts />
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}
          >
            <LatestOrders />
          </Grid> */}
        </Grid>
      </Container>
    </Page>
  );
};

export default Dashboard;
