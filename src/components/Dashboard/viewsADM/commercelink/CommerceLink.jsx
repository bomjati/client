import React, { useState, useEffect } from 'react'
import {
    TextField
} from '@material-ui/core';
import api from "../../../../services/api";

const CommercerLink = () => {
    const [url, setUrl] = useState('')
    const [me, setMe] = useState()
    const CryptoJS = require("crypto-js");

    useEffect(() => {
        api.get("/login/me")
            .then(response => response.data)
            .then(resp => {
                if (resp) {
                    setMe(resp)
                    const test = CryptoJS.AES.encrypt(String(resp.id), "askljdhsdkf").toString()
                    setUrl(`${window.location.origin}/loja?profile=${test}`)
                }
            });
    }, []);

    return (
        <div style={{
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            position: 'absolute',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        }}>
            <div className='w-100 align-items-center'>
                <h3 className='text-black text-center'>Link da sua página</h3>
                <div className='d-flex justify-content-center'>
                    <TextField
                        contentEditable={false}
                        value={url}
                        variant='outlined'
                        style={{
                            width: '50%',
                        }}
                        inputProps={{
                            style: {
                                textAlign: 'center'
                            }
                        }}
                        className='mt-2' />
                </div>
            </div>
        </div>
    )
}

export default CommercerLink;