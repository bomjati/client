import React from 'react'
import {
    Card,
    CardHeader,
    CardContent,
    Divider,
    Box,
    Grid
} from '@material-ui/core'
import '../App.scss';

const CategoriasView = ({ categorys }) => {

    return (
        <div>
            <Card style={{ borderRadius: 20, marginTop: 25 }}>
                <CardHeader
                    title='Selecione Categoria'
                    subheader='Selecione a categoria a qual deseja visualisar a vitrine.'
                />
                <Divider />
                <CardContent>
                    <Grid container>
                        {categorys.map(ct => (
                            <Grid
                                item
                                lg={3}
                                md={3}
                                xs={12} >
                                <div className='ct h-100 w-100'>
                                    <Card
                                        onClick={() => {
                                            const url = `${window.location.origin}/list-product-utilitares?category=${ct.id}`;
                                            const win = window.open(url, '_blank');
                                            win.focus();
                                        }}
                                        className='card-ct m-4'
                                        style={{ height: 100, borderRadius: 15 }}>
                                        <Box className='h-100' display='flex' justifyContent='center' alignItems='center'>
                                            <h5 style={{ color: '#576574' }} className='categorys text-black'>{ct.name}</h5>
                                        </Box>
                                    </Card>
                                </div>
                            </Grid>
                        ))}
                    </Grid>
                </CardContent>
            </Card>
        </div >
    )
}

export default CategoriasView;