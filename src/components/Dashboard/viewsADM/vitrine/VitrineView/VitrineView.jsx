import React, { useState, useEffect } from 'react';
import {
    Card,
    CardHeader,
    CardContent,
    makeStyles,
    Container,
    Divider,
    Grid,
    Box
} from '@material-ui/core';
import {
    Book as BookIcon,
    CollectionsBookmark as CollectionsBookmarkIcon
} from '@material-ui/icons';
import Page from '../../../components/Page';
import CategoriasView from '../CategoriasView'
import api from "../../../../../services/api";
import { showMessage } from '../../../utils/message'

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: '100%',
        padding: theme.spacing(3)
    }
}));

const Vitrine = () => {
    const classes = useStyles();
    const [categorys, setCategorys] = useState([])
    const [listShowCategory, setListShowCategory] = useState(false)
    const [config, setConfig] = useState(null)

    useEffect(() => {
        api.get('/setting/0')
            .then((resp) => {
                if (resp.data) {
                    setConfig(resp.data[0])
                    console.log(resp.data[0])
                }
            })
            .catch((error) => {

            })
    }, [])

    useEffect(() => {
        api.get('/category')
            .then((resp) => {
                if (resp.data) {
                    setCategorys(resp.data)
                }
            })
            .catch((error) => {

            })
    }, [])

    return (
        <Page
            className={classes.root}
            title="Vitrine"
        >
            <Container maxWidth="lg">
                <Card style={{ borderRadius: 20 }}>
                    <CardHeader
                        title='Selecione Vitrine'
                        subheader='Selecione o tipo de vitrine a qual deseja usar.'
                    />
                    <Divider />
                    <CardContent>
                        <Grid container>
                            <Box className='w-100' display='flex' justifyContent='center'>
                                <Grid
                                    item
                                    lg={3}
                                    md={3}
                                    xs={12}>
                                    <Card
                                        onClick={() => {
                                            const url = config.typeShowcases == 'tipo 2' ? `${window.location.origin}/list-product-utilitares?category=*` : `${window.location.origin}/list-product?category=*`;
                                            const win = window.open(url, '_blank');
                                            win.focus();
                                        }}
                                        style={{ cursor: 'pointer', borderRadius: 20, backgroundColor: '#54a0ff' }}
                                        className='p-4 m-4'>
                                        <Box display='flex' justifyContent='center' className='w-100'>
                                            <BookIcon style={{ fontSize: 50, color: 'white' }} className='mb-4' />
                                        </Box>
                                        <Box display='flex' justifyContent='center' className='w-100'>
                                            <h6 className='text-white'>Todas Categorias</h6>
                                        </Box>
                                    </Card>
                                </Grid>
                                <Grid
                                    item
                                    lg={3}
                                    md={3}
                                    xs={12}>
                                    <Card
                                        onClick={() => {
                                            setListShowCategory(!listShowCategory)
                                        }}
                                        style={{
                                            cursor: 'pointer',
                                            borderRadius: 20,
                                            backgroundColor: '#5f27cd'
                                        }}
                                        className='p-4 m-4'>
                                        <Box display='flex' justifyContent='center' className='w-100'>
                                            <CollectionsBookmarkIcon style={{ fontSize: 50, color: 'white' }} className='mb-4' />
                                        </Box>
                                        <Box display='flex' justifyContent='center' className='w-100'>
                                            <h6 className='text-white text-align-center'>{listShowCategory ? "Desabilitar Lista" : "Dividir Por Categorias"}</h6>
                                        </Box>
                                    </Card>
                                </Grid>
                            </Box>
                        </Grid>
                    </CardContent>
                </Card>

                {listShowCategory && <CategoriasView categorys={categorys} />}
            </Container>
        </Page>
    )
}

export default Vitrine;