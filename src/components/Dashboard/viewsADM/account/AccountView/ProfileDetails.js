import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles
} from '@material-ui/core';

import { UFs } from '../../../utils/uf'
const useStyles = makeStyles(() => ({
  root: {}
}));

const ProfileDetails = ({ userMe, className, ...rest }) => {
  const classes = useStyles();
  const [tentativas, setTentativas] = useState(false)
  const [values, setValues] = useState({
    name: '',
    email: '',
    cel: '',
    uf: '',
    city: ''
  });

  const onGetUserMe = () => {
    setValues({
      ...values,
      ["name"]: userMe.name,
      ["email"]: userMe.email
    });
    setTentativas(true)
  }

  // pega inforamações do usuario que vem do arquivo index.js
  useEffect(() => {
    if (!(userMe == "")) {
      if (!tentativas) {
        onGetUserMe()
      }
    }
  })

  // onChange dos campos do form
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20 }}>
        <CardHeader
          subheader="Suas informações para edição"
          title="Conta"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            {/* campo responsivo | nome usuario */}
            <Grid
              item
              md={12}
              xs={12}
            >
              <TextField
                fullWidth
                helperText="Favor especifique seu primeiro nome"
                label="Nome Completo"
                name="name"
                onChange={handleChange}
                required
                value={values.name}
                variant="outlined"
              />
            </Grid>
            {/* campo responsivo | email */}
            <Grid
              item
              md={12}
              xs={12}
            >
              <TextField
                fullWidth
                label="E-mail"
                name="email"
                onChange={handleChange}
                required
                value={values.email}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        {/*  linha direcionada flex-end */}
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          {/* Botão para salvar as informações */}
          <Button
            color="primary"
            variant="contained"
            style={{ borderRadius: 50 }}
          >
            Salvar informações
          </Button>
        </Box>
      </Card>
    </form>
  );
};

ProfileDetails.propTypes = {
  className: PropTypes.string
};

export default ProfileDetails;
