import React, { useEffect, useState } from 'react';
import {
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import Page from '../../../components/Page';
import Profile from './Profile';
import ProfileDetails from './ProfileDetails';
import Password from './Password';
import api from "../../../../../services/api";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));



const Account = () => {
  const classes = useStyles();
  const [userMe, setUserMe] = useState("")

  // requesição para retornar as informações de quem logou
  const onMe = async () => {
    const res = await api.post("/me", {});

    if (res) {
      setUserMe(res.data)
    }
  }

  useEffect(() => {
   onMe();
  }, [])

  return (
    <Page
      className={classes.root}
      title="Contas"
    >
      <Container maxWidth="lg">
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={4}
            md={6}
            xs={12}
          >
            <Profile userMe={userMe} />
          </Grid>
          <Grid
            item
            lg={8}
            md={6}
            xs={12}
          >
            <ProfileDetails userMe={userMe} />
            <Password userMe={userMe} />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default Account;
