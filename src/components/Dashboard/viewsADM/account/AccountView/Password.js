import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(({
  root: {}
}));

const Password = ({ className, ...rest }) => {
  const classes = useStyles();
  const [values, setValues] = useState({
    password: '',
    confirm: ''
  });

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  return (
    <form
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card
        style={{ borderRadius: 20 }}
        className='mt-4'>
        <CardHeader
          subheader="Atualize suas informações de senha"
          title="Senha"
        />
        <Divider />
        <CardContent>
          <TextField
            fullWidth
            label="Senha Atual"
            margin="normal"
            name="password_now"
            onChange={handleChange}
            type="password"
            value={values.password_now}
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Nova Senha"
            margin="normal"
            name="new_password"
            onChange={handleChange}
            type="password"
            value={values.new_password}
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Confirmar Senha"
            margin="normal"
            name="confirm_password"
            onChange={handleChange}
            type="password"
            value={values.confirm_password}
            variant="outlined"
          />
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Button
            color="primary"
            style={{ borderRadius: 50 }}
            variant="contained"
          >
            Atualizar
          </Button>
        </Box>
      </Card>
    </form>
  );
};

Password.propTypes = {
  className: PropTypes.string
};

export default Password;
