import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewTypeContent from "../ModalNewTypeContent";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewTypeContent, setShowNewTypeContent] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [TypeContent, setTypeContent] = useState([]);
  const [TypeContentBusca, setTypeContentBusca] = useState([]);
  const [idTypeContentModify, setIdTypeContentModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getTypeContent();
  }, []);

  const buscaTypeContent = (value) => {
    let resultBusca = TypeContentBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    if(!(resultBusca.length > 0)){
      resultBusca = TypeContentBusca.filter((i) => {
        return (
          String(i.Contentgroup["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
          -1
        );
      });
    }

    setTypeContent(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdTypeContentModify(id);
  };
  const onShowNewTypeContent = () => {
    setShowNewTypeContent(true);
  };

  const onCloseNewTypeContent = () => {
    setShowNewTypeContent(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteTypeContent(idTypeContentModify);
  };

  const getTypeContent = async () => {
    setIsLoading(true);
    const res = await api.get("/TypeContent");

    if (res) {
      setTypeContent(res.data);
      setTypeContentBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeleteTypeContent = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/TypeContent/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getTypeContent();
        showMessage(
          "Tipo Deletado",
          "Tipo selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Tipo Falhou",
        "Houve um problema ao deletar seu tipo, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Tipo de Conteúdo">
      <Container maxWidth={false}>
        <Toolbar
          SearchTypeContent={buscaTypeContent}
          onShowNewTypeContent={onShowNewTypeContent}
        />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onDeleteTypeContent={onShowConfirm}
            customers={TypeContent}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewTypeContent
        open={showNewTypeContent}
        onCloseNewTypeContent={onCloseNewTypeContent}
        getAllTypeContent={getTypeContent}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Tipo será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
