import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewGenre from "../ModalNewGenre";
import ModalEditGenre from "../ModalEditGenre";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewGenre, setShowNewGenre] = useState(false);
  const [showEditGenre, setShowEditGenre] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [Genre, setGenre] = useState([]);
  const [GenreBusca, setGenreBusca] = useState([]);
  const [idGenreModify, setIdGenreModify] = useState(0);
  const [getGenreModify, setGetGenreModify] = useState(null);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getGenre();
  }, []);

  const buscaGenre = (value) => {
    let resultBusca = GenreBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    if (!(resultBusca.length > 0)) {
      resultBusca = GenreBusca.filter((i) => {
        return (
          String(i.Right["name"])
            .toUpperCase()
            .indexOf(String(value).toUpperCase()) > -1
        );
      });
    }

    setGenre(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdGenreModify(id);
  };
  const onShowNewGenre = () => {
    setShowNewGenre(true);
  };

  const onCloseNewGenre = () => {
    setShowNewGenre(false);
  };

  const onShowEditGenre = (value) => {
    setGetGenreModify(value);
    setShowEditGenre(true);
  };

  const onCloseEditGenre = () => {
    setShowEditGenre(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteGenre(idGenreModify);
  };

  const getGenre = async () => {
    setIsLoading(true);
    const res = await api.get("/Genre/admin");

    if (res) {
      const resCovers = await api.get("/cover");
      const resRights = await api.get("/right");

      let genreInfo = [];

      for (let index = 0; index < res.data.length; index++) {
        const element = res.data[index];

        genreInfo.push({
          ...element,
          Covers: resCovers.data.filter((c) => c.genreId == element.id),
          Right: resRights.data.filter((r) => r.id == element.rightId)[0],
        });
      }

      /* const genreInfo = res.data.map((g) => {
      return {
        ...g,
        Covers: [{
          "id": 95,
          "name": "forro (1).jpg",
          "path_s3": "https://bomjafiles.s3.sa-east-1.amazonaws.com/images/photoCover/c519f0ba91deedf7855577c79188513a-forro%20%281%29.jpg",
          "key": "images/photoCover/c519f0ba91deedf7855577c79188513a-forro (1).jpg",
          "genreId": 0,
          "genresubId": 5,
          "segmentId": 0,
          "createdAt": "2021-01-28 09:55:00",
          "updatedAt": "2021-02-16 13:41:17",
          "GenreId": 0,
          "GenresubId": 5,
          "SegmentId": 0
        }],
        Rights: resRights.data.filter((r) => r.id == g.rightId)[0],
      };
    }); */

      setGenre(genreInfo);
      setGenreBusca(genreInfo);
      console.log(genreInfo);
    }

    setIsLoading(false);
  };

  const onDeleteGenre = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Genre/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getGenre();
        showMessage(
          "Gênero Deletado",
          "Gênero selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Gênero Falhou",
        "Houve um problema ao deletar seu gênero, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Gênero">
      <Container maxWidth={false}>
        <Toolbar SearchGenre={buscaGenre} onShowNewGenre={onShowNewGenre} />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onDeleteGenre={onShowConfirm}
            onShowModalEditGenre={onShowEditGenre}
            customers={Genre}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewGenre
        open={showNewGenre}
        onCloseNewGenre={onCloseNewGenre}
        getAllGenre={getGenre}
      />
      <ModalEditGenre
        open={showEditGenre}
        getGenreModify={getGenreModify}
        onCloseEditGenre={onCloseEditGenre}
        getAllGenre={getGenre}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o gênero será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
