import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  CardActions,
  GridList,
  Avatar,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Delete as DeleteIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  AddAPhoto as AddAPhotoIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";

const Formulario = ({ onCloseModal, getAllSegment, dataSegment }) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listSegment, setListSegment] = useState(null);
  const [value, setValue] = useState(0);
  const [indexImage, setIndexImage] = useState(-1);
  const [typeClient, setTypeClient] = useState("juridica");
  const [preview, setPreview] = useState();
  const [selectedFile, setSelectedFile] = useState([]);
  const [listImagem, setListImagem] = useState([]);
  const [idSegment, setIdSegment] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    name: "",
  });

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  useEffect(() => {
    console.log(dataSegment);
    if (dataSegment) {
      setValues({ ...values, name: dataSegment.name });
      setListImagem(dataSegment.Covers);
      setIdSegment(dataSegment.id);
    }
  }, [dataSegment]);

  useEffect(() => {
    /* if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // memória livre sempre que este componente for desmontado
    return () => URL.revokeObjectURL(objectUrl); */
  }, [selectedFile]);

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    // Eu mantive este exemplo simples usando a primeira imagem em vez de vários
    setSelectedFile([...e.target.files]);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused = key == "name" ? " Nome do Segmento " : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  useEffect(() => {
    if (indexImage > -1) {
      if (selectedFile.length > 0) {
        if (indexImage < selectedFile.length) {
          const element = selectedFile[indexImage];

          const capaFormDataImg = new FormData();
          capaFormDataImg.append("isSegment", true);
          capaFormDataImg.append("file", element);

          api
            .post(`/cover/new/${idSegment}`, capaFormDataImg)
            .then((response) => response.data)
            .then((resp) => {
              if (resp.message == "success") {
                if (indexImage + 1 == selectedFile.length) {
                  showMessage(
                    "Novo Segmento",
                    `Cadastro realizado com sucesso.`,
                    "success"
                  );
                  getAllSegment();
                  setIsLoading(false);
                  onCloseModal();
                } else {
                  setIndexImage(indexImage + 1);
                }
              } else {
                showMessage("Novo Segmento", `Falhou.`, "error");
              }
            })
            .catch((error) => {
              console.log(error);
            });
        }
      }
    }
  }, [indexImage]);

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/Segment/edit/${dataSegment.id}`, values)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          setIndexImage(0);
        } else {
          showMessage("Nova Segmento", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const onDeleteImage = (id) => {
    setIsLoading(true);
    api
      .delete(`/Cover/${id}`)
      .then((response) => response.data)
      .then((resp) => {
        getAllSegment();
      })
      .catch((error) => {
        console.log(error);
      });

    setIsLoading(false);
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu segmento"
            title="Novo Segmento"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do segmento" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Card className="p-4" style={{ borderRadius: 20 }}>
                        <Typography
                          className="mb-4"
                          variant="body1"
                          style={{ fontWeight: "bold" }}
                        >
                          Grupo de Imagens
                        </Typography>
                        <Box display="flex" flexWrap="wrap">
                          {selectedFile &&
                            selectedFile.map((sf, index) => (
                              <div style={{ width: 110, margin: 5 }}>
                                <Avatar
                                  variant="rounded"
                                  style={{ height: 80, width: 110 }}
                                  src={URL.createObjectURL(sf)}
                                />
                                <Box
                                  display="flex"
                                  justifyContent="center"
                                  mt={1}
                                  mb={1}
                                >
                                  <IconButton
                                    style={{ ...styleButtonIcon }}
                                    onClick={() => {
                                      setSelectedFile(
                                        selectedFile.filter(
                                          (i, indexi) => indexi != index
                                        )
                                      );
                                    }}
                                    component="span"
                                    size="small"
                                  >
                                    <DeleteIcon style={{ color: "red" }} />
                                  </IconButton>
                                </Box>
                              </div>
                            ))}

                          {listImagem.length > 0 &&
                            listImagem.map((sf, index) => {
                              return (
                                <div style={{ width: 110, margin: 5 }}>
                                  <Avatar
                                    variant="rounded"
                                    style={{ height: 80, width: 110 }}
                                    src={sf.path_s3}
                                  />
                                  <Box
                                    display="flex"
                                    justifyContent="center"
                                    mt={1}
                                    mb={1}
                                  >
                                    <IconButton
                                      style={{ ...styleButtonIcon }}
                                      onClick={() => {
                                        onDeleteImage(sf.id);
                                      }}
                                      component="span"
                                      size="small"
                                    >
                                      <DeleteIcon style={{ color: "red" }} />
                                    </IconButton>
                                  </Box>
                                </div>
                              );
                            })}

                          <label
                            style={{ height: 50, width: 50 }}
                            htmlFor="sampleFile"
                          >
                            <IconButton
                              style={{ backgroundColor: "red" }}
                              component="span"
                              size="medium"
                            >
                              <AddAPhotoIcon style={{ color: "white" }} />
                            </IconButton>
                          </label>
                        </Box>

                        <input
                          onChange={onSelectFile}
                          multiple
                          type="file"
                          id="sampleFile"
                          style={{ display: "none" }}
                        />
                      </Card>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.name}
                        onChange={handleChange}
                        label="Nome do Segmento"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
