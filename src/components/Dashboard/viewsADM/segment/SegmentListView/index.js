import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewSegment from "../ModalNewSegment";
import ModalEditSegment from "../ModalEditSegment";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewSegment, setShowNewSegment] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditSegment, setShowEditSegment] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [Segment, setSegment] = useState([]);
  const [SegmentBusca, setSegmentBusca] = useState([]);
  const [getEditSegment, setGetEditSegment] = useState([]);
  const [idSegmentModify, setIdSegmentModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getSegment();
  }, []);

  const buscaSegment = (value) => {
    const resultBusca = SegmentBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setSegment(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdSegmentModify(id);
  };
  const onShowNewSegment = () => {
    setShowNewSegment(true);
  };

  const onCloseNewSegment = () => {
    setShowNewSegment(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditSegment = (value) => {
    setShowEditSegment(true);
    setGetEditSegment(value);
  };

  const onCloseEditSegment = () => {
    setShowEditSegment(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteSegment(idSegmentModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getSegment = async () => {
    setIsLoading(true);
    const res = await api.get("/Segment");

    if (res) {
      setSegment(res.data);
      setSegmentBusca(res.data);

      if (getEditSegment.id) {
        setGetEditSegment(
          res.data.filter((s) => s.id == getEditSegment.id).length > 0
            ? res.data.filter((s) => s.id == getEditSegment.id)[0]
            : [0]
        );
      }
    }

    setIsLoading(false);
  };

  const onDeleteSegment = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Segment/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getSegment();
        onCloseEditSegment();
        showMessage(
          "Segmento Deletado",
          "Segmento selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Segmento Falhou",
        "Houve um problema ao deletar seu segmento, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Segmento">
      <Container maxWidth={false}>
        <Toolbar
          SearchSegment={buscaSegment}
          onShowNewSegment={onShowNewSegment}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditSegment={onShowEditSegment}
            onDeleteSegment={onShowConfirm}
            customers={Segment}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewSegment
        open={showNewSegment}
        onCloseNewSegment={onCloseNewSegment}
        getAllSegment={getSegment}
      />
      <ModalEditSegment
        open={showEditSegment}
        onCloseEditSegment={onCloseEditSegment}
        getAllSegment={getSegment}
        dataSegment={getEditSegment}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o segmento será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
