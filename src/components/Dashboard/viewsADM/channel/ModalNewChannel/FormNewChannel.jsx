import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../../../services/api";
import { v4 } from "uuid";

const Formulario = ({ onCloseModal, getAllChannel }) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listSegment, setListSegment] = useState(null);
  const [value, setValue] = useState(0);
  const [businessId, setBusinessId] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    name: "",
    segmentId: "",
    terms_and_conditions: "",
  });

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "name"
            ? " Nome do Canal "
            : key == "segmentId"
            ? " Segmento "
            : key == "terms_and_conditions"
            ? " Termos e Política de Privacidade "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .post("/channel/new", { ...values, businessId: businessId })
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          api
            .post("/clientsetting/new", {
              qtdMusic: 1,
              qtdMusicCommertial: 1,
              isRuleVignetteComercial: "false",
              isRuleVignetteDefault: "true",
              sazonalVignetteId: 0,
              institucionalVignetteId: 0,
              clientId: resp.client.id,
            })
            .then((response) => response.data)
            .then((resp) => {
              if (resp.message == "success") {
                showMessage(
                  "Novo Canal",
                  `Cadastro realizado com sucesso.`,
                  "success"
                );
                getAllChannel();
                setIsLoading(false);
                onCloseModal();
              } else {
                showMessage("Novo Canal", `Falhou.`, "error");
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } else {
          showMessage("Novo Canal", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    api
      .get("/segment")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListSegment(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });

    api
      .get("/login/me")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setBusinessId(resp.Business.id);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu canal"
            title="Novo Canal"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do Canal" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.name}
                        onChange={handleChange}
                        label="Nome do Canal"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Segmentos
                          </InputLabel>
                          <Select
                            required
                            native
                            name="segmentId"
                            id="segmentId"
                            value={values.segmentId}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listSegment &&
                              listSegment.map((segments) => (
                                <option value={segments.id}>
                                  {segments.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                      <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        className="mt-4"
                      >
                        <Switch
                          required
                          checked={values.terms_and_conditions}
                          onChange={(event) => {
                            setValues({
                              ...values,
                              terms_and_conditions: event.target.checked,
                            });
                          }}
                          name="novidade"
                          color="primary"
                        />
                        <span className="d-inline-block">
                          Ao logar-se, você aceita nossos{" "}
                          <a style={{ fontWeight: "bold" }} href="/#">
                            Termos
                          </a>{" "}
                          &amp;{" "}
                          <a style={{ fontWeight: "bold" }} href="/#">
                            Política de Privacidade
                          </a>
                        </span>
                      </Box>
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
