import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import NewChannel from './FormNewChannel'

const useStyles = makeStyles(({ breakpoints }) => ({
    modal: {
        display: 'flex',
        [breakpoints.up('xs')]: {
            alignItems: 'start',
        },
        [breakpoints.up('sm')]: {
            alignItems: 'center',
        },
        justifyContent: 'center'
    }
}));

const ModalNewBusiness = ({ open, onCloseNewChannel, getAllChannel }) => {
    const classes = useStyles();

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={onCloseNewChannel}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className='mb-4'>
                        <NewChannel
                            onCloseModal={onCloseNewChannel}
                            getAllChannel={getAllChannel}/>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

export default ModalNewBusiness;