import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  duration,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  AccessTime as AccessTimeIcon,
  Delete as DeleteIcon,
  AddCircleOutline as AddCircleIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";

let test = "";
const Formulario = ({
  onCloseModal,
  getAllVinhetas,
  isLoadingModal,
  selectId,
}) => {
  let history = useNavigate();
  const fileToArrayBuffer = require("file-to-array-buffer");
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listTypeVignette, setListTypeVignette] = useState(null);
  const [value, setValue] = useState(0);
  const [listClient, setListClient] = useState("");
  const [listSegment, setListSegment] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState();
  const [listConcluded, setListConcluded] = useState([]);
  const [listDuration, setListDuration] = useState(null);
  const [dateSelect, setDateSelect] = useState({
    1: {
      dueDate: moment().format("YYYY-MM-DD"),
    },
  });

  const [values, setValues] = useState({
    typevignetteId: "",
    nameTypeVignette: "",
  });

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }

    setSelectedFile(e.target.files);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "typevignetteId" ? " Tipo de Vinheta " : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    if (!selectedFile) {
      showMessage(
        "Atenção",
        `Selecione ao menos uma Vinheta antes de salvar`,
        "warning"
      );

      return;
    }

    if (moment(dateSelect["1"].dueDate, "YYYY-MM-DD").diff(moment()) <= 0) {
      showMessage("Atenção", `Selecione um dia maior que hoje`, "warning");

      return;
    }

    onSave();
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    isLoadingModal(true);
    setIsLoading(true);

    for (let index = 0; index < selectId.length; index++) {
      const element = selectId[index];

      const FormDataVignette = new FormData();
      FormDataVignette.append("clientId", element);
      FormDataVignette.append(
        "cliente",
        listClient.filter((c) => c.id == element)[0].name
      );
      FormDataVignette.append("typevignetteId", values.typevignetteId);
      FormDataVignette.append("typevignette", values.nameTypeVignette);
      FormDataVignette.append("userFile", selectedFile[0]);

      api
        .post("/Vignette/new", FormDataVignette)
        .then((response) => response.data)
        .then((resp) => {
          let arrayConcluded = listConcluded;
          arrayConcluded.push({ key: index });
          setListConcluded(arrayConcluded);

          if (resp.message == "success") {
            let arrayRefreshDays = {};

            for (const key in dateSelect) {
              if (dateSelect.hasOwnProperty(key)) {
                const elementH = dateSelect[key];

                arrayRefreshDays = {
                  ...arrayRefreshDays,
                  [key]: {
                    ...elementH,
                    vignetteId: resp.data.id,
                  },
                };
              }
            }

            api
              .post("/day/new", arrayRefreshDays)
              .then((response) => response.data)
              .then((resp) => {
                if (resp.message == "success") {
                  if (index + 1 == selectId.length) {
                    showMessage(
                      "Novas Vinhetas",
                      `Cadastro realizado com sucesso.`,
                      "success"
                    );
                    setIsLoading(false);
                    isLoadingModal(false);
                    getAllVinhetas();
                    onCloseModal();
                  }
                } else {
                  showMessage("Novo Comercial", `Falhou.`, "error");
                }
              })
              .catch((error) => {
                console.log(error);
              });
          } else {
            showMessage("Nova Vinhetas", `Falhou.`, "error");
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  useEffect(() => {
    api
      .get("/typevignette")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListTypeVignette(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });

    api
      .get(`/client/array/${JSON.stringify(selectId)}`)
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListClient(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={!isLoading && onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu vinheta"
            title="Nova Vinheta"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações da Vinheta" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Tipo de Vinheta
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="typevignetteId"
                            id="typevignetteId"
                            value={`${values.typevignetteId}|${values.nameTypeVignette}`}
                            onChange={(event) => {
                              const result = String(event.target.value).split(
                                "|"
                              );

                              setValues({
                                ...values,
                                [event.target.name]: result[0],
                                nameTypeVignette: result[1],
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listTypeVignette &&
                              listTypeVignette.map((TypeVignettes) => (
                                <option
                                  value={`${TypeVignettes.id}|${TypeVignettes.name}`}
                                >{`${TypeVignettes.name}`}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>

                      <Divider className="mt-4 mb-4" />

                      {Object.keys(dateSelect).map((key, index) => (
                        <Box mt={2} display="flex" justifyContent="center">
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              disableToolbar
                              inputVariant="outlined"
                              format="dd/MM/yyyy"
                              id="date-picker-outlined"
                              label="Enceramento"
                              name="dueDate"
                              value={moment(
                                dateSelect[key].dueDate,
                                "YYYY-MM-DD"
                              )}
                              onChange={(event) => {
                                setDateSelect({
                                  ...dateSelect,
                                  [key]: {
                                    dueDate: moment(event).format("YYYY-MM-DD"),
                                  },
                                });
                              }}
                              fullWidth
                              KeyboardButtonProps={{
                                "aria-label": "change date",
                              }}
                            />
                          </MuiPickersUtilsProvider>
                        </Box>
                      ))}
                    </Grid>
                    {selectedFile &&
                      [...selectedFile].map((Vignette, index) => (
                        <Grid item md={12} xs={12}>
                          <Box display="flex" alignItems="center">
                            {isLoading &&
                              !(
                                listConcluded.filter(
                                  (concluded) => concluded.key == index
                                ).length > 0
                              ) && (
                                <CircularProgress className="mr-2" size={15} />
                              )}
                            {listConcluded.filter(
                              (concluded) => concluded.key == index
                            ).length > 0 && (
                              <CheckIcon
                                className="mr-2"
                                style={{ color: "#2ecc71" }}
                              />
                            )}
                            <h6>{`${Vignette.name}`}</h6>
                          </Box>
                        </Grid>
                      ))}
                  </Grid>

                  <Button
                    className="mt-4"
                    color="primary"
                    variant="outlined"
                    fullWidth
                    disabled={isLoading}
                    htmlFor="sampleFile"
                    component="label"
                    style={{ borderRadius: 20 }}
                  >
                    Buscar Vinheta
                  </Button>

                  <input
                    onChange={onSelectFile}
                    type="file"
                    id="sampleFile"
                    style={{ display: "none" }}
                  />
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
