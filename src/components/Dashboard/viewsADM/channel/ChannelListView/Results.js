import React, { useState, useEffect, Fragment } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Tooltip,
  CircularProgress,
} from "@material-ui/core";
import getInitials from "../../../utils/getInitials";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  PlayCircleOutline as PlayCircleOutlineIcon,
  Settings as SettingsIcon,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  onDeleteChannel,
  onShowModalEditChannel,
  onShowModalSettingChannel,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  onSelectId,
  isLoadingSettings,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.clientId);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  const formatTel = (str) => {
    return String(str).replace(/\D/g, "").length == 11
      ? String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d)(\d{4})(\d{4})$/, "($1) $2 $3-$4")
      : String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d{4})(\d{4})$/, "($1) $2-$3");
  };

  useEffect(() => {
    console.log(selectedCustomerIds);
    onSelectId(selectedCustomerIds);
  }, [selectedCustomerIds]);

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={750}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                {!(selectedCustomerIds.length > 0) ? (
                  <Hidden smDown>
                    <TableCell align="right" />
                  </Hidden>
                ) : null}
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <Fragment>
                      <TableRow
                        hover
                        key={customer.clientId}
                        selected={
                          selectedCustomerIds.indexOf(customer.clientId) !== -1
                        }
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={
                              selectedCustomerIds.indexOf(customer.clientId) !==
                              -1
                            }
                            onChange={(event) =>
                              handleSelectOne(event, customer.clientId)
                            }
                            value="true"
                          />
                        </TableCell>
                        <TableCell align="left">
                          <Box alignItems="center" display="flex">
                            <Avatar
                              className={classes.avatar}
                              /* src={customer.avatarUrl} */
                              src={customer.avatarUrl}
                            >
                              {getInitials(customer.Client.name)}
                            </Avatar>
                            <Typography color="textPrimary" variant="body1">
                              {formatStringName(customer.Client.name)}
                            </Typography>
                          </Box>
                        </TableCell>
                        <TableCell align="left">
                          <Tooltip
                            title={customer.Client.Segmentcontrols.map(
                              (Segmentcontrol, index) =>
                                index + 1 ==
                                customer.Client.Segmentcontrols.length
                                  ? `${Segmentcontrol.Segment.name}`
                                  : `${Segmentcontrol.Segment.name}, `
                            )}
                            arrow
                          >
                            <Typography color="textPrimary" variant="body1">
                              {customer.Client.Segmentcontrols[0].Segment.name}
                              {customer.Client.Segmentcontrols.length > 1
                                ? "..."
                                : ""}
                            </Typography>
                          </Tooltip>
                        </TableCell>
                        <TableCell align="left">
                          {customer.Business
                            ? customer.Business.business_name
                            : ""}
                        </TableCell>
                        {!(selectedCustomerIds.length > 0) ? (
                          <Hidden smDown>
                            <TableCell align="right" alignContent="center">
                              <Tooltip title="Configurações" arrow>
                                {isLoadingSettings ? (
                                  <CircularProgress
                                    size={20}
                                  />
                                ) : (
                                  <IconButton
                                    disabled={isLoading}
                                    onClick={() => {
                                      onShowModalSettingChannel(
                                        customer.Client.id//,
                                        //customer.Client.Clientsettings[0]
                                      );
                                    }}
                                    color="inherit"
                                  >
                                    <SettingsIcon
                                      style={{ color: "#3d3d3d" }}
                                    />
                                  </IconButton>
                                )}
                              </Tooltip>
                              <Tooltip title="Play Canal" arrow>
                                <IconButton
                                  disabled={isLoading}
                                  onClick={() => {
                                    const url = `${window.location.origin}/bomja-music?profile=${customer.Client.keyaccess}`;
                                    const win = window.open(url, "_blank");
                                    win.focus();
                                  }}
                                  color="inherit"
                                >
                                  <PlayCircleOutlineIcon
                                    style={{ color: "#20bf6b" }}
                                  />
                                </IconButton>
                              </Tooltip>
                              <Tooltip title="Deletar Canal" arrow>
                                <IconButton
                                  disabled={isLoading}
                                  style={{ marginLeft: 5 }}
                                  onClick={() => {
                                    onDeleteChannel(customer.id);
                                  }}
                                  color="inherit"
                                >
                                  <DeleteIcon style={{ color: "#ff4d4d" }} />
                                </IconButton>
                              </Tooltip>
                            </TableCell>
                          </Hidden>
                        ) : null}
                      </TableRow>
                      <Hidden lgUp>
                        <TableRow style={{ height: 0 }}>
                          <TableCell style={{ height: "auto !important" }} />
                          {!(selectedCustomerIds.length > 0) ? (
                            <TableCell align="left">
                              <Tooltip title="Configurações" arrow>
                                {isLoadingSettings ? (
                                  <CircularProgress />
                                ) : (
                                  <IconButton
                                    disabled={isLoading}
                                    onClick={() => {
                                      onShowModalSettingChannel(
                                        customer.Client.id//,
                                        //customer.Client.Clientsettings[0]
                                      );
                                    }}
                                    color="inherit"
                                  >
                                    <SettingsIcon
                                      style={{ color: "#3d3d3d" }}
                                    />
                                  </IconButton>
                                )}
                              </Tooltip>
                              <Tooltip title="Play Canal" arrow>
                                <IconButton
                                  disabled={isLoading}
                                  onClick={() => {
                                    const url = `${window.location.origin}/bomja-music?profile=${customer.Client.keyaccess}`;
                                    const win = window.open(url, "_blank");
                                    win.focus();
                                  }}
                                  color="inherit"
                                >
                                  <PlayCircleOutlineIcon
                                    style={{ color: "#20bf6b" }}
                                  />
                                </IconButton>
                              </Tooltip>
                              <Tooltip title="Deletar Canal" arrow>
                                <IconButton
                                  disabled={isLoading}
                                  style={{ marginLeft: 5 }}
                                  onClick={() => {
                                    onDeleteChannel(customer.id);
                                  }}
                                  color="inherit"
                                >
                                  <DeleteIcon style={{ color: "#ff4d4d" }} />
                                </IconButton>
                              </Tooltip>
                            </TableCell>
                          ) : null}
                          <TableCell />
                          <TableCell />
                        </TableRow>
                      </Hidden>
                    </Fragment>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
