import React, { useState, useRef, useEffect } from "react";
import { Box, Card, Button } from "@material-ui/core";
import {
  DeveloperBoard as DeveloperBoardIcon,
  Home as HomeIcon,
  MusicNote as MusicNoteIcon,
  RecordVoiceOver as RecordVoiceOverIcon,
  ArtTrack as ArtTrackIcon,
  Album as AlbumIcon,
  FolderOpen as FolderOpenIcon,
  BookmarkOutlined as BookmarkIcon,
  PostAdd as PostAddIcon,
  Receipt as ReceiptIcon,
  QueueMusic as QueueMusicIcon,
} from "@material-ui/icons";

export default function OptionChannel({ onPage, onChecked }) {
  function useOutsideAlerter(ref) {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        selectPage(-1, "home");
      }
    }

    useEffect(() => {
      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        // Unbind the event listener on clean up
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  const [activeMusic, setActiveMusic] = useState(false);
  const [activeContent, setActiveContent] = useState(false);
  const [activeVignette, setActiveVignette] = useState(false);
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  const styleSubMenu = { position: "absolute", zIndex: 1, marginTop: 10 };
  const styleButtonMenu = {
    borderRadius: 20,
    backgroundColor: "#00022E",
    color: "white",
    margin: 20
  };
  const styleButtonSubMenu = {
    borderRadius: 20
  };

  const selectPage = (value, type) => {
    if (value > -1) {
      onPage(value);
    }

    switch (type) {
      case "home":
        setActiveMusic(false);
        setActiveContent(false);
        setActiveVignette(false);

        break;
      case "music":
        setActiveMusic(!activeMusic);
        setActiveContent(false);
        setActiveVignette(false);

        break;
      case "content":
        setActiveContent(!activeContent);
        setActiveMusic(false);
        setActiveVignette(false);

        break;
      case "vignette":
        setActiveVignette(!activeVignette);
        setActiveMusic(false);
        setActiveContent(false);

        break;
      default:
        break;
    }
  };

  return (
    <Box ref={wrapperRef} style={{}} display="flex" flexWrap="wrap" justifyContent="center">
      <div>
        <Button
          onClick={() => selectPage(0, "home")}
          startIcon={<HomeIcon />}
          style={styleButtonMenu}
          className="mr-2"
          variant="contained"
        >
          Principal
        </Button>
      </div>
      <div>
        <Button
          onClick={() => selectPage(-1, "music")}
          startIcon={<MusicNoteIcon />}
          style={styleButtonMenu} //backgroundColor: "#FAFAFA"
          className="mr-2"
          variant="contained"
        >
          Musicas
        </Button>
        {!activeMusic ? null : (
          <div style={styleSubMenu}>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(1, "music")}
                startIcon={<AlbumIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Generos
              </Button>
            </div>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(2, "music")}
                startIcon={<ArtTrackIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Playlist
              </Button>
            </div>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(3, "music")}
                startIcon={<QueueMusicIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Lista de Musicas
              </Button>
            </div>
          </div>
        )}
      </div>
      <div>
        <Button
          onClick={() => selectPage(-1, "content")}
          startIcon={<DeveloperBoardIcon />}
          style={styleButtonMenu}
          className="mr-2"
          variant="contained"
        >
          Conteúdo
        </Button>
        {!activeContent ? null : (
          <div style={styleSubMenu}>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(4, "content")}
                startIcon={<FolderOpenIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Grupo de Conteúdo
              </Button>
            </div>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(5, "content")}
                startIcon={<BookmarkIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Tipo de Conteúdo
              </Button>
            </div>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(6, "content")}
                startIcon={<PostAddIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Lista de Conteúdo
              </Button>
            </div>
            <div className="mt-2">
              <Button
                onClick={() => selectPage(8, "content")}
                startIcon={<PostAddIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Frases para Conteúdo
              </Button>
            </div>
          </div>
        )}
      </div>
      <div>
        <Button
          onClick={() => selectPage(-1, "vignette")}
          startIcon={<RecordVoiceOverIcon />}
          style={styleButtonMenu}
          className="mr-2"
          variant="contained"
        >
          Vinheta
        </Button>
        {!activeVignette ? null : (
          <div style={styleSubMenu}>
            <div className="mt-1">
              <Button
                onClick={() => selectPage(7, "vignette")}
                startIcon={<ReceiptIcon />}
                style={styleButtonSubMenu}
                className="mr-2"
                variant="contained"
              >
                Tipo de Vinhetas
              </Button>
            </div>
          </div>
        )}
      </div>
    </Box>
  );
}
