import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
  Tooltip,
} from "@material-ui/core";
import { Search as SearchIcon } from "react-feather";
import AddIcon from "@material-ui/icons/AddOutlined";

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({ onShowNewChannel, SearchChannel, className, ...rest }) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box m={-2} mt={1}>
        <Card style={{ borderRadius: 20 }}>
          <CardContent>
            <Box display={"flex"} justifyContent="space-between">
              <Box width="50%">
                <TextField
                  fullWidth
                  onChange={(event) => {
                    SearchChannel(event.target.value);
                  }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SvgIcon fontSize="small" color="action">
                          <SearchIcon />
                        </SvgIcon>
                      </InputAdornment>
                    ),
                  }}
                  placeholder="Procurar Canal"
                  variant="outlined"
                />
              </Box>
              <Tooltip title="Novo Canal" arrow>
                <Button
                  onClick={onShowNewChannel}
                  color="primary"
                  variant="contained"
                  style={{
                    borderRadius: 60,
                    width: 60,
                    height: 65,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  size="large"
                >
                  <AddIcon />
                </Button>
              </Tooltip>
            </Box>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
