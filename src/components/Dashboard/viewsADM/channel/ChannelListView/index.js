import React, { useState, useEffect } from "react";
import {
  Box,
  Container,
  makeStyles,
  LinearProgress,
  Divider,
} from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewChannel from "../ModalNewChannel";
import ModalEditChannel from "../ModalEdit";
import ModalNewVignette from "../ModalVignette";
import ModalNewCommercial from "../ModalCommercial";
import BottomMultSelect from "./multSelectVignetteCommercial";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";
import ModalSettingChannel from "../ModalSettingChannel";
import ModalEditSettingChannel from "../ModalEditSettingChannel";
import Genre from "../../genre/GenreListView";
import GenreSub from "../../genresub/GenreSubListView";
import Music from "../../music/MusicListView";
import ContentGroup from "../../contentgroup/ContentGroupListView";
import TypeContent from "../../typecontent/TypeContentListView";
import Content from "../../content/ContentListView";
import TypeVignette from "../../typevignette/TypeVignetteListView";
import PhraseSegment from "../../phrasesegment/PhraseSegmentListView";
import OptionChannel from "./OptionChannel";
import { TabPanel, a11yProps } from "../help";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewChannel, setShowNewChannel] = useState(false);
  const [showNewVignette, setShowNewVignette] = useState(false);
  const [showNewCommercial, setShowNewCommercial] = useState(false);
  const [showSetting, setShowSetting] = useState(false);
  const [showEditChannel, setShowEditChannel] = useState(false);
  const [showEditSetting, setShowEditSetting] = useState(false);
  const [isLoadingSettings, setIsLoadingSettings] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [Channel, setChannel] = useState([]);
  const [ChannelBusca, setChannelBusca] = useState([]);
  const [getEditChannel, setGetEditChannel] = useState([]);
  const [segmentControls, setSegmentControls] = useState([]);
  const [selectId, setSelectId] = useState([]);
  const [idChannelModify, setIdChannelModify] = useState(0);
  const [pageIndex, setPageIndex] = useState(0);
  const [idClient, setIdClient] = useState(0);
  const [getSettingChannelModify, setGetSettingChannelModify] = useState(null);
  const [titlesTopGrid] = useState(titlesGrid);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  useEffect(() => {
    getSegmentControl();
  }, []);

  useEffect(() => {
    if (segmentControls.length > 0) {
      getChannel();
    }
  }, [segmentControls]);

  const buscaChannel = (value) => {
    const resultBusca = ChannelBusca.filter((i) => {
      return (
        String(i.Client["name"])
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    setChannel(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdChannelModify(id);
  };

  const onShowEditSetting = (id, data) => {
    setShowEditSetting(true);
    setGetSettingChannelModify(data);
    setIdClient(id);
  };

  const onCloseEditSetting = () => {
    setShowEditSetting(false);
  };

  const onShowNewChannel = () => {
    setShowNewChannel(true);
  };

  const onCloseNewChannel = () => {
    setShowNewChannel(false);
  };

  const onShowNewVignette = () => {
    setShowNewVignette(true);
  };

  const onCloseNewVignette = () => {
    setShowNewVignette(false);
  };

  const onShowNewCommercial = () => {
    setShowNewCommercial(true);
  };

  const onCloseNewCommercial = () => {
    setShowNewCommercial(false);
  };

  const onShowEditChannel = (value) => {
    setShowEditChannel(true);
    setGetEditChannel(value);
  };

  const onCloseEditChannel = () => {
    setShowEditChannel(false);
  };

  const onShowModalSettingChannel = async (id, data) => {
    setIsLoadingSettings(true);

    const blockeds = await api.get(`/blocked/client/${id}`);
    console.log("get Blocked");

    const previewLists = await api.get(`/previewlist/client/${id}`);
    console.log("get previewLists");

    const clientLocalities = await api.get(`/clientlocalitie/client/${id}`);
    console.log("get clientLocalities");

    const clientSettings = await api.get(`/clientsetting/client/${id}`);
    console.log("get clientSettings");

    setIsLoadingSettings(false);

    const infoData = {
      ...data,
      Blockeds: blockeds,
      Previewlists: previewLists,
      Clientlocalities: clientLocalities,
      Clientsettings: clientSettings,
    };

    setShowSetting(true);
    setGetSettingChannelModify(clientSettings[0]);
    setIdClient(id);
  };

  const onCloseSettingChannel = () => {
    setShowSetting(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteChannel(idChannelModify);
  };

  const getSegmentControl = async () => {
    setIsLoading(true);
    const res = await api.get("/segmentcontrol");

    if (res) {
      // console.log(res.data);
      setSegmentControls(res.data);
    }

    setIsLoading(false);
  };

  const getChannel = async () => {
    setIsLoading(true);
    const res = await api.get("/channel/admin");

    if (res) {
      let arrayChannelSegmentControl = [];

      for (let index = 0; index < res.data.length; index++) {
        const element = res.data[index];
        const arrayClientChannel = {
          ...element.Client,
          Segmentcontrols: segmentControls.filter(
            (s) => s.clientId == element.clientId
          ),
        };

        arrayChannelSegmentControl.push({
          ...element,
          Client: arrayClientChannel,
        });
      }

      setChannel(arrayChannelSegmentControl);
      setChannelBusca(arrayChannelSegmentControl);
    }

    setIsLoading(false);
  };

  const onUpdateChannel = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/Channel/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Radio Atualisada",
          "Radio selecionada foi atualisada com sucesso.",
          "success"
        );
        getChannel();
        onCloseEditChannel();
      }
    } else {
      showMessage(
        "Radio Falhou",
        "Houve um problema ao atualizar sua radio, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteChannel = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Channel/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getChannel();
        onCloseEditChannel();
        showMessage(
          "Radio Deletada",
          "Radio selecionada foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Loja Falhou",
        "Houve um problema ao deletar sua radio, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onSelectId = (value) => {
    setSelectId(value);
  };

  const handleChangeIndex = (index) => {
    setPageIndex(index);
  };

  const onPage = (value) => {
    handleChangeIndex(value);
  };

  return (
    <Page className={classes.root} title="Canais de Rádios">
      <Container maxWidth={false}>
        <OptionChannel onPage={onPage} />
        <Divider style={{ marginTop: 25 }} />

        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={pageIndex}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel value={pageIndex} index={0} dir={theme.direction}>
            <div className="h-100">
              <Toolbar
                SearchChannel={buscaChannel}
                onShowNewChannel={onShowNewChannel}
              />
              <Box m={-2} mt={3}>
                {isLoading && <LinearProgress />}
                <Results
                  onShowModalEditChannel={onShowEditChannel}
                  onShowModalSettingChannel={onShowModalSettingChannel}
                  onDeleteChannel={onShowConfirm}
                  customers={Channel}
                  isLoading={isLoading}
                  titlesTopGrid={titlesTopGrid}
                  onSelectId={onSelectId}
                  isLoadingSettings={isLoadingSettings}
                />
              </Box>

              <div>
                {selectId.length > 0 && (
                  <BottomMultSelect
                    onNewCommercial={onShowNewCommercial}
                    onNewVignette={onShowNewVignette}
                    onEditSetting={onShowEditSetting}
                  />
                )}
              </div>
            </div>
          </TabPanel>
          <TabPanel value={pageIndex} index={1} dir={theme.direction}>
            <Genre />
          </TabPanel>
          <TabPanel value={pageIndex} index={2} dir={theme.direction}>
            <GenreSub />
          </TabPanel>
          <TabPanel value={pageIndex} index={3} dir={theme.direction}>
            <Music />
          </TabPanel>
          <TabPanel value={pageIndex} index={4} dir={theme.direction}>
            <ContentGroup />
          </TabPanel>
          <TabPanel value={pageIndex} index={5} dir={theme.direction}>
            <TypeContent />
          </TabPanel>
          <TabPanel value={pageIndex} index={6} dir={theme.direction}>
            <Content />
          </TabPanel>
          <TabPanel value={pageIndex} index={7} dir={theme.direction}>
            <TypeVignette />
          </TabPanel>
          <TabPanel value={pageIndex} index={8} dir={theme.direction}>
            <PhraseSegment />
          </TabPanel>
        </SwipeableViews>
      </Container>

      <ModalNewChannel
        open={showNewChannel}
        onCloseNewChannel={onCloseNewChannel}
        getAllChannel={getChannel}
      />
      <ModalNewVignette
        open={showNewVignette}
        selectId={selectId}
        onCloseNewVignette={onCloseNewVignette}
      />
      <ModalNewCommercial
        open={showNewCommercial}
        selectId={selectId}
        onCloseNewCommercial={onCloseNewCommercial}
      />
      <ModalSettingChannel
        open={showSetting}
        selectId={idClient}
        getAllChannel={() => getChannel()}
        getSettingChannelModify={getSettingChannelModify}
        onCloseSettingChannel={onCloseSettingChannel}
      />
      <ModalEditSettingChannel
        open={showEditSetting}
        selectId={selectId}
        getAllChannel={() => getChannel()}
        onCloseEditSettingChannel={onCloseEditSetting}
      />
      <ModalEditChannel
        getEditChannel={getEditChannel}
        open={showEditChannel}
        isLoading={isLoading}
        onCloseEditChannel={onCloseEditChannel}
        onDeleteChannel={onDeleteChannel}
        onUpdateChannel={onUpdateChannel}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a Loja será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
