import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import SreenSettingChannel from "./SreenSettingChannel";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalSettingChannel = ({
  open,
  onCloseSettingChannel,
  getAllChannel,
  selectId,
  getSettingChannelModify
}) => {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onCloseSettingChannel}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4">
              <SreenSettingChannel
                onCloseModal={onCloseSettingChannel}
                getAllChannel={getAllChannel}
                idClient={selectId}
                getSettingChannelModify={getSettingChannelModify}
              />
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalSettingChannel;
