import React from "react";
import { Box, Card, Button } from "@material-ui/core";
import {
  AddCircleOutline as AddCircleIcon,
  RecordVoiceOverOutlined as RecordVoiceOverOutlinedIcon,
  RadioOutlined as RadioOutlinedIcon,
  SettingsOutlined as SettingsOutlinedIcon,
} from "@material-ui/icons";

export default function multSelectVignetteCommercial({
  onNewCommercial,
  onNewVignette,
  onEditSetting
}) {
  return (
    <div className="fixed-bottom m-4">
      <Card style={{ borderRadius: 20 }} className="fixed-bottom p-4 m-4">
        <Box display="flex" justifyContent="flex-end">
          <Button
            onClick={onEditSetting}
            startIcon={<SettingsOutlinedIcon />}
            style={{ borderRadius: 20 }}
            className="mr-2"
            variant="contained"
          >
            Configurações
          </Button>
          <Button
            onClick={onNewCommercial}
            startIcon={<RadioOutlinedIcon />}
            style={{ borderRadius: 20 }}
            className="mr-2"
            variant="contained"
          >
            Novo Comercial
          </Button>
          <Button
            onClick={onNewVignette}
            startIcon={<RecordVoiceOverOutlinedIcon />}
            style={{ borderRadius: 20 }}
            className="mr-2"
            variant="contained"
          >
            Nova Vinheta
          </Button>
        </Box>
      </Card>
    </div>
  );
}
