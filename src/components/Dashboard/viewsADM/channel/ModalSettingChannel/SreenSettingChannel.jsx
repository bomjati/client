import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api, { apiReceitaWs, apiViaCepWs } from "../../../../../services/api";
import Vignette from "./vignette/VignetteListView";
import Commercial from "./commercial/CommercialListView";
import ModalNewVignette from "../ModalVignette";
import ModalNewCommercial from "../ModalCommercial";
import { v4 } from "uuid";

const Formulario = ({
  onCloseModal,
  getAllChannel,
  idClient,
  getSettingChannelModify,
}) => {
  let history = useNavigate();
  const [showNewVignette, setShowNewVignette] = useState(false);
  const [showNewCommercial, setShowNewCommercial] = useState(false);
  const [activeGetAllCommertial, setActiveGetAllCommertial] = useState(false);
  const [activeGetAllVignette, setActiveGetAllVignette] = useState(false);
  const [isRuleVignetteDefault, setIsRuleVignetteDefault] = useState(true);
  const [isRuleVignetteSazonal, setIsRuleVignetteSazonal] = useState(false);
  const [
    isRuleVignetteInstitucional,
    setIsRuleVignetteInstitucional,
  ] = useState(false);
  const [isRuleVignetteComercial, setIsRuleVignetteComercial] = useState(true);
  const [sazonalSelect, setSazonalSelect] = useState();
  const [institucionalSelect, setInstitucionalSelect] = useState();
  const [listVignette, setListVignette] = useState(null);
  const [value, setValue] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    qtdMusic: 0,
    qtdMusicCommertial: 0,
    isRuleVignetteDefault: "false",
    isRuleVignetteComercial: "false",
    sazonalVignetteId: 0,
    institucionalVignetteId: 0,
  });

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const onShowNewVignette = () => {
    setShowNewVignette(true);
  };

  const onCloseNewVignette = () => {
    setShowNewVignette(false);
  };

  const onShowNewCommercial = () => {
    setShowNewCommercial(true);
  };

  const onCloseNewCommercial = () => {
    setShowNewCommercial(false);
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    if (isRuleVignetteSazonal & (values.sazonalVignetteId < 1)) {
      showMessage(
        "Atenção",
        `Você ativou vinheta Sazonal e não selecionou um opção.`,
        "warning"
      );
      return;
    }

    if (isRuleVignetteInstitucional & (values.institucionalVignetteId < 1)) {
      showMessage(
        "Atenção",
        `Você ativou vinheta Institucional e não selecionou um opção.`,
        "warning"
      );
      return;
    }

    if (values.qtdMusic < 1) {
      showMessage(
        "Atenção",
        `Campo Qtd. Musica precisa ser preenchido com um valor maior que 0.`,
        "warning"
      );
      return;
    }

    if (values.qtdMusicCommertial < 1) {
      showMessage(
        "Atenção",
        `Campo Qtd. Musica precisa ser preenchido com um valor maior que 0.`,
        "warning"
      );
      return;
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/clientsetting/edit/${getSettingChannelModify.clientId}`, values)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          showMessage(
            "Modificação de Canal",
            `realizada com sucesso.`,
            "success"
          );
          getAllChannel();
          setIsLoading(false);
          onCloseModal();
        } else {
          showMessage("Modificação de Channel", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllComerciais = () => {
    setActiveGetAllCommertial(true);
  };

  const disableGetAllCommercials = () => {
    setActiveGetAllCommertial(false);
  };

  const getAllVignette = () => {
    setActiveGetAllVignette(true);
  };

  const disableGetAllVignette = () => {
    setActiveGetAllVignette(false);
  };

  useEffect(() => {
    api
      .get(`/vignette/[${idClient}]`)
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListVignette(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    console.log(getSettingChannelModify);
    if (getSettingChannelModify) {
      if (getSettingChannelModify.sazonalVignetteId > 0) {
        setIsRuleVignetteSazonal(true);
      }
      if (getSettingChannelModify.institucionalVignetteId > 0) {
        setIsRuleVignetteInstitucional(true);
      }

      setValues({
        qtdMusic: getSettingChannelModify.qtdMusic,
        qtdMusicCommertial: getSettingChannelModify.qtdMusicCommertial,
        isRuleVignetteDefault: getSettingChannelModify.isRuleVignetteDefault,
        isRuleVignetteComercial:
          getSettingChannelModify.isRuleVignetteComercial,
        sazonalVignetteId: getSettingChannelModify.sazonalVignetteId,
        institucionalVignetteId:
          getSettingChannelModify.institucionalVignetteId,
      });
    }
  }, [getSettingChannelModify]);

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu canal"
            title="Configurações do Canal"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Configurações" {...a11yProps(0)} />
                <Tab label="Comerciais" {...a11yProps(1)} />
                <Tab label="Vinhetas" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Box mt={-2}>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={JSON.parse(values.isRuleVignetteDefault)}
                          onChange={(event) => {
                            setValues({
                              ...values,
                              isRuleVignetteDefault: String(
                                event.target.checked
                              ),
                            });
                          }}
                          name="isRuleVignetteDefault"
                          color="primary"
                        />
                      }
                      label="Vinheta será executada após qualquer conteúdo!"
                    />

                    <Divider className="mt-1 mb-1" />
                  </Box>
                  <Box>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={isRuleVignetteSazonal}
                          onChange={(event) => {
                            setIsRuleVignetteSazonal(event.target.checked);
                            setValues({
                              ...values,
                              sazonalVignetteId: !event.target.checked
                                ? 0
                                : getSettingChannelModify.sazonalVignetteId,
                            });
                          }}
                          name="isRuleVignetteSazonal"
                          color="primary"
                        />
                      }
                      label="Selecionar vinheta para executar após conteúdo Sazonal."
                    />
                    {isRuleVignetteSazonal ? (
                      <Box mt={1} mb={4} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Vinheta
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="vignetteId"
                            id="vignetteId"
                            value={values.sazonalVignetteId}
                            onChange={(event) => {
                              setValues({
                                ...values,
                                sazonalVignetteId: event.target.value,
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listVignette &&
                              listVignette.map((Vignettes) => (
                                <option value={Vignettes.id}>{`${
                                  Vignettes.name
                                } | ${
                                  Vignettes.Typevignette
                                    ? Vignettes.Typevignette.name
                                    : ""
                                }`}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    ) : null}

                    <Divider className="mt-1 mb-1" />
                  </Box>
                  <Box>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={isRuleVignetteInstitucional}
                          onChange={(event) => {
                            setIsRuleVignetteInstitucional(
                              event.target.checked
                            );

                            setValues({
                              ...values,
                              institucionalVignetteId: !event.target.checked
                                ? 0
                                : getSettingChannelModify.institucionalVignetteId,
                            });
                          }}
                          name="isRuleVignetteInstitucional"
                          color="primary"
                        />
                      }
                      label="Selecionar vinheta para executar após conteúdo Institucional."
                    />
                    {isRuleVignetteInstitucional ? (
                      <Box mt={1} mb={4} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Vinheta
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="vignetteId"
                            id="vignetteId"
                            value={values.institucionalVignetteId}
                            onChange={(event) => {
                              setValues({
                                ...values,
                                institucionalVignetteId: event.target.value,
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listVignette &&
                              listVignette.map((Vignettes) => (
                                <option value={Vignettes.id}>{`${
                                  Vignettes.name
                                } | ${
                                  Vignettes.Typevignette
                                    ? Vignettes.Typevignette.name
                                    : ""
                                }`}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    ) : null}
                    <Divider className="mt-1 mb-1" />
                  </Box>
                  <Box>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={JSON.parse(values.isRuleVignetteComercial)}
                          onChange={(event) => {
                            setValues({
                              ...values,
                              isRuleVignetteComercial: String(
                                event.target.checked
                              ),
                            });
                          }}
                          name="isRuleVignetteComercial"
                          color="primary"
                        />
                      }
                      label="Executar vinheta após comercial?"
                    />

                    <Divider className="mt-1 mb-1" />
                  </Box>
                  <Box>
                    <Typography className="mt-4 text-center">
                      Quantidade de músicas que executaram antes do conteúdo
                    </Typography>
                    <Box display="flex" justifyContent="center">
                      <TextField
                        value={values.qtdMusic}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            qtdMusic:
                              event.target.value < 0 ? 0 : event.target.value,
                          });
                        }}
                        label="Qtd. Música"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        type="number"
                        required
                        className="text-center"
                        style={{ width: 120 }}
                      />
                    </Box>
                  </Box>
                  <Divider className="mt-1 mb-1" />
                  <Box>
                    <Typography className="mt-4 text-center">
                      Quantidade de músicas que executaram antes do comercial
                    </Typography>
                    <Box display="flex" justifyContent="center">
                      <TextField
                        value={values.qtdMusicCommertial}
                        onChange={(event) => {
                          setValues({
                            ...values,
                            qtdMusicCommertial:
                              event.target.value < 0 ? 0 : event.target.value,
                          });
                        }}
                        label="Qtd. Música"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        type="number"
                        required
                        className="text-center"
                        style={{ width: 120 }}
                      />
                    </Box>
                  </Box>
                  <Divider className="mt-1 mb-1" />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Commercial
                    onShowNewCommercial={onShowNewCommercial}
                    activeGetAllCommertial={activeGetAllCommertial}
                    disableGetAllCommercials={disableGetAllCommercials}
                    idClient={idClient}
                  />
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                  <Vignette
                    onShowNewVignette={onShowNewVignette}
                    activeGetAllVignette={activeGetAllVignette}
                    disableGetAllVignette={disableGetAllVignette}
                    idClient={idClient}
                  />
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          {value == 0 ? (
            <Box
              display="flex"
              justifyContent="flex-end"
              alignItems="center"
              className="w-100"
              p={2}
            >
              <Button
                onClick={onUpdate}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#10ac84",
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<CheckIcon style={{ color: "white" }} />}
              >
                Salvar
              </Button>
              {isLoading && <CircularProgress />}
            </Box>
          ) : null}
        </Card>
      </Container>

      <ModalNewVignette
        open={showNewVignette}
        selectId={[idClient]}
        onCloseNewVignette={onCloseNewVignette}
        getAllVinhetas={getAllVignette}
      />
      <ModalNewCommercial
        open={showNewCommercial}
        selectId={[idClient]}
        onCloseNewCommercial={onCloseNewCommercial}
        getAllComerciais={getAllComerciais}
      />
    </form>
  );
};

export default Formulario;
