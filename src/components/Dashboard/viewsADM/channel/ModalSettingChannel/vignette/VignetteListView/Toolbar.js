import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
} from "@material-ui/core";
import { Search as SearchIcon } from "react-feather";
import AddCircleIcon from "@material-ui/icons/AddCircleOutline";

const useStyles = makeStyles((theme) => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1),
  },
  exportButton: {
    marginRight: theme.spacing(1),
  },
}));

const Toolbar = ({ onShowNewVignette, SearchVignette, className, ...rest }) => {
  const classes = useStyles();

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Box mt={-4} display="flex" justifyContent="flex-end">
        <Button
           onClick={onShowNewVignette}
          color="default"
          variant="contained"
          style={{ borderRadius: 50, backgroundColor: "#2c2c54", color: "white" }}
          size="large"
          startIcon={<AddCircleIcon />}
        >
          Nova Vinheta
        </Button>
      </Box>
      <Box mt={3}>
        <Card style={{ borderRadius: 20 }}>
          <CardContent>
            <Box maxWidth={500}>
              <TextField
                fullWidth
                onChange={(event) => {
                  SearchVignette(event.target.value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon fontSize="small" color="action">
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  ),
                }}
                placeholder="Procurar Gênero"
                variant="outlined"
              />
            </Box>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string,
};

export default Toolbar;
