import React, { useState, useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Chip,
  Tooltip,
} from "@material-ui/core";
import getInitials from "../../../../../utils/getInitials";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Today as TodayIcon,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  onDeleteVignette,
  onShowEditVignette,
  onIdsVignette,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(3);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  useEffect(() => {
    onIdsVignette(selectedCustomerIds);
  }, [selectedCustomerIds]);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={780}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <TableRow
                      hover
                      key={customer.id}
                      selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={
                            selectedCustomerIds.indexOf(customer.id) !== -1
                          }
                          onChange={(event) =>
                            handleSelectOne(event, customer.id)
                          }
                          value="true"
                        />
                      </TableCell>
                      <TableCell align="left">
                        <Box alignItems="center" display="flex">
                          {customer.name}
                        </Box>
                      </TableCell>
                      <TableCell align="left">
                        <Chip
                          style={{
                            borderColor: "#3498db",
                            backgroundColor: "#3498db",
                            color: "white",
                          }}
                          label={
                            customer.Typevignette
                              ? customer.Typevignette.name
                              : ""
                          }
                          variant="outlined"
                        />
                      </TableCell>
                      <TableCell align="right">
                        <Tooltip title="Modifica Vencimento" arrow>
                          <IconButton
                            disabled={isLoading}
                            style={{ marginLeft: 5 }}
                            onClick={() => {
                              onShowEditVignette(customer);
                            }}
                            color="inherit"
                          >
                            <TodayIcon style={{ color: "#474787" }} />
                          </IconButton>
                        </Tooltip>
                        <Tooltip title="Deletar Vinheta" arrow>
                          <IconButton
                            disabled={isLoading}
                            style={{ marginLeft: 5 }}
                            onClick={() => {
                              onDeleteVignette(customer.id);
                            }}
                            color="inherit"
                          >
                            <DeleteIcon style={{ color: "#ff4d4d" }} />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 3, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
