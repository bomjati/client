import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../../../services/api";
import { showMessage } from "../../../../../utils/message";
import MessageConfirm from "../../../../../utils/MessageConfirm";
import EditCommercial from "../ModalEditCommercial";
import MultSelect from "./multSelectVignetteCommercial";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = ({
  idClient,
  onShowNewCommercial,
  activeGetAllCommertial,
  disableGetAllCommercials,
}) => {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [showConfirmLote, setShowConfirmLote] = useState(false);
  const [Commercial, setCommercial] = useState([]);
  const [CommercialBusca, setCommercialBusca] = useState([]);
  const [showEditCommercial, setShowEditCommercial] = useState(false);
  const [idCommercialModify, setIdCommercialModify] = useState(0);
  const [idsCommercial, setIdsCommercial] = useState([]);
  const [getCommercialModify, setGetCommercialModify] = useState();
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    if (idClient) {
      getCommercial(idClient);
    }
  }, [idClient]);

  useEffect(() => {
    if (activeGetAllCommertial) {
      getCommercial(idClient);
      disableGetAllCommercials();
    }
  }, [activeGetAllCommertial]);

  const buscaCommercial = (value) => {
    let resultBusca = CommercialBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    if (!(resultBusca.length > 0)) {
      resultBusca = CommercialBusca.filter((i) => {
        return (
          String(i.Right["name"])
            .toUpperCase()
            .indexOf(String(value).toUpperCase()) > -1
        );
      });
    }

    setCommercial(resultBusca);
  };

  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdCommercialModify(id);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteCommercial(idCommercialModify);
  };

  const onShowConfirmLote = () => {
    setShowConfirmLote(true);
  };

  const onCloseMessageConfirmLote = () => {
    setShowConfirmLote(false);
  };

  const onAcceptMessageConfirmLote = () => {
    setShowConfirm(false);
    onDeleteLote();
  };

  const getCommercial = async (id) => {
    setIsLoading(true);
    const res = await api.get(`/Commercial/[${id}]`);

    if (res) {
      setCommercial(res.data);
      setCommercialBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeleteCommercial = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Commercial/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getCommercial(idClient);
        showMessage(
          "Comercial Deletado",
          "Comercial selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Comercial Falhou",
        "Houve um problema ao deletar seu comercial, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onCloseEditCommercial = () => {
    setShowEditCommercial(false);
  };

  const onShowEditCommercial = (data) => {
    setGetCommercialModify(data);
    setShowEditCommercial(true);
  };

  const onIdsCommercial = (ids) => {
    setIdsCommercial(ids);
  };

  const onDeleteLote = () => {
    setIsLoading(true);
    for (let index = 0; index < idsCommercial.length; index++) {
      const element = idsCommercial[index];

      api
        .delete(`/Commercial/${element}`)
        .then((response) => response.data)
        .then((res) => {
          if (res.message === "success") {
            if (index + 1 == idsCommercial.length) {
              getCommercial(idClient);
              showMessage(
                "Comercial Deletado",
                "Comercial selecionado foi deletado com sucesso.",
                "success"
              );
              setIsLoading(false);
            }
          } else {
            showMessage(
              "Comercial Falhou",
              "Houve um problema ao deletar seu Comercial, entre em contato com o suporte.",
              "danger"
            );
          }
        });
    }
  };

  return (
    <Page className={classes.root} title="Comerciais">
      <Container maxWidth={false}>
        <Toolbar
          onShowNewCommercial={onShowNewCommercial}
          SearchCommercial={buscaCommercial}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onDeleteCommercial={onShowConfirm}
            onShowEditCommercial={onShowEditCommercial}
            onIdsCommercial={onIdsCommercial}
            customers={Commercial}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
        {idsCommercial.length > 0 && (
          <MultSelect onDelete={() => onShowConfirmLote()} />
        )}
      </Container>
      <EditCommercial
        open={showEditCommercial}
        selectId={[idClient]}
        getCommercialModify={getCommercialModify}
        getAllComerciais={() => getCommercial(idClient)}
        onCloseNewCommercial={onCloseEditCommercial}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o comercial será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
      <MessageConfirm
        open={showConfirmLote}
        textInfo={`Você confirmando essa operação, os comerciais serão deletadas de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirmLote}
        onAccept={onAcceptMessageConfirmLote}
      />
    </Page>
  );
};

export default CompanyListView;
