import React, { useState, useContext, useEffect } from "react";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import { diasSemanais } from "./data";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import MessageConfirm from "../../../utils/MessageConfirm";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  duration,
} from "@material-ui/core";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
} from "@material-ui/icons";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  AccessTime as AccessTimeIcon,
  Delete as DeleteIcon,
  AddCircleOutline as AddCircleIcon,
} from "@material-ui/icons";
import { getAllByAltText } from "@testing-library/react";

let test = "";
const Formulario = ({ onCloseModal, isLoadingModal, selectId, getAllComerciais }) => {
  let history = useNavigate();
  const fileToArrayBuffer = require("file-to-array-buffer");
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listTypeCommercial, setListTypeCommercial] = useState(null);
  const [value, setValue] = useState(0);
  const [listClient, setListClient] = useState("");
  const [listSegment, setListSegment] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isRuleHour, setIsRuleHour] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [selectedFile, setSelectedFile] = useState();
  const [listConcluded, setListConcluded] = useState([]);
  const [listDuration, setListDuration] = useState(null);
  const [hourSelect, setHourSelect] = useState();
  const [dateSelect, setDateSelect] = useState({
    1: {
      startDate: moment().format("YYYY-MM-DD"),
      dueDate: moment().format("YYYY-MM-DD"),
    },
  });
  const [weekSelect, setWeekSelect] = useState({
    sun: "true",
    mon: "true",
    tue: "true",
    wed: "true",
    thu: "true",
    fri: "true",
    sat: "true",
  });

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }

    setSelectedFile(e.target.files);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onSave();
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    if (!selectedFile) {
      showMessage(
        "Atenção",
        `Selecione ao menos uma Comercial antes de salvar`,
        "warning"
      );
      setValue(0);

      return;
    }

    if (hourSelect && (Object.keys(hourSelect).length == 1) & isRuleHour) {
      if (hourSelect["1"].startHour == hourSelect["1"].endHour) {
        showMessage(
          "Atenção",
          `Seu periodo de horario selecionado estão identicos, favor modifica-los.`,
          "warning"
        );

        return;
      }
    }

    if (Object.keys(dateSelect).length == 1) {
      if (
        moment(dateSelect["1"].startDate).format("DD/MM/YYYY") ==
        moment(dateSelect["1"].dueDate).format("DD/MM/YYYY")
      ) {
        setValue(2 + 0);
        setShowConfirm(true);

        return;
      }
    }

    onSave();
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    isLoadingModal(true);
    setIsLoading(true);

    for (let index = 0; index < selectId.length; index++) {
      const element = selectId[index];

      for (let indexx = 0; indexx < selectedFile.length; indexx++) {
        const elementFile = selectedFile[indexx];

        const FormDataCommercial = new FormData();
        FormDataCommercial.append("clientId", element);
        FormDataCommercial.append(
          "cliente",
          listClient.filter((c) => c.id == element)[0].name
        );
        FormDataCommercial.append("userFile", elementFile);

        api
          .post("/Commercial/new", FormDataCommercial)
          .then((response) => response.data)
          .then((commercial) => {
            if (commercial.message == "success") {
              console.log({
                ...weekSelect,
                commercialId: commercial.data.id,
              });
              api
                .post("/weekdayrule/new", {
                  ...weekSelect,
                  commercialId: commercial.data.id,
                })
                .then((response) => response.data)
                .then((resp) => {
                  if (resp.message == "success") {
                    let arrayRefreshDays = {};

                    for (const key in dateSelect) {
                      if (dateSelect.hasOwnProperty(key)) {
                        const elementH = dateSelect[key];

                        arrayRefreshDays = {
                          ...arrayRefreshDays,
                          [key]: {
                            ...elementH,
                            commercialId: commercial.data.id,
                          },
                        };
                      }
                    }

                    api
                      .post("/day/new", arrayRefreshDays)
                      .then((response) => response.data)
                      .then((resp) => {
                        if (resp.message == "success") {
                          if (isRuleHour) {
                            let arrayRefreshHours = {};

                            for (const key in hourSelect) {
                              if (hourSelect.hasOwnProperty(key)) {
                                const elementH = hourSelect[key];

                                arrayRefreshHours = {
                                  ...arrayRefreshHours,
                                  [key]: {
                                    ...elementH,
                                    commercialId: commercial.data.id,
                                  },
                                };
                              }
                            }

                            api
                              .post("/hour/new", arrayRefreshHours)
                              .then((response) => response.data)
                              .then((resp) => {
                                let arrayConcluded = listConcluded;
                                arrayConcluded.push({ key: index });
                                setListConcluded(arrayConcluded);

                                if (resp.message == "success") {
                                  if (
                                    (index + 1 == selectId.length) &
                                    (indexx + 1 == selectedFile.length)
                                  ) {
                                    showMessage(
                                      "Novos Comerciais",
                                      `Cadastro realizado com sucesso.`,
                                      "success"
                                    );
                                    setIsLoading(false);
                                    isLoadingModal(false);
                                    getAllComerciais();
                                    onCloseModal();
                                  }
                                } else {
                                  showMessage(
                                    "Novo Comercial",
                                    `Falhou.`,
                                    "error"
                                  );
                                }
                              })
                              .catch((error) => {
                                console.log(error);
                              });
                          } else {
                            if (
                              (index + 1 == selectId.length) &
                              (indexx + 1 == selectedFile.length)
                            ) {
                              showMessage(
                                "Novos Comerciais",
                                `Cadastro realizado com sucesso.`,
                                "success"
                              );
                              setIsLoading(false);
                              isLoadingModal(false);
                              getAllComerciais();
                              onCloseModal();
                            }
                          }
                        } else {
                          showMessage("Novo Comercial", `Falhou.`, "error");
                        }
                      })
                      .catch((error) => {
                        console.log(error);
                      });
                  } else {
                    showMessage("Novo Comercial", `Falhou.`, "error");
                  }
                })
                .catch((error) => {
                  console.log(error);
                });
            } else {
              showMessage("Novo Comercial", `Falhou.`, "error");
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    }
  };

  const getNameWeek = (name) => {
    return name == "sun"
      ? "dom"
      : name == "mon"
      ? "seg"
      : name == "tue"
      ? "ter"
      : name == "wed"
      ? "qua"
      : name == "thu"
      ? "qui"
      : name == "fri"
      ? "sex"
      : name == "sat"
      ? "sáb"
      : "";
  };

  useEffect(() => {
    api
      .get(`/client/array/${JSON.stringify(selectId)}`)
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListClient(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    console.log(weekSelect);
  }, [weekSelect]);

  return (
    <form autoComplete="off" Novolidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={!isLoading && onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu Comercial"
            title="Novo Comercial"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do Comercial" {...a11yProps(0)} />
                <Tab label="Periodo de Data" {...a11yProps(1)} />
                {isRuleHour && (
                  <Tab label="Periodo de Horas" {...a11yProps(2)} />
                )}
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    {selectedFile ? (
                      [...selectedFile].map((Commercial, index) => (
                        <Grid item md={12} xs={12}>
                          <Card className="p-2">
                            <Box display="flex" alignItems="center">
                              {isLoading &&
                                !(
                                  listConcluded.filter(
                                    (concluded) => concluded.key == index
                                  ).length > 0
                                ) && (
                                  <CircularProgress
                                    className="mr-2"
                                    size={15}
                                  />
                                )}
                              {listConcluded.filter(
                                (concluded) => concluded.key == index
                              ).length > 0 && (
                                <CheckIcon
                                  className="mr-2"
                                  style={{ color: "#2ecc71" }}
                                />
                              )}
                              <h6>{`${Commercial.name}`}</h6>
                            </Box>
                          </Card>
                        </Grid>
                      ))
                    ) : (
                      <Grid item md={12} xs={12}>
                        <Card className="p-2">
                          <Box display="flex" alignItems="center">
                            <h6>{"Nenhum comercial selecionado."}</h6>
                          </Box>
                        </Card>
                      </Grid>
                    )}
                  </Grid>

                  <Button
                    className="mt-4"
                    color="primary"
                    variant="outlined"
                    fullWidth
                    disabled={isLoading}
                    htmlFor="sampleFile"
                    component="label"
                    style={{ borderRadius: 20 }}
                  >
                    Buscar Comercial
                  </Button>

                  <input
                    onChange={onSelectFile}
                    multiple
                    type="file"
                    id="sampleFile"
                    style={{ display: "none" }}
                  />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Box display="flex" justifyContent="center">
                    {diasSemanais.map((d) => (
                      <Button
                        style={{
                          backgroundColor:
                          JSON.parse(String(weekSelect[String(d.name)]).toLowerCase()) && "#34495e",
                          color: JSON.parse(String(weekSelect[String(d.name)]).toLowerCase()) && "white",
                          fontWeight: "bold",
                          borderRadius: 20,
                        }}
                        onClick={() => {
                          setWeekSelect({
                            ...weekSelect,
                            [d.name]: String(!JSON.parse(String(weekSelect[String(d.name)]).toLowerCase())),
                          });
                        }}
                        className="mr-2"
                        variant="contained"
                      >
                        {getNameWeek(d.name)}
                      </Button>
                    ))}
                  </Box>

                  <Divider className="mt-4 mb-4" />

                  {Object.keys(dateSelect).map((key, index) => (
                    <Box mt={2} display="flex" justifyContent="center">
                      {!(index == 0) | (Object.keys(dateSelect).length > 1) ? (
                        <IconButton
                          disabled={isLoading}
                          style={{ marginRight: 5 }}
                          onClick={() => {
                            let arryRefresh = {};
                            let countColum = 0;
                            for (var keys in dateSelect) {
                              if (keys !== key) {
                                countColum = countColum + 1;
                                arryRefresh = {
                                  ...arryRefresh,
                                  [countColum]: dateSelect[keys],
                                };
                              }
                            }

                            setDateSelect(arryRefresh);
                          }}
                          color="inherit"
                        >
                          <DeleteIcon style={{ color: "#ff4d4d" }} />
                        </IconButton>
                      ) : (
                        <div style={{ width: 50 }} />
                      )}
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          disableToolbar
                          inputVariant="outlined"
                          format="dd/MM/yyyy"
                          id="date-picker-outlined"
                          label="Inicio"
                          name="startDate"
                          value={moment(
                            dateSelect[key].startDate,
                            "YYYY-MM-DD"
                          )}
                          onChange={(event) => {
                            setDateSelect({
                              ...dateSelect,
                              [key]: {
                                startDate: moment(event).format("YYYY-MM-DD"),
                                dueDate: dateSelect[key].dueDate,
                              },
                            });
                          }}
                          style={{ width: 170 }}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>

                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          className="ml-4"
                          disableToolbar
                          inputVariant="outlined"
                          format="dd/MM/yyyy"
                          id="date-picker-outlined"
                          label="Enceramento"
                          name="dueDate"
                          value={moment(dateSelect[key].dueDate, "YYYY-MM-DD")}
                          onChange={(event) => {
                            setDateSelect({
                              ...dateSelect,
                              [key]: {
                                startDate: dateSelect[key].startDate,
                                dueDate: moment(event).format("YYYY-MM-DD"),
                              },
                            });
                          }}
                          style={{ width: 170 }}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </MuiPickersUtilsProvider>

                      {Object.keys(dateSelect).length == index + 1 ? (
                        <IconButton
                          disabled={isLoading}
                          style={{ marginLeft: 5 }}
                          onClick={() => {
                            setDateSelect({
                              ...dateSelect,
                              [Object.keys(dateSelect).length + 1]: {
                                startDate: new Date(),
                                dueDate: new Date(),
                              },
                            });
                          }}
                          color="inherit"
                        >
                          <AddCircleIcon style={{ color: "#2ecc71" }} />
                        </IconButton>
                      ) : (
                        <div style={{ width: 50 }} />
                      )}
                    </Box>
                  ))}

                  <Divider className="mt-4 mb-2" />

                  <Box className="mb-2">
                    <FormControlLabel
                      control={
                        <Switch
                          checked={isRuleHour}
                          onChange={(event) => {
                            if (event.target.checked) {
                              setValue(2);
                              setHourSelect({
                                1: {
                                  startHour: moment().format("HH:mm"),
                                  endHour: moment().format("HH:mm"),
                                },
                              });
                            } else {
                              setHourSelect(null);
                            }
                            setIsRuleHour(event.target.checked);
                          }}
                          name="isRuleHour"
                          color="primary"
                        />
                      }
                      label="Habilitar periodo de horário"
                    />
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                  {isRuleHour &&
                    Object.keys(hourSelect).map((key, index) => (
                      <Box m={2} display="flex" justifyContent="center">
                        {!(index == 0) |
                        (Object.keys(hourSelect).length > 1) ? (
                          <IconButton
                            disabled={isLoading}
                            style={{ marginRight: 5 }}
                            onClick={() => {
                              let arryRefresh = {};
                              let countColum = 0;
                              for (var keys in hourSelect) {
                                if (keys !== key) {
                                  countColum = countColum + 1;

                                  arryRefresh = {
                                    ...arryRefresh,
                                    [countColum]: hourSelect[keys],
                                  };
                                }
                              }

                              setHourSelect(arryRefresh);
                            }}
                            color="inherit"
                          >
                            <DeleteIcon style={{ color: "#ff4d4d" }} />
                          </IconButton>
                        ) : (
                          <div style={{ width: 50 }} />
                        )}

                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            id="time-picker"
                            ampm={false}
                            inputVariant="outlined"
                            label="Time picker"
                            name="startHour"
                            label="Hora Inicio"
                            value={moment(
                              `${moment().format("YYYY-MM-DD")} ${
                                hourSelect[key].startHour
                              }`,
                              "YYYY-MM-DD HH:mm"
                            )}
                            onChange={(event) => {
                              setHourSelect({
                                ...hourSelect,
                                [key]: {
                                  startHour: moment(event).format("HH:mm"),
                                  endHour: hourSelect[key].endHour,
                                },
                              });
                            }}
                            style={{ width: 125, marginRight: 10 }}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            keyboardIcon={<AccessTimeIcon />}
                          />
                        </MuiPickersUtilsProvider>

                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                          <KeyboardTimePicker
                            id="time-picker"
                            ampm={false}
                            inputVariant="outlined"
                            label="Time picker"
                            name="endHour"
                            label="Hora Fim"
                            value={moment(
                              `${moment().format("YYYY-MM-DD")} ${
                                hourSelect[key].endHour
                              }`,
                              "YYYY-MM-DD HH:mm"
                            )}
                            onChange={(event) => {
                              setHourSelect({
                                ...hourSelect,
                                [key]: {
                                  endHour: moment(event).format("HH:mm"),
                                  startHour: hourSelect[key].startHour,
                                },
                              });
                            }}
                            style={{ width: 125 }}
                            KeyboardButtonProps={{
                              "aria-label": "change time",
                            }}
                            keyboardIcon={<AccessTimeIcon />}
                          />
                        </MuiPickersUtilsProvider>

                        {Object.keys(hourSelect).length == index + 1 ? (
                          <IconButton
                            disabled={isLoading}
                            style={{ marginLeft: 5 }}
                            onClick={() => {
                              setHourSelect({
                                ...hourSelect,
                                [Object.keys(hourSelect).length + 1]: {
                                  startHour: moment().format("HH:mm"),
                                  endHour: moment().format("HH:mm"),
                                },
                              });
                            }}
                            color="inherit"
                          >
                            <AddCircleIcon style={{ color: "#2ecc71" }} />
                          </IconButton>
                        ) : (
                          <div style={{ width: 50 }} />
                        )}
                      </Box>
                    ))}
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            {value < 1 ? (
              <Button
                onClick={() => {
                  setValue(value + 1);
                }}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#3498db",
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<ArrowForwardIcon style={{ color: "white" }} />}
              >
                Próximo
              </Button>
            ) : (
              <div className="d-flex">
                <Button
                  onClick={() => {
                    setValue(value - 1);
                  }}
                  disabled={isLoading}
                  style={{
                    borderRadius: 50,
                    backgroundColor: "#f39c12",
                  }}
                  color="primary"
                  variant="contained"
                  size="medium"
                  className="mr-2"
                  startIcon={<ArrowBackIcon style={{ color: "white" }} />}
                >
                  Voltar
                </Button>
                {isRuleHour & (value < 2) ? (
                  <Button
                    onClick={() => {
                      setValue(value + 1);
                    }}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#3498db",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    startIcon={<ArrowForwardIcon style={{ color: "white" }} />}
                  >
                    Próximo
                  </Button>
                ) : (
                  <Button
                    onClick={onUpdate}
                    disabled={isLoading}
                    style={{
                      borderRadius: 50,
                      backgroundColor: "#10ac84",
                    }}
                    color="primary"
                    variant="contained"
                    size="medium"
                    startIcon={<CheckIcon style={{ color: "white" }} />}
                  >
                    Salvar
                  </Button>
                )}
              </div>
            )}
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>

      <MessageConfirm
        open={showConfirm}
        textInfo={`Observe, que o periodo de datas selecionado são iguais. Isso significa que esse comercial permanecerá em sua lista apena 1 dia, no caso o dia selecionado. Tem certeza dessa operação? `}
        title="Atenção"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </form>
  );
};

export default Formulario;
