import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewTypeVignette from "../ModalNewTypeVignette";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewTypeVignette, setShowNewTypeVignette] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditTypeVignette, setShowEditTypeVignette] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [TypeVignette, setTypeVignette] = useState([]);
  const [TypeVignetteBusca, setTypeVignetteBusca] = useState([]);
  const [getEditTypeVignette, setGetEditTypeVignette] = useState([]);
  const [idTypeVignetteModify, setIdTypeVignetteModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getTypeVignette();
  }, []);

  const buscaTypeVignette = (value) => {
    const resultBusca = TypeVignetteBusca.filter((i) => {
      return (
        String(i["name"])
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    setTypeVignette(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdTypeVignetteModify(id);
  };
  const onShowNewTypeVignette = () => {
    setShowNewTypeVignette(true);
  };

  const onCloseNewTypeVignette = () => {
    setShowNewTypeVignette(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditTypeVignette = (value) => {
    setShowEditTypeVignette(true);
    setGetEditTypeVignette(value);
  };

  const onCloseEditTypeVignette = () => {
    setShowEditTypeVignette(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteTypeVignette(idTypeVignetteModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getTypeVignette = async () => {
    setIsLoading(true);
    const res = await api.get("/TypeVignette");

    if (res) {
      setTypeVignette(res.data);
      setTypeVignetteBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeleteTypeVignette = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/TypeVignette/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getTypeVignette();
        onCloseEditTypeVignette();
        showMessage(
          "Tipo de Vinheta Deletado",
          "Tipo de Vinheta selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Tipo de Vinheta Falhou",
        "Houve um problema ao deletar seu Tipo de Vinheta, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Tipo de Vinheta">
      <Container maxWidth={false}>
        <Toolbar
          SearchTypeVignette={buscaTypeVignette}
          onShowNewTypeVignette={onShowNewTypeVignette}
        />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditTypeVignette={onShowEditTypeVignette}
            onDeleteTypeVignette={onShowConfirm}
            customers={TypeVignette}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewTypeVignette
        open={showNewTypeVignette}
        onCloseNewTypeVignette={onCloseNewTypeVignette}
        getAllTypeVignette={getTypeVignette}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Tipo de Vinheta será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
