import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewGenreTopic from "../ModalNewGenreTopic";
import ModalEditGenreTopic from "../ModalEditGenreTopic";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewGenreTopic, setShowNewGenreTopic] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditGenreTopic, setShowEditGenreTopic] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [GenreTopic, setGenreTopic] = useState([]);
  const [GenreTopicBusca, setGenreTopicBusca] = useState([]);
  const [getEditGenreTopic, setGetEditGenreTopic] = useState([]);
  const [idGenreTopicModify, setIdGenreTopicModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getGenreTopic();
  }, []);

  const buscaGenreTopic = (value) => {
    const resultBusca = GenreTopicBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setGenreTopic(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdGenreTopicModify(id);
  };
  const onShowNewGenreTopic = () => {
    setShowNewGenreTopic(true);
  };

  const onCloseNewGenreTopic = () => {
    setShowNewGenreTopic(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditGenreTopic = (value) => {
    setShowEditGenreTopic(true);
    setGetEditGenreTopic(value);
  };

  const onCloseEditGenreTopic = () => {
    setShowEditGenreTopic(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteGenreTopic(idGenreTopicModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getGenreTopic = async () => {
    setIsLoading(true);
    const res = await api.get("/GenreTopic");

    if (res) {
      setGenreTopic(res.data);
      setGenreTopicBusca(res.data);

      if (getEditGenreTopic.id) {
        setGetEditGenreTopic(
          res.data.filter((s) => s.id == getEditGenreTopic.id).length > 0
            ? res.data.filter((s) => s.id == getEditGenreTopic.id)[0]
            : [0]
        );
      }
    }

    setIsLoading(false);
  };

  const onDeleteGenreTopic = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/GenreTopic/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getGenreTopic();
        onCloseEditGenreTopic();
        showMessage(
          "Tópico de Gêneros Deletado",
          "Tópico de Gêneros selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Tópico de Gêneros Falhou",
        "Houve um problema ao deletar seu Tópico de Gêneros, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Tópico de Gêneros">
      <Container maxWidth={false}>
        <Toolbar
          SearchGenreTopic={buscaGenreTopic}
          onShowNewGenreTopic={onShowNewGenreTopic}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditGenreTopic={onShowEditGenreTopic}
            onDeleteGenreTopic={onShowConfirm}
            customers={GenreTopic}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewGenreTopic
        open={showNewGenreTopic}
        onCloseNewGenreTopic={onCloseNewGenreTopic}
        getAllGenreTopic={getGenreTopic}
      />
      <ModalEditGenreTopic
        open={showEditGenreTopic}
        onCloseEditGenreTopic={onCloseEditGenreTopic}
        getAllGenreTopic={getGenreTopic}
        dataGenreTopic={getEditGenreTopic}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Tópico de Gêneros será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
