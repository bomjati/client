import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  CardActions,
  GridList,
  Avatar,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Delete as DeleteIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  AddAPhoto as AddAPhotoIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import GenresubListView from "./GenresubListView";

const Formulario = ({
  onCloseModal,
  getAllGenresubTopic,
  dataGenresubTopic,
}) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listGenresubTopic, setListGenresubTopic] = useState(null);
  const [value, setValue] = useState(0);
  const [indexImage, setIndexImage] = useState(-1);
  const [typeClient, setTypeClient] = useState("juridica");
  const [preview, setPreview] = useState();
  const [selectedListControl, setSelectedListControl] = useState([]);
  const [listImagem, setListImagem] = useState([]);
  const [idGenresubTopic, setIdGenresubTopic] = useState(0);
  const [selectIdsGenresub, setSelectIdsGenresub] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [values, setValues] = useState({
    name: "",
  });

  const styleButtonIcon = {
    boxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    WebkitBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
    MozBoxShadow: "1px 0px 8px -3px rgba(0,0,0,0.6)",
  };

  useEffect(() => {
    console.log(dataGenresubTopic);
    if (dataGenresubTopic) {
      setValues({ ...values, name: dataGenresubTopic.name });
      setListImagem(dataGenresubTopic.Covers);
      setIdGenresubTopic(dataGenresubTopic.id);
      onGetListControl(dataGenresubTopic.id);
    }
  }, [dataGenresubTopic]);

  const onGetListControl = (id) => {
    setIsLoading(true);
    api
      .get(`/Genresubcontroltopic/genresub/${id}`)
      .then((response) => response.data)
      .then((res) => {
        if (res) {
          let array = [];

          for (let index = 0; index < res.length; index++) {
            const element = res[index];

            array.push(element.genresubId);
          }

          console.log(array);
          console.log(id);
          setSelectedListControl(array);
          setIsLoading(false);
        } else setIsLoading(false);
      });
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "name" ? " Nome do Tópico de Sub-gêneros " : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/GenresubTopic/edit/${dataGenresubTopic.id}`, values)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          api
            .delete(`/GenresubcontrolTopic/all/${dataGenresubTopic.id}`, values)
            .then((response) => response.data)
            .then((resp) => {
              if (resp.message == "success") {
                for (let index = 0; index < selectIdsGenresub.length; index++) {
                  const element = selectIdsGenresub[index];

                  api
                    .post(`/genresubcontroltopic/new`, {
                      genresubId: element,
                      genresubtopicId: dataGenresubTopic.id,
                    })
                    .then((response) => response.data)
                    .then((resp) => {
                      if (
                        (resp.message == "success") &
                        (selectIdsGenresub.length - 1 == index)
                      ) {
                        showMessage(
                          "Novo Tópico de Sub-gêneros",
                          `Cadastro realizado com sucesso.`,
                          "success"
                        );
                        getAllGenresubTopic();
                        setIsLoading(false);
                        onCloseModal();
                      } else {
                        showMessage("Novo Gênero", `Falhou.`, "error");
                      }
                    })
                    .catch((error) => {
                      console.log(error);
                    });
                }
              } else {
                showMessage("Nova Tópico de Sub-gêneros", `Falhou.`, "error");
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } else {
          showMessage("Nova Tópico de Sub-gêneros", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const selectIdGenresub = (ids) => {
    setSelectIdsGenresub(ids);
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu Tópico de Sub-gêneros"
            title="Novo Tópico de Sub-gêneros"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab
                  label="Informações do Tópico de Sub-gêneros"
                  {...a11yProps(0)}
                />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <TextField
                        value={values.name}
                        onChange={handleChange}
                        label="Nome do Tópico de Sub-gêneros"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <GenresubListView
                        selectIdGenresub={selectIdGenresub}
                        selectedListControl={selectedListControl}
                      />
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
