import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../../services/api";
import { showMessage } from "../../../../utils/message";
import MessageConfirm from "../../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = ({ selectIdGenresub, selectedListControl }) => {
  const classes = useStyles();

  const [showNewMusic, setShowNewMusic] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditMusic, setShowEditMusic] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [Music, setMusic] = useState([]);
  const [Genre, setGenre] = useState([]);
  const [GenreSubBusca, setGenreSubBusca] = useState([]);
  const [MusicBusca, setMusicBusca] = useState([]);
  const [getEditMusic, setGetEditMusic] = useState([]);
  const [idMusicModify, setIdMusicModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getMusic();
  }, []);

  useEffect(() => {
    if (Music) {
      getGenre();
    }
  }, [Music]);

  const buscaGenresub = (value) => {
    let resultBusca = GenreSubBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    
      setGenre(resultBusca);
  };
  
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdMusicModify(id);
  };
  const onShowNewMusic = () => {
    setShowNewMusic(true);
  };

  const onCloseNewMusic = () => {
    setShowNewMusic(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditMusic = (value) => {
    setShowEditMusic(true);
    setGetEditMusic(value);
  };

  const onCloseEditMusic = () => {
    setShowEditMusic(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteMusic(idMusicModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getMusic = async () => {
    setIsLoading(true);
    /* const res = await api.get("/Music");

    if (res) {
      setMusic(res.data);
      setMusicBusca(res.data);
    } */

    setIsLoading(false);
  };

  const getGenre = () => {
    setIsLoading(true);
    api
      .get("/Genresub/admin")
      .then((response) => response.data)
      .then(async (res) => {
        if (res) {
          const resCovers = await api.get("/cover");
          const resRights = await api.get("/right");

          let genreInfo = [];

          for (let index = 0; index < res.length; index++) {
            const element = res[index];

            genreInfo.push({
              ...element,
              Covers: resCovers.data.filter((c) => c.genresubId == element.id),
              Right: resRights.data.filter((r) => r.id == element.rightId)[0],
            });
          }

          console.log(genreInfo);
          setGenre(genreInfo);
          setGenreSubBusca(genreInfo);
          setIsLoading(false);
        } else setIsLoading(false);
      });
  };

  const onDeleteMusic = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Music/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getMusic();
        onCloseEditMusic();
        showMessage(
          "Musica Deletada",
          "Musica selecionada foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Musica Falhou",
        "Houve um problema ao deletar sua musica, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <div className={classes.root}>
         <Toolbar SearchGenresub={buscaGenresub} /> 
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditMusic={onShowEditMusic}
            onDeleteMusic={onShowConfirm}
            selectIdGenresub={selectIdGenresub}
            customers={Genre}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
            selectedListControl={selectedListControl}
          />
        </Box>
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a musica será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </div>
  );
};

export default CompanyListView;
