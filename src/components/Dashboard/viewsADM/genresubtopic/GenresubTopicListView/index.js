import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewGenresubTopic from "../ModalNewGenresubTopic";
import ModalEditGenresubTopic from "../ModalEditGenresubTopic";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewGenresubTopic, setShowNewGenresubTopic] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditGenresubTopic, setShowEditGenresubTopic] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [GenresubTopic, setGenresubTopic] = useState([]);
  const [GenresubTopicBusca, setGenresubTopicBusca] = useState([]);
  const [getEditGenresubTopic, setGetEditGenresubTopic] = useState([]);
  const [idGenresubTopicModify, setIdGenresubTopicModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getGenresubTopic();
  }, []);

  const buscaGenresubTopic = (value) => {
    const resultBusca = GenresubTopicBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setGenresubTopic(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdGenresubTopicModify(id);
  };
  const onShowNewGenresubTopic = () => {
    setShowNewGenresubTopic(true);
  };

  const onCloseNewGenresubTopic = () => {
    setShowNewGenresubTopic(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditGenresubTopic = (value) => {
    setShowEditGenresubTopic(true);
    setGetEditGenresubTopic(value);
  };

  const onCloseEditGenresubTopic = () => {
    setShowEditGenresubTopic(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteGenresubTopic(idGenresubTopicModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getGenresubTopic = async () => {
    setIsLoading(true);
    const res = await api.get("/GenresubTopic");

    if (res) {
      setGenresubTopic(res.data);
      setGenresubTopicBusca(res.data);

      if (getEditGenresubTopic.id) {
        setGetEditGenresubTopic(
          res.data.filter((s) => s.id == getEditGenresubTopic.id).length > 0
            ? res.data.filter((s) => s.id == getEditGenresubTopic.id)[0]
            : [0]
        );
      }
    }

    setIsLoading(false);
  };

  const onDeleteGenresubTopic = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/GenresubTopic/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getGenresubTopic();
        onCloseEditGenresubTopic();
        showMessage(
          "Tópico de Sub-gêneros Deletado",
          "Tópico de Sub-gêneros selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Tópico de Sub-gêneros Falhou",
        "Houve um problema ao deletar seu Tópico de Sub-gêneros, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Tópico de Sub-gêneros">
      <Container maxWidth={false}>
        <Toolbar
          SearchGenresubTopic={buscaGenresubTopic}
          onShowNewGenresubTopic={onShowNewGenresubTopic}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditGenresubTopic={onShowEditGenresubTopic}
            onDeleteGenresubTopic={onShowConfirm}
            customers={GenresubTopic}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewGenresubTopic
        open={showNewGenresubTopic}
        onCloseNewGenresubTopic={onCloseNewGenresubTopic}
        getAllGenresubTopic={getGenresubTopic}
      />
      <ModalEditGenresubTopic
        open={showEditGenresubTopic}
        onCloseEditGenresubTopic={onCloseEditGenresubTopic}
        getAllGenresubTopic={getGenresubTopic}
        dataGenresubTopic={getEditGenresubTopic}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Tópico de Sub-gêneros será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
