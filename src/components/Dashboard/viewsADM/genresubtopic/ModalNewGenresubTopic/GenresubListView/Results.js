import React, { useState, useEffect, Fragment } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Tooltip,
  Chip,
  Collapse,
  Paper,
  TableContainer,
} from "@material-ui/core";
import getInitials from "../../../../utils/getInitials";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  PlayCircleOutline as PlayCircleOutlineIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
} from "@material-ui/icons";
import api from "../../../../../../services/api";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  onDeleteMusic,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  selectIdGenresub,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [selectedCustomerIdsMusics, setSelectedCustomerIdsMusics] = useState(
    []
  );
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [limitMusic, setLimitMusic] = useState(5);
  const [pageMusic, setPageMusic] = useState(0);
  const [musics, setMusics] = useState([]);
  const [open, setOpen] = useState({});
  const styleCellCustom = {
    fontWeight: "bold",
    backgroundColor: "black",
    color: "white",
  };

  useEffect(() => {
    selectIdGenresub(selectedCustomerIds)
  }, [selectedCustomerIds])

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id, data) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleLimitChangeMusic = (event) => {
    setLimitMusic(event.target.value);
  };

  const handlePageChangeMusic = (event, newPage) => {
    setPageMusic(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  const formatTel = (str) => {
    return String(str).replace(/\D/g, "").length == 11
      ? String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d)(\d{4})(\d{4})$/, "($1) $2 $3-$4")
      : String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d{4})(\d{4})$/, "($1) $2-$3");
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={720}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell />
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <Fragment>
                      <TableRow
                        hover
                        key={customer.id}
                        selected={
                          selectedCustomerIds.indexOf(customer.id) !== -1
                        }
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={
                              selectedCustomerIds.indexOf(customer.id) !== -1
                            }
                            onChange={(event) =>
                              handleSelectOne(event, customer.id, customer)
                            }
                            value="true"
                          />
                        </TableCell>
                        <TableCell padding="checkbox">
                          <Tooltip title="Visualisar Musicas" arrow>
                            <IconButton
                              style={{
                                color: "#2c2c54",
                                marginRight: 20,
                              }}
                              aria-label="expand row"
                              size="small"
                              onClick={async() =>{
                                if (!open[customer.id]) {
                                  const res = await api.get(
                                    `/Music/genre/${customer.id}`
                                  );

                                  if (res) {
                                    setMusics(res.data);
                                  }
                                }

                                setOpen({
                                  ...open,
                                  [customer.id]: !JSON.parse(
                                    open[customer.id]
                                      ? open[customer.id]
                                      : "false"
                                  ),
                                })}
                              }
                            >
                              {open[customer.id] ? (
                                <KeyboardArrowUpIcon />
                              ) : (
                                <KeyboardArrowDownIcon />
                              )}
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                        <TableCell align="left">
                          <Box alignItems="center" display="flex">
                            <Avatar
                              className={classes.avatar}
                              /* src={customer.avatarUrl} */
                              src={customer.Covers[0].path_s3}
                            >
                              {getInitials(customer.name)}
                            </Avatar>
                            {customer.name}
                          </Box>
                        </TableCell>
                      </TableRow>
                    </Fragment>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
