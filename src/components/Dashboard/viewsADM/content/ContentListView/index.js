import React, { useState, useEffect } from "react";
import {
  Box,
  Container,
  makeStyles,
  LinearProgress,
  Button,
  Backdrop,
  CircularProgress,
  Modal,
} from "@material-ui/core";
import { Delete as DeleteIcon } from "@material-ui/icons";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewContent from "../ModalNewContent";
import ModalEditContent from "../ModalEditContent";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewContent, setShowNewContent] = useState(false);
  const [showBackDrop, setShowBackDrop] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [showEditContent, setShowEditContent] = useState(false);
  const [Content, setContent] = useState([]);
  const [ContentBusca, setContentBusca] = useState([]);
  const [idContentModify, setIdContentModify] = useState(0);
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [getContentModify, setGetContentModify] = useState(null);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getContent();
  }, []);

  const buscaContent = (value) => {
    let resultBusca = ContentBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    if (!(resultBusca.length > 0)) {
      resultBusca = ContentBusca.filter((i) => {
        return (
          String(i.Typecontent.Contentgroup["name"])
            .toUpperCase()
            .indexOf(String(value).toUpperCase()) > -1
        );
      });

      if (!(resultBusca.length > 0)) {
        resultBusca = ContentBusca.filter((i) => {
          return (
            String(i.Typecontent["name"])
              .toUpperCase()
              .indexOf(String(value).toUpperCase()) > -1
          );
        });

        if (!(resultBusca.length > 0)) {
          resultBusca = ContentBusca.filter((i) => {
            return (
              String(i.Segment["name"])
                .toUpperCase()
                .indexOf(String(value).toUpperCase()) > -1
            );
          });
        }
      }
    }

    setContent(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdContentModify(id);
  };
  const onShowNewContent = () => {
    setShowNewContent(true);
  };

  const onCloseNewContent = () => {
    setShowNewContent(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteContent(idContentModify);
  };

  const getContent = async () => {
    setIsLoading(true);
    const res = await api.get("/Content");

    if (res) {
      setContent(res.data);
      setContentBusca(res.data);
    }

    setIsLoading(false);
  };

  const SelectedContentIds = (ids) => {
    setSelectedCustomerIds(ids);
  };

  const onShowEditContent = (data) => {
    setGetContentModify(data);
    setShowEditContent(true);
  };

  const onCloseEditContent = () => {
    setShowEditContent(false);
  };

  const onDeleteContent = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Content/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getContent();
        showMessage(
          "Conteúdo Deletado",
          "Conteúdo selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Conteúdo Falhou",
        "Houve um problema ao deletar seu Conteúdo, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteLote = async () => {
    setShowBackDrop(true);
    for (let index = 0; index < selectedCustomerIds.length; index++) {
      const element = selectedCustomerIds[index];

      const res = await api.delete(`/Content/${element}`);

      if (res.data) {
        if (res.data.message === "success") {
          if (index + 1 == selectedCustomerIds.length) {
            setShowBackDrop(false);
            getContent();

            showMessage(
              "Conteúdo Deletado",
              "Conteúdo selecionado foi deletado com sucesso.",
              "success"
            );
          }
        }
      } else {
        showMessage(
          "Conteúdo Falhou",
          "Houve um problema ao deletar seu Conteúdo, entre em contato com o suporte.",
          "danger"
        );
      }
    }
  };

  return (
    <Page className={classes.root} title="Conteúdo">
      <Container maxWidth={false}>
        {!(selectedCustomerIds.length > 0) ? null : (
          <Box mb={5} display="flex" justifyContent="flex-end">
            <Button
              onClick={() => {
                onDeleteLote();
              }}
              startIcon={<DeleteIcon />}
              style={{ borderRadius: 20 }}
              variant="contained"
            >
              Deletar
            </Button>
          </Box>
        )}
        <Toolbar
          SearchContent={buscaContent}
          onShowNewContent={onShowNewContent}
        />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onDeleteContent={onShowConfirm}
            onShowEditContent={onShowEditContent}
            customers={Content}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
            SelectedContentIds={SelectedContentIds}
          />
        </Box>
      </Container>

      <Modal open={showBackDrop}>
        <Box
          style={{ width: "100%", height: "100%" }}
          justifyContent="center"
          alignContent="center"
        >
          <CircularProgress style={{ color: "red" }} />
        </Box>
      </Modal>

      <ModalNewContent
        open={showNewContent}
        onCloseNewContent={onCloseNewContent}
        getAllContent={getContent}
      />
      <ModalEditContent
        open={showEditContent}
        onCloseEditContent={onCloseEditContent}
        getAllContent={getContent}
        getContentModify={getContentModify}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o conteúdo será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
