import React, { useState } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Chip,
  Tooltip,
} from "@material-ui/core";
import getInitials from "../../../utils/getInitials";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Today as TodayIcon,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  onDeleteContent,
  onShowEditContent,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  SelectedContentIds,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
    SelectedContentIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={720}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <TableRow
                      hover
                      key={customer.id}
                      selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={
                            selectedCustomerIds.indexOf(customer.id) !== -1
                          }
                          onChange={(event) =>
                            handleSelectOne(event, customer.id)
                          }
                          value="true"
                        />
                      </TableCell>
                      <TableCell align="left">{customer.name}</TableCell>
                      <TableCell align="left">
                        <Chip
                          style={{
                            borderColor: "#3498db",
                            backgroundColor: "#3498db",
                            color: "white",
                          }}
                          label={
                            customer.Typecontent
                              ? customer.Typecontent.Contentgroup.name
                              : ""
                          }
                          variant="outlined"
                        />
                      </TableCell>
                      <TableCell align="left">
                        <Chip
                          style={{
                            borderColor: "#e67e22",
                            backgroundColor: "#e67e22",
                            color: "white",
                          }}
                          label={
                            customer.Typecontent
                              ? customer.Typecontent.name
                              : ""
                          }
                          variant="outlined"
                        />
                      </TableCell>
                      <TableCell align="left">
                        <Chip
                          style={{
                            borderColor: "#34495e",
                            backgroundColor: "#34495e",
                            color: "white",
                          }}
                          label={customer.Segment.name}
                          variant="outlined"
                        />
                      </TableCell>
                      {selectedCustomerIds.length > 0 ? (
                        <TableCell />
                      ) : (
                        <TableCell align="right">
                          <Tooltip title="Modificar Conteúdo" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onShowEditContent(customer);
                              }}
                              color="inherit"
                            >
                              <TodayIcon style={{ color: "#474787" }} />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Deletar Conteúdo" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onDeleteContent(customer.id);
                              }}
                              color="inherit"
                            >
                              <DeleteIcon style={{ color: "#ff4d4d" }} />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      )}
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
