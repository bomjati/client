import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Delete as DeleteIcon,
  AddCircleOutline as AddCircleIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { diasSemanais } from "./data";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import MessageConfirm from "../../../utils/MessageConfirm";

const Formulario = ({ onCloseModal, getAllContent, getContentModify }) => {
  let history = useNavigate();

  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listType, setListType] = useState(null);
  const [listSegments, setListSegments] = useState(null);
  const [listGroups, setListGroups] = useState(null);
  const [value, setValue] = useState(0);
  const [dateSelect, setDateSelect] = useState({
    1: {
      startDate: moment().format("YYYY-MM-DD"),
      dueDate: moment().format("YYYY-MM-DD"),
    },
  });
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState();
  const [listConcluded, setListConcluded] = useState([]);
  const [listAudioQuality, setListAudioQuality] = useState([]);
  const [showConfirm, setShowConfirm] = useState(false);
  const [values, setValues] = useState({
    contentgroupId: "",
    segmentId: "",
    typecontentId: "",
    nameTypeContent: "",
    nameGroupcontent: "",
    audioQualityId: ""
  });

  useEffect(() => {
    getAudioQuality();
  },[]);

  const getAudioQuality = () => {
    api
      .get("/audioquality")
      .then(resp => resp.data)
      .then(response => {
        setListAudioQuality(response);
      })
      .catch(error => console.log(error));
  };

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }

    setSelectedFile(e.target.files);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onSave();
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    if (Object.keys(dateSelect).length == 1) {
      if (
        moment(dateSelect["1"].startDate).format("DD/MM/YYYY") ==
        moment(dateSelect["1"].dueDate).format("DD/MM/YYYY")
      ) {
        setValue(1 + 0);
        setShowConfirm(true);

        return;
      }
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);

    let arrayRefreshDays = {};

    for (const key in dateSelect) {
      if (dateSelect.hasOwnProperty(key)) {
        const elementH = dateSelect[key];

        arrayRefreshDays = {
          ...arrayRefreshDays,
          [key]: {
            ...elementH,
            contentId: getContentModify.id,
          },
        };
      }
    }

    api
      .post("/day/new", arrayRefreshDays)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          showMessage(
            "Novo Conteúdo",
            `Cadastro realizado com sucesso.`,
            "success"
          );
          getAllContent();
          setIsLoading(false);
          onCloseModal();
        } else {
          showMessage("Novo Comercial", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    console.log(getContentModify)
    if (getContentModify) {
      setDateSelect({
        1: {
          startDate:
            getContentModify.Days.length > 0
              ? getContentModify.Days[0].startDate
              : moment().format("YYYY-MM-DD"),
          dueDate:
            getContentModify.Days.length > 0
              ? getContentModify.Days[0].dueDate
              : moment().format("YYYY-MM-DD"),
        },
      });
    }
  }, [getContentModify]);

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={!isLoading && onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações do conteúdo"
            title="Novo Conteúdo"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Data de vigência" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Box display="flex" justifyContent="center">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        inputVariant="outlined"
                        helperText="Campo designado para o inicio conteúdo."
                        format="dd/MM/yyyy"
                        id="date-picker-outlined"
                        label="Inicio"
                        name="startDate"
                        value={moment(dateSelect["1"].startDate, "YYYY-MM-DD")}
                        onChange={(event) => {
                          setDateSelect({
                            1: {
                              ...dateSelect["1"],
                              startDate: moment(event).format("YYYY-MM-DD"),
                            },
                          });
                        }}
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>

                  <Box display="flex" justifyContent="center">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        className="mt-4"
                        disableToolbar
                        inputVariant="outlined"
                        helperText="Campo designados para o encerramento."
                        format="dd/MM/yyyy"
                        id="date-picker-outlined"
                        label="Enceramento"
                        name="dueDate"
                        value={moment(dateSelect["1"].dueDate, "YYYY-MM-DD")}
                        onChange={(event) => {
                          setDateSelect({
                            1: {
                              ...dateSelect["1"],
                              dueDate: moment(event).format("YYYY-MM-DD"),
                            },
                          });
                        }}
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>

      <MessageConfirm
        open={showConfirm}
        textInfo={`Observe, que o periodo de datas selecionado são iguais. Isso significa que esse conteúdo permanecerá em sua lista apena 1 dia, no caso o dia selecionado. Tem certeza dessa operação? `}
        title="Atenção"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </form>
  );
};

export default Formulario;
