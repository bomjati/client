import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  Delete as DeleteIcon,
  AddCircleOutline as AddCircleIcon
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { diasSemanais } from "./data";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import MessageConfirm from "../../../utils/MessageConfirm";

const Formulario = ({ onCloseModal, getAllContent }) => {
  let history = useNavigate();

  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listType, setListType] = useState(null);
  const [listSegments, setListSegments] = useState(null);
  const [listGroups, setListGroups] = useState(null);
  const [value, setValue] = useState(0);
  const [indexValue, setIndexValue] = useState(-1);
  const [dateSelect, setDateSelect] = useState({
    1: {
      startDate: moment().format("YYYY-MM-DD"),
      dueDate: moment().format("YYYY-MM-DD")
    }
  });
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState([]);
  const [listConcluded, setListConcluded] = useState([]);
  const [listAudioQuality, setListAudioQuality] = useState([]);
  const [showConfirm, setShowConfirm] = useState(false);
  const [values, setValues] = useState({
    contentgroupId: "",
    segmentId: "",
    typecontentId: "",
    audioQualityId: "",
    nameTypeContent: "",
    nameGroupcontent: ""
  });

  useEffect(() => {
    getAudioQuality();
  }, []);

  const getAudioQuality = () => {
    api
      .get("/audioquality")
      .then(resp => resp.data)
      .then(response => {
        setListAudioQuality(response);
      })
      .catch(error => console.log(error));
  };

  const onSelectFile = e => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }

    setSelectedFile(e.target.files);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566"
      }
    }
  });

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onSave();
  };

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "segmentId"
            ? " Segmento "
            : key == "contentgroupId"
            ? " Grupo de Conteúdo "
            : key == "typecontentId"
            ? " Tipo de Conteúdo "
            : key == "audioQualityId"
            ? " Qualidade "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    if (!selectedFile) {
      showMessage(
        "Atenção",
        `Selecione ao menos um conteúdo antes de salvar`,
        "warning"
      );

      setValue(0);
      return;
    }

    if (Object.keys(dateSelect).length == 1) {
      if (
        moment(dateSelect["1"].startDate).format("DD/MM/YYYY") ==
        moment(dateSelect["1"].dueDate).format("DD/MM/YYYY")
      ) {
        setValue(1 + 0);
        setShowConfirm(true);

        return;
      }
    }

    onSave();
  };

  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  useEffect(() => {
    if (selectedFile.length > 0) {
      if ((indexValue > -1) & (indexValue < selectedFile.length)) {
        const element = selectedFile[indexValue];

        const FormDataContent = new FormData();
        FormDataContent.append("groupcontent", values.nameGroupcontent);
        FormDataContent.append("typecontentId", values.typecontentId);
        FormDataContent.append("typecontent", values.nameTypeContent);
        FormDataContent.append("segmentId", values.segmentId);
        FormDataContent.append("audioQualityId", values.audioQualityId);
        FormDataContent.append("userFile", element);


        
        api
          .post("/Content/new", FormDataContent)
          .then(response => response.data)
          .then(resp => {
            if (resp.message == "success") {
              let arrayConcluded = listConcluded;
              arrayConcluded.push({ key: indexValue });
              setListConcluded(arrayConcluded);

              let arrayRefreshDays = {};

              for (const key in dateSelect) {
                if (dateSelect.hasOwnProperty(key)) {
                  const elementH = dateSelect[key];

                  arrayRefreshDays = {
                    ...arrayRefreshDays,
                    [key]: {
                      ...elementH,
                      contentId: resp.data.id
                    }
                  };
                }
              }

              console.log(arrayRefreshDays);
              api
                .post("/day/new", arrayRefreshDays)
                .then(response => response.data)
                .then(resp => {
                  if (resp.message == "success") {
                    if (indexValue + 1 == selectedFile.length) {
                      showMessage(
                        "Novo Conteúdo",
                        `Cadastro realizado com sucesso.`,
                        "success"
                      );
                      getAllContent();
                      setIsLoading(false);
                      onCloseModal();
                    } else {
                      setIndexValue(indexValue + 1);
                    }
                  } else {
                    showMessage("Novo Comercial", `Falhou.`, "error");
                  }
                })
                .catch(error => {
                  console.log(error);
                });
            } else {
              showMessage("Novo Conteúdo", `Falhou.`, "error");
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }, [indexValue]);

  const onSave = () => {
    setIsLoading(true);

    setIndexValue(0);
  };

  useEffect(() => {
    api
      .get("/contentgroup/admin")
      .then(response => response.data)
      .then(resp => {
        if (resp) {
          console.log(resp);
          setListGroups(resp);
        }
      })
      .catch(error => {
        console.log(error);
      });

    api
      .get("/segment")
      .then(response => response.data)
      .then(resp => {
        if (resp) {
          console.log(resp);
          setListSegments(resp);
        }
      })
      .catch(error => {
        console.log(error);
      });

    api
      .get("/typecontent")
      .then(response => response.data)
      .then(resp => {
        if (resp) {
          console.log(resp);
          setListType(resp);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={!isLoading && onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações do conteúdo"
            title="Novo Conteúdo"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do conteúdo" {...a11yProps(0)} />
                <Tab label="Data de vigência" {...a11yProps(1)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl
                          style={{
                            marginBottom: 20,
                            marginTop: 20
                          }}
                          requireds
                          variant="outlined"
                          fullWidth
                        >
                          <InputLabel htmlFor="age-native-required">
                            Qualidade
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="audioQualityId"
                            id="audioQualityId"
                            value={values.audioQualityId}
                            onChange={event => {
                              setValues({
                                ...values,
                                audioQualityId: event.target.value
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listAudioQuality &&
                              listAudioQuality.map(AudioQuality => (
                                <option value={AudioQuality.id}>
                                  {AudioQuality.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Box mb={1} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Grupo de Conteúdo
                          </InputLabel>
                          <Select
                            required
                            disabled={isLoading}
                            native
                            name="contentgroupId"
                            id="contentgroupId"
                            value={`${values.contentgroupId}|${values.nameGroupcontent}`}
                            onChange={event => {
                              const result = String(event.target.value).split(
                                "|"
                              );
                              setValues({
                                ...values,
                                [event.target.name]: result[0],
                                nameGroupcontent: result[1]
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listGroups &&
                              listGroups.map(Groups => (
                                <option value={`${Groups.id}|${Groups.name}`}>
                                  {Groups.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Box mb={1} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Tipo de Conteúdo
                          </InputLabel>
                          <Select
                            required
                            disabled={isLoading}
                            native
                            name="typecontentId"
                            id="typecontentId"
                            value={`${values.typecontentId}|${values.nameTypeContent}`}
                            onChange={event => {
                              const result = String(event.target.value).split(
                                "|"
                              );
                              setValues({
                                ...values,
                                [event.target.name]: result[0],
                                nameTypeContent: result[1]
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listType &&
                              listType.map(
                                Types =>
                                  Types.contentgroupId ==
                                    values.contentgroupId && (
                                    <option value={`${Types.id}|${Types.name}`}>
                                      {Types.name}
                                    </option>
                                  )
                              )}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Box mb={1} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Segmento
                          </InputLabel>
                          <Select
                            disabled={isLoading}
                            required
                            native
                            name="segmentId"
                            id="segmentId"
                            value={values.segmentId}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listSegments &&
                              listSegments.map(Segments => (
                                <option value={Segments.id}>
                                  {Segments.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    {selectedFile &&
                      [...selectedFile].map((music, index) => (
                        <Grid item md={12} xs={12}>
                          <Box display="flex" alignItems="center">
                            {isLoading && indexValue == index && (
                              <CircularProgress className="mr-2" size={15} />
                            )}
                            {listConcluded.filter(
                              concluded => concluded.key == index
                            ).length > 0 && (
                              <CheckIcon
                                className="mr-2"
                                style={{ color: "#2ecc71" }}
                              />
                            )}
                            <h6>{`${music.name}`}</h6>
                          </Box>
                        </Grid>
                      ))}
                  </Grid>

                  <Button
                    className="mt-4"
                    color="primary"
                    variant="outlined"
                    fullWidth
                    disabled={isLoading}
                    disabled={isLoading}
                    htmlFor="sampleFile"
                    component="label"
                    style={{ borderRadius: 20 }}
                  >
                    Buscar Musica
                  </Button>

                  <input
                    onChange={onSelectFile}
                    type="file"
                    multiple
                    id="sampleFile"
                    style={{ display: "none" }}
                  />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                  <Box display="flex" justifyContent="center">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        disableToolbar
                        inputVariant="outlined"
                        helperText="Campo designado para o inicio conteúdo."
                        format="dd/MM/yyyy"
                        id="date-picker-outlined"
                        label="Inicio"
                        name="startDate"
                        value={moment(dateSelect["1"].startDate, "YYYY-MM-DD")}
                        onChange={event => {
                          setDateSelect({
                            1: {
                              ...dateSelect["1"],
                              startDate: moment(event).format("YYYY-MM-DD")
                            }
                          });
                        }}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>

                  <Box display="flex" justifyContent="center">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        className="mt-4"
                        disableToolbar
                        inputVariant="outlined"
                        helperText="Campo designados para o encerramento."
                        format="dd/MM/yyyy"
                        id="date-picker-outlined"
                        label="Enceramento"
                        name="dueDate"
                        value={moment(dateSelect["1"].dueDate, "YYYY-MM-DD")}
                        onChange={event => {
                          setDateSelect({
                            1: {
                              ...dateSelect["1"],
                              dueDate: moment(event).format("YYYY-MM-DD")
                            }
                          });
                        }}
                        KeyboardButtonProps={{
                          "aria-label": "change date"
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Box>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            {value < 1 ? (
              <Button
                onClick={() => {
                  setValue(value + 1);
                }}
                disabled={isLoading}
                style={{
                  borderRadius: 50,
                  backgroundColor: "#3498db"
                }}
                color="primary"
                variant="contained"
                size="medium"
                startIcon={<ArrowForwardIcon style={{ color: "white" }} />}
              >
                Próximo
              </Button>
            ) : (
              <div className="d-flex">
                <Button
                  onClick={() => {
                    setValue(value - 1);
                  }}
                  disabled={isLoading}
                  style={{
                    borderRadius: 50,
                    backgroundColor: "#f39c12"
                  }}
                  color="primary"
                  variant="contained"
                  size="medium"
                  className="mr-2"
                  startIcon={<ArrowBackIcon style={{ color: "white" }} />}
                >
                  Voltar
                </Button>
                <Button
                  onClick={onUpdate}
                  disabled={isLoading}
                  style={{
                    borderRadius: 50,
                    backgroundColor: "#10ac84"
                  }}
                  color="primary"
                  variant="contained"
                  size="medium"
                  startIcon={<CheckIcon style={{ color: "white" }} />}
                >
                  Salvar
                </Button>
              </div>
            )}
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>

      <MessageConfirm
        open={showConfirm}
        textInfo={`Observe, que o periodo de datas selecionado são iguais. Isso significa que esse conteúdo permanecerá em sua lista apena 1 dia, no caso o dia selecionado. Tem certeza dessa operação? `}
        title="Atenção"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </form>
  );
};

export default Formulario;
