import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewPhraseSegment from "../ModalNewPhraseSegment";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewPhraseSegment, setShowNewPhraseSegment] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [PhraseSegment, setPhraseSegment] = useState([]);
  const [PhraseSegmentBusca, setPhraseSegmentBusca] = useState([]);
  const [idPhraseSegmentModify, setIdPhraseSegmentModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getPhraseSegment();
  }, []);

  const buscaPhraseSegment = (value) => {
    let resultBusca = PhraseSegmentBusca.filter((i) => {
      return (
        String(i["phrase"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    if(!(resultBusca.length > 0)){
      resultBusca = PhraseSegmentBusca.filter((i) => {
        return (
          String(i.Contentgroup["phrase"]).toUpperCase().indexOf(String(value).toUpperCase()) >
          -1
        );
      });
    }

    setPhraseSegment(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdPhraseSegmentModify(id);
  };
  const onShowNewPhraseSegment = () => {
    setShowNewPhraseSegment(true);
  };

  const onCloseNewPhraseSegment = () => {
    setShowNewPhraseSegment(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeletePhraseSegment(idPhraseSegmentModify);
  };

  const getPhraseSegment = async () => {
    setIsLoading(true);
    const res = await api.get("/PhraseSegment");

    if (res) {
      setPhraseSegment(res.data);
      setPhraseSegmentBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeletePhraseSegment = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/PhraseSegment/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getPhraseSegment();
        showMessage(
          "Tipo Deletado",
          "Tipo selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Tipo Falhou",
        "Houve um problema ao deletar seu tipo, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Tipo de Conteúdo">
      <Container maxWidth={false}>
        <Toolbar
          SearchPhraseSegment={buscaPhraseSegment}
          onShowNewPhraseSegment={onShowNewPhraseSegment}
        />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onDeletePhraseSegment={onShowConfirm}
            customers={PhraseSegment}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewPhraseSegment
        open={showNewPhraseSegment}
        onCloseNewPhraseSegment={onCloseNewPhraseSegment}
        getAllPhraseSegment={getPhraseSegment}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Tipo será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
