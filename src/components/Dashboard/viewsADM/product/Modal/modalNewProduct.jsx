import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Modal,
    Backdrop,
    Fade
} from '@material-ui/core';
import NewProduct from './Product'

const useStyles = makeStyles(({ breakpoints }) => ({
    modal: {
        display: 'flex',
        [breakpoints.up('xs')]: {
            alignItems: 'start',
        },
        [breakpoints.up('sm')]: {
            alignItems: 'center',
        },
        justifyContent: 'center',
        overflow: 'scroll'
    }
}));

const ModalNewBusiness = ({ open, onCloseNewProduct, getAllProduct, isLoading, allCategory, productData, isEditing, onEditProduct }) => {
    const classes = useStyles();

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={onCloseNewProduct}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className='mb-4'>
                        <NewProduct
                            onCloseModal={onCloseNewProduct}
                            onEditProduct={onEditProduct}
                            getAllProduct={getAllProduct}
                            allCategory={allCategory}
                            productData={productData}
                            isLoading={isLoading}
                            isEditing={isEditing} />
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

export default ModalNewBusiness;