import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  LinearProgress,
  FormControlLabel,
  Switch,
  duration,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import moment from "moment";

let test = "";
const Formulario = ({ onCloseModal, getAllMusic, isLoadingModal }) => {
  let history = useNavigate();
  const fileToArrayBuffer = require("file-to-array-buffer");
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listMusic, setListMusic] = useState(null);
  const [value, setValue] = useState(0);
  const [indexMusic, setIndexMusic] = useState(-1);
  const [listGenre, setListGenre] = useState("");
  const [listSegment, setListSegment] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [selectedFile, setSelectedFile] = useState([]);
  const [listConcluded, setListConcluded] = useState([]);
  const [listAudioQuality, setListAudioQuality] = useState([]);
  const audioContext = new window.AudioContext();
  const [listDuration, setListDuration] = useState(null);
  const [listDurationGet, setListDurationGet] = useState(null);

  const [values, setValues] = useState({
    genreId: "",
    nameGenre: "",
    nameRight: "",
    audioQualityId: 0,
  });

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(null);
      return;
    }

    // Eu mantive este exemplo simples usando a primeira imagem em vez de vários
    /* setIsLoading(true);
    const maxFiles = e.target.files.length;
    for (let index = 0; index < e.target.files.length; index++) {
      const element = e.target.files[index];

      fileToArrayBuffer(element).then((data) => {
        console.log(data);
        //=> ArrayBuffer {byteLength: ...}
        audioContext.decodeAudioData(data, (buffer) => {
          const duration = buffer.duration;
          const timeMusic = moment.duration(duration * 1000);

          setTimeout(() => {
            test =
              test.replaceAll("]", "").replaceAll("[", "") +
              JSON.stringify({
                duration: String(
                  timeMusic.minutes() +
                    ":" +
                    (timeMusic.seconds() < 10
                      ? `0${timeMusic.seconds()}`
                      : timeMusic.seconds())
                ),
                key: index,
              }) +
              ",";
            test = `[${test}]`;
            test = test.replaceAll(",]", "]");
            test = test.replaceAll("}{", "},{");
            setListDuration(JSON.parse(test.length > 0 && test));
          }, 100 * index);

          if (index + 1 == maxFiles) {
            setIsLoading(false);
          }

          /* onCheckArrayDuration(
            String(
              timeMusic.minutes() +
                ":" +
                (timeMusic.seconds() < 10
                  ? `0${timeMusic.seconds()}`
                  : timeMusic.seconds())
            ),
            index
          );
        });
      });
    }*/

    setSelectedFile(e.target.files);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused = key == "genreId" ? " Gênero " : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    if (!selectedFile) {
      showMessage(
        "Atenção",
        `Selecione ao menos uma musica antes de salvar`,
        "warning"
      );

      return;
    }

    onSave();
  };

  const onUpdate = () => {
    validateCampos();
  };

  useEffect(() => {
    if (selectedFile.length > 0) {
      if ((indexMusic > -1) & (indexMusic < selectedFile.length)) {
        const element = selectedFile[indexMusic];

        fileToArrayBuffer(element).then((data) => {
          console.log(data);
          //=> ArrayBuffer {byteLength: ...}
          audioContext.decodeAudioData(data, (buffer) => {
            const duration = buffer.duration;
            const timeMusic = moment.duration(duration * 1000);

            const FormDataMusic = new FormData();
            FormDataMusic.append("heap", "acervo_1");
            FormDataMusic.append("right", values.nameRight);
            FormDataMusic.append("genre", values.nameGenre);
            FormDataMusic.append("genreId", values.genreId);
            FormDataMusic.append("audioQualityId", values.audioQualityId);
            FormDataMusic.append(
              "totalTime",
              String(
                timeMusic.minutes() +
                  ":" +
                  (timeMusic.seconds() < 10
                    ? `0${timeMusic.seconds()}`
                    : timeMusic.seconds())
              )
            );
            FormDataMusic.append("userFile", element);

            api
              .post("/Music/new", FormDataMusic)
              .then((response) => response.data)
              .then((resp) => {
                let arrayConcluded = listConcluded;
                arrayConcluded.push({ key: indexMusic });
                setListConcluded(arrayConcluded);
                setIndexMusic(indexMusic + 1);

                if (resp.message == "success") {
                  if (indexMusic + 1 == selectedFile.length) {
                    showMessage(
                      "Novas Musicas",
                      `Cadastro realizado com sucesso.`,
                      "success"
                    );
                    //getAllMusic();
                    setIsLoading(false);
                    onCloseModal();
                  }
                } else {
                  showMessage("Nova Musica", `Falhou.`, "error");
                }
              })
              .catch((error) => {
                console.log(error);
              });
          });
        });
      }
    }
  }, [indexMusic]);

  const onSave = () => {
    setIsLoading(true);

    setIndexMusic(0);
  };

  useEffect(() => {
    isLoadingModal(isLoading);
  }, [isLoading]);

  useEffect(() => {
    api
      .get("/genre/admin")
      .then((response) => response.data)
      .then(async (resp) => {
        if (resp) {
          console.log(resp);
          const resCovers = await api.get("/cover");
          const resRights = await api.get("/right");

          let genreInfo = [];

          for (let index = 0; index < resp.length; index++) {
            const element = resp[index];

            genreInfo.push({
              ...element,
              Covers: resCovers.data.filter((c) => c.genreId == element.id),
              Right: resRights.data.filter((r) => r.id == element.rightId)[0],
            });
          }
          setListGenre(genreInfo);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    getAudioQuality();
  }, []);

  const getAudioQuality = () => {
    api
      .get("/audioquality")
      .then((resp) => resp.data)
      .then((response) => {
        setListAudioQuality(response);
      })
      .catch((error) => console.log(error));
  };

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={!isLoading && onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu musica"
            title="Nova Musica"
          />

          <div>
            {isLoading && <LinearProgress />}
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações da Musica" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Gêneros
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="genreId"
                            id="genreId"
                            value={`${values.genreId}|${
                              values.nameGenre
                            }|${String(values.nameRight).replaceAll("_", " ")}`}
                            onChange={(event) => {
                              const result = String(event.target.value).split(
                                "|"
                              );

                              setValues({
                                ...values,
                                [event.target.name]: result[0],
                                nameGenre: result[1],
                                nameRight: String(result[2]).replaceAll(
                                  " ",
                                  "_"
                                ),
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listGenre &&
                              listGenre.map((Genres) => (
                                <option
                                  value={`${Genres.id}|${Genres.name}|${Genres.Right.name}`}
                                >{`${Genres.name} | ${Genres.Right.name}`}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                      <Box display="flex" justifyContent="center">
                        <FormControl
                          style={{
                            marginBottom: 20,
                            marginTop: 20,
                          }}
                          requireds
                          variant="outlined"
                          fullWidth
                        >
                          <InputLabel htmlFor="age-native-required">
                            Qualidade
                          </InputLabel>
                          <Select
                            required
                            native
                            disabled={isLoading}
                            name="audioQualityId"
                            id="audioQualityId"
                            value={values.audioQualityId}
                            onChange={(event) => {
                              setValues({
                                ...values,
                                audioQualityId: event.target.value,
                              });
                            }}
                          >
                            <option aria-label="None" value="" />
                            {listAudioQuality &&
                              listAudioQuality.map((AudioQuality) => (
                                <option value={AudioQuality.id}>
                                  {AudioQuality.name}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    {selectedFile &&
                      [...selectedFile].map((music, index) => (
                        <Grid item md={12} xs={12}>
                          <Box display="flex" alignItems="center">
                            {isLoading && indexMusic == index && (
                              <CircularProgress className="mr-2" size={15} />
                            )}
                            {listConcluded.filter(
                              (concluded) => concluded.key == index
                            ).length > 0 && (
                              <CheckIcon
                                className="mr-2"
                                style={{ color: "#2ecc71" }}
                              />
                            )}
                            <h6>{`${music.name}`}</h6>
                          </Box>
                        </Grid>
                      ))}
                  </Grid>

                  <Button
                    className="mt-4"
                    color="primary"
                    variant="outlined"
                    fullWidth
                    disabled={isLoading}
                    htmlFor="sampleFile"
                    component="label"
                    style={{ borderRadius: 20 }}
                  >
                    Buscar Musica
                  </Button>

                  <input
                    onChange={onSelectFile}
                    type="file"
                    multiple
                    id="sampleFile"
                    style={{ display: "none" }}
                  />
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
