import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import NewMusic from "./FormNewMusic";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const useStyles = makeStyles(({ breakpoints }) => ({
  modal: {
    display: "flex",
    [breakpoints.up("xs")]: {
      alignItems: "start",
    },
    [breakpoints.up("sm")]: {
      alignItems: "center",
    },
    justifyContent: "center",
  },
}));

const ModalNewMusic = ({ open, onCloseNewMusic, getAllMusic }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);

  const isLoadingModal = (value) => {
    setIsLoading(value);
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={!isLoading && onCloseNewMusic}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <PerfectScrollbar>
            <div className="mb-4">
              <NewMusic
                onCloseModal={onCloseNewMusic}
                isLoadingModal={isLoadingModal}
                getAllMusic={getAllMusic}
              />
            </div>
          </PerfectScrollbar>
        </Fade>
      </Modal>
    </div>
  );
};

export default ModalNewMusic;
