import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewMusic from "../ModalMusic/ModalNewMusic";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewMusic, setShowNewMusic] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditMusic, setShowEditMusic] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [Music, setMusic] = useState([]);
  const [MusicBusca, setMusicBusca] = useState([]);
  const [getEditMusic, setGetEditMusic] = useState([]);
  const [idMusicModify, setIdMusicModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getMusic();
  }, []);

  const buscaMusic = (value) => {
    let resultBusca = MusicBusca.filter((i) => {
      return (
        String(i["Genre"].name)
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    if (resultBusca.length == 0) {
      resultBusca = MusicBusca.filter((i) => {
        return (
          String(i["title"])
            .toUpperCase()
            .indexOf(String(value).toUpperCase()) > -1
        );
      });

      setMusic(resultBusca);
    } else {
      setMusic(resultBusca);
    }
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdMusicModify(id);
  };
  const onShowNewMusic = () => {
    setShowNewMusic(true);
  };

  const onCloseNewMusic = () => {
    setShowNewMusic(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditMusic = (value) => {
    setShowEditMusic(true);
    setGetEditMusic(value);
  };

  const onCloseEditMusic = () => {
    setShowEditMusic(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteMusic(idMusicModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getMusic = async () => {
    setIsLoading(true);
    const resG = await api.get("/genre/admin");
    const res1 = await api.get("/Music/admin/1");

    let musicG1 = [];
    let musicG2 = [];
    let musicG3 = [];

    if (res1) {
      console.log(res1.data);

      for (let index = 0; index < res1.data.length; index++) {
        const element = res1.data[index];

        musicG1.push({
          ...element,
          Genre: resG.data.filter((g) => g.id == element.genreId)[0],
        });
      }

      setMusic(musicG1);
      setMusicBusca(musicG1);
    }

    setIsLoading(false);

     const res2 = await api.get("/Music/admin/2");

    if (res2) {
      console.log(res2.data);

      for (let index = 0; index < res2.data.length; index++) {
        const element = res2.data[index];

        musicG2.push({
          ...element,
          Genre: resG.data.filter((g) => g.id == element.genreId)[0],
        });
      }

      setMusic([...musicG2, ...musicG1]);
      setMusicBusca([...musicG2, ...musicG1]);
    }

    const res3 = await api.get("/Music/admin/3");

    if (res3) {
      console.log(res3.data);

      for (let index = 0; index < res3.data.length; index++) {
        const element = res3.data[index];

        musicG3.push({
          ...element,
          Genre: resG.data.filter((g) => g.id == element.genreId)[0],
        });
      }

      setMusic([...musicG3, ...musicG2, ...musicG1]);
      setMusicBusca([...musicG3, ...musicG2, ...musicG1]);
    } 
  };

  const onDeleteMusic = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Music/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getMusic();
        onCloseEditMusic();
        showMessage(
          "Musica Deletada",
          "Musica selecionada foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Musica Falhou",
        "Houve um problema ao deletar sua musica, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Musica">
      <Container maxWidth={false}>
        <Toolbar SearchMusic={buscaMusic} onShowNewMusic={onShowNewMusic} />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditMusic={onShowEditMusic}
            onDeleteMusic={onShowConfirm}
            customers={Music}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewMusic
        open={showNewMusic}
        onCloseNewMusic={onCloseNewMusic}
        getAllMusic={getMusic}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a musica será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
