import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewContentGroup from "../ModalNewContentGroup";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewContentGroup, setShowNewContentGroup] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditContentGroup, setShowEditContentGroup] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [ContentGroup, setContentGroup] = useState([]);
  const [ContentGroupBusca, setContentGroupBusca] = useState([]);
  const [getEditContentGroup, setGetEditContentGroup] = useState([]);
  const [idContentGroupModify, setIdContentGroupModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getContentGroup();
  }, []);

  const buscaContentGroup = (value) => {
    const resultBusca = ContentGroupBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setContentGroup(resultBusca);
  };

  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdContentGroupModify(id);
  };

  const onShowNewContentGroup = () => {
    setShowNewContentGroup(true);
  };

  const onCloseNewContentGroup = () => {
    setShowNewContentGroup(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditContentGroup = (value) => {
    setShowEditContentGroup(true);
    setGetEditContentGroup(value);
  };

  const onCloseEditContentGroup = () => {
    setShowEditContentGroup(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteContentGroup(idContentGroupModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getContentGroup = async () => {
    setIsLoading(true);
    const res = await api.get("/contentgroup/admin");

    if (res) {
      setContentGroup(res.data);
      setContentGroupBusca(res.data);
    }

    setIsLoading(false);
  };

  const onUpdateContentGroup = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/ContentGroup/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Loja Atualisada",
          "Loja selecionada foi atualisada com sucesso.",
          "success"
        );
        getContentGroup();
        onCloseEditContentGroup();
      }
    } else {
      showMessage(
        "Loja Falhou",
        "Houve um problema ao atualizar sua loja, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteContentGroup = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/contentgroup/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getContentGroup();
        onCloseEditContentGroup();
        showMessage(
          "Grupo Deletado",
          "Grupo selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Grupo Falhou",
        "Houve um problema ao deletar seu grupo, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Grupo de Conteúdo">
      <Container maxWidth={false}>
        <Toolbar
          SearchContentGroup={buscaContentGroup}
          onShowNewContentGroup={onShowNewContentGroup}
        />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditContentGroup={onShowEditContentGroup}
            onDeleteContentGroup={onShowConfirm}
            customers={ContentGroup}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewContentGroup
        open={showNewContentGroup}
        onCloseNewContentGroup={onCloseNewContentGroup}
        getAllContentGroup={getContentGroup}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Grupo será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
