import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Typography,
  makeStyles,
  TextField,
  CircularProgress
} from '@material-ui/core';
import { showMessage } from '../../../utils/message'
import api from "../../../../../services/api";

const useStyles = makeStyles(({
  root: {},
  item: {
    display: 'flex',
    flexDirection: 'column'
  }
}));

const ColorPadrao = ({ isEditing, infoConfig, className, ...rest }) => {
  const classes = useStyles();
  const [primaryColor, setPrimaryColor] = useState('')
  const [secondaryColor, setSecondaryColor] = useState('')
  const [isLoad, setIsLoad] = useState('')

  const onPostNewConfig = async () => {
    const productFormData = {
      primaryColor: primaryColor,
      secondaryColor: secondaryColor
    }

    if (primaryColor == '') {
      showMessage("Cor Primaria", "Informe a cor primaria de sua loja", "warning")
      return
    } else if (secondaryColor == '') {
      showMessage("Cor Secundaria", "Informe a cor secundaria de sua loja", "warning")
      return
    } else {
      if (isEditing) {
        setIsLoad(true)
        await api.put(`/setting/edit/${infoConfig.id}`, productFormData)
          .then(res => {
            console.log(res.data)
            if (res.data.message == 'success') {
              showMessage("Configurações Salvas", "Atualização realizada com sucesso", "success");
            } else {
              showMessage("Configurações Falhou", "Houve um problema ao atualizar sua configurações, entre em contato com o suporte.", "danger");
            }
            setIsLoad(false)
          })
          .catch(error => {
            console.log(error)
            setIsLoad(false)
          });
      } else {
        setIsLoad(true)
        await api.post("/setting/new", productFormData)
          .then(res => {
            console.log(res.data)
            if (res.data.message == 'success') {
              showMessage("Configurações Salvas", "Cadastro realizado com sucesso", "success");
            }
            setIsLoad(false)
          })
          .catch(error => {
            console.log(error)
            setIsLoad(false)
          });
      }
    }
  }


  useEffect(() => {
    if (infoConfig) {
      setPrimaryColor(infoConfig.primaryColor)
      setSecondaryColor(infoConfig.secondaryColor)
    }
  }, [infoConfig])

  return (
    <form
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20 }}>
        <CardHeader
          subheader="Padrão de cores para vitrine"
          title="Cores"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={6}
            wrap="wrap"
          >
            <Grid
              className={classes.item}
              item
              md={4}
              sm={6}
              xs={12}
            >
              <Typography
                color="textPrimary"
                gutterBottom
                variant="h6"
              >
                Primaria
              </Typography>
              <div className='d-flex align-items-center'>
                <TextField
                  fullWidth
                  label=""
                  name="primary_color"
                  onChange={(event) => { setPrimaryColor(event.target.value) }}
                  required
                  value={primaryColor}
                  variant="outlined"
                />
                <div
                  className='ml-2'
                  style={{
                    backgroundColor: primaryColor,
                    width: 20,
                    height: 20
                  }}
                />
              </div>
            </Grid>
            <Grid
              className={classes.item}
              item
              md={4}
              sm={6}
              xs={12}
            >
              <Typography
                color="textPrimary"
                gutterBottom
                variant="h6"
              >
                Secundaria
              </Typography>
              <div className='d-flex align-items-center'>
                <TextField
                  fullWidth
                  label=""
                  name="secundary_color"
                  onChange={(event) => { setSecondaryColor(event.target.value) }}
                  required
                  value={secondaryColor}
                  variant="outlined"
                />
                <div
                  className='ml-2'
                  style={{
                    backgroundColor: secondaryColor,
                    width: 20,
                    height: 20
                  }}
                />
              </div>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Button
            color="primary"
            style={{ borderRadius: 50 }}
            variant="contained"
            onClick={() => { onPostNewConfig() }}
          >
            Salvar
          </Button>
          {isLoad && <CircularProgress className='ml-2' />}
        </Box>
      </Card>
    </form>
  );
};

ColorPadrao.propTypes = {
  className: PropTypes.string
};

export default ColorPadrao;
