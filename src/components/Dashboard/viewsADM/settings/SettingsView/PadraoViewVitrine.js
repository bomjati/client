import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Typography,
  makeStyles,
  Avatar,
  CircularProgress
} from '@material-ui/core';
import {
  CheckCircle as CheckCircleIcon
} from '@material-ui/icons';
import { showMessage } from '../../../utils/message';
import api from "../../../../../services/api";

const useStyles = makeStyles(({
  root: {},
  item: {
    display: 'flex',
    flexDirection: 'column'
  }
}));

const PadraoViewVitrine = ({ isEditing, infoConfig, className, ...rest }) => {
  const classes = useStyles();
  const [typeOne, setTypeOne] = useState(false);
  const [typeTwo, setTypeTwo] = useState(false);
  const [isLoad, setIsLoad] = useState('')

  const onPostNewConfig = async () => {
    const productFormData = {
      typeShowcases: typeOne ? "tipo 1" : typeTwo ? "tipo 2" : "undefind"
    };

    if(!typeOne & !typeTwo){
      showMessage("Estilo de Vitrine", "Informe o tipo de vitrine", "warning")
      return
    }

    if (isEditing) {
      setIsLoad(true)
      await api.put(`/setting/edit/${infoConfig.id}`, productFormData)
        .then(res => {
          console.log(res.data)
          if (res.data.message == 'success') {
            showMessage("Configurações Salvas", "Atualização realizada com sucesso", "success");
          } else {
            showMessage("Configurações Falhou", "Houve um problema ao atualizar sua configurações, entre em contato com o suporte.", "danger");
          }
          setIsLoad(false)
        })
        .catch(error => {
          console.log(error)
          setIsLoad(false)
        });
    } else {
      setIsLoad(true)
      await api.post("/setting/new", productFormData)
        .then(res => {
          console.log(res.data)
          if (res.data.message == 'success') {
            showMessage("Configurações Salvas", "Cadastro realizado com sucesso", "success");
          }
          setIsLoad(false)
        })
        .catch(error => {
          console.log(error)
          setIsLoad(false)
        });
    }
  }

  const handleOne = () => {
    setTypeOne(!typeOne)
    setTypeTwo(false)
  }

  const handleTwo = () => {
    setTypeOne(false)
    setTypeTwo(!typeTwo)
  }

  useEffect(() => {
    if (infoConfig) {
      if (infoConfig.typeShowcases == 'tipo 1') {
        setTypeOne(true)
      } else if (infoConfig.typeShowcases == 'tipo 2') {
        setTypeTwo(true)
      }
    }
  }, [infoConfig])


  return (
    <form
      className={clsx(classes.root, className)}
      {...rest}
    >
      <Card style={{ borderRadius: 20, marginTop: 20 }}>
        <CardHeader
          subheader="Selecione o padrão de visualização da vitrine."
          title="Selecione Vitrine"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={6}
            wrap="wrap"
          >
            <Grid
              className={classes.item}
              item
              md={4}
              sm={6}
              xs={12}
            >
              <Typography
                color="textPrimary"
                gutterBottom
                variant="h6"
              >
                Estilo 1
              </Typography>
              <div>
                <Avatar
                  onClick={() => {
                    handleOne()
                  }}
                  variant='square'
                  style={{
                    height: (200 * 3) / 4,
                    width: 250,
                    cursor: 'pointer'
                  }}

                  src="/img/Tipo-1.png" />
                {typeOne && <CheckCircleIcon
                  style={{
                    position: 'absolute',
                    marginTop: -((200 * 3) / 4),
                    color: '#fbc531'
                  }} />}
              </div>
            </Grid>
            <Grid
              className={classes.item}
              item
              md={4}
              sm={6}
              xs={12}
            >
              <Typography
                color="textPrimary"
                gutterBottom
                variant="h6"
              >
                Estilo 2
              </Typography>
              <div>
                <Avatar
                  onClick={() => {
                    handleTwo()
                  }}
                  variant='square'
                  style={{
                    height: (200 * 3) / 4,
                    width: 250,
                    cursor: 'pointer'
                  }}
                  src="/img/Tipo-2.png" />
                {typeTwo && <CheckCircleIcon
                  style={{
                    position: 'absolute',
                    marginTop: -((200 * 3) / 4),
                    color: '#fbc531'
                  }} />}
              </div>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Button
            color="primary"
            style={{ borderRadius: 50 }}
            variant="contained"
            onClick={() => { onPostNewConfig() }}
          >
            Salvar
          </Button>
          {isLoad && <CircularProgress className='ml-2' />}
        </Box>
      </Card>
    </form>
  );
};

PadraoViewVitrine.propTypes = {
  className: PropTypes.string
};

export default PadraoViewVitrine;
