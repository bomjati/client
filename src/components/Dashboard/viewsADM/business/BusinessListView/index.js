import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewBusiness from "../Modal";
import ModalEditBusiness from "../ModalEdit";
import ModalPlan from "../ModalPlan";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditBusiness, setShowEditBusiness] = useState(false);
  const [showEditPlan, setShowEditPlan] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [business, setBusiness] = useState([]);
  const [businessBusca, setBusinessBusca] = useState([]);
  const [getEditBusiness, setGetEditBusiness] = useState([]);
  const [getEditPlan, setGetEditPlan] = useState([]);
  const [idBusinessModify, setIdBusinessModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getBusiness();
  }, []);

  useEffect(() => {
    if (firstAccess) {
      setShowNewBusiness(true);
    } else {
      setShowNewBusiness(false);
    }
  }, [firstAccess]);

  const buscaBusiness = (value) => {
    const resultBusca = businessBusca.filter((i) => {
      return (
        String(i["business_name"])
          .toUpperCase()
          .indexOf(String(value).toUpperCase()) > -1
      );
    });

    setBusiness(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdBusinessModify(id);
  };
  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditBusiness = (value) => {
    setShowEditBusiness(true);
    setGetEditBusiness(value);
  };

  const onCloseEditBusiness = () => {
    setShowEditBusiness(false);
  };

  const onCloseEditPlan = () => {
    setShowEditPlan(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteBusiness(idBusinessModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getBusiness = async () => {
    setIsLoading(true);
    const res = await api.get("/business/admin");

    if (res) {
      console.log(res.data)
      setBusiness(res.data);
      setBusinessBusca(res.data);

      if (res.data.length == 0) {
        setFirstAccess(true);
      }
    }
    setIsLoading(false);
  };

  const onUpdateBusiness = async (value, id) => {
    setIsLoading(true);
    const res = await api.put(`/business/edit/${id}`, value);

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Empresa Atualizada",
          "Empresa selecionada foi atualisada com sucesso.",
          "success"
        );
        getBusiness();
        onCloseEditBusiness();
      }
    } else {
      showMessage(
        "Empresa Falhou",
        "Houve um problema ao atualizar sua Empresa, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteBusiness = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/business/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getBusiness();
        onCloseEditBusiness();
        showMessage(
          "Empresa Deletada",
          "Empresa selecionada foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Empresa Falhou",
        "Houve um problema ao deletar sua Empresa, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onActiveAccess = async (emp) => {
    setIsLoading(true);
    const res = await api.put(`/plancontrol/edit/${emp.Plancontrols[0].id}`, { status: "ativo" });

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Empresa Atualisada",
          "Ativação realizada com sucesso.",
          "success"
        );
        getBusiness();
      }
    } else {
      showMessage(
        "Empresa Falhou",
        "Houve um problema ao atualizar sua Empresa, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  }

  const onBlockAccess = async (emp) => {
    setIsLoading(true);
    const res = await api.put(`/plancontrol/edit/${emp.Plancontrols[0].id}`, { status: "bloqueado" });

    if (res.data) {
      if (res.data.message === "success") {
        showMessage(
          "Empresa Atualizada",
          "Bloqueio realizado com sucesso.",
          "success"
        );
        getBusiness();
      }
    } else {
      showMessage(
        "Empresa Falhou",
        "Houve um problema ao atualizar sua Empresa, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  }

  const onModifyPlan = (emp) => {
    setShowEditPlan(true);
    setGetEditPlan(emp.Plancontrols[0])
  }

  return (
    <Page className={classes.root} title="Empresas">
      <Container maxWidth={false}>
        <Toolbar
          SearchBusiness={buscaBusiness}
          onShowNewBusiness={onShowNewBusiness}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onActiveAccess={onActiveAccess}
            onBlockAccess={onBlockAccess}
            onModifyPlan={onModifyPlan}
            onShowModalEditBusiness={onShowEditBusiness}
            onDeleteBusiness={onShowConfirm}
            customers={business}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewBusiness
        open={showNewBusiness}
        firstAccess={firstAccess}
        onCloseNewBusiness={onCloseNewBusiness}
        onDisableFirstAccess={onDisableFirstAccess}
        getAllBusiness={getBusiness}
      />
      <ModalEditBusiness
        getEditBusiness={getEditBusiness}
        open={showEditBusiness}
        isLoading={isLoading}
        onCloseEditBusiness={onCloseEditBusiness}
        onDeleteBusiness={onDeleteBusiness}
        onUpdateBusiness={onUpdateBusiness}
      />

      <ModalPlan
        getEditPlan={getEditPlan}
        getAllBusiness={getBusiness}
        open={showEditPlan}
        isLoading={isLoading}
        onCloseEditPlan={onCloseEditPlan}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, a Empresa será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
