import React, { useState, Fragment } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Tooltip
} from '@material-ui/core';
import getInitials from '../../../utils/getInitials';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  Block as BlockIcon,
  Beenhere as BeenhereIcon,
  AccountTree as AccountTreeIcon

} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  }
}));

const Results = ({
  onModifyPlan,
  onActiveAccess,
  onBlockAccess,
  onDeleteBusiness,
  onShowModalEditBusiness,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  ...rest }) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds, id);
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(1));
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str).toLowerCase().replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); });
  }

  const formatTel = (str) => {
    return String(str).replace(/\D/g, '').length == 11 ?
      String(str).replace(/\D/g, '').replace(/(\d{2})(\d)(\d{4})(\d{4})$/, '($1) $2 $3-$4') :
      String(str).replace(/\D/g, '').replace(/(\d{2})(\d{4})(\d{4})$/, '($1) $2-$3');
  }

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0
                      && selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell style={{ fontWeight: "bold" }}>{title.value}</TableCell>)
                )}
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.slice((page * limit), (page * limit) + limit).map((customer) => (
                <Fragment>
                  <TableRow
                    hover
                    key={customer.id}
                    selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={selectedCustomerIds.indexOf(customer.id) !== -1}
                        onChange={(event) => handleSelectOne(event, customer.id)}
                        value="true"
                      />
                    </TableCell>
                    <TableCell>
                      <Box
                        alignItems="center"
                        display="flex"
                      >
                        <Avatar
                          className={classes.avatar}
                          /* src={customer.avatarUrl} */
                          src={customer.avatarUrl}
                        >
                          {getInitials(customer.business_name)}
                        </Avatar>
                        <Typography
                          color="textPrimary"
                          variant="body1"
                        >
                          {formatStringName(customer.business_name)}
                        </Typography>
                      </Box>
                    </TableCell>
                    <TableCell>
                      {customer.email}
                    </TableCell>
                    <TableCell>
                      {`${formatStringName(customer.public_place)}, ${formatStringName(customer.district)}, ${formatStringName(customer.county)}`}
                    </TableCell>
                    <TableCell>
                      {formatTel(customer.phone1)}
                    </TableCell>
                    <TableCell>
                      {moment(customer.created_at).format('DD/MM/YYYY')}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ height: 0 }}>
                    <TableCell style={{ height: "auto !important" }} />
                    <TableCell align="left">
                      {!(customer.Plancontrols.length > 0) ? null : !(customer.Plancontrols[0].status == "ativo") ?
                        (<Tooltip title="Ativar Acesso" arrow>
                          <IconButton
                            disabled={isLoading}
                            onClick={() => { onActiveAccess(customer) }}
                            color="inherit">
                            <BeenhereIcon style={{ color: '#26de81' }} />
                          </IconButton>
                        </Tooltip>) :
                        (<Tooltip title="Bloquear Acesso" arrow>
                          <IconButton
                            disabled={isLoading}
                            onClick={() => { onBlockAccess(customer) }}
                            color="inherit">
                            <BlockIcon style={{ color: '#fa8231' }} />
                          </IconButton>
                        </Tooltip>)}
                      <Tooltip title="Modificar Plano" arrow>
                        <IconButton
                          disabled={isLoading}
                          onClick={() => { onModifyPlan(customer) }}
                          color="inherit">
                          <AccountTreeIcon style={{ color: '#45aaf2' }} />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Editar Dados" arrow>
                        <IconButton
                          disabled={isLoading}
                          onClick={() => { onShowModalEditBusiness(customer) }}
                          color="inherit">
                          <EditIcon style={{ color: '#686de0' }} />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Deletar Empresa" arrow>
                        <IconButton
                          disabled={isLoading}
                          style={{ marginLeft: 5 }}
                          onClick={() => { onDeleteBusiness(customer.id) }}
                          color="inherit">
                          <DeleteIcon style={{ color: '#ff4d4d' }} />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                    <TableCell />
                    <TableCell />
                    <TableCell />
                    <TableCell />
                  </TableRow>
                </Fragment>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage='Linhas por página:'
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card >
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired
};

export default Results;
