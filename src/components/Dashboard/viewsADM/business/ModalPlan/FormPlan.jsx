import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  Switch,
  CardActions,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  ImageSearch as ImageSearchIcon,
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";

const Formulario = ({ onCloseModal, getAllBusiness, getEditPlan }) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listPlans, setListPlans] = useState(null);
  const [value, setValue] = useState(0);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [preview, setPreview] = useState();
  const [selectedFile, setSelectedFile] = useState();
  const [values, setValues] = useState({
    planId: "",
  });

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566",
      },
    },
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "planId"
            ? " Plano "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
          key == "terms_and_conditions"
            ? "não foi aceito"
            : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    onSave();
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/plancontrol/edit/${getEditPlan.id}`, values)
      .then((response) => response.data)
      .then((resp) => {
        if (resp.message == "success") {
          showMessage(
            "Novo Plano",
            `Plano atualizado com sucesso.`,
            "success"
          );
          getAllBusiness();
          setIsLoading(false);
          onCloseModal();
        } else {
          showMessage("Novo plano", `Falhou.`, "error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    api
      .get("/plan")
      .then((response) => response.data)
      .then((resp) => {
        if (resp) {
          console.log(resp);
          setListPlans(resp);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Modificar planos da empresa selecionada"
            title="Plano"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações do Plano" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <p className="text-bold">Plano atual: </p>
                        <h3 className="ml-2"> {getEditPlan ? getEditPlan.Plan.name : ""}</h3>
                      </Box>

                      <Divider className="mb-4 mt-4" />
                    </Grid>


                    <Grid item md={12} xs={12}>
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Selecionar Novo Plano
                          </InputLabel>
                          <Select
                            required
                            native
                            name="planId"
                            id="planId"
                            value={values.planId}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listPlans &&
                              listPlans.map((Plan) => (
                                <option value={Plan.id}>{Plan.name}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                  </Grid>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84",
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
