import React, { useState, useContext, useEffect } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
  useTheme
} from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { useNavigate } from "react-router-dom";
import {
  Container,
  CssBaseline,
  Toolbar,
  CardContent,
  Card,
  CardHeader,
  CardActions,
  Divider,
  Box,
  Grid,
  TextField,
  Select,
  FormControl,
  InputLabel,
  IconButton,
  Tab,
  Tabs,
  AppBar,
  CircularProgress,
  FormControlLabel,
  FormGroup,
  Checkbox,
  Switch
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useErros from "../../../../../Errors/useErros";
import ContextValidacoesCadastro from "../../../../../contexts/ContextValidacoesCadastro";
import { UFs } from "../../../utils/uf";
import {
  ArrowForward as ArrowForwardIcon,
  ArrowBack as ArrowBackIcon,
  Check as CheckIcon,
  Close as CloseIcon,
  BusinessCenter as BusinessCenterIcon,
  AssignmentInd as AssignmentIndIcon,
  ImageSearch as ImageSearchIcon
} from "@material-ui/icons";
import { TabPanel, a11yProps } from "../help";
import { showMessage } from "../../../utils/message";
import api from "../../../../../services/api";
import MusicListView from "./MusicListView";

const Formulario = ({ onCloseModal, getAllGenreSub, getEditGenreSub }) => {
  let history = useNavigate();
  const validacoes = useContext(ContextValidacoesCadastro);
  const [erros, validarCampo, possoEnviar] = useErros(validacoes);
  const [listGenreSub, setListGenreSub] = useState(null);
  const [value, setValue] = useState(0);
  const [idMusics, setIdMusic] = useState([]);
  const [typeClient, setTypeClient] = useState("juridica");
  const [isLoading, setIsLoading] = useState(false);
  const [listRight, setListRight] = useState(null);
  const [preview, setPreview] = useState();
  const [selectedFile, setSelectedFile] = useState(false);
  const [isPejorative, setIsPejorative] = useState("0");
  const [values, setValues] = useState({
    name: "",
    rightId: "",
    typeSegment: ""
  });
  const listTypeSegments = [{ name: "academia" }, { name: "todos" }, { name: "invisivel" }];

  useEffect(() => {
    if (getEditGenreSub) {
      console.log(getEditGenreSub);
      setValues({
        ...values,
        name: getEditGenreSub.name,
        rightId: getEditGenreSub.rightId,
        typeSegment: getEditGenreSub.typeSegment
      });
      setIsPejorative(getEditGenreSub.pejorative);

      let arrayIdMusics = [];

      for (
        let index = 0;
        index < getEditGenreSub.Genresubitems.length;
        index++
      ) {
        const element = getEditGenreSub.Genresubitems[index];

        arrayIdMusics.push(element.musicId);
      }

      setIdMusic(arrayIdMusics);
    }
  }, [getEditGenreSub]);
  useEffect(() => {
    console.log(isPejorative);
  }, [isPejorative]);

  useEffect(() => {
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // memória livre sempre que este componente for desmontado
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onSelectFile = e => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    // Eu mantive este exemplo simples usando a primeira imagem em vez de vários
    setSelectedFile(e.target.files[0]);
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#1c2566"
      }
    }
  });

  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  const validateCampos = () => {
    for (var key in values) {
      if (
        (values[key] == "") |
        (String(values[key]) == "null") |
        (String(values[key]) == "false")
      ) {
        let campoRecused =
          key == "name"
            ? " Nome do Sub Gênero "
            : key == "rightId"
            ? " Grupo de Direitos "
            : key == "typeSegment"
            ? " Tipo de Segmento "
            : "Indefinido";

        showMessage(
          "Atenção",
          `Campo ${campoRecused} ${
            key == "terms_and_conditions"
              ? "não foi aceito"
              : "não foi preenchido"
          }.`,
          "warning"
        );

        return;
      }
    }

    if (!(idMusics.length > 0)) {
      showMessage("Atenção", "Nenhuma musica foi selecionada", "warning");

      return;
    }

    onSave();
  };

  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const onUpdate = () => {
    validateCampos();
  };

  const onSave = () => {
    setIsLoading(true);
    api
      .put(`/GenreSub/edit/${getEditGenreSub.id}`, {...values, pejorative: isPejorative})
      .then(response => response.data)
      .then(respSub => {
        if (respSub.message == "success") {
          api
            .post(`/GenreSubItem/new/${respSub.data.id}`, {
              music: idMusics
            })
            .then(response => response.data)
            .then(resp => {
              if (resp.message == "success") {
                if (selectedFile) {
                  const capaFormDataImg = new FormData();
                  capaFormDataImg.append("typeobect", true);
                  capaFormDataImg.append("file", selectedFile);

                  api
                    .post(
                      `/cover/edit/${getEditGenreSub.Covers[0].id}`,
                      capaFormDataImg
                    )
                    .then(response => response.data)
                    .then(resp => {
                      if (resp.message == "success") {
                        showMessage(
                          "Edição Sub gênero",
                          `Cadastro realizado com sucesso.`,
                          "success"
                        );

                        getAllGenreSub();
                        setIsLoading(false);
                        onCloseModal();
                      } else {
                        showMessage("Novo Gênero", `Falhou.`, "error");
                      }
                    })
                    .catch(error => {
                      console.log(error);
                    });
                } else {
                  showMessage(
                    "Edição Sub gênero",
                    `Cadastro realizado com sucesso.`,
                    "success"
                  );

                  getAllGenreSub();
                  setIsLoading(false);
                }
                onCloseModal();
              } else {
                showMessage("Edição Sub gênero", `Falhou.`, "error");
              }
            })
            .catch(error => {
              console.log(error);
            });
        } else {
          showMessage("Edição Sub gênero", `Falhou.`, "error");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const selectIdMusics = data => {
    if (data) {
      setIdMusic(data);
      console.log(data);
    }
  };

  useEffect(() => {
    api
      .get("/right")
      .then(response => response.data)
      .then(resp => {
        if (resp) {
          console.log(resp);
          setListRight(resp);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  return (
    <form autoComplete="off" noValidate>
      {/* <CssBaseline />
            <Toolbar /> */}

      <Container component="article" maxWidth="md" className="pb-2">
        <Box
          display="flex"
          justifyContent="flex-end"
          className="w-100 mt-2 mb-2 mr-2"
          p={0}
        >
          <IconButton
            style={{ backgroundColor: "white" }}
            onClick={onCloseModal}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Card style={{ borderRadius: 20 }}>
          <CardHeader
            subheader="Preencha as informações de seu Sub Gênero"
            title="Novo Sub Gênero"
          />

          <div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChangeTab}
                textColor="white"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Informações" {...a11yProps(0)} />
              </Tabs>
            </AppBar>
          </div>

          <CardContent>
            <div>
              <SwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={value}
                onChangeIndex={handleChangeIndex}
              >
                <TabPanel value={value} index={0} dir={theme.direction}>
                  <Grid container spacing={1}>
                    <Grid item md={4} xs={4}>
                      <Card style={{ borderRadius: 20 }}>
                        <CardContent>
                          <Box
                            alignItems="center"
                            display="flex"
                            flexDirection="column"
                          >
                            {/* imagem produto  */}
                            {!selectedFile ? (
                              <img
                                height={100}
                                width={100}
                                src={getEditGenreSub.Covers[0].path_s3}
                              />
                            ) : null}
                            {selectedFile && (
                              <img height={100} width={100} src={preview} />
                            )}
                          </Box>
                        </CardContent>
                        {/* Abri Diretorio */}
                        <Divider />

                        <CardActions>
                          <Button
                            color="primary"
                            fullWidth
                            variant="text"
                            htmlFor="sampleFile"
                            component="label"
                          >
                            Selecionar Capa
                          </Button>
                        </CardActions>

                        <input
                          onChange={onSelectFile}
                          type="file"
                          id="sampleFile"
                          style={{ display: "none" }}
                        />
                      </Card>
                    </Grid>
                    <Grid item md={8} xs={8}>
                      <TextField
                        value={values.name}
                        onChange={handleChange}
                        label="Nome do Sub Gênero"
                        margin="normal"
                        id="name"
                        name="name"
                        variant="outlined"
                        className={""}
                        fullWidth
                        required
                      />
                      <Box display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Grupo de Direitos
                          </InputLabel>
                          <Select
                            required
                            native
                            name="rightId"
                            id="rightId"
                            value={values.rightId}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listRight &&
                              listRight.map(Rights => (
                                <option value={Rights.id}>{Rights.name}</option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                      <Box mt={1} display="flex" justifyContent="center">
                        <FormControl requireds variant="outlined" fullWidth>
                          <InputLabel htmlFor="age-native-required">
                            Tipo de Segmento
                          </InputLabel>
                          <Select
                            required
                            native
                            name="typeSegment"
                            id="typeSegment"
                            value={values.typeSegment}
                            onChange={handleChange}
                          >
                            <option aria-label="None" value="" />
                            {listTypeSegments &&
                              listTypeSegments.map(typeSegment => (
                                <option value={typeSegment.name}>
                                  {String(typeSegment.name).toUpperCase()}
                                </option>
                              ))}
                          </Select>
                        </FormControl>
                      </Box>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <FormGroup>
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={isPejorative == "1" ? true : false}
                              onChange={event => {
                                setIsPejorative(
                                  event.target.checked ? "1" : "0"
                                );
                              }}
                            />
                          }
                          label="Contém termo pejorativo."
                        />
                      </FormGroup>
                    </Grid>
                  </Grid>
                  <Grid item md={12} xs={12}></Grid>
                  <Card
                    style={{
                      backgroundColor: "#EEEEEE",
                      borderRadius: 20,
                      marginTop: 10
                    }}
                  >
                    <MusicListView
                      getEditGenreSub={getEditGenreSub}
                      selectIdMusics={selectIdMusics}
                    />
                  </Card>
                </TabPanel>
              </SwipeableViews>
            </div>
          </CardContent>
          <Divider />

          <Box
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            className="w-100"
            p={2}
          >
            <Button
              onClick={onUpdate}
              disabled={isLoading}
              style={{
                borderRadius: 50,
                backgroundColor: "#10ac84"
              }}
              color="primary"
              variant="contained"
              size="medium"
              startIcon={<CheckIcon style={{ color: "white" }} />}
            >
              Salvar
            </Button>
            {isLoading && <CircularProgress />}
          </Box>
        </Card>
      </Container>
    </form>
  );
};

export default Formulario;
