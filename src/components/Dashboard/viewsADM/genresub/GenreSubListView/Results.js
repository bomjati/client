import React, { useState, Fragment } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  Hidden,
  IconButton,
  Badge,
  Collapse,
  Tooltip,
  Chip,
  Paper,
  TableContainer,
} from "@material-ui/core";
import getInitials from "../../../utils/getInitials";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  PlayCircleOutline as PlayCircleOutlineIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  KeyboardArrowUp as KeyboardArrowUpIcon,
  QueueMusic as QueueMusicIcon,
} from "@material-ui/icons";
import api from "../../../../../services/api";

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2),
  },
}));

const Results = ({
  onDeleteGenreSub,
  onDeleteMusicGenreSub,
  className,
  customers,
  isLoading,
  titlesTopGrid,
  onShowModalEditGenreSub,
  ...rest
}) => {
  const classes = useStyles();
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [itemsSub, setItemSub] = useState([]);
  const [open, setOpen] = useState(false);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const styleCellCustom = {
    fontWeight: "bold",
    backgroundColor: "black",
    color: "white",
  };

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds,
        id
      );
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(1)
      );
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const formatStringName = (str) => {
    return String(str)
      .toLowerCase()
      .replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
  };

  const formatTel = (str) => {
    return String(str).replace(/\D/g, "").length == 11
      ? String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d)(\d{4})(\d{4})$/, "($1) $2 $3-$4")
      : String(str)
          .replace(/\D/g, "")
          .replace(/(\d{2})(\d{4})(\d{4})$/, "($1) $2-$3");
  };

  return (
    <Card
      style={{ borderRadius: 20 }}
      className={clsx(classes.root, className)}
      {...rest}
    >
      <PerfectScrollbar>
        <Box minWidth={720}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedCustomerIds.length === customers.length}
                    color="primary"
                    indeterminate={
                      selectedCustomerIds.length > 0 &&
                      selectedCustomerIds.length < customers.length
                    }
                    onChange={handleSelectAll}
                  />
                </TableCell>
                <TableCell />
                {/* busca lista de titulos na pasta data.js */}
                {titlesTopGrid.map((title) => (
                  <TableCell align="left" style={{ fontWeight: "bold" }}>
                    {title.value}
                  </TableCell>
                ))}

                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {customers.length > 0 &&
                customers
                  .slice(page * limit, page * limit + limit)
                  .map((customer) => (
                    <Fragment>
                      <TableRow
                        hover
                        key={customer.id}
                        selected={
                          selectedCustomerIds.indexOf(customer.id) !== -1
                        }
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={
                              selectedCustomerIds.indexOf(customer.id) !== -1
                            }
                            onChange={(event) =>
                              handleSelectOne(event, customer.id)
                            }
                            value="true"
                          />
                        </TableCell>
                        <TableCell padding="checkbox">
                          <Tooltip title="Visualisar Musicas" arrow>
                            <IconButton
                              style={{
                                color: "#2c2c54",
                                marginRight: 20,
                              }}
                              aria-label="expand row"
                              size="small"
                              onClick={async () => {
                                if (!open[customer.id]) {
                                  const resI = await api.get(
                                    `/GenreSubitem/genresub/${customer.id}`
                                  );

                                  setItemSub(resI.data);
                                }

                                setOpen({
                                  ...open,
                                  [customer.id]: !JSON.parse(
                                    open[customer.id]
                                      ? open[customer.id]
                                      : "false"
                                  ),
                                });
                              }}
                            >
                              {open[customer.id] ? (
                                <KeyboardArrowUpIcon />
                              ) : (
                                <KeyboardArrowDownIcon />
                              )}
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                        <TableCell align="left">
                          <Box alignItems="center" display="flex">
                            <Avatar
                              className={classes.avatar}
                              /* src={customer.avatarUrl} */
                              src={
                                customer.Covers.length > 0
                                  ? customer.Covers[0].path_s3
                                  : ""
                              }
                            >
                              {getInitials(customer.name)}
                            </Avatar>
                            {customer.name}
                          </Box>
                        </TableCell>
                        <TableCell align="left">
                          <Chip
                            style={{
                              borderColor: "#3498db",
                              backgroundColor: "#3498db",
                              color: "white",
                            }}
                            label={customer.Right.name}
                            variant="outlined"
                          />
                        </TableCell>
                        <TableCell align="right">
                          <Tooltip title="Editar Sub Gênero" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onShowModalEditGenreSub(customer);
                              }}
                              color="inherit"
                            >
                              <EditIcon style={{ color: "#d35400" }} />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Deletar Sub Gênero" arrow>
                            <IconButton
                              disabled={isLoading}
                              style={{ marginLeft: 5 }}
                              onClick={() => {
                                onDeleteGenreSub(customer.id);
                              }}
                              color="inherit"
                            >
                              <DeleteIcon style={{ color: "#ff4d4d" }} />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell
                          style={{ paddingBottom: 0, paddingTop: 0 }}
                          colSpan={6}
                        >
                          <Collapse
                            in={open[customer.id]}
                            timeout="auto"
                            unmountOnExit
                          >
                            <Box margin={1}>
                              <Typography
                                variant="h6"
                                gutterBottom
                                component="div"
                              >
                                Musicas
                              </Typography>
                              <TableContainer
                                className="mb-4"
                                component={Paper}
                              >
                                <Table size="small" aria-label="purchases">
                                  <TableHead>
                                    <TableRow>
                                      <TableCell style={styleCellCustom} />
                                      <TableCell style={styleCellCustom}>
                                        Nome
                                      </TableCell>
                                      <TableCell style={styleCellCustom}>
                                        Artista
                                      </TableCell>
                                      <TableCell
                                        style={styleCellCustom}
                                        align="right"
                                      >
                                        Gênero
                                      </TableCell>
                                    </TableRow>
                                  </TableHead>
                                  <TableBody>
                                    {itemsSub.length > 0 &&
                                      itemsSub.map((historyRow) => (
                                        <TableRow key={historyRow.id}>
                                          <TableCell align="left">
                                            <Tooltip
                                              title="Deletar Musica"
                                              arrow
                                            >
                                              <IconButton
                                                disabled={isLoading}
                                                style={{ marginLeft: 5 }}
                                                onClick={() => {
                                                  onDeleteMusicGenreSub(
                                                    historyRow.id
                                                  );
                                                }}
                                                color="inherit"
                                              >
                                                <DeleteIcon
                                                  style={{ color: "#d35400" }}
                                                />
                                              </IconButton>
                                            </Tooltip>
                                          </TableCell>
                                          <TableCell component="th" scope="row">
                                            {historyRow.Music
                                              ? historyRow.Music.title
                                              : ""}
                                          </TableCell>
                                          <TableCell>
                                            {historyRow.Music
                                              ? historyRow.Music.artist
                                              : ""}
                                          </TableCell>
                                          <TableCell align="right">
                                            <Chip
                                              style={{
                                                borderColor: "#34495e",
                                                backgroundColor: "#34495e",
                                                color: "white",
                                              }}
                                              label={
                                                historyRow.Music
                                                  ? historyRow.Music.Genre.name
                                                  : ""
                                              }
                                              variant="outlined"
                                            />
                                          </TableCell>
                                        </TableRow>
                                      ))}
                                  </TableBody>
                                </Table>
                              </TableContainer>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </Fragment>
                  ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        labelRowsPerPage="Linhas por página:"
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[2, 5, 10, 25]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
};

export default Results;
