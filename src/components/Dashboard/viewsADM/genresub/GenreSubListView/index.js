import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import ModalNewGenreSub from "../ModalNewGenreSub";
import ModalEditGenreSub from "../ModalEditGenreSub";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [showNewGenreSub, setShowNewGenreSub] = useState(false);
  const [showNewBusiness, setShowNewBusiness] = useState(false);
  const [showEditGenreSub, setShowEditGenreSub] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [showConfirmMusic, setShowConfirmMusic] = useState(false);
  const [firstAccess, setFirstAccess] = useState(false);
  const [GenreSub, setGenreSub] = useState([]);
  const [GenreSubBusca, setGenreSubBusca] = useState([]);
  const [getEditGenreSub, setGetEditGenreSub] = useState([]);
  const [idGenreSubModify, setIdGenreSubModify] = useState(0);
  const [idGenreSubMusicModify, setIdGenreSubMusicModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getGenreSub();
  }, []);

  const buscaGenreSub = (value) => {
    const resultBusca = GenreSubBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    setGenreSub(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdGenreSubModify(id);
  };
  const onShowConfirmMusic = (id) => {
    setShowConfirmMusic(true);
    setIdGenreSubMusicModify(id);
  };
  const onShowNewGenreSub = () => {
    setShowNewGenreSub(true);
  };

  const onCloseNewGenreSub = () => {
    setShowNewGenreSub(false);
  };

  const onShowNewBusiness = () => {
    setShowNewBusiness(true);
  };

  const onCloseNewBusiness = () => {
    setShowNewBusiness(false);
  };

  const onShowEditGenreSub = async (value) => {
    const resI = await api.get(`/GenreSubitem/genresub/${value.id}`);

    console.log({ ...value, Genresubitems: resI.data });
    setGetEditGenreSub({ ...value, Genresubitems: resI.data });
    setShowEditGenreSub(true);

  };

  const onCloseEditGenreSub = () => {
    setShowEditGenreSub(false);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteGenreSub(idGenreSubModify);
  };

  const onCloseMessageConfirmMusic = () => {
    setShowConfirmMusic(false);
  };

  const onAcceptMessageConfirmMusic = () => {
    setShowConfirmMusic(false);
    onDeleteGenreSubMusic(idGenreSubMusicModify);
  };

  const onDisableFirstAccess = () => {
    setFirstAccess(false);
  };

  const getGenreSub = async () => {
    setIsLoading(true);
    const res = await api.get("/GenreSub/admin");

    if (res) {
      console.log(res.data);
      let arrayFull = [];
      const resCovers = await api.get("/cover");
      const resRights = await api.get("/right");

      let genresubInfo = [];

      for (let index = 0; index < res.data.length; index++) {
        const element = res.data[index];

        genresubInfo.push({
          ...element,
          Covers: resCovers.data.filter((c) => c.genresubId == element.id),
          Right: resRights.data.filter((r) => r.id == element.rightId)[0],
        });
      }

      /* for (let index = 0; index < res.data.length; index++) {
        const element = res.data[index];

        const resI = await api.get(`/GenreSubitem/genresub/${element.id}`);

        arrayFull.push({ ...element, Genresubitems: resI.data })
      } */

      // setGenreSub(res.data);
      // setGenreSubBusca(res.data);
      setGenreSub(genresubInfo);
      setGenreSubBusca(genresubInfo);
    }

    setIsLoading(false);
  };

  const onDeleteGenreSub = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/GenreSub/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getGenreSub();
        onCloseEditGenreSub();
        showMessage(
          "Sub Gênero Deletado",
          "Sub Gênero selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Sub Gênero Falhou",
        "Houve um problema ao deletar seu Sub Gênero, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  const onDeleteGenreSubMusic = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/GenreSubitem/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getGenreSub();
        onCloseEditGenreSub();
        showMessage(
          "Musica Deletada",
          "Musica de Sub Genero selecionado foi deletada com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Musica Falhou",
        "Houve um problema ao deletar sua Musica, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Sub Gênero">
      <Container maxWidth={false}>
        <Toolbar
          SearchGenreSub={buscaGenreSub}
          onShowNewGenreSub={onShowNewGenreSub}
        />
        <Box ml={-5} mr={-5} mt={6}>
          {isLoading && <LinearProgress />}
          <Results
            onShowModalEditGenreSub={onShowEditGenreSub}
            onDeleteGenreSub={onShowConfirm}
            onDeleteMusicGenreSub={onShowConfirmMusic}
            customers={GenreSub}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <ModalNewGenreSub
        open={showNewGenreSub}
        onCloseNewGenreSub={onCloseNewGenreSub}
        getAllGenreSub={getGenreSub}
      />
      <ModalEditGenreSub
        open={showEditGenreSub}
        onCloseNewGenreSub={onCloseEditGenreSub}
        getAllGenreSub={getGenreSub}
        getEditGenreSub={getEditGenreSub}
      />
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o Sub Genero será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
      <MessageConfirm
        open={showConfirmMusic}
        textInfo={`Você confirmando essa operação, a musica selecionada será deletada de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirmMusic}
        onAccept={onAcceptMessageConfirmMusic}
      />
    </Page>
  );
};

export default CompanyListView;
