import React, { useState, useEffect } from "react";
import { Box, Container, makeStyles, LinearProgress } from "@material-ui/core";
import Page from "../../../components/Page";
import Results from "./Results";
import Toolbar from "./Toolbar";
import { titlesGrid } from "./data";
import api from "../../../../../services/api";
import { showMessage } from "../../../utils/message";
import MessageConfirm from "../../../utils/MessageConfirm";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
}));

const CompanyListView = () => {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(false);
  const [showConfirm, setShowConfirm] = useState(false);
  const [Commercial, setCommercial] = useState([]);
  const [CommercialBusca, setCommercialBusca] = useState([]);
  const [idCommercialModify, setIdCommercialModify] = useState(0);
  const [titlesTopGrid] = useState(titlesGrid);

  useEffect(() => {
    getCommercial();
  }, []);

  const buscaCommercial = (value) => {
    let resultBusca = CommercialBusca.filter((i) => {
      return (
        String(i["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
        -1
      );
    });

    if(!(resultBusca.length > 0)){
      resultBusca = CommercialBusca.filter((i) => {
        return (
          String(i.Right["name"]).toUpperCase().indexOf(String(value).toUpperCase()) >
          -1
        );
      });
    }

    setCommercial(resultBusca);
  };
  const onShowConfirm = (id) => {
    setShowConfirm(true);
    setIdCommercialModify(id);
  };

  const onCloseMessageConfirm = () => {
    setShowConfirm(false);
  };

  const onAcceptMessageConfirm = () => {
    setShowConfirm(false);
    onDeleteCommercial(idCommercialModify);
  };

  const getCommercial = async () => {
    setIsLoading(true);
    const res = await api.get("/Commercial/*");

    if (res) {
      setCommercial(res.data);
      setCommercialBusca(res.data);
    }

    setIsLoading(false);
  };

  const onDeleteCommercial = async (id) => {
    setIsLoading(true);
    const res = await api.delete(`/Commercial/${id}`);

    if (res.data) {
      if (res.data.message === "success") {
        getCommercial();
        showMessage(
          "Gênero Deletado",
          "Gênero selecionado foi deletado com sucesso.",
          "success"
        );
      }
    } else {
      showMessage(
        "Gênero Falhou",
        "Houve um problema ao deletar seu gênero, entre em contato com o suporte.",
        "danger"
      );
    }
    setIsLoading(false);
  };

  return (
    <Page className={classes.root} title="Gênero">
      <Container maxWidth={false}>
        <Toolbar
          SearchCommercial={buscaCommercial}
        />
        <Box mt={3}>
          {isLoading && <LinearProgress />}
          <Results
            onDeleteCommercial={onShowConfirm}
            customers={Commercial}
            isLoading={isLoading}
            titlesTopGrid={titlesTopGrid}
          />
        </Box>
      </Container>
      <MessageConfirm
        open={showConfirm}
        textInfo={`Você confirmando essa operação, o comercial será deletado de sua base e não estará mais visivel em sua lista. Tem certeza dessa operação? `}
        title="Deseja excluir?"
        onClose={onCloseMessageConfirm}
        onAccept={onAcceptMessageConfirm}
      />
    </Page>
  );
};

export default CompanyListView;
